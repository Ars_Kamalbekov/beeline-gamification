<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 28.08.17
 * Time: 14:12
 */

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Testwork\Hook\Scope\BeforeSuiteScope;

/**
 * Defines application features from the specific context.
 */
class AdministrationContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^выбираю "([^"]*)"$/
     */
    public function выбираю($arg1)
    {
        $this->clickLink($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице BeeSchool$/
     */
    public function яНахожусьНаГлавнойСтраницеBeeSchool()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
        sleep(1);
    }

    /**
     * @When /^затем перехожу на страницу создания пользователя$/
     */
    public function затемПерехожуНаСтраницуСозданияПользователя()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_user_create'));
        sleep(2);
    }


    /**
     * @When /^я заполняю поля :$/
     */
    public function яЗаполняюПоля(TableNode $table)
    {
        $hash = $table->getHash();

        foreach ($hash as $row){
            $this->fillField('Логин', $row['Логин']);
            $this->fillField('email', $row['email']);
            $this->fillField('Активация', $row['Активация']);
            $this->fillField('Пароль', $row['Пароль']);
            $this->fillField('Повторите пароль', $row['Повторите пароль']);
            $this->fillField('Офис', $row['Офис']);
            sleep(2);
        }
    }

    /**
     * @When /^тогда я нажимаю кнопку "([^"]*)"$/
     */
    public function тогдаЯНажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
    }

    /**
     * @BeforeScenario
     * @login
     */
    public function before($event)
    {
        $this->visit('/login');
        $this->fillField('username', 'admin');
        $this->fillField('password', 'admin');
        $this->pressButton('_submit');
    }

    /**
     * @When /^я авторизуюсь как админ с логином "([^"]*)" и паролем "([^"]*)" и нажимаю кнопку "([^"]*)"$/
     */
    public function яАвторизуюсьКакАдминСЛогиномИПаролемИНажимаюКнопку($arg1, $arg2, $arg3)
    {
        $this->visit('/login');
        $this->fillField('username', $arg1);
        $this->fillField('password', $arg2);
        $this->pressButton($arg3);
    }


    /**
     * @When /^я заполняю поле "([^"]*)" с именем "([^"]*)"$/
     */
    public function яЗаполняюПолеСИменем($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^затем поле "([^"]*)" заполняю партнером под именем "([^"]*)"$/
     */
    public function затемПолеЗаполняюПартнеромПодИменем($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^потом заполняю поле "([^"]*)" с именем "([^"]*)"$/
     */
    public function потомЗаполняюПолеСИменем($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^в конце нажимаю кнопку "([^"]*)"$/
     */
    public function вКонцеНажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
    }

    /**
     * @When /^я нахожусь на странице создания партнера$/
     */
    public function яНахожусьНаСтраницеСозданияПартнера()
    {

        $this->visit($this->getContainer()->get('router')->generate('admin_app_partner_create'));
    }

    /**
     * @When /^я нахожусь на странице создания региона$/
     */
    public function яНахожусьНаСтраницеСозданияРегиона()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_region_create'));
    }

    /**
     * @When /^я нахожусь на странице создания офиса$/
     */
    public function яНахожусьНаСтраницеСозданияОфиса()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_office_create'));
    }

    /**
     * @When /^я нахожусь на странице создания пользователя$/
     */
    public function яНахожусьНаСтраницеСозданияПользователя()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_user_create'));
    }

    /**
     * @When /^я нахожусь на странице изменения обьявления$/
     */
    public function яНахожусьНаСтраницеИзмененияОбьявления1()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_announcement_list'));
        sleep(2);
    }

    /**
     * @When /^я меняю поле "([^"]*)" с сообщением "([^"]*)"$/
     */
    public function яМеняюПолеССообщением($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
    }

    /**
     * @When /^я нахожусь на странице загрузки excel файлов$/
     */
    public function яНахожусьНаСтраницеЗагрузкиExcelФайлов()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_exceldata_getdatafromexcel'));

    }

    /**
     * @When /^я вижу страницу с результатом$/
     */
    public function яВижуСтраницуСРезультатом()
    {

        $this->visit($this->getContainer()->get('router')->generate('app_exceldata_getdatafromexcel'));
        sleep(5);
    }

    /**
     * @When /^загружаю файл Восстановления в поле "([^"]*)"$/
     */
    public function загружаюФайлВосстановленияВПоле($arg1)
    {
        $this->getSession()->getPage()->attachFileToField($arg1, __DIR__.'/../../../../web/dummy/document/Восстановления.xlsx');
    }

    /**
     * @When /^загружаю файл результатов предыдущей игры в поле "([^"]*)"$/
     */
    public function загружаюФайлРезультатовПредыдущейИгрыВПоле($arg1)
    {
        $this->getSession()->getPage()->attachFileToField($arg1, __DIR__.'/../../../../web/dummy/document/Начало_3го_тура.xlsx');
    }

    /**
     * @When /^загружаю файл всех операций сотрудников в поле "([^"]*)"$/
     */
    public function загружаюФайлВсехОперацийСотрудниковВПоле($arg1)
    {
        $this->getSession()->getPage()->attachFileToField($arg1, __DIR__.'/../../../../web/dummy/document/все_операции.xlsx');
    }

    /**
     * @When /^где то на странице я навожу на выпадашку "([^"]*)"$/
     */
    public function гдеТоНаСтраницеЯНавожуНаВыпадашку($arg1)
    {
        $page = $this->getSession()->getPage();
        $findName = $page->find("css", $arg1);
        if (!$findName) {
            throw new Exception($arg1 . " could not be found");
        } else {
            $findName->mouseOver();
        }
        sleep(3);
    }

    /**
     * @When /^нажимаю на "([^"]*)"$/
     */
    public function нажимаюНа($arg1)
    {
        $this->clickLink($arg1);
        sleep(3);
    }


}