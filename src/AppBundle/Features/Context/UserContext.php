<?php
/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 28.08.17
 * Time: 14:12
 */

namespace AppBundle\Features\Context;



use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\MinkExtension\Context\MinkAwareContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class UserContext extends MinkContext implements MinkAwareContext
{
    /** @var  KernelAwareContext */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /** @var  MinkAwareContext */
    protected function getContainer()
    {

        return $this->kernel->getContainer();
    }

    /**
     * @When /^я заполняю поля :$/
     */
    public function яЗаполняюПоля(TableNode $table)
    {
        $hash = $table->getHash();

        foreach ($hash as $row){
            $this->fillField('Email', $row['Email']);
            $this->fillField('Username', $row['Username']);
            $this->fillField('Password', $row['Password']);
            $this->fillField('fos_user_registration_form_plainPassword_second', $row['fos_user_registration_form_plainPassword_second']);
            $this->fillField('Имя', $row['Имя']);
            $this->fillField('Фамилия', $row['Фамилия']);
            $this->fillField('Отчество', $row['Отчество']);
            $this->fillField('Офис', $row['Офис']);
            $this->fillField('Номер телефона', $row['Номер телефона']);
            sleep(2);
        }
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * @When /^я выбираю поля и нажимаю кнопку "([^"]*)":$/
     * @param $arg1
     * @param TableNode $table
     */
    public function яВыбираюПоляИНажимаюКнопку($arg1, TableNode $table)
    {
        $hash = $table->getHash();

        foreach ($hash as $row) {
            $this->fillField('Username', $row['Username']);
            $this->fillField('Password', $row['Password']);
        }

        $this->pressButton($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице BeeSchool$/
     */
    public function яНахожусьНаГлавнойСтраницеBeeSchool()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
        sleep(1);
    }

    /**
     * @When /^я нажимаю кнопку "([^"]*)"$/
     * @param $arg1
     */
    public function яНажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
    }

    /**
     * @When /^затем выбираю дату:$/
     */
    public function затемВыбираюДату(TableNode $table)
    {
        $this->fillField('Дата', '01.10.2017');
    }

    /**
     * @When /^перехожу на страницу создания геймификации$/
     */
    public function перехожуНаСтраницуСозданияГеймификации()
    {
        $this->visit($this->getContainer()->get('router')->generate('start_game'));
        sleep(2);
    }

    /**
     * @When /^я вижу ссылку  "([^"]*)" где\-то на странице$/
     * @param $arg1
     */
    public function яВижуСсылкуГдеТоНаСтранице($arg1)
    {
        $this->clickLink($arg1);
        sleep(2);
    }
    /**
     * @When /^я выбираю игрока под номером (\d+) и перехожу на страницу его профиля$/
     * @param $arg1
     */
    public function яВыбираюИгрокаПодНомеромИПерехожуНаСтраницуЕгоПрофиля($arg1)
    {
        $this->visit($this->getContainer()->get('router')->generate('profile',['user_id'=>$arg1]));
        sleep(3);
    }




}