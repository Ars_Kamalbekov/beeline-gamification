# language: ru

Функционал: Тестируем страницу регистрации пользователя
  @registration
  Сценарий: Регистрация пользователя на сайте
    Допустим я нахожусь на главной странице BeeSchool
    И я вижу ссылку  "Регистрация" где-то на странице
    Тогда я заполняю поля :
    | Email       | Username | Password | fos_user_registration_form_plainPassword_second | Имя | Фамилия | Отчество | Офис | Номер телефона |
    | asd@asd.asd | asd      | asd      | asd                                             | asd | asd     | asd      | Sum  | 103057         |
    И я нажимаю кнопку "Register"