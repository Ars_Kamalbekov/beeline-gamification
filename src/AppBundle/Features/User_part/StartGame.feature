# language: ru
@start_game
Функционал: Тестируем страницу создания геймификации

  @login
  Сценарий: Администратор начинает игру
    Допустим я нахожусь на главной странице BeeSchool
    И я вижу ссылку  "Start Game" где-то на странице
    И перехожу на страницу создания геймификации
    И затем выбираю дату:
    | Дата       |
    | 01.10.2017 |
    И я нажимаю кнопку "Начать игру"
