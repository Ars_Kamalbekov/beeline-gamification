<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170920112400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE battle ADD game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE battle ADD CONSTRAINT FK_13991734E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_13991734E48FD905 ON battle (game_id)');
        $this->addSql('ALTER TABLE fos_user ADD quartet VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE quartet ADD game_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE quartet ADD CONSTRAINT FK_F5E24432E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('CREATE INDEX IDX_F5E24432E48FD905 ON quartet (game_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE battle DROP FOREIGN KEY FK_13991734E48FD905');
        $this->addSql('DROP INDEX IDX_13991734E48FD905 ON battle');
        $this->addSql('ALTER TABLE battle DROP game_id');
        $this->addSql('ALTER TABLE fos_user DROP quartet');
        $this->addSql('ALTER TABLE quartet DROP FOREIGN KEY FK_F5E24432E48FD905');
        $this->addSql('DROP INDEX IDX_F5E24432E48FD905 ON quartet');
        $this->addSql('ALTER TABLE quartet DROP game_id');
    }
}
