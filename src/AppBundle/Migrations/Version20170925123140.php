<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170925123140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user ADD quartet_id INT DEFAULT NULL, DROP quartet');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64799BBFFD6E FOREIGN KEY (quartet_id) REFERENCES quartet (id)');
        $this->addSql('CREATE INDEX IDX_957A64799BBFFD6E ON fos_user (quartet_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A64799BBFFD6E');
        $this->addSql('DROP INDEX IDX_957A64799BBFFD6E ON fos_user');
        $this->addSql('ALTER TABLE fos_user ADD quartet VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP quartet_id');
    }
}
