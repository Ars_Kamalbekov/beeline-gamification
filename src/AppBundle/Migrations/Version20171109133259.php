<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171109133259 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_quartet (user_id INT NOT NULL, quartet_id INT NOT NULL, INDEX IDX_ADEF5A38A76ED395 (user_id), INDEX IDX_ADEF5A389BBFFD6E (quartet_id), PRIMARY KEY(user_id, quartet_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_quartet ADD CONSTRAINT FK_ADEF5A38A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_quartet ADD CONSTRAINT FK_ADEF5A389BBFFD6E FOREIGN KEY (quartet_id) REFERENCES quartet (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user DROP FOREIGN KEY FK_957A64799BBFFD6E');
        $this->addSql('DROP INDEX IDX_957A64799BBFFD6E ON fos_user');
        $this->addSql('ALTER TABLE fos_user DROP quartet_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user_quartet');
        $this->addSql('ALTER TABLE fos_user ADD quartet_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user ADD CONSTRAINT FK_957A64799BBFFD6E FOREIGN KEY (quartet_id) REFERENCES quartet (id)');
        $this->addSql('CREATE INDEX IDX_957A64799BBFFD6E ON fos_user (quartet_id)');
    }
}
