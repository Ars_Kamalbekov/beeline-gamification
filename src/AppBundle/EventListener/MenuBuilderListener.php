<?php
// src/AppBundle/EventListener/MenuBuilderListener.php

namespace AppBundle\EventListener;

use Sonata\AdminBundle\Event\ConfigureMenuEvent;

class MenuBuilderListener
{
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('game', [
            'route' => 'start_game',
        ])->setExtras([
            'icon' => '<i class="fa fa-spinner fa-fw"></i>',
        ]);
    }
}