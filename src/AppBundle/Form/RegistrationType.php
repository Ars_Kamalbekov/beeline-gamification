<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


class RegistrationType extends AbstractType
{

    const LABELS = ['POST' => 'Регистрация', 'PUT' => 'Обновить', 'DELETE' => 'Удалить'];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Имя'
            ])
            ->add('surname', TextType::class, [
                'label' => 'Фамилия'
            ])
            ->add('patronymic', TextType::class, [
                'label' => 'Отчество'
            ])
            ->add('office', EntityType::class, [
                'class' => 'AppBundle\Entity\Office',
                'choice_label' => 'name',
                'placeholder' => 'Выберите офис',
                'label' => 'Офис'
            ])
            ->add('phone_number', IntegerType::class, [
                'label' => 'Номер телефона'
            ])
            ->add($options['method'], SubmitType::class, [
                'label' => self::LABELS[$options['method']],
                'attr' => [
                    'class' => 'btn btn-primary btn-lg'
                ]]);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }


}
