<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 11/10/17
 * Time: 2:55 AM
 */

namespace AppBundle\Libs;
use AppBundle\Entity\Badge;
use AppBundle\Entity\BattleResult;
use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use Symfony\Component\HttpFoundation\Session\Session;

class ExcelManager
{
    private $entityManager;
    private $phpExcel;
    private $session;

    public function __construct(EntityManager $entityManager, Factory $PHPExcel, Session $session)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->phpExcel = $PHPExcel;
    }

    /**
     * @param int $id
     * @param $excel
     * @return array|bool
     */
    public function getDataFromExcelAction(int $id, $excel)
    {
        $this->session->get('session');
        $this->session->set('game', $id);
        $userNames = null;
        $needed_data = 0;
        $badge = false;
        /** @var \PHPExcel_Worksheet $sheet */
        $sheet = $this->getSheet($excel);
        $columns = $this->getColumns();

        foreach ($columns as $column) {
            $value = $sheet->getCell($column . 1)->getValue();
            if ($value == 'ТП на 5 день') {
                $needed_data++;
                $tp_column = $column;
            }

            if ($value == 'Логин оператора') {
                $needed_data++;
                $login_column = $column;
            }

            if ($needed_data == 2) {
                $this->getActivatedTpData($sheet, $tp_column, $login_column);
                break;
            }

            if ($value == 'Логин оператора CRM') {
                $this->getRestorationFromExcelAction($sheet, $column);
                break;
            }

            if ($value == 'Бейдж') {
                $badge = true;
                $badge_column = $column;
            }

            if (isset($login_column) && $badge){
                $this->setBadgesForUniqueUsers($sheet, $login_column, $badge_column);
                break;
            }
        }



        return false;
    }


    public function getSheet($excel){
        $phpExcelObject = $this->phpExcel->createPHPExcelObject($excel);
        return $phpExcelObject->getActiveSheet();
    }


    /**
     * @var \PHPExcel_Worksheet $sheet
     * @param $tp_column
     * @param $login_column
     */
    public function getActivatedTpData($sheet, $tp_column, $login_column)
    {
        $userNames = [];
        $row = 2;
        while ($sheet->getCell($login_column . $row)->getValue() != null){
            $userNames[$sheet->getCell($login_column . $row)->getValue()]
            [] = $sheet->getCell($tp_column . $row)->getValue();
            $row++;
        }

        $tariffPlans = $this->entityManager->getRepository('AppBundle:Points')->findAll();

        $registryInfo = $this->checkUsers($userNames);

        $this->setPointsForActivatedTP($registryInfo['registered'], $tariffPlans);
//        $notRegisteredUsers = $this->setPointsForActivatedTP($registryInfo['not_registered'], $tariffPlans);
    }

    public function setPointsForActivatedTP($users, $tariffPlans){
        foreach ($users as $user => $user_tp) {
            $points = 0;
            foreach ($user_tp as $tp) {
                foreach ($tariffPlans as $plan) {
                    if (strtolower($tp) == strtolower($plan->getName())) {
                        $points += $plan->getPoint();
                    }
                }
            }
            $users[$user]['point'] = $points;
        }
        $battleResults = $this->getBattleResults('tariffPlans');
        /** @var BattleResult $battleResult */
        foreach ($battleResults as $battleResult){
            foreach ($users as $username => $data){
                if ($battleResult->getUser()->getUsername() == $username){
                    $battleResult->setUserPoints($data['point']);
                } else if ($battleResult->getOpponent()->getUsername() == $username){
                    $battleResult->setOpponentPoints($data['point']);
                }
            }
        }
        $this->entityManager->flush();
    }

    public function saveTourData($result){
        $users = $this->entityManager->getRepository('AppBundle:User')
            ->findAll();

        foreach ($result['registered'] as $key => $value){
            foreach ($users as $user){
                if ($user->getUsername() == $key){
                    $user->setBalance($value['balance']);
                    $user->setPoints($value['points']);
                }
            }
        };
        $this->entityManager->flush();
    }

    public function checkUsers($exc_usersLogins){
        $users = $this->entityManager->getRepository('AppBundle:User')
            ->findAll();
        $regLoginsInDB = [];
        $registeredUsers = [];
        $notRegisteredUsers = [];
        foreach ($users as $user){
            $regLoginsInDB[] = $user->getUsername();
        }

        foreach ($exc_usersLogins as $key => $value){
            if (!in_array($key, $regLoginsInDB)){
                $notRegisteredUsers[$key] = $value;
                unset($exc_usersLogins[$key]);
            } else {
                $registeredUsers[$key] = $value;
            }
        }

        return ['registered' => $registeredUsers, 'not_registered' => $notRegisteredUsers];
    }

    public function getColumns(){
        $alphabet = range('A', 'Z');
        $alphabetCount = count($alphabet);
        $columns = $alphabet;

        for ($l = 0; $l < $alphabetCount; $l++){
            for ($j = 0; $j < $alphabetCount; $j++){
                $columns[] = $alphabet[$l] . $alphabet[$j];
            }
        }
        return $columns;
    }

    /**
     * @var \PHPExcel_Worksheet $sheet
     * @param $column
     */
    public function getRestorationFromExcelAction($sheet, $column)
    {
        $result = [];
        $restorations = [];
        $restoration = $this->entityManager->getRepository('AppBundle:Points')->findByName('Восстановление');
        $restoration_point = $restoration[0]->getPoint();
        $row = 2;

        while ($sheet->getCell($column . $row)->getValue() != null) {
            $username = $sheet->getCell($column . $row)->getValue();
            isset($restorations[$username]) ? $restorations[$username] += 1 : $restorations[$username] = 1;
            $row++;
        }


        foreach ($restorations as $username => $restoration) {
            $result[$username]['restorations'] = $restoration;
            $result[$username]['points'] = $restoration * $restoration_point;
        }

        $battleResults = $this->getBattleResults('restorations');
        /** @var BattleResult $battleResult */
        foreach ($battleResults as $battleResult){
            foreach ($result as $username => $data){
                if ($battleResult->getUser()->getUsername() == $username){
                    $battleResult->setUserRestorations($data['points']);
                } else if ($battleResult->getOpponent()->getUsername() == $username){
                    $battleResult->setOpponentRestorations($data['points']);
                }
            }
        }

        $this->entityManager->flush();
    }


    /**
     * @var \PHPExcel_Worksheet $sheet
     * @param $login_column
     * @param $badge_column
     */
    public function setBadgesForUniqueUsers($sheet, $login_column, $badge_column)
    {
        $user_for_badges = [];
//        $user_for_view = [];
        $row = 2;
        while ($sheet->getCell($login_column . $row)->getValue() != null) {
            $user_for_badges[$sheet->getCell($login_column . $row)->getValue()]
            [] = $sheet->getCell($badge_column . $row)->getValue();
            $row++;
        }

        $users_from_db = $this->entityManager->getRepository('AppBundle:User')->findAll();
        $guide_of_badges = $this->entityManager->getRepository('AppBundle:DirectoryBadge')->findAll();


        foreach ($user_for_badges as $user => $user_badge) {
            foreach ($users_from_db as $user_db) {
                if ($user_db->getUsername() == $user) {
                    foreach ($guide_of_badges as $badge_from_directory) {
                        if ($badge_from_directory->getName() == $user_badge[0]) {
                            $badge = new Badge();
                            $badge->setName($badge_from_directory->getName());
                            $badge->setImage($badge_from_directory->getImage());
                            $badge->setPoints($badge_from_directory->getPoints());
                            $badge->setUser($user_db);
                            $badge->setBadgeFile($badge_from_directory->getBadgeFile());
                            $this->entityManager->persist($badge);
                            $user_db->setBalance($badge->getPoints());
                            $user_db->addBadge($badge);
                        }
                    }
//                    $user_for_view [] = $user_db;
                }
            }
        }

        $this->entityManager->flush();
    }

    public function getBattleResults($for_what){
        $battles = null;
        $battleResults = null;
        $current_battle = null;
        if ($this->session->has('game')){
            $game = $this->session->get('game');
            $this->session->remove('game');
            $battles = $this->entityManager->getRepository('AppBundle:Battle')
                ->findBy(['game' => $game]);
        }

        foreach ($battles as $battle){
            if ($battle->getBattleResultsCount() < 10){
                $battleResults = $battle->getBattleResults();
                $current_battle = $battle;
                break;
            }
        }

        if ($for_what == 'restorations'){
            $current_battle->setIsRestorationsDownloaded(true);
        } else if ($for_what == 'tariffPlans'){
            $current_battle->setBattleResultsCount($current_battle->getBattleResultsCount() + 1);
        }


        return $battleResults;
    }
}