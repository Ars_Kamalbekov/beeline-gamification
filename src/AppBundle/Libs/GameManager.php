<?php
namespace AppBundle\Libs;
use AppBundle\Entity\Battle;
use AppBundle\Entity\BattleResult;
use AppBundle\Entity\Bot;
use AppBundle\Entity\Game;
use AppBundle\Entity\League;
use AppBundle\Entity\Quartet;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Created by PhpStorm.
 * User: rain
 * Date: 10/26/17
 * Time: 3:35 PM
 */

class GameManager
{
    private $entityManager;
    private $battleResults = [];

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function mayCreateGame($last_game)
    {
        if (count($last_game) > 0) {
            /** @var Game $last_game */
            return $last_game[0]->getNumber();
        } else {
            return false;
        }
    }

    public function getUsersCount($users)
    {
        return count($users);
    }

    public function createAndGetQuartets(array $users, Game $game)
    {
        $this->entityManager->persist($game);
        $users = $this->findAndRemoveAdmin($users);

        $users = $this->sortByPoints($users);
        $count = $this->getUsersCount($users);
        $last_users_count = $count % 4;
        $UsersEvenNum = $count - $last_users_count;
        $last_users = [];
        $quartets = [];

        for ($i = $count - 1; $i >= $UsersEvenNum; $i--) {
            var_dump($i);
            $last_users[] = $users[$i];
        }

        if ($last_users_count != 0) {
            array_splice($users, -$last_users_count);
        }

        $count = count($users);

        for ($i = 0; $i < $count; $i += 4) {
            $quartet = [
                $users[$i],
                $users[$i + 1],
                $users[$i + 2],
                $users[$i + 3]
            ];

            $quartets[] = $this->createQuartet($quartet, new Quartet());
        }

        if (count($last_users) > 0) {
            $quartets[] = $this->createQuartet($last_users, new Quartet());
        }
        /** @var Quartet $quartet */
        foreach ($quartets as $quartet) {
            $quartet->setGame($game);
            $this->entityManager->persist($quartet);
        }
        $this->entityManager->flush();
        $_SESSION['game'] = $game->getId();
    }


    public function createQuartet($users, $quartet)
    {
        /** @var User $user */
        foreach ($users as $user) {
            $user->addQuartet($quartet);
        }

        return $quartet;
    }

    public function getBattles(Game $game)
    {

        $battles = [];
        for ($i = 0; $i < 3; $i++) {
            $battles[$i] = new Battle();
            $battles[$i]->setNumber($i + 1);
            $battles[$i]->setBattleResultsCount(0);
            $battles[$i]->setGame($game);
            $this->entityManager->persist($battles[$i]);
        }

        $this->entityManager->flush();
        return $battles;
    }

    public function getBattlesInfo($quartets, $battles)
    {
        $battleTimeTable = [
            'battle_1' => [0 => 1, 2 => 3],
            'battle_2' => [1 => 2, 0 => 3],
            'battle_3' => [0 => 2, 1 => 3]
        ];

        /** @var Quartet $quartet */
        foreach ($quartets as $quartet) {
            $users = $quartet->getUsers();

            switch (count($users)) {
                case 1:
                    $bot = $this->getBot($users[0]);
                    $this->makeBattleResults($battles, $users[0], $bot, true);
                    $this->entityManager->persist($bot);
                    break;
                case 2:
                    $this->makeBattleResults($battles, $users[0], $users[1]);
                    break;
                case 3:
                    var_dump('case 3');
                    $bot = $this->getBot($users[0]);
                    $this->entityManager->persist($bot);
                    $users[] = $bot;
                    $this->quartetBattle($battleTimeTable, $battles, $users);
                    break;
                case 4:
                    $this->quartetBattle($battleTimeTable, $battles, $users);
                    break;

            }

        }
        $this->entityManager->flush();
        return $this->battleResults;
    }

    public function makeBattleResults(array $battles, User $user, User $opponent, bool $botOpponent = false){
        foreach ($battles as $battle) {
            $br = $this->getBattleResult(new BattleResult(), $battle);
            $br = $this->makeBattle($br, $user, $opponent, $botOpponent);
            $this->entityManager->persist($br);
            $this->battleResults[] = $br;
        }
    }

    public function makeBattle(BattleResult $br, User $user, User $opponent, bool $botOpponent = false){
        /** @var User $user */
        /** @var User $opponent */

        if ($botOpponent) {
            $points_sum = 0;
            $league_users = $user->getLeague()->getUsers();
            foreach ($league_users as $user) {
                $points_sum += $user->getPoints();
            }

            $opp_points = $points_sum / count($league_users);
            $br->setOpponentPoints($opp_points);
        }

        $br->setUser($user)
            ->setOpponent($opponent);

        return $br;
    }

    public function getBot($user){
        $usernames = ['BotGenius', 'BotBeeline', 'BotMonster', 'BotCat'];


        /** @var User $user */
        $bot = new User();
        $bot->setSurname('bbb')
            ->setName('bbb')
            ->setPatronymic('bbb')
            ->setUsername($usernames[rand(0, count($usernames) - 1)])
            ->setEmail($usernames[rand(0, count($usernames) - 1)] . '@email.com')
            ->setPassword('password')
            ->addQuartet($user->getQuartets()[count($user->getQuartets())- 1])
            ->setLeague($user->getLeague());

        return $bot;
    }

    public function getBattleResult(BattleResult $br, $battle){
        /** @var BattleResult $br */
        $br->setBattle($battle);
        return $br;
    }

    public function quartetBattle($battleTimeTable, $battles, $users){
        $battle_index = 0;

        foreach ($battleTimeTable as $battle) {
            foreach ($battle as $user_index => $opponent_index) {
                $br = new BattleResult();
                $br->setBattle($battles[$battle_index]);
                $br = $this->makeBattle($br, $users[$user_index], $users[$opponent_index]);

                $this->entityManager->persist($br);
                $this->battleResults[] = $br;
            }
            $battle_index++;
        }
        $this->entityManager->flush();
    }

    public function sortByPoints(array $users){
        $count = $this->getUsersCount($users);

        for ($i = 0; $i < $count; $i++) {
            for ($j = 0; $j < $count - 1; $j++) {
                if ($users[$j]->getPoints() < $users[$j + 1]->getPoints()) {
                    $temp = $users[$j];
                    $users[$j] = $users[$j + 1];
                    $users[$j + 1] = $temp;
                }
            }
        }

        return $users;
    }

    public function leagueDistribution(array $users, array $leagues){
        $users = $this->findAndRemoveAdmin($users);
        $leags = ['A', 'B', 'C', 'D'];
        $league_index = 0;
        $league = $this->getNeedLeague($leagues, $leags[$league_index]);
        $users = $this->sortByPoints($users);
        $counter = 1;
        $limit = 8;
        $lastLeague = false;
        /** @var User $user */
        foreach ($users as $user){
            if (!$lastLeague) {
                if ($leags[$league_index] != 'D') {
                    if ($counter == $limit) {
                        $league_index++;
                        $limit = $limit * 2;
                        $league = $this->getNeedLeague($leagues, $leags[$league_index]);
                    }
                } else {
                    $lastLeague = true;
                }
            }
            $user->setLeague($league);
            $counter++;


        }
        $this->entityManager->flush();
    }

    public function getNeedLeague(array $leagues, $needLeague){
        /** @var League $league */
        foreach ($leagues as $league){
            if ($league->getLeague() == $needLeague){
                return $league;
            }
        }

        return false;
    }

    public function findAndRemoveAdmin(array $users)
    {
        $count = $this->getUsersCount($users);

        for ($i = 0; $i < $count; $i++) {
            if ($users[$i]->getRoles()[0] == 'ROLE_ADMIN') {
                array_splice($users, $i, -$count + $i + 1);
                break;
            }
        }

        return $users;
    }
}