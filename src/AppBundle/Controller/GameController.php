<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Libs\ExcelManager;
use AppBundle\Libs\GameManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class GameController extends Controller
{
    /**
     * @Route("/start_game", name="start_game")
     * @Method({"GET","POST"})
     * @param Request $request
     * @param GameManager $gameManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function startGameAction(Request $request, GameManager $gameManager)
    {
        if (isset($_SESSION['game'])){
            $em = $this->getDoctrine()->getManager();
            $game = $em->getRepository('AppBundle:Game')->find($_SESSION['game']);
            $quartets = $em->getRepository('AppBundle:Quartet')->findBy(['game' => $game]);
            unset($_SESSION['game']);
            $battles = $gameManager->getBattles($game);
            $gameManager->getBattlesInfo($quartets, $battles);

            return $this->redirectToRoute('app_game_game', [
                'id' => $game->getId()
            ]);
        }
        $date = $request->get('date');

        if (!isset($date)) {
            return $this->render('@App/basic/game.html.twig');
        }

        $users = $this->getDoctrine()->getRepository('AppBundle:User')
            ->findAll();
        $leagues = $this->getDoctrine()->getRepository('AppBundle:League')
            ->findAll();
        $start_date = new \DateTime($date);
        $last_game = $this->getDoctrine()->getRepository('AppBundle:Game')
            ->getLastGame();
        $result = $gameManager->mayCreateGame($last_game);

        if ($result === false){
            return $this->render('@App/ExcelData/tourexist.html.twig', [
                'message' => 'Нельзя создать новый тур. Так как либо не были загружены результаты предыдущего тура, либо какой-то тур еще не закончился!'
            ]);
        }

        $gameNum = $result + 1;
        $game = new Game();


        $end_date = Date("Y-m-d", strtotime($start_date->format('Y-m-d') . ' +30 Day'));
        $end_date = new \DateTime($end_date);

        $game->setStartDate($start_date)
            ->setEndDate($end_date)
            ->setNumber($gameNum);


        $gameManager->leagueDistribution($users, $leagues);
        $gameManager->createAndGetQuartets($users, $game);
        return $this->redirectToRoute('start_game', [

        ]);
    }

    /**
     * @Route("/game/{id}")
     * @Method({"GET", "POST"})
     * @param int $id
     * @param Request $request
     * @param ExcelManager $excelManager
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param GameManager $gameManager
     */
    public function game(int $id, Request $request, ExcelManager $excelManager)
    {

        $game = $this->getDoctrine()->getRepository('AppBundle:Game')
            ->find($id);

        $battles = $game->getBattles();

        $data = [
            'first_battle_result' => $battles[0]->getBattleResults(),
            'second_battle_result' => $battles[1]->getBattleResults(),
            'third_battle_result' => $battles[2]->getBattleResults()
        ];

        $form = $this->createForm('AppBundle\Form\ExcelType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $excel = $form->getData()['excel'];
            $excelManager->getDataFromExcelAction($game->getId(), $excel);
            return $this->redirectToRoute('app_game_game', [
                'id' => $game->getId()
            ]);
        }

        return $this->render('AppBundle:Game:new_game.html.twig', [
            'quartets' => $game->getQuartets(),
            'game' => $game,
            'battles' => $battles,
            'data' => $data,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/games")
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function gameListAction(){
        $games = $this->getDoctrine()->getRepository('AppBundle:Game')
            ->findBy(['previous_game' => false]);

        return $this->render('AppBundle:Game:game_list.html.twig', [
            'games' => $games
        ]);
    }

    /**
     * @Route("/download-game")
     * @param Request $request
     * @param ExcelManager $excelManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadPreviousGameAction(Request $request, ExcelManager $excelManager)
    {
        $form = $this->createForm('AppBundle\Form\GameType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $tour_num = $data['number'];
            $columnsName = ['Логины', 'Итого баланс на начало ' . ($tour_num + 1) . ' тура', 'Итого баллов'];
            $columnsCells = [];
            $tourResult = [];
            $need_columns = 0;
            $games = $this->getDoctrine()->getRepository('AppBundle:Game')
                ->findByNumber($tour_num);

            if (count($games) != 0){
                return $this->render('AppBundle:ExcelData:tourexist.html.twig', [
                    'message' => 'Тур с номером '. $tour_num .' уже был!'
                ]);
            }

            /** @var \PHPExcel_Worksheet $sheet */
            $sheet = $excelManager->getSheet($data['excel']);

            $columns = $excelManager->getColumns();

            foreach ($columns as $column) {
                $value = $sheet->getCell($column . 1)->getValue();

                if ($value == $columnsName[0]) {
                    $columnsCells['username'] = $column;
                    $need_columns++;
                }

                if ($value == $columnsName[1]) {
                    $need_columns++;
                    $columnsCells['balance'] = $column;
                }

                if ($value == $columnsName[2]){
                    $need_columns++;
                    $columnsCells['points'] = $column;
                }

                if ($need_columns == 3){
                    break;
                }
            }

            if ($need_columns != 3){
                return $this->render('AppBundle:ExcelData:tourexist.html.twig', [
                    'message' => 'Ошибка! Необходимые поля для загрузки предыдущей игры не найдены!'
                ]);
            }

            $row = 2;
            while ($sheet->getCell($columnsCells['username'] . $row)->getValue() != null) {
                $tourResult[$sheet->getCell($columnsCells['username'] . $row)->getValue()] =
                    [
                        'balance' => $sheet->getCell($columnsCells['balance'] . $row)->getValue(),
                        'points' => $sheet->getCell($columnsCells['points'] . $row)->getValue()
                    ];
                $row++;
            }

            $usersTourResult = $excelManager->checkUsers($tourResult);

            $tour = new Game();
            /** @var \DateTime $start_date */
            $start_date = $data['start_date'];
            $end_date = Date("Y-m-d", strtotime($start_date->format('Y-m-d') . ' +30 Day'));
            $end_date = new \DateTime($end_date);
            $tour->setNumber($tour_num)
                ->setPreviousGame(true)
                ->setStartDate($start_date)
                ->setEndDate($end_date);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tour);
            $em->flush();

            $excelManager->saveTourData($usersTourResult);

            return $this->render('@App/basic/game.html.twig');
        }

        return $this->render('AppBundle:Game:download_previous_game.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
