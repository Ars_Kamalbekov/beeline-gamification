<?php

namespace AppBundle\Controller;

use AppBundle\Entity\History;
use AppBundle\Entity\User;
use AppBundle\Form\AvatarType;
use AppBundle\Form\RegistrationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST", "HEAD"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $session = $this->get('session');

        if ($session->has('message')){
            $message = $session->get('message');
            $session->remove('message');
        }

        $announcement = $this->getDoctrine()
            ->getRepository('AppBundle:Announcement')
            ->findAll();

        $form = $this->createForm(TextareaType::class, $announcement);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->render('@App/basic/index.html.twig', [
            'message' => $message ?? null,
            'announcement' => $announcement,
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/user/{user_id}", name="profile")
     * @param int $user_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(int $user_id, Request $request)
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        /** @var User $user */
        $profile = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($user_id);

        $form = $this->createForm(AvatarType::class, $profile);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $profile->getImageFile();
            $profile->setAvatar($file);
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }
        return $this->render('@App/basic/profile.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
            'user_for_profile' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/user/{user_id}/edit", name="edit_profile")
     * @param int $user_id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editProfileAction(Request $request, $user_id)
    {
        /** @var User $user */
        $profile = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($user_id);

        $profile->setRoles(['ROLE_SUPER_USER']);

        $form =  $this->createForm( RegistrationType::class, $profile, [
            'method' => 'PUT'
        ]);
        $form->remove('username');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $histories = $profile->getHistories();
            /** @var History $history */
            foreach ($histories as $history) {
                if (!$history->getEndDate()) {
                    if ($history->getOffice() != $profile->getOffice()) {
                        $last_history = $history;
                        $new_history = new History();
                        $new_history->setStartDate(new \DateTime());
                        $new_history->setUser($profile);
                        $new_history->setOffice($profile->getOffice());
                        $last_history->setEndDate($new_history->getStartDate());

                        $em->persist($new_history);
                        $em->persist($last_history);
                    }
                }
            }

            $em->flush();
            return $this->redirectToRoute('profile',[
                'user_id' => $profile->getId()
            ]);
        }

        return $this->render('@App/basic/edit_profile.html.twig', [
            'profile' => $profile,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/all_users", name="all_users")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allUsersAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        $number_users = count($users);

        for ($i = 0; $i < $number_users; $i++){
            if ($users[$i]->getRoles()[0] == 'ROLE_ADMIN'){
                array_splice($users, $i, -$number_users + $i + 1);
                break;
            }
        }

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $paginate_user =  $paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 15)
        );

        return $this->render('@App/basic/all_users.html.twig', [
            'paginate' => $paginate_user,
            'users'=> $number_users
        ]);
    }
}
