<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BadgeController extends Controller
{
    /**
     * @Route("/addBadge")
     */
    public function addBadgeAction()
    {
        return $this->render('AppBundle:Badge:addBadge.html.twig', array(
            // ...
        ));
    }


}
