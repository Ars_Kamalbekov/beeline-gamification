<?php
/**
 * Created by PhpStorm.
 * User: rain
 * Date: 8/28/17
 * Time: 5:46 PM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MessageSenderController extends Controller
{
    /**
     * @Route("/message-to-user/{user_id}")
     * @param int $user_id
     * @Method({"GET", "POST"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendSuccessRegisterAction(int $user_id){
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->find($user_id);
        $em = $this->getDoctrine()->getManager();

        $message = Swift_Message::newInstance()
            ->setSubject('Beeline Gamification')
            ->setFrom('itap.test.for.studens@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                'Поздравляем, ' . $user->getUsername() . '! Администратор подтвердил вашу регистрацию. Теперь вы можете войти на сайт!'
            );

        $this->get('mailer')->send($message);
        $user->setIsNotified(true);

        $em->flush();

        return $this->redirectToRoute('sonata_admin_dashboard');
    }

    /**
     * @Route("/message-to-admin/{user_id}")
     * @Method("GET")
     * @param int $user_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendRegistrationNotifyToAdminAction(int $user_id){
        $user = $this->getDoctrine()->getRepository('AppBundle:User')
            ->find($user_id);

        $message = Swift_Message::newInstance()
            ->setSubject('Beeline Gamification')
            ->setFrom($user->getEmail())
            ->setTo('itap.test.for.studens@gmail.com')
            ->setBody(
                $user->getName() . " " . $user->getSurname() . " " . $user->getPatronymic() . ' (' . $user->getUsername() . ') хочет зарегистрироваться на вашем сайте.');

        $this->get('mailer')->send($message);

        $session = $this->get('session');
        $session->set('message', 'Вам теперь остается только дождаться одобрения вашей регистрации Администратором сайта. Сообщение о вашей регистрации уже отправлено на Email администратора. Как только администратор подтвердит вашу регистрацию, мы отправим на ваш Email сообщение об успешной регистрации. Спасибо!');
        return $this->redirectToRoute('homepage', [
        ]);
    }
}