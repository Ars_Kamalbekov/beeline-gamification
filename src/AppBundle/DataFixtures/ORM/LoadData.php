<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Badge;
use AppBundle\Entity\DirectoryBadge;
use AppBundle\Entity\History;
use AppBundle\Entity\League;
use AppBundle\Entity\Office;
use AppBundle\Entity\Partner;
use AppBundle\Entity\Points;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $partner_1 = new Partner();
        $partner_1
            ->setName('Mobilnik');

        $manager->persist($partner_1);

        $partner_2 = new Partner();
        $partner_2
            ->setName('Mobilniye seti');

        $manager->persist($partner_2);

        $partner_3 = new Partner();
        $partner_3
            ->setName('Planeta GSM');

        $manager->persist($partner_3);

        $region_1 = new Region();
        $region_1
            ->setName('Bishkek');

        $manager->persist($region_1);

        $region_2 = new Region();
        $region_2
            ->setName('Osh');

        $manager->persist($region_2);

        $region_3 = new Region();
        $region_3
            ->setName('Naryn');

        $manager->persist($region_3);

        $office_1 = new Office();
        $office_1
            ->setName('Bishkek park')
            ->setPartner($partner_1)
            ->setRegion($region_1);

        $manager->persist($office_1);

        $office_2 = new Office();
        $office_2
            ->setName('TsUM')
            ->setPartner($partner_2)
            ->setRegion($region_2);

        $manager->persist($office_2);

        $office_3 = new Office();
        $office_3
            ->setName('GUM')
            ->setPartner($partner_3)
            ->setRegion($region_3);

        $manager->persist($office_3);

        $user = new User();
        $user
            ->setUsername('admin')
            ->setRoles(["ROLE_ADMIN"])
            ->setEnabled(true)
            ->setEmail('admin@gmail.com')
            ->setName('admin')
            ->setPatronymic('adminovich')
            ->setPhoneNumber('0776776776')
            ->setStatus(true)
            ->setSurname('adminov');

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, 'admin');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
        $tps = ['Всё Premium' => 4, 'Всё +' => 3, 'Суйло 100' => 3, 'Всё' => 2, 'Суйло 70' => 2, 'Суйло 40' => 1, 'Чексиз Суйло' => 1, 'Восстановление' => 1];
        foreach ($tps as $points => $point){
            $TP = new Points();
            $TP->setName($points);
            $TP->setPoint($point);
            $manager->persist($TP);
        }

        $badges_name = ['Мой Beeline KG'=>700, 'NPS'=>400, 'Качественные подключения'=> 500,'Collaborative'=>1000, 'Тест'=>900];
        $count = 1;
        foreach ($badges_name as $name=>$points){
            $badge = new DirectoryBadge();
            $badge->setName($name);
            $badge->setImage($count.'.jpg');
            $count++;
            $badge->setPoints($points);
            $manager->persist($badge);
        }

        $appPath = $this->container->getParameter('kernel.root_dir');
        $xlsx_path = realpath($appPath . '/../src/AppBundle/DataFixtures/ORM/все операции.xlsx');
        $phpExcelObj = $this->container->get('phpexcel')->createPHPExcelObject($xlsx_path);
        $sheet = $phpExcelObj->getActiveSheet();
        $alphabet = range('A', 'Z');
        $alphabetCount = count($alphabet);
        $columns = $alphabet;

        for ($l = 0; $l < $alphabetCount; $l++){
            for ($j = 0; $j < $alphabetCount; $j++){
                $columns[] = $alphabet[$l] . $alphabet[$j];
            }
        }

        $data_found = 0;
        $registered_userNames = [];
        $allUserNames = [];
        $allFullNames = [];

        foreach ($columns as $column){
            if ($sheet->getCell($column . 1) == 'Логин оператора'){
                $allUserNames = $this->getData($sheet, $column);
                $data_found++;
            }

            if ($sheet->getCell($column . 1) == 'ФИО оператора'){
                $allFullNames = $this->getData($sheet, $column);
                $data_found++;
            }

            if ($data_found == 2) {
                break;
            }
        }

        $offices = [$office_1, $office_2, $office_3];
        $count_of_data = count($allUserNames);
        for ($i = 0; $i < $count_of_data; $i++){
            $username = $allUserNames[$i];
            $fullNameArray = explode(' ', $allFullNames[$i]);
            if (in_array($username, $registered_userNames)){
                continue;
            }

            $registered_userNames[] = $username;
            $registered_user = new User();
            $registered_user
                ->setUsername($username)
                ->setSurname($fullNameArray[0])
                ->setName($fullNameArray[1])
                ->setPatronymic($fullNameArray[2])
                ->setOffice($offices[rand(0, 2)])
                ->setEnabled(true)
                ->setEmail('worker' . $i . '@gmail.com')
                ->setPhoneNumber('0777112233')
                ->setStatus(true)
                ->setIsNotified(true);

            $history = new History();
            $history->setUser($registered_user);
            $history->setOffice($registered_user->getOffice());
            $history->setStartDate(new \DateTime());
            $password = $encoder->encodePassword($registered_user, 'worker');
            $registered_user->setPassword($password);
            $manager->persist($history);
            $manager->persist($registered_user);
        }

        $leagues = ['A' => 4000, 'B' => 2400, 'C' => 1600, 'D' => 800];
        foreach ($leagues as $key => $league){
            $leag = new League();
            $leag->setLeague($key)
                ->setBalance($league);

            $manager->persist($leag);
        }
        $rains  = ['RAIN', 'RAINGAME', 'RAINDEEP'];
        $count = count($rains);
        for ($i = 0; $i < $count; $i++) {
            $rain = new User();
            $rain->setUsername($rains[$i])
                ->setSurname($rains[$i])
                ->setName($rains[$i])
                ->setPatronymic($rains[$i])
                ->setOffice($offices[rand(0, 2)])
                ->setEnabled(true)
                ->setEmail($rains[$i] .'@gmail.com')
                ->setPhoneNumber('111111')
                ->setStatus(true)
                ->setPoints(1)
                ->setIsNotified(true);

            $history = new History();
            $history->setUser($rain);
            $history->setOffice($rain->getOffice());
            $history->setStartDate(new \DateTime());
            $password = $encoder->encodePassword($rain, 'rain');
            $rain->setPassword($password);

            $manager->persist($rain);
        }
        $manager->flush();
    }

    public function getData($sheet, $column){
        $row = 2;
        $data = [];
        while ($sheet->getCell($column . $row)->getValue() != null){
            $data[] = $sheet->getCell($column . $row)->getValue();
            $row++;
        }
        return $data;
    }
}
