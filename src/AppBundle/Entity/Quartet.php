<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Quartet
 *
 * @ORM\Table(name="quartet")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuartetRepository")
 */
class Quartet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="quartets")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function __toString()
    {
        return "Quartet" ?: '';
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="quartets")
     */
    protected $game;

    public function setGame(Game $game)
    {
        $this->game = $game;
        return $this;
    }

    public function getGame()
    {
        return $this->game;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Quartet
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }
}
