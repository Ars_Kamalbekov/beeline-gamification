<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BattleResult
 *
 * @ORM\Table(name="battle_result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BattleResultRepository")
 */
class BattleResult
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\User",
     *      inversedBy="battleResults"
     * )
     */
    private $user;

    /**
     * @var User
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\User",
     *     inversedBy="opponentForUser"
     * )
     */
    private $opponent;

    /**
     * @var Battle
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\Battle",
     *      inversedBy="battleResults"
     * )
     */
    private $battle;

    /**
     * @var string
     * @ORM\Column(name="user_points", type="integer")
     */
    private $user_points = 0;

    /**
     * @var string
     * @ORM\Column(name="opponent_points", type="integer")
     */
    private $opponent_points = 0;

    /**
     * @var int
     * @ORM\Column(name="user_balance", type="integer", nullable=true)
     */
    private $user_balance = 0;

    /**
     * @var int
     * @ORM\Column(name="user_restorations", type="integer")
     */
    private $user_restorations = 0;

    /**
     * @var int
     * @ORM\Column(name="$opponent_restorations", type="integer")
     */
    private $opponent_restorations = 0;

    /**
     * @var int
     * @ORM\Column(name="opponent_balance", type="integer", nullable=true)
     */
    private $opponent_balance = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set battle
     *
     * @param Battle $battle
     *
     * @return BattleResult
     */
    public function setBattle($battle)
    {
        $this->battle = $battle;

        return $this;
    }

    /**
     * Get battle
     *
     * @return Battle
     */
    public function getBattle()
    {
        return $this->battle;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return BattleResult
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set opponent
     *
     * @param User $opponent
     *
     * @return BattleResult
     */
    public function setOpponent($opponent)
    {
        $this->opponent = $opponent;

        return $this;
    }

    /**
     * Get opponent
     *
     * @return User
     */
    public function getOpponent()
    {
        return $this->opponent;
    }


    /**
     * Set opponentPoints
     *
     * @param integer $opponentPoints
     *
     * @return BattleResult
     */
    public function setOpponentPoints($opponentPoints)
    {
        $this->opponent_points += $opponentPoints;

        return $this;
    }

    /**
     * Get opponentPoints
     *
     * @return integer
     */
    public function getOpponentPoints()
    {
        return $this->opponent_points;
    }

    /**
     * Set userBalance
     *
     * @param integer $userBalance
     *
     * @return BattleResult
     */
    public function setUserBalance($userBalance)
    {
        $this->user_balance += $userBalance;

        return $this;
    }

    /**
     * Get userBalance
     *
     * @return integer
     */
    public function getUserBalance()
    {
        return $this->user_balance;
    }

    /**
     * Set opponentBalance
     *
     * @param integer $opponentBalance
     *
     * @return BattleResult
     */
    public function setOpponentBalance($opponentBalance)
    {
        $this->opponent_balance += $opponentBalance;

        return $this;
    }

    /**
     * Get opponentBalance
     *
     * @return integer
     */
    public function getOpponentBalance()
    {
        return $this->opponent_balance;
    }

    /**
     * Set userPoints
     *
     * @param integer $userPoints
     *
     * @return BattleResult
     */
    public function setUserPoints($userPoints)
    {
        $this->user_points += $userPoints;

        return $this;
    }

    /**
     * Get userPoints
     *
     * @return integer
     */
    public function getUserPoints()
    {
        return $this->user_points;
    }

    /**
     * Set userRestorations
     *
     * @param integer $userRestorations
     *
     * @return BattleResult
     */
    public function setUserRestorations($userRestorations)
    {
        $this->user_restorations += $userRestorations;

        return $this;
    }

    /**
     * Get userRestorations
     *
     * @return integer
     */
    public function getUserRestorations()
    {
        return $this->user_restorations;
    }

    /**
     * Set opponentRestorations
     *
     * @param integer $opponentRestorations
     *
     * @return BattleResult
     */
    public function setOpponentRestorations($opponentRestorations)
    {
        $this->opponent_restorations += $opponentRestorations;

        return $this;
    }

    /**
     * Get opponentRestorations
     *
     * @return integer
     */
    public function getOpponentRestorations()
    {
        return $this->opponent_restorations;
    }
}
