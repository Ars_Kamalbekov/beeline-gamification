<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Battle
 *
 * @ORM\Table(name="battle")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BattleRepository")
 */
class Battle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer", length=12)
     */
    private $number;

    /**
     * @var int
     * @ORM\Column(name="battle_results_count", type="integer", nullable=false)
     */
    private $battleResultsCount;

    /**
     * @var bool
     * @ORM\Column(name="is_restorations_downloaded", type="boolean")
     */
    private $isRestorationsDownloaded = false;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="battles")
     */
    private $game;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BattleResult", mappedBy="battle")
     */
    private $battleResults;

    public function __construct()
    {
        $this->battleResults = new ArrayCollection();
    }
    public function setGame(Game $game)
    {
        $this->game = $game;
        return $this;
    }

    public function getGame()
    {
        return $this->game;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Battle
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add battleResult
     *
     * @param \AppBundle\Entity\BattleResult $battleResult
     *
     * @return Battle
     */
    public function addBattleResult(\AppBundle\Entity\BattleResult $battleResult)
    {
        $this->battleResults[] = $battleResult;

        return $this;
    }

    /**
     * Remove battleResult
     *
     * @param \AppBundle\Entity\BattleResult $battleResult
     */
    public function removeBattleResult(\AppBundle\Entity\BattleResult $battleResult)
    {
        $this->battleResults->removeElement($battleResult);
    }

    /**
     * Get battleResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBattleResults()
    {
        return $this->battleResults;
    }

    /**
     * Set battleResultsCount
     *
     * @param integer $battleResultsCount
     *
     * @return Battle
     */
    public function setBattleResultsCount($battleResultsCount = 0)
    {
        $this->battleResultsCount = $battleResultsCount;

        return $this;
    }

    /**
     * Get battleResultsCount
     *
     * @return integer
     */
    public function getBattleResultsCount()
    {
        return $this->battleResultsCount;
    }

    /**
     * Set isRestorationsDownloaded
     *
     * @param boolean $isRestorationsDownloaded
     *
     * @return Battle
     */
    public function setIsRestorationsDownloaded($isRestorationsDownloaded)
    {
        $this->isRestorationsDownloaded = $isRestorationsDownloaded;

        return $this;
    }

    /**
     * Get isRestorationsDownloaded
     *
     * @return boolean
     */
    public function getIsRestorationsDownloaded()
    {
        return $this->isRestorationsDownloaded;
    }
}
