<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="number", type="integer", nullable=false, unique=true)
     */
    private $number;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $start_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $end_date;

    /**
     * @var bool
     * @ORM\Column(name="previous_game", type="boolean")
     */
    private $previous_game = false;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Quartet", mappedBy="game")
     */
    private $quartets;

    public function getQuartets()
    {
        return $this->quartets;
    }

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Battle", mappedBy="game")
     */
    private $battles;

    public function __construct()
    {
        $this->battles = new ArrayCollection();
        $this->quartets = new ArrayCollection();
    }

    public function getBattles()
    {
        return $this->battles;
    }


    public function __toString()
    {
        return "Game" ?: '';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Game
     */
    public function setStartDate($startDate)
    {
        $this->start_date = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Game
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Add quartet
     *
     * @param \AppBundle\Entity\Quartet $quartet
     *
     * @return Game
     */
    public function addQuartet(\AppBundle\Entity\Quartet $quartet)
    {
        $this->quartets[] = $quartet;

        return $this;
    }

    /**
     * Remove quartet
     *
     * @param \AppBundle\Entity\Quartet $quartet
     */
    public function removeQuartet(\AppBundle\Entity\Quartet $quartet)
    {
        $this->quartets->removeElement($quartet);
    }

    /**
     * Add battle
     *
     * @param \AppBundle\Entity\Battle $battle
     *
     * @return Game
     */
    public function addBattle(\AppBundle\Entity\Battle $battle)
    {
        $this->battles[] = $battle;

        return $this;
    }

    /**
     * Remove battle
     *
     * @param \AppBundle\Entity\Battle $battle
     */
    public function removeBattle(\AppBundle\Entity\Battle $battle)
    {
        $this->battles->removeElement($battle);
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Game
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set previousGame
     *
     * @param boolean $previousGame
     *
     * @return Game
     */
    public function setPreviousGame($previousGame)
    {
        $this->previous_game = $previousGame;

        return $this;
    }

    /**
     * Get previousGame
     *
     * @return boolean
     */
    public function getPreviousGame()
    {
        return $this->previous_game;
    }
}
