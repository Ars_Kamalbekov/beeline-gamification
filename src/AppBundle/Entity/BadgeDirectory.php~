<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * BadgeDirectory
 *
 * @ORM\Table(name="badge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BadgeDirectoryRepository")
 * @Vich\Uploadable
 */
class BadgeDirectory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", unique=true)
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Название не может быть числовым значением."
     * )
     * @Assert\NotBlank(message="поле Название не может быть пустым")
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="Название слишком короткое.",
     *     maxMessage="Название слишком длинное.",
     * )
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     * @Assert\Type(
     *     type="integer",
     *     message="Балл должен быть числовым значением"
     * )
     * @Assert\GreaterThan(0)
     * @Assert\NotBlank(message="поле Балл не может быть пустым")
     */
    private $points;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", nullable=true)
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="badge_file", fileNameProperty="image")
     *
     * @Assert\File(
     *     mimeTypes = { "image/gif", "image/jpeg", "image/png", "image/jpg" },
     *     mimeTypesMessage = "Недопустимый тип данных ({{ type }}). Допустимы: {{ types }}."
     * )
     * @var File
     */
    protected $badgeFile;


    /**
     * @return File|null
     */
    public function getBadgeFile()
    {
        return $this->badgeFile;
    }

    /**
     *
     * @param File|null $badgeFile
     * @return BadgeDirectory
     */
    public function setBadgeFile(File $badgeFile = null)
    {
        $this->badgeFile = $badgeFile;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BadgeDirectory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return BadgeDirectory
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    public function __toString()
    {
        return $this->name ?: '';
    }


    /**
     * Set image
     *
     * @param string $image
     *
     * @return BadgeDirectory
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

}
