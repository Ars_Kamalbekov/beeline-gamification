<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Partner
 *
 * @ORM\Table(name="partner")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PartnerRepository")
 */
class Partner
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Office", mappedBy="partner")
     */
    private $office;

    public function __construct()
    {
        $this->office = new ArrayCollection();
    }

    public function getOffice()
    {
        return $this->office;
    }


    public function __toString()
    {
        return $this->name ?: '';
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add office
     *
     * @param \AppBundle\Entity\Office $office
     *
     * @return Partner
     */
    public function addOffice(\AppBundle\Entity\Office $office)
    {
        $this->office[] = $office;

        return $this;
    }

    /**
     * Remove office
     *
     * @param \AppBundle\Entity\Office $office
     */
    public function removeOffice(\AppBundle\Entity\Office $office)
    {
        $this->office->removeElement($office);
    }
}
