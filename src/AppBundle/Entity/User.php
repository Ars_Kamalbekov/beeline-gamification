<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 *
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The name is too short.",
     *     maxMessage="The name is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    protected $status = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Please enter your surname.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The surname is too short.",
     *     maxMessage="The surname is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Please enter your patronymic.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="The patronymic is too short.",
     *     maxMessage="The patronymic is too long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $patronymic;

    /**
     * @var bool
     * @ORM\Column(name="is_notified", type="boolean", nullable=true)
     */
    protected $isNotified;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone_number;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    protected $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    protected $avatar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime", nullable=true)
     */
    protected $registerDate;

    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BattleResult", mappedBy="user")
     */
    protected $battleResults;
    /**
     * @var int
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BattleResult", mappedBy="opponent")
     */
    protected $opponentForUser;

    /**
     * @var League
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\League", inversedBy="users")
     */
    protected $league;
    /**
     * @var int
     * @ORM\Column(name="balance", type="integer", nullable=true)
     */
    protected $balance;

    /**
     * @var int
     * * @ORM\Column(name="points", type="integer", nullable=true)
     */
    protected $points;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Badge", mappedBy="user")
     */
    protected $badges;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\History", mappedBy="user")
     */
    protected $histories;

    /**
     * @ORM\PrePersist()
     *
     */
    public function prePersist()
    {
        $this->registerDate = new \DateTime;
    }

    public function __construct() {
        parent::__construct();
        $this->battleResults = new ArrayCollection();
        $this->opponentForUser = new ArrayCollection();
        $this->histories = new ArrayCollection();
        $this->quartets = new ArrayCollection();
        $this->badges = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->email ?: '';
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Office", inversedBy="users")
     */
    protected $office;

    public function setOffice(Office $office)
    {
        $this->office = $office;
        return $this;
    }

    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Quartet", inversedBy="users")
     */
    protected $quartets;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param mixed $phone_number
     * @return $this
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param mixed $patronymic
     * @return $this
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNotified()
    {
        return $this->isNotified;
    }

    /**
     * @param bool $isNotified
     */
    public function setIsNotified(bool $isNotified)
    {
        $this->isNotified = $isNotified;
    }

    /**
     * get avatar
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar(string $avatar = null)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     *
     * @param File|null $imageFile
     * @return User
     */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add battleResult
     *
     * @param \AppBundle\Entity\BattleResult $battleResult
     *
     * @return User
     */
    public function addBattleResult(\AppBundle\Entity\BattleResult $battleResult)
    {
        $this->battleResults[] = $battleResult;

        return $this;
    }

    /**
     * Remove battleResult
     *
     * @param \AppBundle\Entity\BattleResult $battleResult
     */
    public function removeBattleResult(\AppBundle\Entity\BattleResult $battleResult)
    {
        $this->battleResults->removeElement($battleResult);
    }

    /**
     * Get battleResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBattleResults()
    {
        return $this->battleResults;
    }

    /**
     * Add opponentForUser
     *
     * @param \AppBundle\Entity\BattleResult $opponentForUser
     *
     * @return User
     */
    public function addOpponentForUser(\AppBundle\Entity\BattleResult $opponentForUser)
    {
        $this->opponentForUser[] = $opponentForUser;

        return $this;
    }

    /**
     * Remove opponentForUser
     *
     * @param \AppBundle\Entity\BattleResult $opponentForUser
     */
    public function removeOpponentForUser(\AppBundle\Entity\BattleResult $opponentForUser)
    {
        $this->opponentForUser->removeElement($opponentForUser);
    }

    /**
     * Get opponentForUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpponentForUser()
    {
        return $this->opponentForUser;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     */
    public function setBalance(int $balance)
    {
        $this->balance = $balance;
    }

    /**
     * Set league
     *
     * @param \AppBundle\Entity\League $league
     *
     * @return User
     */
    public function setLeague(\AppBundle\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \AppBundle\Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return User
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Add history
     *
     * @param \AppBundle\Entity\History $history
     *
     * @return User
     */
    public function addHistory(\AppBundle\Entity\History $history)
    {
        $this->histories[] = $history;

        return $this;
    }

    /**
     * Remove history
     *
     * @param \AppBundle\Entity\History $history
     */
    public function removeHistory(\AppBundle\Entity\History $history)
    {
        $this->histories->removeElement($history);
    }

    /**
     * Get histories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistories()
    {
        return $this->histories;
    }

    /**
     * Add quartet
     *
     * @param \AppBundle\Entity\Quartet $quartet
     *
     * @return User
     */
    public function addQuartet(\AppBundle\Entity\Quartet $quartet)
    {
        $this->quartets[] = $quartet;

        return $this;
    }

    /**
     * Remove quartet
     *
     * @param \AppBundle\Entity\Quartet $quartet
     */
    public function removeQuartet(\AppBundle\Entity\Quartet $quartet)
    {
        $this->quartets->removeElement($quartet);
    }

    /**
     * Get quartets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuartets()
    {
        return $this->quartets;
    }

    /**
     * Add badge
     *
     * @param \AppBundle\Entity\Badge $badge
     *
     * @return User
     */
    public function addBadge(\AppBundle\Entity\Badge $badge)
    {
        $this->badges[] = $badge;

        return $this;
    }

    /**
     * Remove badge
     *
     * @param \AppBundle\Entity\Badge $badge
     */
    public function removeBadge(\AppBundle\Entity\Badge $badge)
    {
        $this->badges->removeElement($badge);
    }

    /**
     * Get badges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBadges()
    {
        return $this->badges;
    }
}
