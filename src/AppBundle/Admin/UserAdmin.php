<?php

namespace AppBundle\Admin;

use AppBundle\Entity\History;
use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;


class UserAdmin extends AbstractAdmin
{


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('enabled')
            ->add('roles')
            ->add('office')
            ->add('username');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('surname')
            ->add('patronymic')
            ->add('enabled')
            ->add('roles')
            ->add('office')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->isCurrentRoute('create')) {
            $formMapper
                ->add('username', TextType::class,[
                    'label' => 'Логин'
                ])
                ->add('email', EmailType::class,[
                    'label' => 'email'
                ])
                ->add('enabled', CheckboxType::class,[
                    'label' => 'Активация'
                ])
                ->add('plainPassword', 'repeated', array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'Пароль'),
                    'second_options' => array('label' => 'Повторите пароль'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
                ->add('office', EntityType::class, [
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
                    'placeholder' => 'Выберите офис',
                    'label' => 'Офис'
                ]);
        }

        if ($this->isCurrentRoute('edit')) {
            $formMapper
                ->add('enabled', CheckboxType::class)
                ->add('office', EntityType::class, [
                    'class' => 'AppBundle\Entity\Office',
                    'choice_label' => 'name',
                    'placeholder' => 'Выберите офис',
                    'label' => 'Офис'
                ]);
        }
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('name')
            ->add('surname')
            ->add('patronymic')
            ->add('enabled')
            ->add('roles')
            ->add('office')
            ->add('points')
            ->add('histories', EntityType::class, [
                'class' => 'AppBundle\Entity\History',
            ])
            ->add('badges', EntityType::class, [
                'class' => 'AppBundle\Entity\Badge',
            ]);
    }

    public function postUpdate($user)
    {
        /** @var User $user */

        if ((!$user->isNotified() || $user->isNotified() == null) && $user->isEnabled()) {
            $router = $this->getConfigurationPool()->getContainer()->get('router');
            $url = $router->generate('app_messagesender_sendsuccessregister', [
                'user_id' => $user->getId()
            ]);
            $redirection = new RedirectResponse($url);
            $redirection->send();
        }
    }

    public function postPersist($user)
    {
        /** @var User $user */

        $history = new History();
        $history->setUser($user);
        $history->setOffice($user->getOffice());
        $history->setStartDate($user->getRegisterDate());
        $em = $this->getConfigurationPool()
            ->getContainer()
            ->get('doctrine')
            ->getManager();
        $em->persist($history);
        $em->flush();
    }

    public function preRemove($user)
    {
        /** @var User $user */
        $histories = $user->getHistories();
        $battle_results = $user->getBattleResults();
        $badges = $user->getBadges();
        $em = $this->getConfigurationPool()
            ->getContainer()
            ->get("doctrine")
            ->getManager();

        foreach ($histories as $history){
            $em->remove($history);
        }
        foreach ($battle_results as $battle_result){
            $em->remove($battle_result);
        }
        foreach ($badges as $badge){
            $em->remove($badge);
        }
        $em->flush();

    }
}
