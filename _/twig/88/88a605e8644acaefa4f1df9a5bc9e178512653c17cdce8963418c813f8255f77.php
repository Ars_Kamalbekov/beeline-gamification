<?php

/* SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig */
class TwigTemplatea66a892e5ee95f83794e16a499594646c5f909aa6d4ceedb77d6f126d8d62dad extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal21fc96f844d34d727b5542c51e1b3e4512fa021b554d1b39032f44c370a5e5f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal21fc96f844d34d727b5542c51e1b3e4512fa021b554d1b39032f44c370a5e5f6->enter($internal21fc96f844d34d727b5542c51e1b3e4512fa021b554d1b39032f44c370a5e5f6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig"));

        $internal5662ec0fa7a033d5f477236f46a5b581428c6a3af1982ed0ae5ef28ee1d7ec5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5662ec0fa7a033d5f477236f46a5b581428c6a3af1982ed0ae5ef28ee1d7ec5c->enter($internal5662ec0fa7a033d5f477236f46a5b581428c6a3af1982ed0ae5ef28ee1d7ec5cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig"));

        // line 11
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 11, $this->getSourceContext()); })()), "fielddescription", array()), "hasassociationadmin", array())) {
            // line 12
            echo "    <div id=\"fieldcontainer";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
            echo "\" class=\"field-container\">
        <span id=\"fieldwidget";
            // line 13
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
            echo "\" >
            ";
            // line 14
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 14, $this->getSourceContext()); })()), "edit", array()) == "inline")) {
                // line 15
                echo "                ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 15, $this->getSourceContext()); })()), "inline", array()) == "table")) {
                    // line 16
                    echo "                    ";
                    if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), "children", array())) > 0)) {
                        // line 17
                        echo "                        <table class=\"table table-bordered\">
                            <thead>
                                <tr>
                                    ";
                        // line 20
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 20, $this->getSourceContext()); })()), "children", array()), 0, array(), "array"), "children", array()));
                        foreach ($context['seq'] as $context["fieldname"] => $context["nestedfield"]) {
                            // line 21
                            echo "                                        ";
                            if (($context["fieldname"] == "delete")) {
                                // line 22
                                echo "                                            <th>";
                                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actiondelete", array(), "SonataAdminBundle"), "html", null, true);
                                echo "</th>
                                        ";
                            } else {
                                // line 24
                                echo "                                            <th ";
                                echo ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "required", array(), "array")) ? ("class=\"required\"") : (""));
                                echo ">
                                                ";
                                // line 25
                                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "sonataadmin", array(), "array"), "admin", array()), "trans", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "label", array())), "method"), "html", null, true);
                                echo "
                                            </th>
                                        ";
                            }
                            // line 28
                            echo "                                    ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['fieldname'], $context['nestedfield'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 29
                        echo "                                </tr>
                            </thead>
                            <tbody class=\"sonata-ba-tbody\">
                                ";
                        // line 32
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 32, $this->getSourceContext()); })()), "children", array()));
                        foreach ($context['seq'] as $context["nestedgroupfieldname"] => $context["nestedgroupfield"]) {
                            // line 33
                            echo "                                    <tr>
                                        ";
                            // line 34
                            $context['parent'] = $context;
                            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "children", array()));
                            foreach ($context['seq'] as $context["fieldname"] => $context["nestedfield"]) {
                                // line 35
                                echo "                                            <td class=\"sonata-ba-td-";
                                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
                                echo "-";
                                echo twigescapefilter($this->env, $context["fieldname"], "html", null, true);
                                echo " control-group";
                                if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "errors", array())) > 0)) {
                                    echo " error";
                                }
                                echo "\">
                                                ";
                                // line 36
                                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "associationadmin", array(), "any", false, true), "hasformfielddescriptions", array(0 => $context["fieldname"]), "method", true, true)) {
                                    // line 37
                                    echo "                                                    ";
                                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'widget');
                                    echo "

                                                    ";
                                    // line 39
                                    $context["dummy"] = twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "setrendered", array());
                                    // line 40
                                    echo "                                                ";
                                } else {
                                    // line 41
                                    echo "                                                    ";
                                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'widget');
                                    echo "
                                                ";
                                }
                                // line 43
                                echo "                                                ";
                                if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "errors", array())) > 0)) {
                                    // line 44
                                    echo "                                                    <div class=\"help-inline sonata-ba-field-error-messages\">
                                                        ";
                                    // line 45
                                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'errors');
                                    echo "
                                                    </div>
                                                ";
                                }
                                // line 48
                                echo "                                            </td>
                                        ";
                            }
                            $parent = $context['parent'];
                            unset($context['seq'], $context['iterated'], $context['fieldname'], $context['nestedfield'], $context['parent'], $context['loop']);
                            $context = arrayintersectkey($context, $parent) + $parent;
                            // line 50
                            echo "                                    </tr>
                                ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['nestedgroupfieldname'], $context['nestedgroupfield'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 52
                        echo "                            </tbody>
                        </table>
                    ";
                    }
                    // line 55
                    echo "                ";
                } elseif ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 55, $this->getSourceContext()); })()), "children", array())) > 0)) {
                    // line 56
                    echo "                    <div>
                        ";
                    // line 57
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 57, $this->getSourceContext()); })()), "children", array()));
                    foreach ($context['seq'] as $context["nestedgroupfieldname"] => $context["nestedgroupfield"]) {
                        // line 58
                        echo "                            ";
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "children", array()));
                        foreach ($context['seq'] as $context["fieldname"] => $context["nestedfield"]) {
                            // line 59
                            echo "                                ";
                            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "associationadmin", array(), "any", false, true), "hasformfielddescriptions", array(0 => $context["fieldname"]), "method", true, true)) {
                                // line 60
                                echo "                                    ";
                                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'row', array("inline" => "natural", "edit" => "inline"));
                                // line 63
                                echo "
                                    ";
                                // line 64
                                $context["dummy"] = twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "setrendered", array());
                                // line 65
                                echo "                                ";
                            } else {
                                // line 66
                                echo "                                    ";
                                if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "name", array()) == "delete")) {
                                    // line 67
                                    echo "                                        ";
                                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'row', array("label" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actiondelete", array(), "SonataAdminBundle")));
                                    echo "
                                    ";
                                } else {
                                    // line 69
                                    echo "                                        ";
                                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'row');
                                    echo "
                                    ";
                                }
                                // line 71
                                echo "                                ";
                            }
                            // line 72
                            echo "                            ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['fieldname'], $context['nestedfield'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 73
                        echo "                        ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['nestedgroupfieldname'], $context['nestedgroupfield'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 74
                    echo "                    </div>
                ";
                }
                // line 76
                echo "            ";
            } else {
                // line 77
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 77, $this->getSourceContext()); })()), 'widget');
                echo "
            ";
            }
            // line 79
            echo "
        </span>

        ";
            // line 82
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 82, $this->getSourceContext()); })()), "edit", array()) == "inline")) {
                // line 83
                echo "
            ";
                // line 84
                if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 84, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasroute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 84, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "CREATE"), "method")) && (isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 84, $this->getSourceContext()); })()))) {
                    // line 85
                    echo "                <span id=\"fieldactions";
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 85, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" >
                    <a
                        href=\"";
                    // line 87
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 87, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 87, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    echo "\"
                        onclick=\"return startfieldretrieve";
                    // line 88
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 88, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"";
                    // line 90
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 90, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 90, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                        >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
                    // line 93
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 93, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 93, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                    </a>
                </span>
            ";
                }
                // line 97
                echo "
            ";
                // line 99
                echo "            ";
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig", 99)->display($context);
                // line 100
                echo "
        ";
            } else {
                // line 102
                echo "            <div id=\"fieldcontainer";
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 102, $this->getSourceContext()); })()), "html", null, true);
                echo "\" class=\"field-container\">
                <span id=\"fieldwidget";
                // line 103
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 103, $this->getSourceContext()); })()), "html", null, true);
                echo "\" >
                    ";
                // line 104
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 104, $this->getSourceContext()); })()), 'widget');
                echo "
                </span>

                <span id=\"fieldactions";
                // line 107
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 107, $this->getSourceContext()); })()), "html", null, true);
                echo "\" class=\"field-actions\">
                    ";
                // line 108
                if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 108, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasRoute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 108, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "CREATE"), "method")) && (isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 108, $this->getSourceContext()); })()))) {
                    // line 109
                    echo "                        <a
                            href=\"";
                    // line 110
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 110, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 110, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    echo "\"
                            onclick=\"return startfielddialogformadd";
                    // line 111
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 111, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                            class=\"btn btn-success btn-sm sonata-ba-action\"
                            title=\"";
                    // line 113
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 113, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 113, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                            >
                            <i class=\"fa fa-plus-circle\"></i>
                            ";
                    // line 116
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 116, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 116, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                        </a>
                    ";
                }
                // line 119
                echo "                </span>

                ";
                // line 121
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig", 121)->display($context);
                // line 122
                echo "            </div>

            ";
                // line 124
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig", 124)->display($context);
                // line 125
                echo "        ";
            }
            // line 126
            echo "    </div>
";
        }
        
        $internal21fc96f844d34d727b5542c51e1b3e4512fa021b554d1b39032f44c370a5e5f6->leave($internal21fc96f844d34d727b5542c51e1b3e4512fa021b554d1b39032f44c370a5e5f6prof);

        
        $internal5662ec0fa7a033d5f477236f46a5b581428c6a3af1982ed0ae5ef28ee1d7ec5c->leave($internal5662ec0fa7a033d5f477236f46a5b581428c6a3af1982ed0ae5ef28ee1d7ec5cprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  338 => 126,  335 => 125,  333 => 124,  329 => 122,  327 => 121,  323 => 119,  317 => 116,  311 => 113,  306 => 111,  302 => 110,  299 => 109,  297 => 108,  293 => 107,  287 => 104,  283 => 103,  278 => 102,  274 => 100,  271 => 99,  268 => 97,  261 => 93,  255 => 90,  250 => 88,  246 => 87,  240 => 85,  238 => 84,  235 => 83,  233 => 82,  228 => 79,  222 => 77,  219 => 76,  215 => 74,  209 => 73,  203 => 72,  200 => 71,  194 => 69,  188 => 67,  185 => 66,  182 => 65,  180 => 64,  177 => 63,  174 => 60,  171 => 59,  166 => 58,  162 => 57,  159 => 56,  156 => 55,  151 => 52,  144 => 50,  137 => 48,  131 => 45,  128 => 44,  125 => 43,  119 => 41,  116 => 40,  114 => 39,  108 => 37,  106 => 36,  95 => 35,  91 => 34,  88 => 33,  84 => 32,  79 => 29,  73 => 28,  67 => 25,  62 => 24,  56 => 22,  53 => 21,  49 => 20,  44 => 17,  41 => 16,  38 => 15,  36 => 14,  32 => 13,  27 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% if sonataadmin.fielddescription.hasassociationadmin %}
    <div id=\"fieldcontainer{{ id }}\" class=\"field-container\">
        <span id=\"fieldwidget{{ id }}\" >
            {% if sonataadmin.edit == 'inline' %}
                {% if sonataadmin.inline == 'table' %}
                    {% if form.children|length > 0 %}
                        <table class=\"table table-bordered\">
                            <thead>
                                <tr>
                                    {% for fieldname, nestedfield in form.children[0].children %}
                                        {% if fieldname == 'delete' %}
                                            <th>{{ 'actiondelete'|trans({}, 'SonataAdminBundle') }}</th>
                                        {% else %}
                                            <th {{ nestedfield.vars['required']  ? 'class=\"required\"' : '' }}>
                                                {{ nestedfield.vars['sonataadmin'].admin.trans(nestedfield.vars.label) }}
                                            </th>
                                        {% endif %}
                                    {% endfor %}
                                </tr>
                            </thead>
                            <tbody class=\"sonata-ba-tbody\">
                                {% for nestedgroupfieldname, nestedgroupfield in form.children %}
                                    <tr>
                                        {% for fieldname, nestedfield in nestedgroupfield.children %}
                                            <td class=\"sonata-ba-td-{{ id }}-{{ fieldname  }} control-group{% if nestedfield.vars.errors|length > 0 %} error{% endif %}\">
                                                {% if sonataadmin.fielddescription.associationadmin.hasformfielddescriptions(fieldname) is defined %}
                                                    {{ formwidget(nestedfield) }}

                                                    {% set dummy = nestedgroupfield.setrendered %}
                                                {% else %}
                                                    {{ formwidget(nestedfield) }}
                                                {% endif %}
                                                {% if nestedfield.vars.errors|length > 0 %}
                                                    <div class=\"help-inline sonata-ba-field-error-messages\">
                                                        {{ formerrors(nestedfield) }}
                                                    </div>
                                                {% endif %}
                                            </td>
                                        {% endfor %}
                                    </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    {% endif %}
                {% elseif form.children|length > 0 %}
                    <div>
                        {% for nestedgroupfieldname, nestedgroupfield in form.children %}
                            {% for fieldname, nestedfield in nestedgroupfield.children %}
                                {% if sonataadmin.fielddescription.associationadmin.hasformfielddescriptions(fieldname) is defined %}
                                    {{ formrow(nestedfield, {
                                        'inline': 'natural',
                                        'edit'  : 'inline'
                                    }) }}
                                    {% set dummy = nestedgroupfield.setrendered %}
                                {% else %}
                                    {% if nestedfield.vars.name == 'delete' %}
                                        {{ formrow(nestedfield, { 'label': ('actiondelete'|trans({}, 'SonataAdminBundle')) }) }}
                                    {% else %}
                                        {{ formrow(nestedfield) }}
                                    {% endif %}
                                {% endif %}
                            {% endfor %}
                        {% endfor %}
                    </div>
                {% endif %}
            {% else %}
                {{ formwidget(form) }}
            {% endif %}

        </span>

        {% if sonataadmin.edit == 'inline' %}

            {% if sonataadmin.fielddescription.associationadmin.hasroute('create') and sonataadmin.fielddescription.associationadmin.isGranted('CREATE') and btnadd %}
                <span id=\"fieldactions{{ id }}\" >
                    <a
                        href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('create', sonataadmin.fielddescription.getOption('linkparameters', {})) }}\"
                        onclick=\"return startfieldretrieve{{ id }}(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                        >
                        <i class=\"fa fa-plus-circle\"></i>
                        {{ btnadd|trans({}, btncatalogue) }}
                    </a>
                </span>
            {% endif %}

            {# include association code #}
            {% include 'SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig' %}

        {% else %}
            <div id=\"fieldcontainer{{ id }}\" class=\"field-container\">
                <span id=\"fieldwidget{{ id }}\" >
                    {{ formwidget(form) }}
                </span>

                <span id=\"fieldactions{{ id }}\" class=\"field-actions\">
                    {% if sonataadmin.fielddescription.associationadmin.hasRoute('create') and sonataadmin.fielddescription.associationadmin.isGranted('CREATE') and btnadd %}
                        <a
                            href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('create', sonataadmin.fielddescription.getOption('linkparameters', {})) }}\"
                            onclick=\"return startfielddialogformadd{{ id }}(this);\"
                            class=\"btn btn-success btn-sm sonata-ba-action\"
                            title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                            >
                            <i class=\"fa fa-plus-circle\"></i>
                            {{ btnadd|trans({}, btncatalogue) }}
                        </a>
                    {% endif %}
                </span>

                {% include 'SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig' %}
            </div>

            {% include 'SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig' %}
        {% endif %}
    </div>
{% endif %}
", "SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editormmanytomany.html.twig");
    }
}
