<?php

/* FOSUserBundle:ChangePassword:changepasswordcontent.html.twig */
class TwigTemplate8d052649d356be461d2fe4e26a039122e32c69a8c6dc0c2fe1c3c39153e6d894 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc01766168863a0e5a00fa8d787b430f51cd40193f440a45484b59a42cfc6d51f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc01766168863a0e5a00fa8d787b430f51cd40193f440a45484b59a42cfc6d51f->enter($internalc01766168863a0e5a00fa8d787b430f51cd40193f440a45484b59a42cfc6d51fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changepasswordcontent.html.twig"));

        $internal483bd808e38580565c37a6d0af46097c267138133ac95e6cfd46988484b1613d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal483bd808e38580565c37a6d0af46097c267138133ac95e6cfd46988484b1613d->enter($internal483bd808e38580565c37a6d0af46097c267138133ac95e6cfd46988484b1613dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changepasswordcontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserchangepassword"), "attr" => array("class" => "fosuserchangepassword")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("changepassword.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internalc01766168863a0e5a00fa8d787b430f51cd40193f440a45484b59a42cfc6d51f->leave($internalc01766168863a0e5a00fa8d787b430f51cd40193f440a45484b59a42cfc6d51fprof);

        
        $internal483bd808e38580565c37a6d0af46097c267138133ac95e6cfd46988484b1613d->leave($internal483bd808e38580565c37a6d0af46097c267138133ac95e6cfd46988484b1613dprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changepasswordcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, { 'action': path('fosuserchangepassword'), 'attr': { 'class': 'fosuserchangepassword' } }) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'changepassword.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:ChangePassword:changepasswordcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/changepasswordcontent.html.twig");
    }
}
