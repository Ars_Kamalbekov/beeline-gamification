<?php

/* SonataAdminBundle:Block:blockrssdashboard.html.twig */
class TwigTemplate6cce2178658ee68bcb446d2c927eb40e434cca72b1be219961e083f59b2a2166 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataBlockBundle:Block:blockcorerss.html.twig", "SonataAdminBundle:Block:blockrssdashboard.html.twig", 12);
        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataBlockBundle:Block:blockcorerss.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal24f64a127f9e0f53f814583a9cf29569ca9b17679e69b993ed17c176fabc7169 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal24f64a127f9e0f53f814583a9cf29569ca9b17679e69b993ed17c176fabc7169->enter($internal24f64a127f9e0f53f814583a9cf29569ca9b17679e69b993ed17c176fabc7169prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockrssdashboard.html.twig"));

        $internal417f460142a254e2ef02f440bf30feed2dbf9eada12685458871db6bc5caac76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal417f460142a254e2ef02f440bf30feed2dbf9eada12685458871db6bc5caac76->enter($internal417f460142a254e2ef02f440bf30feed2dbf9eada12685458871db6bc5caac76prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockrssdashboard.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal24f64a127f9e0f53f814583a9cf29569ca9b17679e69b993ed17c176fabc7169->leave($internal24f64a127f9e0f53f814583a9cf29569ca9b17679e69b993ed17c176fabc7169prof);

        
        $internal417f460142a254e2ef02f440bf30feed2dbf9eada12685458871db6bc5caac76->leave($internal417f460142a254e2ef02f440bf30feed2dbf9eada12685458871db6bc5caac76prof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal8779d7231ad632fd9dbf92739f35bcc1bc0ab70f43bcbce600bb3cb2692a2e34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8779d7231ad632fd9dbf92739f35bcc1bc0ab70f43bcbce600bb3cb2692a2e34->enter($internal8779d7231ad632fd9dbf92739f35bcc1bc0ab70f43bcbce600bb3cb2692a2e34prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal623e47d533a251467543bc2b185a48d75e8151dc0a2af524d887d8dc6d364bbe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal623e47d533a251467543bc2b185a48d75e8151dc0a2af524d887d8dc6d364bbe->enter($internal623e47d533a251467543bc2b185a48d75e8151dc0a2af524d887d8dc6d364bbeprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"box box-warning\">
        <div class=\"box-header with-border\">
            <h3 class=\"box-title sonata-feed-title\"><i class=\"fa fa-rss\" aria-hidden=\"true\"></i> ";
        // line 17
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 17, $this->getSourceContext()); })()), "title", array()), "html", null, true);
        echo "</h3>
        </div>

        <div class=\"sonata-feeds-container list-group\">
            ";
        // line 21
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["feeds"]) || arraykeyexists("feeds", $context) ? $context["feeds"] : (function () { throw new TwigErrorRuntime('Variable "feeds" does not exist.', 21, $this->getSourceContext()); })()));
        $context['iterated'] = false;
        foreach ($context['seq'] as $context["key"] => $context["feed"]) {
            // line 22
            echo "                <a class=\"list-group-item\" href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "link", array()), "html", null, true);
            echo "\" rel=\"nofollow\" title=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "title", array()), "html", null, true);
            echo "\">
                    <strong>";
            // line 23
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "title", array()), "html", null, true);
            echo "</strong>
                    <div>";
            // line 24
            echo twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "description", array());
            echo "</div>
                </a>
            ";
            $context['iterated'] = true;
        }
        if (!$context['iterated']) {
            // line 27
            echo "                <div class=\"list-group-item\">No feeds available.</div>
            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['feed'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 29
        echo "        </div>
    </div>
";
        
        $internal623e47d533a251467543bc2b185a48d75e8151dc0a2af524d887d8dc6d364bbe->leave($internal623e47d533a251467543bc2b185a48d75e8151dc0a2af524d887d8dc6d364bbeprof);

        
        $internal8779d7231ad632fd9dbf92739f35bcc1bc0ab70f43bcbce600bb3cb2692a2e34->leave($internal8779d7231ad632fd9dbf92739f35bcc1bc0ab70f43bcbce600bb3cb2692a2e34prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Block:blockrssdashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  84 => 27,  76 => 24,  72 => 23,  65 => 22,  60 => 21,  53 => 17,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends \"SonataBlockBundle:Block:blockcorerss.html.twig\" %}

{% block block %}
    <div class=\"box box-warning\">
        <div class=\"box-header with-border\">
            <h3 class=\"box-title sonata-feed-title\"><i class=\"fa fa-rss\" aria-hidden=\"true\"></i> {{ settings.title }}</h3>
        </div>

        <div class=\"sonata-feeds-container list-group\">
            {% for feed in feeds %}
                <a class=\"list-group-item\" href=\"{{ feed.link}}\" rel=\"nofollow\" title=\"{{ feed.title }}\">
                    <strong>{{ feed.title }}</strong>
                    <div>{{ feed.description|raw }}</div>
                </a>
            {% else %}
                <div class=\"list-group-item\">No feeds available.</div>
            {% endfor %}
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:Block:blockrssdashboard.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Block/blockrssdashboard.html.twig");
    }
}
