<?php

/* SonataAdminBundle:CRUD:baseshowfield.html.twig */
class TwigTemplate1aa37f75f3e9c08929b2a63fffe33e613393b2534f4a3ad9567499dcd644dfc9 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'name' => array($this, 'blockname'),
            'field' => array($this, 'blockfield'),
            'fieldvalue' => array($this, 'blockfieldvalue'),
            'fieldcompare' => array($this, 'blockfieldcompare'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb5c9fea0f6b0ad801f4a4360f246d869f1d888698d10596ba575512b496352c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb5c9fea0f6b0ad801f4a4360f246d869f1d888698d10596ba575512b496352c2->enter($internalb5c9fea0f6b0ad801f4a4360f246d869f1d888698d10596ba575512b496352c2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowfield.html.twig"));

        $internal81df71edcf21aa2624084dca80348c6ede726d05f68d788fc929648582d63eac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81df71edcf21aa2624084dca80348c6ede726d05f68d788fc929648582d63eac->enter($internal81df71edcf21aa2624084dca80348c6ede726d05f68d788fc929648582d63eacprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowfield.html.twig"));

        // line 11
        echo "
<th";
        // line 12
        if (((arraykeyexists("isdiff", $context)) ? (twigdefaultfilter((isset($context["isdiff"]) || arraykeyexists("isdiff", $context) ? $context["isdiff"] : (function () { throw new TwigErrorRuntime('Variable "isdiff" does not exist.', 12, $this->getSourceContext()); })()), false)) : (false))) {
            echo " class=\"diff\"";
        }
        echo ">";
        $this->displayBlock('name', $context, $blocks);
        echo "</th>
<td>";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 31
        echo "</td>

";
        // line 33
        $this->displayBlock('fieldcompare', $context, $blocks);
        
        $internalb5c9fea0f6b0ad801f4a4360f246d869f1d888698d10596ba575512b496352c2->leave($internalb5c9fea0f6b0ad801f4a4360f246d869f1d888698d10596ba575512b496352c2prof);

        
        $internal81df71edcf21aa2624084dca80348c6ede726d05f68d788fc929648582d63eac->leave($internal81df71edcf21aa2624084dca80348c6ede726d05f68d788fc929648582d63eacprof);

    }

    // line 12
    public function blockname($context, array $blocks = array())
    {
        $internal1a4db64e80ded3a335fd3bfbd4249512bc7d9aaa45dab7ed2eafc926f9ed3c5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1a4db64e80ded3a335fd3bfbd4249512bc7d9aaa45dab7ed2eafc926f9ed3c5a->enter($internal1a4db64e80ded3a335fd3bfbd4249512bc7d9aaa45dab7ed2eafc926f9ed3c5aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "name"));

        $internal98a69d06a32769fd7c9f6321d362078bc9feea1d024cb5f3bdb85ab4d78db9d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal98a69d06a32769fd7c9f6321d362078bc9feea1d024cb5f3bdb85ab4d78db9d2->enter($internal98a69d06a32769fd7c9f6321d362078bc9feea1d024cb5f3bdb85ab4d78db9d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "name"));

        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
        
        $internal98a69d06a32769fd7c9f6321d362078bc9feea1d024cb5f3bdb85ab4d78db9d2->leave($internal98a69d06a32769fd7c9f6321d362078bc9feea1d024cb5f3bdb85ab4d78db9d2prof);

        
        $internal1a4db64e80ded3a335fd3bfbd4249512bc7d9aaa45dab7ed2eafc926f9ed3c5a->leave($internal1a4db64e80ded3a335fd3bfbd4249512bc7d9aaa45dab7ed2eafc926f9ed3c5aprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalc93a61af559a2f68bd8a8a7132950c2bedb1397fd3d5890e93ca002ee9d8ea8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc93a61af559a2f68bd8a8a7132950c2bedb1397fd3d5890e93ca002ee9d8ea8a->enter($internalc93a61af559a2f68bd8a8a7132950c2bedb1397fd3d5890e93ca002ee9d8ea8aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal1fcaa0f8cbdfee786498d39c6e636e4df0dc1de7b24527d51b224bbe8112735b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1fcaa0f8cbdfee786498d39c6e636e4df0dc1de7b24527d51b224bbe8112735b->enter($internal1fcaa0f8cbdfee786498d39c6e636e4df0dc1de7b24527d51b224bbe8112735bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        obstart();
        // line 16
        echo "            ";
        $context["collapse"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "collapse", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "collapse", array()), false)) : (false));
        // line 17
        echo "            ";
        if ((isset($context["collapse"]) || arraykeyexists("collapse", $context) ? $context["collapse"] : (function () { throw new TwigErrorRuntime('Variable "collapse" does not exist.', 17, $this->getSourceContext()); })())) {
            // line 18
            echo "                <div class=\"sonata-readmore\"
                      data-readmore-height=\"";
            // line 19
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array()), 40)) : (40)), "html", null, true);
            echo "\"
                      data-readmore-more=\"";
            // line 20
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array()), "readmore")) : ("readmore")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\"
                      data-readmore-less=\"";
            // line 21
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array()), "readless")) : ("readless")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
                    ";
            // line 22
            $this->displayBlock('fieldvalue', $context, $blocks);
            // line 25
            echo "                </div>
            ";
        } else {
            // line 27
            echo "                ";
            $this->displayBlock("fieldvalue", $context, $blocks);
            echo "
            ";
        }
        // line 29
        echo "        ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal1fcaa0f8cbdfee786498d39c6e636e4df0dc1de7b24527d51b224bbe8112735b->leave($internal1fcaa0f8cbdfee786498d39c6e636e4df0dc1de7b24527d51b224bbe8112735bprof);

        
        $internalc93a61af559a2f68bd8a8a7132950c2bedb1397fd3d5890e93ca002ee9d8ea8a->leave($internalc93a61af559a2f68bd8a8a7132950c2bedb1397fd3d5890e93ca002ee9d8ea8aprof);

    }

    // line 22
    public function blockfieldvalue($context, array $blocks = array())
    {
        $internal0986a357c5819b8b403a20eca95e38c3f4b313afc43d84f251c9da18ba6f49ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0986a357c5819b8b403a20eca95e38c3f4b313afc43d84f251c9da18ba6f49ab->enter($internal0986a357c5819b8b403a20eca95e38c3f4b313afc43d84f251c9da18ba6f49abprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldvalue"));

        $internal6b2d95290f85b6d2c535bcacfc365654481b10b166fcd6f3f180bfd2c41d4a3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b2d95290f85b6d2c535bcacfc365654481b10b166fcd6f3f180bfd2c41d4a3a->enter($internal6b2d95290f85b6d2c535bcacfc365654481b10b166fcd6f3f180bfd2c41d4a3aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldvalue"));

        // line 23
        echo "                        ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
            echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 23, $this->getSourceContext()); })());
        } else {
            echo nl2br(twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true));
        }
        // line 24
        echo "                    ";
        
        $internal6b2d95290f85b6d2c535bcacfc365654481b10b166fcd6f3f180bfd2c41d4a3a->leave($internal6b2d95290f85b6d2c535bcacfc365654481b10b166fcd6f3f180bfd2c41d4a3aprof);

        
        $internal0986a357c5819b8b403a20eca95e38c3f4b313afc43d84f251c9da18ba6f49ab->leave($internal0986a357c5819b8b403a20eca95e38c3f4b313afc43d84f251c9da18ba6f49abprof);

    }

    // line 33
    public function blockfieldcompare($context, array $blocks = array())
    {
        $internalafc901968138d59ac8fd2b6aa31e461d78b150b22a214e5615ab5b157db128a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalafc901968138d59ac8fd2b6aa31e461d78b150b22a214e5615ab5b157db128a0->enter($internalafc901968138d59ac8fd2b6aa31e461d78b150b22a214e5615ab5b157db128a0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldcompare"));

        $internal40e05dd6e859d403a323c82ca3e39e89e613063c5129fc9285a69bab7234e68d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal40e05dd6e859d403a323c82ca3e39e89e613063c5129fc9285a69bab7234e68d->enter($internal40e05dd6e859d403a323c82ca3e39e89e613063c5129fc9285a69bab7234e68dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldcompare"));

        // line 34
        echo "    ";
        if (arraykeyexists("valuecompare", $context)) {
            // line 35
            echo "        <td>
            ";
            // line 36
            $context["value"] = (isset($context["valuecompare"]) || arraykeyexists("valuecompare", $context) ? $context["valuecompare"] : (function () { throw new TwigErrorRuntime('Variable "valuecompare" does not exist.', 36, $this->getSourceContext()); })());
            // line 37
            echo "            ";
            $this->displayBlock("field", $context, $blocks);
            echo "
        </td>
    ";
        }
        
        $internal40e05dd6e859d403a323c82ca3e39e89e613063c5129fc9285a69bab7234e68d->leave($internal40e05dd6e859d403a323c82ca3e39e89e613063c5129fc9285a69bab7234e68dprof);

        
        $internalafc901968138d59ac8fd2b6aa31e461d78b150b22a214e5615ab5b157db128a0->leave($internalafc901968138d59ac8fd2b6aa31e461d78b150b22a214e5615ab5b157db128a0prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 37,  170 => 36,  167 => 35,  164 => 34,  155 => 33,  145 => 24,  138 => 23,  129 => 22,  118 => 29,  112 => 27,  108 => 25,  106 => 22,  102 => 21,  98 => 20,  94 => 19,  91 => 18,  88 => 17,  85 => 16,  83 => 15,  74 => 14,  56 => 12,  46 => 33,  42 => 31,  40 => 14,  32 => 12,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<th{% if(isdiff|default(false)) %} class=\"diff\"{% endif %}>{% block name %}{{ fielddescription.label|trans({}, fielddescription.translationDomain ?: admin.translationDomain) }}{% endblock %}</th>
<td>
    {%- block field -%}
        {% spaceless %}
            {% set collapse = fielddescription.options.collapse|default(false) %}
            {% if collapse %}
                <div class=\"sonata-readmore\"
                      data-readmore-height=\"{{ collapse.height|default(40) }}\"
                      data-readmore-more=\"{{ collapse.more|default('readmore')|trans({}, 'SonataAdminBundle') }}\"
                      data-readmore-less=\"{{ collapse.less|default('readless')|trans({}, 'SonataAdminBundle') }}\">
                    {% block fieldvalue %}
                        {% if fielddescription.options.safe %}{{ value|raw }}{% else %}{{ value|nl2br }}{% endif %}
                    {% endblock %}
                </div>
            {% else %}
                {{ block('fieldvalue') }}
            {% endif %}
        {% endspaceless %}
    {%- endblock -%}
</td>

{% block fieldcompare %}
    {% if(valuecompare is defined) %}
        <td>
            {% set value = valuecompare %}
            {{ block('field') }}
        </td>
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:baseshowfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseshowfield.html.twig");
    }
}
