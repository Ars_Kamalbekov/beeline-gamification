<?php

/* SonataAdminBundle:CRUD:listinnerrow.html.twig */
class TwigTemplatef7e7cb8a462b2da94c881711e7d48459d2b7250b3cad5c885c9a1ce82ecee5aa extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baselistinnerrow.html.twig", "SonataAdminBundle:CRUD:listinnerrow.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baselistinnerrow.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal5927b11404d4e39df8996221175403b59955c004f1ea26035fb25dc0464bcf0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5927b11404d4e39df8996221175403b59955c004f1ea26035fb25dc0464bcf0b->enter($internal5927b11404d4e39df8996221175403b59955c004f1ea26035fb25dc0464bcf0bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listinnerrow.html.twig"));

        $internal049caf2964b5ac498ea62b6d1ef3193027f73332494c286e0ce411fe41a3eac5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal049caf2964b5ac498ea62b6d1ef3193027f73332494c286e0ce411fe41a3eac5->enter($internal049caf2964b5ac498ea62b6d1ef3193027f73332494c286e0ce411fe41a3eac5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listinnerrow.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal5927b11404d4e39df8996221175403b59955c004f1ea26035fb25dc0464bcf0b->leave($internal5927b11404d4e39df8996221175403b59955c004f1ea26035fb25dc0464bcf0bprof);

        
        $internal049caf2964b5ac498ea62b6d1ef3193027f73332494c286e0ce411fe41a3eac5->leave($internal049caf2964b5ac498ea62b6d1ef3193027f73332494c286e0ce411fe41a3eac5prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listinnerrow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baselistinnerrow.html.twig' %}
", "SonataAdminBundle:CRUD:listinnerrow.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listinnerrow.html.twig");
    }
}
