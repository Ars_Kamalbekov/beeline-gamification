<?php

/* SonataAdminBundle:CRUD:baseinlineeditfield.html.twig */
class TwigTemplate2f9f1d26fc4698b18d5ea20bbdbc7a3d7083e563676d0ddf887fb5a840713744 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'blocklabel'),
            'field' => array($this, 'blockfield'),
            'errors' => array($this, 'blockerrors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald7b1cbbd0a20d6c5c9bdde527dfd4da8faee32e2b9bc7203ce2bb3270c251dba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald7b1cbbd0a20d6c5c9bdde527dfd4da8faee32e2b9bc7203ce2bb3270c251dba->enter($internald7b1cbbd0a20d6c5c9bdde527dfd4da8faee32e2b9bc7203ce2bb3270c251dbaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseinlineeditfield.html.twig"));

        $internald1dc6a244e6106c31da045a86b01c7af3bd6d4520e383185f001699e24c987e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald1dc6a244e6106c31da045a86b01c7af3bd6d4520e383185f001699e24c987e9->enter($internald1dc6a244e6106c31da045a86b01c7af3bd6d4520e383185f001699e24c987e9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseinlineeditfield.html.twig"));

        // line 11
        echo "
<div id=\"sonata-ba-field-container-";
        // line 12
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\" class=\"sonata-ba-field sonata-ba-field-";
        echo twigescapefilter($this->env, (isset($context["edit"]) || arraykeyexists("edit", $context) ? $context["edit"] : (function () { throw new TwigErrorRuntime('Variable "edit" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, (isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "errors", array()))) {
            echo "sonata-ba-field-error";
        }
        echo "\">

    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('field', $context, $blocks);
        // line 26
        echo "
    <div class=\"sonata-ba-field-error-messages\">
        ";
        // line 28
        $this->displayBlock('errors', $context, $blocks);
        // line 29
        echo "    </div>
</div>
";
        
        $internald7b1cbbd0a20d6c5c9bdde527dfd4da8faee32e2b9bc7203ce2bb3270c251dba->leave($internald7b1cbbd0a20d6c5c9bdde527dfd4da8faee32e2b9bc7203ce2bb3270c251dbaprof);

        
        $internald1dc6a244e6106c31da045a86b01c7af3bd6d4520e383185f001699e24c987e9->leave($internald1dc6a244e6106c31da045a86b01c7af3bd6d4520e383185f001699e24c987e9prof);

    }

    // line 14
    public function blocklabel($context, array $blocks = array())
    {
        $internal4fc607a4070774f4110b9783427f540b6ddb66f4592b4961de5704af92914e05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4fc607a4070774f4110b9783427f540b6ddb66f4592b4961de5704af92914e05->enter($internal4fc607a4070774f4110b9783427f540b6ddb66f4592b4961de5704af92914e05prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal5ca0c10f6001e88d75be21b442d38edaec434c5353a8168d10a5103a0b933c5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5ca0c10f6001e88d75be21b442d38edaec434c5353a8168d10a5103a0b933c5c->enter($internal5ca0c10f6001e88d75be21b442d38edaec434c5353a8168d10a5103a0b933c5cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if (((isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 15, $this->getSourceContext()); })()) == "natural")) {
            // line 16
            echo "            ";
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
                // line 17
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 17, $this->getSourceContext()); })()), 'label', (twigtestempty($label = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $label)));
                echo "
            ";
            } else {
                // line 19
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 19, $this->getSourceContext()); })()), 'label');
                echo "
            ";
            }
            // line 21
            echo "            <br>
        ";
        }
        // line 23
        echo "    ";
        
        $internal5ca0c10f6001e88d75be21b442d38edaec434c5353a8168d10a5103a0b933c5c->leave($internal5ca0c10f6001e88d75be21b442d38edaec434c5353a8168d10a5103a0b933c5cprof);

        
        $internal4fc607a4070774f4110b9783427f540b6ddb66f4592b4961de5704af92914e05->leave($internal4fc607a4070774f4110b9783427f540b6ddb66f4592b4961de5704af92914e05prof);

    }

    // line 25
    public function blockfield($context, array $blocks = array())
    {
        $internal4b27f420f3f278e67779f41a8685480415c2eb0c6f1c22f517f60d3e52234c15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4b27f420f3f278e67779f41a8685480415c2eb0c6f1c22f517f60d3e52234c15->enter($internal4b27f420f3f278e67779f41a8685480415c2eb0c6f1c22f517f60d3e52234c15prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal0a5b80df73503507431542f3afcb714c2192090b77f46152b37236ca65acfdb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0a5b80df73503507431542f3afcb714c2192090b77f46152b37236ca65acfdb5->enter($internal0a5b80df73503507431542f3afcb714c2192090b77f46152b37236ca65acfdb5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 25, $this->getSourceContext()); })()), 'widget');
        
        $internal0a5b80df73503507431542f3afcb714c2192090b77f46152b37236ca65acfdb5->leave($internal0a5b80df73503507431542f3afcb714c2192090b77f46152b37236ca65acfdb5prof);

        
        $internal4b27f420f3f278e67779f41a8685480415c2eb0c6f1c22f517f60d3e52234c15->leave($internal4b27f420f3f278e67779f41a8685480415c2eb0c6f1c22f517f60d3e52234c15prof);

    }

    // line 28
    public function blockerrors($context, array $blocks = array())
    {
        $internalfe5985a82db21ff827951354a28d5db39a9b4fd9da82f8340aa9204b5858b11f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfe5985a82db21ff827951354a28d5db39a9b4fd9da82f8340aa9204b5858b11f->enter($internalfe5985a82db21ff827951354a28d5db39a9b4fd9da82f8340aa9204b5858b11fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        $internalec46732f341ada741866eb91d4e26e996704ba629fb962d17f1ab63731d176fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalec46732f341ada741866eb91d4e26e996704ba629fb962d17f1ab63731d176fd->enter($internalec46732f341ada741866eb91d4e26e996704ba629fb962d17f1ab63731d176fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 28, $this->getSourceContext()); })()), 'errors');
        
        $internalec46732f341ada741866eb91d4e26e996704ba629fb962d17f1ab63731d176fd->leave($internalec46732f341ada741866eb91d4e26e996704ba629fb962d17f1ab63731d176fdprof);

        
        $internalfe5985a82db21ff827951354a28d5db39a9b4fd9da82f8340aa9204b5858b11f->leave($internalfe5985a82db21ff827951354a28d5db39a9b4fd9da82f8340aa9204b5858b11fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseinlineeditfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 28,  110 => 25,  100 => 23,  96 => 21,  90 => 19,  84 => 17,  81 => 16,  78 => 15,  69 => 14,  57 => 29,  55 => 28,  51 => 26,  49 => 25,  46 => 24,  44 => 14,  31 => 12,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div id=\"sonata-ba-field-container-{{ fieldelement.vars.id }}\" class=\"sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if fieldelement.vars.errors|length %}sonata-ba-field-error{% endif %}\">

    {% block label %}
        {% if inline == 'natural' %}
            {% if fielddescription.options.name is defined %}
                {{ formlabel(fieldelement, fielddescription.options.name) }}
            {% else %}
                {{ formlabel(fieldelement) }}
            {% endif %}
            <br>
        {% endif %}
    {% endblock %}

    {% block field %}{{ formwidget(fieldelement) }}{% endblock %}

    <div class=\"sonata-ba-field-error-messages\">
        {% block errors %}{{ formerrors(fieldelement) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:baseinlineeditfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseinlineeditfield.html.twig");
    }
}
