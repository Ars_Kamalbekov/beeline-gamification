<?php

/* SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig */
class TwigTemplate7671f141e5a422118ce064c3221a0cd3d08a6ff4a748efeae3d0b0a1240fd88b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 11
        $this->parent = $this->loadTemplate("SonataBlockBundle:Block:blockbase.html.twig", "SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig", 11);
        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataBlockBundle:Block:blockbase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal382d23e670007972fecfad3dd592fcba2fcbc7e520d0c85d6dd90b44a789e8be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal382d23e670007972fecfad3dd592fcba2fcbc7e520d0c85d6dd90b44a789e8be->enter($internal382d23e670007972fecfad3dd592fcba2fcbc7e520d0c85d6dd90b44a789e8beprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig"));

        $internalb1d8670f72194ff64c78ab9b94913d32e9c3deb5306544b07978164bd8f02431 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb1d8670f72194ff64c78ab9b94913d32e9c3deb5306544b07978164bd8f02431->enter($internalb1d8670f72194ff64c78ab9b94913d32e9c3deb5306544b07978164bd8f02431prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal382d23e670007972fecfad3dd592fcba2fcbc7e520d0c85d6dd90b44a789e8be->leave($internal382d23e670007972fecfad3dd592fcba2fcbc7e520d0c85d6dd90b44a789e8beprof);

        
        $internalb1d8670f72194ff64c78ab9b94913d32e9c3deb5306544b07978164bd8f02431->leave($internalb1d8670f72194ff64c78ab9b94913d32e9c3deb5306544b07978164bd8f02431prof);

    }

    // line 13
    public function blockblock($context, array $blocks = array())
    {
        $internale1ebf7ce1694dd44f5bb64034a1304e5742211877b1db46a03fe3654f487969f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale1ebf7ce1694dd44f5bb64034a1304e5742211877b1db46a03fe3654f487969f->enter($internale1ebf7ce1694dd44f5bb64034a1304e5742211877b1db46a03fe3654f487969fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal2dc67eaf870707d4ebca0867a2ea41f18ed7a16a307b156c371eee8a6e860186 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2dc67eaf870707d4ebca0867a2ea41f18ed7a16a307b156c371eee8a6e860186->enter($internal2dc67eaf870707d4ebca0867a2ea41f18ed7a16a307b156c371eee8a6e860186prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 14
        echo "    <div class=\"box box-primary\">
        <div class=\"box-header with-border\">
            <h3 class=\"box-title\">
                <i class=\"fa fa-history\"></i> ";
        // line 17
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titleauditlog", array(), "SonataAdminBundle"), "html", null, true);
        echo "
            </h3>
        </div>

        <div class=\"box-body\">
            <div class=\"panel-group\" id=\"accordion\">
                ";
        // line 23
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["revisions"]) || arraykeyexists("revisions", $context) ? $context["revisions"] : (function () { throw new TwigErrorRuntime('Variable "revisions" does not exist.', 23, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["revision"]) {
            // line 24
            echo "                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse";
            // line 27
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "\">
                                    ";
            // line 28
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "revision", array()), "rev", array()), "html", null, true);
            echo " - ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "revision", array()), "username", array()), "html", null, true);
            echo "
                                    - ";
            // line 29
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "revision", array()), "timestamp", array())), "html", null, true);
            echo "
                                </a>
                            </h4>
                        </div>
                        <div id=\"collapse";
            // line 33
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "\" class=\"panel-collapse collapse ";
            echo ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) ? ("in") : (""));
            echo "\">
                            <div class=\"panel-body\">
                                <ul>
                                    ";
            // line 36
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "entities", array()));
            foreach ($context['seq'] as $context["key"] => $context["changedEntity"]) {
                // line 37
                echo "                                        <li>
                                            ";
                // line 38
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["changedEntity"], "entity", array()), "html", null, true);
                echo " / ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["changedEntity"], "revisionType", array()), "html", null, true);
                echo "
                                            / ";
                // line 39
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["changedEntity"], "className", array()), "html", null, true);
                echo " - ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["changedEntity"], "id", array()), "id", array()), "html", null, true);
                echo "
                                        </li>
                                    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['changedEntity'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 42
            echo "                                </ul>
                            </div>
                        </div>
                    </div>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['revision'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 47
        echo "            </div>
        </div>
    </div>

";
        
        $internal2dc67eaf870707d4ebca0867a2ea41f18ed7a16a307b156c371eee8a6e860186->leave($internal2dc67eaf870707d4ebca0867a2ea41f18ed7a16a307b156c371eee8a6e860186prof);

        
        $internale1ebf7ce1694dd44f5bb64034a1304e5742211877b1db46a03fe3654f487969f->leave($internale1ebf7ce1694dd44f5bb64034a1304e5742211877b1db46a03fe3654f487969fprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 47,  134 => 42,  123 => 39,  117 => 38,  114 => 37,  110 => 36,  102 => 33,  95 => 29,  89 => 28,  85 => 27,  80 => 24,  63 => 23,  54 => 17,  49 => 14,  40 => 13,  11 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends 'SonataBlockBundle:Block:blockbase.html.twig' %}

{% block block %}
    <div class=\"box box-primary\">
        <div class=\"box-header with-border\">
            <h3 class=\"box-title\">
                <i class=\"fa fa-history\"></i> {{ 'titleauditlog'|trans({}, 'SonataAdminBundle') }}
            </h3>
        </div>

        <div class=\"box-body\">
            <div class=\"panel-group\" id=\"accordion\">
                {% for revision in revisions %}
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse{{ loop.index }}\">
                                    {{ revision.revision.rev }} - {{ revision.revision.username }}
                                    - {{ revision.revision.timestamp | date }}
                                </a>
                            </h4>
                        </div>
                        <div id=\"collapse{{ loop.index }}\" class=\"panel-collapse collapse {{ loop.first ? \"in\" : \"\" }}\">
                            <div class=\"panel-body\">
                                <ul>
                                    {% for changedEntity in revision.entities %}
                                        <li>
                                            {{ changedEntity.entity }} / {{ changedEntity.revisionType }}
                                            / {{ changedEntity.className }} - {{ changedEntity.id.id }}
                                        </li>
                                    {% endfor %}
                                </ul>
                            </div>
                        </div>
                    </div>
                {% endfor %}
            </div>
        </div>
    </div>

{% endblock %}
", "SonataDoctrineORMAdminBundle:Block:blockaudit.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/Block/blockaudit.html.twig");
    }
}
