<?php

/* SonataAdminBundle:CRUD:showhtml.html.twig */
class TwigTemplatece83e05cea081a813580696e9eb83e65e76df81b998f17d08d9e2f1128ee6cc0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showhtml.html.twig", 1);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal44000f41ed0d883e878296b6bb78560594f903c257d6ea5a28664a75a611b5f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal44000f41ed0d883e878296b6bb78560594f903c257d6ea5a28664a75a611b5f6->enter($internal44000f41ed0d883e878296b6bb78560594f903c257d6ea5a28664a75a611b5f6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showhtml.html.twig"));

        $internald47618f5be30bfa65752297ba6a38329ce4b7565e44c43077cd72e9a27faed33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald47618f5be30bfa65752297ba6a38329ce4b7565e44c43077cd72e9a27faed33->enter($internald47618f5be30bfa65752297ba6a38329ce4b7565e44c43077cd72e9a27faed33prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showhtml.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal44000f41ed0d883e878296b6bb78560594f903c257d6ea5a28664a75a611b5f6->leave($internal44000f41ed0d883e878296b6bb78560594f903c257d6ea5a28664a75a611b5f6prof);

        
        $internald47618f5be30bfa65752297ba6a38329ce4b7565e44c43077cd72e9a27faed33->leave($internald47618f5be30bfa65752297ba6a38329ce4b7565e44c43077cd72e9a27faed33prof);

    }

    // line 3
    public function blockfield($context, array $blocks = array())
    {
        $internale7edb89844ec27885bb59b61d5a90e5405ab8ec47a3262b8215269a16f23b3fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale7edb89844ec27885bb59b61d5a90e5405ab8ec47a3262b8215269a16f23b3fe->enter($internale7edb89844ec27885bb59b61d5a90e5405ab8ec47a3262b8215269a16f23b3feprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal1c5348a2587b84493d238123a39f180862497829ece4f8b6bc8389a748e89d62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1c5348a2587b84493d238123a39f180862497829ece4f8b6bc8389a748e89d62->enter($internal1c5348a2587b84493d238123a39f180862497829ece4f8b6bc8389a748e89d62prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 4
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 4, $this->getSourceContext()); })()))) {
            // line 5
            echo "&nbsp;
    ";
        } else {
            // line 7
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "truncate", array(), "any", true, true)) {
                // line 8
                $context["truncate"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 8, $this->getSourceContext()); })()), "options", array()), "truncate", array());
                // line 9
                echo "            ";
                $context["length"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "length", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "length", array()), 30)) : (30));
                // line 10
                echo "            ";
                $context["preserve"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "preserve", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "preserve", array()), false)) : (false));
                // line 11
                echo "            ";
                $context["separator"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "separator", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "separator", array()), "...")) : ("..."));
                // line 12
                echo "            ";
                echo twigtruncatefilter($this->env, striptags((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 12, $this->getSourceContext()); })())), (isset($context["length"]) || arraykeyexists("length", $context) ? $context["length"] : (function () { throw new TwigErrorRuntime('Variable "length" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["preserve"]) || arraykeyexists("preserve", $context) ? $context["preserve"] : (function () { throw new TwigErrorRuntime('Variable "preserve" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["separator"]) || arraykeyexists("separator", $context) ? $context["separator"] : (function () { throw new TwigErrorRuntime('Variable "separator" does not exist.', 12, $this->getSourceContext()); })()));
            } else {
                // line 14
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "strip", array(), "any", true, true)) {
                    // line 15
                    $context["value"] = striptags((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()));
                }
                // line 17
                echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })());
                echo "
        ";
            }
            // line 19
            echo "    ";
        }
        
        $internal1c5348a2587b84493d238123a39f180862497829ece4f8b6bc8389a748e89d62->leave($internal1c5348a2587b84493d238123a39f180862497829ece4f8b6bc8389a748e89d62prof);

        
        $internale7edb89844ec27885bb59b61d5a90e5405ab8ec47a3262b8215269a16f23b3fe->leave($internale7edb89844ec27885bb59b61d5a90e5405ab8ec47a3262b8215269a16f23b3feprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showhtml.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 19,  77 => 17,  74 => 15,  72 => 14,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  57 => 8,  55 => 7,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {% else %}
        {%- if fielddescription.options.truncate is defined -%}
            {% set truncate = fielddescription.options.truncate %}
            {% set length = truncate.length|default(30) %}
            {% set preserve = truncate.preserve|default(false) %}
            {% set separator = truncate.separator|default('...') %}
            {{ value|striptags|truncate(length, preserve, separator)|raw }}
        {%- else -%}
            {%- if fielddescription.options.strip is defined -%}
                {% set value = value|striptags %}
            {%- endif -%}
            {{ value|raw }}
        {% endif %}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:showhtml.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showhtml.html.twig");
    }
}
