<?php

/* FOSUserBundle:Security:logincontent.html.twig */
class TwigTemplate8b0f486059d16ce89937605dd0f6e5f725cd50eb448add57ecc7299d8ac33b13 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf430983d6615654aede0d7e048f446022911de69cdef26cb3d0ca3cf028053b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf430983d6615654aede0d7e048f446022911de69cdef26cb3d0ca3cf028053b4->enter($internalf430983d6615654aede0d7e048f446022911de69cdef26cb3d0ca3cf028053b4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Security:logincontent.html.twig"));

        $internalae18578654e1570e0cca7ddfeda4d56ac467cbd5946c86cbd83a15db2ee8b7dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalae18578654e1570e0cca7ddfeda4d56ac467cbd5946c86cbd83a15db2ee8b7dd->enter($internalae18578654e1570e0cca7ddfeda4d56ac467cbd5946c86cbd83a15db2ee8b7ddprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Security:logincontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        if ((isset($context["error"]) || arraykeyexists("error", $context) ? $context["error"] : (function () { throw new TwigErrorRuntime('Variable "error" does not exist.', 3, $this->getSourceContext()); })())) {
            // line 4
            echo "    <div>";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["error"]) || arraykeyexists("error", $context) ? $context["error"] : (function () { throw new TwigErrorRuntime('Variable "error" does not exist.', 4, $this->getSourceContext()); })()), "messageKey", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["error"]) || arraykeyexists("error", $context) ? $context["error"] : (function () { throw new TwigErrorRuntime('Variable "error" does not exist.', 4, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo "
<form action=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusersecuritycheck");
        echo "\" method=\"post\">
    ";
        // line 8
        if ((isset($context["csrftoken"]) || arraykeyexists("csrftoken", $context) ? $context["csrftoken"] : (function () { throw new TwigErrorRuntime('Variable "csrftoken" does not exist.', 8, $this->getSourceContext()); })())) {
            // line 9
            echo "        <input type=\"hidden\" name=\"csrftoken\" value=\"";
            echo twigescapefilter($this->env, (isset($context["csrftoken"]) || arraykeyexists("csrftoken", $context) ? $context["csrftoken"] : (function () { throw new TwigErrorRuntime('Variable "csrftoken" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
            echo "\" />
    ";
        }
        // line 11
        echo "
    <label for=\"username\">";
        // line 12
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    <input type=\"text\" id=\"username\" name=\"username\" value=\"";
        // line 13
        echo twigescapefilter($this->env, (isset($context["lastusername"]) || arraykeyexists("lastusername", $context) ? $context["lastusername"] : (function () { throw new TwigErrorRuntime('Variable "lastusername" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
        echo "\" required=\"required\" />

    <label for=\"password\">";
        // line 15
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" />

    <input type=\"checkbox\" id=\"rememberme\" name=\"rememberme\" value=\"on\" />
    <label for=\"rememberme\">";
        // line 19
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.rememberme", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>

    <input type=\"submit\" id=\"submit\" name=\"submit\" value=\"";
        // line 21
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
</form>
";
        
        $internalf430983d6615654aede0d7e048f446022911de69cdef26cb3d0ca3cf028053b4->leave($internalf430983d6615654aede0d7e048f446022911de69cdef26cb3d0ca3cf028053b4prof);

        
        $internalae18578654e1570e0cca7ddfeda4d56ac467cbd5946c86cbd83a15db2ee8b7dd->leave($internalae18578654e1570e0cca7ddfeda4d56ac467cbd5946c86cbd83a15db2ee8b7ddprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:logincontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 21,  70 => 19,  63 => 15,  58 => 13,  54 => 12,  51 => 11,  45 => 9,  43 => 8,  39 => 7,  36 => 6,  30 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

<form action=\"{{ path(\"fosusersecuritycheck\") }}\" method=\"post\">
    {% if csrftoken %}
        <input type=\"hidden\" name=\"csrftoken\" value=\"{{ csrftoken }}\" />
    {% endif %}

    <label for=\"username\">{{ 'security.login.username'|trans }}</label>
    <input type=\"text\" id=\"username\" name=\"username\" value=\"{{ lastusername }}\" required=\"required\" />

    <label for=\"password\">{{ 'security.login.password'|trans }}</label>
    <input type=\"password\" id=\"password\" name=\"password\" required=\"required\" />

    <input type=\"checkbox\" id=\"rememberme\" name=\"rememberme\" value=\"on\" />
    <label for=\"rememberme\">{{ 'security.login.rememberme'|trans }}</label>

    <input type=\"submit\" id=\"submit\" name=\"submit\" value=\"{{ 'security.login.submit'|trans }}\" />
</form>
", "FOSUserBundle:Security:logincontent.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Security/logincontent.html.twig");
    }
}
