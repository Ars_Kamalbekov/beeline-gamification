<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class TwigTemplatea88beacbb5926574c1d9a740ea1a76e5bcb7f7b0291289f06a3bdea99791ae86 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'blockhead'),
            'body' => array($this, 'blockbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald01c227ef448cde7086c38f85760248ec2e6d9be4ff379add8721c05c9bbc1ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald01c227ef448cde7086c38f85760248ec2e6d9be4ff379add8721c05c9bbc1ad->enter($internald01c227ef448cde7086c38f85760248ec2e6d9be4ff379add8721c05c9bbc1adprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $internal0a6ece1e686c0315e233958c29687e8eb50b68334f50e9f72025aa6064c062fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0a6ece1e686c0315e233958c29687e8eb50b68334f50e9f72025aa6064c062fc->enter($internal0a6ece1e686c0315e233958c29687e8eb50b68334f50e9f72025aa6064c062fcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald01c227ef448cde7086c38f85760248ec2e6d9be4ff379add8721c05c9bbc1ad->leave($internald01c227ef448cde7086c38f85760248ec2e6d9be4ff379add8721c05c9bbc1adprof);

        
        $internal0a6ece1e686c0315e233958c29687e8eb50b68334f50e9f72025aa6064c062fc->leave($internal0a6ece1e686c0315e233958c29687e8eb50b68334f50e9f72025aa6064c062fcprof);

    }

    // line 3
    public function blockhead($context, array $blocks = array())
    {
        $internalfe83906fd852ceff76560e470e788a18caad3b6fabbc3633db0bbc007c3c70a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfe83906fd852ceff76560e470e788a18caad3b6fabbc3633db0bbc007c3c70a8->enter($internalfe83906fd852ceff76560e470e788a18caad3b6fabbc3633db0bbc007c3c70a8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        $internal06e16caf0663ddbe4435effdf666f91276593f74f1f43fb49007f7511e928739 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal06e16caf0663ddbe4435effdf666f91276593f74f1f43fb49007f7511e928739->enter($internal06e16caf0663ddbe4435effdf666f91276593f74f1f43fb49007f7511e928739prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $internal06e16caf0663ddbe4435effdf666f91276593f74f1f43fb49007f7511e928739->leave($internal06e16caf0663ddbe4435effdf666f91276593f74f1f43fb49007f7511e928739prof);

        
        $internalfe83906fd852ceff76560e470e788a18caad3b6fabbc3633db0bbc007c3c70a8->leave($internalfe83906fd852ceff76560e470e788a18caad3b6fabbc3633db0bbc007c3c70a8prof);

    }

    // line 9
    public function blockbody($context, array $blocks = array())
    {
        $internal20084f33f63798e734867eafe0db0e1bf8ebc553e5aeee475149f01e48cce796 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal20084f33f63798e734867eafe0db0e1bf8ebc553e5aeee475149f01e48cce796->enter($internal20084f33f63798e734867eafe0db0e1bf8ebc553e5aeee475149f01e48cce796prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internalf2095b676948f01ce3e7637b7c06318491c3514c7f5f831b3911ac035bbe9d1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf2095b676948f01ce3e7637b7c06318491c3514c7f5f831b3911ac035bbe9d1d->enter($internalf2095b676948f01ce3e7637b7c06318491c3514c7f5f831b3911ac035bbe9d1dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twigescapefilter($this->env, (isset($context["file"]) || arraykeyexists("file", $context) ? $context["file"] : (function () { throw new TwigErrorRuntime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twigescapefilter($this->env, (isset($context["line"]) || arraykeyexists("line", $context) ? $context["line"] : (function () { throw new TwigErrorRuntime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twigescapefilter($this->env, twigconstant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || arraykeyexists("filename", $context) ? $context["filename"] : (function () { throw new TwigErrorRuntime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || arraykeyexists("line", $context) ? $context["line"] : (function () { throw new TwigErrorRuntime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $internalf2095b676948f01ce3e7637b7c06318491c3514c7f5f831b3911ac035bbe9d1d->leave($internalf2095b676948f01ce3e7637b7c06318491c3514c7f5f831b3911ac035bbe9d1dprof);

        
        $internal20084f33f63798e734867eafe0db0e1bf8ebc553e5aeee475149f01e48cce796->leave($internal20084f33f63798e734867eafe0db0e1bf8ebc553e5aeee475149f01e48cce796prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|fileexcerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
