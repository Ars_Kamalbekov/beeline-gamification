<?php

/* SonataBlockBundle:Block:blockexception.html.twig */
class TwigTemplate3c154b180fd4efdc92c4de08c53ca2ced141c935e5a145673a4302594c3cc801 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockexception.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldf73fbaf750a18d68b96e7119e1acf8e58fb90ec7ae40ae90e182fd97e156562 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldf73fbaf750a18d68b96e7119e1acf8e58fb90ec7ae40ae90e182fd97e156562->enter($internaldf73fbaf750a18d68b96e7119e1acf8e58fb90ec7ae40ae90e182fd97e156562prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockexception.html.twig"));

        $internalef3daf045bfe8a8522c0d9ae64b454ce7d8f0b8f7d1f189a90d9cf3cb0d2444a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalef3daf045bfe8a8522c0d9ae64b454ce7d8f0b8f7d1f189a90d9cf3cb0d2444a->enter($internalef3daf045bfe8a8522c0d9ae64b454ce7d8f0b8f7d1f189a90d9cf3cb0d2444aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockexception.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internaldf73fbaf750a18d68b96e7119e1acf8e58fb90ec7ae40ae90e182fd97e156562->leave($internaldf73fbaf750a18d68b96e7119e1acf8e58fb90ec7ae40ae90e182fd97e156562prof);

        
        $internalef3daf045bfe8a8522c0d9ae64b454ce7d8f0b8f7d1f189a90d9cf3cb0d2444a->leave($internalef3daf045bfe8a8522c0d9ae64b454ce7d8f0b8f7d1f189a90d9cf3cb0d2444aprof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internalade3e8856316fce0b4e87f49763d77086aaee3d146f4f4fc9800338ca4aa87ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalade3e8856316fce0b4e87f49763d77086aaee3d146f4f4fc9800338ca4aa87ff->enter($internalade3e8856316fce0b4e87f49763d77086aaee3d146f4f4fc9800338ca4aa87ffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internalba52c5b5944f419e0ac72716082ae443dbebaab24df6f0486d49ea22de2367e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalba52c5b5944f419e0ac72716082ae443dbebaab24df6f0486d49ea22de2367e6->enter($internalba52c5b5944f419e0ac72716082ae443dbebaab24df6f0486d49ea22de2367e6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\">
        <h2>";
        // line 16
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["block"]) || arraykeyexists("block", $context) ? $context["block"] : (function () { throw new TwigErrorRuntime('Variable "block" does not exist.', 16, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "</h2>
        <h3>";
        // line 17
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 17, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo "</h3>
    </div>
";
        
        $internalba52c5b5944f419e0ac72716082ae443dbebaab24df6f0486d49ea22de2367e6->leave($internalba52c5b5944f419e0ac72716082ae443dbebaab24df6f0486d49ea22de2367e6prof);

        
        $internalade3e8856316fce0b4e87f49763d77086aaee3d146f4f4fc9800338ca4aa87ff->leave($internalade3e8856316fce0b4e87f49763d77086aaee3d146f4f4fc9800338ca4aa87ffprof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockexception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    <div class=\"cms-block-exception\">
        <h2>{{ block.name }}</h2>
        <h3>{{ exception.message }}</h3>
    </div>
{% endblock %}
", "SonataBlockBundle:Block:blockexception.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockexception.html.twig");
    }
}
