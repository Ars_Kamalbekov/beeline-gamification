<?php

/* TwigBundle:Exception:traces.html.twig */
class TwigTemplate30d8c21ff2c47130d4e3f0b1af8f2c5c9cf3eb18b17aed4d5ef0481603db7eff extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal255ababcd2fe7a91d48890542576ab719c0399f326491e2ed4bfae7724d2b7ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal255ababcd2fe7a91d48890542576ab719c0399f326491e2ed4bfae7724d2b7ba->enter($internal255ababcd2fe7a91d48890542576ab719c0399f326491e2ed4bfae7724d2b7baprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.html.twig"));

        $internalb5539c7e8afe9e8847f452a3a531e2300f3f52446750815e7db62abb3ef12db0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb5539c7e8afe9e8847f452a3a531e2300f3f52446750815e7db62abb3ef12db0->enter($internalb5539c7e8afe9e8847f452a3a531e2300f3f52446750815e7db62abb3ef12db0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.html.twig"));

        // line 1
        echo "<div class=\"trace trace-as-html\">
    <div class=\"trace-details\">
        <div class=\"trace-head\">
            <span class=\"sf-toggle\" data-toggle-selector=\"#trace-html-";
        // line 4
        echo twigescapefilter($this->env, (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 4, $this->getSourceContext()); })()), "html", null, true);
        echo "\" data-toggle-initial=\"";
        echo (((isset($context["expand"]) || arraykeyexists("expand", $context) ? $context["expand"] : (function () { throw new TwigErrorRuntime('Variable "expand" does not exist.', 4, $this->getSourceContext()); })())) ? ("display") : (""));
        echo "\">
                <h3 class=\"trace-class\">
                    <span class=\"trace-namespace\">
                        ";
        // line 7
        echo twigescapefilter($this->env, twigjoinfilter(twigslice($this->env, twigsplitfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 7, $this->getSourceContext()); })()), "class", array()), "\\"), 0,  -1), "\\"), "html", null, true);
        // line 8
        echo (((twiglengthfilter($this->env, twigsplitfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 8, $this->getSourceContext()); })()), "class", array()), "\\")) > 1)) ? ("\\") : (""));
        echo "
                    </span>
                    ";
        // line 10
        echo twigescapefilter($this->env, twiglast($this->env, twigsplitfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 10, $this->getSourceContext()); })()), "class", array()), "\\")), "html", null, true);
        echo "

                    <span class=\"icon icon-close\">";
        // line 12
        echo twiginclude($this->env, $context, "@Twig/images/icon-minus-square-o.svg");
        echo "</span>
                    <span class=\"icon icon-open\">";
        // line 13
        echo twiginclude($this->env, $context, "@Twig/images/icon-plus-square-o.svg");
        echo "</span>
                </h3>

                ";
        // line 16
        if (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 16, $this->getSourceContext()); })()), "message", array())) && ((isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 16, $this->getSourceContext()); })()) > 1))) {
            // line 17
            echo "                    <p class=\"break-long-words trace-message\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 17, $this->getSourceContext()); })()), "message", array()), "html", null, true);
            echo "</p>
                ";
        }
        // line 19
        echo "            </span>
        </div>

        <div id=\"trace-html-";
        // line 22
        echo twigescapefilter($this->env, (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 22, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"sf-toggle-content\">
        ";
        // line 23
        $context["isfirstusercode"] = true;
        // line 24
        echo "        ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 24, $this->getSourceContext()); })()), "trace", array()));
        foreach ($context['seq'] as $context["i"] => $context["trace"]) {
            // line 25
            echo "            ";
            $context["displaycodesnippet"] = ((((isset($context["isfirstusercode"]) || arraykeyexists("isfirstusercode", $context) ? $context["isfirstusercode"] : (function () { throw new TwigErrorRuntime('Variable "isfirstusercode" does not exist.', 25, $this->getSourceContext()); })()) && !twiginfilter("/vendor/", twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array()))) && !twiginfilter("/var/cache/", twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array()))) &&  !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array())));
            // line 26
            echo "            ";
            if ((isset($context["displaycodesnippet"]) || arraykeyexists("displaycodesnippet", $context) ? $context["displaycodesnippet"] : (function () { throw new TwigErrorRuntime('Variable "displaycodesnippet" does not exist.', 26, $this->getSourceContext()); })())) {
                $context["isfirstusercode"] = false;
            }
            // line 27
            echo "            <div class=\"trace-line\">
                ";
            // line 28
            echo twiginclude($this->env, $context, "@Twig/Exception/trace.html.twig", array("prefix" => (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 28, $this->getSourceContext()); })()), "i" => $context["i"], "trace" => $context["trace"], "displaycodesnippet" => (isset($context["displaycodesnippet"]) || arraykeyexists("displaycodesnippet", $context) ? $context["displaycodesnippet"] : (function () { throw new TwigErrorRuntime('Variable "displaycodesnippet" does not exist.', 28, $this->getSourceContext()); })())), false);
            echo "
            </div>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['i'], $context['trace'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 31
        echo "        </div>
    </div>
</div>
";
        
        $internal255ababcd2fe7a91d48890542576ab719c0399f326491e2ed4bfae7724d2b7ba->leave($internal255ababcd2fe7a91d48890542576ab719c0399f326491e2ed4bfae7724d2b7baprof);

        
        $internalb5539c7e8afe9e8847f452a3a531e2300f3f52446750815e7db62abb3ef12db0->leave($internalb5539c7e8afe9e8847f452a3a531e2300f3f52446750815e7db62abb3ef12db0prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 31,  95 => 28,  92 => 27,  87 => 26,  84 => 25,  79 => 24,  77 => 23,  73 => 22,  68 => 19,  62 => 17,  60 => 16,  54 => 13,  50 => 12,  45 => 10,  40 => 8,  38 => 7,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div class=\"trace trace-as-html\">
    <div class=\"trace-details\">
        <div class=\"trace-head\">
            <span class=\"sf-toggle\" data-toggle-selector=\"#trace-html-{{ index }}\" data-toggle-initial=\"{{ expand ? 'display' }}\">
                <h3 class=\"trace-class\">
                    <span class=\"trace-namespace\">
                        {{ exception.class|split('\\\\')|slice(0, -1)|join('\\\\') }}
                        {{- exception.class|split('\\\\')|length > 1 ? '\\\\' }}
                    </span>
                    {{ exception.class|split('\\\\')|last }}

                    <span class=\"icon icon-close\">{{ include('@Twig/images/icon-minus-square-o.svg') }}</span>
                    <span class=\"icon icon-open\">{{ include('@Twig/images/icon-plus-square-o.svg') }}</span>
                </h3>

                {% if exception.message is not empty and index > 1 %}
                    <p class=\"break-long-words trace-message\">{{ exception.message }}</p>
                {% endif %}
            </span>
        </div>

        <div id=\"trace-html-{{ index }}\" class=\"sf-toggle-content\">
        {% set isfirstusercode = true %}
        {% for i, trace in exception.trace %}
            {% set displaycodesnippet = isfirstusercode and ('/vendor/' not in trace.file) and ('/var/cache/' not in trace.file) and (trace.file is not empty) %}
            {% if displaycodesnippet %}{% set isfirstusercode = false %}{% endif %}
            <div class=\"trace-line\">
                {{ include('@Twig/Exception/trace.html.twig', { prefix: index, i: i, trace: trace, displaycodesnippet: displaycodesnippet }, withcontext = false) }}
            </div>
        {% endfor %}
        </div>
    </div>
</div>
", "TwigBundle:Exception:traces.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.html.twig");
    }
}
