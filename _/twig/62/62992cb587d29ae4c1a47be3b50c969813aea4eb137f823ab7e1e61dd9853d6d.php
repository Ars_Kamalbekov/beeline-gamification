<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class TwigTemplatefcf16ac7cc93cf1080405da07d2834f1525aba0debff166ba7dd1154d2d8d750 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfbfa1db55e33bff238a22324be6fd3c0be8391e8bbb72a3ad06b51a954fef2c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfbfa1db55e33bff238a22324be6fd3c0be8391e8bbb72a3ad06b51a954fef2c4->enter($internalfbfa1db55e33bff238a22324be6fd3c0be8391e8bbb72a3ad06b51a954fef2c4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $internalc606277f87049851177288491bb86244d76f6bc95663cc82c451ad07f7645dc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc606277f87049851177288491bb86244d76f6bc95663cc82c451ad07f7645dc7->enter($internalc606277f87049851177288491bb86244d76f6bc95663cc82c451ad07f7645dc7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalfbfa1db55e33bff238a22324be6fd3c0be8391e8bbb72a3ad06b51a954fef2c4->leave($internalfbfa1db55e33bff238a22324be6fd3c0be8391e8bbb72a3ad06b51a954fef2c4prof);

        
        $internalc606277f87049851177288491bb86244d76f6bc95663cc82c451ad07f7645dc7->leave($internalc606277f87049851177288491bb86244d76f6bc95663cc82c451ad07f7645dc7prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal71cb33834212c4cc1cb5df389f365e0b644c18b476413c9ddcccad358658e428 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal71cb33834212c4cc1cb5df389f365e0b644c18b476413c9ddcccad358658e428->enter($internal71cb33834212c4cc1cb5df389f365e0b644c18b476413c9ddcccad358658e428prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalce746f0aedfd3da164084638b92e28806888ed0abd851b10f46b17768f3b752d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalce746f0aedfd3da164084638b92e28806888ed0abd851b10f46b17768f3b752d->enter($internalce746f0aedfd3da164084638b92e28806888ed0abd851b10f46b17768f3b752dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        obstart();
        // line 5
        echo "        ";
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => false));
        echo "
";
        
        $internalce746f0aedfd3da164084638b92e28806888ed0abd851b10f46b17768f3b752d->leave($internalce746f0aedfd3da164084638b92e28806888ed0abd851b10f46b17768f3b752dprof);

        
        $internal71cb33834212c4cc1cb5df389f365e0b644c18b476413c9ddcccad358658e428->leave($internal71cb33834212c4cc1cb5df389f365e0b644c18b476413c9ddcccad358658e428prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
