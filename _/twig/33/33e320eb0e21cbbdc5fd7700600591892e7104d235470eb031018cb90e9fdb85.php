<?php

/* SonataCoreBundle:FlashMessage:render.html.twig */
class TwigTemplated2c40ae2524881de1ba1e04ab513505dfa01a45aab3190fd1e3b1e9ab5c9d700 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4f7b45b2664a45215e6985a1942ec5f98d18ba1901a3e926506924bc69bc3376 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4f7b45b2664a45215e6985a1942ec5f98d18ba1901a3e926506924bc69bc3376->enter($internal4f7b45b2664a45215e6985a1942ec5f98d18ba1901a3e926506924bc69bc3376prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:FlashMessage:render.html.twig"));

        $internal96b3d772b74d51797b5ed16aebb6d7540df305a0cea2889bffcf243216aec12a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal96b3d772b74d51797b5ed16aebb6d7540df305a0cea2889bffcf243216aec12a->enter($internal96b3d772b74d51797b5ed16aebb6d7540df305a0cea2889bffcf243216aec12aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:FlashMessage:render.html.twig"));

        // line 11
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable($this->env->getExtension('Sonata\CoreBundle\Twig\Extension\FlashMessageExtension')->getFlashMessagesTypes());
        foreach ($context['seq'] as $context["key"] => $context["type"]) {
            // line 12
            echo "    ";
            $context["domain"] = ((arraykeyexists("domain", $context)) ? ((isset($context["domain"]) || arraykeyexists("domain", $context) ? $context["domain"] : (function () { throw new TwigErrorRuntime('Variable "domain" does not exist.', 12, $this->getSourceContext()); })())) : (null));
            // line 13
            echo "    ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable($this->env->getExtension('Sonata\CoreBundle\Twig\Extension\FlashMessageExtension')->getFlashMessages($context["type"], (isset($context["domain"]) || arraykeyexists("domain", $context) ? $context["domain"] : (function () { throw new TwigErrorRuntime('Variable "domain" does not exist.', 13, $this->getSourceContext()); })())));
            foreach ($context['seq'] as $context["key"] => $context["message"]) {
                // line 14
                echo "        <div class=\"alert alert-";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\CoreBundle\Twig\Extension\StatusExtension')->statusClass($context["type"]), "html", null, true);
                echo " alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            ";
                // line 16
                echo $context["message"];
                echo "
        </div>
    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['message'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['type'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internal4f7b45b2664a45215e6985a1942ec5f98d18ba1901a3e926506924bc69bc3376->leave($internal4f7b45b2664a45215e6985a1942ec5f98d18ba1901a3e926506924bc69bc3376prof);

        
        $internal96b3d772b74d51797b5ed16aebb6d7540df305a0cea2889bffcf243216aec12a->leave($internal96b3d772b74d51797b5ed16aebb6d7540df305a0cea2889bffcf243216aec12aprof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:FlashMessage:render.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 16,  37 => 14,  32 => 13,  29 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% for type in sonataflashmessagestypes() %}
    {% set domain = domain is defined ? domain : null %}
    {% for message in sonataflashmessagesget(type, domain) %}
        <div class=\"alert alert-{{ type|sonatastatusclass }} alert-dismissable\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
            {{ message|raw }}
        </div>
    {% endfor %}
{% endfor %}
", "SonataCoreBundle:FlashMessage:render.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/core-bundle/Resources/views/FlashMessage/render.html.twig");
    }
}
