<?php

/* SonataAdminBundle:CRUD/Association:listmanytoone.html.twig */
class TwigTemplate6d036eda8073ea989b900f5c212da82886096fcb2c54ad8dab993dd65f6b42de extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD/Association:listmanytoone.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale808385c2db12fe3a78a5138994273ad189fc1f48639253666a1867328d7588c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale808385c2db12fe3a78a5138994273ad189fc1f48639253666a1867328d7588c->enter($internale808385c2db12fe3a78a5138994273ad189fc1f48639253666a1867328d7588cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listmanytoone.html.twig"));

        $internalf0b957bb08220fd9674995c8021189548e1ee4b5c4b69c47e02791160d122b43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf0b957bb08220fd9674995c8021189548e1ee4b5c4b69c47e02791160d122b43->enter($internalf0b957bb08220fd9674995c8021189548e1ee4b5c4b69c47e02791160d122b43prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listmanytoone.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internale808385c2db12fe3a78a5138994273ad189fc1f48639253666a1867328d7588c->leave($internale808385c2db12fe3a78a5138994273ad189fc1f48639253666a1867328d7588cprof);

        
        $internalf0b957bb08220fd9674995c8021189548e1ee4b5c4b69c47e02791160d122b43->leave($internalf0b957bb08220fd9674995c8021189548e1ee4b5c4b69c47e02791160d122b43prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalc2fee0881d4209f4694b755a4abdf7e8fe7fbd2aa2b445847bf4bbc1f13466c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc2fee0881d4209f4694b755a4abdf7e8fe7fbd2aa2b445847bf4bbc1f13466c0->enter($internalc2fee0881d4209f4694b755a4abdf7e8fe7fbd2aa2b445847bf4bbc1f13466c0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal1edac839fbd1e3aa8ae402956720bb4f12c18f3a23d244ecb52f18dc89374675 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1edac839fbd1e3aa8ae402956720bb4f12c18f3a23d244ecb52f18dc89374675->enter($internal1edac839fbd1e3aa8ae402956720bb4f12c18f3a23d244ecb52f18dc89374675prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })())) {
            // line 16
            echo "        ";
            $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
            // line 17
            echo "        ";
            if (((( !((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "identifier", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "identifier", array()), false)) : (false)) && twiggetattribute($this->env, $this->getSourceContext(),             // line 18
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "hasAssociationAdmin", array())) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 20
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })())), "method"))) {
                // line 22
                echo "            <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 22, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 22, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                ";
                // line 23
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 23, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
                echo "
            </a>
        ";
            } else {
                // line 26
                echo "            ";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 26, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 26, $this->getSourceContext()); })())), "html", null, true);
                echo "
        ";
            }
            // line 28
            echo "    ";
        }
        
        $internal1edac839fbd1e3aa8ae402956720bb4f12c18f3a23d244ecb52f18dc89374675->leave($internal1edac839fbd1e3aa8ae402956720bb4f12c18f3a23d244ecb52f18dc89374675prof);

        
        $internalc2fee0881d4209f4694b755a4abdf7e8fe7fbd2aa2b445847bf4bbc1f13466c0->leave($internalc2fee0881d4209f4694b755a4abdf7e8fe7fbd2aa2b445847bf4bbc1f13466c0prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:listmanytoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 28,  71 => 26,  65 => 23,  60 => 22,  58 => 20,  57 => 19,  56 => 18,  54 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% if value %}
        {% set routename = fielddescription.options.route.name %}
        {% if not fielddescription.options.identifier|default(false) and
              fielddescription.hasAssociationAdmin and
              fielddescription.associationadmin.hasRoute(routename) and
              fielddescription.associationadmin.hasAccess(routename, value)
        %}
            <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, value, fielddescription.options.route.parameters) }}\">
                {{ value|renderrelationelement(fielddescription) }}
            </a>
        {% else %}
            {{ value|renderrelationelement(fielddescription) }}
        {% endif %}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD/Association:listmanytoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/listmanytoone.html.twig");
    }
}
