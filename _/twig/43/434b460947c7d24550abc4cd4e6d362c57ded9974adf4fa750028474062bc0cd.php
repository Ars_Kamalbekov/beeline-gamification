<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class TwigTemplatedaad3dfd0a63f5678a46dba1d6c6430b07ed878858e3ce9d8245dfe0f2348f50 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'blocksubject'),
            'bodytext' => array($this, 'blockbodytext'),
            'bodyhtml' => array($this, 'blockbodyhtml'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6dba1ae971485c502a3032386cb715b6cc51a2c1fd8c6dea16f3a499fe0484d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6dba1ae971485c502a3032386cb715b6cc51a2c1fd8c6dea16f3a499fe0484d1->enter($internal6dba1ae971485c502a3032386cb715b6cc51a2c1fd8c6dea16f3a499fe0484d1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $internal3b7a457ca6b501538aaccd48acd354176f1cc1914233329aa34dd50178dfb58c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3b7a457ca6b501538aaccd48acd354176f1cc1914233329aa34dd50178dfb58c->enter($internal3b7a457ca6b501538aaccd48acd354176f1cc1914233329aa34dd50178dfb58cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('bodytext', $context, $blocks);
        // line 13
        $this->displayBlock('bodyhtml', $context, $blocks);
        
        $internal6dba1ae971485c502a3032386cb715b6cc51a2c1fd8c6dea16f3a499fe0484d1->leave($internal6dba1ae971485c502a3032386cb715b6cc51a2c1fd8c6dea16f3a499fe0484d1prof);

        
        $internal3b7a457ca6b501538aaccd48acd354176f1cc1914233329aa34dd50178dfb58c->leave($internal3b7a457ca6b501538aaccd48acd354176f1cc1914233329aa34dd50178dfb58cprof);

    }

    // line 2
    public function blocksubject($context, array $blocks = array())
    {
        $internalc8993d9061b22355c4df05628ceda9f3421596dd102037139718df0777a0965a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc8993d9061b22355c4df05628ceda9f3421596dd102037139718df0777a0965a->enter($internalc8993d9061b22355c4df05628ceda9f3421596dd102037139718df0777a0965aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "subject"));

        $internal2d750849f3259b0d2b7b6f7c0240c1cf6d90ad3eba5d8d13be7effa4dcf60fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2d750849f3259b0d2b7b6f7c0240c1cf6d90ad3eba5d8d13be7effa4dcf60fd8->enter($internal2d750849f3259b0d2b7b6f7c0240c1cf6d90ad3eba5d8d13be7effa4dcf60fd8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle");
        
        $internal2d750849f3259b0d2b7b6f7c0240c1cf6d90ad3eba5d8d13be7effa4dcf60fd8->leave($internal2d750849f3259b0d2b7b6f7c0240c1cf6d90ad3eba5d8d13be7effa4dcf60fd8prof);

        
        $internalc8993d9061b22355c4df05628ceda9f3421596dd102037139718df0777a0965a->leave($internalc8993d9061b22355c4df05628ceda9f3421596dd102037139718df0777a0965aprof);

    }

    // line 8
    public function blockbodytext($context, array $blocks = array())
    {
        $internal849ac3116e4a75769302da2c47953fd845deb6bc949e8c337d965118b469bf02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal849ac3116e4a75769302da2c47953fd845deb6bc949e8c337d965118b469bf02->enter($internal849ac3116e4a75769302da2c47953fd845deb6bc949e8c337d965118b469bf02prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodytext"));

        $internalf9c80b83e32de617aac28487a10af0c76300509e5e5d0e7aec574197a0559bf6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf9c80b83e32de617aac28487a10af0c76300509e5e5d0e7aec574197a0559bf6->enter($internalf9c80b83e32de617aac28487a10af0c76300509e5e5d0e7aec574197a0559bf6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodytext"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || arraykeyexists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new TwigErrorRuntime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $internalf9c80b83e32de617aac28487a10af0c76300509e5e5d0e7aec574197a0559bf6->leave($internalf9c80b83e32de617aac28487a10af0c76300509e5e5d0e7aec574197a0559bf6prof);

        
        $internal849ac3116e4a75769302da2c47953fd845deb6bc949e8c337d965118b469bf02->leave($internal849ac3116e4a75769302da2c47953fd845deb6bc949e8c337d965118b469bf02prof);

    }

    // line 13
    public function blockbodyhtml($context, array $blocks = array())
    {
        $internal92e2b4b75accbd11d6b142c16b45ce3792d0bd92fe7d8009f07f63eb1881cf46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal92e2b4b75accbd11d6b142c16b45ce3792d0bd92fe7d8009f07f63eb1881cf46->enter($internal92e2b4b75accbd11d6b142c16b45ce3792d0bd92fe7d8009f07f63eb1881cf46prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyhtml"));

        $internalfae009405095ee14247c351a426d840e3e2b0fed1de4ab93eb9f1d6d55d38607 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfae009405095ee14247c351a426d840e3e2b0fed1de4ab93eb9f1d6d55d38607->enter($internalfae009405095ee14247c351a426d840e3e2b0fed1de4ab93eb9f1d6d55d38607prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyhtml"));

        
        $internalfae009405095ee14247c351a426d840e3e2b0fed1de4ab93eb9f1d6d55d38607->leave($internalfae009405095ee14247c351a426d840e3e2b0fed1de4ab93eb9f1d6d55d38607prof);

        
        $internal92e2b4b75accbd11d6b142c16b45ce3792d0bd92fe7d8009f07f63eb1881cf46->leave($internal92e2b4b75accbd11d6b142c16b45ce3792d0bd92fe7d8009f07f63eb1881cf46prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block bodytext %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block bodyhtml %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/email.txt.twig");
    }
}
