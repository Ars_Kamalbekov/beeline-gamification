<?php

/* SonataAdminBundle:CRUD:delete.html.twig */
class TwigTemplate561a453e428fc864ffb44a047ac0b6c7a826013d59174dcb392698fec1deaa15 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'tabmenu' => array($this, 'blocktabmenu'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:delete.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb5baa9ce0ab012ea2157508650b01c0c1fb69222b8c4ac648d5fc4874d8a901a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb5baa9ce0ab012ea2157508650b01c0c1fb69222b8c4ac648d5fc4874d8a901a->enter($internalb5baa9ce0ab012ea2157508650b01c0c1fb69222b8c4ac648d5fc4874d8a901aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $internalc4053cceca5604a384abcfee012a32b1b1865c86773b2687a308920ee6a5b3e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc4053cceca5604a384abcfee012a32b1b1865c86773b2687a308920ee6a5b3e6->enter($internalc4053cceca5604a384abcfee012a32b1b1865c86773b2687a308920ee6a5b3e6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb5baa9ce0ab012ea2157508650b01c0c1fb69222b8c4ac648d5fc4874d8a901a->leave($internalb5baa9ce0ab012ea2157508650b01c0c1fb69222b8c4ac648d5fc4874d8a901aprof);

        
        $internalc4053cceca5604a384abcfee012a32b1b1865c86773b2687a308920ee6a5b3e6->leave($internalc4053cceca5604a384abcfee012a32b1b1865c86773b2687a308920ee6a5b3e6prof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internal446a99e24b520da0be4e6fde0378908d8b7fc9afb933d0356623e8c276af98c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal446a99e24b520da0be4e6fde0378908d8b7fc9afb933d0356623e8c276af98c6->enter($internal446a99e24b520da0be4e6fde0378908d8b7fc9afb933d0356623e8c276af98c6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal06f7b22ee535974394e5be28f881df66dff1243e9ffefc8a7ed8bac28048c10f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal06f7b22ee535974394e5be28f881df66dff1243e9ffefc8a7ed8bac28048c10f->enter($internal06f7b22ee535974394e5be28f881df66dff1243e9ffefc8a7ed8bac28048c10fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:delete.html.twig", 15)->display($context);
        
        $internal06f7b22ee535974394e5be28f881df66dff1243e9ffefc8a7ed8bac28048c10f->leave($internal06f7b22ee535974394e5be28f881df66dff1243e9ffefc8a7ed8bac28048c10fprof);

        
        $internal446a99e24b520da0be4e6fde0378908d8b7fc9afb933d0356623e8c276af98c6->leave($internal446a99e24b520da0be4e6fde0378908d8b7fc9afb933d0356623e8c276af98c6prof);

    }

    // line 18
    public function blocktabmenu($context, array $blocks = array())
    {
        $internal08e41327b606f38be403e157c06cff5239babc7915c2205283cbbed277deabf4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal08e41327b606f38be403e157c06cff5239babc7915c2205283cbbed277deabf4->enter($internal08e41327b606f38be403e157c06cff5239babc7915c2205283cbbed277deabf4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal21cd6e1ccca776da4a55138f6858a47692efb7661fe041289142fe26cea7b2bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal21cd6e1ccca776da4a55138f6858a47692efb7661fe041289142fe26cea7b2bb->enter($internal21cd6e1ccca776da4a55138f6858a47692efb7661fe041289142fe26cea7b2bbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 18, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 18, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
        
        $internal21cd6e1ccca776da4a55138f6858a47692efb7661fe041289142fe26cea7b2bb->leave($internal21cd6e1ccca776da4a55138f6858a47692efb7661fe041289142fe26cea7b2bbprof);

        
        $internal08e41327b606f38be403e157c06cff5239babc7915c2205283cbbed277deabf4->leave($internal08e41327b606f38be403e157c06cff5239babc7915c2205283cbbed277deabf4prof);

    }

    // line 20
    public function blockcontent($context, array $blocks = array())
    {
        $internalc0e8dbded9633101ae3f07025d033fa26752a4fbc462ed8d7759b55a5277ac36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc0e8dbded9633101ae3f07025d033fa26752a4fbc462ed8d7759b55a5277ac36->enter($internalc0e8dbded9633101ae3f07025d033fa26752a4fbc462ed8d7759b55a5277ac36prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internal556439950bdbe1be23575ac816fab4e3759a13e766b5beb077c89abc34ecf88a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal556439950bdbe1be23575ac816fab4e3759a13e766b5beb077c89abc34ecf88a->enter($internal556439950bdbe1be23575ac816fab4e3759a13e766b5beb077c89abc34ecf88aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">";
        // line 25
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titledelete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</h3>
            </div>
            <div class=\"box-body\">
                ";
        // line 28
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("messagedeleteconfirmation", array("%object%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "toString", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 28, $this->getSourceContext()); })())), "method")), "SonataAdminBundle"), "html", null, true);
        echo "
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"";
        // line 31
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 31, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"";
        // line 33
        echo twigescapefilter($this->env, (isset($context["csrftoken"]) || arraykeyexists("csrftoken", $context) ? $context["csrftoken"] : (function () { throw new TwigErrorRuntime('Variable "csrftoken" does not exist.', 33, $this->getSourceContext()); })()), "html", null, true);
        echo "\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> ";
        // line 35
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btndelete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</button>
                    ";
        // line 36
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 36, $this->getSourceContext()); })())), "method"))) {
            // line 37
            echo "                        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("deleteor", array(), "SonataAdminBundle"), "html", null, true);
            echo "

                        <a class=\"btn btn-success\" href=\"";
            // line 39
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 39, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 39, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            ";
            // line 41
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionedit", array(), "SonataAdminBundle"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 43
        echo "                </form>
            </div>
        </div>
    </div>
";
        
        $internal556439950bdbe1be23575ac816fab4e3759a13e766b5beb077c89abc34ecf88a->leave($internal556439950bdbe1be23575ac816fab4e3759a13e766b5beb077c89abc34ecf88aprof);

        
        $internalc0e8dbded9633101ae3f07025d033fa26752a4fbc462ed8d7759b55a5277ac36->leave($internalc0e8dbded9633101ae3f07025d033fa26752a4fbc462ed8d7759b55a5277ac36prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 43,  132 => 41,  127 => 39,  121 => 37,  119 => 36,  115 => 35,  110 => 33,  105 => 31,  99 => 28,  93 => 25,  87 => 21,  78 => 20,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}{{ knpmenurender(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')}, 'twig') }}{% endblock %}

{% block content %}
    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">{{ 'titledelete'|trans({}, 'SonataAdminBundle') }}</h3>
            </div>
            <div class=\"box-body\">
                {{ 'messagedeleteconfirmation'|trans({'%object%': admin.toString(object)}, 'SonataAdminBundle') }}
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"{{ admin.generateObjectUrl('delete', object) }}\">
                    <input type=\"hidden\" name=\"method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"{{ csrftoken }}\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> {{ 'btndelete'|trans({}, 'SonataAdminBundle') }}</button>
                    {% if admin.hasRoute('edit') and admin.hasAccess('edit', object) %}
                        {{ 'deleteor'|trans({}, 'SonataAdminBundle') }}

                        <a class=\"btn btn-success\" href=\"{{ admin.generateObjectUrl('edit', object) }}\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            {{ 'linkactionedit'|trans({}, 'SonataAdminBundle') }}</a>
                    {% endif %}
                </form>
            </div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:delete.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/delete.html.twig");
    }
}
