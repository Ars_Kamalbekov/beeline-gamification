<?php

/* @Framework/Form/formrow.html.php */
class TwigTemplate531f684bfcae03fbc1b05d8b1f5263e72313fcae81e1b23f903cfb066c8be1d5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal52662b2d60bdf537d481354080bc63b624edaa32ad24622039b22f637f5b0620 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal52662b2d60bdf537d481354080bc63b624edaa32ad24622039b22f637f5b0620->enter($internal52662b2d60bdf537d481354080bc63b624edaa32ad24622039b22f637f5b0620prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrow.html.php"));

        $internalb1e3523b7bf3c5fe1b6e9fd857c22355a8d2f02d49fa2b1a738d15d7c2e4dede = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb1e3523b7bf3c5fe1b6e9fd857c22355a8d2f02d49fa2b1a738d15d7c2e4dede->enter($internalb1e3523b7bf3c5fe1b6e9fd857c22355a8d2f02d49fa2b1a738d15d7c2e4dedeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrow.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $internal52662b2d60bdf537d481354080bc63b624edaa32ad24622039b22f637f5b0620->leave($internal52662b2d60bdf537d481354080bc63b624edaa32ad24622039b22f637f5b0620prof);

        
        $internalb1e3523b7bf3c5fe1b6e9fd857c22355a8d2f02d49fa2b1a738d15d7c2e4dede->leave($internalb1e3523b7bf3c5fe1b6e9fd857c22355a8d2f02d49fa2b1a738d15d7c2e4dedeprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/formrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formrow.html.php");
    }
}
