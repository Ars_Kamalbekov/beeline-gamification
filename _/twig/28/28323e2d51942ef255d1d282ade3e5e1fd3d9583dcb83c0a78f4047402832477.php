<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class TwigTemplate94a112557463dd337f294bdc6165983d78593b2a225fd5bb8857de40f2be3f81 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb4fe19c0244cf053dcfa8e331ce5f58255c350d97b03301d0e5bf9e50a55ec11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb4fe19c0244cf053dcfa8e331ce5f58255c350d97b03301d0e5bf9e50a55ec11->enter($internalb4fe19c0244cf053dcfa8e331ce5f58255c350d97b03301d0e5bf9e50a55ec11prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $internal66e46973116a051802c78f479aab4ec463a83239c5f8b4d77b88340d3079739b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal66e46973116a051802c78f479aab4ec463a83239c5f8b4d77b88340d3079739b->enter($internal66e46973116a051802c78f479aab4ec463a83239c5f8b4d77b88340d3079739bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb4fe19c0244cf053dcfa8e331ce5f58255c350d97b03301d0e5bf9e50a55ec11->leave($internalb4fe19c0244cf053dcfa8e331ce5f58255c350d97b03301d0e5bf9e50a55ec11prof);

        
        $internal66e46973116a051802c78f479aab4ec463a83239c5f8b4d77b88340d3079739b->leave($internal66e46973116a051802c78f479aab4ec463a83239c5f8b4d77b88340d3079739bprof);

    }

    // line 5
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal90904858fc695b9fe267669e651fc966af9cab73f27b8bf3851cd4ef9d876482 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal90904858fc695b9fe267669e651fc966af9cab73f27b8bf3851cd4ef9d876482->enter($internal90904858fc695b9fe267669e651fc966af9cab73f27b8bf3851cd4ef9d876482prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal6ddd1e2261bbfcd4aaa067a21201d831357fbdecb42fa036a7a13d6be266703f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6ddd1e2261bbfcd4aaa067a21201d831357fbdecb42fa036a7a13d6be266703f->enter($internal6ddd1e2261bbfcd4aaa067a21201d831357fbdecb42fa036a7a13d6be266703fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 6
        echo "    <p>";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) || arraykeyexists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new TwigErrorRuntime('Variable "targetUrl" does not exist.', 7, $this->getSourceContext()); })())) {
            // line 8
            echo "    <p><a href=\"";
            echo twigescapefilter($this->env, (isset($context["targetUrl"]) || arraykeyexists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new TwigErrorRuntime('Variable "targetUrl" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $internal6ddd1e2261bbfcd4aaa067a21201d831357fbdecb42fa036a7a13d6be266703f->leave($internal6ddd1e2261bbfcd4aaa067a21201d831357fbdecb42fa036a7a13d6be266703fprof);

        
        $internal90904858fc695b9fe267669e651fc966af9cab73f27b8bf3851cd4ef9d876482->leave($internal90904858fc695b9fe267669e651fc966af9cab73f27b8bf3851cd4ef9d876482prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% transdefaultdomain 'FOSUserBundle' %}

{% block fosusercontent %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fosusercontent %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Registration/confirmed.html.twig");
    }
}
