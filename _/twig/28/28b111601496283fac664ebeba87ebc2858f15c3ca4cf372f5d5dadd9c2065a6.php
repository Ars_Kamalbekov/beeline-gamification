<?php

/* SonataAdminBundle:CRUD:listhtml.html.twig */
class TwigTemplated660ab85376ea5dc805fc6d5a398232ae466a85dffeeb99f7ed2d41d99cb4b43 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 1, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listhtml.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal92ed2d366a2e936738a52b84f4a0b9f53712db1d37b07ada74bb8cc37d11c075 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal92ed2d366a2e936738a52b84f4a0b9f53712db1d37b07ada74bb8cc37d11c075->enter($internal92ed2d366a2e936738a52b84f4a0b9f53712db1d37b07ada74bb8cc37d11c075prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listhtml.html.twig"));

        $internala99d6e3f4eb9c4483da93c719a4a2585119c9dca3cd2a9916dd4d487d9e2076b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala99d6e3f4eb9c4483da93c719a4a2585119c9dca3cd2a9916dd4d487d9e2076b->enter($internala99d6e3f4eb9c4483da93c719a4a2585119c9dca3cd2a9916dd4d487d9e2076bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listhtml.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal92ed2d366a2e936738a52b84f4a0b9f53712db1d37b07ada74bb8cc37d11c075->leave($internal92ed2d366a2e936738a52b84f4a0b9f53712db1d37b07ada74bb8cc37d11c075prof);

        
        $internala99d6e3f4eb9c4483da93c719a4a2585119c9dca3cd2a9916dd4d487d9e2076b->leave($internala99d6e3f4eb9c4483da93c719a4a2585119c9dca3cd2a9916dd4d487d9e2076bprof);

    }

    // line 3
    public function blockfield($context, array $blocks = array())
    {
        $internalc7c957fc6c2337bf4cfacfca65399f9e4ad9082d1bc6a0a4388d2b803ac822fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc7c957fc6c2337bf4cfacfca65399f9e4ad9082d1bc6a0a4388d2b803ac822fd->enter($internalc7c957fc6c2337bf4cfacfca65399f9e4ad9082d1bc6a0a4388d2b803ac822fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal138410b75d97a44acf69a2c98bde21e0e12f11f8ae87c590b786c03fa783c4f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal138410b75d97a44acf69a2c98bde21e0e12f11f8ae87c590b786c03fa783c4f9->enter($internal138410b75d97a44acf69a2c98bde21e0e12f11f8ae87c590b786c03fa783c4f9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 4
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 4, $this->getSourceContext()); })()))) {
            // line 5
            echo "&nbsp;
    ";
        } else {
            // line 7
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "truncate", array(), "any", true, true)) {
                // line 8
                $context["truncate"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 8, $this->getSourceContext()); })()), "options", array()), "truncate", array());
                // line 9
                echo "            ";
                $context["length"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "length", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "length", array()), 30)) : (30));
                // line 10
                echo "            ";
                $context["preserve"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "preserve", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "preserve", array()), false)) : (false));
                // line 11
                echo "            ";
                $context["separator"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "separator", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["truncate"] ?? null), "separator", array()), "...")) : ("..."));
                // line 12
                echo "            ";
                echo twigtruncatefilter($this->env, striptags((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 12, $this->getSourceContext()); })())), (isset($context["length"]) || arraykeyexists("length", $context) ? $context["length"] : (function () { throw new TwigErrorRuntime('Variable "length" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["preserve"]) || arraykeyexists("preserve", $context) ? $context["preserve"] : (function () { throw new TwigErrorRuntime('Variable "preserve" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["separator"]) || arraykeyexists("separator", $context) ? $context["separator"] : (function () { throw new TwigErrorRuntime('Variable "separator" does not exist.', 12, $this->getSourceContext()); })()));
            } else {
                // line 14
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "strip", array(), "any", true, true)) {
                    // line 15
                    $context["value"] = striptags((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()));
                }
                // line 17
                echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })());
                echo "
        ";
            }
            // line 19
            echo "    ";
        }
        
        $internal138410b75d97a44acf69a2c98bde21e0e12f11f8ae87c590b786c03fa783c4f9->leave($internal138410b75d97a44acf69a2c98bde21e0e12f11f8ae87c590b786c03fa783c4f9prof);

        
        $internalc7c957fc6c2337bf4cfacfca65399f9e4ad9082d1bc6a0a4388d2b803ac822fd->leave($internalc7c957fc6c2337bf4cfacfca65399f9e4ad9082d1bc6a0a4388d2b803ac822fdprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listhtml.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 19,  76 => 17,  73 => 15,  71 => 14,  67 => 12,  64 => 11,  61 => 10,  58 => 9,  56 => 8,  54 => 7,  50 => 5,  48 => 4,  39 => 3,  18 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {% else %}
        {%- if fielddescription.options.truncate is defined -%}
            {% set truncate = fielddescription.options.truncate %}
            {% set length = truncate.length|default(30) %}
            {% set preserve = truncate.preserve|default(false) %}
            {% set separator = truncate.separator|default('...') %}
            {{ value|striptags|truncate(length, preserve, separator)|raw }}
        {%- else -%}
            {%- if fielddescription.options.strip is defined -%}
                {% set value = value|striptags %}
            {%- endif -%}
            {{ value|raw }}
        {% endif %}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:listhtml.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listhtml.html.twig");
    }
}
