<?php

/* FOSUserBundle:Group:show.html.twig */
class TwigTemplate85422e3d43b6c27373e66a807053f1005b221eacdb0234ffca6b62bd9abd1b65 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9ecd22c38ea48bcaaf57df6a1d8c3b29a1f32abe86ceb9d0cd34dc408f6795d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9ecd22c38ea48bcaaf57df6a1d8c3b29a1f32abe86ceb9d0cd34dc408f6795d8->enter($internal9ecd22c38ea48bcaaf57df6a1d8c3b29a1f32abe86ceb9d0cd34dc408f6795d8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $internal98412754a59a9b77b63b0df6136fbebc44160896723f7c7d0a64654c067c43b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal98412754a59a9b77b63b0df6136fbebc44160896723f7c7d0a64654c067c43b0->enter($internal98412754a59a9b77b63b0df6136fbebc44160896723f7c7d0a64654c067c43b0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal9ecd22c38ea48bcaaf57df6a1d8c3b29a1f32abe86ceb9d0cd34dc408f6795d8->leave($internal9ecd22c38ea48bcaaf57df6a1d8c3b29a1f32abe86ceb9d0cd34dc408f6795d8prof);

        
        $internal98412754a59a9b77b63b0df6136fbebc44160896723f7c7d0a64654c067c43b0->leave($internal98412754a59a9b77b63b0df6136fbebc44160896723f7c7d0a64654c067c43b0prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internala4a1590db15c69d8dc3fb9c15903b88844894d2fec791c62c8122d1b0495ac6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala4a1590db15c69d8dc3fb9c15903b88844894d2fec791c62c8122d1b0495ac6c->enter($internala4a1590db15c69d8dc3fb9c15903b88844894d2fec791c62c8122d1b0495ac6cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal680c8030ef57bc37257bd0f7ff3280e211d2568b2b7f34ff0f9e727bc0e27ca3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal680c8030ef57bc37257bd0f7ff3280e211d2568b2b7f34ff0f9e727bc0e27ca3->enter($internal680c8030ef57bc37257bd0f7ff3280e211d2568b2b7f34ff0f9e727bc0e27ca3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/showcontent.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $internal680c8030ef57bc37257bd0f7ff3280e211d2568b2b7f34ff0f9e727bc0e27ca3->leave($internal680c8030ef57bc37257bd0f7ff3280e211d2568b2b7f34ff0f9e727bc0e27ca3prof);

        
        $internala4a1590db15c69d8dc3fb9c15903b88844894d2fec791c62c8122d1b0495ac6c->leave($internala4a1590db15c69d8dc3fb9c15903b88844894d2fec791c62c8122d1b0495ac6cprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Group/showcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Group:show.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}
