<?php

/* SonataAdminBundle:Menu:sonatamenu.html.twig */
class TwigTemplate441ed11e6775c1b9b8c1e7f800524e1996c2fc02893a52c32def3b4c635c8a42 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Menu:sonatamenu.html.twig", 1);
        $this->blocks = array(
            'root' => array($this, 'blockroot'),
            'item' => array($this, 'blockitem'),
            'linkElement' => array($this, 'blocklinkElement'),
            'spanElement' => array($this, 'blockspanElement'),
            'label' => array($this, 'blocklabel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knpmenu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc993e87fa744645d0e4467c7556993fcfdefea37c15ae1ccf740ab3d656052c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc993e87fa744645d0e4467c7556993fcfdefea37c15ae1ccf740ab3d656052c8->enter($internalc993e87fa744645d0e4467c7556993fcfdefea37c15ae1ccf740ab3d656052c8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Menu:sonatamenu.html.twig"));

        $internal0aaa0deb71f1de8848453179c7a91795e0918574a4a81cb0809cbd23bf76cde9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0aaa0deb71f1de8848453179c7a91795e0918574a4a81cb0809cbd23bf76cde9->enter($internal0aaa0deb71f1de8848453179c7a91795e0918574a4a81cb0809cbd23bf76cde9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Menu:sonatamenu.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc993e87fa744645d0e4467c7556993fcfdefea37c15ae1ccf740ab3d656052c8->leave($internalc993e87fa744645d0e4467c7556993fcfdefea37c15ae1ccf740ab3d656052c8prof);

        
        $internal0aaa0deb71f1de8848453179c7a91795e0918574a4a81cb0809cbd23bf76cde9->leave($internal0aaa0deb71f1de8848453179c7a91795e0918574a4a81cb0809cbd23bf76cde9prof);

    }

    // line 3
    public function blockroot($context, array $blocks = array())
    {
        $internalbb441c7f378ecdcc45a1ad671388449c30c0ced313a1937e011f9f54acfc7318 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbb441c7f378ecdcc45a1ad671388449c30c0ced313a1937e011f9f54acfc7318->enter($internalbb441c7f378ecdcc45a1ad671388449c30c0ced313a1937e011f9f54acfc7318prof = new TwigProfilerProfile($this->getTemplateName(), "block", "root"));

        $internal12a78d92188652b3a7a4e58907152e001346eaab551dd42ce48f0a840d86c7de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal12a78d92188652b3a7a4e58907152e001346eaab551dd42ce48f0a840d86c7de->enter($internal12a78d92188652b3a7a4e58907152e001346eaab551dd42ce48f0a840d86c7deprof = new TwigProfilerProfile($this->getTemplateName(), "block", "root"));

        // line 4
        $context["listAttributes"] = twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 4, $this->getSourceContext()); })()), "childrenAttributes", array()), array("class" => "sidebar-menu"));
        // line 5
        $context["request"] = ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 5, $this->getSourceContext()); })()), "extra", array(0 => "request"), "method")) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 5, $this->getSourceContext()); })()), "extra", array(0 => "request"), "method")) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 5, $this->getSourceContext()); })()), "request", array())));
        // line 6
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
        
        $internal12a78d92188652b3a7a4e58907152e001346eaab551dd42ce48f0a840d86c7de->leave($internal12a78d92188652b3a7a4e58907152e001346eaab551dd42ce48f0a840d86c7deprof);

        
        $internalbb441c7f378ecdcc45a1ad671388449c30c0ced313a1937e011f9f54acfc7318->leave($internalbb441c7f378ecdcc45a1ad671388449c30c0ced313a1937e011f9f54acfc7318prof);

    }

    // line 9
    public function blockitem($context, array $blocks = array())
    {
        $internala30fc78126ca75203771ad655c7d59e75875ee169392c27c4a492b9064105325 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala30fc78126ca75203771ad655c7d59e75875ee169392c27c4a492b9064105325->enter($internala30fc78126ca75203771ad655c7d59e75875ee169392c27c4a492b9064105325prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        $internald76b777c378923eca67be5728a473f9db86f72026860886cd21cb0a8b1fcb541 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald76b777c378923eca67be5728a473f9db86f72026860886cd21cb0a8b1fcb541->enter($internald76b777c378923eca67be5728a473f9db86f72026860886cd21cb0a8b1fcb541prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        // line 10
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 10, $this->getSourceContext()); })()), "displayed", array())) {
            // line 12
            $context["display"] = (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 12, $this->getSourceContext()); })()), "extra", array(0 => "roles"), "method")) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLESUPERADMIN"));
            // line 13
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 13, $this->getSourceContext()); })()), "extra", array(0 => "roles"), "method"));
            foreach ($context['seq'] as $context["key"] => $context["role"]) {
                if ( !(isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 13, $this->getSourceContext()); })())) {
                    // line 14
                    $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['role'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        }
        // line 18
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 18, $this->getSourceContext()); })()), "displayed", array()) && ((arraykeyexists("display", $context)) ? (twigdefaultfilter((isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 18, $this->getSourceContext()); })()))) : ("")))) {
            // line 19
            echo "        ";
            $context["options"] = twigarraymerge((isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 19, $this->getSourceContext()); })()), array("branchclass" => "treeview", "currentClass" => "active"));
            // line 20
            twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 20, $this->getSourceContext()); })()), "setChildrenAttribute", array(0 => "class", 1 => twigtrimfilter((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 20, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method") . " active"))), "method");
            // line 21
            twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 21, $this->getSourceContext()); })()), "setChildrenAttribute", array(0 => "class", 1 => twigtrimfilter((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 21, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method") . " treeview-menu"))), "method");
            // line 22
            echo "        ";
            $this->displayParentBlock("item", $context, $blocks);
            echo "
    ";
        }
        
        $internald76b777c378923eca67be5728a473f9db86f72026860886cd21cb0a8b1fcb541->leave($internald76b777c378923eca67be5728a473f9db86f72026860886cd21cb0a8b1fcb541prof);

        
        $internala30fc78126ca75203771ad655c7d59e75875ee169392c27c4a492b9064105325->leave($internala30fc78126ca75203771ad655c7d59e75875ee169392c27c4a492b9064105325prof);

    }

    // line 26
    public function blocklinkElement($context, array $blocks = array())
    {
        $internal220e5cdf9c2924eaa7f2d0f5906e12ab8207c8ac53dd7c77041f8c4027df9d43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal220e5cdf9c2924eaa7f2d0f5906e12ab8207c8ac53dd7c77041f8c4027df9d43->enter($internal220e5cdf9c2924eaa7f2d0f5906e12ab8207c8ac53dd7c77041f8c4027df9d43prof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        $internal1393f447c7b73f23075447e6b7c14f892fe12509b7a5d2ed686bf4eb224403ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1393f447c7b73f23075447e6b7c14f892fe12509b7a5d2ed686bf4eb224403ff->enter($internal1393f447c7b73f23075447e6b7c14f892fe12509b7a5d2ed686bf4eb224403ffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        // line 27
        echo "    ";
        obstart();
        // line 28
        echo "        ";
        $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 28, $this->getSourceContext()); })()), "extra", array(0 => "labelcatalogue", 1 => "messages"), "method");
        // line 29
        echo "        ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "ontop"), "method", true, true) &&  !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 29, $this->getSourceContext()); })()), "extra", array(0 => "ontop"), "method"))) {
            // line 30
            echo "            ";
            $context["icon"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "icon"), "method", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "icon"), "method"), (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 30, $this->getSourceContext()); })()), "level", array()) > 1)) ? ("<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>") : ("")))) : ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 30, $this->getSourceContext()); })()), "level", array()) > 1)) ? ("<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>") : (""))));
            // line 31
            echo "        ";
        } else {
            // line 32
            echo "            ";
            $context["icon"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 32, $this->getSourceContext()); })()), "extra", array(0 => "icon"), "method");
            // line 33
            echo "        ";
        }
        // line 34
        echo "        ";
        $context["islink"] = true;
        // line 35
        echo "        ";
        $this->displayParentBlock("linkElement", $context, $blocks);
        echo "
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal1393f447c7b73f23075447e6b7c14f892fe12509b7a5d2ed686bf4eb224403ff->leave($internal1393f447c7b73f23075447e6b7c14f892fe12509b7a5d2ed686bf4eb224403ffprof);

        
        $internal220e5cdf9c2924eaa7f2d0f5906e12ab8207c8ac53dd7c77041f8c4027df9d43->leave($internal220e5cdf9c2924eaa7f2d0f5906e12ab8207c8ac53dd7c77041f8c4027df9d43prof);

    }

    // line 39
    public function blockspanElement($context, array $blocks = array())
    {
        $internal1a0321b4e9eea1ce77e937c7a86cd40675ded8820013ea145753cb8df888eea3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1a0321b4e9eea1ce77e937c7a86cd40675ded8820013ea145753cb8df888eea3->enter($internal1a0321b4e9eea1ce77e937c7a86cd40675ded8820013ea145753cb8df888eea3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        $internal2e4758503cba09ffef98e4709a04700e526809b36a8b40171051bea78ee089c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2e4758503cba09ffef98e4709a04700e526809b36a8b40171051bea78ee089c7->enter($internal2e4758503cba09ffef98e4709a04700e526809b36a8b40171051bea78ee089c7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        // line 40
        echo "    ";
        obstart();
        // line 41
        echo "        <a href=\"#\">
            ";
        // line 42
        $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 42, $this->getSourceContext()); })()), "extra", array(0 => "labelcatalogue"), "method");
        // line 43
        echo "            ";
        $context["icon"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "icon"), "method", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "icon"), "method"), "")) : (""));
        // line 44
        echo "            ";
        echo (isset($context["icon"]) || arraykeyexists("icon", $context) ? $context["icon"] : (function () { throw new TwigErrorRuntime('Variable "icon" does not exist.', 44, $this->getSourceContext()); })());
        echo "
            ";
        // line 45
        $this->displayParentBlock("spanElement", $context, $blocks);
        // line 46
        if (( !twiggetattribute($this->env, $this->getSourceContext(), ($context["item"] ?? null), "extra", array(0 => "keepopen"), "method", true, true) ||  !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 46, $this->getSourceContext()); })()), "extra", array(0 => "keepopen"), "method"))) {
            // line 47
            echo "<span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span>";
        }
        // line 49
        echo "</a>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal2e4758503cba09ffef98e4709a04700e526809b36a8b40171051bea78ee089c7->leave($internal2e4758503cba09ffef98e4709a04700e526809b36a8b40171051bea78ee089c7prof);

        
        $internal1a0321b4e9eea1ce77e937c7a86cd40675ded8820013ea145753cb8df888eea3->leave($internal1a0321b4e9eea1ce77e937c7a86cd40675ded8820013ea145753cb8df888eea3prof);

    }

    // line 53
    public function blocklabel($context, array $blocks = array())
    {
        $internal6b5b7bf2c009c40b05a302b2c80e2c95483605632bba04a1229f4713cd336b9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6b5b7bf2c009c40b05a302b2c80e2c95483605632bba04a1229f4713cd336b9d->enter($internal6b5b7bf2c009c40b05a302b2c80e2c95483605632bba04a1229f4713cd336b9dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internaleb9c53a584ce738534995b3118533e43ea7d225e375cbfe271844f4ab12ee535 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaleb9c53a584ce738534995b3118533e43ea7d225e375cbfe271844f4ab12ee535->enter($internaleb9c53a584ce738534995b3118533e43ea7d225e375cbfe271844f4ab12ee535prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        if ((arraykeyexists("islink", $context) && (isset($context["islink"]) || arraykeyexists("islink", $context) ? $context["islink"] : (function () { throw new TwigErrorRuntime('Variable "islink" does not exist.', 53, $this->getSourceContext()); })()))) {
            echo ((arraykeyexists("icon", $context)) ? (twigdefaultfilter((isset($context["icon"]) || arraykeyexists("icon", $context) ? $context["icon"] : (function () { throw new TwigErrorRuntime('Variable "icon" does not exist.', 53, $this->getSourceContext()); })()))) : (""));
        }
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 53, $this->getSourceContext()); })()), "allowsafelabels", array()) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 53, $this->getSourceContext()); })()), "extra", array(0 => "safelabel", 1 => false), "method"))) {
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 53, $this->getSourceContext()); })()), "label", array());
        } else {
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 53, $this->getSourceContext()); })()), "label", array()), array(), ((arraykeyexists("translationdomain", $context)) ? (twigdefaultfilter((isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 53, $this->getSourceContext()); })()), "messages")) : ("messages"))), "html", null, true);
        }
        
        $internaleb9c53a584ce738534995b3118533e43ea7d225e375cbfe271844f4ab12ee535->leave($internaleb9c53a584ce738534995b3118533e43ea7d225e375cbfe271844f4ab12ee535prof);

        
        $internal6b5b7bf2c009c40b05a302b2c80e2c95483605632bba04a1229f4713cd336b9d->leave($internal6b5b7bf2c009c40b05a302b2c80e2c95483605632bba04a1229f4713cd336b9dprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Menu:sonatamenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 53,  196 => 49,  193 => 47,  191 => 46,  189 => 45,  184 => 44,  181 => 43,  179 => 42,  176 => 41,  173 => 40,  164 => 39,  150 => 35,  147 => 34,  144 => 33,  141 => 32,  138 => 31,  135 => 30,  132 => 29,  129 => 28,  126 => 27,  117 => 26,  103 => 22,  101 => 21,  99 => 20,  96 => 19,  94 => 18,  86 => 14,  81 => 13,  79 => 12,  77 => 10,  68 => 9,  57 => 6,  55 => 5,  53 => 4,  44 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'knpmenu.html.twig' %}

{% block root %}
    {%- set listAttributes = item.childrenAttributes|merge({'class': 'sidebar-menu'}) %}
    {%- set request        = item.extra('request') ?: app.request %}
    {{ block('list') -}}
{% endblock %}

{% block item %}
    {%- if item.displayed %}
        {#- check role of the group #}
        {%- set display = (item.extra('roles') is empty or isgranted('ROLESUPERADMIN') ) %}
        {%- for role in item.extra('roles') if not display %}
            {%- set display = isgranted(role) %}
        {%- endfor %}
    {%- endif %}

    {%- if item.displayed and display|default %}
        {% set options = options|merge({branchclass: 'treeview', currentClass: \"active\"}) %}
        {%- do item.setChildrenAttribute('class', (item.childrenAttribute('class')~' active')|trim) %}
        {%- do item.setChildrenAttribute('class', (item.childrenAttribute('class')~' treeview-menu')|trim) %}
        {{ parent() }}
    {% endif %}
{% endblock %}

{% block linkElement %}
    {% spaceless %}
        {% set translationdomain = item.extra('labelcatalogue', 'messages') %}
        {% if item.extra('ontop') is defined and not item.extra('ontop') %}
            {% set icon = item.extra('icon')|default(item.level > 1 ? '<i class=\"fa fa-angle-double-right\" aria-hidden=\"true\"></i>' : '') %}
        {% else %}
            {% set icon = item.extra('icon') %}
        {% endif %}
        {% set islink = true %}
        {{ parent() }}
    {% endspaceless %}
{% endblock %}

{% block spanElement %}
    {% spaceless %}
        <a href=\"#\">
            {% set translationdomain = item.extra('labelcatalogue') %}
            {% set icon = item.extra('icon')|default('') %}
            {{ icon|raw }}
            {{ parent() }}
            {%- if item.extra('keepopen') is not defined or not item.extra('keepopen') -%}
                <span class=\"pull-right-container\"><i class=\"fa pull-right fa-angle-left\"></i></span>
            {%- endif -%}
        </a>
    {% endspaceless %}
{% endblock %}

{% block label %}{% if islink is defined and islink %}{{ icon|default|raw }}{% endif %}{% if options.allowsafelabels and item.extra('safelabel', false) %}{{ item.label|raw }}{% else %}{{ item.label|trans({}, translationdomain|default('messages')) }}{% endif %}{% endblock %}
", "SonataAdminBundle:Menu:sonatamenu.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Menu/sonatamenu.html.twig");
    }
}
