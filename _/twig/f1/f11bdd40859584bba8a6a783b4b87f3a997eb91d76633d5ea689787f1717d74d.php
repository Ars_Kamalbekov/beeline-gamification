<?php

/* @Framework/Form/formstart.html.php */
class TwigTemplatee899943aad4efb8462de9cfea342d74b2cac1994baf01b2d5bfb5b91b46e1246 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal53c4076d7f7dd45e5599aee0f559c4f5daca95d9acce2a0cfd0dd69d9402188b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal53c4076d7f7dd45e5599aee0f559c4f5daca95d9acce2a0cfd0dd69d9402188b->enter($internal53c4076d7f7dd45e5599aee0f559c4f5daca95d9acce2a0cfd0dd69d9402188bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formstart.html.php"));

        $internalc6caa6da0d1e4c130d0b1c7f79270f71109e65d13430050abe17d82b9d32eece = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc6caa6da0d1e4c130d0b1c7f79270f71109e65d13430050abe17d82b9d32eece->enter($internalc6caa6da0d1e4c130d0b1c7f79270f71109e65d13430050abe17d82b9d32eeceprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formstart.html.php"));

        // line 1
        echo "<?php \$method = strtoupper(\$method) ?>
<?php \$formmethod = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$formmethod) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$formmethod !== \$method): ?>
    <input type=\"hidden\" name=\"method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
";
        
        $internal53c4076d7f7dd45e5599aee0f559c4f5daca95d9acce2a0cfd0dd69d9402188b->leave($internal53c4076d7f7dd45e5599aee0f559c4f5daca95d9acce2a0cfd0dd69d9402188bprof);

        
        $internalc6caa6da0d1e4c130d0b1c7f79270f71109e65d13430050abe17d82b9d32eece->leave($internalc6caa6da0d1e4c130d0b1c7f79270f71109e65d13430050abe17d82b9d32eeceprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formstart.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php \$method = strtoupper(\$method) ?>
<?php \$formmethod = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$formmethod) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$formmethod !== \$method): ?>
    <input type=\"hidden\" name=\"method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
", "@Framework/Form/formstart.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formstart.html.php");
    }
}
