<?php

/* SonataAdminBundle:Button:listbutton.html.twig */
class TwigTemplatef5f7d021133a4901e03a921f853584177368f63adfe411d7bc126d6b5cdaa194 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal536a9e7dbeb276675ccc33341d33976540c61056a62de6ef4fb5be5982266506 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal536a9e7dbeb276675ccc33341d33976540c61056a62de6ef4fb5be5982266506->enter($internal536a9e7dbeb276675ccc33341d33976540c61056a62de6ef4fb5be5982266506prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:listbutton.html.twig"));

        $internal1d30bbc58d63e9026d15349d521d99c36f3b62d5b558ec9bc1e51cae23bd9a63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1d30bbc58d63e9026d15349d521d99c36f3b62d5b558ec9bc1e51cae23bd9a63->enter($internal1d30bbc58d63e9026d15349d521d99c36f3b62d5b558ec9bc1e51cae23bd9a63prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:listbutton.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasAccess", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "list"), "method"))) {
            // line 13
            echo "    <li>
        <a class=\"sonata-action-element\" href=\"";
            // line 14
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
            echo "\">
            <i class=\"fa fa-list\" aria-hidden=\"true\"></i>
            ";
            // line 16
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionlist", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </a>
    </li>
";
        }
        
        $internal536a9e7dbeb276675ccc33341d33976540c61056a62de6ef4fb5be5982266506->leave($internal536a9e7dbeb276675ccc33341d33976540c61056a62de6ef4fb5be5982266506prof);

        
        $internal1d30bbc58d63e9026d15349d521d99c36f3b62d5b558ec9bc1e51cae23bd9a63->leave($internal1d30bbc58d63e9026d15349d521d99c36f3b62d5b558ec9bc1e51cae23bd9a63prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Button:listbutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 16,  33 => 14,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.hasAccess('list') and admin.hasRoute('list') %}
    <li>
        <a class=\"sonata-action-element\" href=\"{{ admin.generateUrl('list') }}\">
            <i class=\"fa fa-list\" aria-hidden=\"true\"></i>
            {{ 'linkactionlist'|trans({}, 'SonataAdminBundle') }}
        </a>
    </li>
{% endif %}
", "SonataAdminBundle:Button:listbutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Button/listbutton.html.twig");
    }
}
