<?php

/* SonataAdminBundle:Form:filteradminfields.html.twig */
class TwigTemplate3cd8dc7d0106c2f5856e51281927e4248e3a9bcd299f540fcce92fcbbb486b05 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("formdivlayout.html.twig", "SonataAdminBundle:Form:filteradminfields.html.twig", 12);
        $this->blocks = array(
            'choicewidgetcollapsed' => array($this, 'blockchoicewidgetcollapsed'),
            'textareawidget' => array($this, 'blocktextareawidget'),
            'formwidgetsimple' => array($this, 'blockformwidgetsimple'),
            'choicewidgetexpanded' => array($this, 'blockchoicewidgetexpanded'),
            'checkboxwidget' => array($this, 'blockcheckboxwidget'),
            'sonatatypemodelautocompletewidget' => array($this, 'blocksonatatypemodelautocompletewidget'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "formdivlayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf9797da6be9b06debf06d06b501235bad59305ad09d5596fd3403e567bea63c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf9797da6be9b06debf06d06b501235bad59305ad09d5596fd3403e567bea63c7->enter($internalf9797da6be9b06debf06d06b501235bad59305ad09d5596fd3403e567bea63c7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:filteradminfields.html.twig"));

        $internal0b14965699a36d73a4110a06b7cfe4da3b0f8d0f0a9a659a6201c29193411130 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0b14965699a36d73a4110a06b7cfe4da3b0f8d0f0a9a659a6201c29193411130->enter($internal0b14965699a36d73a4110a06b7cfe4da3b0f8d0f0a9a659a6201c29193411130prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:filteradminfields.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf9797da6be9b06debf06d06b501235bad59305ad09d5596fd3403e567bea63c7->leave($internalf9797da6be9b06debf06d06b501235bad59305ad09d5596fd3403e567bea63c7prof);

        
        $internal0b14965699a36d73a4110a06b7cfe4da3b0f8d0f0a9a659a6201c29193411130->leave($internal0b14965699a36d73a4110a06b7cfe4da3b0f8d0f0a9a659a6201c29193411130prof);

    }

    // line 18
    public function blockchoicewidgetcollapsed($context, array $blocks = array())
    {
        $internal7dd90ad81e39b6aa9f851797b8bec5c2f843cf6df3d3fe626eb7d39092fe647a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7dd90ad81e39b6aa9f851797b8bec5c2f843cf6df3d3fe626eb7d39092fe647a->enter($internal7dd90ad81e39b6aa9f851797b8bec5c2f843cf6df3d3fe626eb7d39092fe647aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetcollapsed"));

        $internal1d360cd7ca82fa2e559281a889979e3418cdd74c9b19e819a7d4679ebc1dc299 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1d360cd7ca82fa2e559281a889979e3418cdd74c9b19e819a7d4679ebc1dc299->enter($internal1d360cd7ca82fa2e559281a889979e3418cdd74c9b19e819a7d4679ebc1dc299prof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetcollapsed"));

        // line 19
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 19, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
        // line 20
        echo "    ";
        $this->displayParentBlock("choicewidgetcollapsed", $context, $blocks);
        echo "
";
        
        $internal1d360cd7ca82fa2e559281a889979e3418cdd74c9b19e819a7d4679ebc1dc299->leave($internal1d360cd7ca82fa2e559281a889979e3418cdd74c9b19e819a7d4679ebc1dc299prof);

        
        $internal7dd90ad81e39b6aa9f851797b8bec5c2f843cf6df3d3fe626eb7d39092fe647a->leave($internal7dd90ad81e39b6aa9f851797b8bec5c2f843cf6df3d3fe626eb7d39092fe647aprof);

    }

    // line 23
    public function blocktextareawidget($context, array $blocks = array())
    {
        $internald61b613a5ce575bd07f6dc199e785ac27e6a2245eac05d9c487e0ba9ee50b084 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald61b613a5ce575bd07f6dc199e785ac27e6a2245eac05d9c487e0ba9ee50b084->enter($internald61b613a5ce575bd07f6dc199e785ac27e6a2245eac05d9c487e0ba9ee50b084prof = new TwigProfilerProfile($this->getTemplateName(), "block", "textareawidget"));

        $internal14b300f63e5fc85fc8b65f24512f52decea5a0279ef923b7a743058bedb7331b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal14b300f63e5fc85fc8b65f24512f52decea5a0279ef923b7a743058bedb7331b->enter($internal14b300f63e5fc85fc8b65f24512f52decea5a0279ef923b7a743058bedb7331bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "textareawidget"));

        // line 24
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 24, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
        // line 25
        echo "    ";
        $this->displayParentBlock("textareawidget", $context, $blocks);
        echo "
";
        
        $internal14b300f63e5fc85fc8b65f24512f52decea5a0279ef923b7a743058bedb7331b->leave($internal14b300f63e5fc85fc8b65f24512f52decea5a0279ef923b7a743058bedb7331bprof);

        
        $internald61b613a5ce575bd07f6dc199e785ac27e6a2245eac05d9c487e0ba9ee50b084->leave($internald61b613a5ce575bd07f6dc199e785ac27e6a2245eac05d9c487e0ba9ee50b084prof);

    }

    // line 28
    public function blockformwidgetsimple($context, array $blocks = array())
    {
        $internalcbfa70b2a7e254cf7d9a369a69ead63732bc4c21a8d6885a48a0cbd50c85aa3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcbfa70b2a7e254cf7d9a369a69ead63732bc4c21a8d6885a48a0cbd50c85aa3a->enter($internalcbfa70b2a7e254cf7d9a369a69ead63732bc4c21a8d6885a48a0cbd50c85aa3aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidgetsimple"));

        $internala8f8a272f25afa7afc6d59478208eeb9c67826d4359416fd82ba315491336bd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala8f8a272f25afa7afc6d59478208eeb9c67826d4359416fd82ba315491336bd2->enter($internala8f8a272f25afa7afc6d59478208eeb9c67826d4359416fd82ba315491336bd2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidgetsimple"));

        // line 29
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 29, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
        // line 30
        echo "    ";
        $this->displayParentBlock("formwidgetsimple", $context, $blocks);
        echo "
";
        
        $internala8f8a272f25afa7afc6d59478208eeb9c67826d4359416fd82ba315491336bd2->leave($internala8f8a272f25afa7afc6d59478208eeb9c67826d4359416fd82ba315491336bd2prof);

        
        $internalcbfa70b2a7e254cf7d9a369a69ead63732bc4c21a8d6885a48a0cbd50c85aa3a->leave($internalcbfa70b2a7e254cf7d9a369a69ead63732bc4c21a8d6885a48a0cbd50c85aa3aprof);

    }

    // line 33
    public function blockchoicewidgetexpanded($context, array $blocks = array())
    {
        $internalfad71110d9bb80fd3b6ca163924ed5751196b82b0469482e709f85672bf45164 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfad71110d9bb80fd3b6ca163924ed5751196b82b0469482e709f85672bf45164->enter($internalfad71110d9bb80fd3b6ca163924ed5751196b82b0469482e709f85672bf45164prof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetexpanded"));

        $internal8882d33b6ccc6a6dab2c84b52b1132b690e88eae555998e6affcc6c086d57d13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8882d33b6ccc6a6dab2c84b52b1132b690e88eae555998e6affcc6c086d57d13->enter($internal8882d33b6ccc6a6dab2c84b52b1132b690e88eae555998e6affcc6c086d57d13prof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetexpanded"));

        // line 34
        echo "    ";
        obstart();
        // line 35
        echo "        ";
        $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 35, $this->getSourceContext()); })()), array("class" => ((twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array()), "")) : (""))));
        // line 36
        echo "        ";
        $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 36, $this->getSourceContext()); })()), array("class" => ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 36, $this->getSourceContext()); })()), "class", array()) . " ") . (((arraykeyexists("widgettype", $context) && ((isset($context["widgettype"]) || arraykeyexists("widgettype", $context) ? $context["widgettype"] : (function () { throw new TwigErrorRuntime('Variable "widgettype" does not exist.', 36, $this->getSourceContext()); })()) != ""))) ? ((((((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 36, $this->getSourceContext()); })())) ? ("checkbox") : ("radio")) . "-") . (isset($context["widgettype"]) || arraykeyexists("widgettype", $context) ? $context["widgettype"] : (function () { throw new TwigErrorRuntime('Variable "widgettype" does not exist.', 36, $this->getSourceContext()); })()))) : ("")))));
        // line 37
        echo "        ";
        if ((isset($context["expanded"]) || arraykeyexists("expanded", $context) ? $context["expanded"] : (function () { throw new TwigErrorRuntime('Variable "expanded" does not exist.', 37, $this->getSourceContext()); })())) {
            // line 38
            echo "            ";
            $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 38, $this->getSourceContext()); })()), array("class" => ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : (""))));
            // line 39
            echo "            <div ";
            $this->displayBlock("widgetcontainerattributes", $context, $blocks);
            echo ">
        ";
        }
        // line 41
        echo "        ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 41, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["child"]) {
            // line 42
            echo "            ";
            if ((arraykeyexists("widgettype", $context) && ((isset($context["widgettype"]) || arraykeyexists("widgettype", $context) ? $context["widgettype"] : (function () { throw new TwigErrorRuntime('Variable "widgettype" does not exist.', 42, $this->getSourceContext()); })()) != "inline"))) {
                // line 43
                echo "                <div class=\"";
                echo (((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 43, $this->getSourceContext()); })())) ? ("checkbox") : ("radio"));
                echo "\">
            ";
            }
            // line 45
            echo "            <label";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 45, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twigescapefilter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twigescapefilter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['attrname'], $context['attrvalue'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            echo ">
            ";
            // line 46
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("attr" => array("class" => ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "widgetclass", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "widgetclass", array()), "")) : ("")))));
            echo "
            ";
            // line 47
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "vars", array()), "label", array()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 47, $this->getSourceContext()); })())), "html", null, true);
            echo "
            </label>
            ";
            // line 49
            if ((arraykeyexists("widgettype", $context) && ((isset($context["widgettype"]) || arraykeyexists("widgettype", $context) ? $context["widgettype"] : (function () { throw new TwigErrorRuntime('Variable "widgettype" does not exist.', 49, $this->getSourceContext()); })()) != "inline"))) {
                // line 50
                echo "                </div>
            ";
            }
            // line 52
            echo "        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 53
        echo "        ";
        if (        $this->hasBlock("formmessage", $context, $blocks)) {
            // line 54
            echo "            ";
            $this->displayBlock("formmessage", $context, $blocks);
            echo "
        ";
        }
        // line 56
        echo "        ";
        if ((isset($context["expanded"]) || arraykeyexists("expanded", $context) ? $context["expanded"] : (function () { throw new TwigErrorRuntime('Variable "expanded" does not exist.', 56, $this->getSourceContext()); })())) {
            // line 57
            echo "            </div>
        ";
        }
        // line 59
        echo "    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal8882d33b6ccc6a6dab2c84b52b1132b690e88eae555998e6affcc6c086d57d13->leave($internal8882d33b6ccc6a6dab2c84b52b1132b690e88eae555998e6affcc6c086d57d13prof);

        
        $internalfad71110d9bb80fd3b6ca163924ed5751196b82b0469482e709f85672bf45164->leave($internalfad71110d9bb80fd3b6ca163924ed5751196b82b0469482e709f85672bf45164prof);

    }

    // line 62
    public function blockcheckboxwidget($context, array $blocks = array())
    {
        $internal47e1e975e80e2c854e1ffab1965b6dc46796fb03c75af9e6731a4c1bc2c445da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal47e1e975e80e2c854e1ffab1965b6dc46796fb03c75af9e6731a4c1bc2c445da->enter($internal47e1e975e80e2c854e1ffab1965b6dc46796fb03c75af9e6731a4c1bc2c445daprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxwidget"));

        $internal29adc3093e820b796fde0495b7de616b09ba188798d44543d58b6abaedfb29bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal29adc3093e820b796fde0495b7de616b09ba188798d44543d58b6abaedfb29bd->enter($internal29adc3093e820b796fde0495b7de616b09ba188798d44543d58b6abaedfb29bdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxwidget"));

        // line 63
        echo "    ";
        obstart();
        // line 64
        echo "        ";
        if (( !((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 64, $this->getSourceContext()); })()) === false) && twigtestempty((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 64, $this->getSourceContext()); })())))) {
            // line 65
            echo "            ";
            $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 65, $this->getSourceContext()); })()));
            // line 66
            echo "        ";
        }
        // line 67
        echo "        ";
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 67, $this->getSourceContext()); })()), "parent", array()) != null) && !twiginfilter("choice", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 67, $this->getSourceContext()); })()), "parent", array()), "vars", array()), "blockprefixes", array())))) {
            // line 68
            echo "            <div class=\"checkbox\">
        ";
        }
        // line 70
        echo "
        ";
        // line 71
        if ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 71, $this->getSourceContext()); })()), "parent", array()) != null) && !twiginfilter("choice", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 71, $this->getSourceContext()); })()), "parent", array()), "vars", array()), "blockprefixes", array()))) && arraykeyexists("labelrender", $context))) {
            // line 72
            echo "            <label class=\"";
            if ((arraykeyexists("inline", $context) && (isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 72, $this->getSourceContext()); })()))) {
                echo "checkbox-inline";
            }
            echo "\">
        ";
        }
        // line 74
        echo "
        <input type=\"checkbox\" ";
        // line 75
        $this->displayBlock("widgetattributes", $context, $blocks);
        if (arraykeyexists("value", $context)) {
            echo " value=\"";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 75, $this->getSourceContext()); })()), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) || arraykeyexists("checked", $context) ? $context["checked"] : (function () { throw new TwigErrorRuntime('Variable "checked" does not exist.', 75, $this->getSourceContext()); })())) {
            echo " checked=\"checked\"";
        }
        echo "/>
        ";
        // line 76
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 76, $this->getSourceContext()); })()), "parent", array()) != null) && !twiginfilter("choice", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 76, $this->getSourceContext()); })()), "parent", array()), "vars", array()), "blockprefixes", array())))) {
            // line 77
            echo "            ";
            if ((arraykeyexists("labelrender", $context) && twiginfilter((isset($context["widgetcheckboxlabel"]) || arraykeyexists("widgetcheckboxlabel", $context) ? $context["widgetcheckboxlabel"] : (function () { throw new TwigErrorRuntime('Variable "widgetcheckboxlabel" does not exist.', 77, $this->getSourceContext()); })()), array(0 => "both", 1 => "widget")))) {
                // line 78
                echo "                ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 78, $this->getSourceContext()); })()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 78, $this->getSourceContext()); })())), "html", null, true);
                echo "
                </label>
            ";
            }
            // line 81
            echo "        ";
        }
        // line 82
        echo "        ";
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 82, $this->getSourceContext()); })()), "parent", array()) != null) && !twiginfilter("choice", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 82, $this->getSourceContext()); })()), "parent", array()), "vars", array()), "blockprefixes", array())))) {
            // line 83
            echo "            </div>
            ";
            // line 84
            if (            $this->hasBlock("formmessage", $context, $blocks)) {
                // line 85
                echo "                ";
                $this->displayBlock("formmessage", $context, $blocks);
                echo "
            ";
            }
            // line 87
            echo "        ";
        }
        // line 88
        echo "    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal29adc3093e820b796fde0495b7de616b09ba188798d44543d58b6abaedfb29bd->leave($internal29adc3093e820b796fde0495b7de616b09ba188798d44543d58b6abaedfb29bdprof);

        
        $internal47e1e975e80e2c854e1ffab1965b6dc46796fb03c75af9e6731a4c1bc2c445da->leave($internal47e1e975e80e2c854e1ffab1965b6dc46796fb03c75af9e6731a4c1bc2c445daprof);

    }

    // line 91
    public function blocksonatatypemodelautocompletewidget($context, array $blocks = array())
    {
        $internal8e7c1bcf04dfab586b6a021c6a2ba8636cf38d929ed2f001be7a75a9f1e3c32f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8e7c1bcf04dfab586b6a021c6a2ba8636cf38d929ed2f001be7a75a9f1e3c32f->enter($internal8e7c1bcf04dfab586b6a021c6a2ba8636cf38d929ed2f001be7a75a9f1e3c32fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletewidget"));

        $internal8bf020b532bec709ef07298db7e7d1a8c3e50e83cc478b999915349f84e3b6b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8bf020b532bec709ef07298db7e7d1a8c3e50e83cc478b999915349f84e3b6b7->enter($internal8bf020b532bec709ef07298db7e7d1a8c3e50e83cc478b999915349f84e3b6b7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletewidget"));

        // line 92
        echo "    ";
        $this->loadTemplate((isset($context["template"]) || arraykeyexists("template", $context) ? $context["template"] : (function () { throw new TwigErrorRuntime('Variable "template" does not exist.', 92, $this->getSourceContext()); })()), "SonataAdminBundle:Form:filteradminfields.html.twig", 92)->display($context);
        
        $internal8bf020b532bec709ef07298db7e7d1a8c3e50e83cc478b999915349f84e3b6b7->leave($internal8bf020b532bec709ef07298db7e7d1a8c3e50e83cc478b999915349f84e3b6b7prof);

        
        $internal8e7c1bcf04dfab586b6a021c6a2ba8636cf38d929ed2f001be7a75a9f1e3c32f->leave($internal8e7c1bcf04dfab586b6a021c6a2ba8636cf38d929ed2f001be7a75a9f1e3c32fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Form:filteradminfields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  336 => 92,  327 => 91,  316 => 88,  313 => 87,  307 => 85,  305 => 84,  302 => 83,  299 => 82,  296 => 81,  289 => 78,  286 => 77,  284 => 76,  272 => 75,  269 => 74,  261 => 72,  259 => 71,  256 => 70,  252 => 68,  249 => 67,  246 => 66,  243 => 65,  240 => 64,  237 => 63,  228 => 62,  217 => 59,  213 => 57,  210 => 56,  204 => 54,  201 => 53,  195 => 52,  191 => 50,  189 => 49,  184 => 47,  180 => 46,  164 => 45,  158 => 43,  155 => 42,  150 => 41,  144 => 39,  141 => 38,  138 => 37,  135 => 36,  132 => 35,  129 => 34,  120 => 33,  107 => 30,  104 => 29,  95 => 28,  82 => 25,  79 => 24,  70 => 23,  57 => 20,  54 => 19,  45 => 18,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'formdivlayout.html.twig' %}

{#
    Inspired from MopaBootstrapBundle: https://github.com/phiamo/MopaBootstrapBundle
#}

{% block choicewidgetcollapsed %}
    {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {{ parent() }}
{% endblock choicewidgetcollapsed %}

{% block textareawidget %}
    {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {{ parent() }}
{% endblock textareawidget %}

{% block formwidgetsimple %}
    {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {{ parent() }}
{% endblock formwidgetsimple %}

{% block choicewidgetexpanded %}
    {% spaceless %}
        {% set labelattr = labelattr|merge({'class': (labelattr.class|default(''))}) %}
        {% set labelattr = labelattr|merge({'class': (labelattr.class ~ ' ' ~ (widgettype is defined and widgettype != '' ? (multiple ? 'checkbox' : 'radio') ~ '-' ~ widgettype : ''))}) %}
        {% if expanded %}
            {% set attr = attr|merge({'class': attr.class|default('')}) %}
            <div {{ block('widgetcontainerattributes') }}>
        {% endif %}
        {% for child in form %}
            {% if widgettype is defined and widgettype != 'inline' %}
                <div class=\"{{ multiple ? 'checkbox' : 'radio' }}\">
            {% endif %}
            <label{% for attrname, attrvalue in labelattr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{ formwidget(child, {'attr': {'class': attr.widgetclass|default('')}}) }}
            {{ child.vars.label|trans({}, translationdomain) }}
            </label>
            {% if widgettype is defined and widgettype != 'inline' %}
                </div>
            {% endif %}
        {% endfor %}
        {% if block('formmessage') is defined %}
            {{ block('formmessage') }}
        {% endif %}
        {% if expanded %}
            </div>
        {% endif %}
    {% endspaceless %}
{% endblock choicewidgetexpanded %}

{% block checkboxwidget %}
    {% spaceless %}
        {% if label is not same as(false) and label is empty %}
            {% set label = name|humanize %}
        {% endif %}
        {% if form.parent != null and 'choice' not in form.parent.vars.blockprefixes %}
            <div class=\"checkbox\">
        {% endif %}

        {% if form.parent != null and 'choice' not in form.parent.vars.blockprefixes and labelrender is defined %}
            <label class=\"{% if inline is defined and inline %}checkbox-inline{% endif %}\">
        {% endif %}

        <input type=\"checkbox\" {{ block('widgetattributes') }}{% if value is defined %} value=\"{{ value }}\"{% endif %}{% if checked %} checked=\"checked\"{% endif %}/>
        {% if form.parent != null and 'choice' not in form.parent.vars.blockprefixes %}
            {% if labelrender is defined and widgetcheckboxlabel in ['both', 'widget'] %}
                {{ label|trans({}, translationdomain) }}
                </label>
            {% endif %}
        {% endif %}
        {% if form.parent != null and 'choice' not in form.parent.vars.blockprefixes %}
            </div>
            {% if block('formmessage') is defined %}
                {{ block('formmessage') }}
            {% endif %}
        {% endif %}
    {% endspaceless %}
{% endblock checkboxwidget %}

{% block sonatatypemodelautocompletewidget %}
    {% include template %}
{% endblock sonatatypemodelautocompletewidget %}
", "SonataAdminBundle:Form:filteradminfields.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Form/filteradminfields.html.twig");
    }
}
