<?php

/* SonataAdminBundle:CRUD:baseacl.html.twig */
class TwigTemplate6e923e4c8bf04507e62a475e7ba9491cbeb6b966def0a2eb8d25fc6867649dd1 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'form' => array($this, 'blockform'),
            'formaclroles' => array($this, 'blockformaclroles'),
            'formaclusers' => array($this, 'blockformaclusers'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:baseacl.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal84176f0ec6700e53112043c99ebc769ca8649e82a305c1f4ff617ee131ebad80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal84176f0ec6700e53112043c99ebc769ca8649e82a305c1f4ff617ee131ebad80->enter($internal84176f0ec6700e53112043c99ebc769ca8649e82a305c1f4ff617ee131ebad80prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseacl.html.twig"));

        $internal37078a069ca9be0b8be5a02b4610dbf9bd6dbd3857ff81640d6150fcd676d5d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal37078a069ca9be0b8be5a02b4610dbf9bd6dbd3857ff81640d6150fcd676d5d5->enter($internal37078a069ca9be0b8be5a02b4610dbf9bd6dbd3857ff81640d6150fcd676d5d5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseacl.html.twig"));

        // line 18
        $context["acl"] = $this->loadTemplate("SonataAdminBundle:CRUD:baseaclmacro.html.twig", "SonataAdminBundle:CRUD:baseacl.html.twig", 18);
        // line 12
        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal84176f0ec6700e53112043c99ebc769ca8649e82a305c1f4ff617ee131ebad80->leave($internal84176f0ec6700e53112043c99ebc769ca8649e82a305c1f4ff617ee131ebad80prof);

        
        $internal37078a069ca9be0b8be5a02b4610dbf9bd6dbd3857ff81640d6150fcd676d5d5->leave($internal37078a069ca9be0b8be5a02b4610dbf9bd6dbd3857ff81640d6150fcd676d5d5prof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internal9c832d1b35879c0308dc90d5fa833a1921d19075c23348bccb71157c5b139fa7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9c832d1b35879c0308dc90d5fa833a1921d19075c23348bccb71157c5b139fa7->enter($internal9c832d1b35879c0308dc90d5fa833a1921d19075c23348bccb71157c5b139fa7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal3212e480d7335d68e5a598ecfb68a9fac0feed6717783b72d14ea1cefc1dc5fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3212e480d7335d68e5a598ecfb68a9fac0feed6717783b72d14ea1cefc1dc5fb->enter($internal3212e480d7335d68e5a598ecfb68a9fac0feed6717783b72d14ea1cefc1dc5fbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:baseacl.html.twig", 15)->display($context);
        
        $internal3212e480d7335d68e5a598ecfb68a9fac0feed6717783b72d14ea1cefc1dc5fb->leave($internal3212e480d7335d68e5a598ecfb68a9fac0feed6717783b72d14ea1cefc1dc5fbprof);

        
        $internal9c832d1b35879c0308dc90d5fa833a1921d19075c23348bccb71157c5b139fa7->leave($internal9c832d1b35879c0308dc90d5fa833a1921d19075c23348bccb71157c5b139fa7prof);

    }

    // line 20
    public function blockform($context, array $blocks = array())
    {
        $internal32fd300b6217d6d675714515a01507ffdb504478c56e0900402f876f69ae5143 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal32fd300b6217d6d675714515a01507ffdb504478c56e0900402f876f69ae5143->enter($internal32fd300b6217d6d675714515a01507ffdb504478c56e0900402f876f69ae5143prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        $internal3fe7f854e1e207abbc30e39b65b6d97edcc5439c5a87aa9fb354f79e97c3e6f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3fe7f854e1e207abbc30e39b65b6d97edcc5439c5a87aa9fb354f79e97c3e6f8->enter($internal3fe7f854e1e207abbc30e39b65b6d97edcc5439c5a87aa9fb354f79e97c3e6f8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        // line 21
        echo "    ";
        $this->displayBlock('formaclroles', $context, $blocks);
        // line 24
        echo "    ";
        $this->displayBlock('formaclusers', $context, $blocks);
        
        $internal3fe7f854e1e207abbc30e39b65b6d97edcc5439c5a87aa9fb354f79e97c3e6f8->leave($internal3fe7f854e1e207abbc30e39b65b6d97edcc5439c5a87aa9fb354f79e97c3e6f8prof);

        
        $internal32fd300b6217d6d675714515a01507ffdb504478c56e0900402f876f69ae5143->leave($internal32fd300b6217d6d675714515a01507ffdb504478c56e0900402f876f69ae5143prof);

    }

    // line 21
    public function blockformaclroles($context, array $blocks = array())
    {
        $internale56032819550ebf505e1fbda0d20936cf6ed989200cf6b60a8cbbc19b542a93c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale56032819550ebf505e1fbda0d20936cf6ed989200cf6b60a8cbbc19b542a93c->enter($internale56032819550ebf505e1fbda0d20936cf6ed989200cf6b60a8cbbc19b542a93cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formaclroles"));

        $internalab4b49999982aaf306eaea86ea7c4bee3400fbd316f3ae162eee9cfb854fc7d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalab4b49999982aaf306eaea86ea7c4bee3400fbd316f3ae162eee9cfb854fc7d0->enter($internalab4b49999982aaf306eaea86ea7c4bee3400fbd316f3ae162eee9cfb854fc7d0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formaclroles"));

        // line 22
        echo "        ";
        echo $context["acl"]->macrorenderform((isset($context["aclRolesForm"]) || arraykeyexists("aclRolesForm", $context) ? $context["aclRolesForm"] : (function () { throw new TwigErrorRuntime('Variable "aclRolesForm" does not exist.', 22, $this->getSourceContext()); })()), (isset($context["permissions"]) || arraykeyexists("permissions", $context) ? $context["permissions"] : (function () { throw new TwigErrorRuntime('Variable "permissions" does not exist.', 22, $this->getSourceContext()); })()), "tdrole", (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 22, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 22, $this->getSourceContext()); })()), "adminPool", array()), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 22, $this->getSourceContext()); })()));
        echo "
    ";
        
        $internalab4b49999982aaf306eaea86ea7c4bee3400fbd316f3ae162eee9cfb854fc7d0->leave($internalab4b49999982aaf306eaea86ea7c4bee3400fbd316f3ae162eee9cfb854fc7d0prof);

        
        $internale56032819550ebf505e1fbda0d20936cf6ed989200cf6b60a8cbbc19b542a93c->leave($internale56032819550ebf505e1fbda0d20936cf6ed989200cf6b60a8cbbc19b542a93cprof);

    }

    // line 24
    public function blockformaclusers($context, array $blocks = array())
    {
        $internal49088ca758791862b638baba3b1700e35c0ba527d662f850b7368ae5f2e53b35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal49088ca758791862b638baba3b1700e35c0ba527d662f850b7368ae5f2e53b35->enter($internal49088ca758791862b638baba3b1700e35c0ba527d662f850b7368ae5f2e53b35prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formaclusers"));

        $internal20445e535b8ceed8fd476dbb299dc18f939e707730ab95c415b3e57a37d80731 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal20445e535b8ceed8fd476dbb299dc18f939e707730ab95c415b3e57a37d80731->enter($internal20445e535b8ceed8fd476dbb299dc18f939e707730ab95c415b3e57a37d80731prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formaclusers"));

        // line 25
        echo "        ";
        echo $context["acl"]->macrorenderform((isset($context["aclUsersForm"]) || arraykeyexists("aclUsersForm", $context) ? $context["aclUsersForm"] : (function () { throw new TwigErrorRuntime('Variable "aclUsersForm" does not exist.', 25, $this->getSourceContext()); })()), (isset($context["permissions"]) || arraykeyexists("permissions", $context) ? $context["permissions"] : (function () { throw new TwigErrorRuntime('Variable "permissions" does not exist.', 25, $this->getSourceContext()); })()), "tdusername", (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 25, $this->getSourceContext()); })()), "adminPool", array()), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 25, $this->getSourceContext()); })()));
        echo "
    ";
        
        $internal20445e535b8ceed8fd476dbb299dc18f939e707730ab95c415b3e57a37d80731->leave($internal20445e535b8ceed8fd476dbb299dc18f939e707730ab95c415b3e57a37d80731prof);

        
        $internal49088ca758791862b638baba3b1700e35c0ba527d662f850b7368ae5f2e53b35->leave($internal49088ca758791862b638baba3b1700e35c0ba527d662f850b7368ae5f2e53b35prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseacl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 25,  109 => 24,  96 => 22,  87 => 21,  76 => 24,  73 => 21,  64 => 20,  54 => 15,  45 => 14,  35 => 12,  33 => 18,  21 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% import 'SonataAdminBundle:CRUD:baseaclmacro.html.twig' as acl %}

{% block form %}
    {% block formaclroles %}
        {{ acl.renderform(aclRolesForm, permissions, 'tdrole', admin, sonataadmin.adminPool, object) }}
    {% endblock %}
    {% block formaclusers %}
        {{ acl.renderform(aclUsersForm, permissions, 'tdusername', admin, sonataadmin.adminPool, object) }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle:CRUD:baseacl.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseacl.html.twig");
    }
}
