<?php

/* @Framework/Form/textareawidget.html.php */
class TwigTemplatefc55f5b24e9be32c0de1d3c9739935ad03c0dbbf7ac2093713ef6007b6d682d1 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal90a74dade96da650f827a08935186061f05120b00b51bb31242541ec71dfb2c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal90a74dade96da650f827a08935186061f05120b00b51bb31242541ec71dfb2c0->enter($internal90a74dade96da650f827a08935186061f05120b00b51bb31242541ec71dfb2c0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/textareawidget.html.php"));

        $internal7cbe87ab66c299510642031e4199a190886df0a47e28dca6878069ae6d153e39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7cbe87ab66c299510642031e4199a190886df0a47e28dca6878069ae6d153e39->enter($internal7cbe87ab66c299510642031e4199a190886df0a47e28dca6878069ae6d153e39prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/textareawidget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $internal90a74dade96da650f827a08935186061f05120b00b51bb31242541ec71dfb2c0->leave($internal90a74dade96da650f827a08935186061f05120b00b51bb31242541ec71dfb2c0prof);

        
        $internal7cbe87ab66c299510642031e4199a190886df0a47e28dca6878069ae6d153e39->leave($internal7cbe87ab66c299510642031e4199a190886df0a47e28dca6878069ae6d153e39prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textareawidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<textarea <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textareawidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textareawidget.html.php");
    }
}
