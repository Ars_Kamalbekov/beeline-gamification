<?php

/* SonataAdminBundle:Block:blocksearchresult.html.twig */
class TwigTemplate427b7897bafd07b9e0f4460a9c6b53b529bdf93f93d4a908517a7728b58594df extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataAdminBundle:Block:blocksearchresult.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale5ab42a3871338854a56c31968cb4966299ceeaeb95544421db963b9e59a3301 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale5ab42a3871338854a56c31968cb4966299ceeaeb95544421db963b9e59a3301->enter($internale5ab42a3871338854a56c31968cb4966299ceeaeb95544421db963b9e59a3301prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blocksearchresult.html.twig"));

        $internaleb2ba9b9e310696648e1b87b1a34e7ae7823632cdf5bd2c7fb82cbb06bbd3822 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaleb2ba9b9e310696648e1b87b1a34e7ae7823632cdf5bd2c7fb82cbb06bbd3822->enter($internaleb2ba9b9e310696648e1b87b1a34e7ae7823632cdf5bd2c7fb82cbb06bbd3822prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blocksearchresult.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internale5ab42a3871338854a56c31968cb4966299ceeaeb95544421db963b9e59a3301->leave($internale5ab42a3871338854a56c31968cb4966299ceeaeb95544421db963b9e59a3301prof);

        
        $internaleb2ba9b9e310696648e1b87b1a34e7ae7823632cdf5bd2c7fb82cbb06bbd3822->leave($internaleb2ba9b9e310696648e1b87b1a34e7ae7823632cdf5bd2c7fb82cbb06bbd3822prof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal597ca14536cb164ebc7f40aeb13899a8f2377b2fa4bec83c8639b4fdc41cf0c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal597ca14536cb164ebc7f40aeb13899a8f2377b2fa4bec83c8639b4fdc41cf0c2->enter($internal597ca14536cb164ebc7f40aeb13899a8f2377b2fa4bec83c8639b4fdc41cf0c2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal31a090d907683535fe4453c5ff33cb52a649439ec15efd5eceed47ae04db3163 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal31a090d907683535fe4453c5ff33cb52a649439ec15efd5eceed47ae04db3163->enter($internal31a090d907683535fe4453c5ff33cb52a649439ec15efd5eceed47ae04db3163prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        $context["showemptyboxes"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 15, $this->getSourceContext()); })()), "adminPool", array()), "container", array()), "getParameter", array(0 => "sonata.admin.configuration.globalsearch.emptyboxes"), "method");
        // line 16
        echo "    ";
        $context["visibilityclass"] = "";
        // line 17
        echo "    ";
        if (((isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 17, $this->getSourceContext()); })()) &&  !twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 17, $this->getSourceContext()); })()), "getResults", array(), "method")))) {
            // line 18
            echo "        ";
            $context["visibilityclass"] = ("sonata-search-result-" . (isset($context["showemptyboxes"]) || arraykeyexists("showemptyboxes", $context) ? $context["showemptyboxes"] : (function () { throw new TwigErrorRuntime('Variable "showemptyboxes" does not exist.', 18, $this->getSourceContext()); })()));
            // line 19
            echo "    ";
        }
        // line 20
        echo "
    <div class=\"col-lg-4 col-md-6 search-box-item ";
        // line 21
        echo twigescapefilter($this->env, (isset($context["visibilityclass"]) || arraykeyexists("visibilityclass", $context) ? $context["visibilityclass"] : (function () { throw new TwigErrorRuntime('Variable "visibilityclass" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        <div class=\"box box-solid box-primary";
        // line 22
        echo twigescapefilter($this->env, (isset($context["visibilityclass"]) || arraykeyexists("visibilityclass", $context) ? $context["visibilityclass"] : (function () { throw new TwigErrorRuntime('Variable "visibilityclass" does not exist.', 22, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
            <div class=\"box-header with-border ";
        // line 23
        echo twigescapefilter($this->env, (isset($context["visibilityclass"]) || arraykeyexists("visibilityclass", $context) ? $context["visibilityclass"] : (function () { throw new TwigErrorRuntime('Variable "visibilityclass" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
                ";
        // line 24
        $context["icon"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["settings"] ?? null), "icon", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["settings"] ?? null), "icon", array()), "")) : (""));
        // line 25
        echo "                ";
        echo (isset($context["icon"]) || arraykeyexists("icon", $context) ? $context["icon"] : (function () { throw new TwigErrorRuntime('Variable "icon" does not exist.', 25, $this->getSourceContext()); })());
        echo "
                <h3 class=\"box-title\">
                    ";
        // line 27
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 27, $this->getSourceContext()); })()), "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 27, $this->getSourceContext()); })()), "translationdomain", array())), "html", null, true);
        echo "
                </h3>

                <div class=\"box-tools pull-right\">
                    ";
        // line 31
        if (((isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 31, $this->getSourceContext()); })()) && (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 31, $this->getSourceContext()); })()), "getNbResults", array(), "method") > 0))) {
            // line 32
            echo "                        <span class=\"badge bg-light-blue\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 32, $this->getSourceContext()); })()), "getNbResults", array(), "method"), "html", null, true);
            echo "</span>
                    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(),         // line 33
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 33, $this->getSourceContext()); })()), "hasRoute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 33, $this->getSourceContext()); })()), "hasAccess", array(0 => "create"), "method"))) {
            // line 34
            echo "                        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 34, $this->getSourceContext()); })()), "generateUrl", array(0 => "create"), "method"), "html", null, true);
            echo "\" class=\"btn btn-box-tool\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 38
        echo "                    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "hasRoute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "hasAccess", array(0 => "list"), "method"))) {
            // line 39
            echo "                        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 39, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
            echo "\" class=\"btn btn-box-tool\">
                            <i class=\"fa fa-list\" aria-hidden=\"true\"></i>
                        </a>
                    ";
        }
        // line 43
        echo "                </div>
            </div>
            ";
        // line 45
        if (((isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 45, $this->getSourceContext()); })()) && twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 45, $this->getSourceContext()); })()), "getResults", array(), "method")))) {
            // line 46
            echo "                <div class=\"box-body no-padding\">
                    <ul class=\"nav nav-stacked sonata-search-result-list\">
                        ";
            // line 48
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 48, $this->getSourceContext()); })()), "getResults", array(), "method"));
            foreach ($context['seq'] as $context["key"] => $context["result"]) {
                // line 49
                echo "                            ";
                $context["link"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 49, $this->getSourceContext()); })()), "getSearchResultLink", array(0 => $context["result"]), "method");
                // line 50
                echo "                            ";
                if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 50, $this->getSourceContext()); })())) {
                    // line 51
                    echo "                                <li><a href=\"";
                    echo twigescapefilter($this->env, (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 51, $this->getSourceContext()); })()), "html", null, true);
                    echo "\">";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 51, $this->getSourceContext()); })()), "toString", array(0 => $context["result"]), "method"), "html", null, true);
                    echo "</a></li>
                            ";
                } else {
                    // line 53
                    echo "                                <li><a>";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 53, $this->getSourceContext()); })()), "toString", array(0 => $context["result"]), "method"), "html", null, true);
                    echo "</a></li>
                            ";
                }
                // line 55
                echo "                        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['result'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 56
            echo "                    </ul>
                </div>
            ";
        } else {
            // line 59
            echo "                <div class=\"box-body\">
                    <p>
                        <em>";
            // line 61
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("noresultsfound", array(), "SonataAdminBundle"), "html", null, true);
            echo "</em>
                    </p>
                </div>
            ";
        }
        // line 65
        echo "        </div>
    </div>
";
        
        $internal31a090d907683535fe4453c5ff33cb52a649439ec15efd5eceed47ae04db3163->leave($internal31a090d907683535fe4453c5ff33cb52a649439ec15efd5eceed47ae04db3163prof);

        
        $internal597ca14536cb164ebc7f40aeb13899a8f2377b2fa4bec83c8639b4fdc41cf0c2->leave($internal597ca14536cb164ebc7f40aeb13899a8f2377b2fa4bec83c8639b4fdc41cf0c2prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Block:blocksearchresult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 65,  170 => 61,  166 => 59,  161 => 56,  155 => 55,  149 => 53,  141 => 51,  138 => 50,  135 => 49,  131 => 48,  127 => 46,  125 => 45,  121 => 43,  113 => 39,  110 => 38,  102 => 34,  100 => 33,  95 => 32,  93 => 31,  86 => 27,  80 => 25,  78 => 24,  74 => 23,  70 => 22,  66 => 21,  63 => 20,  60 => 19,  57 => 18,  54 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    {% set showemptyboxes = sonataadmin.adminPool.container.getParameter('sonata.admin.configuration.globalsearch.emptyboxes') %}
    {% set visibilityclass = '' %}
    {% if pager and not pager.getResults()|length %}
        {% set visibilityclass = 'sonata-search-result-' ~ showemptyboxes %}
    {% endif %}

    <div class=\"col-lg-4 col-md-6 search-box-item {{ visibilityclass }}\">
        <div class=\"box box-solid box-primary{{ visibilityclass }}\">
            <div class=\"box-header with-border {{ visibilityclass }}\">
                {% set icon = settings.icon|default('') %}
                {{ icon|raw }}
                <h3 class=\"box-title\">
                    {{ admin.label|trans({}, admin.translationdomain) }}
                </h3>

                <div class=\"box-tools pull-right\">
                    {% if pager and pager.getNbResults() > 0 %}
                        <span class=\"badge bg-light-blue\">{{ pager.getNbResults() }}</span>
                    {% elseif admin.hasRoute('create') and admin.hasAccess('create') %}
                        <a href=\"{{ admin.generateUrl('create') }}\" class=\"btn btn-box-tool\">
                            <i class=\"fa fa-plus\" aria-hidden=\"true\"></i>
                        </a>
                    {% endif %}
                    {% if admin.hasRoute('list') and admin.hasAccess('list') %}
                        <a href=\"{{ admin.generateUrl('list') }}\" class=\"btn btn-box-tool\">
                            <i class=\"fa fa-list\" aria-hidden=\"true\"></i>
                        </a>
                    {% endif %}
                </div>
            </div>
            {% if pager and pager.getResults()|length %}
                <div class=\"box-body no-padding\">
                    <ul class=\"nav nav-stacked sonata-search-result-list\">
                        {% for result in pager.getResults() %}
                            {% set link = admin.getSearchResultLink(result) %}
                            {% if link %}
                                <li><a href=\"{{ link }}\">{{ admin.toString(result) }}</a></li>
                            {% else %}
                                <li><a>{{ admin.toString(result) }}</a></li>
                            {% endif %}
                        {% endfor %}
                    </ul>
                </div>
            {% else %}
                <div class=\"box-body\">
                    <p>
                        <em>{{ 'noresultsfound'|trans({}, 'SonataAdminBundle') }}</em>
                    </p>
                </div>
            {% endif %}
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:Block:blocksearchresult.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Block/blocksearchresult.html.twig");
    }
}
