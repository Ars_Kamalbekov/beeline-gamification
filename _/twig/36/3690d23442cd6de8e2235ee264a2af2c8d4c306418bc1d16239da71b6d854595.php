<?php

/* @Framework/Form/widgetcontainerattributes.html.php */
class TwigTemplateb229c603cf4e343e86e9278341d50129b0adcfc9c6c3daa4dea16f7b4ee602f5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4872e5546c7abed198f163a47609f9b1594f30ea4c24d322220011e7e9a8ee6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4872e5546c7abed198f163a47609f9b1594f30ea4c24d322220011e7e9a8ee6f->enter($internal4872e5546c7abed198f163a47609f9b1594f30ea4c24d322220011e7e9a8ee6fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/widgetcontainerattributes.html.php"));

        $internal6c045aa7291eb07237be9fee37d926d8995f4859c785f974db196094c4fde689 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6c045aa7291eb07237be9fee37d926d8995f4859c785f974db196094c4fde689->enter($internal6c045aa7291eb07237be9fee37d926d8995f4859c785f974db196094c4fde689prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/widgetcontainerattributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $internal4872e5546c7abed198f163a47609f9b1594f30ea4c24d322220011e7e9a8ee6f->leave($internal4872e5546c7abed198f163a47609f9b1594f30ea4c24d322220011e7e9a8ee6fprof);

        
        $internal6c045aa7291eb07237be9fee37d926d8995f4859c785f974db196094c4fde689->leave($internal6c045aa7291eb07237be9fee37d926d8995f4859c785f974db196094c4fde689prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widgetcontainerattributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widgetcontainerattributes.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widgetcontainerattributes.html.php");
    }
}
