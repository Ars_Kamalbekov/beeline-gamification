<?php

/* SonataAdminBundle:CRUD:edittext.html.twig */
class TwigTemplate5db8dd5de1117cd39b5439450bc48123e59b1893479f5ff10d0ffa8788b4440d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:edittext.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf10c232d40c8bf10de11749519d0e3353185c0ee1971b70d34b0bc4ad72e90d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf10c232d40c8bf10de11749519d0e3353185c0ee1971b70d34b0bc4ad72e90d7->enter($internalf10c232d40c8bf10de11749519d0e3353185c0ee1971b70d34b0bc4ad72e90d7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edittext.html.twig"));

        $internalc4c411dacedf3e52785be95525a6c47d58b23bd6bad8a7012f5fd886bc6513d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc4c411dacedf3e52785be95525a6c47d58b23bd6bad8a7012f5fd886bc6513d4->enter($internalc4c411dacedf3e52785be95525a6c47d58b23bd6bad8a7012f5fd886bc6513d4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edittext.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf10c232d40c8bf10de11749519d0e3353185c0ee1971b70d34b0bc4ad72e90d7->leave($internalf10c232d40c8bf10de11749519d0e3353185c0ee1971b70d34b0bc4ad72e90d7prof);

        
        $internalc4c411dacedf3e52785be95525a6c47d58b23bd6bad8a7012f5fd886bc6513d4->leave($internalc4c411dacedf3e52785be95525a6c47d58b23bd6bad8a7012f5fd886bc6513d4prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal95b2e58d196e0efda7ab912722e0615b5c6aa4309f548f67093c9a234bb3f41b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal95b2e58d196e0efda7ab912722e0615b5c6aa4309f548f67093c9a234bb3f41b->enter($internal95b2e58d196e0efda7ab912722e0615b5c6aa4309f548f67093c9a234bb3f41bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal385343a5bf9080ff58bb9c1743dc9774bf0ceea7938a1406a31427a3aa3bd58d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal385343a5bf9080ff58bb9c1743dc9774bf0ceea7938a1406a31427a3aa3bd58d->enter($internal385343a5bf9080ff58bb9c1743dc9774bf0ceea7938a1406a31427a3aa3bd58dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $internal385343a5bf9080ff58bb9c1743dc9774bf0ceea7938a1406a31427a3aa3bd58d->leave($internal385343a5bf9080ff58bb9c1743dc9774bf0ceea7938a1406a31427a3aa3bd58dprof);

        
        $internal95b2e58d196e0efda7ab912722e0615b5c6aa4309f548f67093c9a234bb3f41b->leave($internal95b2e58d196e0efda7ab912722e0615b5c6aa4309f548f67093c9a234bb3f41bprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edittext.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block field %}{{ formwidget(fieldelement, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edittext.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edittext.html.twig");
    }
}
