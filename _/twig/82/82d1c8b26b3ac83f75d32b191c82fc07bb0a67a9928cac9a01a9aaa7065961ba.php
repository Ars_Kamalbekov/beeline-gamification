<?php

/* TwigBundle:Exception:exception.js.twig */
class TwigTemplate5d93fc426d4ef442e2a6a36d2e6c274e02e0006bbc37a72f6973b80e12430ed2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale2d40153f009976fce23cd5af0a2367a2bbbd04b63020978be85ba8a446a9161 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale2d40153f009976fce23cd5af0a2367a2bbbd04b63020978be85ba8a446a9161->enter($internale2d40153f009976fce23cd5af0a2367a2bbbd04b63020978be85ba8a446a9161prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $internalf27dd549e8912dcc049e1b07d0e486098a9bfc69bb50780691488453b778822e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf27dd549e8912dcc049e1b07d0e486098a9bfc69bb50780691488453b778822e->enter($internalf27dd549e8912dcc049e1b07d0e486098a9bfc69bb50780691488453b778822eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twiginclude($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $internale2d40153f009976fce23cd5af0a2367a2bbbd04b63020978be85ba8a446a9161->leave($internale2d40153f009976fce23cd5af0a2367a2bbbd04b63020978be85ba8a446a9161prof);

        
        $internalf27dd549e8912dcc049e1b07d0e486098a9bfc69bb50780691488453b778822e->leave($internalf27dd549e8912dcc049e1b07d0e486098a9bfc69bb50780691488453b778822eprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
