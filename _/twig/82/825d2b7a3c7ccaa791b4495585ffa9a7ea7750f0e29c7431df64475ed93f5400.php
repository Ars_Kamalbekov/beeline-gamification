<?php

/* @WebProfiler/Icon/cache.svg */
class TwigTemplate46bdf93a3b38288b764d04bc6a726111f030d1278f24c552e59fb69b3ce68703 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2414362f9acb8d8e7f421e5704e85fadb50df82e2c603d9153899980589a1b86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2414362f9acb8d8e7f421e5704e85fadb50df82e2c603d9153899980589a1b86->enter($internal2414362f9acb8d8e7f421e5704e85fadb50df82e2c603d9153899980589a1b86prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        $internal4619e26999fa675a3ceb11da0fa927386034cae81cfc7137cf47ebda3f4bc35c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4619e26999fa675a3ceb11da0fa927386034cae81cfc7137cf47ebda3f4bc35c->enter($internal4619e26999fa675a3ceb11da0fa927386034cae81cfc7137cf47ebda3f4bc35cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
";
        
        $internal2414362f9acb8d8e7f421e5704e85fadb50df82e2c603d9153899980589a1b86->leave($internal2414362f9acb8d8e7f421e5704e85fadb50df82e2c603d9153899980589a1b86prof);

        
        $internal4619e26999fa675a3ceb11da0fa927386034cae81cfc7137cf47ebda3f4bc35c->leave($internal4619e26999fa675a3ceb11da0fa927386034cae81cfc7137cf47ebda3f4bc35cprof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/cache.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
", "@WebProfiler/Icon/cache.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/cache.svg");
    }
}
