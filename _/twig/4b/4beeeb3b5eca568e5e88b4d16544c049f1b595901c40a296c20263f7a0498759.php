<?php

/* TwigBundle:Exception:error.json.twig */
class TwigTemplate4c16b29cfb0a169616d85135033653ce43b566c45c89d7d660440ad7fccddcd8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal33bf5f5ab4ce909d6cb76c061e4f8aba554deece90705368ff683399decd8ee2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal33bf5f5ab4ce909d6cb76c061e4f8aba554deece90705368ff683399decd8ee2->enter($internal33bf5f5ab4ce909d6cb76c061e4f8aba554deece90705368ff683399decd8ee2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $internalcf5b73b86007b0a17294460056a035e2387575a097571f43133e940a6cefb070 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcf5b73b86007b0a17294460056a035e2387575a097571f43133e940a6cefb070->enter($internalcf5b73b86007b0a17294460056a035e2387575a097571f43133e940a6cefb070prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo jsonencode(array("error" => array("code" => (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 1, $this->getSourceContext()); })()))));
        echo "
";
        
        $internal33bf5f5ab4ce909d6cb76c061e4f8aba554deece90705368ff683399decd8ee2->leave($internal33bf5f5ab4ce909d6cb76c061e4f8aba554deece90705368ff683399decd8ee2prof);

        
        $internalcf5b73b86007b0a17294460056a035e2387575a097571f43133e940a6cefb070->leave($internalcf5b73b86007b0a17294460056a035e2387575a097571f43133e940a6cefb070prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{{ { 'error': { 'code': statuscode, 'message': statustext } }|jsonencode|raw }}
", "TwigBundle:Exception:error.json.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
