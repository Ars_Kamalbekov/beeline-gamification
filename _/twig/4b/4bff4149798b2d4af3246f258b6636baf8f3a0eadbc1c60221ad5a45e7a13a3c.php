<?php

/* SonataAdminBundle:CRUD/Association:showmanytomany.html.twig */
class TwigTemplateb2a8bcca9eded8a2a81aff9fa853d9551bff5a7515185e167972ccee810c4bee extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD/Association:showmanytomany.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8a60b1595448e9eb3e120de56d195146af0a962b17bb6a6802ea041efd058431 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8a60b1595448e9eb3e120de56d195146af0a962b17bb6a6802ea041efd058431->enter($internal8a60b1595448e9eb3e120de56d195146af0a962b17bb6a6802ea041efd058431prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showmanytomany.html.twig"));

        $internalcefb3294347c9e04088cb5ac0531ab860056c15ab2e73e525bd87453a85c267f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcefb3294347c9e04088cb5ac0531ab860056c15ab2e73e525bd87453a85c267f->enter($internalcefb3294347c9e04088cb5ac0531ab860056c15ab2e73e525bd87453a85c267fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showmanytomany.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal8a60b1595448e9eb3e120de56d195146af0a962b17bb6a6802ea041efd058431->leave($internal8a60b1595448e9eb3e120de56d195146af0a962b17bb6a6802ea041efd058431prof);

        
        $internalcefb3294347c9e04088cb5ac0531ab860056c15ab2e73e525bd87453a85c267f->leave($internalcefb3294347c9e04088cb5ac0531ab860056c15ab2e73e525bd87453a85c267fprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal8002b1b059a205a228c21f027c480a1a1a180692a9e5fab964fb07d3fa716aa5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8002b1b059a205a228c21f027c480a1a1a180692a9e5fab964fb07d3fa716aa5->enter($internal8002b1b059a205a228c21f027c480a1a1a180692a9e5fab964fb07d3fa716aa5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalaf6353008f356e5b03548e64c18b9d462c2bdf39c29131f397de00b90753fd7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaf6353008f356e5b03548e64c18b9d462c2bdf39c29131f397de00b90753fd7e->enter($internalaf6353008f356e5b03548e64c18b9d462c2bdf39c29131f397de00b90753fd7eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <ul class=\"sonata-ba-show-many-to-many\">
    ";
        // line 16
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 17
        echo "        ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["element"]) {
            // line 18
            echo "            <li>
                ";
            // line 19
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 20
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 21
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 21, $this->getSourceContext()); })()), 1 => $context["element"]), "method"))) {
                // line 22
                echo "                    <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 22, $this->getSourceContext()); })()), 1 => $context["element"], 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                        ";
                // line 23
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
                echo "
                    </a>
                ";
            } else {
                // line 26
                echo "                    ";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 26, $this->getSourceContext()); })())), "html", null, true);
                echo "
                ";
            }
            // line 28
            echo "            </li>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 30
        echo "    </ul>
";
        
        $internalaf6353008f356e5b03548e64c18b9d462c2bdf39c29131f397de00b90753fd7e->leave($internalaf6353008f356e5b03548e64c18b9d462c2bdf39c29131f397de00b90753fd7eprof);

        
        $internal8002b1b059a205a228c21f027c480a1a1a180692a9e5fab964fb07d3fa716aa5->leave($internal8002b1b059a205a228c21f027c480a1a1a180692a9e5fab964fb07d3fa716aa5prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:showmanytomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 30,  83 => 28,  77 => 26,  71 => 23,  66 => 22,  64 => 21,  63 => 20,  62 => 19,  59 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    <ul class=\"sonata-ba-show-many-to-many\">
    {% set routename = fielddescription.options.route.name %}
        {% for element in value %}
            <li>
                {% if fielddescription.hasassociationadmin
                and fielddescription.associationadmin.hasRoute(routename)
                and fielddescription.associationadmin.hasAccess(routename, element) %}
                    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
                        {{ element|renderrelationelement(fielddescription) }}
                    </a>
                {% else %}
                    {{ element|renderrelationelement(fielddescription) }}
                {% endif %}
            </li>
        {% endfor %}
    </ul>
{% endblock %}
", "SonataAdminBundle:CRUD/Association:showmanytomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/showmanytomany.html.twig");
    }
}
