<?php

/* TwigBundle:Exception:error.txt.twig */
class TwigTemplatedb5c2c8fbea1746a96f846b5dba9d7240070bfeac0bf462e5c788f20d81547b4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaleb2670a2f3046bf2df04cf880aeea8f4364b71f38b8ff3ce3a0cfa13df153d59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleb2670a2f3046bf2df04cf880aeea8f4364b71f38b8ff3ce3a0cfa13df153d59->enter($internaleb2670a2f3046bf2df04cf880aeea8f4364b71f38b8ff3ce3a0cfa13df153d59prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $internal97e07d3bd3ec1a5a2fe86c19eb07bf8b37464b4eac4de9c3160ccddd2322af48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal97e07d3bd3ec1a5a2fe86c19eb07bf8b37464b4eac4de9c3160ccddd2322af48->enter($internal97e07d3bd3ec1a5a2fe86c19eb07bf8b37464b4eac4de9c3160ccddd2322af48prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 4, $this->getSourceContext()); })());
        echo " ";
        echo (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 4, $this->getSourceContext()); })());
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $internaleb2670a2f3046bf2df04cf880aeea8f4364b71f38b8ff3ce3a0cfa13df153d59->leave($internaleb2670a2f3046bf2df04cf880aeea8f4364b71f38b8ff3ce3a0cfa13df153d59prof);

        
        $internal97e07d3bd3ec1a5a2fe86c19eb07bf8b37464b4eac4de9c3160ccddd2322af48->leave($internal97e07d3bd3ec1a5a2fe86c19eb07bf8b37464b4eac4de9c3160ccddd2322af48prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("Oops! An Error Occurred
=======================

The server returned a \"{{ statuscode }} {{ statustext }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
