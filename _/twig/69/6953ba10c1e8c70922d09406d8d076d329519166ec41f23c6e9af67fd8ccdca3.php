<?php

/* SonataAdminBundle:CRUD:displayboolean.html.twig */
class TwigTemplate22002edabf7adb8c3713bde75ed302bb159a0fec3447f1eab3b81b6822a8847e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal05afd2ff223638b45f0c068427e025b7d459b3a1e2858ba9cd30fad9942a1a68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal05afd2ff223638b45f0c068427e025b7d459b3a1e2858ba9cd30fad9942a1a68->enter($internal05afd2ff223638b45f0c068427e025b7d459b3a1e2858ba9cd30fad9942a1a68prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:displayboolean.html.twig"));

        $internalbd88c79aac640f9ff94beaad9217fc71f759d1dbd7d9c1b3e469e62510d50ccc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbd88c79aac640f9ff94beaad9217fc71f759d1dbd7d9c1b3e469e62510d50ccc->enter($internalbd88c79aac640f9ff94beaad9217fc71f759d1dbd7d9c1b3e469e62510d50cccprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:displayboolean.html.twig"));

        // line 12
        obstart();
        // line 13
        echo "    ";
        if ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 13, $this->getSourceContext()); })())) {
            // line 14
            echo "        ";
            $context["text"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labeltypeyes", array(), "SonataAdminBundle");
            // line 15
            echo "    ";
        } else {
            // line 16
            echo "        ";
            $context["text"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labeltypeno", array(), "SonataAdminBundle");
            // line 17
            echo "    ";
        }
        // line 18
        echo "
    ";
        // line 19
        if (((((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "inverse", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "inverse", array()), false)) : (false))) ? ( !(isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())) : ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())))) {
            // line 20
            echo "        ";
            $context["class"] = "label-success";
            // line 21
            echo "    ";
        } else {
            // line 22
            echo "        ";
            $context["class"] = "label-danger";
            // line 23
            echo "    ";
        }
        // line 24
        echo "
    <span class=\"label ";
        // line 25
        echo twigescapefilter($this->env, (isset($context["class"]) || arraykeyexists("class", $context) ? $context["class"] : (function () { throw new TwigErrorRuntime('Variable "class" does not exist.', 25, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twigescapefilter($this->env, (isset($context["text"]) || arraykeyexists("text", $context) ? $context["text"] : (function () { throw new TwigErrorRuntime('Variable "text" does not exist.', 25, $this->getSourceContext()); })()), "html", null, true);
        echo "</span>
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal05afd2ff223638b45f0c068427e025b7d459b3a1e2858ba9cd30fad9942a1a68->leave($internal05afd2ff223638b45f0c068427e025b7d459b3a1e2858ba9cd30fad9942a1a68prof);

        
        $internalbd88c79aac640f9ff94beaad9217fc71f759d1dbd7d9c1b3e469e62510d50ccc->leave($internalbd88c79aac640f9ff94beaad9217fc71f759d1dbd7d9c1b3e469e62510d50cccprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:displayboolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 25,  59 => 24,  56 => 23,  53 => 22,  50 => 21,  47 => 20,  45 => 19,  42 => 18,  39 => 17,  36 => 16,  33 => 15,  30 => 14,  27 => 13,  25 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{%- spaceless %}
    {% if value %}
        {% set text = 'labeltypeyes'|trans({}, 'SonataAdminBundle') %}
    {% else %}
        {% set text = 'labeltypeno'|trans({}, 'SonataAdminBundle') %}
    {% endif %}

    {% if fielddescription.options.inverse|default(false) ? not value : value %}
        {% set class = 'label-success' %}
    {% else %}
        {% set class = 'label-danger' %}
    {% endif %}

    <span class=\"label {{ class }}\">{{ text }}</span>
{% endspaceless -%}
", "SonataAdminBundle:CRUD:displayboolean.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/displayboolean.html.twig");
    }
}
