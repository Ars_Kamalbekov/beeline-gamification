<?php

/* SonataAdminBundle:Helper:short-object-description.html.twig */
class TwigTemplate624fc922c1b3b1db34cd92686afcf1f923c4584061f57ecec11762680df0598c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0ee3fde946b7486fb8a4b9e2a25109f38d12654bd27ad1eaff402494f5f29a4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0ee3fde946b7486fb8a4b9e2a25109f38d12654bd27ad1eaff402494f5f29a4e->enter($internal0ee3fde946b7486fb8a4b9e2a25109f38d12654bd27ad1eaff402494f5f29a4eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:short-object-description.html.twig"));

        $internal9175bdb23b6b78cb420092d3ab0c827657cf0b46f05f8fdd5d593ddda3d35b39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9175bdb23b6b78cb420092d3ab0c827657cf0b46f05f8fdd5d593ddda3d35b39->enter($internal9175bdb23b6b78cb420092d3ab0c827657cf0b46f05f8fdd5d593ddda3d35b39prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:short-object-description.html.twig"));

        // line 1
        echo "<span class=\"inner-field-short-description\">
    ";
        // line 2
        if ((((isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 2, $this->getSourceContext()); })()) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 2, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method")) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 2, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit"), "method"))) {
            // line 3
            echo "        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 3, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 3, $this->getSourceContext()); })()), 2 => (isset($context["linkparameters"]) || arraykeyexists("linkparameters", $context) ? $context["linkparameters"] : (function () { throw new TwigErrorRuntime('Variable "linkparameters" does not exist.', 3, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\" target=\"new\">";
            echo twigescapefilter($this->env, (isset($context["description"]) || arraykeyexists("description", $context) ? $context["description"] : (function () { throw new TwigErrorRuntime('Variable "description" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 5
            echo "        ";
            echo twigescapefilter($this->env, (isset($context["description"]) || arraykeyexists("description", $context) ? $context["description"] : (function () { throw new TwigErrorRuntime('Variable "description" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        // line 7
        echo "</span>
";
        
        $internal0ee3fde946b7486fb8a4b9e2a25109f38d12654bd27ad1eaff402494f5f29a4e->leave($internal0ee3fde946b7486fb8a4b9e2a25109f38d12654bd27ad1eaff402494f5f29a4eprof);

        
        $internal9175bdb23b6b78cb420092d3ab0c827657cf0b46f05f8fdd5d593ddda3d35b39->leave($internal9175bdb23b6b78cb420092d3ab0c827657cf0b46f05f8fdd5d593ddda3d35b39prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Helper:short-object-description.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 7,  38 => 5,  30 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<span class=\"inner-field-short-description\">
    {% if object and admin.hasRoute('edit') and admin.hasAccess('edit') %}
        <a href=\"{{ admin.generateObjectUrl('edit', object, linkparameters) }}\" target=\"new\">{{ description }}</a>
    {% else %}
        {{ description }}
    {% endif %}
</span>
", "SonataAdminBundle:Helper:short-object-description.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Helper/short-object-description.html.twig");
    }
}
