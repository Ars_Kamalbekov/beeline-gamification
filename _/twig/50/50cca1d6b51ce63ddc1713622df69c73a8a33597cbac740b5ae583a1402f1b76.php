<?php

/* SonataAdminBundle:CRUD:listurl.html.twig */
class TwigTemplatef71bdf6da9b32db26f92c2b84e88adaf0075c2783b71e94029c1464ee2de328a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listurl.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb2ec75284d52466a5dc6cf165f0c0e49c8d3c6e1af7ea316c5d1ed7c2e587c23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb2ec75284d52466a5dc6cf165f0c0e49c8d3c6e1af7ea316c5d1ed7c2e587c23->enter($internalb2ec75284d52466a5dc6cf165f0c0e49c8d3c6e1af7ea316c5d1ed7c2e587c23prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listurl.html.twig"));

        $internalb40645dd6d9a5550db5149b55f92527eb5f540d1dbca3d7d53518fb29985d456 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb40645dd6d9a5550db5149b55f92527eb5f540d1dbca3d7d53518fb29985d456->enter($internalb40645dd6d9a5550db5149b55f92527eb5f540d1dbca3d7d53518fb29985d456prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listurl.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb2ec75284d52466a5dc6cf165f0c0e49c8d3c6e1af7ea316c5d1ed7c2e587c23->leave($internalb2ec75284d52466a5dc6cf165f0c0e49c8d3c6e1af7ea316c5d1ed7c2e587c23prof);

        
        $internalb40645dd6d9a5550db5149b55f92527eb5f540d1dbca3d7d53518fb29985d456->leave($internalb40645dd6d9a5550db5149b55f92527eb5f540d1dbca3d7d53518fb29985d456prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal199e5cf6efed99f1796e98e72f35fd17e90e9163fcd248aed70c0b41b647ca11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal199e5cf6efed99f1796e98e72f35fd17e90e9163fcd248aed70c0b41b647ca11->enter($internal199e5cf6efed99f1796e98e72f35fd17e90e9163fcd248aed70c0b41b647ca11prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal0db533a162db2ef5a0e6f231400ad1c87970b60a585867b759e52ef1dc0ec866 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0db533a162db2ef5a0e6f231400ad1c87970b60a585867b759e52ef1dc0ec866->enter($internal0db533a162db2ef5a0e6f231400ad1c87970b60a585867b759e52ef1dc0ec866prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        obstart();
        // line 16
        echo "    ";
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()))) {
            // line 17
            echo "        &nbsp;
    ";
        } else {
            // line 19
            echo "        ";
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "url", array(), "any", true, true)) {
                // line 20
                echo "            ";
                // line 21
                echo "            ";
                $context["urladdress"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "options", array()), "url", array());
                // line 22
                echo "        ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", true, true) && !twiginfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), array(0 => "edit", 1 => "show")))) {
                // line 23
                echo "            ";
                // line 24
                echo "            ";
                $context["parameters"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "parameters", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "parameters", array()), array())) : (array()));
                // line 25
                echo "
            ";
                // line 27
                echo "            ";
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "identifierparametername", array(), "any", true, true)) {
                    // line 28
                    echo "                ";
                    $context["parameters"] = twigarraymerge((isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 28, $this->getSourceContext()); })()), array(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 28, $this->getSourceContext()); })()), "options", array()), "route", array()), "identifierparametername", array()) => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "normalizedidentifier", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 28, $this->getSourceContext()); })())), "method")));
                    // line 29
                    echo "            ";
                }
                // line 30
                echo "
            ";
                // line 31
                if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "absolute", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "absolute", array()), false)) : (false))) {
                    // line 32
                    echo "                ";
                    $context["urladdress"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 32, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), (isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 32, $this->getSourceContext()); })()));
                    // line 33
                    echo "            ";
                } else {
                    // line 34
                    echo "                ";
                    $context["urladdress"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), (isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 34, $this->getSourceContext()); })()));
                    // line 35
                    echo "            ";
                }
                // line 36
                echo "        ";
            } else {
                // line 37
                echo "            ";
                // line 38
                echo "            ";
                $context["urladdress"] = (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 38, $this->getSourceContext()); })());
                // line 39
                echo "        ";
            }
            // line 40
            echo "
        ";
            // line 41
            if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "hideprotocol", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "hideprotocol", array()), false)) : (false))) {
                // line 42
                echo "            ";
                $context["value"] = twigreplacefilter((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 42, $this->getSourceContext()); })()), array("http://" => "", "https://" => ""));
                // line 43
                echo "        ";
            }
            // line 44
            echo "
        <a
            href=\"";
            // line 46
            echo twigescapefilter($this->env, (isset($context["urladdress"]) || arraykeyexists("urladdress", $context) ? $context["urladdress"] : (function () { throw new TwigErrorRuntime('Variable "urladdress" does not exist.', 46, $this->getSourceContext()); })()), "html", null, true);
            echo "\"";
            // line 47
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "attributes", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "attributes", array()), array())) : (array())));
            foreach ($context['seq'] as $context["attribute"] => $context["value"]) {
                // line 48
                echo "                ";
                echo twigescapefilter($this->env, $context["attribute"], "html", null, true);
                echo "=\"";
                echo twigescapefilter($this->env, $context["value"], "htmlattr");
                echo "\"";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['attribute'], $context['value'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 50
            echo ">";
            // line 51
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 51, $this->getSourceContext()); })()), "html", null, true);
            // line 52
            echo "</a>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal0db533a162db2ef5a0e6f231400ad1c87970b60a585867b759e52ef1dc0ec866->leave($internal0db533a162db2ef5a0e6f231400ad1c87970b60a585867b759e52ef1dc0ec866prof);

        
        $internal199e5cf6efed99f1796e98e72f35fd17e90e9163fcd248aed70c0b41b647ca11->leave($internal199e5cf6efed99f1796e98e72f35fd17e90e9163fcd248aed70c0b41b647ca11prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listurl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 52,  147 => 51,  145 => 50,  135 => 48,  131 => 47,  128 => 46,  124 => 44,  121 => 43,  118 => 42,  116 => 41,  113 => 40,  110 => 39,  107 => 38,  105 => 37,  102 => 36,  99 => 35,  96 => 34,  93 => 33,  90 => 32,  88 => 31,  85 => 30,  82 => 29,  79 => 28,  76 => 27,  73 => 25,  70 => 24,  68 => 23,  65 => 22,  62 => 21,  60 => 20,  57 => 19,  53 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
{% spaceless %}
    {% if value is empty %}
        &nbsp;
    {% else %}
        {% if fielddescription.options.url is defined %}
            {# target url is string #}
            {% set urladdress = fielddescription.options.url %}
        {% elseif fielddescription.options.route is defined and fielddescription.options.route.name not in ['edit', 'show'] %}
            {# target url is Symfony route #}
            {% set parameters = fielddescription.options.route.parameters|default([]) %}

            {# route with paramter related to object ID #}
            {% if fielddescription.options.route.identifierparametername is defined %}
                {% set parameters = parameters|merge({(fielddescription.options.route.identifierparametername):(admin.normalizedidentifier(object))}) %}
            {% endif %}

            {% if fielddescription.options.route.absolute|default(false) %}
                {% set urladdress = url(fielddescription.options.route.name, parameters) %}
            {% else %}
                {% set urladdress = path(fielddescription.options.route.name, parameters) %}
            {% endif %}
        {% else %}
            {# value is url #}
            {% set urladdress = value %}
        {% endif %}

        {% if fielddescription.options.hideprotocol|default(false) %}
            {% set value = value|replace({'http://': '', 'https://': ''}) %}
        {% endif %}

        <a
            href=\"{{ urladdress }}\"
            {%- for attribute, value in fielddescription.options.attributes|default([]) %}
                {{ attribute }}=\"{{ value|escape('htmlattr') }}\"
            {%- endfor -%}
        >
            {{- value -}}
        </a>
    {% endif %}
{% endspaceless %}
{% endblock %}
", "SonataAdminBundle:CRUD:listurl.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listurl.html.twig");
    }
}
