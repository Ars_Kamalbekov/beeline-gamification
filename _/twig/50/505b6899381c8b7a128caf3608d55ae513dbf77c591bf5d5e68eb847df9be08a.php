<?php

/* SonataAdminBundle:CRUD:listtime.html.twig */
class TwigTemplate52743b100c4c9a430cc8ecd22c04e5fb549ed616a55377c452f95a800c79d3d8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listtime.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbb1ad82e58906430de1282c57927f07f74abb67a322a78e495a3ad937c742d38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbb1ad82e58906430de1282c57927f07f74abb67a322a78e495a3ad937c742d38->enter($internalbb1ad82e58906430de1282c57927f07f74abb67a322a78e495a3ad937c742d38prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listtime.html.twig"));

        $internal3bc87447de121899139a3a2cfedb2b98a86d57dd3f9fcbeaeb8932e945b9e6e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3bc87447de121899139a3a2cfedb2b98a86d57dd3f9fcbeaeb8932e945b9e6e7->enter($internal3bc87447de121899139a3a2cfedb2b98a86d57dd3f9fcbeaeb8932e945b9e6e7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listtime.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalbb1ad82e58906430de1282c57927f07f74abb67a322a78e495a3ad937c742d38->leave($internalbb1ad82e58906430de1282c57927f07f74abb67a322a78e495a3ad937c742d38prof);

        
        $internal3bc87447de121899139a3a2cfedb2b98a86d57dd3f9fcbeaeb8932e945b9e6e7->leave($internal3bc87447de121899139a3a2cfedb2b98a86d57dd3f9fcbeaeb8932e945b9e6e7prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal087db3f3a1ce4e00ba977bc287391c1f9ca75996eb5a9ff8429b27790349ee65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal087db3f3a1ce4e00ba977bc287391c1f9ca75996eb5a9ff8429b27790349ee65->enter($internal087db3f3a1ce4e00ba977bc287391c1f9ca75996eb5a9ff8429b27790349ee65prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal54720daad6d74e3d168be565feeda18b7a7f4c42aab1238549f492f4988771ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal54720daad6d74e3d168be565feeda18b7a7f4c42aab1238549f492f4988771ce->enter($internal54720daad6d74e3d168be565feeda18b7a7f4c42aab1238549f492f4988771ceprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), "H:i:s"), "html", null, true);
        }
        
        $internal54720daad6d74e3d168be565feeda18b7a7f4c42aab1238549f492f4988771ce->leave($internal54720daad6d74e3d168be565feeda18b7a7f4c42aab1238549f492f4988771ceprof);

        
        $internal087db3f3a1ce4e00ba977bc287391c1f9ca75996eb5a9ff8429b27790349ee65->leave($internal087db3f3a1ce4e00ba977bc287391c1f9ca75996eb5a9ff8429b27790349ee65prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listtime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 18,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:listtime.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listtime.html.twig");
    }
}
