<?php

/* SonataBlockBundle:Block:blockcorerss.html.twig */
class TwigTemplate921d2f8af22ddf03871d5c9be28304124c8f935d259c2789d07dcfc1b51b830e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 11, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockcorerss.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1eaa4f679443ba3ce592916cc21bccfbc3625066c10a2fe3ed6e4937c76e1cd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1eaa4f679443ba3ce592916cc21bccfbc3625066c10a2fe3ed6e4937c76e1cd6->enter($internal1eaa4f679443ba3ce592916cc21bccfbc3625066c10a2fe3ed6e4937c76e1cd6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcorerss.html.twig"));

        $internal9e412cfd1fbaa6af4078a689e62b1d59a2509e21ec3fcbebbf3d9e083eaa4fd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9e412cfd1fbaa6af4078a689e62b1d59a2509e21ec3fcbebbf3d9e083eaa4fd9->enter($internal9e412cfd1fbaa6af4078a689e62b1d59a2509e21ec3fcbebbf3d9e083eaa4fd9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcorerss.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal1eaa4f679443ba3ce592916cc21bccfbc3625066c10a2fe3ed6e4937c76e1cd6->leave($internal1eaa4f679443ba3ce592916cc21bccfbc3625066c10a2fe3ed6e4937c76e1cd6prof);

        
        $internal9e412cfd1fbaa6af4078a689e62b1d59a2509e21ec3fcbebbf3d9e083eaa4fd9->leave($internal9e412cfd1fbaa6af4078a689e62b1d59a2509e21ec3fcbebbf3d9e083eaa4fd9prof);

    }

    // line 13
    public function blockblock($context, array $blocks = array())
    {
        $internal993a94df418eac275c981f0a5d2406c15522fb9c299eedd21b28fae2fb90706a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal993a94df418eac275c981f0a5d2406c15522fb9c299eedd21b28fae2fb90706a->enter($internal993a94df418eac275c981f0a5d2406c15522fb9c299eedd21b28fae2fb90706aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internale2a77dcab9f5db73da464bc45b0dee78a254b0b6a933acfcf23a8f92f9578674 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale2a77dcab9f5db73da464bc45b0dee78a254b0b6a933acfcf23a8f92f9578674->enter($internale2a77dcab9f5db73da464bc45b0dee78a254b0b6a933acfcf23a8f92f9578674prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 14
        echo "    <h3 class=\"sonata-feed-title\">";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 14, $this->getSourceContext()); })()), "title", array()), "html", null, true);
        echo "</h3>

    <div class=\"sonata-feeds-container\">
        ";
        // line 17
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["feeds"]) || arraykeyexists("feeds", $context) ? $context["feeds"] : (function () { throw new TwigErrorRuntime('Variable "feeds" does not exist.', 17, $this->getSourceContext()); })()));
        $context['iterated'] = false;
        foreach ($context['seq'] as $context["key"] => $context["feed"]) {
            // line 18
            echo "            <div>
                <strong><a href=\"";
            // line 19
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "link", array()), "html", null, true);
            echo "\" rel=\"nofollow\" title=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "title", array()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "title", array()), "html", null, true);
            echo "</a></strong>
                <div>";
            // line 20
            echo twiggetattribute($this->env, $this->getSourceContext(), $context["feed"], "description", array());
            echo "</div>
            </div>
        ";
            $context['iterated'] = true;
        }
        if (!$context['iterated']) {
            // line 23
            echo "                No feeds available.
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['feed'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 25
        echo "    </div>
";
        
        $internale2a77dcab9f5db73da464bc45b0dee78a254b0b6a933acfcf23a8f92f9578674->leave($internale2a77dcab9f5db73da464bc45b0dee78a254b0b6a933acfcf23a8f92f9578674prof);

        
        $internal993a94df418eac275c981f0a5d2406c15522fb9c299eedd21b28fae2fb90706a->leave($internal993a94df418eac275c981f0a5d2406c15522fb9c299eedd21b28fae2fb90706aprof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockcorerss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 25,  79 => 23,  71 => 20,  63 => 19,  60 => 18,  55 => 17,  48 => 14,  39 => 13,  18 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends sonatablock.templates.blockbase %}

{% block block %}
    <h3 class=\"sonata-feed-title\">{{ settings.title }}</h3>

    <div class=\"sonata-feeds-container\">
        {% for feed in feeds %}
            <div>
                <strong><a href=\"{{ feed.link}}\" rel=\"nofollow\" title=\"{{ feed.title }}\">{{ feed.title }}</a></strong>
                <div>{{ feed.description|raw }}</div>
            </div>
        {% else %}
                No feeds available.
        {% endfor %}
    </div>
{% endblock %}
", "SonataBlockBundle:Block:blockcorerss.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockcorerss.html.twig");
    }
}
