<?php

/* @Framework/Form/formwidgetsimple.html.php */
class TwigTemplate8654e792d9d9e0f54474310d9e923f767c18f716cbcd7137e57e8dd1a1e456df extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal14d8d04015daa7b2d602fa49ea67f064ccda68a2f4eeea1ef7a1b219126163fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal14d8d04015daa7b2d602fa49ea67f064ccda68a2f4eeea1ef7a1b219126163fe->enter($internal14d8d04015daa7b2d602fa49ea67f064ccda68a2f4eeea1ef7a1b219126163feprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidgetsimple.html.php"));

        $internal0b047fcec1d0c413b1f5d72e588a1c387556f21a8a1da9d53a54f14fa54cc633 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0b047fcec1d0c413b1f5d72e588a1c387556f21a8a1da9d53a54f14fa54cc633->enter($internal0b047fcec1d0c413b1f5d72e588a1c387556f21a8a1da9d53a54f14fa54cc633prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidgetsimple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widgetattributes') ?><?php if (!empty(\$value) || isnumeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $internal14d8d04015daa7b2d602fa49ea67f064ccda68a2f4eeea1ef7a1b219126163fe->leave($internal14d8d04015daa7b2d602fa49ea67f064ccda68a2f4eeea1ef7a1b219126163feprof);

        
        $internal0b047fcec1d0c413b1f5d72e588a1c387556f21a8a1da9d53a54f14fa54cc633->leave($internal0b047fcec1d0c413b1f5d72e588a1c387556f21a8a1da9d53a54f14fa54cc633prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formwidgetsimple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widgetattributes') ?><?php if (!empty(\$value) || isnumeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/formwidgetsimple.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formwidgetsimple.html.php");
    }
}
