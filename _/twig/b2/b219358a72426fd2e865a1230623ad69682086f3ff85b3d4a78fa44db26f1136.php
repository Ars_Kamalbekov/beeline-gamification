<?php

/* SonataAdminBundle:CRUD:baseedit.html.twig */
class TwigTemplate2407332988e119cf7da119d7e02629ee8494a9b64e55d4f458293e76ed60773d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $trait0 = $this->loadTemplate("SonataAdminBundle:CRUD:baseeditform.html.twig", "SonataAdminBundle:CRUD:baseedit.html.twig", 32);
        // line 32
        if (!$trait0->isTraitable()) {
            throw new TwigErrorRuntime('Template "'."SonataAdminBundle:CRUD:baseeditform.html.twig".'" cannot be used as a trait.');
        }
        $trait0blocks = $trait0->getBlocks();

        if (!isset($trait0blocks["form"])) {
            throw new TwigErrorRuntime(sprintf('Block "form" is not defined in trait "SonataAdminBundle:CRUD:baseeditform.html.twig".'));
        }

        $trait0blocks["parentForm"] = $trait0blocks["form"]; unset($trait0blocks["form"]);

        $this->traits = $trait0blocks;

        $this->blocks = arraymerge(
            $this->traits,
            array(
                'title' => array($this, 'blocktitle'),
                'navbartitle' => array($this, 'blocknavbartitle'),
                'actions' => array($this, 'blockactions'),
                'tabmenu' => array($this, 'blocktabmenu'),
                'form' => array($this, 'blockform'),
            )
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:baseedit.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3b10c6342a09f1aa0deeb95c93d25209569efac39f0ff3eab12dbc8c375000cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3b10c6342a09f1aa0deeb95c93d25209569efac39f0ff3eab12dbc8c375000cc->enter($internal3b10c6342a09f1aa0deeb95c93d25209569efac39f0ff3eab12dbc8c375000ccprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseedit.html.twig"));

        $internal4a8aeebb92d814db0d97756ee0b019a2a45a015ceed55e3d34cf5aecf02c5396 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4a8aeebb92d814db0d97756ee0b019a2a45a015ceed55e3d34cf5aecf02c5396->enter($internal4a8aeebb92d814db0d97756ee0b019a2a45a015ceed55e3d34cf5aecf02c5396prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseedit.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal3b10c6342a09f1aa0deeb95c93d25209569efac39f0ff3eab12dbc8c375000cc->leave($internal3b10c6342a09f1aa0deeb95c93d25209569efac39f0ff3eab12dbc8c375000ccprof);

        
        $internal4a8aeebb92d814db0d97756ee0b019a2a45a015ceed55e3d34cf5aecf02c5396->leave($internal4a8aeebb92d814db0d97756ee0b019a2a45a015ceed55e3d34cf5aecf02c5396prof);

    }

    // line 14
    public function blocktitle($context, array $blocks = array())
    {
        $internalaaa99394fa0346351fc262135d93324fd467ba7138d240ca3c3f3843b1adfd2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalaaa99394fa0346351fc262135d93324fd467ba7138d240ca3c3f3843b1adfd2b->enter($internalaaa99394fa0346351fc262135d93324fd467ba7138d240ca3c3f3843b1adfd2bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internaldb7024e69e9b449e1612492fdd4e4d9926f04f7d4f11533051f7a26bd658e8ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldb7024e69e9b449e1612492fdd4e4d9926f04f7d4f11533051f7a26bd658e8ed->enter($internaldb7024e69e9b449e1612492fdd4e4d9926f04f7d4f11533051f7a26bd658e8edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"))) {
            // line 16
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titleedit", array("%name%" => twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 16, $this->getSourceContext()); })()), "toString", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 16, $this->getSourceContext()); })())), "method"), 15)), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 18
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titlecreate", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        
        $internaldb7024e69e9b449e1612492fdd4e4d9926f04f7d4f11533051f7a26bd658e8ed->leave($internaldb7024e69e9b449e1612492fdd4e4d9926f04f7d4f11533051f7a26bd658e8edprof);

        
        $internalaaa99394fa0346351fc262135d93324fd467ba7138d240ca3c3f3843b1adfd2b->leave($internalaaa99394fa0346351fc262135d93324fd467ba7138d240ca3c3f3843b1adfd2bprof);

    }

    // line 22
    public function blocknavbartitle($context, array $blocks = array())
    {
        $internalb829dae839f31e1afcceef2569989d821b5ea721e75fb384b559b18d64c372fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb829dae839f31e1afcceef2569989d821b5ea721e75fb384b559b18d64c372fe->enter($internalb829dae839f31e1afcceef2569989d821b5ea721e75fb384b559b18d64c372feprof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        $internal0d2e709097a49dfd4002c169056d04bc4c416a5af6401465f4b95d73251549ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0d2e709097a49dfd4002c169056d04bc4c416a5af6401465f4b95d73251549ea->enter($internal0d2e709097a49dfd4002c169056d04bc4c416a5af6401465f4b95d73251549eaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        // line 23
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $internal0d2e709097a49dfd4002c169056d04bc4c416a5af6401465f4b95d73251549ea->leave($internal0d2e709097a49dfd4002c169056d04bc4c416a5af6401465f4b95d73251549eaprof);

        
        $internalb829dae839f31e1afcceef2569989d821b5ea721e75fb384b559b18d64c372fe->leave($internalb829dae839f31e1afcceef2569989d821b5ea721e75fb384b559b18d64c372feprof);

    }

    // line 26
    public function blockactions($context, array $blocks = array())
    {
        $internal1bdae4afe5db4b301bd4a34c71ca53213a6c6036107d949baa8a651b17e174d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1bdae4afe5db4b301bd4a34c71ca53213a6c6036107d949baa8a651b17e174d6->enter($internal1bdae4afe5db4b301bd4a34c71ca53213a6c6036107d949baa8a651b17e174d6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internala0a5977e02e759e9f17247edde3e6188d59a17903f4b162d1a61765454e4de2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala0a5977e02e759e9f17247edde3e6188d59a17903f4b162d1a61765454e4de2b->enter($internala0a5977e02e759e9f17247edde3e6188d59a17903f4b162d1a61765454e4de2bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 27
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:baseedit.html.twig", 27)->display($context);
        
        $internala0a5977e02e759e9f17247edde3e6188d59a17903f4b162d1a61765454e4de2b->leave($internala0a5977e02e759e9f17247edde3e6188d59a17903f4b162d1a61765454e4de2bprof);

        
        $internal1bdae4afe5db4b301bd4a34c71ca53213a6c6036107d949baa8a651b17e174d6->leave($internal1bdae4afe5db4b301bd4a34c71ca53213a6c6036107d949baa8a651b17e174d6prof);

    }

    // line 30
    public function blocktabmenu($context, array $blocks = array())
    {
        $internalbcc44e93bd6b9110a10a9c330ab18bd9c7e117887a9cfd0322c208a1a7eb4a0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbcc44e93bd6b9110a10a9c330ab18bd9c7e117887a9cfd0322c208a1a7eb4a0e->enter($internalbcc44e93bd6b9110a10a9c330ab18bd9c7e117887a9cfd0322c208a1a7eb4a0eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal189c473ea1f928251e48165f99867cdd6e0cb5ca239a487f08ad788e7f4ffead = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal189c473ea1f928251e48165f99867cdd6e0cb5ca239a487f08ad788e7f4ffead->enter($internal189c473ea1f928251e48165f99867cdd6e0cb5ca239a487f08ad788e7f4ffeadprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 30, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 30, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 30, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
        
        $internal189c473ea1f928251e48165f99867cdd6e0cb5ca239a487f08ad788e7f4ffead->leave($internal189c473ea1f928251e48165f99867cdd6e0cb5ca239a487f08ad788e7f4ffeadprof);

        
        $internalbcc44e93bd6b9110a10a9c330ab18bd9c7e117887a9cfd0322c208a1a7eb4a0e->leave($internalbcc44e93bd6b9110a10a9c330ab18bd9c7e117887a9cfd0322c208a1a7eb4a0eprof);

    }

    // line 34
    public function blockform($context, array $blocks = array())
    {
        $internald9a41d97a2d53ee6795d2172b0077344ffd7d19f2f466506aa8d9b24a02999b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald9a41d97a2d53ee6795d2172b0077344ffd7d19f2f466506aa8d9b24a02999b6->enter($internald9a41d97a2d53ee6795d2172b0077344ffd7d19f2f466506aa8d9b24a02999b6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        $internal80a08c6d19772e5d652cc03e1d8c2649e9e55b4359a9398cdf43ff2e23d86bea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal80a08c6d19772e5d652cc03e1d8c2649e9e55b4359a9398cdf43ff2e23d86bea->enter($internal80a08c6d19772e5d652cc03e1d8c2649e9e55b4359a9398cdf43ff2e23d86beaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        // line 35
        echo "    ";
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
        
        $internal80a08c6d19772e5d652cc03e1d8c2649e9e55b4359a9398cdf43ff2e23d86bea->leave($internal80a08c6d19772e5d652cc03e1d8c2649e9e55b4359a9398cdf43ff2e23d86beaprof);

        
        $internald9a41d97a2d53ee6795d2172b0077344ffd7d19f2f466506aa8d9b24a02999b6->leave($internald9a41d97a2d53ee6795d2172b0077344ffd7d19f2f466506aa8d9b24a02999b6prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 35,  152 => 34,  134 => 30,  124 => 27,  115 => 26,  102 => 23,  93 => 22,  79 => 18,  73 => 16,  70 => 15,  61 => 14,  40 => 12,  12 => 32,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block title %}
    {% if admin.id(object) is not null %}
        {{ \"titleedit\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
    {% else %}
        {{ \"titlecreate\"|trans({}, 'SonataAdminBundle') }}
    {% endif %}
{% endblock %}

{% block navbartitle %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}{{ knpmenurender(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')}, 'twig') }}{% endblock %}

{% use 'SonataAdminBundle:CRUD:baseeditform.html.twig' with form as parentForm %}

{% block form %}
    {{ block('parentForm') }}
{% endblock %}
", "SonataAdminBundle:CRUD:baseedit.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseedit.html.twig");
    }
}
