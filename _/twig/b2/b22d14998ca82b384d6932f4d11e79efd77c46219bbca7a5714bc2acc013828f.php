<?php

/* @Framework/Form/hiddenwidget.html.php */
class TwigTemplate0f1d6029143a4832d84ecd074d2450a44e729020628850135f6231c9e2d3cf2f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7ecf331686f450e95876f025f5c1755b5a61dd9e9b3b7bbfb6a2c31098b28d05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7ecf331686f450e95876f025f5c1755b5a61dd9e9b3b7bbfb6a2c31098b28d05->enter($internal7ecf331686f450e95876f025f5c1755b5a61dd9e9b3b7bbfb6a2c31098b28d05prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/hiddenwidget.html.php"));

        $internal20c24adfaf7cfee75b3587a8d1575e45187e7be0b15b910238e7b3e1cdc09dcf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal20c24adfaf7cfee75b3587a8d1575e45187e7be0b15b910238e7b3e1cdc09dcf->enter($internal20c24adfaf7cfee75b3587a8d1575e45187e7be0b15b910238e7b3e1cdc09dcfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/hiddenwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $internal7ecf331686f450e95876f025f5c1755b5a61dd9e9b3b7bbfb6a2c31098b28d05->leave($internal7ecf331686f450e95876f025f5c1755b5a61dd9e9b3b7bbfb6a2c31098b28d05prof);

        
        $internal20c24adfaf7cfee75b3587a8d1575e45187e7be0b15b910238e7b3e1cdc09dcf->leave($internal20c24adfaf7cfee75b3587a8d1575e45187e7be0b15b910238e7b3e1cdc09dcfprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hiddenwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hiddenwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hiddenwidget.html.php");
    }
}
