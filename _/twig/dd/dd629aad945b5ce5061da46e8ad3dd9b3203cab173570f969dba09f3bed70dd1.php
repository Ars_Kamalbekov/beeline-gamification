<?php

/* FOSUserBundle:Registration:email.txt.twig */
class TwigTemplatebeb442045c4466f78034c81df40b331ff5a10e8fe916d0bc5032137f5672b476 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'blocksubject'),
            'bodytext' => array($this, 'blockbodytext'),
            'bodyhtml' => array($this, 'blockbodyhtml'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald35eb7e438f9979525433f65e3f462f15cb9e6a6f6ddb1b0ac612670a19c2c50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald35eb7e438f9979525433f65e3f462f15cb9e6a6f6ddb1b0ac612670a19c2c50->enter($internald35eb7e438f9979525433f65e3f462f15cb9e6a6f6ddb1b0ac612670a19c2c50prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $internal51dd171185d649e5260fd9f8d63f72fd3fe3cea81d48ec29e11c622a7b14707f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal51dd171185d649e5260fd9f8d63f72fd3fe3cea81d48ec29e11c622a7b14707f->enter($internal51dd171185d649e5260fd9f8d63f72fd3fe3cea81d48ec29e11c622a7b14707fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('bodytext', $context, $blocks);
        // line 13
        $this->displayBlock('bodyhtml', $context, $blocks);
        
        $internald35eb7e438f9979525433f65e3f462f15cb9e6a6f6ddb1b0ac612670a19c2c50->leave($internald35eb7e438f9979525433f65e3f462f15cb9e6a6f6ddb1b0ac612670a19c2c50prof);

        
        $internal51dd171185d649e5260fd9f8d63f72fd3fe3cea81d48ec29e11c622a7b14707f->leave($internal51dd171185d649e5260fd9f8d63f72fd3fe3cea81d48ec29e11c622a7b14707fprof);

    }

    // line 2
    public function blocksubject($context, array $blocks = array())
    {
        $internalb40d403587b4a7901ff67fb51e8453d47b698443d3dd4ab917f4e7fcb0fe8e91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb40d403587b4a7901ff67fb51e8453d47b698443d3dd4ab917f4e7fcb0fe8e91->enter($internalb40d403587b4a7901ff67fb51e8453d47b698443d3dd4ab917f4e7fcb0fe8e91prof = new TwigProfilerProfile($this->getTemplateName(), "block", "subject"));

        $internale750769dfcbe610e806bd8cd94f97db07c1e4e3aeb8b40f071121e000e38276d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale750769dfcbe610e806bd8cd94f97db07c1e4e3aeb8b40f071121e000e38276d->enter($internale750769dfcbe610e806bd8cd94f97db07c1e4e3aeb8b40f071121e000e38276dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || arraykeyexists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new TwigErrorRuntime('Variable "confirmationUrl" does not exist.', 4, $this->getSourceContext()); })())), "FOSUserBundle");
        
        $internale750769dfcbe610e806bd8cd94f97db07c1e4e3aeb8b40f071121e000e38276d->leave($internale750769dfcbe610e806bd8cd94f97db07c1e4e3aeb8b40f071121e000e38276dprof);

        
        $internalb40d403587b4a7901ff67fb51e8453d47b698443d3dd4ab917f4e7fcb0fe8e91->leave($internalb40d403587b4a7901ff67fb51e8453d47b698443d3dd4ab917f4e7fcb0fe8e91prof);

    }

    // line 8
    public function blockbodytext($context, array $blocks = array())
    {
        $internalf41512927d8d5785b426a5d1dfebb8b0d92f87249bf5e2fa4c140d5f619ce14d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf41512927d8d5785b426a5d1dfebb8b0d92f87249bf5e2fa4c140d5f619ce14d->enter($internalf41512927d8d5785b426a5d1dfebb8b0d92f87249bf5e2fa4c140d5f619ce14dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodytext"));

        $internal9adf80eb90b9d36d6b414c06b498259f2ce7e9d1bd8d99a2111e489dba873b86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9adf80eb90b9d36d6b414c06b498259f2ce7e9d1bd8d99a2111e489dba873b86->enter($internal9adf80eb90b9d36d6b414c06b498259f2ce7e9d1bd8d99a2111e489dba873b86prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodytext"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || arraykeyexists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new TwigErrorRuntime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $internal9adf80eb90b9d36d6b414c06b498259f2ce7e9d1bd8d99a2111e489dba873b86->leave($internal9adf80eb90b9d36d6b414c06b498259f2ce7e9d1bd8d99a2111e489dba873b86prof);

        
        $internalf41512927d8d5785b426a5d1dfebb8b0d92f87249bf5e2fa4c140d5f619ce14d->leave($internalf41512927d8d5785b426a5d1dfebb8b0d92f87249bf5e2fa4c140d5f619ce14dprof);

    }

    // line 13
    public function blockbodyhtml($context, array $blocks = array())
    {
        $internalc98705db1f7a51b11ba4a57c39fa1bd913c0e373fac055c7278d3b8e615e397d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc98705db1f7a51b11ba4a57c39fa1bd913c0e373fac055c7278d3b8e615e397d->enter($internalc98705db1f7a51b11ba4a57c39fa1bd913c0e373fac055c7278d3b8e615e397dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyhtml"));

        $internala5d57de75da851461e1ddebf246ff4ef043e66bea9cda152ab3153e7a9889e15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala5d57de75da851461e1ddebf246ff4ef043e66bea9cda152ab3153e7a9889e15->enter($internala5d57de75da851461e1ddebf246ff4ef043e66bea9cda152ab3153e7a9889e15prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyhtml"));

        
        $internala5d57de75da851461e1ddebf246ff4ef043e66bea9cda152ab3153e7a9889e15->leave($internala5d57de75da851461e1ddebf246ff4ef043e66bea9cda152ab3153e7a9889e15prof);

        
        $internalc98705db1f7a51b11ba4a57c39fa1bd913c0e373fac055c7278d3b8e615e397d->leave($internalc98705db1f7a51b11ba4a57c39fa1bd913c0e373fac055c7278d3b8e615e397dprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block bodytext %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block bodyhtml %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Registration/email.txt.twig");
    }
}
