<?php

/* SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig */
class TwigTemplate77ef80c7444c32a6556ef6661bc96ec3401033c79fc09847adc765ab91ce4c75 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'relationlink' => array($this, 'blockrelationlink'),
            'relationvalue' => array($this, 'blockrelationvalue'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4ee768403618a42ac68b3d0092ed33ba6d0d29890749ddca29e27a1e95d5127d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4ee768403618a42ac68b3d0092ed33ba6d0d29890749ddca29e27a1e95d5127d->enter($internal4ee768403618a42ac68b3d0092ed33ba6d0d29890749ddca29e27a1e95d5127dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig"));

        $internal5b6deb53d0b4d39eb667ff563b320e16e1189b3fe529aaeb50d9dd72c96ea13e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5b6deb53d0b4d39eb667ff563b320e16e1189b3fe529aaeb50d9dd72c96ea13e->enter($internal5b6deb53d0b4d39eb667ff563b320e16e1189b3fe529aaeb50d9dd72c96ea13eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal4ee768403618a42ac68b3d0092ed33ba6d0d29890749ddca29e27a1e95d5127d->leave($internal4ee768403618a42ac68b3d0092ed33ba6d0d29890749ddca29e27a1e95d5127dprof);

        
        $internal5b6deb53d0b4d39eb667ff563b320e16e1189b3fe529aaeb50d9dd72c96ea13e->leave($internal5b6deb53d0b4d39eb667ff563b320e16e1189b3fe529aaeb50d9dd72c96ea13eprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal2c67a5c333773665c1af1225cb98493a6db5cde26011b831e3de4ac7ef0fe37b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2c67a5c333773665c1af1225cb98493a6db5cde26011b831e3de4ac7ef0fe37b->enter($internal2c67a5c333773665c1af1225cb98493a6db5cde26011b831e3de4ac7ef0fe37bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internaled39e28275fa098caf0ea6d66b7695ba74d74a1f2616b23cfed6f0d13ed1a134 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaled39e28275fa098caf0ea6d66b7695ba74d74a1f2616b23cfed6f0d13ed1a134->enter($internaled39e28275fa098caf0ea6d66b7695ba74d74a1f2616b23cfed6f0d13ed1a134prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 16, $this->getSourceContext()); })())), "method"))) {
            // line 17
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 18
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })()), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relationlink", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relationvalue", $context, $blocks);
                }
                // line 23
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 26, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relationvalue", $context, $blocks);
                echo "
            ";
                // line 28
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 29
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 30
            echo "    ";
        }
        
        $internaled39e28275fa098caf0ea6d66b7695ba74d74a1f2616b23cfed6f0d13ed1a134->leave($internaled39e28275fa098caf0ea6d66b7695ba74d74a1f2616b23cfed6f0d13ed1a134prof);

        
        $internal2c67a5c333773665c1af1225cb98493a6db5cde26011b831e3de4ac7ef0fe37b->leave($internal2c67a5c333773665c1af1225cb98493a6db5cde26011b831e3de4ac7ef0fe37bprof);

    }

    // line 33
    public function blockrelationlink($context, array $blocks = array())
    {
        $internal4aafe4283781fd4b063a6b92e96fd2cb045d1c24221395d0b2543af2b600438d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4aafe4283781fd4b063a6b92e96fd2cb045d1c24221395d0b2543af2b600438d->enter($internal4aafe4283781fd4b063a6b92e96fd2cb045d1c24221395d0b2543af2b600438dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        $internala53a35d809b1795923b7fe01c25b2a1663fba6f90b94bacc4152e07187723bd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala53a35d809b1795923b7fe01c25b2a1663fba6f90b94bacc4152e07187723bd8->enter($internala53a35d809b1795923b7fe01c25b2a1663fba6f90b94bacc4152e07187723bd8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        // line 34
        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 34, $this->getSourceContext()); })()), 1 => (isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 34, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 35
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 35, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 35, $this->getSourceContext()); })())), "html", null, true);
        // line 36
        echo "</a>";
        
        $internala53a35d809b1795923b7fe01c25b2a1663fba6f90b94bacc4152e07187723bd8->leave($internala53a35d809b1795923b7fe01c25b2a1663fba6f90b94bacc4152e07187723bd8prof);

        
        $internal4aafe4283781fd4b063a6b92e96fd2cb045d1c24221395d0b2543af2b600438d->leave($internal4aafe4283781fd4b063a6b92e96fd2cb045d1c24221395d0b2543af2b600438dprof);

    }

    // line 39
    public function blockrelationvalue($context, array $blocks = array())
    {
        $internale6c74f1f292640a185e259a9e4450213044dde9909295b04526eeddea0f1027c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale6c74f1f292640a185e259a9e4450213044dde9909295b04526eeddea0f1027c->enter($internale6c74f1f292640a185e259a9e4450213044dde9909295b04526eeddea0f1027cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        $internaleeabd60235223382cfff91ad087a9a534a3bcde33f5340adcfc54516ed3af54f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaleeabd60235223382cfff91ad087a9a534a3bcde33f5340adcfc54516ed3af54f->enter($internaleeabd60235223382cfff91ad087a9a534a3bcde33f5340adcfc54516ed3af54fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        // line 40
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 40, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 40, $this->getSourceContext()); })())), "html", null, true);
        
        $internaleeabd60235223382cfff91ad087a9a534a3bcde33f5340adcfc54516ed3af54f->leave($internaleeabd60235223382cfff91ad087a9a534a3bcde33f5340adcfc54516ed3af54fprof);

        
        $internale6c74f1f292640a185e259a9e4450213044dde9909295b04526eeddea0f1027c->leave($internale6c74f1f292640a185e259a9e4450213044dde9909295b04526eeddea0f1027cprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 40,  180 => 39,  170 => 36,  168 => 35,  164 => 34,  155 => 33,  144 => 30,  130 => 29,  126 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasassociationadmin and fielddescription.associationadmin.hasRoute(routename) %}
        {% for element in value %}
            {%- if fielddescription.associationadmin.hasAccess(routename, element) -%}
                {{ block('relationlink') }}
            {%- else -%}
                {{ block('relationvalue') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value%}
            {{ block('relationvalue') }}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relationlink -%}
    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
        {{- element|renderrelationelement(fielddescription) -}}
    </a>
{%- endblock -%}

{%- block relationvalue -%}
    {{- element|renderrelationelement(fielddescription) -}}
{%- endblock -%}
", "SonataDoctrineORMAdminBundle:CRUD:listormmanytomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/listormmanytomany.html.twig");
    }
}
