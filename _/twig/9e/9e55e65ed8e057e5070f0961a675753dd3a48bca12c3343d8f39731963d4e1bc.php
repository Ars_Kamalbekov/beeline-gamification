<?php

/* SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig */
class TwigTemplate58bfeaf4bb993994d2e1e129fcc175d6fc9d7af7aa9f8066e6d4121e6b1376e2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal10de9e668a08b04732d4eff152d728be3af8772c5c350d6f6896523aef8d4bc7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal10de9e668a08b04732d4eff152d728be3af8772c5c350d6f6896523aef8d4bc7->enter($internal10de9e668a08b04732d4eff152d728be3af8772c5c350d6f6896523aef8d4bc7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig"));

        $internala7b68e29278df82d825a99f2f04dd00524c0c0f1772fe5e10b944e03a5f85ae1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala7b68e29278df82d825a99f2f04dd00524c0c0f1772fe5e10b944e03a5f85ae1->enter($internala7b68e29278df82d825a99f2f04dd00524c0c0f1772fe5e10b944e03a5f85ae1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal10de9e668a08b04732d4eff152d728be3af8772c5c350d6f6896523aef8d4bc7->leave($internal10de9e668a08b04732d4eff152d728be3af8772c5c350d6f6896523aef8d4bc7prof);

        
        $internala7b68e29278df82d825a99f2f04dd00524c0c0f1772fe5e10b944e03a5f85ae1->leave($internala7b68e29278df82d825a99f2f04dd00524c0c0f1772fe5e10b944e03a5f85ae1prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal3c7ee2a5f85823154986c55271ad3d14314bc44af15a5f9c03a0fcce04e68f96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3c7ee2a5f85823154986c55271ad3d14314bc44af15a5f9c03a0fcce04e68f96->enter($internal3c7ee2a5f85823154986c55271ad3d14314bc44af15a5f9c03a0fcce04e68f96prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal2a6cb8213d9bba2bc00dc9d285055d0a387c17c03ad373d08df0ea98ddfa3858 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2a6cb8213d9bba2bc00dc9d285055d0a387c17c03ad373d08df0ea98ddfa3858->enter($internal2a6cb8213d9bba2bc00dc9d285055d0a387c17c03ad373d08df0ea98ddfa3858prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <ul class=\"sonata-ba-show-one-to-many\">
    ";
        // line 16
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 17
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["element"]) {
            // line 18
            echo "        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 20
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => $context["element"]), "method"))) {
                // line 21
                echo "            <li>
                <a href=\"";
                // line 22
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 22, $this->getSourceContext()); })()), 1 => $context["element"], 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                    ";
                // line 23
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
                echo "
                </a>
            </li>
        ";
            } else {
                // line 27
                echo "            <li>";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 27, $this->getSourceContext()); })())), "html", null, true);
                echo "</li>
        ";
            }
            // line 29
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 30
        echo "    </ul>
";
        
        $internal2a6cb8213d9bba2bc00dc9d285055d0a387c17c03ad373d08df0ea98ddfa3858->leave($internal2a6cb8213d9bba2bc00dc9d285055d0a387c17c03ad373d08df0ea98ddfa3858prof);

        
        $internal3c7ee2a5f85823154986c55271ad3d14314bc44af15a5f9c03a0fcce04e68f96->leave($internal3c7ee2a5f85823154986c55271ad3d14314bc44af15a5f9c03a0fcce04e68f96prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 30,  84 => 29,  78 => 27,  71 => 23,  67 => 22,  64 => 21,  62 => 20,  61 => 19,  59 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    <ul class=\"sonata-ba-show-one-to-many\">
    {% set routename = fielddescription.options.route.name %}
    {% for element in value%}
        {% if fielddescription.hasassociationadmin
        and fielddescription.associationadmin.hasRoute(routename)
        and fielddescription.associationadmin.hasAccess(routename, element) %}
            <li>
                <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
                    {{ element|renderrelationelement(fielddescription) }}
                </a>
            </li>
        {% else %}
            <li>{{ element|renderrelationelement(fielddescription) }}</li>
        {% endif %}
    {% endfor %}
    </ul>
{% endblock %}
", "SonataDoctrineORMAdminBundle:CRUD:showormonetomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/showormonetomany.html.twig");
    }
}
