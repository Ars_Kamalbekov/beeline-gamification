<?php

/* @Framework/FormTable/formrow.html.php */
class TwigTemplate6026ce6a9cb2bf989e7445be81a44888666ab900a39de3f0a69117225e19d826 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3004316d2fbf9818f056f6f2d0384e4995f1c7e9eb5a148845b463bad67d3388 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3004316d2fbf9818f056f6f2d0384e4995f1c7e9eb5a148845b463bad67d3388->enter($internal3004316d2fbf9818f056f6f2d0384e4995f1c7e9eb5a148845b463bad67d3388prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/formrow.html.php"));

        $internal92629acbbb4684fe7fa8593b814fb9f4d52403e474131ff1551a0504ff3a7a9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal92629acbbb4684fe7fa8593b814fb9f4d52403e474131ff1551a0504ff3a7a9a->enter($internal92629acbbb4684fe7fa8593b814fb9f4d52403e474131ff1551a0504ff3a7a9aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/formrow.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $internal3004316d2fbf9818f056f6f2d0384e4995f1c7e9eb5a148845b463bad67d3388->leave($internal3004316d2fbf9818f056f6f2d0384e4995f1c7e9eb5a148845b463bad67d3388prof);

        
        $internal92629acbbb4684fe7fa8593b814fb9f4d52403e474131ff1551a0504ff3a7a9a->leave($internal92629acbbb4684fe7fa8593b814fb9f4d52403e474131ff1551a0504ff3a7a9aprof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/formrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/formrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/formrow.html.php");
    }
}
