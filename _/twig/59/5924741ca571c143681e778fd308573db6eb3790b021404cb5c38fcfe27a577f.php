<?php

/* SonataAdminBundle:CRUD:showemail.html.twig */
class TwigTemplateeb2aabd2fc423be44ba0457e932fcfa4dcf0beb1be9295b1e35962cfda430158 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showemail.html.twig", 1);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalec0295e0a1cde43ae85733f2cf47d411cbb1e052fc65084488eaba05ea42b48b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalec0295e0a1cde43ae85733f2cf47d411cbb1e052fc65084488eaba05ea42b48b->enter($internalec0295e0a1cde43ae85733f2cf47d411cbb1e052fc65084488eaba05ea42b48bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showemail.html.twig"));

        $internald9fc5a27d9e8a82b9008269901633e994283aaefe491c0597c489e468711316b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald9fc5a27d9e8a82b9008269901633e994283aaefe491c0597c489e468711316b->enter($internald9fc5a27d9e8a82b9008269901633e994283aaefe491c0597c489e468711316bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showemail.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalec0295e0a1cde43ae85733f2cf47d411cbb1e052fc65084488eaba05ea42b48b->leave($internalec0295e0a1cde43ae85733f2cf47d411cbb1e052fc65084488eaba05ea42b48bprof);

        
        $internald9fc5a27d9e8a82b9008269901633e994283aaefe491c0597c489e468711316b->leave($internald9fc5a27d9e8a82b9008269901633e994283aaefe491c0597c489e468711316bprof);

    }

    // line 3
    public function blockfield($context, array $blocks = array())
    {
        $internale29af9c16821b91b6ae87a219148fe706beddd8b0bbd61efc9d871456d88cd9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale29af9c16821b91b6ae87a219148fe706beddd8b0bbd61efc9d871456d88cd9d->enter($internale29af9c16821b91b6ae87a219148fe706beddd8b0bbd61efc9d871456d88cd9dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal4c04f49cbf674515825c29060a71801eedb3d0df265a2463fb1f6f5cc3d9f60e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4c04f49cbf674515825c29060a71801eedb3d0df265a2463fb1f6f5cc3d9f60e->enter($internal4c04f49cbf674515825c29060a71801eedb3d0df265a2463fb1f6f5cc3d9f60eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 4
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:emaillink.html.twig", "SonataAdminBundle:CRUD:showemail.html.twig", 4)->display($context);
        
        $internal4c04f49cbf674515825c29060a71801eedb3d0df265a2463fb1f6f5cc3d9f60e->leave($internal4c04f49cbf674515825c29060a71801eedb3d0df265a2463fb1f6f5cc3d9f60eprof);

        
        $internale29af9c16821b91b6ae87a219148fe706beddd8b0bbd61efc9d871456d88cd9d->leave($internale29af9c16821b91b6ae87a219148fe706beddd8b0bbd61efc9d871456d88cd9dprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:emaillink.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:showemail.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showemail.html.twig");
    }
}
