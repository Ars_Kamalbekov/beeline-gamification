<?php

/* WebProfilerBundle:Collector:translation.html.twig */
class TwigTemplatebf772c644d1a1a9b689f6b68c8d0e5d16b63bf43e062f5a020e5d9eca301aa9c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:translation.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
            'panelContent' => array($this, 'blockpanelContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal518e044dee536252bb88eff5e555a0faa05f85de9c6f20dd3fba353fa1dd4b09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal518e044dee536252bb88eff5e555a0faa05f85de9c6f20dd3fba353fa1dd4b09->enter($internal518e044dee536252bb88eff5e555a0faa05f85de9c6f20dd3fba353fa1dd4b09prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:translation.html.twig"));

        $internal23544090ee4cd6b323c797c0c2b987304f90b62535d0c6a8496a7867503b6cca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal23544090ee4cd6b323c797c0c2b987304f90b62535d0c6a8496a7867503b6cca->enter($internal23544090ee4cd6b323c797c0c2b987304f90b62535d0c6a8496a7867503b6ccaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:translation.html.twig"));

        // line 3
        $context["helper"] = $this;
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal518e044dee536252bb88eff5e555a0faa05f85de9c6f20dd3fba353fa1dd4b09->leave($internal518e044dee536252bb88eff5e555a0faa05f85de9c6f20dd3fba353fa1dd4b09prof);

        
        $internal23544090ee4cd6b323c797c0c2b987304f90b62535d0c6a8496a7867503b6cca->leave($internal23544090ee4cd6b323c797c0c2b987304f90b62535d0c6a8496a7867503b6ccaprof);

    }

    // line 5
    public function blocktoolbar($context, array $blocks = array())
    {
        $internalee78d62d1570aeeef43232b21cf75fde62b0e27d330e3815e2e36be4f02c9f3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalee78d62d1570aeeef43232b21cf75fde62b0e27d330e3815e2e36be4f02c9f3a->enter($internalee78d62d1570aeeef43232b21cf75fde62b0e27d330e3815e2e36be4f02c9f3aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalb375baa142440a7ea774d7ea46bbad9a9c661051789b6cef8eea7e1efb914a7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb375baa142440a7ea774d7ea46bbad9a9c661051789b6cef8eea7e1efb914a7b->enter($internalb375baa142440a7ea774d7ea46bbad9a9c661051789b6cef8eea7e1efb914a7bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 6
        echo "    ";
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "messages", array()))) {
            // line 7
            echo "        ";
            obstart();
            // line 8
            echo "            ";
            echo twiginclude($this->env, $context, "@WebProfiler/Icon/translation.svg");
            echo "
            ";
            // line 9
            $context["statuscolor"] = ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 9, $this->getSourceContext()); })()), "countMissings", array())) ? ("red") : (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 9, $this->getSourceContext()); })()), "countFallbacks", array())) ? ("yellow") : (""))));
            // line 10
            echo "            ";
            $context["errorcount"] = (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "countMissings", array()) + twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "countFallbacks", array()));
            // line 11
            echo "            <span class=\"sf-toolbar-value\">";
            echo twigescapefilter($this->env, (((isset($context["errorcount"]) || arraykeyexists("errorcount", $context) ? $context["errorcount"] : (function () { throw new TwigErrorRuntime('Variable "errorcount" does not exist.', 11, $this->getSourceContext()); })())) ? ((isset($context["errorcount"]) || arraykeyexists("errorcount", $context) ? $context["errorcount"] : (function () { throw new TwigErrorRuntime('Variable "errorcount" does not exist.', 11, $this->getSourceContext()); })())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "countDefines", array()))), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 13
            echo "
        ";
            // line 14
            obstart();
            // line 15
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Missing messages</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 17
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 17, $this->getSourceContext()); })()), "countMissings", array())) ? ("red") : (""));
            echo "\">
                    ";
            // line 18
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "countMissings", array()), "html", null, true);
            echo "
                </span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Fallback messages</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 24
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 24, $this->getSourceContext()); })()), "countFallbacks", array())) ? ("yellow") : (""));
            echo "\">
                    ";
            // line 25
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 25, $this->getSourceContext()); })()), "countFallbacks", array()), "html", null, true);
            echo "
                </span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Defined messages</b>
                <span class=\"sf-toolbar-status\">";
            // line 31
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "countDefines", array()), "html", null, true);
            echo "</span>
            </div>
        ";
            $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 34
            echo "
        ";
            // line 35
            echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 35, $this->getSourceContext()); })()), "status" => (isset($context["statuscolor"]) || arraykeyexists("statuscolor", $context) ? $context["statuscolor"] : (function () { throw new TwigErrorRuntime('Variable "statuscolor" does not exist.', 35, $this->getSourceContext()); })())));
            echo "
    ";
        }
        
        $internalb375baa142440a7ea774d7ea46bbad9a9c661051789b6cef8eea7e1efb914a7b->leave($internalb375baa142440a7ea774d7ea46bbad9a9c661051789b6cef8eea7e1efb914a7bprof);

        
        $internalee78d62d1570aeeef43232b21cf75fde62b0e27d330e3815e2e36be4f02c9f3a->leave($internalee78d62d1570aeeef43232b21cf75fde62b0e27d330e3815e2e36be4f02c9f3aprof);

    }

    // line 39
    public function blockmenu($context, array $blocks = array())
    {
        $internald776233301c2f887c889f1d3cb7e7504d1b7d7ba55531c861b9a72d62d478fe9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald776233301c2f887c889f1d3cb7e7504d1b7d7ba55531c861b9a72d62d478fe9->enter($internald776233301c2f887c889f1d3cb7e7504d1b7d7ba55531c861b9a72d62d478fe9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internal53b4e0d76b2da040556b72368c7cd7831113202cc4955012983436937024702b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal53b4e0d76b2da040556b72368c7cd7831113202cc4955012983436937024702b->enter($internal53b4e0d76b2da040556b72368c7cd7831113202cc4955012983436937024702bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 40
        echo "    <span class=\"label label-status-";
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "countMissings", array())) ? ("error") : (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "countFallbacks", array())) ? ("warning") : (""))));
        echo " ";
        echo ((twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "messages", array()))) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">";
        // line 41
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/translation.svg");
        echo "</span>
        <strong>Translation</strong>
        ";
        // line 43
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 43, $this->getSourceContext()); })()), "countMissings", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 43, $this->getSourceContext()); })()), "countFallbacks", array()))) {
            // line 44
            echo "            ";
            $context["errorcount"] = (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 44, $this->getSourceContext()); })()), "countMissings", array()) + twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 44, $this->getSourceContext()); })()), "countFallbacks", array()));
            // line 45
            echo "            <span class=\"count\">
                <span>";
            // line 46
            echo twigescapefilter($this->env, (isset($context["errorcount"]) || arraykeyexists("errorcount", $context) ? $context["errorcount"] : (function () { throw new TwigErrorRuntime('Variable "errorcount" does not exist.', 46, $this->getSourceContext()); })()), "html", null, true);
            echo "</span>
            </span>
        ";
        }
        // line 49
        echo "    </span>
";
        
        $internal53b4e0d76b2da040556b72368c7cd7831113202cc4955012983436937024702b->leave($internal53b4e0d76b2da040556b72368c7cd7831113202cc4955012983436937024702bprof);

        
        $internald776233301c2f887c889f1d3cb7e7504d1b7d7ba55531c861b9a72d62d478fe9->leave($internald776233301c2f887c889f1d3cb7e7504d1b7d7ba55531c861b9a72d62d478fe9prof);

    }

    // line 52
    public function blockpanel($context, array $blocks = array())
    {
        $internal4c6d14cc760842a30b2d490e3930118eb5daecd6cf2617f4f2e5cdbc1320d108 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4c6d14cc760842a30b2d490e3930118eb5daecd6cf2617f4f2e5cdbc1320d108->enter($internal4c6d14cc760842a30b2d490e3930118eb5daecd6cf2617f4f2e5cdbc1320d108prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal401b013be5766657691547553230589e399c51a839d57edf7a01f2ca1ef57cd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal401b013be5766657691547553230589e399c51a839d57edf7a01f2ca1ef57cd5->enter($internal401b013be5766657691547553230589e399c51a839d57edf7a01f2ca1ef57cd5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 53
        echo "    ";
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 53, $this->getSourceContext()); })()), "messages", array()))) {
            // line 54
            echo "        <h2>Translations</h2>
        <div class=\"empty\">
            <p>No translations have been called.</p>
        </div>
    ";
        } else {
            // line 59
            echo "        ";
            $this->displayBlock("panelContent", $context, $blocks);
            echo "
    ";
        }
        
        $internal401b013be5766657691547553230589e399c51a839d57edf7a01f2ca1ef57cd5->leave($internal401b013be5766657691547553230589e399c51a839d57edf7a01f2ca1ef57cd5prof);

        
        $internal4c6d14cc760842a30b2d490e3930118eb5daecd6cf2617f4f2e5cdbc1320d108->leave($internal4c6d14cc760842a30b2d490e3930118eb5daecd6cf2617f4f2e5cdbc1320d108prof);

    }

    // line 63
    public function blockpanelContent($context, array $blocks = array())
    {
        $internal5a4e6d8953ce609f2e9b05b120bc5e9ee284cb7c5c84cc8968a8a3eefa9f3251 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5a4e6d8953ce609f2e9b05b120bc5e9ee284cb7c5c84cc8968a8a3eefa9f3251->enter($internal5a4e6d8953ce609f2e9b05b120bc5e9ee284cb7c5c84cc8968a8a3eefa9f3251prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panelContent"));

        $internal1324b5fbce5864b574f6cc96771fcd5cc9069a8d7e14924f817602919589e0d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1324b5fbce5864b574f6cc96771fcd5cc9069a8d7e14924f817602919589e0d5->enter($internal1324b5fbce5864b574f6cc96771fcd5cc9069a8d7e14924f817602919589e0d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panelContent"));

        // line 64
        echo "    <h2>Translation Metrics</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 68
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 68, $this->getSourceContext()); })()), "countDefines", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Defined messages</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 73
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 73, $this->getSourceContext()); })()), "countFallbacks", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Fallback messages</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 78
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 78, $this->getSourceContext()); })()), "countMissings", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Missing messages</span>
        </div>
    </div>

    <h2>Translation Messages</h2>

    ";
        // line 86
        echo "    ";
        list($context["messagesdefined"], $context["messagesmissing"], $context["messagesfallback"]) =         array(array(), array(), array());
        // line 87
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 87, $this->getSourceContext()); })()), "messages", array()));
        foreach ($context['seq'] as $context["key"] => $context["message"]) {
            // line 88
            echo "        ";
            if ((twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "state", array()) == twigconstant("Symfony\\Component\\Translation\\DataCollectorTranslator::MESSAGEDEFINED"))) {
                // line 89
                echo "            ";
                $context["messagesdefined"] = twigarraymerge((isset($context["messagesdefined"]) || arraykeyexists("messagesdefined", $context) ? $context["messagesdefined"] : (function () { throw new TwigErrorRuntime('Variable "messagesdefined" does not exist.', 89, $this->getSourceContext()); })()), array(0 => $context["message"]));
                // line 90
                echo "        ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "state", array()) == twigconstant("Symfony\\Component\\Translation\\DataCollectorTranslator::MESSAGEMISSING"))) {
                // line 91
                echo "            ";
                $context["messagesmissing"] = twigarraymerge((isset($context["messagesmissing"]) || arraykeyexists("messagesmissing", $context) ? $context["messagesmissing"] : (function () { throw new TwigErrorRuntime('Variable "messagesmissing" does not exist.', 91, $this->getSourceContext()); })()), array(0 => $context["message"]));
                // line 92
                echo "        ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "state", array()) == twigconstant("Symfony\\Component\\Translation\\DataCollectorTranslator::MESSAGEEQUALSFALLBACK"))) {
                // line 93
                echo "            ";
                $context["messagesfallback"] = twigarraymerge((isset($context["messagesfallback"]) || arraykeyexists("messagesfallback", $context) ? $context["messagesfallback"] : (function () { throw new TwigErrorRuntime('Variable "messagesfallback" does not exist.', 93, $this->getSourceContext()); })()), array(0 => $context["message"]));
                // line 94
                echo "        ";
            }
            // line 95
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['message'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 96
        echo "
    <div class=\"sf-tabs\">
        <div class=\"tab\">
            <h3 class=\"tab-title\">Defined <span class=\"badge\">";
        // line 99
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 99, $this->getSourceContext()); })()), "countDefines", array()), "html", null, true);
        echo "</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are correctly translated into the given locale.
                </p>

                ";
        // line 106
        if (twigtestempty((isset($context["messagesdefined"]) || arraykeyexists("messagesdefined", $context) ? $context["messagesdefined"] : (function () { throw new TwigErrorRuntime('Variable "messagesdefined" does not exist.', 106, $this->getSourceContext()); })()))) {
            // line 107
            echo "                    <div class=\"empty\">
                        <p>None of the used translation messages are defined for the given locale.</p>
                    </div>
                ";
        } else {
            // line 111
            echo "                    ";
            echo $context["helper"]->macrorendertable((isset($context["messagesdefined"]) || arraykeyexists("messagesdefined", $context) ? $context["messagesdefined"] : (function () { throw new TwigErrorRuntime('Variable "messagesdefined" does not exist.', 111, $this->getSourceContext()); })()));
            echo "
                ";
        }
        // line 113
        echo "            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">Fallback <span class=\"badge ";
        // line 117
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 117, $this->getSourceContext()); })()), "countFallbacks", array())) ? ("status-warning") : (""));
        echo "\">";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 117, $this->getSourceContext()); })()), "countFallbacks", array()), "html", null, true);
        echo "</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are not available for the given locale
                    but Symfony found them in the fallback locale catalog.
                </p>

                ";
        // line 125
        if (twigtestempty((isset($context["messagesfallback"]) || arraykeyexists("messagesfallback", $context) ? $context["messagesfallback"] : (function () { throw new TwigErrorRuntime('Variable "messagesfallback" does not exist.', 125, $this->getSourceContext()); })()))) {
            // line 126
            echo "                    <div class=\"empty\">
                        <p>No fallback translation messages were used.</p>
                    </div>
                ";
        } else {
            // line 130
            echo "                    ";
            echo $context["helper"]->macrorendertable((isset($context["messagesfallback"]) || arraykeyexists("messagesfallback", $context) ? $context["messagesfallback"] : (function () { throw new TwigErrorRuntime('Variable "messagesfallback" does not exist.', 130, $this->getSourceContext()); })()));
            echo "
                ";
        }
        // line 132
        echo "            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">Missing <span class=\"badge ";
        // line 136
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 136, $this->getSourceContext()); })()), "countMissings", array())) ? ("status-error") : (""));
        echo "\">";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 136, $this->getSourceContext()); })()), "countMissings", array()), "html", null, true);
        echo "</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are not available for the given locale and cannot
                    be found in the fallback locales. Add them to the translation
                    catalogue to avoid Symfony outputting untranslated contents.
                </p>

                ";
        // line 145
        if (twigtestempty((isset($context["messagesmissing"]) || arraykeyexists("messagesmissing", $context) ? $context["messagesmissing"] : (function () { throw new TwigErrorRuntime('Variable "messagesmissing" does not exist.', 145, $this->getSourceContext()); })()))) {
            // line 146
            echo "                    <div class=\"empty\">
                        <p>There are no messages of this category.</p>
                    </div>
                ";
        } else {
            // line 150
            echo "                    ";
            echo $context["helper"]->macrorendertable((isset($context["messagesmissing"]) || arraykeyexists("messagesmissing", $context) ? $context["messagesmissing"] : (function () { throw new TwigErrorRuntime('Variable "messagesmissing" does not exist.', 150, $this->getSourceContext()); })()));
            echo "
                ";
        }
        // line 152
        echo "            </div>
        </div>
    </div>
";
        
        $internal1324b5fbce5864b574f6cc96771fcd5cc9069a8d7e14924f817602919589e0d5->leave($internal1324b5fbce5864b574f6cc96771fcd5cc9069a8d7e14924f817602919589e0d5prof);

        
        $internal5a4e6d8953ce609f2e9b05b120bc5e9ee284cb7c5c84cc8968a8a3eefa9f3251->leave($internal5a4e6d8953ce609f2e9b05b120bc5e9ee284cb7c5c84cc8968a8a3eefa9f3251prof);

    }

    // line 157
    public function macrorendertable($messages = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "messages" => $messages,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal121553b66047d3d8e02bdd22642e53408e34e7c18fdbdeaba5aea27b84eb0c80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal121553b66047d3d8e02bdd22642e53408e34e7c18fdbdeaba5aea27b84eb0c80->enter($internal121553b66047d3d8e02bdd22642e53408e34e7c18fdbdeaba5aea27b84eb0c80prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            $internalee9a41b9ed834424135bbd1a66b899f8e5fcd1dccb660412c87138e1eb0b84f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internalee9a41b9ed834424135bbd1a66b899f8e5fcd1dccb660412c87138e1eb0b84f8->enter($internalee9a41b9ed834424135bbd1a66b899f8e5fcd1dccb660412c87138e1eb0b84f8prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            // line 158
            echo "    <table>
        <thead>
            <tr>
                <th>Locale</th>
                <th>Domain</th>
                <th>Times used</th>
                <th>Message ID</th>
                <th>Message Preview</th>
            </tr>
        </thead>
        <tbody>
        ";
            // line 169
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["messages"]) || arraykeyexists("messages", $context) ? $context["messages"] : (function () { throw new TwigErrorRuntime('Variable "messages" does not exist.', 169, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["message"]) {
                // line 170
                echo "            <tr>
                <td class=\"font-normal text-small\">";
                // line 171
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "locale", array()), "html", null, true);
                echo "</td>
                <td class=\"font-normal text-small text-bold nowrap\">";
                // line 172
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "domain", array()), "html", null, true);
                echo "</td>
                <td class=\"font-normal text-small\">";
                // line 173
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "count", array()), "html", null, true);
                echo "</td>
                <td>
                    <span class=\"nowrap\">";
                // line 175
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "id", array()), "html", null, true);
                echo "</span>

                    ";
                // line 177
                if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "transChoiceNumber", array()))) {
                    // line 178
                    echo "                        <small class=\"newline\">(pluralization is used)</small>
                    ";
                }
                // line 180
                echo "
                    ";
                // line 181
                if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "parameters", array())) > 0)) {
                    // line 182
                    echo "                        <button class=\"btn-link newline text-small sf-toggle\" data-toggle-selector=\"#parameters-";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                    echo "\" data-toggle-alt-content=\"Hide parameters\">Show parameters</button>

                        <div id=\"parameters-";
                    // line 184
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                    echo "\" class=\"hidden\">
                            ";
                    // line 185
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "parameters", array()));
                    foreach ($context['seq'] as $context["key"] => $context["parameters"]) {
                        // line 186
                        echo "                                ";
                        echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, $context["parameters"], 1));
                        echo "
                            ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['parameters'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 188
                    echo "                        </div>
                    ";
                }
                // line 190
                echo "                </td>
                <td class=\"prewrap\">";
                // line 191
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "translation", array()), "html", null, true);
                echo "</td>
            </tr>
        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['message'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 194
            echo "        </tbody>
    </table>
";
            
            $internalee9a41b9ed834424135bbd1a66b899f8e5fcd1dccb660412c87138e1eb0b84f8->leave($internalee9a41b9ed834424135bbd1a66b899f8e5fcd1dccb660412c87138e1eb0b84f8prof);

            
            $internal121553b66047d3d8e02bdd22642e53408e34e7c18fdbdeaba5aea27b84eb0c80->leave($internal121553b66047d3d8e02bdd22642e53408e34e7c18fdbdeaba5aea27b84eb0c80prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:translation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  525 => 194,  508 => 191,  505 => 190,  501 => 188,  492 => 186,  488 => 185,  484 => 184,  478 => 182,  476 => 181,  473 => 180,  469 => 178,  467 => 177,  462 => 175,  457 => 173,  453 => 172,  449 => 171,  446 => 170,  429 => 169,  416 => 158,  398 => 157,  385 => 152,  379 => 150,  373 => 146,  371 => 145,  357 => 136,  351 => 132,  345 => 130,  339 => 126,  337 => 125,  324 => 117,  318 => 113,  312 => 111,  306 => 107,  304 => 106,  294 => 99,  289 => 96,  283 => 95,  280 => 94,  277 => 93,  274 => 92,  271 => 91,  268 => 90,  265 => 89,  262 => 88,  257 => 87,  254 => 86,  244 => 78,  236 => 73,  228 => 68,  222 => 64,  213 => 63,  199 => 59,  192 => 54,  189 => 53,  180 => 52,  169 => 49,  163 => 46,  160 => 45,  157 => 44,  155 => 43,  150 => 41,  143 => 40,  134 => 39,  121 => 35,  118 => 34,  112 => 31,  103 => 25,  99 => 24,  90 => 18,  86 => 17,  82 => 15,  80 => 14,  77 => 13,  71 => 11,  68 => 10,  66 => 9,  61 => 8,  58 => 7,  55 => 6,  46 => 5,  36 => 1,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% import self as helper %}

{% block toolbar %}
    {% if collector.messages|length %}
        {% set icon %}
            {{ include('@WebProfiler/Icon/translation.svg') }}
            {% set statuscolor = collector.countMissings ? 'red' : collector.countFallbacks ? 'yellow' %}
            {% set errorcount = collector.countMissings + collector.countFallbacks %}
            <span class=\"sf-toolbar-value\">{{ errorcount ?: collector.countDefines }}</span>
        {% endset %}

        {% set text %}
            <div class=\"sf-toolbar-info-piece\">
                <b>Missing messages</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.countMissings ? 'red' }}\">
                    {{ collector.countMissings }}
                </span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Fallback messages</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.countFallbacks ? 'yellow' }}\">
                    {{ collector.countFallbacks }}
                </span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Defined messages</b>
                <span class=\"sf-toolbar-status\">{{ collector.countDefines }}</span>
            </div>
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl, status: statuscolor }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    <span class=\"label label-status-{{ collector.countMissings ? 'error' : collector.countFallbacks ? 'warning' }} {{ collector.messages is empty ? 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/translation.svg') }}</span>
        <strong>Translation</strong>
        {% if collector.countMissings or collector.countFallbacks %}
            {% set errorcount = collector.countMissings + collector.countFallbacks %}
            <span class=\"count\">
                <span>{{ errorcount }}</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    {% if collector.messages is empty %}
        <h2>Translations</h2>
        <div class=\"empty\">
            <p>No translations have been called.</p>
        </div>
    {% else %}
        {{ block('panelContent') }}
    {% endif %}
{% endblock %}

{% block panelContent %}
    <h2>Translation Metrics</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">{{ collector.countDefines }}</span>
            <span class=\"label\">Defined messages</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.countFallbacks }}</span>
            <span class=\"label\">Fallback messages</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.countMissings }}</span>
            <span class=\"label\">Missing messages</span>
        </div>
    </div>

    <h2>Translation Messages</h2>

    {# sort translation messages in groups #}
    {% set messagesdefined, messagesmissing, messagesfallback = [], [], [] %}
    {% for message in collector.messages %}
        {% if message.state == constant('Symfony\\\\Component\\\\Translation\\\\DataCollectorTranslator::MESSAGEDEFINED') %}
            {% set messagesdefined = messagesdefined|merge([message]) %}
        {% elseif message.state == constant('Symfony\\\\Component\\\\Translation\\\\DataCollectorTranslator::MESSAGEMISSING') %}
            {% set messagesmissing = messagesmissing|merge([message]) %}
        {% elseif message.state == constant('Symfony\\\\Component\\\\Translation\\\\DataCollectorTranslator::MESSAGEEQUALSFALLBACK') %}
            {% set messagesfallback = messagesfallback|merge([message]) %}
        {% endif %}
    {% endfor %}

    <div class=\"sf-tabs\">
        <div class=\"tab\">
            <h3 class=\"tab-title\">Defined <span class=\"badge\">{{ collector.countDefines }}</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are correctly translated into the given locale.
                </p>

                {% if messagesdefined is empty %}
                    <div class=\"empty\">
                        <p>None of the used translation messages are defined for the given locale.</p>
                    </div>
                {% else %}
                    {{ helper.rendertable(messagesdefined) }}
                {% endif %}
            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">Fallback <span class=\"badge {{ collector.countFallbacks ? 'status-warning' }}\">{{ collector.countFallbacks }}</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are not available for the given locale
                    but Symfony found them in the fallback locale catalog.
                </p>

                {% if messagesfallback is empty %}
                    <div class=\"empty\">
                        <p>No fallback translation messages were used.</p>
                    </div>
                {% else %}
                    {{ helper.rendertable(messagesfallback) }}
                {% endif %}
            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">Missing <span class=\"badge {{ collector.countMissings ? 'status-error' }}\">{{ collector.countMissings }}</span></h3>

            <div class=\"tab-content\">
                <p class=\"help\">
                    These messages are not available for the given locale and cannot
                    be found in the fallback locales. Add them to the translation
                    catalogue to avoid Symfony outputting untranslated contents.
                </p>

                {% if messagesmissing is empty %}
                    <div class=\"empty\">
                        <p>There are no messages of this category.</p>
                    </div>
                {% else %}
                    {{ helper.rendertable(messagesmissing) }}
                {% endif %}
            </div>
        </div>
    </div>
{% endblock %}

{% macro rendertable(messages) %}
    <table>
        <thead>
            <tr>
                <th>Locale</th>
                <th>Domain</th>
                <th>Times used</th>
                <th>Message ID</th>
                <th>Message Preview</th>
            </tr>
        </thead>
        <tbody>
        {% for message in messages %}
            <tr>
                <td class=\"font-normal text-small\">{{ message.locale }}</td>
                <td class=\"font-normal text-small text-bold nowrap\">{{ message.domain }}</td>
                <td class=\"font-normal text-small\">{{ message.count }}</td>
                <td>
                    <span class=\"nowrap\">{{ message.id }}</span>

                    {% if message.transChoiceNumber is not null %}
                        <small class=\"newline\">(pluralization is used)</small>
                    {% endif %}

                    {% if message.parameters|length > 0 %}
                        <button class=\"btn-link newline text-small sf-toggle\" data-toggle-selector=\"#parameters-{{ loop.index }}\" data-toggle-alt-content=\"Hide parameters\">Show parameters</button>

                        <div id=\"parameters-{{ loop.index }}\" class=\"hidden\">
                            {% for parameters in message.parameters %}
                                {{ profilerdump(parameters, maxDepth=1) }}
                            {% endfor %}
                        </div>
                    {% endif %}
                </td>
                <td class=\"prewrap\">{{ message.translation }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endmacro %}
", "WebProfilerBundle:Collector:translation.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/translation.html.twig");
    }
}
