<?php

/* SonataAdminBundle::emptylayout.html.twig */
class TwigTemplate0e4435bb111ba8641946e44d464568bec65bd2397eef0df7985d54eeda9cd22b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'sonataheader' => array($this, 'blocksonataheader'),
            'sonataleftside' => array($this, 'blocksonataleftside'),
            'sonatanav' => array($this, 'blocksonatanav'),
            'sonatabreadcrumb' => array($this, 'blocksonatabreadcrumb'),
            'stylesheets' => array($this, 'blockstylesheets'),
            'sonatawrapper' => array($this, 'blocksonatawrapper'),
            'sonatapagecontent' => array($this, 'blocksonatapagecontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["adminpool"]) || arraykeyexists("adminpool", $context) ? $context["adminpool"] : (function () { throw new TwigErrorRuntime('Variable "adminpool" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "layout"), "method"), "SonataAdminBundle::emptylayout.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal87eb120cc00e8fb2269537a39fd8df51492e1f10aafb2798da5d7311fa056ec0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal87eb120cc00e8fb2269537a39fd8df51492e1f10aafb2798da5d7311fa056ec0->enter($internal87eb120cc00e8fb2269537a39fd8df51492e1f10aafb2798da5d7311fa056ec0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::emptylayout.html.twig"));

        $internal89d2b7852d25e3f39b23d6d063283f031526bf526884adc4d377a92977ba89c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal89d2b7852d25e3f39b23d6d063283f031526bf526884adc4d377a92977ba89c0->enter($internal89d2b7852d25e3f39b23d6d063283f031526bf526884adc4d377a92977ba89c0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::emptylayout.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal87eb120cc00e8fb2269537a39fd8df51492e1f10aafb2798da5d7311fa056ec0->leave($internal87eb120cc00e8fb2269537a39fd8df51492e1f10aafb2798da5d7311fa056ec0prof);

        
        $internal89d2b7852d25e3f39b23d6d063283f031526bf526884adc4d377a92977ba89c0->leave($internal89d2b7852d25e3f39b23d6d063283f031526bf526884adc4d377a92977ba89c0prof);

    }

    // line 14
    public function blocksonataheader($context, array $blocks = array())
    {
        $internalc2a8a81e101abc6ee0fbb0a0039ddc56dfe1dab0ebcec22f0899aaf8ddcc6f7b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc2a8a81e101abc6ee0fbb0a0039ddc56dfe1dab0ebcec22f0899aaf8ddcc6f7b->enter($internalc2a8a81e101abc6ee0fbb0a0039ddc56dfe1dab0ebcec22f0899aaf8ddcc6f7bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheader"));

        $internal15fc263f69a349ffe3b8756b8040e05259232dc9de5d914d73073cb273d93950 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal15fc263f69a349ffe3b8756b8040e05259232dc9de5d914d73073cb273d93950->enter($internal15fc263f69a349ffe3b8756b8040e05259232dc9de5d914d73073cb273d93950prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheader"));

        
        $internal15fc263f69a349ffe3b8756b8040e05259232dc9de5d914d73073cb273d93950->leave($internal15fc263f69a349ffe3b8756b8040e05259232dc9de5d914d73073cb273d93950prof);

        
        $internalc2a8a81e101abc6ee0fbb0a0039ddc56dfe1dab0ebcec22f0899aaf8ddcc6f7b->leave($internalc2a8a81e101abc6ee0fbb0a0039ddc56dfe1dab0ebcec22f0899aaf8ddcc6f7bprof);

    }

    // line 15
    public function blocksonataleftside($context, array $blocks = array())
    {
        $internalb48a8101e700be08143aaf4d06990d585e076d90d837c45f048cad687bfbc28c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb48a8101e700be08143aaf4d06990d585e076d90d837c45f048cad687bfbc28c->enter($internalb48a8101e700be08143aaf4d06990d585e076d90d837c45f048cad687bfbc28cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataleftside"));

        $internal5ef16d183b19a0151a35e05305f363437f7c2d0c017dab7e1ad318813b8b0f54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5ef16d183b19a0151a35e05305f363437f7c2d0c017dab7e1ad318813b8b0f54->enter($internal5ef16d183b19a0151a35e05305f363437f7c2d0c017dab7e1ad318813b8b0f54prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataleftside"));

        
        $internal5ef16d183b19a0151a35e05305f363437f7c2d0c017dab7e1ad318813b8b0f54->leave($internal5ef16d183b19a0151a35e05305f363437f7c2d0c017dab7e1ad318813b8b0f54prof);

        
        $internalb48a8101e700be08143aaf4d06990d585e076d90d837c45f048cad687bfbc28c->leave($internalb48a8101e700be08143aaf4d06990d585e076d90d837c45f048cad687bfbc28cprof);

    }

    // line 16
    public function blocksonatanav($context, array $blocks = array())
    {
        $internal546e4e5fe7d7a35718306ef20166e13e782914c81c9b9e2d0273e1414dc40b2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal546e4e5fe7d7a35718306ef20166e13e782914c81c9b9e2d0273e1414dc40b2d->enter($internal546e4e5fe7d7a35718306ef20166e13e782914c81c9b9e2d0273e1414dc40b2dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatanav"));

        $internale3c55811542d97edb8ed78974dc0ee55bf69f19dc13df83eab7051507eddf1d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale3c55811542d97edb8ed78974dc0ee55bf69f19dc13df83eab7051507eddf1d6->enter($internale3c55811542d97edb8ed78974dc0ee55bf69f19dc13df83eab7051507eddf1d6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatanav"));

        
        $internale3c55811542d97edb8ed78974dc0ee55bf69f19dc13df83eab7051507eddf1d6->leave($internale3c55811542d97edb8ed78974dc0ee55bf69f19dc13df83eab7051507eddf1d6prof);

        
        $internal546e4e5fe7d7a35718306ef20166e13e782914c81c9b9e2d0273e1414dc40b2d->leave($internal546e4e5fe7d7a35718306ef20166e13e782914c81c9b9e2d0273e1414dc40b2dprof);

    }

    // line 17
    public function blocksonatabreadcrumb($context, array $blocks = array())
    {
        $internal2a065611fa14ecaa4c54dcfa110e20844a46905e0da6f51f74fd7c8022516fac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2a065611fa14ecaa4c54dcfa110e20844a46905e0da6f51f74fd7c8022516fac->enter($internal2a065611fa14ecaa4c54dcfa110e20844a46905e0da6f51f74fd7c8022516facprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatabreadcrumb"));

        $internal75a88e32d13f4ad2185a4c4b14091d2524218578e1778e453106faeea2568b27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal75a88e32d13f4ad2185a4c4b14091d2524218578e1778e453106faeea2568b27->enter($internal75a88e32d13f4ad2185a4c4b14091d2524218578e1778e453106faeea2568b27prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatabreadcrumb"));

        
        $internal75a88e32d13f4ad2185a4c4b14091d2524218578e1778e453106faeea2568b27->leave($internal75a88e32d13f4ad2185a4c4b14091d2524218578e1778e453106faeea2568b27prof);

        
        $internal2a065611fa14ecaa4c54dcfa110e20844a46905e0da6f51f74fd7c8022516fac->leave($internal2a065611fa14ecaa4c54dcfa110e20844a46905e0da6f51f74fd7c8022516facprof);

    }

    // line 19
    public function blockstylesheets($context, array $blocks = array())
    {
        $internal0fcd1003a2225bb3327b889867c6b6d4dba263c10b234ef43c28b25be3ee9c09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0fcd1003a2225bb3327b889867c6b6d4dba263c10b234ef43c28b25be3ee9c09->enter($internal0fcd1003a2225bb3327b889867c6b6d4dba263c10b234ef43c28b25be3ee9c09prof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        $internal93c1b1b3cbf6285439e4df8a623873dcb86a633c818992bee6a902d37b6a1755 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal93c1b1b3cbf6285439e4df8a623873dcb86a633c818992bee6a902d37b6a1755->enter($internal93c1b1b3cbf6285439e4df8a623873dcb86a633c818992bee6a902d37b6a1755prof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
";
        
        $internal93c1b1b3cbf6285439e4df8a623873dcb86a633c818992bee6a902d37b6a1755->leave($internal93c1b1b3cbf6285439e4df8a623873dcb86a633c818992bee6a902d37b6a1755prof);

        
        $internal0fcd1003a2225bb3327b889867c6b6d4dba263c10b234ef43c28b25be3ee9c09->leave($internal0fcd1003a2225bb3327b889867c6b6d4dba263c10b234ef43c28b25be3ee9c09prof);

    }

    // line 30
    public function blocksonatawrapper($context, array $blocks = array())
    {
        $internalce813452b7bbcc1e7570a11d8549ec14089abad3edb7ee2168daabe7f1041bc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalce813452b7bbcc1e7570a11d8549ec14089abad3edb7ee2168daabe7f1041bc9->enter($internalce813452b7bbcc1e7570a11d8549ec14089abad3edb7ee2168daabe7f1041bc9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatawrapper"));

        $internal20009d3d15a6c7bbe2b64ad8056039c3e63af464993e4ae32a18991eaec9ab63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal20009d3d15a6c7bbe2b64ad8056039c3e63af464993e4ae32a18991eaec9ab63->enter($internal20009d3d15a6c7bbe2b64ad8056039c3e63af464993e4ae32a18991eaec9ab63prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatawrapper"));

        // line 31
        echo "    ";
        $this->displayBlock('sonatapagecontent', $context, $blocks);
        
        $internal20009d3d15a6c7bbe2b64ad8056039c3e63af464993e4ae32a18991eaec9ab63->leave($internal20009d3d15a6c7bbe2b64ad8056039c3e63af464993e4ae32a18991eaec9ab63prof);

        
        $internalce813452b7bbcc1e7570a11d8549ec14089abad3edb7ee2168daabe7f1041bc9->leave($internalce813452b7bbcc1e7570a11d8549ec14089abad3edb7ee2168daabe7f1041bc9prof);

    }

    public function blocksonatapagecontent($context, array $blocks = array())
    {
        $internalfb2f3670e0570e1b275e3294ca1c257f97b6e6c1a078fef25f3f2a26b0b7457c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfb2f3670e0570e1b275e3294ca1c257f97b6e6c1a078fef25f3f2a26b0b7457c->enter($internalfb2f3670e0570e1b275e3294ca1c257f97b6e6c1a078fef25f3f2a26b0b7457cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontent"));

        $internal0b1fc09e1b50ace2f2c8fc758e2e3c4569c7ae476731fbb3ffa2fdd716c47dab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0b1fc09e1b50ace2f2c8fc758e2e3c4569c7ae476731fbb3ffa2fdd716c47dab->enter($internal0b1fc09e1b50ace2f2c8fc758e2e3c4569c7ae476731fbb3ffa2fdd716c47dabprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontent"));

        // line 32
        echo "        ";
        $this->displayParentBlock("sonatapagecontent", $context, $blocks);
        echo "
    ";
        
        $internal0b1fc09e1b50ace2f2c8fc758e2e3c4569c7ae476731fbb3ffa2fdd716c47dab->leave($internal0b1fc09e1b50ace2f2c8fc758e2e3c4569c7ae476731fbb3ffa2fdd716c47dabprof);

        
        $internalfb2f3670e0570e1b275e3294ca1c257f97b6e6c1a078fef25f3f2a26b0b7457c->leave($internalfb2f3670e0570e1b275e3294ca1c257f97b6e6c1a078fef25f3f2a26b0b7457cprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::emptylayout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 32,  151 => 31,  142 => 30,  122 => 20,  113 => 19,  96 => 17,  79 => 16,  62 => 15,  45 => 14,  24 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends adminpool.getTemplate('layout') %}

{% block sonataheader %}{% endblock %}
{% block sonataleftside %}{% endblock %}
{% block sonatanav %}{% endblock %}
{% block sonatabreadcrumb %}{% endblock %}

{% block stylesheets %}
    {{ parent() }}

    <style>
        .content {
            margin: 0px;
            padding: 0px;
        }
    </style>
{% endblock %}

{% block sonatawrapper %}
    {% block sonatapagecontent %}
        {{ parent() }}
    {% endblock %}
{% endblock %}
", "SonataAdminBundle::emptylayout.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/emptylayout.html.twig");
    }
}
