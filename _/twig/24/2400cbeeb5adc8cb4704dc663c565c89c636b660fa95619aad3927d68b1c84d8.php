<?php

/* SonataAdminBundle:Core:addblock.html.twig */
class TwigTemplatea5b8d6d87058df0b1fb423ec6c58f7ba35b28190dcc44f70b8a7376ed0c0bd95 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'userblock' => array($this, 'blockuserblock'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald97040c3369ba2dd829d9ce2588983f58642f058ff99635ed8757a3f86f8a2e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald97040c3369ba2dd829d9ce2588983f58642f058ff99635ed8757a3f86f8a2e3->enter($internald97040c3369ba2dd829d9ce2588983f58642f058ff99635ed8757a3f86f8a2e3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:addblock.html.twig"));

        $internal2ac0c00a0db7a2a12b952f4eed11f0e27db5d18d0590e9408d1fa7b497f37ff4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2ac0c00a0db7a2a12b952f4eed11f0e27db5d18d0590e9408d1fa7b497f37ff4->enter($internal2ac0c00a0db7a2a12b952f4eed11f0e27db5d18d0590e9408d1fa7b497f37ff4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:addblock.html.twig"));

        // line 1
        $this->displayBlock('userblock', $context, $blocks);
        
        $internald97040c3369ba2dd829d9ce2588983f58642f058ff99635ed8757a3f86f8a2e3->leave($internald97040c3369ba2dd829d9ce2588983f58642f058ff99635ed8757a3f86f8a2e3prof);

        
        $internal2ac0c00a0db7a2a12b952f4eed11f0e27db5d18d0590e9408d1fa7b497f37ff4->leave($internal2ac0c00a0db7a2a12b952f4eed11f0e27db5d18d0590e9408d1fa7b497f37ff4prof);

    }

    public function blockuserblock($context, array $blocks = array())
    {
        $internal2c324e81fcd4256757bb8629e87b7e804582204a5c9f152e3cb99a993c135377 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2c324e81fcd4256757bb8629e87b7e804582204a5c9f152e3cb99a993c135377->enter($internal2c324e81fcd4256757bb8629e87b7e804582204a5c9f152e3cb99a993c135377prof = new TwigProfilerProfile($this->getTemplateName(), "block", "userblock"));

        $internalbfc5e01aaf521fae7ae085fdb3cfdc9fb344afc322c73031d52d7f42baaf5f78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbfc5e01aaf521fae7ae085fdb3cfdc9fb344afc322c73031d52d7f42baaf5f78->enter($internalbfc5e01aaf521fae7ae085fdb3cfdc9fb344afc322c73031d52d7f42baaf5f78prof = new TwigProfilerProfile($this->getTemplateName(), "block", "userblock"));

        // line 2
        echo "    ";
        $context["itemspercolumn"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 2, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "dropdownnumbergroupspercolums"), "method");
        // line 3
        echo "    ";
        $context["groups"] = array();
        // line 4
        echo "
    ";
        // line 5
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 5, $this->getSourceContext()); })()), "adminPool", array()), "dashboardgroups", array()));
        foreach ($context['seq'] as $context["key"] => $context["group"]) {
            // line 6
            echo "        ";
            $context["displaygroup"] = false;
            // line 7
            echo "
        ";
            // line 8
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "items", array()));
            foreach ($context['seq'] as $context["key"] => $context["admin"]) {
                if (((isset($context["displaygroup"]) || arraykeyexists("displaygroup", $context) ? $context["displaygroup"] : (function () { throw new TwigErrorRuntime('Variable "displaygroup" does not exist.', 8, $this->getSourceContext()); })()) == false)) {
                    // line 9
                    echo "            ";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "create"), "method"))) {
                        // line 10
                        echo "                ";
                        $context["displaygroup"] = true;
                        // line 11
                        echo "                ";
                        $context["groups"] = twigarraymerge(array(0 => $context["group"]), (isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 11, $this->getSourceContext()); })()));
                        // line 12
                        echo "            ";
                    }
                    // line 13
                    echo "        ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['admin'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 14
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['group'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 15
        echo "
    ";
        // line 16
        $context["columncount"] = twiground((twiglengthfilter($this->env, (isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 16, $this->getSourceContext()); })())) / (isset($context["itemspercolumn"]) || arraykeyexists("itemspercolumn", $context) ? $context["itemspercolumn"] : (function () { throw new TwigErrorRuntime('Variable "itemspercolumn" does not exist.', 16, $this->getSourceContext()); })())), 0, "ceil");
        // line 17
        echo "
    <div class=\"dropdown-menu multi-column dropdown-add\"
        ";
        // line 19
        if (((isset($context["columncount"]) || arraykeyexists("columncount", $context) ? $context["columncount"] : (function () { throw new TwigErrorRuntime('Variable "columncount" does not exist.', 19, $this->getSourceContext()); })()) > 1)) {
            echo "style=\"width: ";
            echo twigescapefilter($this->env, ((isset($context["columncount"]) || arraykeyexists("columncount", $context) ? $context["columncount"] : (function () { throw new TwigErrorRuntime('Variable "columncount" does not exist.', 19, $this->getSourceContext()); })()) * 140), "html", null, true);
            echo "px;\"";
        }
        // line 20
        echo "            >
        ";
        // line 21
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twigreversefilter($this->env, (isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 21, $this->getSourceContext()); })())));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["group"]) {
            // line 22
            echo "            ";
            $context["display"] = (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array())) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLESONATAADMIN"));
            // line 23
            echo "            ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array()));
            foreach ($context['seq'] as $context["key"] => $context["role"]) {
                if ( !(isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 23, $this->getSourceContext()); })())) {
                    // line 24
                    echo "                ";
                    $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                    // line 25
                    echo "            ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['role'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 26
            echo "
            ";
            // line 27
            if ((isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 27, $this->getSourceContext()); })())) {
                // line 28
                echo "
                ";
                // line 29
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array()) || ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()) % (isset($context["itemspercolumn"]) || arraykeyexists("itemspercolumn", $context) ? $context["itemspercolumn"] : (function () { throw new TwigErrorRuntime('Variable "itemspercolumn" does not exist.', 29, $this->getSourceContext()); })())) == 0))) {
                    // line 30
                    echo "                    ";
                    if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                        // line 31
                        echo "                        <div class=\"container-fluid\">
                            <div class=\"row\">
                    ";
                    } else {
                        // line 34
                        echo "                        </ul>
                    ";
                    }
                    // line 36
                    echo "
                    <ul class=\"dropdown-menu";
                    // line 37
                    if (((isset($context["columncount"]) || arraykeyexists("columncount", $context) ? $context["columncount"] : (function () { throw new TwigErrorRuntime('Variable "columncount" does not exist.', 37, $this->getSourceContext()); })()) > 1)) {
                        echo " col-md-";
                        echo twigescapefilter($this->env, twiground((12 / (isset($context["columncount"]) || arraykeyexists("columncount", $context) ? $context["columncount"] : (function () { throw new TwigErrorRuntime('Variable "columncount" does not exist.', 37, $this->getSourceContext()); })()))), "html", null, true);
                    }
                    echo "\">
                ";
                }
                // line 39
                echo "
                ";
                // line 40
                if (((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()) % (isset($context["itemspercolumn"]) || arraykeyexists("itemspercolumn", $context) ? $context["itemspercolumn"] : (function () { throw new TwigErrorRuntime('Variable "itemspercolumn" does not exist.', 40, $this->getSourceContext()); })())) != 0)) {
                    // line 41
                    echo "                    <li role=\"presentation\" class=\"divider\"></li>
                ";
                }
                // line 43
                echo "                <li role=\"presentation\" class=\"dropdown-header\">
                    ";
                // line 44
                echo twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "icon", array());
                echo "
                    ";
                // line 45
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "labelcatalogue", array())), "html", null, true);
                echo "
                </li>

                ";
                // line 48
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "items", array()));
                foreach ($context['seq'] as $context["key"] => $context["admin"]) {
                    // line 49
                    echo "                    ";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "create"), "method"))) {
                        // line 50
                        echo "                        ";
                        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "subClasses", array()))) {
                            // line 51
                            echo "                            <li role=\"presentation\">
                                <a role=\"menuitem\" tabindex=\"-1\" href=\"";
                            // line 52
                            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "generateUrl", array(0 => "create"), "method"), "html", null, true);
                            echo "\">";
                            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "translationdomain", array())), "html", null, true);
                            echo "</a>
                            </li>
                        ";
                        } else {
                            // line 55
                            echo "                            ";
                            $context['parent'] = $context;
                            $context['seq'] = twigensuretraversable(twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "subclasses", array())));
                            foreach ($context['seq'] as $context["key"] => $context["subclass"]) {
                                // line 56
                                echo "                                <li role=\"presentation\">
                                    <a role=\"menuitem\" tabindex=\"-1\" href=\"";
                                // line 57
                                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "generateUrl", array(0 => "create", 1 => array("subclass" => $context["subclass"])), "method"), "html", null, true);
                                echo "\">";
                                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["subclass"], array(), twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "translationdomain", array())), "html", null, true);
                                echo "</a>
                                </li>
                            ";
                            }
                            $parent = $context['parent'];
                            unset($context['seq'], $context['iterated'], $context['key'], $context['subclass'], $context['parent'], $context['loop']);
                            $context = arrayintersectkey($context, $parent) + $parent;
                            // line 60
                            echo "                        ";
                        }
                        // line 61
                        echo "                    ";
                    }
                    // line 62
                    echo "                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['admin'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 63
                echo "
                ";
                // line 64
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    // line 65
                    echo "                            </ul>
                        </div>
                    </div>
                ";
                }
                // line 69
                echo "
            ";
            }
            // line 71
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['group'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 72
        echo "    </div>
";
        
        $internalbfc5e01aaf521fae7ae085fdb3cfdc9fb344afc322c73031d52d7f42baaf5f78->leave($internalbfc5e01aaf521fae7ae085fdb3cfdc9fb344afc322c73031d52d7f42baaf5f78prof);

        
        $internal2c324e81fcd4256757bb8629e87b7e804582204a5c9f152e3cb99a993c135377->leave($internal2c324e81fcd4256757bb8629e87b7e804582204a5c9f152e3cb99a993c135377prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:addblock.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  283 => 72,  269 => 71,  265 => 69,  259 => 65,  257 => 64,  254 => 63,  248 => 62,  245 => 61,  242 => 60,  231 => 57,  228 => 56,  223 => 55,  215 => 52,  212 => 51,  209 => 50,  206 => 49,  202 => 48,  196 => 45,  192 => 44,  189 => 43,  185 => 41,  183 => 40,  180 => 39,  172 => 37,  169 => 36,  165 => 34,  160 => 31,  157 => 30,  155 => 29,  152 => 28,  150 => 27,  147 => 26,  140 => 25,  137 => 24,  131 => 23,  128 => 22,  111 => 21,  108 => 20,  102 => 19,  98 => 17,  96 => 16,  93 => 15,  87 => 14,  80 => 13,  77 => 12,  74 => 11,  71 => 10,  68 => 9,  63 => 8,  60 => 7,  57 => 6,  53 => 5,  50 => 4,  47 => 3,  44 => 2,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% block userblock %}
    {% set itemspercolumn = sonataadmin.adminPool.getOption('dropdownnumbergroupspercolums') %}
    {% set groups = [] %}

    {% for group in sonataadmin.adminPool.dashboardgroups %}
        {% set displaygroup = false %}

        {% for admin in group.items if displaygroup == false %}
            {% if admin.hasRoute('create') and admin.hasAccess('create') %}
                {% set displaygroup = true %}
                {% set groups = [group]|merge(groups) %}
            {% endif %}
        {% endfor %}
    {% endfor %}

    {% set columncount = (groups|length / itemspercolumn)|round(0, 'ceil') %}

    <div class=\"dropdown-menu multi-column dropdown-add\"
        {% if columncount > 1 %}style=\"width: {{ columncount*140 }}px;\"{% endif %}
            >
        {% for group in groups|reverse %}
            {% set display = (group.roles is empty or isgranted('ROLESONATAADMIN') ) %}
            {% for role in group.roles if not display %}
                {% set display = isgranted(role) %}
            {% endfor %}

            {% if display %}

                {% if loop.first or loop.index0 % itemspercolumn == 0 %}
                    {% if loop.first %}
                        <div class=\"container-fluid\">
                            <div class=\"row\">
                    {% else %}
                        </ul>
                    {% endif %}

                    <ul class=\"dropdown-menu{% if columncount > 1 %} col-md-{{ (12/columncount)|round }}{% endif %}\">
                {% endif %}

                {% if loop.index0 % itemspercolumn != 0 %}
                    <li role=\"presentation\" class=\"divider\"></li>
                {% endif %}
                <li role=\"presentation\" class=\"dropdown-header\">
                    {{ group.icon|raw }}
                    {{ group.label|trans({}, group.labelcatalogue) }}
                </li>

                {% for admin in group.items %}
                    {% if admin.hasRoute('create') and admin.hasAccess('create') %}
                        {% if admin.subClasses is empty %}
                            <li role=\"presentation\">
                                <a role=\"menuitem\" tabindex=\"-1\" href=\"{{ admin.generateUrl('create')}}\">{{ admin.label|trans({}, admin.translationdomain) }}</a>
                            </li>
                        {% else %}
                            {% for subclass in admin.subclasses|keys %}
                                <li role=\"presentation\">
                                    <a role=\"menuitem\" tabindex=\"-1\" href=\"{{ admin.generateUrl('create', {'subclass': subclass}) }}\">{{ subclass|trans({}, admin.translationdomain) }}</a>
                                </li>
                            {% endfor %}
                        {% endif %}
                    {% endif %}
                {% endfor %}

                {% if loop.last %}
                            </ul>
                        </div>
                    </div>
                {% endif %}

            {% endif %}
        {% endfor %}
    </div>
{% endblock %}
", "SonataAdminBundle:Core:addblock.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/addblock.html.twig");
    }
}
