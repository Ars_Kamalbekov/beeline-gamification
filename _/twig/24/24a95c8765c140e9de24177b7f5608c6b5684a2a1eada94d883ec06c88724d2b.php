<?php

/* FOSUserBundle:Security:login.html.twig */
class TwigTemplate2bcec543bbc3dff3f705146d296bb3e602d8880deaa72a871eb092c8bab97067 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal993529ca6a2faf6d5b1c684da769b8381f6ec2b7f5b2c9065989eb37c3ed9c0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal993529ca6a2faf6d5b1c684da769b8381f6ec2b7f5b2c9065989eb37c3ed9c0d->enter($internal993529ca6a2faf6d5b1c684da769b8381f6ec2b7f5b2c9065989eb37c3ed9c0dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $internal73f83ebce87847f802b5aae3d8812f32bdf84a28f266fdc0b36409f70c901f33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal73f83ebce87847f802b5aae3d8812f32bdf84a28f266fdc0b36409f70c901f33->enter($internal73f83ebce87847f802b5aae3d8812f32bdf84a28f266fdc0b36409f70c901f33prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal993529ca6a2faf6d5b1c684da769b8381f6ec2b7f5b2c9065989eb37c3ed9c0d->leave($internal993529ca6a2faf6d5b1c684da769b8381f6ec2b7f5b2c9065989eb37c3ed9c0dprof);

        
        $internal73f83ebce87847f802b5aae3d8812f32bdf84a28f266fdc0b36409f70c901f33->leave($internal73f83ebce87847f802b5aae3d8812f32bdf84a28f266fdc0b36409f70c901f33prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal7912f602839314fbd59ce3f6640fdf31b5229bd135f8e5d0f2572af8516aacff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7912f602839314fbd59ce3f6640fdf31b5229bd135f8e5d0f2572af8516aacff->enter($internal7912f602839314fbd59ce3f6640fdf31b5229bd135f8e5d0f2572af8516aacffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal60e28c1060938504c31f84c2c04493916f71d6aa893a280e6ff5b7d6d58820d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal60e28c1060938504c31f84c2c04493916f71d6aa893a280e6ff5b7d6d58820d7->enter($internal60e28c1060938504c31f84c2c04493916f71d6aa893a280e6ff5b7d6d58820d7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        echo "    ";
        echo twiginclude($this->env, $context, "@FOSUser/Registration/logincontent.html.twig");
        echo "
";
        
        $internal60e28c1060938504c31f84c2c04493916f71d6aa893a280e6ff5b7d6d58820d7->leave($internal60e28c1060938504c31f84c2c04493916f71d6aa893a280e6ff5b7d6d58820d7prof);

        
        $internal7912f602839314fbd59ce3f6640fdf31b5229bd135f8e5d0f2572af8516aacff->leave($internal7912f602839314fbd59ce3f6640fdf31b5229bd135f8e5d0f2572af8516aacffprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
    {{ include('@FOSUser/Registration/logincontent.html.twig') }}
{% endblock fosusercontent %}
", "FOSUserBundle:Security:login.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
