<?php

/* SonataBlockBundle:Block:blockcoretext.html.twig */
class TwigTemplatea1388127e5ba5af88c0846e26cfa4b44b365eaaa6034f2ea8660a68e19e86b8c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockcoretext.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc9bc4ec2871d5fb284b6f6732a6b6fb0a660de6b7570f52b2bcd2c4c2f3bd1b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc9bc4ec2871d5fb284b6f6732a6b6fb0a660de6b7570f52b2bcd2c4c2f3bd1b0->enter($internalc9bc4ec2871d5fb284b6f6732a6b6fb0a660de6b7570f52b2bcd2c4c2f3bd1b0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoretext.html.twig"));

        $internal8325402aacbb2f85610f2d9439ae8b5a804fbe8d654bcf88a38f2da1f698378d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8325402aacbb2f85610f2d9439ae8b5a804fbe8d654bcf88a38f2da1f698378d->enter($internal8325402aacbb2f85610f2d9439ae8b5a804fbe8d654bcf88a38f2da1f698378dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoretext.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc9bc4ec2871d5fb284b6f6732a6b6fb0a660de6b7570f52b2bcd2c4c2f3bd1b0->leave($internalc9bc4ec2871d5fb284b6f6732a6b6fb0a660de6b7570f52b2bcd2c4c2f3bd1b0prof);

        
        $internal8325402aacbb2f85610f2d9439ae8b5a804fbe8d654bcf88a38f2da1f698378d->leave($internal8325402aacbb2f85610f2d9439ae8b5a804fbe8d654bcf88a38f2da1f698378dprof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internala41ad9c3faa2280a86e94a8684057b64cd74346d36a9a108479567fc499dc83e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala41ad9c3faa2280a86e94a8684057b64cd74346d36a9a108479567fc499dc83e->enter($internala41ad9c3faa2280a86e94a8684057b64cd74346d36a9a108479567fc499dc83eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internale1053205abc3da589534e6620dc448992348be8091551ab84c9bb7bf2630b62b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale1053205abc3da589534e6620dc448992348be8091551ab84c9bb7bf2630b62b->enter($internale1053205abc3da589534e6620dc448992348be8091551ab84c9bb7bf2630b62bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "content", array());
        echo "
";
        
        $internale1053205abc3da589534e6620dc448992348be8091551ab84c9bb7bf2630b62b->leave($internale1053205abc3da589534e6620dc448992348be8091551ab84c9bb7bf2630b62bprof);

        
        $internala41ad9c3faa2280a86e94a8684057b64cd74346d36a9a108479567fc499dc83e->leave($internala41ad9c3faa2280a86e94a8684057b64cd74346d36a9a108479567fc499dc83eprof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockcoretext.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    {{ settings.content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:blockcoretext.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockcoretext.html.twig");
    }
}
