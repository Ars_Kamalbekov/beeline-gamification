<?php

/* SonataAdminBundle:Core:userblock.html.twig */
class TwigTemplate844bfc69ab148783d9fc614d0d5c2d27c26151470fefe6b1bf9eca7dc303f114 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'userblock' => array($this, 'blockuserblock'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2cf5e93e5297ca9d0ac9ecad5a6225c1f25892d495a18162c6a557d89f3f2542 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2cf5e93e5297ca9d0ac9ecad5a6225c1f25892d495a18162c6a557d89f3f2542->enter($internal2cf5e93e5297ca9d0ac9ecad5a6225c1f25892d495a18162c6a557d89f3f2542prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:userblock.html.twig"));

        $internal5001267e53557f02a1ea57507f77a0ea7ae89e2beefa18faa3281b50d24ec6f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5001267e53557f02a1ea57507f77a0ea7ae89e2beefa18faa3281b50d24ec6f7->enter($internal5001267e53557f02a1ea57507f77a0ea7ae89e2beefa18faa3281b50d24ec6f7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:userblock.html.twig"));

        // line 1
        $this->displayBlock('userblock', $context, $blocks);
        
        $internal2cf5e93e5297ca9d0ac9ecad5a6225c1f25892d495a18162c6a557d89f3f2542->leave($internal2cf5e93e5297ca9d0ac9ecad5a6225c1f25892d495a18162c6a557d89f3f2542prof);

        
        $internal5001267e53557f02a1ea57507f77a0ea7ae89e2beefa18faa3281b50d24ec6f7->leave($internal5001267e53557f02a1ea57507f77a0ea7ae89e2beefa18faa3281b50d24ec6f7prof);

    }

    public function blockuserblock($context, array $blocks = array())
    {
        $internal2f7a7bcb2337b3cfa74fe39571ee9af5712df8ada3abcff993b3d0e62a6a82c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2f7a7bcb2337b3cfa74fe39571ee9af5712df8ada3abcff993b3d0e62a6a82c9->enter($internal2f7a7bcb2337b3cfa74fe39571ee9af5712df8ada3abcff993b3d0e62a6a82c9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "userblock"));

        $internal51ded9ce4e56c75928504af987633ca2350d328bfc3dc2ea6f4f6fba64547f3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal51ded9ce4e56c75928504af987633ca2350d328bfc3dc2ea6f4f6fba64547f3c->enter($internal51ded9ce4e56c75928504af987633ca2350d328bfc3dc2ea6f4f6fba64547f3cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "userblock"));

        
        $internal51ded9ce4e56c75928504af987633ca2350d328bfc3dc2ea6f4f6fba64547f3c->leave($internal51ded9ce4e56c75928504af987633ca2350d328bfc3dc2ea6f4f6fba64547f3cprof);

        
        $internal2f7a7bcb2337b3cfa74fe39571ee9af5712df8ada3abcff993b3d0e62a6a82c9->leave($internal2f7a7bcb2337b3cfa74fe39571ee9af5712df8ada3abcff993b3d0e62a6a82c9prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:userblock.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% block userblock %}{# Customize this value #}{% endblock %}
", "SonataAdminBundle:Core:userblock.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/userblock.html.twig");
    }
}
