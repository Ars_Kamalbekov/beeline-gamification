<?php

/* SonataAdminBundle:CRUD:baselistflatfield.html.twig */
class TwigTemplated2b9a7db01cf3fe9ff63367dbc198aa6151cfed5e36bb9aea1e4d4190d7f7f4d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbc7f6c158c3b5bd03e3feacb184927ebb72ea348c1ab686ba44c0882e25acae5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbc7f6c158c3b5bd03e3feacb184927ebb72ea348c1ab686ba44c0882e25acae5->enter($internalbc7f6c158c3b5bd03e3feacb184927ebb72ea348c1ab686ba44c0882e25acae5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistflatfield.html.twig"));

        $internal8def9e1750dcad99c40427fdbedbcd85cb34437ed5981097fad2e2518951e57a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8def9e1750dcad99c40427fdbedbcd85cb34437ed5981097fad2e2518951e57a->enter($internal8def9e1750dcad99c40427fdbedbcd85cb34437ed5981097fad2e2518951e57aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistflatfield.html.twig"));

        // line 11
        echo "
<span class=\"sonata-ba-list-field sonata-ba-list-field-";
        // line 12
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "type", array()), "html", null, true);
        echo "\" objectId=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
    ";
        // line 13
        if ((((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 14
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "identifier", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 15
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", true, true)) && twiggetattribute($this->env, $this->getSourceContext(),         // line 16
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 16, $this->getSourceContext()); })()), "hasAccess", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 16, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(),         // line 17
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 17, $this->getSourceContext()); })()), "hasRoute", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array())), "method"))) {
            // line 19
            echo "        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 19, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 19, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
            echo "\">";
            // line 20
            $this->displayBlock('field', $context, $blocks);
            // line 21
            echo "</a>
    ";
        } else {
            // line 23
            echo "        ";
            $this->displayBlock("field", $context, $blocks);
            echo "
    ";
        }
        // line 25
        echo "</span>
";
        
        $internalbc7f6c158c3b5bd03e3feacb184927ebb72ea348c1ab686ba44c0882e25acae5->leave($internalbc7f6c158c3b5bd03e3feacb184927ebb72ea348c1ab686ba44c0882e25acae5prof);

        
        $internal8def9e1750dcad99c40427fdbedbcd85cb34437ed5981097fad2e2518951e57a->leave($internal8def9e1750dcad99c40427fdbedbcd85cb34437ed5981097fad2e2518951e57aprof);

    }

    // line 20
    public function blockfield($context, array $blocks = array())
    {
        $internal29899e8d72f60aeae0b2b716c45e04dbfca24fc1152cb96f5169b824e8807e52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29899e8d72f60aeae0b2b716c45e04dbfca24fc1152cb96f5169b824e8807e52->enter($internal29899e8d72f60aeae0b2b716c45e04dbfca24fc1152cb96f5169b824e8807e52prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalacb02bc75a96573b6f51d7242e2ee08409804703c786b4646248ce7008bdfc8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalacb02bc75a96573b6f51d7242e2ee08409804703c786b4646248ce7008bdfc8c->enter($internalacb02bc75a96573b6f51d7242e2ee08409804703c786b4646248ce7008bdfc8cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
        
        $internalacb02bc75a96573b6f51d7242e2ee08409804703c786b4646248ce7008bdfc8c->leave($internalacb02bc75a96573b6f51d7242e2ee08409804703c786b4646248ce7008bdfc8cprof);

        
        $internal29899e8d72f60aeae0b2b716c45e04dbfca24fc1152cb96f5169b824e8807e52->leave($internal29899e8d72f60aeae0b2b716c45e04dbfca24fc1152cb96f5169b824e8807e52prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baselistflatfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 20,  57 => 25,  51 => 23,  47 => 21,  45 => 20,  41 => 19,  39 => 17,  38 => 16,  37 => 15,  36 => 14,  35 => 13,  29 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<span class=\"sonata-ba-list-field sonata-ba-list-field-{{ fielddescription.type }}\" objectId=\"{{ admin.id(object) }}\">
    {% if
            fielddescription.options.identifier is defined
        and fielddescription.options.route is defined
        and admin.hasAccess(fielddescription.options.route.name, object)
        and admin.hasRoute(fielddescription.options.route.name)
    %}
        <a href=\"{{ admin.generateObjectUrl(fielddescription.options.route.name, object, fielddescription.options.route.parameters) }}\">
            {%- block field %}{{ value }}{% endblock -%}
        </a>
    {% else %}
        {{ block('field') }}
    {% endif %}
</span>
", "SonataAdminBundle:CRUD:baselistflatfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baselistflatfield.html.twig");
    }
}
