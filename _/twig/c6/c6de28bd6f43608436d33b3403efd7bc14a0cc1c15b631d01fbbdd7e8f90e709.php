<?php

/* SonataAdminBundle:CRUD/Association:showonetoone.html.twig */
class TwigTemplate5400e8af4a0ecc62ab382f28ca2f337e8af5a836cb236dcabf5d21a2b155dffd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD/Association:showonetoone.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfb026300f010e83670312c36901e929bbefe75b2b4c45630e7e1e1b05789e332 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfb026300f010e83670312c36901e929bbefe75b2b4c45630e7e1e1b05789e332->enter($internalfb026300f010e83670312c36901e929bbefe75b2b4c45630e7e1e1b05789e332prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showonetoone.html.twig"));

        $internalc2937d815fa30ff3c8642d7a675085990bfa3e039820c6a2174a1d63d3b6529e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc2937d815fa30ff3c8642d7a675085990bfa3e039820c6a2174a1d63d3b6529e->enter($internalc2937d815fa30ff3c8642d7a675085990bfa3e039820c6a2174a1d63d3b6529eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showonetoone.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalfb026300f010e83670312c36901e929bbefe75b2b4c45630e7e1e1b05789e332->leave($internalfb026300f010e83670312c36901e929bbefe75b2b4c45630e7e1e1b05789e332prof);

        
        $internalc2937d815fa30ff3c8642d7a675085990bfa3e039820c6a2174a1d63d3b6529e->leave($internalc2937d815fa30ff3c8642d7a675085990bfa3e039820c6a2174a1d63d3b6529eprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal269316ce1c058a8e953127a62f4eba5ee707342c2c3128ba5db426faa1767848 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal269316ce1c058a8e953127a62f4eba5ee707342c2c3128ba5db426faa1767848->enter($internal269316ce1c058a8e953127a62f4eba5ee707342c2c3128ba5db426faa1767848prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internala2a103a7e9211a3513d8953ecb856377a397a9c5a68d3f6eca9e05b64b0a4877 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala2a103a7e9211a3513d8953ecb856377a397a9c5a68d3f6eca9e05b64b0a4877->enter($internala2a103a7e9211a3513d8953ecb856377a397a9c5a68d3f6eca9e05b64b0a4877prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasAssociationAdmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "associationadmin", array()), "id", array(0 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 18
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())), "method"))) {
            // line 20
            echo "        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
            echo "\">
            ";
            // line 21
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })())), "html", null, true);
            echo "
        </a>
    ";
        } else {
            // line 24
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 24, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 24, $this->getSourceContext()); })())), "html", null, true);
            echo "
    ";
        }
        
        $internala2a103a7e9211a3513d8953ecb856377a397a9c5a68d3f6eca9e05b64b0a4877->leave($internala2a103a7e9211a3513d8953ecb856377a397a9c5a68d3f6eca9e05b64b0a4877prof);

        
        $internal269316ce1c058a8e953127a62f4eba5ee707342c2c3128ba5db426faa1767848->leave($internal269316ce1c058a8e953127a62f4eba5ee707342c2c3128ba5db426faa1767848prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:showonetoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 24,  63 => 21,  58 => 20,  56 => 19,  55 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasAssociationAdmin
    and fielddescription.associationadmin.id(value)
    and fielddescription.associationadmin.hasRoute(routename)
    and fielddescription.associationadmin.hasAccess(routename, value) %}
        <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, value, fielddescription.options.route.parameters) }}\">
            {{ value|renderrelationelement(fielddescription) }}
        </a>
    {% else %}
        {{ value|renderrelationelement(fielddescription) }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD/Association:showonetoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/showonetoone.html.twig");
    }
}
