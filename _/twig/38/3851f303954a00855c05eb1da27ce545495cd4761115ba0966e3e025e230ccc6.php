<?php

/* SonataAdminBundle:Button:aclbutton.html.twig */
class TwigTemplatee43024cbddc6fd7d9a7c2bdc16b09df9bdea942146835fa2e3eee2d6ed7a4e52 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala93090688925fab7230cd96e84ef635390e4d259cc57ff80a4272651f07fc818 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala93090688925fab7230cd96e84ef635390e4d259cc57ff80a4272651f07fc818->enter($internala93090688925fab7230cd96e84ef635390e4d259cc57ff80a4272651f07fc818prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:aclbutton.html.twig"));

        $internale9a078e1b45de7d75cfc4b0b8f21a9f451dc84becfc12ef3abc89168bbb7c315 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale9a078e1b45de7d75cfc4b0b8f21a9f451dc84becfc12ef3abc89168bbb7c315->enter($internale9a078e1b45de7d75cfc4b0b8f21a9f451dc84becfc12ef3abc89168bbb7c315prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:aclbutton.html.twig"));

        // line 11
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "isAclEnabled", array(), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "canAccessObject", array(0 => "acl", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 11, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "hasRoute", array(0 => "acl"), "method"))) {
            // line 12
            echo "    <li>
        <a class=\"sonata-action-element\" href=\"";
            // line 13
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "acl", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 13, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
            <i class=\"fa fa-users\" aria-hidden=\"true\"></i>
            ";
            // line 15
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionacl", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </a>
    </li>
";
        }
        
        $internala93090688925fab7230cd96e84ef635390e4d259cc57ff80a4272651f07fc818->leave($internala93090688925fab7230cd96e84ef635390e4d259cc57ff80a4272651f07fc818prof);

        
        $internale9a078e1b45de7d75cfc4b0b8f21a9f451dc84becfc12ef3abc89168bbb7c315->leave($internale9a078e1b45de7d75cfc4b0b8f21a9f451dc84becfc12ef3abc89168bbb7c315prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Button:aclbutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 15,  30 => 13,  27 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% if admin.isAclEnabled() and admin.canAccessObject('acl', object) and admin.hasRoute('acl') %}
    <li>
        <a class=\"sonata-action-element\" href=\"{{ admin.generateObjectUrl('acl', object) }}\">
            <i class=\"fa fa-users\" aria-hidden=\"true\"></i>
            {{ 'linkactionacl'|trans({}, 'SonataAdminBundle') }}
        </a>
    </li>
{% endif %}
", "SonataAdminBundle:Button:aclbutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Button/aclbutton.html.twig");
    }
}
