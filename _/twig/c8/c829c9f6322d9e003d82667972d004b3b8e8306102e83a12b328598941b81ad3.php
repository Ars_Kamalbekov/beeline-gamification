<?php

/* SonataBlockBundle:Block:blockcontainer.html.twig */
class TwigTemplateabe2a3dce5494f2f3c426512fe9349b0447a493272c6e18706b8e570be7cc09e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'blockclass' => array($this, 'blockblockclass'),
            'blockrole' => array($this, 'blockblockrole'),
            'block' => array($this, 'blockblock'),
            'blockchildrender' => array($this, 'blockblockchildrender'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockcontainer.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9307daff3301a2b22e8b77f87ebe7bde97eaa19f20427dd7ea014260a317cb06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9307daff3301a2b22e8b77f87ebe7bde97eaa19f20427dd7ea014260a317cb06->enter($internal9307daff3301a2b22e8b77f87ebe7bde97eaa19f20427dd7ea014260a317cb06prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcontainer.html.twig"));

        $internal3844d94f1302908a5281fa244a54aa11f33792541bd1d896fed7d8978907dbc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3844d94f1302908a5281fa244a54aa11f33792541bd1d896fed7d8978907dbc7->enter($internal3844d94f1302908a5281fa244a54aa11f33792541bd1d896fed7d8978907dbc7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcontainer.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal9307daff3301a2b22e8b77f87ebe7bde97eaa19f20427dd7ea014260a317cb06->leave($internal9307daff3301a2b22e8b77f87ebe7bde97eaa19f20427dd7ea014260a317cb06prof);

        
        $internal3844d94f1302908a5281fa244a54aa11f33792541bd1d896fed7d8978907dbc7->leave($internal3844d94f1302908a5281fa244a54aa11f33792541bd1d896fed7d8978907dbc7prof);

    }

    // line 15
    public function blockblockclass($context, array $blocks = array())
    {
        $internalca58e91d6d149de6d8eef992f5dd9feba310d565916a95be206d782a8e865c58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalca58e91d6d149de6d8eef992f5dd9feba310d565916a95be206d782a8e865c58->enter($internalca58e91d6d149de6d8eef992f5dd9feba310d565916a95be206d782a8e865c58prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockclass"));

        $internal68fb8227b758f9d40752419a8036132f12c5c7ea4192c78b2955a77eb9680e89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal68fb8227b758f9d40752419a8036132f12c5c7ea4192c78b2955a77eb9680e89->enter($internal68fb8227b758f9d40752419a8036132f12c5c7ea4192c78b2955a77eb9680e89prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockclass"));

        echo " cms-container";
        if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["block"]) || arraykeyexists("block", $context) ? $context["block"] : (function () { throw new TwigErrorRuntime('Variable "block" does not exist.', 15, $this->getSourceContext()); })()), "hasParent", array(), "method")) {
            echo " cms-container-root";
        }
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "class", array())) {
            echo " ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 15, $this->getSourceContext()); })()), "class", array()), "html", null, true);
        }
        
        $internal68fb8227b758f9d40752419a8036132f12c5c7ea4192c78b2955a77eb9680e89->leave($internal68fb8227b758f9d40752419a8036132f12c5c7ea4192c78b2955a77eb9680e89prof);

        
        $internalca58e91d6d149de6d8eef992f5dd9feba310d565916a95be206d782a8e865c58->leave($internalca58e91d6d149de6d8eef992f5dd9feba310d565916a95be206d782a8e865c58prof);

    }

    // line 18
    public function blockblockrole($context, array $blocks = array())
    {
        $internal1fc93bf93cbdd5641b9ed861d238eb1b05b465f46008fd699c09cd8e5c738727 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1fc93bf93cbdd5641b9ed861d238eb1b05b465f46008fd699c09cd8e5c738727->enter($internal1fc93bf93cbdd5641b9ed861d238eb1b05b465f46008fd699c09cd8e5c738727prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockrole"));

        $internal4eb22899b92ac29bc06fe2c5ab57625a4aab2b49e68cf2a98734691683191bc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4eb22899b92ac29bc06fe2c5ab57625a4aab2b49e68cf2a98734691683191bc0->enter($internal4eb22899b92ac29bc06fe2c5ab57625a4aab2b49e68cf2a98734691683191bc0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockrole"));

        echo "container";
        
        $internal4eb22899b92ac29bc06fe2c5ab57625a4aab2b49e68cf2a98734691683191bc0->leave($internal4eb22899b92ac29bc06fe2c5ab57625a4aab2b49e68cf2a98734691683191bc0prof);

        
        $internal1fc93bf93cbdd5641b9ed861d238eb1b05b465f46008fd699c09cd8e5c738727->leave($internal1fc93bf93cbdd5641b9ed861d238eb1b05b465f46008fd699c09cd8e5c738727prof);

    }

    // line 21
    public function blockblock($context, array $blocks = array())
    {
        $internalc305ddb9bb6a06365d00d54781661cf196bdf46c4c2e9806a12fed19ee5e5ae2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc305ddb9bb6a06365d00d54781661cf196bdf46c4c2e9806a12fed19ee5e5ae2->enter($internalc305ddb9bb6a06365d00d54781661cf196bdf46c4c2e9806a12fed19ee5e5ae2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internalf945fdad8ab0513b92e010e3e07984dd67e61737754150dfb26294dbbbcce05b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf945fdad8ab0513b92e010e3e07984dd67e61737754150dfb26294dbbbcce05b->enter($internalf945fdad8ab0513b92e010e3e07984dd67e61737754150dfb26294dbbbcce05bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 22
        echo "    ";
        if ((isset($context["decorator"]) || arraykeyexists("decorator", $context) ? $context["decorator"] : (function () { throw new TwigErrorRuntime('Variable "decorator" does not exist.', 22, $this->getSourceContext()); })())) {
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["decorator"]) || arraykeyexists("decorator", $context) ? $context["decorator"] : (function () { throw new TwigErrorRuntime('Variable "decorator" does not exist.', 22, $this->getSourceContext()); })()), "pre", array());
        }
        // line 23
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["block"]) || arraykeyexists("block", $context) ? $context["block"] : (function () { throw new TwigErrorRuntime('Variable "block" does not exist.', 23, $this->getSourceContext()); })()), "children", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["child"]) {
            // line 24
            echo "        ";
            $this->displayBlock('blockchildrender', $context, $blocks);
            // line 27
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 28
        echo "    ";
        if ((isset($context["decorator"]) || arraykeyexists("decorator", $context) ? $context["decorator"] : (function () { throw new TwigErrorRuntime('Variable "decorator" does not exist.', 28, $this->getSourceContext()); })())) {
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["decorator"]) || arraykeyexists("decorator", $context) ? $context["decorator"] : (function () { throw new TwigErrorRuntime('Variable "decorator" does not exist.', 28, $this->getSourceContext()); })()), "post", array());
        }
        
        $internalf945fdad8ab0513b92e010e3e07984dd67e61737754150dfb26294dbbbcce05b->leave($internalf945fdad8ab0513b92e010e3e07984dd67e61737754150dfb26294dbbbcce05bprof);

        
        $internalc305ddb9bb6a06365d00d54781661cf196bdf46c4c2e9806a12fed19ee5e5ae2->leave($internalc305ddb9bb6a06365d00d54781661cf196bdf46c4c2e9806a12fed19ee5e5ae2prof);

    }

    // line 24
    public function blockblockchildrender($context, array $blocks = array())
    {
        $internal60e85df22bf0c1879c87e69464ce0eb4e737613ae918483bfd12fa4935ba1e75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal60e85df22bf0c1879c87e69464ce0eb4e737613ae918483bfd12fa4935ba1e75->enter($internal60e85df22bf0c1879c87e69464ce0eb4e737613ae918483bfd12fa4935ba1e75prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockchildrender"));

        $internal40be9cdcfab3f640b96afda40ea4665862fcb66731bcb998692376fcc8746973 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal40be9cdcfab3f640b96afda40ea4665862fcb66731bcb998692376fcc8746973->enter($internal40be9cdcfab3f640b96afda40ea4665862fcb66731bcb998692376fcc8746973prof = new TwigProfilerProfile($this->getTemplateName(), "block", "blockchildrender"));

        // line 25
        echo "            ";
        echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array((isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 25, $this->getSourceContext()); })())));
        echo "
        ";
        
        $internal40be9cdcfab3f640b96afda40ea4665862fcb66731bcb998692376fcc8746973->leave($internal40be9cdcfab3f640b96afda40ea4665862fcb66731bcb998692376fcc8746973prof);

        
        $internal60e85df22bf0c1879c87e69464ce0eb4e737613ae918483bfd12fa4935ba1e75->leave($internal60e85df22bf0c1879c87e69464ce0eb4e737613ae918483bfd12fa4935ba1e75prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockcontainer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 25,  147 => 24,  134 => 28,  120 => 27,  117 => 24,  99 => 23,  94 => 22,  85 => 21,  67 => 18,  42 => 15,  21 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{# block classes are prepended with a container class #}
{% block blockclass %} cms-container{% if not block.hasParent() %} cms-container-root{%endif%}{% if settings.class %} {{ settings.class }}{% endif %}{% endblock %}

{# identify a block role used by the page editor #}
{% block blockrole %}container{% endblock %}

{# render container block #}
{% block block %}
    {% if decorator %}{{ decorator.pre|raw }}{% endif %}
    {% for child in block.children %}
        {% block blockchildrender %}
            {{ sonatablockrender(child) }}
        {% endblock %}
    {% endfor %}
    {% if decorator %}{{ decorator.post|raw }}{% endif %}
{% endblock %}
", "SonataBlockBundle:Block:blockcontainer.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockcontainer.html.twig");
    }
}
