<?php

/* SonataAdminBundle:CRUD/Association:editmodal.html.twig */
class TwigTemplate421b22c5a0d91a3d68df466dfa0ce56a7f7b85c769030e1034f2b6b39b04d91b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcd856c0bc9bf988d3fa156f82af1a75f7a378393b5b59c7b13a9d68ec494dcee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcd856c0bc9bf988d3fa156f82af1a75f7a378393b5b59c7b13a9d68ec494dcee->enter($internalcd856c0bc9bf988d3fa156f82af1a75f7a378393b5b59c7b13a9d68ec494dceeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmodal.html.twig"));

        $internal03faebc7449aab933920909e0c0ffc8484da82aa967ad930e685f989c148b2c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal03faebc7449aab933920909e0c0ffc8484da82aa967ad930e685f989c148b2c8->enter($internal03faebc7449aab933920909e0c0ffc8484da82aa967ad930e685f989c148b2c8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmodal.html.twig"));

        // line 11
        echo "
<div class=\"modal fade\" id=\"fielddialog";
        // line 12
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
        </div>
    </div>
</div>
";
        
        $internalcd856c0bc9bf988d3fa156f82af1a75f7a378393b5b59c7b13a9d68ec494dcee->leave($internalcd856c0bc9bf988d3fa156f82af1a75f7a378393b5b59c7b13a9d68ec494dceeprof);

        
        $internal03faebc7449aab933920909e0c0ffc8484da82aa967ad930e685f989c148b2c8->leave($internal03faebc7449aab933920909e0c0ffc8484da82aa967ad930e685f989c148b2c8prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editmodal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div class=\"modal fade\" id=\"fielddialog{{ id }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
        </div>
    </div>
</div>
", "SonataAdminBundle:CRUD/Association:editmodal.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editmodal.html.twig");
    }
}
