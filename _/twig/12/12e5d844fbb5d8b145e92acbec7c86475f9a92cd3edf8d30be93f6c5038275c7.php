<?php

/* FOSUserBundle:Registration:register.html.twig */
class TwigTemplatec36ae8fd4841d0e942cd86a0f1946c29940895989b940d7c775866f1f2409e40 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalea68ed40d5dfe67b3c811668aef28c98dee6f400cbe8bceb6842f02a46409a48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalea68ed40d5dfe67b3c811668aef28c98dee6f400cbe8bceb6842f02a46409a48->enter($internalea68ed40d5dfe67b3c811668aef28c98dee6f400cbe8bceb6842f02a46409a48prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $internalb6ea5752ee1f3996b6ac66d07d85e301b53e50d8c8f3aab15937894400b83976 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb6ea5752ee1f3996b6ac66d07d85e301b53e50d8c8f3aab15937894400b83976->enter($internalb6ea5752ee1f3996b6ac66d07d85e301b53e50d8c8f3aab15937894400b83976prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalea68ed40d5dfe67b3c811668aef28c98dee6f400cbe8bceb6842f02a46409a48->leave($internalea68ed40d5dfe67b3c811668aef28c98dee6f400cbe8bceb6842f02a46409a48prof);

        
        $internalb6ea5752ee1f3996b6ac66d07d85e301b53e50d8c8f3aab15937894400b83976->leave($internalb6ea5752ee1f3996b6ac66d07d85e301b53e50d8c8f3aab15937894400b83976prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal37dcdc0eb538f51d5fd769da0ecfa8a8c677dbd695122ae073b704d38bb4e392 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal37dcdc0eb538f51d5fd769da0ecfa8a8c677dbd695122ae073b704d38bb4e392->enter($internal37dcdc0eb538f51d5fd769da0ecfa8a8c677dbd695122ae073b704d38bb4e392prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal1c9fed27b9618d2b974baaf510da5a2191bebdcd6c9b1016e4d4c43b37e58373 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1c9fed27b9618d2b974baaf510da5a2191bebdcd6c9b1016e4d4c43b37e58373->enter($internal1c9fed27b9618d2b974baaf510da5a2191bebdcd6c9b1016e4d4c43b37e58373prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/registercontent.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $internal1c9fed27b9618d2b974baaf510da5a2191bebdcd6c9b1016e4d4c43b37e58373->leave($internal1c9fed27b9618d2b974baaf510da5a2191bebdcd6c9b1016e4d4c43b37e58373prof);

        
        $internal37dcdc0eb538f51d5fd769da0ecfa8a8c677dbd695122ae073b704d38bb4e392->leave($internal37dcdc0eb538f51d5fd769da0ecfa8a8c677dbd695122ae073b704d38bb4e392prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Registration/registercontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Registration:register.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Registration/register.html.twig");
    }
}
