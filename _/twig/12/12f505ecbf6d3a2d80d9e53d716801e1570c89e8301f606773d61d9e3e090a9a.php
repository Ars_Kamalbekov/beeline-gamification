<?php

/* DebugBundle:Profiler:dump.html.twig */
class TwigTemplate748654b9fd8ad12b57a261b5397320976d0bff1eb3c251432d96edd5c1ece004 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "DebugBundle:Profiler:dump.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal60f07a518adf1e1b5d093be3212929706b99e37a43b18ae6af4f00f016b5537c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal60f07a518adf1e1b5d093be3212929706b99e37a43b18ae6af4f00f016b5537c->enter($internal60f07a518adf1e1b5d093be3212929706b99e37a43b18ae6af4f00f016b5537cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "DebugBundle:Profiler:dump.html.twig"));

        $internalc7d7b77abc4f73ee21870e58d3110bd65fd9f0d3d3c8d9e0bec40248323cd394 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc7d7b77abc4f73ee21870e58d3110bd65fd9f0d3d3c8d9e0bec40248323cd394->enter($internalc7d7b77abc4f73ee21870e58d3110bd65fd9f0d3d3c8d9e0bec40248323cd394prof = new TwigProfilerProfile($this->getTemplateName(), "template", "DebugBundle:Profiler:dump.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal60f07a518adf1e1b5d093be3212929706b99e37a43b18ae6af4f00f016b5537c->leave($internal60f07a518adf1e1b5d093be3212929706b99e37a43b18ae6af4f00f016b5537cprof);

        
        $internalc7d7b77abc4f73ee21870e58d3110bd65fd9f0d3d3c8d9e0bec40248323cd394->leave($internalc7d7b77abc4f73ee21870e58d3110bd65fd9f0d3d3c8d9e0bec40248323cd394prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal4ec298af6b42c095f03ae2dc89df6764e05a659e7bb19a35d568f0fdcbfb9a8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4ec298af6b42c095f03ae2dc89df6764e05a659e7bb19a35d568f0fdcbfb9a8d->enter($internal4ec298af6b42c095f03ae2dc89df6764e05a659e7bb19a35d568f0fdcbfb9a8dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalb040e2e9e734810129fcb86f0e0ed410f88e67bb162253e1a84d7241bd653e1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb040e2e9e734810129fcb86f0e0ed410f88e67bb162253e1a84d7241bd653e1b->enter($internalb040e2e9e734810129fcb86f0e0ed410f88e67bb162253e1a84d7241bd653e1bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "dumpsCount", array())) {
            // line 5
            echo "        ";
            obstart();
            // line 6
            echo "            ";
            echo twiginclude($this->env, $context, "@Debug/Profiler/icon.svg");
            echo "
            <span class=\"sf-toolbar-value\">";
            // line 7
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 7, $this->getSourceContext()); })()), "dumpsCount", array()), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 9
            echo "
        ";
            // line 10
            obstart();
            // line 11
            echo "            ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "getDumps", array(0 => "html"), "method"));
            foreach ($context['seq'] as $context["key"] => $context["dump"]) {
                // line 12
                echo "                <div class=\"sf-toolbar-info-piece\">
                    <span>
                    ";
                // line 14
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array())) {
                    // line 15
                    echo "                        ";
                    $context["link"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->getFileLink(twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array()));
                    // line 16
                    echo "                        ";
                    if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 16, $this->getSourceContext()); })())) {
                        // line 17
                        echo "                            <a href=\"";
                        echo twigescapefilter($this->env, (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 17, $this->getSourceContext()); })()), "html", null, true);
                        echo "\" title=\"";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), "html", null, true);
                        echo "\">";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                        echo "</a>
                        ";
                    } else {
                        // line 19
                        echo "                            <abbr title=\"";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), "html", null, true);
                        echo "\">";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                        echo "</abbr>
                        ";
                    }
                    // line 21
                    echo "                    ";
                } else {
                    // line 22
                    echo "                        ";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                    echo "
                    ";
                }
                // line 24
                echo "                    </span>
                    <span class=\"sf-toolbar-file-line\">line ";
                // line 25
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array()), "html", null, true);
                echo "</span>

                    ";
                // line 27
                echo twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "data", array());
                echo "
                </div>
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['dump'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 30
            echo "        ";
            $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 31
            echo "
        ";
            // line 32
            echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => true));
            echo "
    ";
        }
        
        $internalb040e2e9e734810129fcb86f0e0ed410f88e67bb162253e1a84d7241bd653e1b->leave($internalb040e2e9e734810129fcb86f0e0ed410f88e67bb162253e1a84d7241bd653e1bprof);

        
        $internal4ec298af6b42c095f03ae2dc89df6764e05a659e7bb19a35d568f0fdcbfb9a8d->leave($internal4ec298af6b42c095f03ae2dc89df6764e05a659e7bb19a35d568f0fdcbfb9a8dprof);

    }

    // line 36
    public function blockmenu($context, array $blocks = array())
    {
        $internalfc5e6a4112ad28e16c766c32615c6d3f262299d963d41bb0169e71e52b789fd9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfc5e6a4112ad28e16c766c32615c6d3f262299d963d41bb0169e71e52b789fd9->enter($internalfc5e6a4112ad28e16c766c32615c6d3f262299d963d41bb0169e71e52b789fd9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internal0d3d0c61a8ebd3a5d2e2e3020fcc4fe006e6775475863ef2b537666b0c101e71 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0d3d0c61a8ebd3a5d2e2e3020fcc4fe006e6775475863ef2b537666b0c101e71->enter($internal0d3d0c61a8ebd3a5d2e2e3020fcc4fe006e6775475863ef2b537666b0c101e71prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 37
        echo "    <span class=\"label ";
        echo (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 37, $this->getSourceContext()); })()), "dumpsCount", array()) == 0)) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">";
        // line 38
        echo twiginclude($this->env, $context, "@Debug/Profiler/icon.svg");
        echo "</span>
        <strong>Debug</strong>
    </span>
";
        
        $internal0d3d0c61a8ebd3a5d2e2e3020fcc4fe006e6775475863ef2b537666b0c101e71->leave($internal0d3d0c61a8ebd3a5d2e2e3020fcc4fe006e6775475863ef2b537666b0c101e71prof);

        
        $internalfc5e6a4112ad28e16c766c32615c6d3f262299d963d41bb0169e71e52b789fd9->leave($internalfc5e6a4112ad28e16c766c32615c6d3f262299d963d41bb0169e71e52b789fd9prof);

    }

    // line 43
    public function blockpanel($context, array $blocks = array())
    {
        $internal449ca3b932ece2f3edb029437fe802e39348d1d184b4e4b21b47f27f487b8e6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal449ca3b932ece2f3edb029437fe802e39348d1d184b4e4b21b47f27f487b8e6a->enter($internal449ca3b932ece2f3edb029437fe802e39348d1d184b4e4b21b47f27f487b8e6aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internalb2207ab575b7bbf02963ebdc0e28aa6c71be26842bea4e9d121ca557bc910278 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb2207ab575b7bbf02963ebdc0e28aa6c71be26842bea4e9d121ca557bc910278->enter($internalb2207ab575b7bbf02963ebdc0e28aa6c71be26842bea4e9d121ca557bc910278prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 44
        echo "    <h2>Dumped Contents</h2>

    ";
        // line 46
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 46, $this->getSourceContext()); })()), "getDumps", array(0 => "html"), "method"));
        $context['iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["dump"]) {
            // line 47
            echo "        <div class=\"sf-dump sf-reset\">
            <span class=\"metadata\">In
                ";
            // line 49
            if (twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array())) {
                // line 50
                echo "                    ";
                $context["link"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->getFileLink(twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array()));
                // line 51
                echo "                    ";
                if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 51, $this->getSourceContext()); })())) {
                    // line 52
                    echo "                        <a href=\"";
                    echo twigescapefilter($this->env, (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 52, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" title=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), "html", null, true);
                    echo "\">";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                    echo "</a>
                    ";
                } else {
                    // line 54
                    echo "                        <abbr title=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), "html", null, true);
                    echo "\">";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                    echo "</abbr>
                    ";
                }
                // line 56
                echo "                ";
            } else {
                // line 57
                echo "                    ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "name", array()), "html", null, true);
                echo "
                ";
            }
            // line 59
            echo "                line <a class=\"text-small sf-toggle\" data-toggle-selector=\"#sf-trace-";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array()), "html", null, true);
            echo "</a>:
            </span>

            <div class=\"sf-dump-compact hidden\" id=\"sf-trace-";
            // line 62
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()), "html", null, true);
            echo "\">
                <div class=\"trace\">
                    ";
            // line 64
            echo ((twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "fileExcerpt", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "fileExcerpt", array())) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "file", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "line", array()))));
            echo "
                </div>
            </div>

            ";
            // line 68
            echo twiggetattribute($this->env, $this->getSourceContext(), $context["dump"], "data", array());
            echo "
        </div>
    ";
            $context['iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['iterated']) {
            // line 71
            echo "        <div class=\"empty\">
            <p>No content was dumped.</p>
        </div>
    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['dump'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internalb2207ab575b7bbf02963ebdc0e28aa6c71be26842bea4e9d121ca557bc910278->leave($internalb2207ab575b7bbf02963ebdc0e28aa6c71be26842bea4e9d121ca557bc910278prof);

        
        $internal449ca3b932ece2f3edb029437fe802e39348d1d184b4e4b21b47f27f487b8e6a->leave($internal449ca3b932ece2f3edb029437fe802e39348d1d184b4e4b21b47f27f487b8e6aprof);

    }

    public function getTemplateName()
    {
        return "DebugBundle:Profiler:dump.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  287 => 71,  271 => 68,  264 => 64,  259 => 62,  250 => 59,  244 => 57,  241 => 56,  233 => 54,  223 => 52,  220 => 51,  217 => 50,  215 => 49,  211 => 47,  193 => 46,  189 => 44,  180 => 43,  166 => 38,  161 => 37,  152 => 36,  139 => 32,  136 => 31,  133 => 30,  124 => 27,  119 => 25,  116 => 24,  110 => 22,  107 => 21,  99 => 19,  89 => 17,  86 => 16,  83 => 15,  81 => 14,  77 => 12,  72 => 11,  70 => 10,  67 => 9,  62 => 7,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% if collector.dumpsCount %}
        {% set icon %}
            {{ include('@Debug/Profiler/icon.svg') }}
            <span class=\"sf-toolbar-value\">{{ collector.dumpsCount }}</span>
        {% endset %}

        {% set text %}
            {% for dump in collector.getDumps('html') %}
                <div class=\"sf-toolbar-info-piece\">
                    <span>
                    {% if dump.file %}
                        {% set link = dump.file|filelink(dump.line) %}
                        {% if link %}
                            <a href=\"{{ link }}\" title=\"{{ dump.file }}\">{{ dump.name }}</a>
                        {% else %}
                            <abbr title=\"{{ dump.file }}\">{{ dump.name }}</abbr>
                        {% endif %}
                    {% else %}
                        {{ dump.name }}
                    {% endif %}
                    </span>
                    <span class=\"sf-toolbar-file-line\">line {{ dump.line }}</span>

                    {{ dump.data|raw }}
                </div>
            {% endfor %}
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { 'link': true }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.dumpsCount == 0 ? 'disabled' }}\">
        <span class=\"icon\">{{ include('@Debug/Profiler/icon.svg') }}</span>
        <strong>Debug</strong>
    </span>
{% endblock %}

{% block panel %}
    <h2>Dumped Contents</h2>

    {% for dump in collector.getDumps('html') %}
        <div class=\"sf-dump sf-reset\">
            <span class=\"metadata\">In
                {% if dump.line %}
                    {% set link = dump.file|filelink(dump.line) %}
                    {% if link %}
                        <a href=\"{{ link }}\" title=\"{{ dump.file }}\">{{ dump.name }}</a>
                    {% else %}
                        <abbr title=\"{{ dump.file }}\">{{ dump.name }}</abbr>
                    {% endif %}
                {% else %}
                    {{ dump.name }}
                {% endif %}
                line <a class=\"text-small sf-toggle\" data-toggle-selector=\"#sf-trace-{{ loop.index0 }}\">{{ dump.line }}</a>:
            </span>

            <div class=\"sf-dump-compact hidden\" id=\"sf-trace-{{ loop.index0 }}\">
                <div class=\"trace\">
                    {{ dump.fileExcerpt ? dump.fileExcerpt|raw : dump.file|fileexcerpt(dump.line) }}
                </div>
            </div>

            {{ dump.data|raw }}
        </div>
    {% else %}
        <div class=\"empty\">
            <p>No content was dumped.</p>
        </div>
    {% endfor %}
{% endblock %}
", "DebugBundle:Profiler:dump.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/DebugBundle/Resources/views/Profiler/dump.html.twig");
    }
}
