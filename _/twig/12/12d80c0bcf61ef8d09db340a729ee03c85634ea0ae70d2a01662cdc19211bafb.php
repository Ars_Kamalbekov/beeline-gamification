<?php

/* SonataAdminBundle:CRUD:listdatetime.html.twig */
class TwigTemplated085d134f303594562ab48998a28e682f4983149a2738dbbd2f7b94a7274c549 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listdatetime.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb11a13af8f5f3fb2be6298121b14d393dab0336547df7864a5fcecab11ce0e04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb11a13af8f5f3fb2be6298121b14d393dab0336547df7864a5fcecab11ce0e04->enter($internalb11a13af8f5f3fb2be6298121b14d393dab0336547df7864a5fcecab11ce0e04prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listdatetime.html.twig"));

        $internal5cb508a34a3ecabf58736c2b2f66a780dc89fa23fc3a0d668dae7bcabe7c7648 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5cb508a34a3ecabf58736c2b2f66a780dc89fa23fc3a0d668dae7bcabe7c7648->enter($internal5cb508a34a3ecabf58736c2b2f66a780dc89fa23fc3a0d668dae7bcabe7c7648prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listdatetime.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb11a13af8f5f3fb2be6298121b14d393dab0336547df7864a5fcecab11ce0e04->leave($internalb11a13af8f5f3fb2be6298121b14d393dab0336547df7864a5fcecab11ce0e04prof);

        
        $internal5cb508a34a3ecabf58736c2b2f66a780dc89fa23fc3a0d668dae7bcabe7c7648->leave($internal5cb508a34a3ecabf58736c2b2f66a780dc89fa23fc3a0d668dae7bcabe7c7648prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal2e68a93ee47898a8d7834f9a65b8bd2b88c76859c1512277291a3e433517234e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2e68a93ee47898a8d7834f9a65b8bd2b88c76859c1512277291a3e433517234e->enter($internal2e68a93ee47898a8d7834f9a65b8bd2b88c76859c1512277291a3e433517234eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internala4babb726a6ead0aa4d6a1cc348c4bae98673ecceb52eb7fc8b9f9f530379087 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala4babb726a6ead0aa4d6a1cc348c4bae98673ecceb52eb7fc8b9f9f530379087->enter($internala4babb726a6ead0aa4d6a1cc348c4bae98673ecceb52eb7fc8b9f9f530379087prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            $context["timezone"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "timezone", array(), "any", true, true)) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "timezone", array())) : (null));
            // line 19
            echo "        ";
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "options", array()), "format", array()), (isset($context["timezone"]) || arraykeyexists("timezone", $context) ? $context["timezone"] : (function () { throw new TwigErrorRuntime('Variable "timezone" does not exist.', 19, $this->getSourceContext()); })())), "html", null, true);
        } elseif (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 20
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "timezone", array(), "any", true, true)) {
            // line 21
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })()), null, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "options", array()), "timezone", array())), "html", null, true);
        } else {
            // line 23
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
        }
        
        $internala4babb726a6ead0aa4d6a1cc348c4bae98673ecceb52eb7fc8b9f9f530379087->leave($internala4babb726a6ead0aa4d6a1cc348c4bae98673ecceb52eb7fc8b9f9f530379087prof);

        
        $internal2e68a93ee47898a8d7834f9a65b8bd2b88c76859c1512277291a3e433517234e->leave($internal2e68a93ee47898a8d7834f9a65b8bd2b88c76859c1512277291a3e433517234eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listdatetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 23,  61 => 21,  59 => 20,  56 => 19,  54 => 18,  52 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif fielddescription.options.format is defined -%}
        {% set timezone = fielddescription.options.timezone is defined ? fielddescription.options.timezone : null %}
        {{ value|date(fielddescription.options.format, timezone) }}
    {%- elseif fielddescription.options.timezone is defined -%}
        {{ value|date(null, fielddescription.options.timezone) }}
    {%- else -%}
        {{ value|date }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:listdatetime.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listdatetime.html.twig");
    }
}
