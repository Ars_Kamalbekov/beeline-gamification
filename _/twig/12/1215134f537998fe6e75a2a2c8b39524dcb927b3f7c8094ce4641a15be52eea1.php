<?php

/* @Framework/Form/choiceattributes.html.php */
class TwigTemplatecaffaf3c35df11298a65fa60232241d6c601fd1c6b9a8518579380c68669d499 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb7f2271e7de9f355e2603265cfa9442e0423d30fcef55f31912930d1956b7a17 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb7f2271e7de9f355e2603265cfa9442e0423d30fcef55f31912930d1956b7a17->enter($internalb7f2271e7de9f355e2603265cfa9442e0423d30fcef55f31912930d1956b7a17prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choiceattributes.html.php"));

        $internal6c917ac7f4ebc465b9312184e8728e691dcb8fe3ea01eba68e7ab380d41aa2e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6c917ac7f4ebc465b9312184e8728e691dcb8fe3ea01eba68e7ab380d41aa2e2->enter($internal6c917ac7f4ebc465b9312184e8728e691dcb8fe3ea01eba68e7ab380d41aa2e2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choiceattributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choiceattr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $internalb7f2271e7de9f355e2603265cfa9442e0423d30fcef55f31912930d1956b7a17->leave($internalb7f2271e7de9f355e2603265cfa9442e0423d30fcef55f31912930d1956b7a17prof);

        
        $internal6c917ac7f4ebc465b9312184e8728e691dcb8fe3ea01eba68e7ab380d41aa2e2->leave($internal6c917ac7f4ebc465b9312184e8728e691dcb8fe3ea01eba68e7ab380d41aa2e2prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choiceattributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choiceattr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choiceattributes.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choiceattributes.html.php");
    }
}
