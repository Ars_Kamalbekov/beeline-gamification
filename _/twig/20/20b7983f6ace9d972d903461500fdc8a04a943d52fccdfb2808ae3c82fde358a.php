<?php

/* SonataAdminBundle:Form:silexformdivlayout.html.twig */
class TwigTemplatef6d9810e000ff5279759ad6a346840bc186980d83196f19db38ff45ac79f9506 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal41027e6c76756706fefdc2dcf770fc5474d86f60c5737bef34534ba859bc191f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal41027e6c76756706fefdc2dcf770fc5474d86f60c5737bef34534ba859bc191f->enter($internal41027e6c76756706fefdc2dcf770fc5474d86f60c5737bef34534ba859bc191fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:silexformdivlayout.html.twig"));

        $internalebf0355d1116235e3ed412281b6645bd5701af93d9d93c05c42d49b6aa0fc7a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalebf0355d1116235e3ed412281b6645bd5701af93d9d93c05c42d49b6aa0fc7a0->enter($internalebf0355d1116235e3ed412281b6645bd5701af93d9d93c05c42d49b6aa0fc7a0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:silexformdivlayout.html.twig"));

        // line 2
        echo "
";
        // line 7
        echo "
";
        
        $internal41027e6c76756706fefdc2dcf770fc5474d86f60c5737bef34534ba859bc191f->leave($internal41027e6c76756706fefdc2dcf770fc5474d86f60c5737bef34534ba859bc191fprof);

        
        $internalebf0355d1116235e3ed412281b6645bd5701af93d9d93c05c42d49b6aa0fc7a0->leave($internalebf0355d1116235e3ed412281b6645bd5701af93d9d93c05c42d49b6aa0fc7a0prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Form:silexformdivlayout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  28 => 7,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{# Widgets #}

{#
    This file doesn't seem to be used anymore ; commenting it for now in order not to mess with user's configurations.
    TODO: Remove it and all of its references
#}

{#
{% block choicewidget %}
{% spaceless %}
    {% if expanded %}
        <ul {{ block('widgetcontainerattributes') }} class=\"inputs-list\">
        {% for child in form %}
            <li>
                {% set formwidgetcontent %}
                    {{ formwidget(child) }}
                {% endset %}
                {{ formlabel(child, null, { 'inlistcheckbox' : true, 'widget' : formwidgetcontent } ) }}
            </li>
        {% endfor %}
        </ul>
    {% else %}
    <select {{ block('widgetattributes') }}{% if multiple %} multiple=\"multiple\"{% endif %}>
        {% if emptyvalue is not none %}
            <option value=\"\">{{ emptyvalue|trans({}, translationdomain) }}</option>
        {% endif %}
        {% if preferredchoices|length > 0 %}
            {% set options = preferredchoices %}
            {{ block('choicewidgetoptions') }}
            {% if choices|length > 0 and separator is not none %}
                <option disabled=\"disabled\">{{ separator }}</option>
            {% endif %}
        {% endif %}
        {% set options = choices %}
        {{ block('choicewidgetoptions') }}
    </select>
    {% endif %}
{% endspaceless %}
{% endblock choicewidget %}

{% block formwidgetsimple %}
{% spaceless %}
    {% set type = type|default('text') %}
    <input type=\"{{ type }}\" {{ block('widgetattributes') }} value=\"{{ value }}\">
{% endspaceless %}
{% endblock formwidgetsimple %}

&#123;&#35; Labels &#35;&#125;

{% block formlabel %}
{% spaceless %}
    {% if not compound %}
        {% set attr = attr|merge({'for': id}) %}
    {% endif %}
    {% if required %}
        {% set attr = attr|merge({'class': attr.class|default('') ~ ' required'}) %}
    {% endif %}
    {% if inlistcheckbox is defined and inlistcheckbox and widget is defined %}
        <label{% for attrname,attrvalue in attr %} {{attrname}}=\"{{attrvalue}}\"{% endfor %}>
            {{ widget|raw }}
            <span>
                {{ label|trans({}, translationdomain) }}
            </span>
        </label>
    {% else %}
        <label{% for attrname,attrvalue in attr %} {{attrname}}=\"{{attrvalue}}\"{% endfor %}>{{ label|trans({}, translationdomain) }}</label>
    {% endif %}
{% endspaceless %}
{% endblock %}

&#123;&#35; Rows &#35;&#125;

{% block formrow %}
{% spaceless %}
    <div class=\"form-group {{ (0 < formerrors(form)|length)? 'has-error':'' }} \">
        {{ formlabel(form, label|default(null)) }}
        <div class=\"col-sm-10 col-md-5\">
            {{ formwidget(form) }}
            {{ formerrors(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock formrow %}

&#123;&#35; Misc &#35;&#125;

{% block formerrors %}
{% spaceless %}
    {% if errors|length > 0 %}
        {% if not form.parent  or 'repeated' in form.vars['blockprefixes'] %}
            <div class=\"form-group has-error\">
        {% endif %}
        <span class=\"help-block\">
            {% for error in errors %}
                {% if loop.first %}
                    {{ error.messageTemplate|trans(error.messageParameters, 'validators') }}
                {% endif %}
            {% endfor %}
        </span>
        {% if not form.parent  or 'repeated' in form.vars['blockprefixes'] %}
            </div>
        {% endif %}
    {% endif %}
{% endspaceless %}
{% endblock formerrors %}
#}
", "SonataAdminBundle:Form:silexformdivlayout.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Form/silexformdivlayout.html.twig");
    }
}
