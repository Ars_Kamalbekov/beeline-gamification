<?php

/* SonataCoreBundle:Form:colorpicker.html.twig */
class TwigTemplate628ccd39f0d44c3fc061717d3441e79802152b1cda94a2125810b24df7dcd52e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonatatypecolorselectorwidget' => array($this, 'blocksonatatypecolorselectorwidget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal92eb6e360552e954d50c3d2dedc8c7f6c79af7fee51ec604034cdead22ddd450 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal92eb6e360552e954d50c3d2dedc8c7f6c79af7fee51ec604034cdead22ddd450->enter($internal92eb6e360552e954d50c3d2dedc8c7f6c79af7fee51ec604034cdead22ddd450prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:Form:colorpicker.html.twig"));

        $internalea93e08ee5c769802b07433211addcae01f024077449d1c2d89a2b9d0d8b0b56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalea93e08ee5c769802b07433211addcae01f024077449d1c2d89a2b9d0d8b0b56->enter($internalea93e08ee5c769802b07433211addcae01f024077449d1c2d89a2b9d0d8b0b56prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:Form:colorpicker.html.twig"));

        // line 11
        $this->displayBlock('sonatatypecolorselectorwidget', $context, $blocks);
        
        $internal92eb6e360552e954d50c3d2dedc8c7f6c79af7fee51ec604034cdead22ddd450->leave($internal92eb6e360552e954d50c3d2dedc8c7f6c79af7fee51ec604034cdead22ddd450prof);

        
        $internalea93e08ee5c769802b07433211addcae01f024077449d1c2d89a2b9d0d8b0b56->leave($internalea93e08ee5c769802b07433211addcae01f024077449d1c2d89a2b9d0d8b0b56prof);

    }

    public function blocksonatatypecolorselectorwidget($context, array $blocks = array())
    {
        $internalcee67c591baff53cd3c753532b6a8e616d979a191f2d336ddef1ca5021ef2aa5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcee67c591baff53cd3c753532b6a8e616d979a191f2d336ddef1ca5021ef2aa5->enter($internalcee67c591baff53cd3c753532b6a8e616d979a191f2d336ddef1ca5021ef2aa5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypecolorselectorwidget"));

        $internal0d02bd40861314b03d359afb5f2cf70d5b240bf6a8b9db83307299a7b72a480f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0d02bd40861314b03d359afb5f2cf70d5b240bf6a8b9db83307299a7b72a480f->enter($internal0d02bd40861314b03d359afb5f2cf70d5b240bf6a8b9db83307299a7b72a480fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypecolorselectorwidget"));

        // line 12
        echo "    ";
        $this->displayBlock("choicewidget", $context, $blocks);
        echo "
    ";
        // line 13
        obstart();
        // line 14
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#";
        // line 24
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 24, $this->getSourceContext()); })()), "html", null, true);
        echo "').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal0d02bd40861314b03d359afb5f2cf70d5b240bf6a8b9db83307299a7b72a480f->leave($internal0d02bd40861314b03d359afb5f2cf70d5b240bf6a8b9db83307299a7b72a480fprof);

        
        $internalcee67c591baff53cd3c753532b6a8e616d979a191f2d336ddef1ca5021ef2aa5->leave($internalcee67c591baff53cd3c753532b6a8e616d979a191f2d336ddef1ca5021ef2aa5prof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:Form:colorpicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  63 => 24,  51 => 14,  49 => 13,  44 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonatatypecolorselectorwidget %}
    {{ block('choicewidget') }}
    {% spaceless %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                var select2FormatColorSelect = function format(state) {
                    if (!state.id || state.disabled) {
                        return state.text;
                    }

                    return ' <i class=\"fa fa-square\" style=\"color: '+ state.id +'\"></i> ' + state.text;
                };

                \$('#{{ id }}').select2({
                    formatResult:    select2FormatColorSelect,
                    formatSelection: select2FormatColorSelect,
                    width:           '100%',
                    escapeMarkup:    function(m) { return m; }
                });
            });
        </script>
    {% endspaceless %}
{% endblock sonatatypecolorselectorwidget %}
", "SonataCoreBundle:Form:colorpicker.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/core-bundle/Resources/views/Form/colorpicker.html.twig");
    }
}
