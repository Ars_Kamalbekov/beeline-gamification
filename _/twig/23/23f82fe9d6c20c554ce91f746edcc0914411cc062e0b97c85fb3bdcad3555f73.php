<?php

/* @Framework/Form/buttonattributes.html.php */
class TwigTemplate53ad405b0c9a8cfe242be4314805aa56697ae967f1b2c47d8666372b13842334 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal796aa6b6ea063140196b37c941980cf299a2ef966ca50f173a9efb082079d4c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal796aa6b6ea063140196b37c941980cf299a2ef966ca50f173a9efb082079d4c4->enter($internal796aa6b6ea063140196b37c941980cf299a2ef966ca50f173a9efb082079d4c4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonattributes.html.php"));

        $internald123190a27b7cc023a1193211d0e248b8bdc6151192625e89249a16678b504be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald123190a27b7cc023a1193211d0e248b8bdc6151192625e89249a16678b504be->enter($internald123190a27b7cc023a1193211d0e248b8bdc6151192625e89249a16678b504beprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonattributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$fullname) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $internal796aa6b6ea063140196b37c941980cf299a2ef966ca50f173a9efb082079d4c4->leave($internal796aa6b6ea063140196b37c941980cf299a2ef966ca50f173a9efb082079d4c4prof);

        
        $internald123190a27b7cc023a1193211d0e248b8bdc6151192625e89249a16678b504be->leave($internald123190a27b7cc023a1193211d0e248b8bdc6151192625e89249a16678b504beprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/buttonattributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$fullname) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/buttonattributes.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/buttonattributes.html.php");
    }
}
