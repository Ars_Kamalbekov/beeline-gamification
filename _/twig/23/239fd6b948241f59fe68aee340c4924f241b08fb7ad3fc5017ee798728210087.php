<?php

/* @Framework/Form/formwidgetcompound.html.php */
class TwigTemplate556e23d0a20ce4933757cd0d0ba0655441952e2fc9f700e2dc4c336aec4f623e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal22e52b2d110b8369b35e8f4f679994fd3f52ecc40fa6f1c38a7be589771b24aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal22e52b2d110b8369b35e8f4f679994fd3f52ecc40fa6f1c38a7be589771b24aa->enter($internal22e52b2d110b8369b35e8f4f679994fd3f52ecc40fa6f1c38a7be589771b24aaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidgetcompound.html.php"));

        $internal708febe8ef4d99020a28f3157fb639e8f7cd73ae780250db63502f119c413a5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal708febe8ef4d99020a28f3157fb639e8f7cd73ae780250db63502f119c413a5e->enter($internal708febe8ef4d99020a28f3157fb639e8f7cd73ae780250db63502f119c413a5eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidgetcompound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'formrows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $internal22e52b2d110b8369b35e8f4f679994fd3f52ecc40fa6f1c38a7be589771b24aa->leave($internal22e52b2d110b8369b35e8f4f679994fd3f52ecc40fa6f1c38a7be589771b24aaprof);

        
        $internal708febe8ef4d99020a28f3157fb639e8f7cd73ae780250db63502f119c413a5e->leave($internal708febe8ef4d99020a28f3157fb639e8f7cd73ae780250db63502f119c413a5eprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formwidgetcompound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'formrows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/formwidgetcompound.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formwidgetcompound.html.php");
    }
}
