<?php

/* @Framework/Form/choicewidgetcollapsed.html.php */
class TwigTemplated5b35fb1b3915f536e8ce3ab9062a69186f3383d1e341c5cc0eb6befaccb846b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal171c16398405ee0d18412ee0620163fdb37cf5d72c09817f0e9146d6dbcc3f6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal171c16398405ee0d18412ee0620163fdb37cf5d72c09817f0e9146d6dbcc3f6c->enter($internal171c16398405ee0d18412ee0620163fdb37cf5d72c09817f0e9146d6dbcc3f6cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetcollapsed.html.php"));

        $internal701a44a586314fcf99765313d751c4dfca97a2b27d7c4c8c6ac2f9dd021885cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal701a44a586314fcf99765313d751c4dfca97a2b27d7c4c8c6ac2f9dd021885cf->enter($internal701a44a586314fcf99765313d751c4dfca97a2b27d7c4c8c6ac2f9dd021885cfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetcollapsed.html.php"));

        // line 1
        echo "<select
    <?php if (\$required && null === \$placeholder && \$placeholderinchoices === false && \$multiple === false && (!isset(\$attr['size']) || \$attr['size'] <= 1)):
        \$required = false;
    endif; ?>
    <?php echo \$view['form']->block(\$form, 'widgetattributes', array(
        'required' => \$required,
    )) ?>
    <?php if (\$multiple): ?> multiple=\"multiple\"<?php endif ?>
>
    <?php if (null !== \$placeholder): ?><option value=\"\"<?php if (\$required && empty(\$value) && '0' !== \$value): ?> selected=\"selected\"<?php endif?>><?php echo '' != \$placeholder ? \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$placeholder, array(), \$translationdomain) : \$placeholder) : '' ?></option><?php endif; ?>
    <?php if (count(\$preferredchoices) > 0): ?>
        <?php echo \$view['form']->block(\$form, 'choicewidgetoptions', array('choices' => \$preferredchoices)) ?>
        <?php if (count(\$choices) > 0 && null !== \$separator): ?>
            <option disabled=\"disabled\"><?php echo \$separator ?></option>
        <?php endif ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'choicewidgetoptions', array('choices' => \$choices)) ?>
</select>
";
        
        $internal171c16398405ee0d18412ee0620163fdb37cf5d72c09817f0e9146d6dbcc3f6c->leave($internal171c16398405ee0d18412ee0620163fdb37cf5d72c09817f0e9146d6dbcc3f6cprof);

        
        $internal701a44a586314fcf99765313d751c4dfca97a2b27d7c4c8c6ac2f9dd021885cf->leave($internal701a44a586314fcf99765313d751c4dfca97a2b27d7c4c8c6ac2f9dd021885cfprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choicewidgetcollapsed.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<select
    <?php if (\$required && null === \$placeholder && \$placeholderinchoices === false && \$multiple === false && (!isset(\$attr['size']) || \$attr['size'] <= 1)):
        \$required = false;
    endif; ?>
    <?php echo \$view['form']->block(\$form, 'widgetattributes', array(
        'required' => \$required,
    )) ?>
    <?php if (\$multiple): ?> multiple=\"multiple\"<?php endif ?>
>
    <?php if (null !== \$placeholder): ?><option value=\"\"<?php if (\$required && empty(\$value) && '0' !== \$value): ?> selected=\"selected\"<?php endif?>><?php echo '' != \$placeholder ? \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$placeholder, array(), \$translationdomain) : \$placeholder) : '' ?></option><?php endif; ?>
    <?php if (count(\$preferredchoices) > 0): ?>
        <?php echo \$view['form']->block(\$form, 'choicewidgetoptions', array('choices' => \$preferredchoices)) ?>
        <?php if (count(\$choices) > 0 && null !== \$separator): ?>
            <option disabled=\"disabled\"><?php echo \$separator ?></option>
        <?php endif ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'choicewidgetoptions', array('choices' => \$choices)) ?>
</select>
", "@Framework/Form/choicewidgetcollapsed.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choicewidgetcollapsed.html.php");
    }
}
