<?php

/* TwigBundle:Exception:trace.html.twig */
class TwigTemplate38e05bdf014824207c299fcfd5d8d0cbccc6273824b2f18071462abd57c4f01c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale4f25e5e43146107dc531523bd48b9cb0bf318531f83b5a2cebccb4c2b556ee1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale4f25e5e43146107dc531523bd48b9cb0bf318531f83b5a2cebccb4c2b556ee1->enter($internale4f25e5e43146107dc531523bd48b9cb0bf318531f83b5a2cebccb4c2b556ee1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:trace.html.twig"));

        $internalc6173cafb4af46932610588b0125de98a884aff20b5fc53e09d941afed72e351 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc6173cafb4af46932610588b0125de98a884aff20b5fc53e09d941afed72e351->enter($internalc6173cafb4af46932610588b0125de98a884aff20b5fc53e09d941afed72e351prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:trace.html.twig"));

        // line 1
        echo "<div class=\"trace-line-header ";
        echo ((((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array()), false)) : (false))) ? ("sf-toggle") : (""));
        echo "\" data-toggle-selector=\"#trace-html-";
        echo twigescapefilter($this->env, (isset($context["prefix"]) || arraykeyexists("prefix", $context) ? $context["prefix"] : (function () { throw new TwigErrorRuntime('Variable "prefix" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, (isset($context["i"]) || arraykeyexists("i", $context) ? $context["i"] : (function () { throw new TwigErrorRuntime('Variable "i" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "\" data-toggle-initial=\"";
        echo (((isset($context["displaycodesnippet"]) || arraykeyexists("displaycodesnippet", $context) ? $context["displaycodesnippet"] : (function () { throw new TwigErrorRuntime('Variable "displaycodesnippet" does not exist.', 1, $this->getSourceContext()); })())) ? ("display") : (""));
        echo "\">
    ";
        // line 2
        if (((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array()), false)) : (false))) {
            // line 3
            echo "        <span class=\"icon icon-close\">";
            echo twiginclude($this->env, $context, "@Twig/images/icon-minus-square.svg");
            echo "</span>
        <span class=\"icon icon-open\">";
            // line 4
            echo twiginclude($this->env, $context, "@Twig/images/icon-plus-square.svg");
            echo "</span>
    ";
        }
        // line 6
        echo "
    ";
        // line 7
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 7, $this->getSourceContext()); })()), "function", array())) {
            // line 8
            echo "        <span class=\"trace-class\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->abbrClass(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 8, $this->getSourceContext()); })()), "class", array()));
            echo "</span>";
            if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 8, $this->getSourceContext()); })()), "type", array()))) {
                echo "<span class=\"trace-type\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 8, $this->getSourceContext()); })()), "type", array()), "html", null, true);
                echo "</span>";
            }
            echo "<span class=\"trace-method\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 8, $this->getSourceContext()); })()), "function", array()), "html", null, true);
            echo "</span><span class=\"trace-arguments\">(";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatArgs(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 8, $this->getSourceContext()); })()), "args", array()));
            echo ")</span>
    ";
        }
        // line 10
        echo "
    ";
        // line 11
        if (((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array()), false)) : (false))) {
            // line 12
            echo "        ";
            $context["linenumber"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array()), 1)) : (1));
            // line 13
            echo "        ";
            $context["filelink"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->getFileLink(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 13, $this->getSourceContext()); })()), "file", array()), (isset($context["linenumber"]) || arraykeyexists("linenumber", $context) ? $context["linenumber"] : (function () { throw new TwigErrorRuntime('Variable "linenumber" does not exist.', 13, $this->getSourceContext()); })()));
            // line 14
            echo "        ";
            $context["filepath"] = twigreplacefilter(striptags($this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatFile(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 14, $this->getSourceContext()); })()), "file", array()), (isset($context["linenumber"]) || arraykeyexists("linenumber", $context) ? $context["linenumber"] : (function () { throw new TwigErrorRuntime('Variable "linenumber" does not exist.', 14, $this->getSourceContext()); })()))), array((" at line " . (isset($context["linenumber"]) || arraykeyexists("linenumber", $context) ? $context["linenumber"] : (function () { throw new TwigErrorRuntime('Variable "linenumber" does not exist.', 14, $this->getSourceContext()); })())) => ""));
            // line 15
            echo "        ";
            $context["filepathparts"] = twigsplitfilter($this->env, (isset($context["filepath"]) || arraykeyexists("filepath", $context) ? $context["filepath"] : (function () { throw new TwigErrorRuntime('Variable "filepath" does not exist.', 15, $this->getSourceContext()); })()), twigconstant("DIRECTORYSEPARATOR"));
            // line 16
            echo "
        <span class=\"block trace-file-path\">
            in
            <a href=\"";
            // line 19
            echo twigescapefilter($this->env, (isset($context["filelink"]) || arraykeyexists("filelink", $context) ? $context["filelink"] : (function () { throw new TwigErrorRuntime('Variable "filelink" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twigjoinfilter(twigslice($this->env, (isset($context["filepathparts"]) || arraykeyexists("filepathparts", $context) ? $context["filepathparts"] : (function () { throw new TwigErrorRuntime('Variable "filepathparts" does not exist.', 19, $this->getSourceContext()); })()), 0,  -1), twigconstant("DIRECTORYSEPARATOR")), "html", null, true);
            echo twigescapefilter($this->env, twigconstant("DIRECTORYSEPARATOR"), "html", null, true);
            echo "<strong>";
            echo twigescapefilter($this->env, twiglast($this->env, (isset($context["filepathparts"]) || arraykeyexists("filepathparts", $context) ? $context["filepathparts"] : (function () { throw new TwigErrorRuntime('Variable "filepathparts" does not exist.', 19, $this->getSourceContext()); })())), "html", null, true);
            echo "</strong></a>
            (line ";
            // line 20
            echo twigescapefilter($this->env, (isset($context["linenumber"]) || arraykeyexists("linenumber", $context) ? $context["linenumber"] : (function () { throw new TwigErrorRuntime('Variable "linenumber" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
            echo ")
        </span>
    ";
        }
        // line 23
        echo "</div>
";
        // line 24
        if (((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array()), false)) : (false))) {
            // line 25
            echo "    <div id=\"trace-html-";
            echo twigescapefilter($this->env, (((isset($context["prefix"]) || arraykeyexists("prefix", $context) ? $context["prefix"] : (function () { throw new TwigErrorRuntime('Variable "prefix" does not exist.', 25, $this->getSourceContext()); })()) . "-") . (isset($context["i"]) || arraykeyexists("i", $context) ? $context["i"] : (function () { throw new TwigErrorRuntime('Variable "i" does not exist.', 25, $this->getSourceContext()); })())), "html", null, true);
            echo "\" class=\"trace-code sf-toggle-content\">
        ";
            // line 26
            echo twigreplacefilter($this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 26, $this->getSourceContext()); })()), "file", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 26, $this->getSourceContext()); })()), "line", array()), 5), array("#DD0000" => "#183691", "#007700" => "#a71d5d", "#0000BB" => "#222222", "#FF8000" => "#969896"));
            // line 31
            echo "
    </div>
";
        }
        
        $internale4f25e5e43146107dc531523bd48b9cb0bf318531f83b5a2cebccb4c2b556ee1->leave($internale4f25e5e43146107dc531523bd48b9cb0bf318531f83b5a2cebccb4c2b556ee1prof);

        
        $internalc6173cafb4af46932610588b0125de98a884aff20b5fc53e09d941afed72e351->leave($internalc6173cafb4af46932610588b0125de98a884aff20b5fc53e09d941afed72e351prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:trace.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 31,  116 => 26,  111 => 25,  109 => 24,  106 => 23,  100 => 20,  91 => 19,  86 => 16,  83 => 15,  80 => 14,  77 => 13,  74 => 12,  72 => 11,  69 => 10,  53 => 8,  51 => 7,  48 => 6,  43 => 4,  38 => 3,  36 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div class=\"trace-line-header {{ trace.file|default(false) ? 'sf-toggle' }}\" data-toggle-selector=\"#trace-html-{{ prefix }}-{{ i }}\" data-toggle-initial=\"{{ displaycodesnippet ? 'display' }}\">
    {% if trace.file|default(false) %}
        <span class=\"icon icon-close\">{{ include('@Twig/images/icon-minus-square.svg') }}</span>
        <span class=\"icon icon-open\">{{ include('@Twig/images/icon-plus-square.svg') }}</span>
    {% endif %}

    {% if trace.function %}
        <span class=\"trace-class\">{{ trace.class|abbrclass }}</span>{% if trace.type is not empty %}<span class=\"trace-type\">{{ trace.type }}</span>{% endif %}<span class=\"trace-method\">{{ trace.function }}</span><span class=\"trace-arguments\">({{ trace.args|formatargs }})</span>
    {% endif %}

    {% if trace.file|default(false) %}
        {% set linenumber = trace.line|default(1) %}
        {% set filelink = trace.file|filelink(linenumber) %}
        {% set filepath = trace.file|formatfile(linenumber)|striptags|replace({ (' at line ' ~ linenumber): '' }) %}
        {% set filepathparts = filepath|split(constant('DIRECTORYSEPARATOR')) %}

        <span class=\"block trace-file-path\">
            in
            <a href=\"{{ filelink }}\">{{ filepathparts[:-1]|join(constant('DIRECTORYSEPARATOR')) }}{{ constant('DIRECTORYSEPARATOR') }}<strong>{{ filepathparts|last }}</strong></a>
            (line {{ linenumber }})
        </span>
    {% endif %}
</div>
{% if trace.file|default(false) %}
    <div id=\"trace-html-{{ prefix ~ '-' ~ i }}\" class=\"trace-code sf-toggle-content\">
        {{ trace.file|fileexcerpt(trace.line, 5)|replace({
            '#DD0000': '#183691',
            '#007700': '#a71d5d',
            '#0000BB': '#222222',
            '#FF8000': '#969896'
        })|raw }}
    </div>
{% endif %}
", "TwigBundle:Exception:trace.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/trace.html.twig");
    }
}
