<?php

/* SonataAdminBundle:CRUD:listcurrency.html.twig */
class TwigTemplatead2daeaea43572820e562c09add5f78de3168f6daecdd92052879a733802aa6d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listcurrency.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3288646f8bd088bc71a801225f9239aeed79c472278ccd388c4f1cd0f0cce93d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3288646f8bd088bc71a801225f9239aeed79c472278ccd388c4f1cd0f0cce93d->enter($internal3288646f8bd088bc71a801225f9239aeed79c472278ccd388c4f1cd0f0cce93dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listcurrency.html.twig"));

        $internalc4337f273c11d3d8490ee8aa9ec4adcea492981756c985f1174f5a30d6c09c39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc4337f273c11d3d8490ee8aa9ec4adcea492981756c985f1174f5a30d6c09c39->enter($internalc4337f273c11d3d8490ee8aa9ec4adcea492981756c985f1174f5a30d6c09c39prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listcurrency.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal3288646f8bd088bc71a801225f9239aeed79c472278ccd388c4f1cd0f0cce93d->leave($internal3288646f8bd088bc71a801225f9239aeed79c472278ccd388c4f1cd0f0cce93dprof);

        
        $internalc4337f273c11d3d8490ee8aa9ec4adcea492981756c985f1174f5a30d6c09c39->leave($internalc4337f273c11d3d8490ee8aa9ec4adcea492981756c985f1174f5a30d6c09c39prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal096cc20f50e09add67918a90be6a23f4b1f8286e7bf2adae1b68ac7413fd8941 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal096cc20f50e09add67918a90be6a23f4b1f8286e7bf2adae1b68ac7413fd8941->enter($internal096cc20f50e09add67918a90be6a23f4b1f8286e7bf2adae1b68ac7413fd8941prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internala7f9c6f9d5f2c0562753c16aa4d3ceafcbcaaac0ee304eb423e8ffdefa85325e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala7f9c6f9d5f2c0562753c16aa4d3ceafcbcaaac0ee304eb423e8ffdefa85325e->enter($internala7f9c6f9d5f2c0562753c16aa4d3ceafcbcaaac0ee304eb423e8ffdefa85325eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "        ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $internala7f9c6f9d5f2c0562753c16aa4d3ceafcbcaaac0ee304eb423e8ffdefa85325e->leave($internala7f9c6f9d5f2c0562753c16aa4d3ceafcbcaaac0ee304eb423e8ffdefa85325eprof);

        
        $internal096cc20f50e09add67918a90be6a23f4b1f8286e7bf2adae1b68ac7413fd8941->leave($internal096cc20f50e09add67918a90be6a23f4b1f8286e7bf2adae1b68ac7413fd8941prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listcurrency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% if value is not null %}
        {{ fielddescription.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:listcurrency.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listcurrency.html.twig");
    }
}
