<?php

/* SonataAdminBundle:CRUD:action.html.twig */
class TwigTemplate3c9599740c0eaf0d0524ce32163922e7dca14ddaad69f8ed91826e5a971da7b3 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'tabmenu' => array($this, 'blocktabmenu'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf2b1a9a6820342d68a32a01b05c6de61aac5ff4735f83acaa17f4a738752d782 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf2b1a9a6820342d68a32a01b05c6de61aac5ff4735f83acaa17f4a738752d782->enter($internalf2b1a9a6820342d68a32a01b05c6de61aac5ff4735f83acaa17f4a738752d782prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $internalf0bc975f02e210943b18b28170062e359a4a73f6b1b049f212dc07ef0add32a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf0bc975f02e210943b18b28170062e359a4a73f6b1b049f212dc07ef0add32a5->enter($internalf0bc975f02e210943b18b28170062e359a4a73f6b1b049f212dc07ef0add32a5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:action.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf2b1a9a6820342d68a32a01b05c6de61aac5ff4735f83acaa17f4a738752d782->leave($internalf2b1a9a6820342d68a32a01b05c6de61aac5ff4735f83acaa17f4a738752d782prof);

        
        $internalf0bc975f02e210943b18b28170062e359a4a73f6b1b049f212dc07ef0add32a5->leave($internalf0bc975f02e210943b18b28170062e359a4a73f6b1b049f212dc07ef0add32a5prof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internal12116f7b5e95c44b57c30dacaa4e7e6d517f4e7016f76c10e58dffe4f72f782a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal12116f7b5e95c44b57c30dacaa4e7e6d517f4e7016f76c10e58dffe4f72f782a->enter($internal12116f7b5e95c44b57c30dacaa4e7e6d517f4e7016f76c10e58dffe4f72f782aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internala0059ec7b6777a4cd5fd2caa8f22107f3fa9c1b960e1b51be961b2a9b78224a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala0059ec7b6777a4cd5fd2caa8f22107f3fa9c1b960e1b51be961b2a9b78224a5->enter($internala0059ec7b6777a4cd5fd2caa8f22107f3fa9c1b960e1b51be961b2a9b78224a5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:action.html.twig", 15)->display($context);
        
        $internala0059ec7b6777a4cd5fd2caa8f22107f3fa9c1b960e1b51be961b2a9b78224a5->leave($internala0059ec7b6777a4cd5fd2caa8f22107f3fa9c1b960e1b51be961b2a9b78224a5prof);

        
        $internal12116f7b5e95c44b57c30dacaa4e7e6d517f4e7016f76c10e58dffe4f72f782a->leave($internal12116f7b5e95c44b57c30dacaa4e7e6d517f4e7016f76c10e58dffe4f72f782aprof);

    }

    // line 18
    public function blocktabmenu($context, array $blocks = array())
    {
        $internala05ff5da591188e1a8432d638b826a759f2672ba58154552dc7d552e15d3bc53 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala05ff5da591188e1a8432d638b826a759f2672ba58154552dc7d552e15d3bc53->enter($internala05ff5da591188e1a8432d638b826a759f2672ba58154552dc7d552e15d3bc53prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal39f935f246445312a630b7ad6d064b576370cf337e56b8c0f246cb9e92e20081 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal39f935f246445312a630b7ad6d064b576370cf337e56b8c0f246cb9e92e20081->enter($internal39f935f246445312a630b7ad6d064b576370cf337e56b8c0f246cb9e92e20081prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        // line 19
        echo "    ";
        if (arraykeyexists("action", $context)) {
            // line 20
            echo "        ";
            echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 20, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 20, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
            echo "
    ";
        }
        
        $internal39f935f246445312a630b7ad6d064b576370cf337e56b8c0f246cb9e92e20081->leave($internal39f935f246445312a630b7ad6d064b576370cf337e56b8c0f246cb9e92e20081prof);

        
        $internala05ff5da591188e1a8432d638b826a759f2672ba58154552dc7d552e15d3bc53->leave($internala05ff5da591188e1a8432d638b826a759f2672ba58154552dc7d552e15d3bc53prof);

    }

    // line 24
    public function blockcontent($context, array $blocks = array())
    {
        $internal2507a71cfa2a1bea8eea943beb32922502d614c72aaed9f5a2cf5e061e9ebf4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2507a71cfa2a1bea8eea943beb32922502d614c72aaed9f5a2cf5e061e9ebf4c->enter($internal2507a71cfa2a1bea8eea943beb32922502d614c72aaed9f5a2cf5e061e9ebf4cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internal31c60f996e509e56d21a52042aceb509cdb49e086677d2f850829793a34f01ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal31c60f996e509e56d21a52042aceb509cdb49e086677d2f850829793a34f01ab->enter($internal31c60f996e509e56d21a52042aceb509cdb49e086677d2f850829793a34f01abprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 25
        echo "
    Redefine the content block in your action template

";
        
        $internal31c60f996e509e56d21a52042aceb509cdb49e086677d2f850829793a34f01ab->leave($internal31c60f996e509e56d21a52042aceb509cdb49e086677d2f850829793a34f01abprof);

        
        $internal2507a71cfa2a1bea8eea943beb32922502d614c72aaed9f5a2cf5e061e9ebf4c->leave($internal2507a71cfa2a1bea8eea943beb32922502d614c72aaed9f5a2cf5e061e9ebf4cprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  86 => 24,  72 => 20,  69 => 19,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}
    {% if action is defined %}
        {{ knpmenurender(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')}, 'twig') }}
    {% endif %}
{% endblock %}

{% block content %}

    Redefine the content block in your action template

{% endblock %}
", "SonataAdminBundle:CRUD:action.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/action.html.twig");
    }
}
