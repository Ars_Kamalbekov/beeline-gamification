<?php

/* SonataAdminBundle:CRUD:listpercent.html.twig */
class TwigTemplate53971db68b608fd115fdee664134c490e1fac12f5a6b1b411b37e11a85a0bb7c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listpercent.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal54f5966cb0aca0ebcc8559365e7747b374814768a6e1f248046c65fe2870aafb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal54f5966cb0aca0ebcc8559365e7747b374814768a6e1f248046c65fe2870aafb->enter($internal54f5966cb0aca0ebcc8559365e7747b374814768a6e1f248046c65fe2870aafbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listpercent.html.twig"));

        $internalbce02d64309839b7c09e7fcfb9f289014557ee73042abbe8bb76c75eabbfa345 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbce02d64309839b7c09e7fcfb9f289014557ee73042abbe8bb76c75eabbfa345->enter($internalbce02d64309839b7c09e7fcfb9f289014557ee73042abbe8bb76c75eabbfa345prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listpercent.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal54f5966cb0aca0ebcc8559365e7747b374814768a6e1f248046c65fe2870aafb->leave($internal54f5966cb0aca0ebcc8559365e7747b374814768a6e1f248046c65fe2870aafbprof);

        
        $internalbce02d64309839b7c09e7fcfb9f289014557ee73042abbe8bb76c75eabbfa345->leave($internalbce02d64309839b7c09e7fcfb9f289014557ee73042abbe8bb76c75eabbfa345prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal214789bf29ac7ceb67f72586339cabdf493833383db6edf1125184ff73658728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal214789bf29ac7ceb67f72586339cabdf493833383db6edf1125184ff73658728->enter($internal214789bf29ac7ceb67f72586339cabdf493833383db6edf1125184ff73658728prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal85d1ae700a35123c750744011bcf24ab166953003629d98cc9eada9144c20c4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal85d1ae700a35123c750744011bcf24ab166953003629d98cc9eada9144c20c4b->enter($internal85d1ae700a35123c750744011bcf24ab166953003629d98cc9eada9144c20c4bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["value"] = ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()) * 100);
        // line 16
        echo "    ";
        echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo " %
";
        
        $internal85d1ae700a35123c750744011bcf24ab166953003629d98cc9eada9144c20c4b->leave($internal85d1ae700a35123c750744011bcf24ab166953003629d98cc9eada9144c20c4bprof);

        
        $internal214789bf29ac7ceb67f72586339cabdf493833383db6edf1125184ff73658728->leave($internal214789bf29ac7ceb67f72586339cabdf493833383db6edf1125184ff73658728prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listpercent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set value = value * 100 %}
    {{ value }} %
{% endblock %}
", "SonataAdminBundle:CRUD:listpercent.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listpercent.html.twig");
    }
}
