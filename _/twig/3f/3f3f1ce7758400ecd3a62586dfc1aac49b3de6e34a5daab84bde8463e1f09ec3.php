<?php

/* @Framework/Form/resetwidget.html.php */
class TwigTemplate7f95446ce860e4019fbea2c1db22d4c85d0c840627a6d1c85e2674bd5e042cff extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalce250c1298d0ba3b84571dfacb705dbe5044de45912d634df4c4f48c3f1d5a58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalce250c1298d0ba3b84571dfacb705dbe5044de45912d634df4c4f48c3f1d5a58->enter($internalce250c1298d0ba3b84571dfacb705dbe5044de45912d634df4c4f48c3f1d5a58prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/resetwidget.html.php"));

        $internala2b8d330256e28847ea7ca53091ddeda9e4312ee71fce1f872fe4f66157032e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala2b8d330256e28847ea7ca53091ddeda9e4312ee71fce1f872fe4f66157032e9->enter($internala2b8d330256e28847ea7ca53091ddeda9e4312ee71fce1f872fe4f66157032e9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/resetwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'buttonwidget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $internalce250c1298d0ba3b84571dfacb705dbe5044de45912d634df4c4f48c3f1d5a58->leave($internalce250c1298d0ba3b84571dfacb705dbe5044de45912d634df4c4f48c3f1d5a58prof);

        
        $internala2b8d330256e28847ea7ca53091ddeda9e4312ee71fce1f872fe4f66157032e9->leave($internala2b8d330256e28847ea7ca53091ddeda9e4312ee71fce1f872fe4f66157032e9prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/resetwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'buttonwidget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/resetwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/resetwidget.html.php");
    }
}
