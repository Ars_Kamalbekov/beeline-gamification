<?php

/* ::base.html.twig */
class TwigTemplatefe563e3afbf708e1cb33ee7fd227ad54628c2c1e7460358731135997a5c0c233 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'stylesheets' => array($this, 'blockstylesheets'),
            'body' => array($this, 'blockbody'),
            'javascripts' => array($this, 'blockjavascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaleb60336a73a332a94698256d46a11a751139b9af657327ee5d9a269f296a81f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleb60336a73a332a94698256d46a11a751139b9af657327ee5d9a269f296a81f6->enter($internaleb60336a73a332a94698256d46a11a751139b9af657327ee5d9a269f296a81f6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "::base.html.twig"));

        $internal962b6e793c7e4f8c601b8218387ca0fc5238186b8b0147d379fc41cd58167020 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal962b6e793c7e4f8c601b8218387ca0fc5238186b8b0147d379fc41cd58167020->enter($internal962b6e793c7e4f8c601b8218387ca0fc5238186b8b0147d379fc41cd58167020prof = new TwigProfilerProfile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap-3.3.7-dist/css/bootstrap.css"), "html", null, true);
        echo "\">
    </head>
    <body>
        ";
        // line 11
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "    </body>
</html>
";
        
        $internaleb60336a73a332a94698256d46a11a751139b9af657327ee5d9a269f296a81f6->leave($internaleb60336a73a332a94698256d46a11a751139b9af657327ee5d9a269f296a81f6prof);

        
        $internal962b6e793c7e4f8c601b8218387ca0fc5238186b8b0147d379fc41cd58167020->leave($internal962b6e793c7e4f8c601b8218387ca0fc5238186b8b0147d379fc41cd58167020prof);

    }

    // line 5
    public function blocktitle($context, array $blocks = array())
    {
        $internal4b41b076572f7fda6f03ec707c81a6517f884b5c8f4dbefc07c45e55bd71f6a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4b41b076572f7fda6f03ec707c81a6517f884b5c8f4dbefc07c45e55bd71f6a4->enter($internal4b41b076572f7fda6f03ec707c81a6517f884b5c8f4dbefc07c45e55bd71f6a4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal35733a250deea63d5357538ea1a5b7e13c97497bf851a9de684e34389a35d41e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal35733a250deea63d5357538ea1a5b7e13c97497bf851a9de684e34389a35d41e->enter($internal35733a250deea63d5357538ea1a5b7e13c97497bf851a9de684e34389a35d41eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $internal35733a250deea63d5357538ea1a5b7e13c97497bf851a9de684e34389a35d41e->leave($internal35733a250deea63d5357538ea1a5b7e13c97497bf851a9de684e34389a35d41eprof);

        
        $internal4b41b076572f7fda6f03ec707c81a6517f884b5c8f4dbefc07c45e55bd71f6a4->leave($internal4b41b076572f7fda6f03ec707c81a6517f884b5c8f4dbefc07c45e55bd71f6a4prof);

    }

    // line 6
    public function blockstylesheets($context, array $blocks = array())
    {
        $internal5bd240e0a75ebbf8ced144d1d6eff8f4a3ec60c1dde47ee178504a29390fe35b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5bd240e0a75ebbf8ced144d1d6eff8f4a3ec60c1dde47ee178504a29390fe35b->enter($internal5bd240e0a75ebbf8ced144d1d6eff8f4a3ec60c1dde47ee178504a29390fe35bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        $internal81c80c8f8e0fab391e8db838ecf0291a52790798199a69fc351007f39400b7f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81c80c8f8e0fab391e8db838ecf0291a52790798199a69fc351007f39400b7f5->enter($internal81c80c8f8e0fab391e8db838ecf0291a52790798199a69fc351007f39400b7f5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        
        $internal81c80c8f8e0fab391e8db838ecf0291a52790798199a69fc351007f39400b7f5->leave($internal81c80c8f8e0fab391e8db838ecf0291a52790798199a69fc351007f39400b7f5prof);

        
        $internal5bd240e0a75ebbf8ced144d1d6eff8f4a3ec60c1dde47ee178504a29390fe35b->leave($internal5bd240e0a75ebbf8ced144d1d6eff8f4a3ec60c1dde47ee178504a29390fe35bprof);

    }

    // line 11
    public function blockbody($context, array $blocks = array())
    {
        $internal0779bc1577a687969acfc1284fee52f9ac5632c1b465006af2650aee72d83e1e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0779bc1577a687969acfc1284fee52f9ac5632c1b465006af2650aee72d83e1e->enter($internal0779bc1577a687969acfc1284fee52f9ac5632c1b465006af2650aee72d83e1eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internal72c701988855d94991b0b77647b651c8e4a067d63f0ec37bc0c9154d7e86e5e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal72c701988855d94991b0b77647b651c8e4a067d63f0ec37bc0c9154d7e86e5e2->enter($internal72c701988855d94991b0b77647b651c8e4a067d63f0ec37bc0c9154d7e86e5e2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "        ";
        
        $internal72c701988855d94991b0b77647b651c8e4a067d63f0ec37bc0c9154d7e86e5e2->leave($internal72c701988855d94991b0b77647b651c8e4a067d63f0ec37bc0c9154d7e86e5e2prof);

        
        $internal0779bc1577a687969acfc1284fee52f9ac5632c1b465006af2650aee72d83e1e->leave($internal0779bc1577a687969acfc1284fee52f9ac5632c1b465006af2650aee72d83e1eprof);

    }

    // line 13
    public function blockjavascripts($context, array $blocks = array())
    {
        $internal01c24457942037ab07f7193e428f7ba939abc63818682085b526854c6dbbf0d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal01c24457942037ab07f7193e428f7ba939abc63818682085b526854c6dbbf0d5->enter($internal01c24457942037ab07f7193e428f7ba939abc63818682085b526854c6dbbf0d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "javascripts"));

        $internalf0b0a63e4ba38f218715c2d40d54cdeab39d72851dd14af1abb105983f365499 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf0b0a63e4ba38f218715c2d40d54cdeab39d72851dd14af1abb105983f365499->enter($internalf0b0a63e4ba38f218715c2d40d54cdeab39d72851dd14af1abb105983f365499prof = new TwigProfilerProfile($this->getTemplateName(), "block", "javascripts"));

        
        $internalf0b0a63e4ba38f218715c2d40d54cdeab39d72851dd14af1abb105983f365499->leave($internalf0b0a63e4ba38f218715c2d40d54cdeab39d72851dd14af1abb105983f365499prof);

        
        $internal01c24457942037ab07f7193e428f7ba939abc63818682085b526854c6dbbf0d5->leave($internal01c24457942037ab07f7193e428f7ba939abc63818682085b526854c6dbbf0d5prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 13,  113 => 12,  104 => 11,  87 => 6,  69 => 5,  57 => 14,  54 => 13,  52 => 11,  46 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('bootstrap-3.3.7-dist/css/bootstrap.css') }}\">
    </head>
    <body>
        {% block body %}
        {% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/var/www/html/beeline-gamification/app/Resources/views/base.html.twig");
    }
}
