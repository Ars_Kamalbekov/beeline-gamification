<?php

/* WebProfilerBundle:Collector:time.html.twig */
class TwigTemplatef09e4d33910be665a63bfd3141385bd3f03b78d860db75cac1406f8ebc366b64 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:time.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
            'panelContent' => array($this, 'blockpanelContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcf9ea513e2331a0bfa128ceb6cfd922724011907e55560801b19788eefe90932 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcf9ea513e2331a0bfa128ceb6cfd922724011907e55560801b19788eefe90932->enter($internalcf9ea513e2331a0bfa128ceb6cfd922724011907e55560801b19788eefe90932prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:time.html.twig"));

        $internal0f7a62981b69be69b8053c6d3ea58d729b60d0a90dd030c3563e743053f3113a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0f7a62981b69be69b8053c6d3ea58d729b60d0a90dd030c3563e743053f3113a->enter($internal0f7a62981b69be69b8053c6d3ea58d729b60d0a90dd030c3563e743053f3113aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:time.html.twig"));

        // line 3
        $context["helper"] = $this;
        // line 5
        if ( !arraykeyexists("colors", $context)) {
            // line 6
            $context["colors"] = array("default" => "#999", "section" => "#444", "eventlistener" => "#00B8F5", "eventlistenerloading" => "#00B8F5", "template" => "#66CC00", "doctrine" => "#FF6633", "propel" => "#FF6633");
        }
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalcf9ea513e2331a0bfa128ceb6cfd922724011907e55560801b19788eefe90932->leave($internalcf9ea513e2331a0bfa128ceb6cfd922724011907e55560801b19788eefe90932prof);

        
        $internal0f7a62981b69be69b8053c6d3ea58d729b60d0a90dd030c3563e743053f3113a->leave($internal0f7a62981b69be69b8053c6d3ea58d729b60d0a90dd030c3563e743053f3113aprof);

    }

    // line 17
    public function blocktoolbar($context, array $blocks = array())
    {
        $internalc1b3fdcf81f30ea385b8a2bed25de4a2339446a599f2d3de7193d59c18053908 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc1b3fdcf81f30ea385b8a2bed25de4a2339446a599f2d3de7193d59c18053908->enter($internalc1b3fdcf81f30ea385b8a2bed25de4a2339446a599f2d3de7193d59c18053908prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internal0e6eedfcbbb07260ad181d1a8e081c345557d7b1cbf90d2b129bfb24e128154e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0e6eedfcbbb07260ad181d1a8e081c345557d7b1cbf90d2b129bfb24e128154e->enter($internal0e6eedfcbbb07260ad181d1a8e081c345557d7b1cbf90d2b129bfb24e128154eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 18
        echo "    ";
        $context["totaltime"] = ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "events", array()))) ? (sprintf("%.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "duration", array()))) : ("n/a"));
        // line 19
        echo "    ";
        $context["initializationtime"] = ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 19, $this->getSourceContext()); })()), "events", array()))) ? (sprintf("%.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 19, $this->getSourceContext()); })()), "inittime", array()))) : ("n/a"));
        // line 20
        echo "    ";
        $context["statuscolor"] = (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 20, $this->getSourceContext()); })()), "events", array())) && (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 20, $this->getSourceContext()); })()), "duration", array()) > 1000))) ? ("yellow") : (""));
        // line 21
        echo "
    ";
        // line 22
        obstart();
        // line 23
        echo "        ";
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/time.svg");
        echo "
        <span class=\"sf-toolbar-value\">";
        // line 24
        echo twigescapefilter($this->env, (isset($context["totaltime"]) || arraykeyexists("totaltime", $context) ? $context["totaltime"] : (function () { throw new TwigErrorRuntime('Variable "totaltime" does not exist.', 24, $this->getSourceContext()); })()), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-label\">ms</span>
    ";
        $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 27
        echo "
    ";
        // line 28
        obstart();
        // line 29
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Total time</b>
            <span>";
        // line 31
        echo twigescapefilter($this->env, (isset($context["totaltime"]) || arraykeyexists("totaltime", $context) ? $context["totaltime"] : (function () { throw new TwigErrorRuntime('Variable "totaltime" does not exist.', 31, $this->getSourceContext()); })()), "html", null, true);
        echo " ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Initialization time</b>
            <span>";
        // line 35
        echo twigescapefilter($this->env, (isset($context["initializationtime"]) || arraykeyexists("initializationtime", $context) ? $context["initializationtime"] : (function () { throw new TwigErrorRuntime('Variable "initializationtime" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
        echo " ms</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 38
        echo "
    ";
        // line 39
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 39, $this->getSourceContext()); })()), "status" => (isset($context["statuscolor"]) || arraykeyexists("statuscolor", $context) ? $context["statuscolor"] : (function () { throw new TwigErrorRuntime('Variable "statuscolor" does not exist.', 39, $this->getSourceContext()); })())));
        echo "
";
        
        $internal0e6eedfcbbb07260ad181d1a8e081c345557d7b1cbf90d2b129bfb24e128154e->leave($internal0e6eedfcbbb07260ad181d1a8e081c345557d7b1cbf90d2b129bfb24e128154eprof);

        
        $internalc1b3fdcf81f30ea385b8a2bed25de4a2339446a599f2d3de7193d59c18053908->leave($internalc1b3fdcf81f30ea385b8a2bed25de4a2339446a599f2d3de7193d59c18053908prof);

    }

    // line 42
    public function blockmenu($context, array $blocks = array())
    {
        $internalbf553a7597c59d681bee8a7ce8a4b13d0779a7e8e944ddd79e4dd58de93c5979 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbf553a7597c59d681bee8a7ce8a4b13d0779a7e8e944ddd79e4dd58de93c5979->enter($internalbf553a7597c59d681bee8a7ce8a4b13d0779a7e8e944ddd79e4dd58de93c5979prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internale82f4620310c3071ec3e9c38533889374ccac4a89b7f0bd2c6b395a350d189dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale82f4620310c3071ec3e9c38533889374ccac4a89b7f0bd2c6b395a350d189dd->enter($internale82f4620310c3071ec3e9c38533889374ccac4a89b7f0bd2c6b395a350d189ddprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 43
        echo "    <span class=\"label\">
        <span class=\"icon\">";
        // line 44
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/time.svg");
        echo "</span>
        <strong>Performance</strong>
    </span>
";
        
        $internale82f4620310c3071ec3e9c38533889374ccac4a89b7f0bd2c6b395a350d189dd->leave($internale82f4620310c3071ec3e9c38533889374ccac4a89b7f0bd2c6b395a350d189ddprof);

        
        $internalbf553a7597c59d681bee8a7ce8a4b13d0779a7e8e944ddd79e4dd58de93c5979->leave($internalbf553a7597c59d681bee8a7ce8a4b13d0779a7e8e944ddd79e4dd58de93c5979prof);

    }

    // line 49
    public function blockpanel($context, array $blocks = array())
    {
        $internal17b9e3bfe5b64558e890360a5ec2efcbdd8f99d1163673c15c7a216aeda59c76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal17b9e3bfe5b64558e890360a5ec2efcbdd8f99d1163673c15c7a216aeda59c76->enter($internal17b9e3bfe5b64558e890360a5ec2efcbdd8f99d1163673c15c7a216aeda59c76prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal019c338139f6b8e53593b855448f74c78b985358fa5ff1edc2b2341a353f1ece = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal019c338139f6b8e53593b855448f74c78b985358fa5ff1edc2b2341a353f1ece->enter($internal019c338139f6b8e53593b855448f74c78b985358fa5ff1edc2b2341a353f1eceprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 50
        echo "    <h2>Performance metrics</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 54
        echo twigescapefilter($this->env, sprintf("%.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 54, $this->getSourceContext()); })()), "duration", array())), "html", null, true);
        echo " <span class=\"unit\">ms</span></span>
            <span class=\"label\">Total execution time</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 59
        echo twigescapefilter($this->env, sprintf("%.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 59, $this->getSourceContext()); })()), "inittime", array())), "html", null, true);
        echo " <span class=\"unit\">ms</span></span>
            <span class=\"label\">Symfony initialization</span>
        </div>

        ";
        // line 63
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 63, $this->getSourceContext()); })()), "collectors", array()), "memory", array())) {
            // line 64
            echo "            <div class=\"metric\">
                <span class=\"value\">";
            // line 65
            echo twigescapefilter($this->env, sprintf("%.2f", ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 65, $this->getSourceContext()); })()), "collectors", array()), "memory", array()), "memory", array()) / 1024) / 1024)), "html", null, true);
            echo " <span class=\"unit\">MB</span></span>
                <span class=\"label\">Peak memory usage</span>
            </div>
        ";
        }
        // line 69
        echo "
        ";
        // line 70
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 70, $this->getSourceContext()); })()), "children", array())) > 0)) {
            // line 71
            echo "            <div class=\"metric-divider\"></div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 74
            echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 74, $this->getSourceContext()); })()), "children", array())), "html", null, true);
            echo "</span>
                <span class=\"label\">Sub-Request";
            // line 75
            echo (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 75, $this->getSourceContext()); })()), "children", array())) > 1)) ? ("s") : (""));
            echo "</span>
            </div>

            ";
            // line 78
            $context["subrequeststime"] = 0;
            // line 79
            echo "            ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 79, $this->getSourceContext()); })()), "children", array()));
            foreach ($context['seq'] as $context["key"] => $context["child"]) {
                // line 80
                echo "                ";
                $context["subrequeststime"] = ((isset($context["subrequeststime"]) || arraykeyexists("subrequeststime", $context) ? $context["subrequeststime"] : (function () { throw new TwigErrorRuntime('Variable "subrequeststime" does not exist.', 80, $this->getSourceContext()); })()) + twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "getcollector", array(0 => "time"), "method"), "events", array()), "section", array()), "duration", array()));
                // line 81
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 82
            echo "
            <div class=\"metric\">
                <span class=\"value\">";
            // line 84
            echo twigescapefilter($this->env, (isset($context["subrequeststime"]) || arraykeyexists("subrequeststime", $context) ? $context["subrequeststime"] : (function () { throw new TwigErrorRuntime('Variable "subrequeststime" does not exist.', 84, $this->getSourceContext()); })()), "html", null, true);
            echo " <span class=\"unit\">ms</span></span>
                <span class=\"label\">Sub-Request";
            // line 85
            echo (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 85, $this->getSourceContext()); })()), "children", array())) > 1)) ? ("s") : (""));
            echo " time</span>
            </div>
        ";
        }
        // line 88
        echo "    </div>

    <h2>Execution timeline</h2>

    ";
        // line 92
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 92, $this->getSourceContext()); })()), "events", array()))) {
            // line 93
            echo "        <div class=\"empty\">
            <p>No timing events have been recorded. Are you sure that debugging is enabled in the kernel?</p>
        </div>
    ";
        } else {
            // line 97
            echo "        ";
            $this->displayBlock("panelContent", $context, $blocks);
            echo "
    ";
        }
        
        $internal019c338139f6b8e53593b855448f74c78b985358fa5ff1edc2b2341a353f1ece->leave($internal019c338139f6b8e53593b855448f74c78b985358fa5ff1edc2b2341a353f1eceprof);

        
        $internal17b9e3bfe5b64558e890360a5ec2efcbdd8f99d1163673c15c7a216aeda59c76->leave($internal17b9e3bfe5b64558e890360a5ec2efcbdd8f99d1163673c15c7a216aeda59c76prof);

    }

    // line 101
    public function blockpanelContent($context, array $blocks = array())
    {
        $internal3a31afac8021c20ea8748e6dbedf003ca41d6662cecdaaa96792f80ddecb2f77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a31afac8021c20ea8748e6dbedf003ca41d6662cecdaaa96792f80ddecb2f77->enter($internal3a31afac8021c20ea8748e6dbedf003ca41d6662cecdaaa96792f80ddecb2f77prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panelContent"));

        $internald1c0971f9e21486c6fb67885683a37d20380d0adbd2424bd796f29cf4e4b2119 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald1c0971f9e21486c6fb67885683a37d20380d0adbd2424bd796f29cf4e4b2119->enter($internald1c0971f9e21486c6fb67885683a37d20380d0adbd2424bd796f29cf4e4b2119prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panelContent"));

        // line 102
        echo "    <form id=\"timeline-control\" action=\"\" method=\"get\">
        <input type=\"hidden\" name=\"panel\" value=\"time\">
        <label for=\"threshold\">Threshold</label>
        <input type=\"number\" size=\"3\" name=\"threshold\" id=\"threshold\" value=\"3\" min=\"0\"> ms
        <span class=\"help\">(timeline only displays events with a duration longer than this threshold)</span>
    </form>

    ";
        // line 109
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 109, $this->getSourceContext()); })()), "parent", array())) {
            // line 110
            echo "        <h3 class=\"dump-inline\">
            Sub-Request ";
            // line 111
            echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 111, $this->getSourceContext()); })()), "getcollector", array(0 => "request"), "method"), "requestattributes", array()), "get", array(0 => "controller"), "method")));
            echo "
            <small>
                ";
            // line 113
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 113, $this->getSourceContext()); })()), "events", array()), "section", array()), "duration", array()), "html", null, true);
            echo " ms
                <a class=\"newline\" href=\"";
            // line 114
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 114, $this->getSourceContext()); })()), "parent", array()), "token", array()), "panel" => "time")), "html", null, true);
            echo "\">Return to parent request</a>
            </small>
        </h3>
    ";
        } elseif ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(),         // line 117
(isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 117, $this->getSourceContext()); })()), "children", array())) > 0)) {
            // line 118
            echo "        <h3>
            Main Request <small>";
            // line 119
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 119, $this->getSourceContext()); })()), "events", array()), "section", array()), "duration", array()), "html", null, true);
            echo " ms</small>
        </h3>
    ";
        }
        // line 122
        echo "
    ";
        // line 123
        echo $context["helper"]->macrodisplaytimeline(("timeline" . (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 123, $this->getSourceContext()); })())), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 123, $this->getSourceContext()); })()), "events", array()), (isset($context["colors"]) || arraykeyexists("colors", $context) ? $context["colors"] : (function () { throw new TwigErrorRuntime('Variable "colors" does not exist.', 123, $this->getSourceContext()); })()));
        echo "

    ";
        // line 125
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 125, $this->getSourceContext()); })()), "children", array()))) {
            // line 126
            echo "        <p class=\"help\">Note: sections with a striped background correspond to sub-requests.</p>

        <h3>Sub-requests <small>(";
            // line 128
            echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 128, $this->getSourceContext()); })()), "children", array())), "html", null, true);
            echo ")</small></h3>

        ";
            // line 130
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 130, $this->getSourceContext()); })()), "children", array()));
            foreach ($context['seq'] as $context["key"] => $context["child"]) {
                // line 131
                echo "            ";
                $context["events"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "getcollector", array(0 => "time"), "method"), "events", array());
                // line 132
                echo "            <h4>
                <a href=\"";
                // line 133
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "token", array()), "panel" => "time")), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "getcollector", array(0 => "request"), "method"), "identifier", array()), "html", null, true);
                echo "</a>
                <small>";
                // line 134
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["events"]) || arraykeyexists("events", $context) ? $context["events"] : (function () { throw new TwigErrorRuntime('Variable "events" does not exist.', 134, $this->getSourceContext()); })()), "section", array()), "duration", array()), "html", null, true);
                echo " ms</small>
            </h4>

            ";
                // line 137
                echo $context["helper"]->macrodisplaytimeline(("timeline" . twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "token", array())), (isset($context["events"]) || arraykeyexists("events", $context) ? $context["events"] : (function () { throw new TwigErrorRuntime('Variable "events" does not exist.', 137, $this->getSourceContext()); })()), (isset($context["colors"]) || arraykeyexists("colors", $context) ? $context["colors"] : (function () { throw new TwigErrorRuntime('Variable "colors" does not exist.', 137, $this->getSourceContext()); })()));
                echo "
        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 139
            echo "    ";
        }
        // line 140
        echo "
    <script>";
        // line 141
        echo "//<![CDATA[
        /**
         * In-memory key-value cache manager
         */
        var cache = new function() {
            \"use strict\";
            var dict = {};

            this.get = function(key) {
                return dict.hasOwnProperty(key)
                    ? dict[key]
                    : null;
                };

            this.set = function(key, value) {
                dict[key] = value;

                return value;
            };
        };

        /**
         * Query an element with a CSS selector.
         *
         * @param {string} selector - a CSS-selector-compatible query string
         *
         * @return DOMElement|null
         */
        function query(selector)
        {
            \"use strict\";
            var key = 'SELECTOR: ' + selector;

            return cache.get(key) || cache.set(key, document.querySelector(selector));
        }

        /**
         * Canvas Manager
         */
        function CanvasManager(requests, maxRequestTime) {
            \"use strict\";

            var drawingColors = ";
        // line 183
        echo jsonencode((isset($context["colors"]) || arraykeyexists("colors", $context) ? $context["colors"] : (function () { throw new TwigErrorRuntime('Variable "colors" does not exist.', 183, $this->getSourceContext()); })()));
        echo ",
                storagePrefix = 'timeline/',
                threshold = 1,
                requests = requests,
                maxRequestTime = maxRequestTime;

            /**
             * Check whether this event is a child event.
             *
             * @return true if it is
             */
            function isChildEvent(event)
            {
                return 'section.child' === event.name;
            }

            /**
             * Check whether this event is categorized in 'section'.
             *
             * @return true if it is
             */
            function isSectionEvent(event)
            {
                return 'section' === event.category;
            }

            /**
             * Get the width of the container.
             */
            function getContainerWidth()
            {
                return query('#collector-content h2').clientWidth;
            }

            /**
             * Draw one canvas.
             *
             * @param request   the request object
             * @param max       <subjected for removal>
             * @param threshold the threshold (lower bound) of the length of the timeline (in milliseconds)
             * @param width     the width of the canvas
             */
            this.drawOne = function(request, max, threshold, width)
            {
                \"use strict\";
                var text,
                    ms,
                    xc,
                    drawableEvents,
                    mainEvents,
                    elementId = 'timeline' + request.id,
                    canvasHeight = 0,
                    gapPerEvent = 38,
                    colors = drawingColors,
                    space = 10.5,
                    ratio = (width - space * 2) / max,
                    h = space,
                    x = request.left * ratio + space, // position
                    canvas = cache.get(elementId) || cache.set(elementId, document.getElementById(elementId)),
                    ctx = canvas.getContext(\"2d\"),
                    scaleRatio,
                    devicePixelRatio;

                // Filter events whose total time is below the threshold.
                drawableEvents = request.events.filter(function(event) {
                    return event.duration >= threshold;
                });

                canvasHeight += gapPerEvent * drawableEvents.length;

                // For retina displays so text and boxes will be crisp
                devicePixelRatio = window.devicePixelRatio == \"undefined\" ? 1 : window.devicePixelRatio;
                scaleRatio = devicePixelRatio / 1;

                canvas.width = width * scaleRatio;
                canvas.height = canvasHeight * scaleRatio;

                canvas.style.width = width + 'px';
                canvas.style.height = canvasHeight + 'px';

                ctx.scale(scaleRatio, scaleRatio);

                ctx.textBaseline = \"middle\";
                ctx.lineWidth = 0;

                // For each event, draw a line.
                ctx.strokeStyle = \"#CCC\";

                drawableEvents.forEach(function(event) {
                    event.periods.forEach(function(period) {
                        var timelineHeadPosition = x + period.start * ratio;

                        if (isChildEvent(event)) {
                            /* create a striped background dynamically */
                            var img = new Image();
                            img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKBAMAAAB/HNKOAAAAIVBMVEX////w8PDd7h7d7h7d7h7d7h7w8PDw8PDw8PDw8PDw8PAOi84XAAAAKUlEQVQImWNI71zAwMBQMYuBgY0BxExnADErGEDMTgYQE8hnAKtCZwIAlcMNSR9a1OEAAAAASUVORK5CYII=';
                            var pattern = ctx.createPattern(img, 'repeat');

                            ctx.fillStyle = pattern;
                            ctx.fillRect(timelineHeadPosition, 0, (period.end - period.start) * ratio, canvasHeight);
                        } else if (isSectionEvent(event)) {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, 0);
                            ctx.lineTo(timelineHeadPosition, canvasHeight);
                            ctx.moveTo(timelineTailPosition, 0);
                            ctx.lineTo(timelineTailPosition, canvasHeight);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });
                });

                // Filter for main events.
                mainEvents = drawableEvents.filter(function(event) {
                    return !isChildEvent(event)
                });

                // For each main event, draw the visual presentation of timelines.
                mainEvents.forEach(function(event) {

                    h += 8;

                    // For each sub event, ...
                    event.periods.forEach(function(period) {
                        // Set the drawing style.
                        ctx.fillStyle = colors['default'];
                        ctx.strokeStyle = colors['default'];

                        if (colors[event.name]) {
                            ctx.fillStyle = colors[event.name];
                            ctx.strokeStyle = colors[event.name];
                        } else if (colors[event.category]) {
                            ctx.fillStyle = colors[event.category];
                            ctx.strokeStyle = colors[event.category];
                        }

                        // Draw the timeline
                        var timelineHeadPosition = x + period.start * ratio;

                        if (!isSectionEvent(event)) {
                            ctx.fillRect(timelineHeadPosition, h + 3, 2, 8);
                            ctx.fillRect(timelineHeadPosition, h, (period.end - period.start) * ratio || 2, 6);
                        } else {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineHeadPosition, h + 11);
                            ctx.lineTo(timelineHeadPosition + 8, h);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 11);
                            ctx.lineTo(timelineTailPosition - 8, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });

                    h += 30;

                    ctx.beginPath();
                    ctx.strokeStyle = \"#E0E0E0\";
                    ctx.moveTo(0, h - 10);
                    ctx.lineTo(width, h - 10);
                    ctx.closePath();
                    ctx.stroke();
                });

                h = space;

                // For each event, draw the label.
                mainEvents.forEach(function(event) {

                    ctx.fillStyle = \"#444\";
                    ctx.font = \"12px sans-serif\";
                    text = event.name;
                    ms = \"  \" + (event.duration < 1 ? event.duration : parseInt(event.duration, 10)) + \" ms / \" + event.memory + \" MB\";
                    if (x + event.starttime * ratio + ctx.measureText(text + ms).width > width) {
                        ctx.textAlign = \"end\";
                        ctx.font = \"10px sans-serif\";
                        ctx.fillStyle = \"#777\";
                        xc = x + event.endtime * ratio - 1;
                        ctx.fillText(ms, xc, h);

                        xc -= ctx.measureText(ms).width;
                        ctx.font = \"12px sans-serif\";
                        ctx.fillStyle = \"#222\";
                        ctx.fillText(text, xc, h);
                    } else {
                        ctx.textAlign = \"start\";
                        ctx.font = \"13px sans-serif\";
                        ctx.fillStyle = \"#222\";
                        xc = x + event.starttime * ratio + 1;
                        ctx.fillText(text, xc, h);

                        xc += ctx.measureText(text).width;
                        ctx.font = \"11px sans-serif\";
                        ctx.fillStyle = \"#777\";
                        ctx.fillText(ms, xc, h);
                    }

                    h += gapPerEvent;
                });
            };

            this.drawAll = function(width, threshold)
            {
                \"use strict\";

                width = width || getContainerWidth();
                threshold = threshold || this.getThreshold();

                var self = this;

                requests.forEach(function(request) {
                    self.drawOne(request, maxRequestTime, threshold, width);
                });
            };

            this.getThreshold = function() {
                var threshold = Sfjs.getPreference(storagePrefix + 'threshold');

                if (null === threshold) {
                    return threshold;
                }

                threshold = parseInt(threshold);

                return threshold;
            };

            this.setThreshold = function(threshold)
            {
                threshold = threshold;

                Sfjs.setPreference(storagePrefix + 'threshold', threshold);

                return this;
            };
        }

        function canvasAutoUpdateOnResizeAndSubmit(e) {
            e.preventDefault();
            canvasManager.drawAll();
        }

        function canvasAutoUpdateOnThresholdChange(e) {
            canvasManager
                .setThreshold(query('input[name=\"threshold\"]').value)
                .drawAll();
        }

        var requestsdata = {
            \"max\": ";
        // line 456
        echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 456, $this->getSourceContext()); })()), "events", array()), "section", array()), "endtime", array())), "js", null, true);
        echo ",
            \"requests\": [
";
        // line 458
        echo $context["helper"]->macrodumprequestdata((isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 458, $this->getSourceContext()); })()), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 458, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 458, $this->getSourceContext()); })()), "events", array()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 458, $this->getSourceContext()); })()), "events", array()), "section", array()), "origin", array()));
        echo "

";
        // line 460
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 460, $this->getSourceContext()); })()), "children", array()))) {
            // line 461
            echo "                ,
";
            // line 462
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 462, $this->getSourceContext()); })()), "children", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["child"]) {
                // line 463
                echo $context["helper"]->macrodumprequestdata(twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "token", array()), $context["child"], twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "getcollector", array(0 => "time"), "method"), "events", array()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 463, $this->getSourceContext()); })()), "events", array()), "section", array()), "origin", array()));
                echo ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) ? ("") : (","));
                echo "
";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        }
        // line 466
        echo "            ]
        };

        var canvasManager = new CanvasManager(requestsdata.requests, requestsdata.max);

        query('input[name=\"threshold\"]').value = canvasManager.getThreshold();
        canvasManager.drawAll();

        // Update the colors of legends.
        var timelineLegends = document.querySelectorAll('.sf-profiler-timeline > .legends > span[data-color]');

        for (var i = 0; i < timelineLegends.length; ++i) {
            var timelineLegend = timelineLegends[i];

            timelineLegend.style.borderLeftColor = timelineLegend.getAttribute('data-color');
        }

        // Bind event handlers
        var elementTimelineControl = query('#timeline-control'),
            elementThresholdControl = query('input[name=\"threshold\"]');

        window.onresize = canvasAutoUpdateOnResizeAndSubmit;
        elementTimelineControl.onsubmit = canvasAutoUpdateOnResizeAndSubmit;

        elementThresholdControl.onclick = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onchange = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onkeyup = canvasAutoUpdateOnThresholdChange;

        window.setTimeout(function() {
            canvasAutoUpdateOnThresholdChange(null);
        }, 50);

    //]]>";
        // line 498
        echo "</script>
";
        
        $internald1c0971f9e21486c6fb67885683a37d20380d0adbd2424bd796f29cf4e4b2119->leave($internald1c0971f9e21486c6fb67885683a37d20380d0adbd2424bd796f29cf4e4b2119prof);

        
        $internal3a31afac8021c20ea8748e6dbedf003ca41d6662cecdaaa96792f80ddecb2f77->leave($internal3a31afac8021c20ea8748e6dbedf003ca41d6662cecdaaa96792f80ddecb2f77prof);

    }

    // line 501
    public function macrodumprequestdata($token = null, $profile = null, $events = null, $origin = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "token" => $token,
            "profile" => $profile,
            "events" => $events,
            "origin" => $origin,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal0c8f7534615cc9f13b4a14e91f2b0bef036db3745d02ca4f0ad4d0305fd269fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal0c8f7534615cc9f13b4a14e91f2b0bef036db3745d02ca4f0ad4d0305fd269fe->enter($internal0c8f7534615cc9f13b4a14e91f2b0bef036db3745d02ca4f0ad4d0305fd269feprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "dumprequestdata"));

            $internal58be6c0255bef16f6debacc7b637bd628f9f70c910629b728bbd1d9041045572 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal58be6c0255bef16f6debacc7b637bd628f9f70c910629b728bbd1d9041045572->enter($internal58be6c0255bef16f6debacc7b637bd628f9f70c910629b728bbd1d9041045572prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "dumprequestdata"));

            // line 503
            $context["internal86b4930cd969dbfe97d5ecdd42d10813750e41e5d4d40c0f2522da630768f08d"] = $this;
            // line 504
            echo "                {
                    \"id\": \"";
            // line 505
            echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 505, $this->getSourceContext()); })()), "js", null, true);
            echo "\",
                    \"left\": ";
            // line 506
            echo twigescapefilter($this->env, sprintf("%F", (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["events"]) || arraykeyexists("events", $context) ? $context["events"] : (function () { throw new TwigErrorRuntime('Variable "events" does not exist.', 506, $this->getSourceContext()); })()), "section", array()), "origin", array()) - (isset($context["origin"]) || arraykeyexists("origin", $context) ? $context["origin"] : (function () { throw new TwigErrorRuntime('Variable "origin" does not exist.', 506, $this->getSourceContext()); })()))), "js", null, true);
            echo ",
                    \"events\": [
";
            // line 508
            echo $context["internal86b4930cd969dbfe97d5ecdd42d10813750e41e5d4d40c0f2522da630768f08d"]->macrodumpevents((isset($context["events"]) || arraykeyexists("events", $context) ? $context["events"] : (function () { throw new TwigErrorRuntime('Variable "events" does not exist.', 508, $this->getSourceContext()); })()));
            echo "
                    ]
                }
";
            
            $internal58be6c0255bef16f6debacc7b637bd628f9f70c910629b728bbd1d9041045572->leave($internal58be6c0255bef16f6debacc7b637bd628f9f70c910629b728bbd1d9041045572prof);

            
            $internal0c8f7534615cc9f13b4a14e91f2b0bef036db3745d02ca4f0ad4d0305fd269fe->leave($internal0c8f7534615cc9f13b4a14e91f2b0bef036db3745d02ca4f0ad4d0305fd269feprof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    // line 514
    public function macrodumpevents($events = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "events" => $events,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal60f2e9defb4f45a27d3ff64b04f7b065a9738795c74dfcf5c0daae79cfe794ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal60f2e9defb4f45a27d3ff64b04f7b065a9738795c74dfcf5c0daae79cfe794ca->enter($internal60f2e9defb4f45a27d3ff64b04f7b065a9738795c74dfcf5c0daae79cfe794caprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "dumpevents"));

            $internal19b9c74b4b6e63e9b8ae751917bcdc309bc56465f7b6964cf3d2bc85afb42823 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal19b9c74b4b6e63e9b8ae751917bcdc309bc56465f7b6964cf3d2bc85afb42823->enter($internal19b9c74b4b6e63e9b8ae751917bcdc309bc56465f7b6964cf3d2bc85afb42823prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "dumpevents"));

            // line 516
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["events"]) || arraykeyexists("events", $context) ? $context["events"] : (function () { throw new TwigErrorRuntime('Variable "events" does not exist.', 516, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["event"]) {
                // line 517
                if (("section" != $context["name"])) {
                    // line 518
                    echo "                        {
                            \"name\": \"";
                    // line 519
                    echo twigescapefilter($this->env, $context["name"], "js", null, true);
                    echo "\",
                            \"category\": \"";
                    // line 520
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "category", array()), "js", null, true);
                    echo "\",
                            \"origin\": ";
                    // line 521
                    echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "origin", array())), "js", null, true);
                    echo ",
                            \"starttime\": ";
                    // line 522
                    echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "starttime", array())), "js", null, true);
                    echo ",
                            \"endtime\": ";
                    // line 523
                    echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "endtime", array())), "js", null, true);
                    echo ",
                            \"duration\": ";
                    // line 524
                    echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "duration", array())), "js", null, true);
                    echo ",
                            \"memory\": ";
                    // line 525
                    echo twigescapefilter($this->env, sprintf("%.1F", ((twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "memory", array()) / 1024) / 1024)), "js", null, true);
                    echo ",
                            \"periods\": [";
                    // line 527
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "periods", array()));
                    $context['loop'] = array(
                      'parent' => $context['parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                        $length = count($context['seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['seq'] as $context["key"] => $context["period"]) {
                        // line 528
                        echo "{\"start\": ";
                        echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["period"], "starttime", array())), "js", null, true);
                        echo ", \"end\": ";
                        echo twigescapefilter($this->env, sprintf("%F", twiggetattribute($this->env, $this->getSourceContext(), $context["period"], "endtime", array())), "js", null, true);
                        echo "}";
                        echo ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) ? ("") : (", "));
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['period'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 530
                    echo "]
                        }";
                    // line 531
                    echo ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) ? ("") : (","));
                    echo "
";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['event'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            
            $internal19b9c74b4b6e63e9b8ae751917bcdc309bc56465f7b6964cf3d2bc85afb42823->leave($internal19b9c74b4b6e63e9b8ae751917bcdc309bc56465f7b6964cf3d2bc85afb42823prof);

            
            $internal60f2e9defb4f45a27d3ff64b04f7b065a9738795c74dfcf5c0daae79cfe794ca->leave($internal60f2e9defb4f45a27d3ff64b04f7b065a9738795c74dfcf5c0daae79cfe794caprof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    // line 537
    public function macrodisplaytimeline($id = null, $events = null, $colors = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "id" => $id,
            "events" => $events,
            "colors" => $colors,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internalb4e58fb2f1daf68954ee243e0de0d7d89264dae9854b48279c0660408d44c4fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internalb4e58fb2f1daf68954ee243e0de0d7d89264dae9854b48279c0660408d44c4fb->enter($internalb4e58fb2f1daf68954ee243e0de0d7d89264dae9854b48279c0660408d44c4fbprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "displaytimeline"));

            $internal6b05a4426fe9ed7278c5669e7747fd0289a021813ef4a5b123fa03a50afbe445 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal6b05a4426fe9ed7278c5669e7747fd0289a021813ef4a5b123fa03a50afbe445->enter($internal6b05a4426fe9ed7278c5669e7747fd0289a021813ef4a5b123fa03a50afbe445prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "displaytimeline"));

            // line 538
            echo "    <div class=\"sf-profiler-timeline\">
        <div class=\"legends\">
            ";
            // line 540
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["colors"]) || arraykeyexists("colors", $context) ? $context["colors"] : (function () { throw new TwigErrorRuntime('Variable "colors" does not exist.', 540, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["category"] => $context["color"]) {
                // line 541
                echo "                <span data-color=\"";
                echo twigescapefilter($this->env, $context["color"], "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, $context["category"], "html", null, true);
                echo "</span>
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['category'], $context['color'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 543
            echo "        </div>
        <canvas width=\"680\" height=\"\" id=\"";
            // line 544
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 544, $this->getSourceContext()); })()), "html", null, true);
            echo "\" class=\"timeline\"></canvas>
    </div>
";
            
            $internal6b05a4426fe9ed7278c5669e7747fd0289a021813ef4a5b123fa03a50afbe445->leave($internal6b05a4426fe9ed7278c5669e7747fd0289a021813ef4a5b123fa03a50afbe445prof);

            
            $internalb4e58fb2f1daf68954ee243e0de0d7d89264dae9854b48279c0660408d44c4fb->leave($internalb4e58fb2f1daf68954ee243e0de0d7d89264dae9854b48279c0660408d44c4fbprof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1020 => 544,  1017 => 543,  1006 => 541,  1002 => 540,  998 => 538,  978 => 537,  948 => 531,  945 => 530,  926 => 528,  909 => 527,  905 => 525,  901 => 524,  897 => 523,  893 => 522,  889 => 521,  885 => 520,  881 => 519,  878 => 518,  876 => 517,  859 => 516,  841 => 514,  822 => 508,  817 => 506,  813 => 505,  810 => 504,  808 => 503,  787 => 501,  776 => 498,  742 => 466,  724 => 463,  707 => 462,  704 => 461,  702 => 460,  697 => 458,  692 => 456,  416 => 183,  372 => 141,  369 => 140,  366 => 139,  358 => 137,  352 => 134,  346 => 133,  343 => 132,  340 => 131,  336 => 130,  331 => 128,  327 => 126,  325 => 125,  320 => 123,  317 => 122,  311 => 119,  308 => 118,  306 => 117,  300 => 114,  296 => 113,  291 => 111,  288 => 110,  286 => 109,  277 => 102,  268 => 101,  254 => 97,  248 => 93,  246 => 92,  240 => 88,  234 => 85,  230 => 84,  226 => 82,  220 => 81,  217 => 80,  212 => 79,  210 => 78,  204 => 75,  200 => 74,  195 => 71,  193 => 70,  190 => 69,  183 => 65,  180 => 64,  178 => 63,  171 => 59,  163 => 54,  157 => 50,  148 => 49,  134 => 44,  131 => 43,  122 => 42,  110 => 39,  107 => 38,  101 => 35,  94 => 31,  90 => 29,  88 => 28,  85 => 27,  79 => 24,  74 => 23,  72 => 22,  69 => 21,  66 => 20,  63 => 19,  60 => 18,  51 => 17,  41 => 1,  38 => 6,  36 => 5,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% import self as helper %}

{% if colors is not defined %}
    {% set colors = {
        'default':                '#999',
        'section':                '#444',
        'eventlistener':         '#00B8F5',
        'eventlistenerloading': '#00B8F5',
        'template':               '#66CC00',
        'doctrine':               '#FF6633',
        'propel':                 '#FF6633',
    } %}
{% endif %}

{% block toolbar %}
    {% set totaltime = collector.events|length ? '%.0f'|format(collector.duration) : 'n/a' %}
    {% set initializationtime = collector.events|length ? '%.0f'|format(collector.inittime) : 'n/a' %}
    {% set statuscolor = collector.events|length and collector.duration > 1000 ? 'yellow' : '' %}

    {% set icon %}
        {{ include('@WebProfiler/Icon/time.svg') }}
        <span class=\"sf-toolbar-value\">{{ totaltime }}</span>
        <span class=\"sf-toolbar-label\">ms</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b>Total time</b>
            <span>{{ totaltime }} ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Initialization time</b>
            <span>{{ initializationtime }} ms</span>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl, status: statuscolor }) }}
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/time.svg') }}</span>
        <strong>Performance</strong>
    </span>
{% endblock %}

{% block panel %}
    <h2>Performance metrics</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">{{ '%.0f'|format(collector.duration) }} <span class=\"unit\">ms</span></span>
            <span class=\"label\">Total execution time</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ '%.0f'|format(collector.inittime) }} <span class=\"unit\">ms</span></span>
            <span class=\"label\">Symfony initialization</span>
        </div>

        {% if profile.collectors.memory %}
            <div class=\"metric\">
                <span class=\"value\">{{ '%.2f'|format(profile.collectors.memory.memory / 1024 / 1024) }} <span class=\"unit\">MB</span></span>
                <span class=\"label\">Peak memory usage</span>
            </div>
        {% endif %}

        {% if profile.children|length > 0 %}
            <div class=\"metric-divider\"></div>

            <div class=\"metric\">
                <span class=\"value\">{{ profile.children|length }}</span>
                <span class=\"label\">Sub-Request{{ profile.children|length > 1 ? 's' }}</span>
            </div>

            {% set subrequeststime = 0 %}
            {% for child in profile.children %}
                {% set subrequeststime = subrequeststime + child.getcollector('time').events.section.duration %}
            {% endfor %}

            <div class=\"metric\">
                <span class=\"value\">{{ subrequeststime }} <span class=\"unit\">ms</span></span>
                <span class=\"label\">Sub-Request{{ profile.children|length > 1 ? 's' }} time</span>
            </div>
        {% endif %}
    </div>

    <h2>Execution timeline</h2>

    {% if collector.events is empty %}
        <div class=\"empty\">
            <p>No timing events have been recorded. Are you sure that debugging is enabled in the kernel?</p>
        </div>
    {% else %}
        {{ block('panelContent') }}
    {% endif %}
{% endblock %}

{% block panelContent %}
    <form id=\"timeline-control\" action=\"\" method=\"get\">
        <input type=\"hidden\" name=\"panel\" value=\"time\">
        <label for=\"threshold\">Threshold</label>
        <input type=\"number\" size=\"3\" name=\"threshold\" id=\"threshold\" value=\"3\" min=\"0\"> ms
        <span class=\"help\">(timeline only displays events with a duration longer than this threshold)</span>
    </form>

    {% if profile.parent %}
        <h3 class=\"dump-inline\">
            Sub-Request {{ profilerdump(profile.getcollector('request').requestattributes.get('controller')) }}
            <small>
                {{ collector.events.section.duration }} ms
                <a class=\"newline\" href=\"{{ path('profiler', { token: profile.parent.token, panel: 'time' }) }}\">Return to parent request</a>
            </small>
        </h3>
    {% elseif profile.children|length > 0 %}
        <h3>
            Main Request <small>{{ collector.events.section.duration }} ms</small>
        </h3>
    {% endif %}

    {{ helper.displaytimeline('timeline' ~ token, collector.events, colors) }}

    {% if profile.children|length %}
        <p class=\"help\">Note: sections with a striped background correspond to sub-requests.</p>

        <h3>Sub-requests <small>({{ profile.children|length }})</small></h3>

        {% for child in profile.children %}
            {% set events = child.getcollector('time').events %}
            <h4>
                <a href=\"{{ path('profiler', { token: child.token, panel: 'time' }) }}\">{{ child.getcollector('request').identifier }}</a>
                <small>{{ events.section.duration }} ms</small>
            </h4>

            {{ helper.displaytimeline('timeline' ~ child.token, events, colors) }}
        {% endfor %}
    {% endif %}

    <script>{% autoescape 'js' %}//<![CDATA[
        /**
         * In-memory key-value cache manager
         */
        var cache = new function() {
            \"use strict\";
            var dict = {};

            this.get = function(key) {
                return dict.hasOwnProperty(key)
                    ? dict[key]
                    : null;
                };

            this.set = function(key, value) {
                dict[key] = value;

                return value;
            };
        };

        /**
         * Query an element with a CSS selector.
         *
         * @param {string} selector - a CSS-selector-compatible query string
         *
         * @return DOMElement|null
         */
        function query(selector)
        {
            \"use strict\";
            var key = 'SELECTOR: ' + selector;

            return cache.get(key) || cache.set(key, document.querySelector(selector));
        }

        /**
         * Canvas Manager
         */
        function CanvasManager(requests, maxRequestTime) {
            \"use strict\";

            var drawingColors = {{ colors|jsonencode|raw }},
                storagePrefix = 'timeline/',
                threshold = 1,
                requests = requests,
                maxRequestTime = maxRequestTime;

            /**
             * Check whether this event is a child event.
             *
             * @return true if it is
             */
            function isChildEvent(event)
            {
                return 'section.child' === event.name;
            }

            /**
             * Check whether this event is categorized in 'section'.
             *
             * @return true if it is
             */
            function isSectionEvent(event)
            {
                return 'section' === event.category;
            }

            /**
             * Get the width of the container.
             */
            function getContainerWidth()
            {
                return query('#collector-content h2').clientWidth;
            }

            /**
             * Draw one canvas.
             *
             * @param request   the request object
             * @param max       <subjected for removal>
             * @param threshold the threshold (lower bound) of the length of the timeline (in milliseconds)
             * @param width     the width of the canvas
             */
            this.drawOne = function(request, max, threshold, width)
            {
                \"use strict\";
                var text,
                    ms,
                    xc,
                    drawableEvents,
                    mainEvents,
                    elementId = 'timeline' + request.id,
                    canvasHeight = 0,
                    gapPerEvent = 38,
                    colors = drawingColors,
                    space = 10.5,
                    ratio = (width - space * 2) / max,
                    h = space,
                    x = request.left * ratio + space, // position
                    canvas = cache.get(elementId) || cache.set(elementId, document.getElementById(elementId)),
                    ctx = canvas.getContext(\"2d\"),
                    scaleRatio,
                    devicePixelRatio;

                // Filter events whose total time is below the threshold.
                drawableEvents = request.events.filter(function(event) {
                    return event.duration >= threshold;
                });

                canvasHeight += gapPerEvent * drawableEvents.length;

                // For retina displays so text and boxes will be crisp
                devicePixelRatio = window.devicePixelRatio == \"undefined\" ? 1 : window.devicePixelRatio;
                scaleRatio = devicePixelRatio / 1;

                canvas.width = width * scaleRatio;
                canvas.height = canvasHeight * scaleRatio;

                canvas.style.width = width + 'px';
                canvas.style.height = canvasHeight + 'px';

                ctx.scale(scaleRatio, scaleRatio);

                ctx.textBaseline = \"middle\";
                ctx.lineWidth = 0;

                // For each event, draw a line.
                ctx.strokeStyle = \"#CCC\";

                drawableEvents.forEach(function(event) {
                    event.periods.forEach(function(period) {
                        var timelineHeadPosition = x + period.start * ratio;

                        if (isChildEvent(event)) {
                            /* create a striped background dynamically */
                            var img = new Image();
                            img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKBAMAAAB/HNKOAAAAIVBMVEX////w8PDd7h7d7h7d7h7d7h7w8PDw8PDw8PDw8PDw8PAOi84XAAAAKUlEQVQImWNI71zAwMBQMYuBgY0BxExnADErGEDMTgYQE8hnAKtCZwIAlcMNSR9a1OEAAAAASUVORK5CYII=';
                            var pattern = ctx.createPattern(img, 'repeat');

                            ctx.fillStyle = pattern;
                            ctx.fillRect(timelineHeadPosition, 0, (period.end - period.start) * ratio, canvasHeight);
                        } else if (isSectionEvent(event)) {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, 0);
                            ctx.lineTo(timelineHeadPosition, canvasHeight);
                            ctx.moveTo(timelineTailPosition, 0);
                            ctx.lineTo(timelineTailPosition, canvasHeight);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });
                });

                // Filter for main events.
                mainEvents = drawableEvents.filter(function(event) {
                    return !isChildEvent(event)
                });

                // For each main event, draw the visual presentation of timelines.
                mainEvents.forEach(function(event) {

                    h += 8;

                    // For each sub event, ...
                    event.periods.forEach(function(period) {
                        // Set the drawing style.
                        ctx.fillStyle = colors['default'];
                        ctx.strokeStyle = colors['default'];

                        if (colors[event.name]) {
                            ctx.fillStyle = colors[event.name];
                            ctx.strokeStyle = colors[event.name];
                        } else if (colors[event.category]) {
                            ctx.fillStyle = colors[event.category];
                            ctx.strokeStyle = colors[event.category];
                        }

                        // Draw the timeline
                        var timelineHeadPosition = x + period.start * ratio;

                        if (!isSectionEvent(event)) {
                            ctx.fillRect(timelineHeadPosition, h + 3, 2, 8);
                            ctx.fillRect(timelineHeadPosition, h, (period.end - period.start) * ratio || 2, 6);
                        } else {
                            var timelineTailPosition = x + period.end * ratio;

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineHeadPosition, h + 11);
                            ctx.lineTo(timelineHeadPosition + 8, h);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 11);
                            ctx.lineTo(timelineTailPosition - 8, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();

                            ctx.beginPath();
                            ctx.moveTo(timelineHeadPosition, h);
                            ctx.lineTo(timelineTailPosition, h);
                            ctx.lineTo(timelineTailPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h + 2);
                            ctx.lineTo(timelineHeadPosition, h);
                            ctx.fill();
                            ctx.closePath();
                            ctx.stroke();
                        }
                    });

                    h += 30;

                    ctx.beginPath();
                    ctx.strokeStyle = \"#E0E0E0\";
                    ctx.moveTo(0, h - 10);
                    ctx.lineTo(width, h - 10);
                    ctx.closePath();
                    ctx.stroke();
                });

                h = space;

                // For each event, draw the label.
                mainEvents.forEach(function(event) {

                    ctx.fillStyle = \"#444\";
                    ctx.font = \"12px sans-serif\";
                    text = event.name;
                    ms = \"  \" + (event.duration < 1 ? event.duration : parseInt(event.duration, 10)) + \" ms / \" + event.memory + \" MB\";
                    if (x + event.starttime * ratio + ctx.measureText(text + ms).width > width) {
                        ctx.textAlign = \"end\";
                        ctx.font = \"10px sans-serif\";
                        ctx.fillStyle = \"#777\";
                        xc = x + event.endtime * ratio - 1;
                        ctx.fillText(ms, xc, h);

                        xc -= ctx.measureText(ms).width;
                        ctx.font = \"12px sans-serif\";
                        ctx.fillStyle = \"#222\";
                        ctx.fillText(text, xc, h);
                    } else {
                        ctx.textAlign = \"start\";
                        ctx.font = \"13px sans-serif\";
                        ctx.fillStyle = \"#222\";
                        xc = x + event.starttime * ratio + 1;
                        ctx.fillText(text, xc, h);

                        xc += ctx.measureText(text).width;
                        ctx.font = \"11px sans-serif\";
                        ctx.fillStyle = \"#777\";
                        ctx.fillText(ms, xc, h);
                    }

                    h += gapPerEvent;
                });
            };

            this.drawAll = function(width, threshold)
            {
                \"use strict\";

                width = width || getContainerWidth();
                threshold = threshold || this.getThreshold();

                var self = this;

                requests.forEach(function(request) {
                    self.drawOne(request, maxRequestTime, threshold, width);
                });
            };

            this.getThreshold = function() {
                var threshold = Sfjs.getPreference(storagePrefix + 'threshold');

                if (null === threshold) {
                    return threshold;
                }

                threshold = parseInt(threshold);

                return threshold;
            };

            this.setThreshold = function(threshold)
            {
                threshold = threshold;

                Sfjs.setPreference(storagePrefix + 'threshold', threshold);

                return this;
            };
        }

        function canvasAutoUpdateOnResizeAndSubmit(e) {
            e.preventDefault();
            canvasManager.drawAll();
        }

        function canvasAutoUpdateOnThresholdChange(e) {
            canvasManager
                .setThreshold(query('input[name=\"threshold\"]').value)
                .drawAll();
        }

        var requestsdata = {
            \"max\": {{ \"%F\"|format(collector.events.section.endtime) }},
            \"requests\": [
{{ helper.dumprequestdata(token, profile, collector.events, collector.events.section.origin) }}

{% if profile.children|length %}
                ,
{% for child in profile.children %}
{{ helper.dumprequestdata(child.token, child, child.getcollector('time').events, collector.events.section.origin) }}{{ loop.last ? '' : ',' }}
{% endfor %}
{% endif %}
            ]
        };

        var canvasManager = new CanvasManager(requestsdata.requests, requestsdata.max);

        query('input[name=\"threshold\"]').value = canvasManager.getThreshold();
        canvasManager.drawAll();

        // Update the colors of legends.
        var timelineLegends = document.querySelectorAll('.sf-profiler-timeline > .legends > span[data-color]');

        for (var i = 0; i < timelineLegends.length; ++i) {
            var timelineLegend = timelineLegends[i];

            timelineLegend.style.borderLeftColor = timelineLegend.getAttribute('data-color');
        }

        // Bind event handlers
        var elementTimelineControl = query('#timeline-control'),
            elementThresholdControl = query('input[name=\"threshold\"]');

        window.onresize = canvasAutoUpdateOnResizeAndSubmit;
        elementTimelineControl.onsubmit = canvasAutoUpdateOnResizeAndSubmit;

        elementThresholdControl.onclick = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onchange = canvasAutoUpdateOnThresholdChange;
        elementThresholdControl.onkeyup = canvasAutoUpdateOnThresholdChange;

        window.setTimeout(function() {
            canvasAutoUpdateOnThresholdChange(null);
        }, 50);

    //]]>{% endautoescape %}</script>
{% endblock %}

{% macro dumprequestdata(token, profile, events, origin) %}
{% autoescape 'js' %}
{% from self import dumpevents %}
                {
                    \"id\": \"{{ token }}\",
                    \"left\": {{ \"%F\"|format(events.section.origin - origin) }},
                    \"events\": [
{{ dumpevents(events) }}
                    ]
                }
{% endautoescape %}
{% endmacro %}

{% macro dumpevents(events) %}
{% autoescape 'js' %}
{% for name, event in events %}
{% if 'section' != name %}
                        {
                            \"name\": \"{{ name }}\",
                            \"category\": \"{{ event.category }}\",
                            \"origin\": {{ \"%F\"|format(event.origin) }},
                            \"starttime\": {{ \"%F\"|format(event.starttime) }},
                            \"endtime\": {{ \"%F\"|format(event.endtime) }},
                            \"duration\": {{ \"%F\"|format(event.duration) }},
                            \"memory\": {{ \"%.1F\"|format(event.memory / 1024 / 1024) }},
                            \"periods\": [
                                {%- for period in event.periods -%}
                                    {\"start\": {{ \"%F\"|format(period.starttime) }}, \"end\": {{ \"%F\"|format(period.endtime) }}}{{ loop.last ? '' : ', ' }}
                                {%- endfor -%}
                            ]
                        }{{ loop.last ? '' : ',' }}
{% endif %}
{% endfor %}
{% endautoescape %}
{% endmacro %}

{% macro displaytimeline(id, events, colors) %}
    <div class=\"sf-profiler-timeline\">
        <div class=\"legends\">
            {% for category, color in colors %}
                <span data-color=\"{{ color }}\">{{ category }}</span>
            {% endfor %}
        </div>
        <canvas width=\"680\" height=\"\" id=\"{{ id }}\" class=\"timeline\"></canvas>
    </div>
{% endmacro %}
", "WebProfilerBundle:Collector:time.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/time.html.twig");
    }
}
