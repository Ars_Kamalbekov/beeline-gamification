<?php

/* SonataAdminBundle:CRUD:baseshowcompare.html.twig */
class TwigTemplatedcadade28a005269dd20b98e55bb7b1f465040839969985566b0fc08d18173f7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshow.html.twig", "SonataAdminBundle:CRUD:baseshowcompare.html.twig", 12);
        $this->blocks = array(
            'showfield' => array($this, 'blockshowfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshow.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal29f8fe1f2e68a57ad520c7d7acfd078e49634069f7bd05ea10452b5024082893 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29f8fe1f2e68a57ad520c7d7acfd078e49634069f7bd05ea10452b5024082893->enter($internal29f8fe1f2e68a57ad520c7d7acfd078e49634069f7bd05ea10452b5024082893prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowcompare.html.twig"));

        $internal991ea16286a21de4b42c2d7ba07c43bc568addcf4e1766161d561cb053be60dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal991ea16286a21de4b42c2d7ba07c43bc568addcf4e1766161d561cb053be60dc->enter($internal991ea16286a21de4b42c2d7ba07c43bc568addcf4e1766161d561cb053be60dcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowcompare.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal29f8fe1f2e68a57ad520c7d7acfd078e49634069f7bd05ea10452b5024082893->leave($internal29f8fe1f2e68a57ad520c7d7acfd078e49634069f7bd05ea10452b5024082893prof);

        
        $internal991ea16286a21de4b42c2d7ba07c43bc568addcf4e1766161d561cb053be60dc->leave($internal991ea16286a21de4b42c2d7ba07c43bc568addcf4e1766161d561cb053be60dcprof);

    }

    // line 14
    public function blockshowfield($context, array $blocks = array())
    {
        $internal1a1d30ba74f266b9a98a5aba2bfc1d2ef52d7aeb39fbe38386aa6d9b786b51da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1a1d30ba74f266b9a98a5aba2bfc1d2ef52d7aeb39fbe38386aa6d9b786b51da->enter($internal1a1d30ba74f266b9a98a5aba2bfc1d2ef52d7aeb39fbe38386aa6d9b786b51daprof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        $internala3e70ef4bdcda7dc914e18176b22ea83b644a765171b48a90af24db1d4016742 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala3e70ef4bdcda7dc914e18176b22ea83b644a765171b48a90af24db1d4016742->enter($internala3e70ef4bdcda7dc914e18176b22ea83b644a765171b48a90af24db1d4016742prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        // line 15
        echo "    <tr class=\"sonata-ba-view-container history-audit-compare\">
        ";
        // line 16
        if (twiggetattribute($this->env, $this->getSourceContext(), ($context["elements"] ?? null), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 16, $this->getSourceContext()); })()), array(), "array", true, true)) {
            // line 17
            echo "            ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElementCompare($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["elements"]) || arraykeyexists("elements", $context) ? $context["elements"] : (function () { throw new TwigErrorRuntime('Variable "elements" does not exist.', 17, $this->getSourceContext()); })()), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 17, $this->getSourceContext()); })()), array(), "array"), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 17, $this->getSourceContext()); })()), (isset($context["objectcompare"]) || arraykeyexists("objectcompare", $context) ? $context["objectcompare"] : (function () { throw new TwigErrorRuntime('Variable "objectcompare" does not exist.', 17, $this->getSourceContext()); })()));
            echo "
        ";
        }
        // line 19
        echo "    </tr>
";
        
        $internala3e70ef4bdcda7dc914e18176b22ea83b644a765171b48a90af24db1d4016742->leave($internala3e70ef4bdcda7dc914e18176b22ea83b644a765171b48a90af24db1d4016742prof);

        
        $internal1a1d30ba74f266b9a98a5aba2bfc1d2ef52d7aeb39fbe38386aa6d9b786b51da->leave($internal1a1d30ba74f266b9a98a5aba2bfc1d2ef52d7aeb39fbe38386aa6d9b786b51daprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseshowcompare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshow.html.twig' %}

{% block showfield %}
    <tr class=\"sonata-ba-view-container history-audit-compare\">
        {% if elements[fieldname] is defined %}
            {{ elements[fieldname]|renderviewelementcompare(object, objectcompare) }}
        {% endif %}
    </tr>
{% endblock %}
", "SonataAdminBundle:CRUD:baseshowcompare.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseshowcompare.html.twig");
    }
}
