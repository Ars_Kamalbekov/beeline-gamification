<?php

/* SonataAdminBundle:CRUD:editboolean.html.twig */
class TwigTemplate3f83819b75cfd6b3aac3bd52afc8c0927ad8ffb9be9e030bf901a615a3e43eaa extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'label' => array($this, 'blocklabel'),
            'errors' => array($this, 'blockerrors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc5cf3d180dc3ce9f1bb5416cf712991216c649d293dd9772ef60d98d313a84d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc5cf3d180dc3ce9f1bb5416cf712991216c649d293dd9772ef60d98d313a84d4->enter($internalc5cf3d180dc3ce9f1bb5416cf712991216c649d293dd9772ef60d98d313a84d4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editboolean.html.twig"));

        $internal5930f51d98a8a404628e8b9123b86993ee997ae5e5cf75266e9d0e5862938ed7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5930f51d98a8a404628e8b9123b86993ee997ae5e5cf75266e9d0e5862938ed7->enter($internal5930f51d98a8a404628e8b9123b86993ee997ae5e5cf75266e9d0e5862938ed7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editboolean.html.twig"));

        // line 11
        echo "
<div>
    <div class=\"sonata-ba-field ";
        // line 13
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 13, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 14
        $this->displayBlock('field', $context, $blocks);
        // line 15
        echo "        ";
        $this->displayBlock('label', $context, $blocks);
        // line 23
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 25
        $this->displayBlock('errors', $context, $blocks);
        // line 26
        echo "        </div>

    </div>
</div>
";
        
        $internalc5cf3d180dc3ce9f1bb5416cf712991216c649d293dd9772ef60d98d313a84d4->leave($internalc5cf3d180dc3ce9f1bb5416cf712991216c649d293dd9772ef60d98d313a84d4prof);

        
        $internal5930f51d98a8a404628e8b9123b86993ee997ae5e5cf75266e9d0e5862938ed7->leave($internal5930f51d98a8a404628e8b9123b86993ee997ae5e5cf75266e9d0e5862938ed7prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal868e89614576331fde5a52e964c4cac2866f6d79fa2df4f952f82a9db9914221 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal868e89614576331fde5a52e964c4cac2866f6d79fa2df4f952f82a9db9914221->enter($internal868e89614576331fde5a52e964c4cac2866f6d79fa2df4f952f82a9db9914221prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalef2d7304060a118e06498cc60a712abc3afd75f52cfcbc0881d29c3d0c8c321e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalef2d7304060a118e06498cc60a712abc3afd75f52cfcbc0881d29c3d0c8c321e->enter($internalef2d7304060a118e06498cc60a712abc3afd75f52cfcbc0881d29c3d0c8c321eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 14, $this->getSourceContext()); })()), 'widget');
        
        $internalef2d7304060a118e06498cc60a712abc3afd75f52cfcbc0881d29c3d0c8c321e->leave($internalef2d7304060a118e06498cc60a712abc3afd75f52cfcbc0881d29c3d0c8c321eprof);

        
        $internal868e89614576331fde5a52e964c4cac2866f6d79fa2df4f952f82a9db9914221->leave($internal868e89614576331fde5a52e964c4cac2866f6d79fa2df4f952f82a9db9914221prof);

    }

    // line 15
    public function blocklabel($context, array $blocks = array())
    {
        $internal27eacc839b1d1cb21eb3fb358aef44b717e330cd347efdbf753a3d6aeb18cca6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal27eacc839b1d1cb21eb3fb358aef44b717e330cd347efdbf753a3d6aeb18cca6->enter($internal27eacc839b1d1cb21eb3fb358aef44b717e330cd347efdbf753a3d6aeb18cca6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal58293d1e7711c945954513afc2eec577e6bf4ff119317c99fda562b0d411fe2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal58293d1e7711c945954513afc2eec577e6bf4ff119317c99fda562b0d411fe2d->enter($internal58293d1e7711c945954513afc2eec577e6bf4ff119317c99fda562b0d411fe2dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 16
        echo "            ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 17
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 17, $this->getSourceContext()); })()), 'label', (twigtestempty($label = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $label)));
            echo "
            ";
        } else {
            // line 19
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 19, $this->getSourceContext()); })()), 'label');
            echo "
            ";
        }
        // line 21
        echo "            <br>
        ";
        
        $internal58293d1e7711c945954513afc2eec577e6bf4ff119317c99fda562b0d411fe2d->leave($internal58293d1e7711c945954513afc2eec577e6bf4ff119317c99fda562b0d411fe2dprof);

        
        $internal27eacc839b1d1cb21eb3fb358aef44b717e330cd347efdbf753a3d6aeb18cca6->leave($internal27eacc839b1d1cb21eb3fb358aef44b717e330cd347efdbf753a3d6aeb18cca6prof);

    }

    // line 25
    public function blockerrors($context, array $blocks = array())
    {
        $internald2d91bac40bc0ea07f79ddd4975ac6fa4fc57ec9cfdae8d62089dfcd4ff19e44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald2d91bac40bc0ea07f79ddd4975ac6fa4fc57ec9cfdae8d62089dfcd4ff19e44->enter($internald2d91bac40bc0ea07f79ddd4975ac6fa4fc57ec9cfdae8d62089dfcd4ff19e44prof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        $internal63ca1d3cb7059782f9f5856a254d2b67c835d22d686b1c291fea221e3bbf4ff1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal63ca1d3cb7059782f9f5856a254d2b67c835d22d686b1c291fea221e3bbf4ff1->enter($internal63ca1d3cb7059782f9f5856a254d2b67c835d22d686b1c291fea221e3bbf4ff1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 25, $this->getSourceContext()); })()), 'errors');
        
        $internal63ca1d3cb7059782f9f5856a254d2b67c835d22d686b1c291fea221e3bbf4ff1->leave($internal63ca1d3cb7059782f9f5856a254d2b67c835d22d686b1c291fea221e3bbf4ff1prof);

        
        $internald2d91bac40bc0ea07f79ddd4975ac6fa4fc57ec9cfdae8d62089dfcd4ff19e44->leave($internald2d91bac40bc0ea07f79ddd4975ac6fa4fc57ec9cfdae8d62089dfcd4ff19e44prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editboolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 25,  105 => 21,  99 => 19,  93 => 17,  90 => 16,  81 => 15,  63 => 14,  49 => 26,  47 => 25,  43 => 23,  40 => 15,  38 => 14,  32 => 13,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div>
    <div class=\"sonata-ba-field {% if fieldelement.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">
        {% block field %}{{ formwidget(fieldelement) }}{% endblock %}
        {% block label %}
            {% if fielddescription.options.name is defined %}
                {{ formlabel(fieldelement, fielddescription.options.name) }}
            {% else %}
                {{ formlabel(fieldelement) }}
            {% endif %}
            <br>
        {% endblock %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ formerrors(fieldelement) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:editboolean.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editboolean.html.twig");
    }
}
