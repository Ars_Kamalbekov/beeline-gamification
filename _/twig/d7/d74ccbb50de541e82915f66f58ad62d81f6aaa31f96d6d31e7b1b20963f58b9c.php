<?php

/* @WebProfiler/Icon/forward.svg */
class TwigTemplate2e73a67b16477c3f59a714987db455bc18cec63a1e8ef735c4b9e885abaf6a8a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3a75782928f035380653df25ca0855557c4e85bad07db05893ece01942b198f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a75782928f035380653df25ca0855557c4e85bad07db05893ece01942b198f1->enter($internal3a75782928f035380653df25ca0855557c4e85bad07db05893ece01942b198f1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $internal99c0b6fdb4155ad1ef868c732700bc5a3ce90ed8861dbd850df65074a00f4d0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal99c0b6fdb4155ad1ef868c732700bc5a3ce90ed8861dbd850df65074a00f4d0c->enter($internal99c0b6fdb4155ad1ef868c732700bc5a3ce90ed8861dbd850df65074a00f4d0cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $internal3a75782928f035380653df25ca0855557c4e85bad07db05893ece01942b198f1->leave($internal3a75782928f035380653df25ca0855557c4e85bad07db05893ece01942b198f1prof);

        
        $internal99c0b6fdb4155ad1ef868c732700bc5a3ce90ed8861dbd850df65074a00f4d0c->leave($internal99c0b6fdb4155ad1ef868c732700bc5a3ce90ed8861dbd850df65074a00f4d0cprof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/forward.svg");
    }
}
