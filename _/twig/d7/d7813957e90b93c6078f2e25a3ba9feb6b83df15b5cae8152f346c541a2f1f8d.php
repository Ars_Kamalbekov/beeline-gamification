<?php

/* @Twig/images/icon-plus-square.svg */
class TwigTemplate632e2a4a7a05e05b01706f60c72920575980266a1e92e2ef62a1e079921b4666 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6cd1413eaee90e3e8d7d0486ff9d5227d8339f1139e8ec76c856c6cf77417e84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6cd1413eaee90e3e8d7d0486ff9d5227d8339f1139e8ec76c856c6cf77417e84->enter($internal6cd1413eaee90e3e8d7d0486ff9d5227d8339f1139e8ec76c856c6cf77417e84prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        $internalafc2506e1e7046e5506ad869278adc7fa598c5fc0ff9097766681ae432bc5256 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalafc2506e1e7046e5506ad869278adc7fa598c5fc0ff9097766681ae432bc5256->enter($internalafc2506e1e7046e5506ad869278adc7fa598c5fc0ff9097766681ae432bc5256prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
";
        
        $internal6cd1413eaee90e3e8d7d0486ff9d5227d8339f1139e8ec76c856c6cf77417e84->leave($internal6cd1413eaee90e3e8d7d0486ff9d5227d8339f1139e8ec76c856c6cf77417e84prof);

        
        $internalafc2506e1e7046e5506ad869278adc7fa598c5fc0ff9097766681ae432bc5256->leave($internalafc2506e1e7046e5506ad869278adc7fa598c5fc0ff9097766681ae432bc5256prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960v-128q0-26-19-45t-45-19h-320v-320q0-26-19-45t-45-19h-128q-26 0-45 19t-19 45v320h-320q-26 0-45 19t-19 45v128q0 26 19 45t45 19h320v320q0 26 19 45t45 19h128q26 0 45-19t19-45v-320h320q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5t-203.5 84.5h-960q-119 0-203.5-84.5t-84.5-203.5v-960q0-119 84.5-203.5t203.5-84.5h960q119 0 203.5 84.5t84.5 203.5z\"/></svg>
", "@Twig/images/icon-plus-square.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square.svg");
    }
}
