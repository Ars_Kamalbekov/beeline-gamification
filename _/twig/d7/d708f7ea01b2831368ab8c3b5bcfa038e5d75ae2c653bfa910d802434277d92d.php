<?php

/* @Framework/FormTable/hiddenrow.html.php */
class TwigTemplate5670d54e90ef24f76417a70ca69b2323b6d6964bfbb86db79a62be0247435313 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal90245f3d592d024addc7f015d94f60830596cd097aac4a59d08ff39c88144bd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal90245f3d592d024addc7f015d94f60830596cd097aac4a59d08ff39c88144bd5->enter($internal90245f3d592d024addc7f015d94f60830596cd097aac4a59d08ff39c88144bd5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/hiddenrow.html.php"));

        $internalf1e7425b6f4a438e5e35bf57b27332b41df2720fd163b7f10336b3babf5806b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf1e7425b6f4a438e5e35bf57b27332b41df2720fd163b7f10336b3babf5806b6->enter($internalf1e7425b6f4a438e5e35bf57b27332b41df2720fd163b7f10336b3babf5806b6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/hiddenrow.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $internal90245f3d592d024addc7f015d94f60830596cd097aac4a59d08ff39c88144bd5->leave($internal90245f3d592d024addc7f015d94f60830596cd097aac4a59d08ff39c88144bd5prof);

        
        $internalf1e7425b6f4a438e5e35bf57b27332b41df2720fd163b7f10336b3babf5806b6->leave($internalf1e7425b6f4a438e5e35bf57b27332b41df2720fd163b7f10336b3babf5806b6prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hiddenrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hiddenrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hiddenrow.html.php");
    }
}
