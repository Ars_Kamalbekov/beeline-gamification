<?php

/* SonataAdminBundle:CRUD:listactionshow.html.twig */
class TwigTemplatee2abbd6a8c1c87c20fd31bc1dd4e8aab33ed4273d758762cc73386f25d0a339f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9bc0581655d4c92d92bd5b26543933bd6735dff3c728b8c21601be393a709ccf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9bc0581655d4c92d92bd5b26543933bd6735dff3c728b8c21601be393a709ccf->enter($internal9bc0581655d4c92d92bd5b26543933bd6735dff3c728b8c21601be393a709ccfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactionshow.html.twig"));

        $internalc2ec87cbc5d7987b830cf24f2f5740fefe475a532d6c1fbd31fa1f890bcd36be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc2ec87cbc5d7987b830cf24f2f5740fefe475a532d6c1fbd31fa1f890bcd36be->enter($internalc2ec87cbc5d7987b830cf24f2f5740fefe475a532d6c1fbd31fa1f890bcd36beprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactionshow.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasAccess", array(0 => "show", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "show"), "method"))) {
            // line 13
            echo "    <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "show", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 13, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default viewlink\" title=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actionshow", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
        <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
        ";
            // line 15
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actionshow", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    </a>
";
        }
        
        $internal9bc0581655d4c92d92bd5b26543933bd6735dff3c728b8c21601be393a709ccf->leave($internal9bc0581655d4c92d92bd5b26543933bd6735dff3c728b8c21601be393a709ccfprof);

        
        $internalc2ec87cbc5d7987b830cf24f2f5740fefe475a532d6c1fbd31fa1f890bcd36be->leave($internalc2ec87cbc5d7987b830cf24f2f5740fefe475a532d6c1fbd31fa1f890bcd36beprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listactionshow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 15,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.hasAccess('show', object) and admin.hasRoute('show') %}
    <a href=\"{{ admin.generateObjectUrl('show', object) }}\" class=\"btn btn-sm btn-default viewlink\" title=\"{{ 'actionshow'|trans({}, 'SonataAdminBundle') }}\">
        <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
        {{ 'actionshow'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endif %}
", "SonataAdminBundle:CRUD:listactionshow.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listactionshow.html.twig");
    }
}
