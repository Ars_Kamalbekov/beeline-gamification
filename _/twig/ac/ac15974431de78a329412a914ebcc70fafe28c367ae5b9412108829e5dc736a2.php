<?php

/* WebProfilerBundle:Profiler:table.html.twig */
class TwigTemplate9c4da9d76734b805b15f9dd1f0d76fd92ecafff432dddb31e80c1c9033f809fa extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0c0a2147c06b206f67a43be0cf90c2b0ca97c06a8e29f6e4ff3553716ec6844e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0c0a2147c06b206f67a43be0cf90c2b0ca97c06a8e29f6e4ff3553716ec6844e->enter($internal0c0a2147c06b206f67a43be0cf90c2b0ca97c06a8e29f6e4ff3553716ec6844eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:table.html.twig"));

        $internal569e17939adfad62cd9af1ace832d89cc7af8a342f720be52fa9c7495b8f4cc9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal569e17939adfad62cd9af1ace832d89cc7af8a342f720be52fa9c7495b8f4cc9->enter($internal569e17939adfad62cd9af1ace832d89cc7af8a342f720be52fa9c7495b8f4cc9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:table.html.twig"));

        // line 1
        echo "<table class=\"";
        echo twigescapefilter($this->env, ((arraykeyexists("class", $context)) ? (twigdefaultfilter((isset($context["class"]) || arraykeyexists("class", $context) ? $context["class"] : (function () { throw new TwigErrorRuntime('Variable "class" does not exist.', 1, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
        echo "\">
    <thead>
        <tr>
            <th scope=\"col\" class=\"key\">";
        // line 4
        echo twigescapefilter($this->env, ((arraykeyexists("labels", $context)) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["labels"]) || arraykeyexists("labels", $context) ? $context["labels"] : (function () { throw new TwigErrorRuntime('Variable "labels" does not exist.', 4, $this->getSourceContext()); })()), 0, array(), "array")) : ("Key")), "html", null, true);
        echo "</th>
            <th scope=\"col\">";
        // line 5
        echo twigescapefilter($this->env, ((arraykeyexists("labels", $context)) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["labels"]) || arraykeyexists("labels", $context) ? $context["labels"] : (function () { throw new TwigErrorRuntime('Variable "labels" does not exist.', 5, $this->getSourceContext()); })()), 1, array(), "array")) : ("Value")), "html", null, true);
        echo "</th>
        </tr>
    </thead>
    <tbody>
        ";
        // line 9
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twigsortfilter(twiggetarraykeysfilter((isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 9, $this->getSourceContext()); })()))));
        foreach ($context['seq'] as $context["key"] => $context["key"]) {
            // line 10
            echo "            <tr>
                <th scope=\"row\">";
            // line 11
            echo twigescapefilter($this->env, $context["key"], "html", null, true);
            echo "</th>
                <td>";
            // line 12
            echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 12, $this->getSourceContext()); })()), $context["key"], array(), "array")));
            echo "</td>
            </tr>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['key'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 15
        echo "    </tbody>
</table>
";
        
        $internal0c0a2147c06b206f67a43be0cf90c2b0ca97c06a8e29f6e4ff3553716ec6844e->leave($internal0c0a2147c06b206f67a43be0cf90c2b0ca97c06a8e29f6e4ff3553716ec6844eprof);

        
        $internal569e17939adfad62cd9af1ace832d89cc7af8a342f720be52fa9c7495b8f4cc9->leave($internal569e17939adfad62cd9af1ace832d89cc7af8a342f720be52fa9c7495b8f4cc9prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 15,  54 => 12,  50 => 11,  47 => 10,  43 => 9,  36 => 5,  32 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<table class=\"{{ class|default('') }}\">
    <thead>
        <tr>
            <th scope=\"col\" class=\"key\">{{ labels is defined ? labels[0] : 'Key' }}</th>
            <th scope=\"col\">{{ labels is defined ? labels[1] : 'Value' }}</th>
        </tr>
    </thead>
    <tbody>
        {% for key in data|keys|sort %}
            <tr>
                <th scope=\"row\">{{ key }}</th>
                <td>{{ profilerdump(data[key]) }}</td>
            </tr>
        {% endfor %}
    </tbody>
</table>
", "WebProfilerBundle:Profiler:table.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/table.html.twig");
    }
}
