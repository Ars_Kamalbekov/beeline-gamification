<?php

/* @WebProfiler/Icon/yes.svg */
class TwigTemplatec5a56d60dc2b4444998cbc2eb36e8ec3d363547856a1a3432d5167c458e81b35 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala7d07bf8e5c4d4631672483e78dfb1823918fc54cc152f929b71e249acbe905e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala7d07bf8e5c4d4631672483e78dfb1823918fc54cc152f929b71e249acbe905e->enter($internala7d07bf8e5c4d4631672483e78dfb1823918fc54cc152f929b71e249acbe905eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/yes.svg"));

        $internal0e1b0427e0bee53744c3cd47020261eddbf2ed6165146318ba4d10f72c8fae72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0e1b0427e0bee53744c3cd47020261eddbf2ed6165146318ba4d10f72c8fae72->enter($internal0e1b0427e0bee53744c3cd47020261eddbf2ed6165146318ba4d10f72c8fae72prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/yes.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#5E976E\" d=\"M12,3.1c0,0.4-0.1,0.8-0.4,1.1L5.9,9.8c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-1-0.4L0.4,6.3
    C0.1,6,0,5.6,0,5.2c0-0.4,0.2-0.7,0.4-0.9C0.6,4,1,3.9,1.3,3.9c0.4,0,0.8,0.1,1.1,0.4l2.5,2.5l4.7-4.7c0.3-0.3,0.7-0.4,1-0.4
    c0.4,0,0.7,0.2,0.9,0.4C11.8,2.4,12,2.7,12,3.1z\"/>
</svg>
";
        
        $internala7d07bf8e5c4d4631672483e78dfb1823918fc54cc152f929b71e249acbe905e->leave($internala7d07bf8e5c4d4631672483e78dfb1823918fc54cc152f929b71e249acbe905eprof);

        
        $internal0e1b0427e0bee53744c3cd47020261eddbf2ed6165146318ba4d10f72c8fae72->leave($internal0e1b0427e0bee53744c3cd47020261eddbf2ed6165146318ba4d10f72c8fae72prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/yes.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#5E976E\" d=\"M12,3.1c0,0.4-0.1,0.8-0.4,1.1L5.9,9.8c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-1-0.4L0.4,6.3
    C0.1,6,0,5.6,0,5.2c0-0.4,0.2-0.7,0.4-0.9C0.6,4,1,3.9,1.3,3.9c0.4,0,0.8,0.1,1.1,0.4l2.5,2.5l4.7-4.7c0.3-0.3,0.7-0.4,1-0.4
    c0.4,0,0.7,0.2,0.9,0.4C11.8,2.4,12,2.7,12,3.1z\"/>
</svg>
", "@WebProfiler/Icon/yes.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/yes.svg");
    }
}
