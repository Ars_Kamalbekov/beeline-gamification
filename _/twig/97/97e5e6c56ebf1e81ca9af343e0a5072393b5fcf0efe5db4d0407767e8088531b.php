<?php

/* SonataAdminBundle:CRUD:baseshow.html.twig */
class TwigTemplate14f5d403fe2464fc62d219e62a7c2b1b3e605d24dd55c68b8a301d64b683be0d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'navbartitle' => array($this, 'blocknavbartitle'),
            'actions' => array($this, 'blockactions'),
            'tabmenu' => array($this, 'blocktabmenu'),
            'show' => array($this, 'blockshow'),
            'showgroups' => array($this, 'blockshowgroups'),
            'fieldrow' => array($this, 'blockfieldrow'),
            'showtitle' => array($this, 'blockshowtitle'),
            'showfield' => array($this, 'blockshowfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:baseshow.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal61d843299a686a4882cdab1ef3d37a8bb9e773a919a3de00800c3975b67ca7fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal61d843299a686a4882cdab1ef3d37a8bb9e773a919a3de00800c3975b67ca7fe->enter($internal61d843299a686a4882cdab1ef3d37a8bb9e773a919a3de00800c3975b67ca7feprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshow.html.twig"));

        $internaldfc69615182b44de93bfc63b62838f14e589453fab26dbb79e264c75f0f573f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldfc69615182b44de93bfc63b62838f14e589453fab26dbb79e264c75f0f573f8->enter($internaldfc69615182b44de93bfc63b62838f14e589453fab26dbb79e264c75f0f573f8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshow.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal61d843299a686a4882cdab1ef3d37a8bb9e773a919a3de00800c3975b67ca7fe->leave($internal61d843299a686a4882cdab1ef3d37a8bb9e773a919a3de00800c3975b67ca7feprof);

        
        $internaldfc69615182b44de93bfc63b62838f14e589453fab26dbb79e264c75f0f573f8->leave($internaldfc69615182b44de93bfc63b62838f14e589453fab26dbb79e264c75f0f573f8prof);

    }

    // line 14
    public function blocktitle($context, array $blocks = array())
    {
        $internal3a7376212713af3899b426473772563ac95ac440ea8bc6412d89706c37101f26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a7376212713af3899b426473772563ac95ac440ea8bc6412d89706c37101f26->enter($internal3a7376212713af3899b426473772563ac95ac440ea8bc6412d89706c37101f26prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal44829bd1356a87db35c890b3cad9c5ef5375b6f3231c7494366f7ee07aae300e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal44829bd1356a87db35c890b3cad9c5ef5375b6f3231c7494366f7ee07aae300e->enter($internal44829bd1356a87db35c890b3cad9c5ef5375b6f3231c7494366f7ee07aae300eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titleshow", array("%name%" => twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "toString", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"), 15)), "SonataAdminBundle"), "html", null, true);
        echo "
";
        
        $internal44829bd1356a87db35c890b3cad9c5ef5375b6f3231c7494366f7ee07aae300e->leave($internal44829bd1356a87db35c890b3cad9c5ef5375b6f3231c7494366f7ee07aae300eprof);

        
        $internal3a7376212713af3899b426473772563ac95ac440ea8bc6412d89706c37101f26->leave($internal3a7376212713af3899b426473772563ac95ac440ea8bc6412d89706c37101f26prof);

    }

    // line 18
    public function blocknavbartitle($context, array $blocks = array())
    {
        $internal6ecbb7be1594ef71439d8552316cd81be78d5da9254669dff05fcd14a028a241 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6ecbb7be1594ef71439d8552316cd81be78d5da9254669dff05fcd14a028a241->enter($internal6ecbb7be1594ef71439d8552316cd81be78d5da9254669dff05fcd14a028a241prof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        $internal5e3b75466d349fdb5a9af1842d81afc32ccf448a9753a022bb08883f8cf4746a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5e3b75466d349fdb5a9af1842d81afc32ccf448a9753a022bb08883f8cf4746a->enter($internal5e3b75466d349fdb5a9af1842d81afc32ccf448a9753a022bb08883f8cf4746aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        // line 19
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $internal5e3b75466d349fdb5a9af1842d81afc32ccf448a9753a022bb08883f8cf4746a->leave($internal5e3b75466d349fdb5a9af1842d81afc32ccf448a9753a022bb08883f8cf4746aprof);

        
        $internal6ecbb7be1594ef71439d8552316cd81be78d5da9254669dff05fcd14a028a241->leave($internal6ecbb7be1594ef71439d8552316cd81be78d5da9254669dff05fcd14a028a241prof);

    }

    // line 22
    public function blockactions($context, array $blocks = array())
    {
        $internalff55263d36cbaf62e41f8b238bd581156f52293ad0fd7147f4c08ed7fb6f6af7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalff55263d36cbaf62e41f8b238bd581156f52293ad0fd7147f4c08ed7fb6f6af7->enter($internalff55263d36cbaf62e41f8b238bd581156f52293ad0fd7147f4c08ed7fb6f6af7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal67b64fe07be5cebc0df49fccf78cea086c66be329a062463f3b8ea5e2c93af28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal67b64fe07be5cebc0df49fccf78cea086c66be329a062463f3b8ea5e2c93af28->enter($internal67b64fe07be5cebc0df49fccf78cea086c66be329a062463f3b8ea5e2c93af28prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 23
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:baseshow.html.twig", 23)->display($context);
        
        $internal67b64fe07be5cebc0df49fccf78cea086c66be329a062463f3b8ea5e2c93af28->leave($internal67b64fe07be5cebc0df49fccf78cea086c66be329a062463f3b8ea5e2c93af28prof);

        
        $internalff55263d36cbaf62e41f8b238bd581156f52293ad0fd7147f4c08ed7fb6f6af7->leave($internalff55263d36cbaf62e41f8b238bd581156f52293ad0fd7147f4c08ed7fb6f6af7prof);

    }

    // line 26
    public function blocktabmenu($context, array $blocks = array())
    {
        $internalef8cb4ee399fb28d7d3c06d4485f80677d40fe0248189e621965b0839b13e352 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalef8cb4ee399fb28d7d3c06d4485f80677d40fe0248189e621965b0839b13e352->enter($internalef8cb4ee399fb28d7d3c06d4485f80677d40fe0248189e621965b0839b13e352prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal4d0070dfc530ff5b0489364ec2efd5b7725064af4033c4c1e20a9d609b79c856 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4d0070dfc530ff5b0489364ec2efd5b7725064af4033c4c1e20a9d609b79c856->enter($internal4d0070dfc530ff5b0489364ec2efd5b7725064af4033c4c1e20a9d609b79c856prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        // line 27
        echo "    ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 27, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 27, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 29
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 29, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
        // line 30
        echo "
";
        
        $internal4d0070dfc530ff5b0489364ec2efd5b7725064af4033c4c1e20a9d609b79c856->leave($internal4d0070dfc530ff5b0489364ec2efd5b7725064af4033c4c1e20a9d609b79c856prof);

        
        $internalef8cb4ee399fb28d7d3c06d4485f80677d40fe0248189e621965b0839b13e352->leave($internalef8cb4ee399fb28d7d3c06d4485f80677d40fe0248189e621965b0839b13e352prof);

    }

    // line 33
    public function blockshow($context, array $blocks = array())
    {
        $internald9320e723faefbc0ac2dbead9016ab62feee6403847806e2ac8b457d3900cd68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald9320e723faefbc0ac2dbead9016ab62feee6403847806e2ac8b457d3900cd68->enter($internald9320e723faefbc0ac2dbead9016ab62feee6403847806e2ac8b457d3900cd68prof = new TwigProfilerProfile($this->getTemplateName(), "block", "show"));

        $internalfd1b7442f4110abb8b33440753572f7686de77c40e4be8d0e946fec555eab188 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfd1b7442f4110abb8b33440753572f7686de77c40e4be8d0e946fec555eab188->enter($internalfd1b7442f4110abb8b33440753572f7686de77c40e4be8d0e946fec555eab188prof = new TwigProfilerProfile($this->getTemplateName(), "block", "show"));

        // line 34
        echo "    <div class=\"sonata-ba-view\">

        ";
        // line 36
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.show.top", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "object" => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 36, $this->getSourceContext()); })()))));
        echo "

        ";
        // line 38
        $context["hastab"] = (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "showtabs", array())) == 1) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "showtabs", array())), 0, array(), "array") != "default")) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "showtabs", array())) > 1));
        // line 39
        echo "
        ";
        // line 40
        if ((isset($context["hastab"]) || arraykeyexists("hastab", $context) ? $context["hastab"] : (function () { throw new TwigErrorRuntime('Variable "hastab" does not exist.', 40, $this->getSourceContext()); })())) {
            // line 41
            echo "            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    ";
            // line 43
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 43, $this->getSourceContext()); })()), "showtabs", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["showtab"]) {
                // line 44
                echo "                        <li";
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                    echo " class=\"active\"";
                }
                echo ">
                            <a href=\"#tab";
                // line 45
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 45, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\" data-toggle=\"tab\">
                                <i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i>
                                ";
                // line 47
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "translationdomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "translationdomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 47, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                echo "
                            </a>
                        </li>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['showtab'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 51
            echo "                </ul>

                <div class=\"tab-content\">
                    ";
            // line 54
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 54, $this->getSourceContext()); })()), "showtabs", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["code"] => $context["showtab"]) {
                // line 55
                echo "                        <div
                                class=\"tab-pane fade";
                // line 56
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                    echo " in active";
                }
                echo "\"
                                id=\"tab";
                // line 57
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 57, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\"
                        >
                            <div class=\"box-body  container-fluid\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    ";
                // line 61
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "description", array()) != false)) {
                    // line 62
                    echo "                                        <p>";
                    echo twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "description", array());
                    echo "</p>
                                    ";
                }
                // line 64
                echo "
                                    ";
                // line 65
                $context["groups"] = twiggetattribute($this->env, $this->getSourceContext(), $context["showtab"], "groups", array());
                // line 66
                echo "                                    ";
                $this->displayBlock("showgroups", $context, $blocks);
                echo "
                                </div>
                            </div>
                        </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['code'], $context['showtab'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 71
            echo "                </div>
            </div>
        ";
        } elseif (twigtestiterable(twiggetattribute($this->env, $this->getSourceContext(),         // line 73
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 73, $this->getSourceContext()); })()), "showtabs", array()))) {
            // line 74
            echo "            ";
            $context["groups"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 74, $this->getSourceContext()); })()), "showtabs", array()), "default", array()), "groups", array());
            // line 75
            echo "            ";
            $this->displayBlock("showgroups", $context, $blocks);
            echo "
        ";
        }
        // line 77
        echo "
    </div>

    ";
        // line 80
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.show.bottom", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 80, $this->getSourceContext()); })()), "object" => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 80, $this->getSourceContext()); })()))));
        echo "
";
        
        $internalfd1b7442f4110abb8b33440753572f7686de77c40e4be8d0e946fec555eab188->leave($internalfd1b7442f4110abb8b33440753572f7686de77c40e4be8d0e946fec555eab188prof);

        
        $internald9320e723faefbc0ac2dbead9016ab62feee6403847806e2ac8b457d3900cd68->leave($internald9320e723faefbc0ac2dbead9016ab62feee6403847806e2ac8b457d3900cd68prof);

    }

    // line 83
    public function blockshowgroups($context, array $blocks = array())
    {
        $internal877389c3b9bccb587253e0bc34f74526339145ebdd2cba79e3c08786f9dcc917 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal877389c3b9bccb587253e0bc34f74526339145ebdd2cba79e3c08786f9dcc917->enter($internal877389c3b9bccb587253e0bc34f74526339145ebdd2cba79e3c08786f9dcc917prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showgroups"));

        $internald0b0d7dbc42a76e21fe5647eb427fc65df59373ab359f8a8195f33e04c30df86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald0b0d7dbc42a76e21fe5647eb427fc65df59373ab359f8a8195f33e04c30df86->enter($internald0b0d7dbc42a76e21fe5647eb427fc65df59373ab359f8a8195f33e04c30df86prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showgroups"));

        // line 84
        echo "    <div class=\"row\">
        ";
        // line 85
        $this->displayBlock('fieldrow', $context, $blocks);
        // line 117
        echo "
    </div>
";
        
        $internald0b0d7dbc42a76e21fe5647eb427fc65df59373ab359f8a8195f33e04c30df86->leave($internald0b0d7dbc42a76e21fe5647eb427fc65df59373ab359f8a8195f33e04c30df86prof);

        
        $internal877389c3b9bccb587253e0bc34f74526339145ebdd2cba79e3c08786f9dcc917->leave($internal877389c3b9bccb587253e0bc34f74526339145ebdd2cba79e3c08786f9dcc917prof);

    }

    // line 85
    public function blockfieldrow($context, array $blocks = array())
    {
        $internalf3d7e4168236dc85fae5ba2de6fb8f03ed3231f3fba41eb1a83d59eb8005f645 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf3d7e4168236dc85fae5ba2de6fb8f03ed3231f3fba41eb1a83d59eb8005f645->enter($internalf3d7e4168236dc85fae5ba2de6fb8f03ed3231f3fba41eb1a83d59eb8005f645prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldrow"));

        $internalc988ebb23be68d8ef25e2b9158bf3e861c66e6c00f583e4af14ffe0067aba801 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc988ebb23be68d8ef25e2b9158bf3e861c66e6c00f583e4af14ffe0067aba801->enter($internalc988ebb23be68d8ef25e2b9158bf3e861c66e6c00f583e4af14ffe0067aba801prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldrow"));

        // line 86
        echo "            ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 86, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["code"]) {
            // line 87
            echo "                ";
            $context["showgroup"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 87, $this->getSourceContext()); })()), "showgroups", array()), $context["code"], array(), "array");
            // line 88
            echo "
                <div class=\"";
            // line 89
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "class", array()), "col-md-12")) : ("col-md-12")), "html", null, true);
            echo " ";
            echo ((((arraykeyexists("nopadding", $context)) ? (twigdefaultfilter((isset($context["nopadding"]) || arraykeyexists("nopadding", $context) ? $context["nopadding"] : (function () { throw new TwigErrorRuntime('Variable "nopadding" does not exist.', 89, $this->getSourceContext()); })()), false)) : (false))) ? ("nopadding") : (""));
            echo "\">
                    <div class=\"";
            // line 90
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 90, $this->getSourceContext()); })()), "boxclass", array()), "html", null, true);
            echo "\">
                        <div class=\"box-header\">
                            <h4 class=\"box-title\">
                                ";
            // line 93
            $this->displayBlock('showtitle', $context, $blocks);
            // line 96
            echo "                            </h4>
                        </div>
                        <div class=\"box-body table-responsive no-padding\">
                            <table class=\"table\">
                                <tbody>
                                ";
            // line 101
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 101, $this->getSourceContext()); })()), "fields", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["fieldname"]) {
                // line 102
                echo "                                    ";
                $this->displayBlock('showfield', $context, $blocks);
                // line 109
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['fieldname'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 110
            echo "                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['code'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 116
        echo "        ";
        
        $internalc988ebb23be68d8ef25e2b9158bf3e861c66e6c00f583e4af14ffe0067aba801->leave($internalc988ebb23be68d8ef25e2b9158bf3e861c66e6c00f583e4af14ffe0067aba801prof);

        
        $internalf3d7e4168236dc85fae5ba2de6fb8f03ed3231f3fba41eb1a83d59eb8005f645->leave($internalf3d7e4168236dc85fae5ba2de6fb8f03ed3231f3fba41eb1a83d59eb8005f645prof);

    }

    // line 93
    public function blockshowtitle($context, array $blocks = array())
    {
        $internal13ba121b4e0340bd99caa3d12e25fa9e2c8cf27af4ecf989a29a65e3a3a766c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal13ba121b4e0340bd99caa3d12e25fa9e2c8cf27af4ecf989a29a65e3a3a766c3->enter($internal13ba121b4e0340bd99caa3d12e25fa9e2c8cf27af4ecf989a29a65e3a3a766c3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showtitle"));

        $internal0d0c68492b294d650c8b3804004837dc8fc65b27c8a6f64c13bec82cdd78621c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0d0c68492b294d650c8b3804004837dc8fc65b27c8a6f64c13bec82cdd78621c->enter($internal0d0c68492b294d650c8b3804004837dc8fc65b27c8a6f64c13bec82cdd78621cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "showtitle"));

        // line 94
        echo "                                    ";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 94, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "translationdomain", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 94, $this->getSourceContext()); })()), "translationDomain", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 94, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
        echo "
                                ";
        
        $internal0d0c68492b294d650c8b3804004837dc8fc65b27c8a6f64c13bec82cdd78621c->leave($internal0d0c68492b294d650c8b3804004837dc8fc65b27c8a6f64c13bec82cdd78621cprof);

        
        $internal13ba121b4e0340bd99caa3d12e25fa9e2c8cf27af4ecf989a29a65e3a3a766c3->leave($internal13ba121b4e0340bd99caa3d12e25fa9e2c8cf27af4ecf989a29a65e3a3a766c3prof);

    }

    // line 102
    public function blockshowfield($context, array $blocks = array())
    {
        $internal1a335c4f494ea1720da6c2d2ef30f055e73bdd880518d48139fdc99bef7ce38e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1a335c4f494ea1720da6c2d2ef30f055e73bdd880518d48139fdc99bef7ce38e->enter($internal1a335c4f494ea1720da6c2d2ef30f055e73bdd880518d48139fdc99bef7ce38eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        $internal3b9b877cee2ea1c40e11459a4ab2765cc6b15acec4ca8006fe6ec733d75f8021 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3b9b877cee2ea1c40e11459a4ab2765cc6b15acec4ca8006fe6ec733d75f8021->enter($internal3b9b877cee2ea1c40e11459a4ab2765cc6b15acec4ca8006fe6ec733d75f8021prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        // line 103
        echo "                                        <tr class=\"sonata-ba-view-container\">
                                            ";
        // line 104
        if (twiggetattribute($this->env, $this->getSourceContext(), ($context["elements"] ?? null), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 104, $this->getSourceContext()); })()), array(), "array", true, true)) {
            // line 105
            echo "                                                ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["elements"]) || arraykeyexists("elements", $context) ? $context["elements"] : (function () { throw new TwigErrorRuntime('Variable "elements" does not exist.', 105, $this->getSourceContext()); })()), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 105, $this->getSourceContext()); })()), array(), "array"), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 105, $this->getSourceContext()); })()));
            echo "
                                            ";
        }
        // line 107
        echo "                                        </tr>
                                    ";
        
        $internal3b9b877cee2ea1c40e11459a4ab2765cc6b15acec4ca8006fe6ec733d75f8021->leave($internal3b9b877cee2ea1c40e11459a4ab2765cc6b15acec4ca8006fe6ec733d75f8021prof);

        
        $internal1a335c4f494ea1720da6c2d2ef30f055e73bdd880518d48139fdc99bef7ce38e->leave($internal1a335c4f494ea1720da6c2d2ef30f055e73bdd880518d48139fdc99bef7ce38eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseshow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  502 => 107,  496 => 105,  494 => 104,  491 => 103,  482 => 102,  469 => 94,  460 => 93,  450 => 116,  431 => 110,  417 => 109,  414 => 102,  397 => 101,  390 => 96,  388 => 93,  382 => 90,  376 => 89,  373 => 88,  370 => 87,  352 => 86,  343 => 85,  331 => 117,  329 => 85,  326 => 84,  317 => 83,  305 => 80,  300 => 77,  294 => 75,  291 => 74,  289 => 73,  285 => 71,  265 => 66,  263 => 65,  260 => 64,  254 => 62,  252 => 61,  243 => 57,  237 => 56,  234 => 55,  217 => 54,  212 => 51,  194 => 47,  187 => 45,  180 => 44,  163 => 43,  159 => 41,  157 => 40,  154 => 39,  152 => 38,  147 => 36,  143 => 34,  134 => 33,  123 => 30,  121 => 29,  119 => 27,  110 => 26,  100 => 23,  91 => 22,  78 => 19,  69 => 18,  56 => 15,  47 => 14,  26 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block title %}
    {{ \"titleshow\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
{% endblock %}

{% block navbartitle %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}
    {{ knpmenurender(admin.sidemenu(action), {
        'currentClass' : 'active',
        'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')
    }, 'twig') }}
{% endblock %}

{% block show %}
    <div class=\"sonata-ba-view\">

        {{ sonatablockrenderevent('sonata.admin.show.top', { 'admin': admin, 'object': object }) }}

        {% set hastab = (admin.showtabs|length == 1 and admin.showtabs|keys[0] != 'default') or admin.showtabs|length > 1 %}

        {% if hastab %}
            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    {% for name, showtab in admin.showtabs %}
                        <li{% if loop.first %} class=\"active\"{% endif %}>
                            <a href=\"#tab{{ admin.uniqid }}{{ loop.index }}\" data-toggle=\"tab\">
                                <i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i>
                                {{ showtab.label|trans({}, showtab.translationdomain ?: admin.translationDomain) }}
                            </a>
                        </li>
                    {% endfor %}
                </ul>

                <div class=\"tab-content\">
                    {% for code, showtab in admin.showtabs %}
                        <div
                                class=\"tab-pane fade{% if loop.first %} in active{% endif %}\"
                                id=\"tab{{ admin.uniqid }}{{ loop.index }}\"
                        >
                            <div class=\"box-body  container-fluid\">
                                <div class=\"sonata-ba-collapsed-fields\">
                                    {% if showtab.description != false %}
                                        <p>{{ showtab.description|raw }}</p>
                                    {% endif %}

                                    {% set groups = showtab.groups %}
                                    {{ block('showgroups') }}
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        {% elseif admin.showtabs is iterable %}
            {% set groups = admin.showtabs.default.groups %}
            {{ block('showgroups') }}
        {% endif %}

    </div>

    {{ sonatablockrenderevent('sonata.admin.show.bottom', { 'admin': admin, 'object': object }) }}
{% endblock %}

{% block showgroups %}
    <div class=\"row\">
        {% block fieldrow %}
            {% for code in groups %}
                {% set showgroup = admin.showgroups[code] %}

                <div class=\"{{ showgroup.class|default('col-md-12') }} {{ nopadding|default(false) ? 'nopadding' }}\">
                    <div class=\"{{ showgroup.boxclass }}\">
                        <div class=\"box-header\">
                            <h4 class=\"box-title\">
                                {% block showtitle %}
                                    {{ showgroup.label|trans({}, showgroup.translationdomain|default(admin.translationDomain)) }}
                                {% endblock %}
                            </h4>
                        </div>
                        <div class=\"box-body table-responsive no-padding\">
                            <table class=\"table\">
                                <tbody>
                                {% for fieldname in showgroup.fields %}
                                    {% block showfield %}
                                        <tr class=\"sonata-ba-view-container\">
                                            {% if elements[fieldname] is defined %}
                                                {{ elements[fieldname]|renderviewelement(object)}}
                                            {% endif %}
                                        </tr>
                                    {% endblock %}
                                {% endfor %}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            {% endfor %}
        {% endblock %}

    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:baseshow.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseshow.html.twig");
    }
}
