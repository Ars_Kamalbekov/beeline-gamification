<?php

/* @Twig/images/chevron-right.svg */
class TwigTemplatec491ad478d07e25ca016ab97fc6ce9a0a1db9cb463b5559c02a9c4d3d0b29e11 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcc090e8c838ef1d170626f33647fcc7ec52d8ed12269157bb5b03f4605a33ecb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcc090e8c838ef1d170626f33647fcc7ec52d8ed12269157bb5b03f4605a33ecb->enter($internalcc090e8c838ef1d170626f33647fcc7ec52d8ed12269157bb5b03f4605a33ecbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $internald1b1225ee0bb00e32f2765cb2cf076f3b99b3dd44f1a452db64edf683e47b392 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald1b1225ee0bb00e32f2765cb2cf076f3b99b3dd44f1a452db64edf683e47b392->enter($internald1b1225ee0bb00e32f2765cb2cf076f3b99b3dd44f1a452db64edf683e47b392prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $internalcc090e8c838ef1d170626f33647fcc7ec52d8ed12269157bb5b03f4605a33ecb->leave($internalcc090e8c838ef1d170626f33647fcc7ec52d8ed12269157bb5b03f4605a33ecbprof);

        
        $internald1b1225ee0bb00e32f2765cb2cf076f3b99b3dd44f1a452db64edf683e47b392->leave($internald1b1225ee0bb00e32f2765cb2cf076f3b99b3dd44f1a452db64edf683e47b392prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/chevron-right.svg");
    }
}
