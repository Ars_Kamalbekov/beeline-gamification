<?php

/* SonataAdminBundle:CRUD:listemail.html.twig */
class TwigTemplate99bba95c04365bc38dda76c781707d7dcad012224f7a115fa36a3d0764343bb8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listemail.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal30ff355443a853282df14ed3efe5de40bc2ccc61362b6818ed0ee1c87d123d49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal30ff355443a853282df14ed3efe5de40bc2ccc61362b6818ed0ee1c87d123d49->enter($internal30ff355443a853282df14ed3efe5de40bc2ccc61362b6818ed0ee1c87d123d49prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listemail.html.twig"));

        $internal38066f024485bc2ee7ded853511e42941849bbeb351cb607f171ad5e4988dfb4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal38066f024485bc2ee7ded853511e42941849bbeb351cb607f171ad5e4988dfb4->enter($internal38066f024485bc2ee7ded853511e42941849bbeb351cb607f171ad5e4988dfb4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listemail.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal30ff355443a853282df14ed3efe5de40bc2ccc61362b6818ed0ee1c87d123d49->leave($internal30ff355443a853282df14ed3efe5de40bc2ccc61362b6818ed0ee1c87d123d49prof);

        
        $internal38066f024485bc2ee7ded853511e42941849bbeb351cb607f171ad5e4988dfb4->leave($internal38066f024485bc2ee7ded853511e42941849bbeb351cb607f171ad5e4988dfb4prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalfe270ace62522a1b9999c6d6ad203c1c64186cf7a7b04c6905eb713fa53b4771 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfe270ace62522a1b9999c6d6ad203c1c64186cf7a7b04c6905eb713fa53b4771->enter($internalfe270ace62522a1b9999c6d6ad203c1c64186cf7a7b04c6905eb713fa53b4771prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal5d3505810973f5212a477b8fd620b225a5e5aa5e0f958984c640af19c79d784f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5d3505810973f5212a477b8fd620b225a5e5aa5e0f958984c640af19c79d784f->enter($internal5d3505810973f5212a477b8fd620b225a5e5aa5e0f958984c640af19c79d784fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:emaillink.html.twig", "SonataAdminBundle:CRUD:listemail.html.twig", 15)->display($context);
        
        $internal5d3505810973f5212a477b8fd620b225a5e5aa5e0f958984c640af19c79d784f->leave($internal5d3505810973f5212a477b8fd620b225a5e5aa5e0f958984c640af19c79d784fprof);

        
        $internalfe270ace62522a1b9999c6d6ad203c1c64186cf7a7b04c6905eb713fa53b4771->leave($internalfe270ace62522a1b9999c6d6ad203c1c64186cf7a7b04c6905eb713fa53b4771prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:emaillink.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:listemail.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listemail.html.twig");
    }
}
