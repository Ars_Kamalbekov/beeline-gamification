<?php

/* SonataAdminBundle:CRUD:list.html.twig */
class TwigTemplate886988979dca5e3d5758ffd0d09df5f6f09abcde8033ee96695bfae978140645 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baselist.html.twig", "SonataAdminBundle:CRUD:list.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baselist.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalac5412b61c9ae099b3dfd3716063d663845e29547bf1036b59bbbcb13353bf7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalac5412b61c9ae099b3dfd3716063d663845e29547bf1036b59bbbcb13353bf7f->enter($internalac5412b61c9ae099b3dfd3716063d663845e29547bf1036b59bbbcb13353bf7fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $internal3042ca21cabe4618f1e1ba2d5abfa86f3a932f38d3f9f4ee045d9a967ecc8b0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3042ca21cabe4618f1e1ba2d5abfa86f3a932f38d3f9f4ee045d9a967ecc8b0f->enter($internal3042ca21cabe4618f1e1ba2d5abfa86f3a932f38d3f9f4ee045d9a967ecc8b0fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalac5412b61c9ae099b3dfd3716063d663845e29547bf1036b59bbbcb13353bf7f->leave($internalac5412b61c9ae099b3dfd3716063d663845e29547bf1036b59bbbcb13353bf7fprof);

        
        $internal3042ca21cabe4618f1e1ba2d5abfa86f3a932f38d3f9f4ee045d9a967ecc8b0f->leave($internal3042ca21cabe4618f1e1ba2d5abfa86f3a932f38d3f9f4ee045d9a967ecc8b0fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baselist.html.twig' %}
", "SonataAdminBundle:CRUD:list.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/list.html.twig");
    }
}
