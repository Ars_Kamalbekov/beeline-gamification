<?php

/* SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig */
class TwigTemplate2585e2e369e8846467f3229947bfc3b63818c87ba77c293a29c222eab5bb25ae extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3423c958be5b5e378262e6117c5f29ba7008ba8b65e996a276dee1934c038257 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3423c958be5b5e378262e6117c5f29ba7008ba8b65e996a276dee1934c038257->enter($internal3423c958be5b5e378262e6117c5f29ba7008ba8b65e996a276dee1934c038257prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig"));

        $internal7507bfc6faf9dddb272bcedd654fb5c1caa40bf450b3a2bf67a9958be0904be8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7507bfc6faf9dddb272bcedd654fb5c1caa40bf450b3a2bf67a9958be0904be8->enter($internal7507bfc6faf9dddb272bcedd654fb5c1caa40bf450b3a2bf67a9958be0904be8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig"));

        // line 11
        echo "
<div class=\"modal fade\" id=\"fielddialog";
        // line 12
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
        </div>
    </div>
</div>
";
        
        $internal3423c958be5b5e378262e6117c5f29ba7008ba8b65e996a276dee1934c038257->leave($internal3423c958be5b5e378262e6117c5f29ba7008ba8b65e996a276dee1934c038257prof);

        
        $internal7507bfc6faf9dddb272bcedd654fb5c1caa40bf450b3a2bf67a9958be0904be8->leave($internal7507bfc6faf9dddb272bcedd654fb5c1caa40bf450b3a2bf67a9958be0904be8prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div class=\"modal fade\" id=\"fielddialog{{ id }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-lg\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\"></h4>
            </div>
            <div class=\"modal-body\">
            </div>
        </div>
    </div>
</div>
", "SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editmodal.html.twig");
    }
}
