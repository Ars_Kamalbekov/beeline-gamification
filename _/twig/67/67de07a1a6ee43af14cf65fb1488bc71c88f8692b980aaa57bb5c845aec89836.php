<?php

/* SonataAdminBundle:Pager:links.html.twig */
class TwigTemplate4904010b3b6bad3227d5488f8b0b9ae3c132251fc72ea65886189665e777a965 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:baselinks.html.twig", "SonataAdminBundle:Pager:links.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:baselinks.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal16057dad77777b311788d0ba160ff3dbb1e88a66859695dff1d2cf18054eba91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal16057dad77777b311788d0ba160ff3dbb1e88a66859695dff1d2cf18054eba91->enter($internal16057dad77777b311788d0ba160ff3dbb1e88a66859695dff1d2cf18054eba91prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:links.html.twig"));

        $internal2e23d3e23ec6cc2058a1aa92c3d85fb943e9837d36c9213fc38d58acf03c98fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2e23d3e23ec6cc2058a1aa92c3d85fb943e9837d36c9213fc38d58acf03c98fc->enter($internal2e23d3e23ec6cc2058a1aa92c3d85fb943e9837d36c9213fc38d58acf03c98fcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:links.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal16057dad77777b311788d0ba160ff3dbb1e88a66859695dff1d2cf18054eba91->leave($internal16057dad77777b311788d0ba160ff3dbb1e88a66859695dff1d2cf18054eba91prof);

        
        $internal2e23d3e23ec6cc2058a1aa92c3d85fb943e9837d36c9213fc38d58acf03c98fc->leave($internal2e23d3e23ec6cc2058a1aa92c3d85fb943e9837d36c9213fc38d58acf03c98fcprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:links.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:baselinks.html.twig' %}
", "SonataAdminBundle:Pager:links.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Pager/links.html.twig");
    }
}
