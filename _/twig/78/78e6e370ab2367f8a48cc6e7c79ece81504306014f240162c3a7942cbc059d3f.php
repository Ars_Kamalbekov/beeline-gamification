<?php

/* DoctrineBundle:Collector:explain.html.twig */
class TwigTemplatef90a79c3753820eded90f063fc7cfd965ef4a3b57df3ee07a1645a67482a6bcd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalae5a2a4ea18d351dfe4618fc16c4502f9f13b0b00fa05be1f58fe78b7a13633f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalae5a2a4ea18d351dfe4618fc16c4502f9f13b0b00fa05be1f58fe78b7a13633f->enter($internalae5a2a4ea18d351dfe4618fc16c4502f9f13b0b00fa05be1f58fe78b7a13633fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "DoctrineBundle:Collector:explain.html.twig"));

        $internal91b3d9ec5d6c82e6fa45d2b1b2a561f68afe0c43db8d901685c8be24c40146e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal91b3d9ec5d6c82e6fa45d2b1b2a561f68afe0c43db8d901685c8be24c40146e5->enter($internal91b3d9ec5d6c82e6fa45d2b1b2a561f68afe0c43db8d901685c8be24c40146e5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "DoctrineBundle:Collector:explain.html.twig"));

        // line 1
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 1, $this->getSourceContext()); })()), 0, array(), "array")) > 1)) {
            // line 2
            echo "    ";
            // line 3
            echo "    <table style=\"margin: 5px 0;\">
        <thead>
            <tr>
                ";
            // line 6
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 6, $this->getSourceContext()); })()), 0, array(), "array")));
            foreach ($context['seq'] as $context["key"] => $context["label"]) {
                // line 7
                echo "                    <th>";
                echo twigescapefilter($this->env, $context["label"], "html", null, true);
                echo "</th>
                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['label'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 9
            echo "            </tr>
        </thead>
        <tbody>
            ";
            // line 12
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 12, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["row"]) {
                // line 13
                echo "            <tr>
                ";
                // line 14
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable($context["row"]);
                foreach ($context['seq'] as $context["key"] => $context["item"]) {
                    // line 15
                    echo "                    <td>";
                    echo twigescapefilter($this->env, twigreplacefilter($context["item"], array("," => ", ")), "html", null, true);
                    echo "</td>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['item'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 17
                echo "            </tr>
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['row'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 19
            echo "        </tbody>
    </table>
";
        } else {
            // line 22
            echo "    ";
            // line 23
            echo "    <pre style=\"margin: 5px 0;\">";
            // line 24
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 24, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["row"]) {
                // line 25
                echo twigescapefilter($this->env, twigfirst($this->env, $context["row"]), "html", null, true);
                echo "
";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['row'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 27
            echo "</pre>
";
        }
        
        $internalae5a2a4ea18d351dfe4618fc16c4502f9f13b0b00fa05be1f58fe78b7a13633f->leave($internalae5a2a4ea18d351dfe4618fc16c4502f9f13b0b00fa05be1f58fe78b7a13633fprof);

        
        $internal91b3d9ec5d6c82e6fa45d2b1b2a561f68afe0c43db8d901685c8be24c40146e5->leave($internal91b3d9ec5d6c82e6fa45d2b1b2a561f68afe0c43db8d901685c8be24c40146e5prof);

    }

    public function getTemplateName()
    {
        return "DoctrineBundle:Collector:explain.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 27,  92 => 25,  88 => 24,  86 => 23,  84 => 22,  79 => 19,  72 => 17,  63 => 15,  59 => 14,  56 => 13,  52 => 12,  47 => 9,  38 => 7,  34 => 6,  29 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% if data[0]|length > 1 %}
    {# The platform returns a table for the explanation (e.g. MySQL), display all columns #}
    <table style=\"margin: 5px 0;\">
        <thead>
            <tr>
                {% for label in data[0]|keys %}
                    <th>{{ label }}</th>
                {% endfor %}
            </tr>
        </thead>
        <tbody>
            {% for row in data %}
            <tr>
                {% for key, item in row %}
                    <td>{{ item|replace({',': ', '}) }}</td>
                {% endfor %}
            </tr>
            {% endfor %}
        </tbody>
    </table>
{% else %}
    {# The Platform returns a single column for a textual explanation (e.g. PostgreSQL), display all lines #}
    <pre style=\"margin: 5px 0;\">
        {%- for row in data -%}
            {{ row|first }}{{ \"\\n\" }}
        {%- endfor -%}
    </pre>
{% endif %}
", "DoctrineBundle:Collector:explain.html.twig", "/var/www/html/beeline-gamification/vendor/doctrine/doctrine-bundle/Resources/views/Collector/explain.html.twig");
    }
}
