<?php

/* SonataAdminBundle:CRUD:showtrans.html.twig */
class TwigTemplate3c1ea0d5ef10b8e51f826dcca553e09e3139f33ec2e937e3e466b7657170352b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 11
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showtrans.html.twig", 11);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc01eecc8c68dfcac8d9ae86e3cf5309a70308a5840793883e0d2562ab88b8fac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc01eecc8c68dfcac8d9ae86e3cf5309a70308a5840793883e0d2562ab88b8fac->enter($internalc01eecc8c68dfcac8d9ae86e3cf5309a70308a5840793883e0d2562ab88b8facprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showtrans.html.twig"));

        $internal45ebe80c0b939d4b92c5c95ebdde00ac3d42835330660d40a2897f72f19e5de4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal45ebe80c0b939d4b92c5c95ebdde00ac3d42835330660d40a2897f72f19e5de4->enter($internal45ebe80c0b939d4b92c5c95ebdde00ac3d42835330660d40a2897f72f19e5de4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showtrans.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc01eecc8c68dfcac8d9ae86e3cf5309a70308a5840793883e0d2562ab88b8fac->leave($internalc01eecc8c68dfcac8d9ae86e3cf5309a70308a5840793883e0d2562ab88b8facprof);

        
        $internal45ebe80c0b939d4b92c5c95ebdde00ac3d42835330660d40a2897f72f19e5de4->leave($internal45ebe80c0b939d4b92c5c95ebdde00ac3d42835330660d40a2897f72f19e5de4prof);

    }

    // line 13
    public function blockfield($context, array $blocks = array())
    {
        $internala0fa417fc11e32199931b48b98b1a08d47c9ae33c55af72e80723a5f1e2dd984 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala0fa417fc11e32199931b48b98b1a08d47c9ae33c55af72e80723a5f1e2dd984->enter($internala0fa417fc11e32199931b48b98b1a08d47c9ae33c55af72e80723a5f1e2dd984prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internala3c3b6a6948cce51b737f344a7392e5e60bb9f3b1159087cda906becfcb078de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala3c3b6a6948cce51b737f344a7392e5e60bb9f3b1159087cda906becfcb078de->enter($internala3c3b6a6948cce51b737f344a7392e5e60bb9f3b1159087cda906becfcb078deprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 14
        echo "    ";
        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
            // line 15
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()));
            // line 16
            echo "    ";
        } else {
            // line 17
            echo "        ";
            $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "options", array()), "catalogue", array()));
            // line 18
            echo "    ";
        }
        // line 19
        echo "
    ";
        // line 20
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
            // line 21
            echo "        ";
            echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })());
            echo "
    ";
        } else {
            // line 23
            echo "        ";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $internala3c3b6a6948cce51b737f344a7392e5e60bb9f3b1159087cda906becfcb078de->leave($internala3c3b6a6948cce51b737f344a7392e5e60bb9f3b1159087cda906becfcb078deprof);

        
        $internala0fa417fc11e32199931b48b98b1a08d47c9ae33c55af72e80723a5f1e2dd984->leave($internala0fa417fc11e32199931b48b98b1a08d47c9ae33c55af72e80723a5f1e2dd984prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showtrans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 23,  69 => 21,  67 => 20,  64 => 19,  61 => 18,  58 => 17,  55 => 16,  52 => 15,  49 => 14,  40 => 13,  11 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field%}
    {% if fielddescription.options.catalogue is not defined %}
        {% set value = value|trans %}
    {% else %}
        {% set value = value|trans({}, fielddescription.options.catalogue) %}
    {% endif %}

    {% if fielddescription.options.safe %}
        {{ value|raw }}
    {% else %}
        {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:showtrans.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showtrans.html.twig");
    }
}
