<?php

/* @FOSUser/layout.html.twig */
class TwigTemplateced5b4d7797504105ee8a52ec5385cfbfe5ba0aeafec26aba87d3dffd0ab1078 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1a1e0d98f2528b864d7f9e83b5ae683ea6c835d0dbd15d8d25d511d2e2b11c56 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1a1e0d98f2528b864d7f9e83b5ae683ea6c835d0dbd15d8d25d511d2e2b11c56->enter($internal1a1e0d98f2528b864d7f9e83b5ae683ea6c835d0dbd15d8d25d511d2e2b11c56prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $internal06f8cdaa0348e86f30f1b7ea972e4af8a9d23bcc0e4d2dd88a984d00724fb47c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal06f8cdaa0348e86f30f1b7ea972e4af8a9d23bcc0e4d2dd88a984d00724fb47c->enter($internal06f8cdaa0348e86f30f1b7ea972e4af8a9d23bcc0e4d2dd88a984d00724fb47cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap-3.3.7-dist/css/bootstrap.css"), "html", null, true);
        echo "\">
    </head>
    <body>
        <div>
            ";
        // line 9
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ISAUTHENTICATEDREMEMBERED")) {
            // line 10
            echo "                ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.loggedinas", array("%username%" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 10, $this->getSourceContext()); })()), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
                <a href=\"";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusersecuritylogout");
            echo "\">
                    ";
            // line 12
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
            ";
        } else {
            // line 15
            echo "                <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusersecuritylogin");
            echo "\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
            ";
        }
        // line 17
        echo "        </div>

        ";
        // line 19
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 19, $this->getSourceContext()); })()), "request", array()), "hasPreviousSession", array())) {
            // line 20
            echo "            ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['seq'] as $context["type"] => $context["messages"]) {
                // line 21
                echo "                ";
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable($context["messages"]);
                foreach ($context['seq'] as $context["key"] => $context["message"]) {
                    // line 22
                    echo "                    <div class=\"flash-";
                    echo twigescapefilter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 23
                    echo twigescapefilter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['message'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 26
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['type'], $context['messages'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 27
            echo "        ";
        }
        // line 28
        echo "
        <div>
            ";
        // line 30
        $this->displayBlock('fosusercontent', $context, $blocks);
        // line 32
        echo "        </div>
    </body>
</html>
";
        
        $internal1a1e0d98f2528b864d7f9e83b5ae683ea6c835d0dbd15d8d25d511d2e2b11c56->leave($internal1a1e0d98f2528b864d7f9e83b5ae683ea6c835d0dbd15d8d25d511d2e2b11c56prof);

        
        $internal06f8cdaa0348e86f30f1b7ea972e4af8a9d23bcc0e4d2dd88a984d00724fb47c->leave($internal06f8cdaa0348e86f30f1b7ea972e4af8a9d23bcc0e4d2dd88a984d00724fb47cprof);

    }

    // line 30
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal9de89c931e18fb4ab9c27f1ad15b076a04fcd29097aed5536a561f6b07793647 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9de89c931e18fb4ab9c27f1ad15b076a04fcd29097aed5536a561f6b07793647->enter($internal9de89c931e18fb4ab9c27f1ad15b076a04fcd29097aed5536a561f6b07793647prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal2fbde19ee6b97c3660d6a73304733cee09d5960cc6508739afb5742ef0854b41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2fbde19ee6b97c3660d6a73304733cee09d5960cc6508739afb5742ef0854b41->enter($internal2fbde19ee6b97c3660d6a73304733cee09d5960cc6508739afb5742ef0854b41prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 31
        echo "            ";
        
        $internal2fbde19ee6b97c3660d6a73304733cee09d5960cc6508739afb5742ef0854b41->leave($internal2fbde19ee6b97c3660d6a73304733cee09d5960cc6508739afb5742ef0854b41prof);

        
        $internal9de89c931e18fb4ab9c27f1ad15b076a04fcd29097aed5536a561f6b07793647->leave($internal9de89c931e18fb4ab9c27f1ad15b076a04fcd29097aed5536a561f6b07793647prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 31,  122 => 30,  109 => 32,  107 => 30,  103 => 28,  100 => 27,  94 => 26,  85 => 23,  80 => 22,  75 => 21,  70 => 20,  68 => 19,  64 => 17,  56 => 15,  50 => 12,  46 => 11,  41 => 10,  39 => 9,  32 => 5,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <link rel=\"stylesheet\" href=\"{{ asset('bootstrap-3.3.7-dist/css/bootstrap.css') }}\">
    </head>
    <body>
        <div>
            {% if isgranted(\"ISAUTHENTICATEDREMEMBERED\") %}
                {{ 'layout.loggedinas'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fosusersecuritylogout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
                <a href=\"{{ path('fosusersecuritylogin') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>
            {% endif %}
        </div>

        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fosusercontent %}
            {% endblock fosusercontent %}
        </div>
    </body>
</html>
", "@FOSUser/layout.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/layout.html.twig");
    }
}
