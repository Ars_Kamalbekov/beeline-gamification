<?php

/* SonataAdminBundle:CRUD:listboolean.html.twig */
class TwigTemplate04878b34ac5243d2c97c9c3a90ea369bb6e18dff28cd36b8d57f33eb98f65c3c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'fieldspanattributes' => array($this, 'blockfieldspanattributes'),
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listboolean.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala9bd78af6bb9488523894852f62a71cf2314a9a61da8e32f695acc41a0c87f21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala9bd78af6bb9488523894852f62a71cf2314a9a61da8e32f695acc41a0c87f21->enter($internala9bd78af6bb9488523894852f62a71cf2314a9a61da8e32f695acc41a0c87f21prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listboolean.html.twig"));

        $internalba5e537803216a513ce8c0ccb7f1d588b63c2b85b706aa4c4382a701c475c6f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalba5e537803216a513ce8c0ccb7f1d588b63c2b85b706aa4c4382a701c475c6f6->enter($internalba5e537803216a513ce8c0ccb7f1d588b63c2b85b706aa4c4382a701c475c6f6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listboolean.html.twig"));

        // line 14
        $context["isEditable"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "editable", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 14, $this->getSourceContext()); })()), "options", array()), "editable", array())) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 14, $this->getSourceContext()); })())), "method"));
        // line 15
        $context["xEditableType"] = $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getXEditableType(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "type", array()));
        // line 17
        if (((isset($context["isEditable"]) || arraykeyexists("isEditable", $context) ? $context["isEditable"] : (function () { throw new TwigErrorRuntime('Variable "isEditable" does not exist.', 17, $this->getSourceContext()); })()) && (isset($context["xEditableType"]) || arraykeyexists("xEditableType", $context) ? $context["xEditableType"] : (function () { throw new TwigErrorRuntime('Variable "xEditableType" does not exist.', 17, $this->getSourceContext()); })()))) {
        }
        // line 12
        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internala9bd78af6bb9488523894852f62a71cf2314a9a61da8e32f695acc41a0c87f21->leave($internala9bd78af6bb9488523894852f62a71cf2314a9a61da8e32f695acc41a0c87f21prof);

        
        $internalba5e537803216a513ce8c0ccb7f1d588b63c2b85b706aa4c4382a701c475c6f6->leave($internalba5e537803216a513ce8c0ccb7f1d588b63c2b85b706aa4c4382a701c475c6f6prof);

    }

    // line 18
    public function blockfieldspanattributes($context, array $blocks = array())
    {
        $internalba7681fd90531e329fcbbc58debf3a26bafc913b7c02a1a6bb5d8d0bcba512c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalba7681fd90531e329fcbbc58debf3a26bafc913b7c02a1a6bb5d8d0bcba512c1->enter($internalba7681fd90531e329fcbbc58debf3a26bafc913b7c02a1a6bb5d8d0bcba512c1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        $internal8860c19a9e3029e0ee41a3cfb882b87b6fb4c9bc9baa8d920aa70a6f433260af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8860c19a9e3029e0ee41a3cfb882b87b6fb4c9bc9baa8d920aa70a6f433260af->enter($internal8860c19a9e3029e0ee41a3cfb882b87b6fb4c9bc9baa8d920aa70a6f433260afprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        // line 19
        echo "        ";
        obstart();
        // line 20
        echo "            ";
        $this->displayParentBlock("fieldspanattributes", $context, $blocks);
        echo "
            data-source=\"[{value: 0, text: '";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("labeltypeno", array(), "SonataAdminBundle");
        echo "'},{value: 1, text: '";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("labeltypeyes", array(), "SonataAdminBundle");
        echo "'}]\"
        ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        // line 23
        echo "    ";
        
        $internal8860c19a9e3029e0ee41a3cfb882b87b6fb4c9bc9baa8d920aa70a6f433260af->leave($internal8860c19a9e3029e0ee41a3cfb882b87b6fb4c9bc9baa8d920aa70a6f433260afprof);

        
        $internalba7681fd90531e329fcbbc58debf3a26bafc913b7c02a1a6bb5d8d0bcba512c1->leave($internalba7681fd90531e329fcbbc58debf3a26bafc913b7c02a1a6bb5d8d0bcba512c1prof);

    }

    // line 26
    public function blockfield($context, array $blocks = array())
    {
        $internal809744bf1f5b68168a7f7f6ff66ae7f0bafd0b8f2a99d3d2544ee1cf5269160a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal809744bf1f5b68168a7f7f6ff66ae7f0bafd0b8f2a99d3d2544ee1cf5269160a->enter($internal809744bf1f5b68168a7f7f6ff66ae7f0bafd0b8f2a99d3d2544ee1cf5269160aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalab325207bdaec3af7c1a59996308da23dd8eda1e92a6ed6ecb5cdda4b4f972be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalab325207bdaec3af7c1a59996308da23dd8eda1e92a6ed6ecb5cdda4b4f972be->enter($internalab325207bdaec3af7c1a59996308da23dd8eda1e92a6ed6ecb5cdda4b4f972beprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 27
        $this->loadTemplate("SonataAdminBundle:CRUD:displayboolean.html.twig", "SonataAdminBundle:CRUD:listboolean.html.twig", 27)->display($context);
        
        $internalab325207bdaec3af7c1a59996308da23dd8eda1e92a6ed6ecb5cdda4b4f972be->leave($internalab325207bdaec3af7c1a59996308da23dd8eda1e92a6ed6ecb5cdda4b4f972beprof);

        
        $internal809744bf1f5b68168a7f7f6ff66ae7f0bafd0b8f2a99d3d2544ee1cf5269160a->leave($internal809744bf1f5b68168a7f7f6ff66ae7f0bafd0b8f2a99d3d2544ee1cf5269160aprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listboolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 27,  82 => 26,  72 => 23,  65 => 21,  60 => 20,  57 => 19,  48 => 18,  38 => 12,  35 => 17,  33 => 15,  31 => 14,  19 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% set isEditable = fielddescription.options.editable is defined and fielddescription.options.editable and admin.hasAccess('edit', object) %}
{% set xEditableType = fielddescription.type|sonataxeditabletype %}

{% if isEditable and xEditableType %}
    {% block fieldspanattributes %}
        {% spaceless %}
            {{ parent() }}
            data-source=\"[{value: 0, text: '{%- trans from 'SonataAdminBundle' %}labeltypeno{% endtrans -%}'},{value: 1, text: '{%- trans from 'SonataAdminBundle' %}labeltypeyes{% endtrans -%}'}]\"
        {% endspaceless %}
    {% endblock %}
{% endif %}

{% block field %}
    {%- include 'SonataAdminBundle:CRUD:displayboolean.html.twig' -%}
{% endblock %}
", "SonataAdminBundle:CRUD:listboolean.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listboolean.html.twig");
    }
}
