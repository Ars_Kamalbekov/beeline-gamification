<?php

/* SonataAdminBundle:CRUD:showdatetime.html.twig */
class TwigTemplatedc8c42b25fea1cd3f607b9ca9af8bc7e3471786de53ce5503c41ca46a9cdfdc3 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showdatetime.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2cec735b1e274ef06597a5522e62e584d0be8d292052ef9a5aa03452aab4fb1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2cec735b1e274ef06597a5522e62e584d0be8d292052ef9a5aa03452aab4fb1f->enter($internal2cec735b1e274ef06597a5522e62e584d0be8d292052ef9a5aa03452aab4fb1fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showdatetime.html.twig"));

        $internal4dda17ae356e150dc3c16b8b4b2c395eee5c147fc45e5c1e8d54c1e427059818 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4dda17ae356e150dc3c16b8b4b2c395eee5c147fc45e5c1e8d54c1e427059818->enter($internal4dda17ae356e150dc3c16b8b4b2c395eee5c147fc45e5c1e8d54c1e427059818prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showdatetime.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal2cec735b1e274ef06597a5522e62e584d0be8d292052ef9a5aa03452aab4fb1f->leave($internal2cec735b1e274ef06597a5522e62e584d0be8d292052ef9a5aa03452aab4fb1fprof);

        
        $internal4dda17ae356e150dc3c16b8b4b2c395eee5c147fc45e5c1e8d54c1e427059818->leave($internal4dda17ae356e150dc3c16b8b4b2c395eee5c147fc45e5c1e8d54c1e427059818prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal370fcbda31c59b0e3399a0ae3ece137df976c8fe217b4260f21f1fd0a26554a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal370fcbda31c59b0e3399a0ae3ece137df976c8fe217b4260f21f1fd0a26554a5->enter($internal370fcbda31c59b0e3399a0ae3ece137df976c8fe217b4260f21f1fd0a26554a5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal58a01ca00c45ed3b2f5b8f00858527183b6270abcb7b78846958fc616ef2d0ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal58a01ca00c45ed3b2f5b8f00858527183b6270abcb7b78846958fc616ef2d0ed->enter($internal58a01ca00c45ed3b2f5b8f00858527183b6270abcb7b78846958fc616ef2d0edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })())), "html", null, true);
        }
        
        $internal58a01ca00c45ed3b2f5b8f00858527183b6270abcb7b78846958fc616ef2d0ed->leave($internal58a01ca00c45ed3b2f5b8f00858527183b6270abcb7b78846958fc616ef2d0edprof);

        
        $internal370fcbda31c59b0e3399a0ae3ece137df976c8fe217b4260f21f1fd0a26554a5->leave($internal370fcbda31c59b0e3399a0ae3ece137df976c8fe217b4260f21f1fd0a26554a5prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showdatetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif fielddescription.options.format is defined -%}
        {{ value|date(fielddescription.options.format) }}
    {%- else -%}
        {{ value|date }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:showdatetime.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showdatetime.html.twig");
    }
}
