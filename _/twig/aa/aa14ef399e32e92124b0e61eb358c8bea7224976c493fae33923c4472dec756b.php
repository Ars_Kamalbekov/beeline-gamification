<?php

/* @Framework/FormTable/formwidgetcompound.html.php */
class TwigTemplate0f1afbce9ba8872bbb50eef726c48847a9d1f3e3c172b87ed877676a9dbc4c89 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6594fae74f00f7f030436358118da1c772764f4566b6e8c4b67bf9039d83cc32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6594fae74f00f7f030436358118da1c772764f4566b6e8c4b67bf9039d83cc32->enter($internal6594fae74f00f7f030436358118da1c772764f4566b6e8c4b67bf9039d83cc32prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/formwidgetcompound.html.php"));

        $internala9fbd76be787ac72d086218e2fc5e47b72357602618364482218f96e90766896 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala9fbd76be787ac72d086218e2fc5e47b72357602618364482218f96e90766896->enter($internala9fbd76be787ac72d086218e2fc5e47b72357602618364482218f96e90766896prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/formwidgetcompound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'formrows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $internal6594fae74f00f7f030436358118da1c772764f4566b6e8c4b67bf9039d83cc32->leave($internal6594fae74f00f7f030436358118da1c772764f4566b6e8c4b67bf9039d83cc32prof);

        
        $internala9fbd76be787ac72d086218e2fc5e47b72357602618364482218f96e90766896->leave($internala9fbd76be787ac72d086218e2fc5e47b72357602618364482218f96e90766896prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/formwidgetcompound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<table <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'formrows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/formwidgetcompound.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/formwidgetcompound.html.php");
    }
}
