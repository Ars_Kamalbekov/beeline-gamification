<?php

/* WebProfilerBundle:Router:panel.html.twig */
class TwigTemplate2910868808ff60ae18ea04fbb63f203939768b628cef4a25132fd630f244b18a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2e193a28c901260146aa88b54b79e4478423cebbc6ec2355623981120d4e40c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2e193a28c901260146aa88b54b79e4478423cebbc6ec2355623981120d4e40c2->enter($internal2e193a28c901260146aa88b54b79e4478423cebbc6ec2355623981120d4e40c2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Router:panel.html.twig"));

        $internal4ed4c1b64a637a3c87fbd02d78092cc1b0b4dbc6de77a883285543d108f04afe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4ed4c1b64a637a3c87fbd02d78092cc1b0b4dbc6de77a883285543d108f04afe->enter($internal4ed4c1b64a637a3c87fbd02d78092cc1b0b4dbc6de77a883285543d108f04afeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Router:panel.html.twig"));

        // line 1
        echo "<h2>Routing</h2>

<div class=\"metrics\">
    <div class=\"metric\">
        <span class=\"value\">";
        // line 5
        echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 5, $this->getSourceContext()); })()), "route", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 5, $this->getSourceContext()); })()), "route", array())) : ("(none)")), "html", null, true);
        echo "</span>
        <span class=\"label\">Matched route</span>
    </div>

    ";
        // line 9
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 9, $this->getSourceContext()); })()), "route", array())) {
            // line 10
            echo "        <div class=\"metric\">
            <span class=\"value\">";
            // line 11
            echo twigescapefilter($this->env, twiglengthfilter($this->env, (isset($context["traces"]) || arraykeyexists("traces", $context) ? $context["traces"] : (function () { throw new TwigErrorRuntime('Variable "traces" does not exist.', 11, $this->getSourceContext()); })())), "html", null, true);
            echo "</span>
            <span class=\"label\">Tested routes before match</span>
        </div>
    ";
        }
        // line 15
        echo "</div>

";
        // line 17
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 17, $this->getSourceContext()); })()), "route", array())) {
            // line 18
            echo "    <h3>Route Parameters</h3>

    ";
            // line 20
            if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 20, $this->getSourceContext()); })()), "routeParams", array()))) {
                // line 21
                echo "        <div class=\"empty\">
            <p>No parameters.</p>
        </div>
    ";
            } else {
                // line 25
                echo "        ";
                echo twiginclude($this->env, $context, "@WebProfiler/Profiler/table.html.twig", array("data" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 25, $this->getSourceContext()); })()), "routeParams", array()), "labels" => array(0 => "Name", 1 => "Value")), false);
                echo "
    ";
            }
        }
        // line 28
        echo "
";
        // line 29
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["router"]) || arraykeyexists("router", $context) ? $context["router"] : (function () { throw new TwigErrorRuntime('Variable "router" does not exist.', 29, $this->getSourceContext()); })()), "redirect", array())) {
            // line 30
            echo "    <h3>Route Redirection</h3>

    <p>This page redirects to:</p>
    <div class=\"card break-long-words\">
        ";
            // line 34
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["router"]) || arraykeyexists("router", $context) ? $context["router"] : (function () { throw new TwigErrorRuntime('Variable "router" does not exist.', 34, $this->getSourceContext()); })()), "targetUrl", array()), "html", null, true);
            echo "
        ";
            // line 35
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["router"]) || arraykeyexists("router", $context) ? $context["router"] : (function () { throw new TwigErrorRuntime('Variable "router" does not exist.', 35, $this->getSourceContext()); })()), "targetRoute", array())) {
                echo "<span class=\"text-muted\">(route: \"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["router"]) || arraykeyexists("router", $context) ? $context["router"] : (function () { throw new TwigErrorRuntime('Variable "router" does not exist.', 35, $this->getSourceContext()); })()), "targetRoute", array()), "html", null, true);
                echo "\")</span>";
            }
            // line 36
            echo "    </div>
";
        }
        // line 38
        echo "
<h3>Route Matching Logs</h3>

<div class=\"card\">
    <strong>Path to match:</strong> <code>";
        // line 42
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 42, $this->getSourceContext()); })()), "pathinfo", array()), "html", null, true);
        echo "</code>
</div>

<table id=\"router-logs\">
    <thead>
        <tr>
            <th>#</th>
            <th>Route name</th>
            <th>Path</th>
            <th>Log</th>
        </tr>
    </thead>
    <tbody>
    ";
        // line 55
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["traces"]) || arraykeyexists("traces", $context) ? $context["traces"] : (function () { throw new TwigErrorRuntime('Variable "traces" does not exist.', 55, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["trace"]) {
            // line 56
            echo "        <tr class=\"";
            echo (((twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "level", array()) == 1)) ? ("status-warning") : ((((twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "level", array()) == 2)) ? ("status-success") : (""))));
            echo "\">
            <td class=\"font-normal text-muted nowrap\">";
            // line 57
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 58
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "name", array()), "html", null, true);
            echo "</td>
            <td>";
            // line 59
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "path", array()), "html", null, true);
            echo "</td>
            <td class=\"font-normal\">
                ";
            // line 61
            if ((twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "level", array()) == 1)) {
                // line 62
                echo "                    Path almost matches, but
                    <span class=\"newline\">";
                // line 63
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "log", array()), "html", null, true);
                echo "</span>
                ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(),             // line 64
$context["trace"], "level", array()) == 2)) {
                // line 65
                echo "                    ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "log", array()), "html", null, true);
                echo "
                ";
            } else {
                // line 67
                echo "                    Path does not match
                ";
            }
            // line 69
            echo "            </td>
        </tr>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['trace'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 72
        echo "    </tbody>
</table>

<p class=\"help\">
    Note: These matching logs are based on the current router configuration,
    which might differ from the configuration used when profiling this request.
</p>
";
        
        $internal2e193a28c901260146aa88b54b79e4478423cebbc6ec2355623981120d4e40c2->leave($internal2e193a28c901260146aa88b54b79e4478423cebbc6ec2355623981120d4e40c2prof);

        
        $internal4ed4c1b64a637a3c87fbd02d78092cc1b0b4dbc6de77a883285543d108f04afe->leave($internal4ed4c1b64a637a3c87fbd02d78092cc1b0b4dbc6de77a883285543d108f04afeprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Router:panel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 72,  178 => 69,  174 => 67,  168 => 65,  166 => 64,  162 => 63,  159 => 62,  157 => 61,  152 => 59,  148 => 58,  144 => 57,  139 => 56,  122 => 55,  106 => 42,  100 => 38,  96 => 36,  90 => 35,  86 => 34,  80 => 30,  78 => 29,  75 => 28,  68 => 25,  62 => 21,  60 => 20,  56 => 18,  54 => 17,  50 => 15,  43 => 11,  40 => 10,  38 => 9,  31 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<h2>Routing</h2>

<div class=\"metrics\">
    <div class=\"metric\">
        <span class=\"value\">{{ request.route ?: '(none)' }}</span>
        <span class=\"label\">Matched route</span>
    </div>

    {% if request.route %}
        <div class=\"metric\">
            <span class=\"value\">{{ traces|length }}</span>
            <span class=\"label\">Tested routes before match</span>
        </div>
    {% endif %}
</div>

{% if request.route %}
    <h3>Route Parameters</h3>

    {% if request.routeParams is empty %}
        <div class=\"empty\">
            <p>No parameters.</p>
        </div>
    {% else %}
        {{ include('@WebProfiler/Profiler/table.html.twig', { data: request.routeParams, labels: ['Name', 'Value'] }, withcontext = false) }}
    {% endif %}
{% endif %}

{% if router.redirect %}
    <h3>Route Redirection</h3>

    <p>This page redirects to:</p>
    <div class=\"card break-long-words\">
        {{ router.targetUrl }}
        {% if router.targetRoute %}<span class=\"text-muted\">(route: \"{{ router.targetRoute }}\")</span>{% endif %}
    </div>
{% endif %}

<h3>Route Matching Logs</h3>

<div class=\"card\">
    <strong>Path to match:</strong> <code>{{ request.pathinfo }}</code>
</div>

<table id=\"router-logs\">
    <thead>
        <tr>
            <th>#</th>
            <th>Route name</th>
            <th>Path</th>
            <th>Log</th>
        </tr>
    </thead>
    <tbody>
    {% for trace in traces %}
        <tr class=\"{{ trace.level == 1 ? 'status-warning' : trace.level == 2 ? 'status-success' }}\">
            <td class=\"font-normal text-muted nowrap\">{{ loop.index }}</td>
            <td>{{ trace.name }}</td>
            <td>{{ trace.path }}</td>
            <td class=\"font-normal\">
                {% if trace.level == 1 %}
                    Path almost matches, but
                    <span class=\"newline\">{{ trace.log }}</span>
                {% elseif trace.level == 2 %}
                    {{ trace.log }}
                {% else %}
                    Path does not match
                {% endif %}
            </td>
        </tr>
    {% endfor %}
    </tbody>
</table>

<p class=\"help\">
    Note: These matching logs are based on the current router configuration,
    which might differ from the configuration used when profiling this request.
</p>
", "WebProfilerBundle:Router:panel.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Router/panel.html.twig");
    }
}
