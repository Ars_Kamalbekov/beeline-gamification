<?php

/* TwigBundle:Exception:exception.txt.twig */
class TwigTemplate110e2da31f3687f33cf113f669b3422a552e45bf70600420086457a26f85192e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldf1c5bb675a0488f5c5c99d53f53a731b64475b06e195a413dbe03732849fc18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldf1c5bb675a0488f5c5c99d53f53a731b64475b06e195a413dbe03732849fc18->enter($internaldf1c5bb675a0488f5c5c99d53f53a731b64475b06e195a413dbe03732849fc18prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.txt.twig"));

        $internalbc417571e8f876d95d54f51608dde7a41ae8d8e61b2cae3902ce30fd05efce3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbc417571e8f876d95d54f51608dde7a41ae8d8e61b2cae3902ce30fd05efce3e->enter($internalbc417571e8f876d95d54f51608dde7a41ae8d8e61b2cae3902ce30fd05efce3eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.txt.twig"));

        // line 1
        echo "[exception] ";
        echo (((((isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 1, $this->getSourceContext()); })()) . " | ") . (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 1, $this->getSourceContext()); })())) . " | ") . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "class", array()));
        echo "
[message] ";
        // line 2
        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()), "message", array());
        echo "
";
        // line 3
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 3, $this->getSourceContext()); })()), "toarray", array()));
        foreach ($context['seq'] as $context["i"] => $context["e"]) {
            // line 4
            echo "[";
            echo ($context["i"] + 1);
            echo "] ";
            echo twiggetattribute($this->env, $this->getSourceContext(), $context["e"], "class", array());
            echo ": ";
            echo twiggetattribute($this->env, $this->getSourceContext(), $context["e"], "message", array());
            echo "
";
            // line 5
            echo twiginclude($this->env, $context, "@Twig/Exception/traces.txt.twig", array("exception" => $context["e"]), false);
            echo "

";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['i'], $context['e'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internaldf1c5bb675a0488f5c5c99d53f53a731b64475b06e195a413dbe03732849fc18->leave($internaldf1c5bb675a0488f5c5c99d53f53a731b64475b06e195a413dbe03732849fc18prof);

        
        $internalbc417571e8f876d95d54f51608dde7a41ae8d8e61b2cae3902ce30fd05efce3e->leave($internalbc417571e8f876d95d54f51608dde7a41ae8d8e61b2cae3902ce30fd05efce3eprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 5,  38 => 4,  34 => 3,  30 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("[exception] {{ statuscode ~ ' | ' ~ statustext ~ ' | ' ~ exception.class }}
[message] {{ exception.message }}
{% for i, e in exception.toarray %}
[{{ i + 1 }}] {{ e.class }}: {{ e.message }}
{{ include('@Twig/Exception/traces.txt.twig', { exception: e }, withcontext = false) }}

{% endfor %}
", "TwigBundle:Exception:exception.txt.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.txt.twig");
    }
}
