<?php

/* SonataAdminBundle:CRUD:showpercent.html.twig */
class TwigTemplate0bab609004dff6e3fbf600ce8d70ac43afbc0b476fc3f36c332f6e4c8dc144fc extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showpercent.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc2b274973b7d393f6a8dc9e239945c3b1a4ff29979fe322cc52f8fabe067c5dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc2b274973b7d393f6a8dc9e239945c3b1a4ff29979fe322cc52f8fabe067c5dc->enter($internalc2b274973b7d393f6a8dc9e239945c3b1a4ff29979fe322cc52f8fabe067c5dcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showpercent.html.twig"));

        $internal3bc70c4fc4194424d41118e371c0143fb1ecfba481f0239ae3bb1b11a801c3ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3bc70c4fc4194424d41118e371c0143fb1ecfba481f0239ae3bb1b11a801c3ed->enter($internal3bc70c4fc4194424d41118e371c0143fb1ecfba481f0239ae3bb1b11a801c3edprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showpercent.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc2b274973b7d393f6a8dc9e239945c3b1a4ff29979fe322cc52f8fabe067c5dc->leave($internalc2b274973b7d393f6a8dc9e239945c3b1a4ff29979fe322cc52f8fabe067c5dcprof);

        
        $internal3bc70c4fc4194424d41118e371c0143fb1ecfba481f0239ae3bb1b11a801c3ed->leave($internal3bc70c4fc4194424d41118e371c0143fb1ecfba481f0239ae3bb1b11a801c3edprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal7703fef2f92584472bd6e34be15236da07c79aec94d613440b5a084eb4584681 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7703fef2f92584472bd6e34be15236da07c79aec94d613440b5a084eb4584681->enter($internal7703fef2f92584472bd6e34be15236da07c79aec94d613440b5a084eb4584681prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internald77784825f657c847a5ed66e8eed8e93c5779f03b3428c42bdafa205b96071a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald77784825f657c847a5ed66e8eed8e93c5779f03b3428c42bdafa205b96071a4->enter($internald77784825f657c847a5ed66e8eed8e93c5779f03b3428c42bdafa205b96071a4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["value"] = ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()) * 100);
        // line 16
        echo "    ";
        echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo " %
";
        
        $internald77784825f657c847a5ed66e8eed8e93c5779f03b3428c42bdafa205b96071a4->leave($internald77784825f657c847a5ed66e8eed8e93c5779f03b3428c42bdafa205b96071a4prof);

        
        $internal7703fef2f92584472bd6e34be15236da07c79aec94d613440b5a084eb4584681->leave($internal7703fef2f92584472bd6e34be15236da07c79aec94d613440b5a084eb4584681prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showpercent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% set value = value * 100 %}
    {{ value }} %
{% endblock %}
", "SonataAdminBundle:CRUD:showpercent.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showpercent.html.twig");
    }
}
