<?php

/* SonataAdminBundle:CRUD:baselistfield.html.twig */
class TwigTemplatea7ef09e40e97a4a9921b6b647c31f4cb6374bd83afe923be8e7e765598bda0e0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'fieldspanattributes' => array($this, 'blockfieldspanattributes'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfddb619867f259b9c06f34d149e0b3cc92bf417280a19642dc387bd5daf9d6b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfddb619867f259b9c06f34d149e0b3cc92bf417280a19642dc387bd5daf9d6b0->enter($internalfddb619867f259b9c06f34d149e0b3cc92bf417280a19642dc387bd5daf9d6b0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistfield.html.twig"));

        $internal51d67b1281fd54aef052070003e61cfd731a5627e460fd2440ed86861a59346f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal51d67b1281fd54aef052070003e61cfd731a5627e460fd2440ed86861a59346f->enter($internal51d67b1281fd54aef052070003e61cfd731a5627e460fd2440ed86861a59346fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistfield.html.twig"));

        // line 11
        echo "
<td class=\"sonata-ba-list-field sonata-ba-list-field-";
        // line 12
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "type", array()), "html", null, true);
        echo "\" objectId=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\"";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "rowalign", array(), "any", true, true)) {
            echo " style=\"text-align:";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 12, $this->getSourceContext()); })()), "options", array()), "rowalign", array()), "html", null, true);
            echo "\"";
        }
        echo ">
    ";
        // line 13
        $context["route"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "name", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "name", array()), null)) : (null));
        // line 14
        echo "
    ";
        // line 15
        if ((((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 16
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "identifier", array(), "any", true, true) &&         // line 17
(isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 17, $this->getSourceContext()); })())) && twiggetattribute($this->env, $this->getSourceContext(),         // line 18
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "hasRoute", array(0 => (isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 18, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(),         // line 19
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 19, $this->getSourceContext()); })()), "hasAccess", array(0 => (isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 19, $this->getSourceContext()); })()), 1 => ((twiginfilter((isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 19, $this->getSourceContext()); })()), array(0 => "show", 1 => "edit"))) ? ((isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 19, $this->getSourceContext()); })())) : (null))), "method"))) {
            // line 21
            echo "        <a class=\"sonata-link-identifier\" href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 21, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => (isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 21, $this->getSourceContext()); })()), 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 21, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
            echo "\">";
            // line 22
            $this->displayBlock('field', $context, $blocks);
            // line 35
            echo "</a>
    ";
        } else {
            // line 37
            echo "        ";
            $context["isEditable"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "editable", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 37, $this->getSourceContext()); })()), "options", array()), "editable", array())) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 37, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 37, $this->getSourceContext()); })())), "method"));
            // line 38
            echo "        ";
            $context["xEditableType"] = $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getXEditableType(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 38, $this->getSourceContext()); })()), "type", array()));
            // line 39
            echo "
        ";
            // line 40
            if (((isset($context["isEditable"]) || arraykeyexists("isEditable", $context) ? $context["isEditable"] : (function () { throw new TwigErrorRuntime('Variable "isEditable" does not exist.', 40, $this->getSourceContext()); })()) && (isset($context["xEditableType"]) || arraykeyexists("xEditableType", $context) ? $context["xEditableType"] : (function () { throw new TwigErrorRuntime('Variable "xEditableType" does not exist.', 40, $this->getSourceContext()); })()))) {
                // line 41
                echo "            ";
                $context["url"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminsetobjectfieldvalue", twigarraymerge(((twiggetattribute($this->env, $this->getSourceContext(),                 // line 43
($context["admin"] ?? null), "getPersistentParameters", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "getPersistentParameters", array()), array())) : (array())), array("context" => "list", "field" => twiggetattribute($this->env, $this->getSourceContext(),                 // line 45
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 45, $this->getSourceContext()); })()), "name", array()), "objectId" => twiggetattribute($this->env, $this->getSourceContext(),                 // line 46
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 46, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 46, $this->getSourceContext()); })())), "method"), "code" => twiggetattribute($this->env, $this->getSourceContext(),                 // line 47
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 47, $this->getSourceContext()); })()), "code", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 47, $this->getSourceContext()); })())), "method"))));
                // line 50
                echo "
            ";
                // line 51
                if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 51, $this->getSourceContext()); })()), "type", array()) == "date") &&  !twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 51, $this->getSourceContext()); })())))) {
                    // line 52
                    echo "                ";
                    $context["datavalue"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 52, $this->getSourceContext()); })()), "format", array(0 => "Y-m-d"), "method");
                    // line 53
                    echo "            ";
                } elseif (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 53, $this->getSourceContext()); })()), "type", array()) == "boolean") && twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 53, $this->getSourceContext()); })())))) {
                    // line 54
                    echo "                ";
                    $context["datavalue"] = 0;
                    // line 55
                    echo "            ";
                } else {
                    // line 56
                    echo "                ";
                    $context["datavalue"] = (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 56, $this->getSourceContext()); })());
                    // line 57
                    echo "            ";
                }
                // line 58
                echo "
            <span ";
                // line 59
                $this->displayBlock('fieldspanattributes', $context, $blocks);
                // line 64
                echo ">
                ";
                // line 65
                $this->displayBlock("field", $context, $blocks);
                echo "
            </span>
        ";
            } else {
                // line 68
                echo "            ";
                $this->displayBlock("field", $context, $blocks);
                echo "
        ";
            }
            // line 70
            echo "    ";
        }
        // line 71
        echo "</td>
";
        
        $internalfddb619867f259b9c06f34d149e0b3cc92bf417280a19642dc387bd5daf9d6b0->leave($internalfddb619867f259b9c06f34d149e0b3cc92bf417280a19642dc387bd5daf9d6b0prof);

        
        $internal51d67b1281fd54aef052070003e61cfd731a5627e460fd2440ed86861a59346f->leave($internal51d67b1281fd54aef052070003e61cfd731a5627e460fd2440ed86861a59346fprof);

    }

    // line 22
    public function blockfield($context, array $blocks = array())
    {
        $internalbd98ff6ed378f91de2d6bbe7b3763c83d0734ef117e469ec55dade774e406b63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbd98ff6ed378f91de2d6bbe7b3763c83d0734ef117e469ec55dade774e406b63->enter($internalbd98ff6ed378f91de2d6bbe7b3763c83d0734ef117e469ec55dade774e406b63prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal134c05dd44df3070fdb15fb4d003c6648c7f6b42d75c2ea04bba55d29d4715f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal134c05dd44df3070fdb15fb4d003c6648c7f6b42d75c2ea04bba55d29d4715f0->enter($internal134c05dd44df3070fdb15fb4d003c6648c7f6b42d75c2ea04bba55d29d4715f0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 23
        echo "                ";
        obstart();
        // line 24
        echo "                ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "collapse", array(), "any", true, true)) {
            // line 25
            echo "                    ";
            $context["collapse"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 25, $this->getSourceContext()); })()), "options", array()), "collapse", array());
            // line 26
            echo "                    <div class=\"sonata-readmore\"
                          data-readmore-height=\"";
            // line 27
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "height", array()), 40)) : (40)), "html", null, true);
            echo "\"
                          data-readmore-more=\"";
            // line 28
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "more", array()), "readmore")) : ("readmore")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\"
                          data-readmore-less=\"";
            // line 29
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collapse"] ?? null), "less", array()), "readless")) : ("readless")), array(), "SonataAdminBundle"), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 29, $this->getSourceContext()); })()), "html", null, true);
            echo "</div>
                ";
        } else {
            // line 31
            echo "                    ";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 31, $this->getSourceContext()); })()), "html", null, true);
            echo "
                ";
        }
        // line 33
        echo "                ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        // line 34
        echo "            ";
        
        $internal134c05dd44df3070fdb15fb4d003c6648c7f6b42d75c2ea04bba55d29d4715f0->leave($internal134c05dd44df3070fdb15fb4d003c6648c7f6b42d75c2ea04bba55d29d4715f0prof);

        
        $internalbd98ff6ed378f91de2d6bbe7b3763c83d0734ef117e469ec55dade774e406b63->leave($internalbd98ff6ed378f91de2d6bbe7b3763c83d0734ef117e469ec55dade774e406b63prof);

    }

    // line 59
    public function blockfieldspanattributes($context, array $blocks = array())
    {
        $internal999f4eab99da46812e4e758abcdb84bc40c29e60fec30d50865038fef07aacdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal999f4eab99da46812e4e758abcdb84bc40c29e60fec30d50865038fef07aacdb->enter($internal999f4eab99da46812e4e758abcdb84bc40c29e60fec30d50865038fef07aacdbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        $internal8d7e157cbafa41112d98cee76fd46be790472c58a52e581e13df26b2ea70580e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8d7e157cbafa41112d98cee76fd46be790472c58a52e581e13df26b2ea70580e->enter($internal8d7e157cbafa41112d98cee76fd46be790472c58a52e581e13df26b2ea70580eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        echo "class=\"x-editable\"
                  data-type=\"";
        // line 60
        echo twigescapefilter($this->env, (isset($context["xEditableType"]) || arraykeyexists("xEditableType", $context) ? $context["xEditableType"] : (function () { throw new TwigErrorRuntime('Variable "xEditableType" does not exist.', 60, $this->getSourceContext()); })()), "html", null, true);
        echo "\"
                  data-value=\"";
        // line 61
        echo twigescapefilter($this->env, (isset($context["datavalue"]) || arraykeyexists("datavalue", $context) ? $context["datavalue"] : (function () { throw new TwigErrorRuntime('Variable "datavalue" does not exist.', 61, $this->getSourceContext()); })()), "html", null, true);
        echo "\"
                  data-title=\"";
        // line 62
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 62, $this->getSourceContext()); })()), "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 62, $this->getSourceContext()); })()), "translationDomain", array())), "html", null, true);
        echo "\"
                  data-pk=\"";
        // line 63
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 63, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 63, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\"
                  data-url=\"";
        // line 64
        echo twigescapefilter($this->env, (isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 64, $this->getSourceContext()); })()), "html", null, true);
        echo "\" ";
        
        $internal8d7e157cbafa41112d98cee76fd46be790472c58a52e581e13df26b2ea70580e->leave($internal8d7e157cbafa41112d98cee76fd46be790472c58a52e581e13df26b2ea70580eprof);

        
        $internal999f4eab99da46812e4e758abcdb84bc40c29e60fec30d50865038fef07aacdb->leave($internal999f4eab99da46812e4e758abcdb84bc40c29e60fec30d50865038fef07aacdbprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baselistfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  220 => 64,  216 => 63,  212 => 62,  208 => 61,  204 => 60,  193 => 59,  183 => 34,  180 => 33,  174 => 31,  167 => 29,  163 => 28,  159 => 27,  156 => 26,  153 => 25,  150 => 24,  147 => 23,  138 => 22,  127 => 71,  124 => 70,  118 => 68,  112 => 65,  109 => 64,  107 => 59,  104 => 58,  101 => 57,  98 => 56,  95 => 55,  92 => 54,  89 => 53,  86 => 52,  84 => 51,  81 => 50,  79 => 47,  78 => 46,  77 => 45,  76 => 43,  74 => 41,  72 => 40,  69 => 39,  66 => 38,  63 => 37,  59 => 35,  57 => 22,  53 => 21,  51 => 19,  50 => 18,  49 => 17,  48 => 16,  47 => 15,  44 => 14,  42 => 13,  30 => 12,  27 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<td class=\"sonata-ba-list-field sonata-ba-list-field-{{ fielddescription.type }}\" objectId=\"{{ admin.id(object) }}\"{% if fielddescription.options.rowalign is defined %} style=\"text-align:{{ fielddescription.options.rowalign }}\"{% endif %}>
    {% set route = fielddescription.options.route.name|default(null) %}

    {% if
        fielddescription.options.identifier is defined
        and route
        and admin.hasRoute(route)
        and admin.hasAccess(route, route in ['show', 'edit'] ? object : null)
    %}
        <a class=\"sonata-link-identifier\" href=\"{{ admin.generateObjectUrl(route, object, fielddescription.options.route.parameters) }}\">
            {%- block field %}
                {% spaceless %}
                {% if fielddescription.options.collapse is defined %}
                    {% set collapse = fielddescription.options.collapse %}
                    <div class=\"sonata-readmore\"
                          data-readmore-height=\"{{ collapse.height|default(40) }}\"
                          data-readmore-more=\"{{ collapse.more|default('readmore')|trans({}, 'SonataAdminBundle') }}\"
                          data-readmore-less=\"{{ collapse.less|default('readless')|trans({}, 'SonataAdminBundle') }}\">{{ value }}</div>
                {% else %}
                    {{ value }}
                {% endif %}
                {% endspaceless %}
            {% endblock -%}
        </a>
    {% else %}
        {% set isEditable = fielddescription.options.editable is defined and fielddescription.options.editable and admin.hasAccess('edit', object) %}
        {% set xEditableType = fielddescription.type|sonataxeditabletype %}

        {% if isEditable and xEditableType %}
            {% set url = path(
                'sonataadminsetobjectfieldvalue',
                admin.getPersistentParameters|default([])|merge({
                    'context': 'list',
                    'field': fielddescription.name,
                    'objectId': admin.id(object),
                    'code': admin.code(object)
                })
            ) %}

            {% if fielddescription.type == 'date' and value is not empty %}
                {% set datavalue = value.format('Y-m-d') %}
            {% elseif fielddescription.type == 'boolean' and value is empty %}
                {% set datavalue = 0 %}
            {% else %}
                {% set datavalue = value %}
            {% endif %}

            <span {% block fieldspanattributes %}class=\"x-editable\"
                  data-type=\"{{ xEditableType }}\"
                  data-value=\"{{ datavalue }}\"
                  data-title=\"{{ fielddescription.label|trans({}, fielddescription.translationDomain) }}\"
                  data-pk=\"{{ admin.id(object) }}\"
                  data-url=\"{{ url }}\" {% endblock %}>
                {{ block('field') }}
            </span>
        {% else %}
            {{ block('field') }}
        {% endif %}
    {% endif %}
</td>
", "SonataAdminBundle:CRUD:baselistfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baselistfield.html.twig");
    }
}
