<?php

/* SonataAdminBundle:CRUD:tree.html.twig */
class TwigTemplated0c0335b69a4736caf79d932d6a6f656be1ff4733edd07ae3d3c97c528fbba52 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 16
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baselist.html.twig", "SonataAdminBundle:CRUD:tree.html.twig", 16);
        $this->blocks = array(
            'tabmenu' => array($this, 'blocktabmenu'),
            'listtable' => array($this, 'blocklisttable'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baselist.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldab8c2a724d0afca06899bfa8deacc9c8f3b2923007dbe4d30f3afda994c739e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldab8c2a724d0afca06899bfa8deacc9c8f3b2923007dbe4d30f3afda994c739e->enter($internaldab8c2a724d0afca06899bfa8deacc9c8f3b2923007dbe4d30f3afda994c739eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:tree.html.twig"));

        $internal42e50fa93a7fbf2efef0b1c2c1d762ee6d1002abb588c2d6358c2d05e4c32a51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal42e50fa93a7fbf2efef0b1c2c1d762ee6d1002abb588c2d6358c2d05e4c32a51->enter($internal42e50fa93a7fbf2efef0b1c2c1d762ee6d1002abb588c2d6358c2d05e4c32a51prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:tree.html.twig"));

        // line 18
        $context["tree"] = $this;
        // line 16
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internaldab8c2a724d0afca06899bfa8deacc9c8f3b2923007dbe4d30f3afda994c739e->leave($internaldab8c2a724d0afca06899bfa8deacc9c8f3b2923007dbe4d30f3afda994c739eprof);

        
        $internal42e50fa93a7fbf2efef0b1c2c1d762ee6d1002abb588c2d6358c2d05e4c32a51->leave($internal42e50fa93a7fbf2efef0b1c2c1d762ee6d1002abb588c2d6358c2d05e4c32a51prof);

    }

    // line 40
    public function blocktabmenu($context, array $blocks = array())
    {
        $internal7bc1676ad868ac0c09e84b4f09d0d7801ef697afbc94de368e8fe6d2726b7c5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7bc1676ad868ac0c09e84b4f09d0d7801ef697afbc94de368e8fe6d2726b7c5e->enter($internal7bc1676ad868ac0c09e84b4f09d0d7801ef697afbc94de368e8fe6d2726b7c5eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal14c23a68646ad3a709c193504242ad4994413eedf27ec7cffb6821a5192b5e57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal14c23a68646ad3a709c193504242ad4994413eedf27ec7cffb6821a5192b5e57->enter($internal14c23a68646ad3a709c193504242ad4994413eedf27ec7cffb6821a5192b5e57prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        // line 41
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:listtabmenu.html.twig", "SonataAdminBundle:CRUD:tree.html.twig", 41)->display(array("mode" => "tree", "action" =>         // line 43
(isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 43, $this->getSourceContext()); })()), "admin" =>         // line 44
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 44, $this->getSourceContext()); })())));
        
        $internal14c23a68646ad3a709c193504242ad4994413eedf27ec7cffb6821a5192b5e57->leave($internal14c23a68646ad3a709c193504242ad4994413eedf27ec7cffb6821a5192b5e57prof);

        
        $internal7bc1676ad868ac0c09e84b4f09d0d7801ef697afbc94de368e8fe6d2726b7c5e->leave($internal7bc1676ad868ac0c09e84b4f09d0d7801ef697afbc94de368e8fe6d2726b7c5eprof);

    }

    // line 48
    public function blocklisttable($context, array $blocks = array())
    {
        $internalc868813c664bac753c5b0f3ff158948de36754fa0191b7bea3f6368227edc2e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc868813c664bac753c5b0f3ff158948de36754fa0191b7bea3f6368227edc2e8->enter($internalc868813c664bac753c5b0f3ff158948de36754fa0191b7bea3f6368227edc2e8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listtable"));

        $internal84f4f6b09af81d2f96bb29c44041ad84a1bff4c28aa711562fa2251ee7b6e2ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal84f4f6b09af81d2f96bb29c44041ad84a1bff4c28aa711562fa2251ee7b6e2ed->enter($internal84f4f6b09af81d2f96bb29c44041ad84a1bff4c28aa711562fa2251ee7b6e2edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listtable"));

        // line 49
        echo "    <div class=\"col-xs-12 col-md-12\">
        <div class=\"box box-primary\">
            <div class=\"box-header\">
                <h1 class=\"box-title\">
                    ";
        // line 53
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("element.treesitelabel", array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 53, $this->getSourceContext()); })()), "translationdomain", array())), "html", null, true);
        echo "
                    <div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                            <strong class=\"text-info\">";
        // line 56
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentSite"]) || arraykeyexists("currentSite", $context) ? $context["currentSite"] : (function () { throw new TwigErrorRuntime('Variable "currentSite" does not exist.', 56, $this->getSourceContext()); })()), "name", array()), "html", null, true);
        echo "</strong> <span class=\"caret\"></span>
                        </button>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                            ";
        // line 59
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["contexts"]) || arraykeyexists("contexts", $context) ? $context["contexts"] : (function () { throw new TwigErrorRuntime('Variable "contexts" does not exist.', 59, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["context"]) {
            // line 60
            echo "                                <li>
                                    <a href=\"";
            // line 61
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 61, $this->getSourceContext()); })()), "generateUrl", array(0 => "tree", 1 => array("context" => twiggetattribute($this->env, $this->getSourceContext(), $context["context"], "id", array()))), "method"), "html", null, true);
            echo "\">
                                        ";
            // line 62
            if (((isset($context["currentContext"]) || arraykeyexists("currentContext", $context) ? $context["currentContext"] : (function () { throw new TwigErrorRuntime('Variable "currentContext" does not exist.', 62, $this->getSourceContext()); })()) && (twiggetattribute($this->env, $this->getSourceContext(), $context["context"], "id", array()) == twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentContext"]) || arraykeyexists("currentContext", $context) ? $context["currentContext"] : (function () { throw new TwigErrorRuntime('Variable "currentContext" does not exist.', 62, $this->getSourceContext()); })()), "id", array())))) {
                // line 63
                echo "                                            <span class=\"pull-right\">
                                                <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
                                            </span>
                                        ";
            }
            // line 67
            echo "                                        ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["site"]) || arraykeyexists("site", $context) ? $context["site"] : (function () { throw new TwigErrorRuntime('Variable "site" does not exist.', 67, $this->getSourceContext()); })()), "name", array()), "html", null, true);
            echo "
                                    </a>
                                </li>
                            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['context'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 71
        echo "                        </ul>
                    </div>
                </h1>
            </div>
            <div class=\"box-content\">
                ";
        // line 76
        echo $context["tree"]->macronavigatechild((isset($context["collection"]) || arraykeyexists("collection", $context) ? $context["collection"] : (function () { throw new TwigErrorRuntime('Variable "collection" does not exist.', 76, $this->getSourceContext()); })()), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 76, $this->getSourceContext()); })()), true);
        echo "
            </div>
        </div>
    </div>
";
        
        $internal84f4f6b09af81d2f96bb29c44041ad84a1bff4c28aa711562fa2251ee7b6e2ed->leave($internal84f4f6b09af81d2f96bb29c44041ad84a1bff4c28aa711562fa2251ee7b6e2edprof);

        
        $internalc868813c664bac753c5b0f3ff158948de36754fa0191b7bea3f6368227edc2e8->leave($internalc868813c664bac753c5b0f3ff158948de36754fa0191b7bea3f6368227edc2e8prof);

    }

    // line 19
    public function macronavigatechild($collection = null, $admin = null, $root = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "collection" => $collection,
            "admin" => $admin,
            "root" => $root,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal671619f7c79178bbeaa98499de218ca873646f3c485e6cf004f84767ae0bbe14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal671619f7c79178bbeaa98499de218ca873646f3c485e6cf004f84767ae0bbe14->enter($internal671619f7c79178bbeaa98499de218ca873646f3c485e6cf004f84767ae0bbe14prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "navigatechild"));

            $internala43e94dd57594a09121d6eb0843d8440c492a438b3f55a94e8fa8ab6df816adb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internala43e94dd57594a09121d6eb0843d8440c492a438b3f55a94e8fa8ab6df816adb->enter($internala43e94dd57594a09121d6eb0843d8440c492a438b3f55a94e8fa8ab6df816adbprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "navigatechild"));

            // line 20
            echo "    <ul";
            if ((isset($context["root"]) || arraykeyexists("root", $context) ? $context["root"] : (function () { throw new TwigErrorRuntime('Variable "root" does not exist.', 20, $this->getSourceContext()); })())) {
                echo " class=\"sonata-tree\"";
            }
            echo ">
        ";
            // line 21
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["collection"]) || arraykeyexists("collection", $context) ? $context["collection"] : (function () { throw new TwigErrorRuntime('Variable "collection" does not exist.', 21, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                if ( !(isset($context["root"]) || arraykeyexists("root", $context) ? $context["root"] : (function () { throw new TwigErrorRuntime('Variable "root" does not exist.', 21, $this->getSourceContext()); })())) {
                    // line 22
                    echo "            <li>
                <div class=\"sonata-treeitem\">
                    ";
                    // line 24
                    if (twiggetattribute($this->env, $this->getSourceContext(), $context["element"], "parent", array())) {
                        echo "<i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>";
                    }
                    // line 25
                    echo "                    <i class=\"fa fa-code\" aria-hidden=\"true\"></i>
                    <a class=\"sonata-treeitemedit\" href=\"";
                    // line 26
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => $context["element"]), "method"), "html", null, true);
                    echo "\">";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["element"], "name", array()), "html", null, true);
                    echo "</a>
                    <i class=\"text-muted\">";
                    // line 27
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["element"], "description", array()), "html", null, true);
                    echo "</i>
                    <a class=\"label label-default pull-right\" href=\"";
                    // line 28
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => $context["element"]), "method"), "html", null, true);
                    echo "\">edit <i class=\"fa fa-magic\" aria-hidden=\"true\"></i></a>
                    ";
                    // line 29
                    if (true) {
                        echo "<span class=\"label label-warning pull-right\">true</span>";
                    }
                    // line 30
                    echo "                </div>

                ";
                    // line 32
                    if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["element"], "children", array()))) {
                        // line 33
                        echo "                    ";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $this->getTemplateName(), "navigatechild", array(0 => twiggetattribute($this->env, $this->getSourceContext(), $context["element"], "children", array()), 1 => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 33, $this->getSourceContext()); })()), 2 => false), "method"), "html", null, true);
                        echo "
                ";
                    }
                    // line 35
                    echo "            </li>
        ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 37
            echo "    </ul>
";
            
            $internala43e94dd57594a09121d6eb0843d8440c492a438b3f55a94e8fa8ab6df816adb->leave($internala43e94dd57594a09121d6eb0843d8440c492a438b3f55a94e8fa8ab6df816adbprof);

            
            $internal671619f7c79178bbeaa98499de218ca873646f3c485e6cf004f84767ae0bbe14->leave($internal671619f7c79178bbeaa98499de218ca873646f3c485e6cf004f84767ae0bbe14prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:tree.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 37,  218 => 35,  212 => 33,  210 => 32,  206 => 30,  202 => 29,  198 => 28,  194 => 27,  188 => 26,  185 => 25,  181 => 24,  177 => 22,  172 => 21,  165 => 20,  145 => 19,  130 => 76,  123 => 71,  112 => 67,  106 => 63,  104 => 62,  100 => 61,  97 => 60,  93 => 59,  87 => 56,  81 => 53,  75 => 49,  66 => 48,  56 => 44,  55 => 43,  53 => 41,  44 => 40,  34 => 16,  32 => 18,  11 => 16,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{#
    This template is not used at all, it is just a template that you can use to create
    your own custom tree view.
#}
{% extends 'SonataAdminBundle:CRUD:baselist.html.twig' %}

{% import self as tree %}
{% macro navigatechild(collection, admin, root) %}
    <ul{% if root %} class=\"sonata-tree\"{% endif %}>
        {% for element in collection if not root %}
            <li>
                <div class=\"sonata-treeitem\">
                    {% if element.parent %}<i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i>{% endif %}
                    <i class=\"fa fa-code\" aria-hidden=\"true\"></i>
                    <a class=\"sonata-treeitemedit\" href=\"{{ admin.generateObjectUrl('edit', element) }}\">{{ element.name }}</a>
                    <i class=\"text-muted\">{{ element.description }}</i>
                    <a class=\"label label-default pull-right\" href=\"{{ admin.generateObjectUrl('edit', element) }}\">edit <i class=\"fa fa-magic\" aria-hidden=\"true\"></i></a>
                    {% if true %}<span class=\"label label-warning pull-right\">true</span>{% endif %}
                </div>

                {% if element.children|length %}
                    {{ self.navigatechild(element.children, admin, false) }}
                {% endif %}
            </li>
        {% endfor %}
    </ul>
{% endmacro %}

{% block tabmenu %}
    {% include 'SonataAdminBundle:CRUD:listtabmenu.html.twig' with {
        'mode':   'tree',
        'action': action,
        'admin':  admin,
    } only %}
{% endblock %}

{% block listtable %}
    <div class=\"col-xs-12 col-md-12\">
        <div class=\"box box-primary\">
            <div class=\"box-header\">
                <h1 class=\"box-title\">
                    {{ 'element.treesitelabel'|trans({}, admin.translationdomain) }}
                    <div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                            <strong class=\"text-info\">{{ currentSite.name }}</strong> <span class=\"caret\"></span>
                        </button>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                            {% for context in contexts %}
                                <li>
                                    <a href=\"{{ admin.generateUrl('tree', { 'context': context.id }) }}\">
                                        {% if currentContext and context.id == currentContext.id %}
                                            <span class=\"pull-right\">
                                                <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
                                            </span>
                                        {% endif %}
                                        {{ site.name }}
                                    </a>
                                </li>
                            {% endfor%}
                        </ul>
                    </div>
                </h1>
            </div>
            <div class=\"box-content\">
                {{ tree.navigatechild(collection, admin, true) }}
            </div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:tree.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/tree.html.twig");
    }
}
