<?php

/* @WebProfiler/Icon/close.svg */
class TwigTemplate632fa43a72380154802b47d410a43e8492a22086b54cb131da4f767ae10a9fa0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal31c5fd16762027cae6650b10562bcd98c83ae68c5b15b257a85c966b69fcb485 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal31c5fd16762027cae6650b10562bcd98c83ae68c5b15b257a85c966b69fcb485->enter($internal31c5fd16762027cae6650b10562bcd98c83ae68c5b15b257a85c966b69fcb485prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        $internal83c5adddc876ed8524b203507c0917990e0edf854a75a4efbf9b478aa6a4f313 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal83c5adddc876ed8524b203507c0917990e0edf854a75a4efbf9b478aa6a4f313->enter($internal83c5adddc876ed8524b203507c0917990e0edf854a75a4efbf9b478aa6a4f313prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/close.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
";
        
        $internal31c5fd16762027cae6650b10562bcd98c83ae68c5b15b257a85c966b69fcb485->leave($internal31c5fd16762027cae6650b10562bcd98c83ae68c5b15b257a85c966b69fcb485prof);

        
        $internal83c5adddc876ed8524b203507c0917990e0edf854a75a4efbf9b478aa6a4f313->leave($internal83c5adddc876ed8524b203507c0917990e0edf854a75a4efbf9b478aa6a4f313prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/close.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21.1,18.3c0.8,0.8,0.8,2,0,2.8c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6L12,14.8l-6.3,6.3
    c-0.4,0.4-0.9,0.6-1.4,0.6s-1-0.2-1.4-0.6c-0.8-0.8-0.8-2,0-2.8L9.2,12L2.9,5.7c-0.8-0.8-0.8-2,0-2.8c0.8-0.8,2-0.8,2.8,0L12,9.2
    l6.3-6.3c0.8-0.8,2-0.8,2.8,0c0.8,0.8,0.8,2,0,2.8L14.8,12L21.1,18.3z\"/>
</svg>
", "@WebProfiler/Icon/close.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/close.svg");
    }
}
