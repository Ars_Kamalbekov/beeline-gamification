<?php

/* FOSUserBundle:Group:showcontent.html.twig */
class TwigTemplate016857a7a11f3bf7eba27675c0f8784dd820ae207796899463a2344ccf826ec9 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal45ec9620cd6cce8d415a65c8dc23a45756ed64aa2bd6d77e54de223e9b332678 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal45ec9620cd6cce8d415a65c8dc23a45756ed64aa2bd6d77e54de223e9b332678->enter($internal45ec9620cd6cce8d415a65c8dc23a45756ed64aa2bd6d77e54de223e9b332678prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:showcontent.html.twig"));

        $internal18df3a5942db551eda1712849919836a46a03955975722b0914ad02445c3f6bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal18df3a5942db551eda1712849919836a46a03955975722b0914ad02445c3f6bf->enter($internal18df3a5942db551eda1712849919836a46a03955975722b0914ad02445c3f6bfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:showcontent.html.twig"));

        // line 2
        echo "
<div class=\"fosusergroupshow\">
    <p>";
        // line 4
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["group"]) || arraykeyexists("group", $context) ? $context["group"] : (function () { throw new TwigErrorRuntime('Variable "group" does not exist.', 4, $this->getSourceContext()); })()), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $internal45ec9620cd6cce8d415a65c8dc23a45756ed64aa2bd6d77e54de223e9b332678->leave($internal45ec9620cd6cce8d415a65c8dc23a45756ed64aa2bd6d77e54de223e9b332678prof);

        
        $internal18df3a5942db551eda1712849919836a46a03955975722b0914ad02445c3f6bf->leave($internal18df3a5942db551eda1712849919836a46a03955975722b0914ad02445c3f6bfprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:showcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

<div class=\"fosusergroupshow\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:showcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/showcontent.html.twig");
    }
}
