<?php

/* TwigBundle:Exception:exception.xml.twig */
class TwigTemplatea9dffbf344e6fe5704fc64901f5951339359843be94fd1c7caa63d50287f0c95 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7105102db1e45304d4aa517cae3c8b0162fd549b902b4c291ed8d6e4c4079c6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7105102db1e45304d4aa517cae3c8b0162fd549b902b4c291ed8d6e4c4079c6c->enter($internal7105102db1e45304d4aa517cae3c8b0162fd549b902b4c291ed8d6e4c4079c6cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.xml.twig"));

        $internald6e2b845cee21bb38757c565928c70a200e806cdc12f3cb9c6d1a15581e0a7ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald6e2b845cee21bb38757c565928c70a200e806cdc12f3cb9c6d1a15581e0a7ae->enter($internald6e2b845cee21bb38757c565928c70a200e806cdc12f3cb9c6d1a15581e0a7aeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twigescapefilter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" message=\"";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
";
        // line 4
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 4, $this->getSourceContext()); })()), "toarray", array()));
        foreach ($context['seq'] as $context["key"] => $context["e"]) {
            // line 5
            echo "    <exception class=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["e"], "class", array()), "html", null, true);
            echo "\" message=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["e"], "message", array()), "html", null, true);
            echo "\">
";
            // line 6
            echo twiginclude($this->env, $context, "@Twig/Exception/traces.xml.twig", array("exception" => $context["e"]), false);
            echo "
    </exception>
";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['e'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 9
        echo "</error>
";
        
        $internal7105102db1e45304d4aa517cae3c8b0162fd549b902b4c291ed8d6e4c4079c6c->leave($internal7105102db1e45304d4aa517cae3c8b0162fd549b902b4c291ed8d6e4c4079c6cprof);

        
        $internald6e2b845cee21bb38757c565928c70a200e806cdc12f3cb9c6d1a15581e0a7ae->leave($internald6e2b845cee21bb38757c565928c70a200e806cdc12f3cb9c6d1a15581e0a7aeprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 9,  48 => 6,  41 => 5,  37 => 4,  31 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?xml version=\"1.0\" encoding=\"{{ charset }}\" ?>

<error code=\"{{ statuscode }}\" message=\"{{ statustext }}\">
{% for e in exception.toarray %}
    <exception class=\"{{ e.class }}\" message=\"{{ e.message }}\">
{{ include('@Twig/Exception/traces.xml.twig', { exception: e }, withcontext = false) }}
    </exception>
{% endfor %}
</error>
", "TwigBundle:Exception:exception.xml.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.xml.twig");
    }
}
