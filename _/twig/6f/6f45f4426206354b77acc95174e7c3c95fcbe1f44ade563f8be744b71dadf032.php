<?php

/* @Framework/Form/passwordwidget.html.php */
class TwigTemplate66938d0136d624f93f92d03ed4a8f8cd0aa466721f59bbed1885fd7a51b7b02b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb3cddbb956a21043429cc7e9cacc68707906a6615514805161f61f63a4e43a89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb3cddbb956a21043429cc7e9cacc68707906a6615514805161f61f63a4e43a89->enter($internalb3cddbb956a21043429cc7e9cacc68707906a6615514805161f61f63a4e43a89prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/passwordwidget.html.php"));

        $internal3e200abc9bad51373535fab01b889e5cd0198aac8da0ae0bf4b7737efb1839ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3e200abc9bad51373535fab01b889e5cd0198aac8da0ae0bf4b7737efb1839ee->enter($internal3e200abc9bad51373535fab01b889e5cd0198aac8da0ae0bf4b7737efb1839eeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/passwordwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $internalb3cddbb956a21043429cc7e9cacc68707906a6615514805161f61f63a4e43a89->leave($internalb3cddbb956a21043429cc7e9cacc68707906a6615514805161f61f63a4e43a89prof);

        
        $internal3e200abc9bad51373535fab01b889e5cd0198aac8da0ae0bf4b7737efb1839ee->leave($internal3e200abc9bad51373535fab01b889e5cd0198aac8da0ae0bf4b7737efb1839eeprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/passwordwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/passwordwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/passwordwidget.html.php");
    }
}
