<?php

/* SonataAdminBundle:CRUD/Association:editonetomanyinlinetable.html.twig */
class TwigTemplatee7c1b2e1aca6f0e99af17af529d3aa1cc7ab52481e05b3e18376baed06e38428 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb934aad18abfd3785dc4e54fe58142dae1531b53683a1f2715077b19cdcf6890 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb934aad18abfd3785dc4e54fe58142dae1531b53683a1f2715077b19cdcf6890->enter($internalb934aad18abfd3785dc4e54fe58142dae1531b53683a1f2715077b19cdcf6890prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetable.html.twig"));

        $internal41f5925c4d195aa7ec7381b08e2823eb5348b16ae897a15961e8587e03b3118d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal41f5925c4d195aa7ec7381b08e2823eb5348b16ae897a15961e8587e03b3118d->enter($internal41f5925c4d195aa7ec7381b08e2823eb5348b16ae897a15961e8587e03b3118dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetable.html.twig"));

        // line 11
        echo "<table class=\"table table-bordered\">
    <thead>
        <tr>
            ";
        // line 14
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twigfirst($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 14, $this->getSourceContext()); })()), "children", array())), "children", array()));
        foreach ($context['seq'] as $context["fieldname"] => $context["nestedfield"]) {
            // line 15
            echo "                ";
            if (($context["fieldname"] == "delete")) {
                // line 16
                echo "                    <th>";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actiondelete", array(), "SonataAdminBundle"), "html", null, true);
                echo "</th>
                ";
            } else {
                // line 18
                echo "                    <th
                        ";
                // line 19
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "required", array(), "array")) {
                    // line 20
                    echo "                            class=\"required\"
                        ";
                }
                // line 22
                echo "                        ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array(), "any", false, true), "attr", array(), "array", false, true), "hidden", array(), "array", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "attr", array(), "array"), "hidden", array(), "array"))) {
                    // line 23
                    echo "                            style=\"display:none;\"
                        ";
                }
                // line 25
                echo "                    >
                        ";
                // line 26
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array(), "any", false, true), "sonataadmin", array(), "array", false, true), "admin", array(), "any", false, true), "translationDomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array(), "any", false, true), "sonataadmin", array(), "array", false, true), "admin", array(), "any", false, true), "translationDomain", array()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                 // line 27
$context["nestedfield"], "vars", array()), "translationdomain", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "translationdomain", array())))), "html", null, true);
                // line 28
                echo "
                    </th>
                ";
            }
            // line 31
            echo "            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['fieldname'], $context['nestedfield'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 32
        echo "        </tr>
    </thead>
    <tbody class=\"sonata-ba-tbody\">
        ";
        // line 35
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 35, $this->getSourceContext()); })()), "children", array()));
        foreach ($context['seq'] as $context["nestedgroupfieldname"] => $context["nestedgroupfield"]) {
            // line 36
            echo "            <tr>
                ";
            // line 37
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "children", array()));
            foreach ($context['seq'] as $context["fieldname"] => $context["nestedfield"]) {
                // line 38
                echo "                    <td class=\"
                        sonata-ba-td-";
                // line 39
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
                echo "-";
                echo twigescapefilter($this->env, $context["fieldname"], "html", null, true);
                echo "
                        control-group
                        ";
                // line 41
                if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "errors", array())) > 0)) {
                    echo " error sonata-ba-field-error";
                }
                // line 42
                echo "                        \"
                        ";
                // line 43
                if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array(), "any", false, true), "attr", array(), "array", false, true), "hidden", array(), "array", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "attr", array(), "array"), "hidden", array(), "array"))) {
                    // line 44
                    echo "                            style=\"display:none;\"
                        ";
                }
                // line 46
                echo "                    >
                        ";
                // line 47
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "associationadmin", array(), "any", false, true), "formfielddescriptions", array(), "any", false, true), $context["fieldname"], array(), "array", true, true)) {
                    // line 48
                    echo "                            ";
                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'widget');
                    echo "

                            ";
                    // line 50
                    $context["dummy"] = twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "setrendered", array());
                    // line 51
                    echo "                        ";
                } else {
                    // line 52
                    echo "                            ";
                    if (($context["fieldname"] == "delete")) {
                        // line 53
                        echo "                                ";
                        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'widget', array("label" => false));
                        echo "
                            ";
                    } else {
                        // line 55
                        echo "                                ";
                        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'widget');
                        echo "
                            ";
                    }
                    // line 57
                    echo "                        ";
                }
                // line 58
                echo "                        ";
                if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedfield"], "vars", array()), "errors", array())) > 0)) {
                    // line 59
                    echo "                            <div class=\"help-inline sonata-ba-field-error-messages\">
                                ";
                    // line 60
                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["nestedfield"], 'errors');
                    echo "
                            </div>
                        ";
                }
                // line 63
                echo "                    </td>
                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['fieldname'], $context['nestedfield'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 65
            echo "            </tr>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['nestedgroupfieldname'], $context['nestedgroupfield'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 67
        echo "    </tbody>
</table>
";
        
        $internalb934aad18abfd3785dc4e54fe58142dae1531b53683a1f2715077b19cdcf6890->leave($internalb934aad18abfd3785dc4e54fe58142dae1531b53683a1f2715077b19cdcf6890prof);

        
        $internal41f5925c4d195aa7ec7381b08e2823eb5348b16ae897a15961e8587e03b3118d->leave($internal41f5925c4d195aa7ec7381b08e2823eb5348b16ae897a15961e8587e03b3118dprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editonetomanyinlinetable.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 67,  168 => 65,  161 => 63,  155 => 60,  152 => 59,  149 => 58,  146 => 57,  140 => 55,  134 => 53,  131 => 52,  128 => 51,  126 => 50,  120 => 48,  118 => 47,  115 => 46,  111 => 44,  109 => 43,  106 => 42,  102 => 41,  95 => 39,  92 => 38,  88 => 37,  85 => 36,  81 => 35,  76 => 32,  70 => 31,  65 => 28,  63 => 27,  62 => 26,  59 => 25,  55 => 23,  52 => 22,  48 => 20,  46 => 19,  43 => 18,  37 => 16,  34 => 15,  30 => 14,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<table class=\"table table-bordered\">
    <thead>
        <tr>
            {% for fieldname, nestedfield in form.children|first.children %}
                {% if fieldname == 'delete' %}
                    <th>{{ 'actiondelete'|trans({}, 'SonataAdminBundle') }}</th>
                {% else %}
                    <th
                        {% if nestedfield.vars['required'] %}
                            class=\"required\"
                        {% endif %}
                        {% if (nestedfield.vars['attr']['hidden'] is defined) and (nestedfield.vars['attr']['hidden']) %}
                            style=\"display:none;\"
                        {% endif %}
                    >
                        {{ nestedfield.vars.label|trans({}, nestedfield.vars['sonataadmin'].admin.translationDomain
                            |default(nestedfield.vars.translationdomain)
                        ) }}
                    </th>
                {% endif %}
            {% endfor %}
        </tr>
    </thead>
    <tbody class=\"sonata-ba-tbody\">
        {% for nestedgroupfieldname, nestedgroupfield in form.children %}
            <tr>
                {% for fieldname, nestedfield in nestedgroupfield.children %}
                    <td class=\"
                        sonata-ba-td-{{ id }}-{{ fieldname  }}
                        control-group
                        {% if nestedfield.vars.errors|length > 0 %} error sonata-ba-field-error{% endif %}
                        \"
                        {% if (nestedfield.vars['attr']['hidden'] is defined) and (nestedfield.vars['attr']['hidden']) %}
                            style=\"display:none;\"
                        {% endif %}
                    >
                        {% if sonataadmin.fielddescription.associationadmin.formfielddescriptions[fieldname] is defined %}
                            {{ formwidget(nestedfield) }}

                            {% set dummy = nestedgroupfield.setrendered %}
                        {% else %}
                            {% if fieldname == 'delete' %}
                                {{ formwidget(nestedfield, { label: false }) }}
                            {% else %}
                                {{ formwidget(nestedfield) }}
                            {% endif %}
                        {% endif %}
                        {% if nestedfield.vars.errors|length > 0 %}
                            <div class=\"help-inline sonata-ba-field-error-messages\">
                                {{ formerrors(nestedfield) }}
                            </div>
                        {% endif %}
                    </td>
                {% endfor %}
            </tr>
        {% endfor %}
    </tbody>
</table>
", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetable.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editonetomanyinlinetable.html.twig");
    }
}
