<?php

/* SonataAdminBundle:CRUD:preview.html.twig */
class TwigTemplate778a97e6feeab2b9346977620ecd4b99eaee27a61edcb1444d9729bf23eac9d1 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:edit.html.twig", "SonataAdminBundle:CRUD:preview.html.twig", 12);
        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'sidemenu' => array($this, 'blocksidemenu'),
            'formactions' => array($this, 'blockformactions'),
            'preview' => array($this, 'blockpreview'),
            'form' => array($this, 'blockform'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7bb652544d346e3dbf3a01c81bc7c11e8a93c5e2d7c06d9f93ca49aae88bf031 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7bb652544d346e3dbf3a01c81bc7c11e8a93c5e2d7c06d9f93ca49aae88bf031->enter($internal7bb652544d346e3dbf3a01c81bc7c11e8a93c5e2d7c06d9f93ca49aae88bf031prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $internal77595acc4d84cd050a9c60032a368a532a4b885048d998da4c59be0175046526 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal77595acc4d84cd050a9c60032a368a532a4b885048d998da4c59be0175046526->enter($internal77595acc4d84cd050a9c60032a368a532a4b885048d998da4c59be0175046526prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal7bb652544d346e3dbf3a01c81bc7c11e8a93c5e2d7c06d9f93ca49aae88bf031->leave($internal7bb652544d346e3dbf3a01c81bc7c11e8a93c5e2d7c06d9f93ca49aae88bf031prof);

        
        $internal77595acc4d84cd050a9c60032a368a532a4b885048d998da4c59be0175046526->leave($internal77595acc4d84cd050a9c60032a368a532a4b885048d998da4c59be0175046526prof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internal0baa66cd02644285cae9105684471f2f1b45c66c41aeee3a97380687a53b8096 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0baa66cd02644285cae9105684471f2f1b45c66c41aeee3a97380687a53b8096->enter($internal0baa66cd02644285cae9105684471f2f1b45c66c41aeee3a97380687a53b8096prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal25a01a33c9e19e5146abff1e87affcca698dc1074b1c9f9d1561adbc62010d1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal25a01a33c9e19e5146abff1e87affcca698dc1074b1c9f9d1561adbc62010d1b->enter($internal25a01a33c9e19e5146abff1e87affcca698dc1074b1c9f9d1561adbc62010d1bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        
        $internal25a01a33c9e19e5146abff1e87affcca698dc1074b1c9f9d1561adbc62010d1b->leave($internal25a01a33c9e19e5146abff1e87affcca698dc1074b1c9f9d1561adbc62010d1bprof);

        
        $internal0baa66cd02644285cae9105684471f2f1b45c66c41aeee3a97380687a53b8096->leave($internal0baa66cd02644285cae9105684471f2f1b45c66c41aeee3a97380687a53b8096prof);

    }

    // line 17
    public function blocksidemenu($context, array $blocks = array())
    {
        $internal3145b24a3735e67a37ba827ed580ecc57ec931b935dc1fcfe646797235df6fd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3145b24a3735e67a37ba827ed580ecc57ec931b935dc1fcfe646797235df6fd1->enter($internal3145b24a3735e67a37ba827ed580ecc57ec931b935dc1fcfe646797235df6fd1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidemenu"));

        $internald025a06c07f0fca5f28fa56888467f5aa8078c53d803387859b4580af529b8b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald025a06c07f0fca5f28fa56888467f5aa8078c53d803387859b4580af529b8b1->enter($internald025a06c07f0fca5f28fa56888467f5aa8078c53d803387859b4580af529b8b1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidemenu"));

        
        $internald025a06c07f0fca5f28fa56888467f5aa8078c53d803387859b4580af529b8b1->leave($internald025a06c07f0fca5f28fa56888467f5aa8078c53d803387859b4580af529b8b1prof);

        
        $internal3145b24a3735e67a37ba827ed580ecc57ec931b935dc1fcfe646797235df6fd1->leave($internal3145b24a3735e67a37ba827ed580ecc57ec931b935dc1fcfe646797235df6fd1prof);

    }

    // line 20
    public function blockformactions($context, array $blocks = array())
    {
        $internal1fc1ea6b2831fd15960bddcc6081361dbcb725ea494671dec0d6d22784e1d710 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1fc1ea6b2831fd15960bddcc6081361dbcb725ea494671dec0d6d22784e1d710->enter($internal1fc1ea6b2831fd15960bddcc6081361dbcb725ea494671dec0d6d22784e1d710prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formactions"));

        $internal394cc624b10443401bd33855dd3f24878c1adac72d8b2b3d0f08dfb055f09f65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal394cc624b10443401bd33855dd3f24878c1adac72d8b2b3d0f08dfb055f09f65->enter($internal394cc624b10443401bd33855dd3f24878c1adac72d8b2b3d0f08dfb055f09f65prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formactions"));

        // line 21
        echo "    <button class=\"btn btn-success\" type=\"submit\" name=\"btnpreviewapprove\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 23
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnpreviewapprove", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btnpreviewdecline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        ";
        // line 27
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnpreviewdecline", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
";
        
        $internal394cc624b10443401bd33855dd3f24878c1adac72d8b2b3d0f08dfb055f09f65->leave($internal394cc624b10443401bd33855dd3f24878c1adac72d8b2b3d0f08dfb055f09f65prof);

        
        $internal1fc1ea6b2831fd15960bddcc6081361dbcb725ea494671dec0d6d22784e1d710->leave($internal1fc1ea6b2831fd15960bddcc6081361dbcb725ea494671dec0d6d22784e1d710prof);

    }

    // line 31
    public function blockpreview($context, array $blocks = array())
    {
        $internal7fc11402fe78d2a14add67718db3749267f89890bacde5cf689612d40c074e75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7fc11402fe78d2a14add67718db3749267f89890bacde5cf689612d40c074e75->enter($internal7fc11402fe78d2a14add67718db3749267f89890bacde5cf689612d40c074e75prof = new TwigProfilerProfile($this->getTemplateName(), "block", "preview"));

        $internale48d883f7fd0495be3602b99de69da70dc649481aa1dfbfe80e40f785273e04b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale48d883f7fd0495be3602b99de69da70dc649481aa1dfbfe80e40f785273e04b->enter($internale48d883f7fd0495be3602b99de69da70dc649481aa1dfbfe80e40f785273e04bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "preview"));

        // line 32
        echo "    <div class=\"sonata-ba-view\">
        ";
        // line 33
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 33, $this->getSourceContext()); })()), "showgroups", array()));
        foreach ($context['seq'] as $context["name"] => $context["viewgroup"]) {
            // line 34
            echo "            <table class=\"table table-bordered\">
                ";
            // line 35
            if ($context["name"]) {
                // line 36
                echo "                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            ";
                // line 38
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["name"], array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "translationdomain", array())), "html", null, true);
                echo "
                        </td>
                    </tr>
                ";
            }
            // line 42
            echo "
                ";
            // line 43
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["viewgroup"], "fields", array()));
            foreach ($context['seq'] as $context["key"] => $context["fieldname"]) {
                // line 44
                echo "                    <tr class=\"sonata-ba-view-container\">
                        ";
                // line 45
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "show", array(), "any", false, true), $context["fieldname"], array(), "array", true, true)) {
                    // line 46
                    echo "                            ";
                    echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 46, $this->getSourceContext()); })()), "show", array()), $context["fieldname"], array(), "array"), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 46, $this->getSourceContext()); })()));
                    echo "
                        ";
                }
                // line 48
                echo "                    </tr>
                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['fieldname'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 50
            echo "            </table>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['name'], $context['viewgroup'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 52
        echo "    </div>
";
        
        $internale48d883f7fd0495be3602b99de69da70dc649481aa1dfbfe80e40f785273e04b->leave($internale48d883f7fd0495be3602b99de69da70dc649481aa1dfbfe80e40f785273e04bprof);

        
        $internal7fc11402fe78d2a14add67718db3749267f89890bacde5cf689612d40c074e75->leave($internal7fc11402fe78d2a14add67718db3749267f89890bacde5cf689612d40c074e75prof);

    }

    // line 55
    public function blockform($context, array $blocks = array())
    {
        $internald0cfcda771dc3196dfd85f63ea7cb090a7a9a73e62fa03ae9d78225db42835af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald0cfcda771dc3196dfd85f63ea7cb090a7a9a73e62fa03ae9d78225db42835af->enter($internald0cfcda771dc3196dfd85f63ea7cb090a7a9a73e62fa03ae9d78225db42835afprof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        $internal3e71aa6ca1dc663fc958ba8270f759c83e3a7bdd4658bbfb1c59bfdad41b8e6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3e71aa6ca1dc663fc958ba8270f759c83e3a7bdd4658bbfb1c59bfdad41b8e6b->enter($internal3e71aa6ca1dc663fc958ba8270f759c83e3a7bdd4658bbfb1c59bfdad41b8e6bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        // line 56
        echo "    <div class=\"sonata-preview-form-container\">
        ";
        // line 57
        $this->displayParentBlock("form", $context, $blocks);
        echo "
    </div>
";
        
        $internal3e71aa6ca1dc663fc958ba8270f759c83e3a7bdd4658bbfb1c59bfdad41b8e6b->leave($internal3e71aa6ca1dc663fc958ba8270f759c83e3a7bdd4658bbfb1c59bfdad41b8e6bprof);

        
        $internald0cfcda771dc3196dfd85f63ea7cb090a7a9a73e62fa03ae9d78225db42835af->leave($internald0cfcda771dc3196dfd85f63ea7cb090a7a9a73e62fa03ae9d78225db42835afprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 57,  195 => 56,  186 => 55,  175 => 52,  168 => 50,  161 => 48,  155 => 46,  153 => 45,  150 => 44,  146 => 43,  143 => 42,  136 => 38,  132 => 36,  130 => 35,  127 => 34,  123 => 33,  120 => 32,  111 => 31,  98 => 27,  91 => 23,  87 => 21,  78 => 20,  61 => 17,  44 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:edit.html.twig' %}

{% block actions %}
{% endblock %}

{% block sidemenu %}
{% endblock %}

{% block formactions %}
    <button class=\"btn btn-success\" type=\"submit\" name=\"btnpreviewapprove\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'btnpreviewapprove'|trans({}, 'SonataAdminBundle') }}
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btnpreviewdecline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        {{ 'btnpreviewdecline'|trans({}, 'SonataAdminBundle') }}
    </button>
{% endblock %}

{% block preview %}
    <div class=\"sonata-ba-view\">
        {% for name, viewgroup in admin.showgroups %}
            <table class=\"table table-bordered\">
                {% if name %}
                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            {{ name|trans({}, admin.translationdomain) }}
                        </td>
                    </tr>
                {% endif %}

                {% for fieldname in viewgroup.fields %}
                    <tr class=\"sonata-ba-view-container\">
                        {% if admin.show[fieldname] is defined %}
                            {{ admin.show[fieldname]|renderviewelement(object) }}
                        {% endif %}
                    </tr>
                {% endfor %}
            </table>
        {% endfor %}
    </div>
{% endblock %}

{% block form %}
    <div class=\"sonata-preview-form-container\">
        {{ parent() }}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:preview.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/preview.html.twig");
    }
}
