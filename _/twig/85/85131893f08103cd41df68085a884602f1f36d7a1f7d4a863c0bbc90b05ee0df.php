<?php

/* WebProfilerBundle:Profiler:toolbar.html.twig */
class TwigTemplate9823e37747c0b679157c48ef160f4271ef51f59189221b166318a737ae2790c8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6636b3051512e9bb862edecb888cf72c59d2095d43dd03d0ed33e0ee2a877128 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6636b3051512e9bb862edecb888cf72c59d2095d43dd03d0ed33e0ee2a877128->enter($internal6636b3051512e9bb862edecb888cf72c59d2095d43dd03d0ed33e0ee2a877128prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar.html.twig"));

        $internalb69d9b097a2fd167596b8970867f3ea7098043e0450f8118c900853546e362a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb69d9b097a2fd167596b8970867f3ea7098043e0450f8118c900853546e362a7->enter($internalb69d9b097a2fd167596b8970867f3ea7098043e0450f8118c900853546e362a7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar.html.twig"));

        // line 1
        echo "<!-- START of Symfony Web Debug Toolbar -->
<div id=\"sfMiniToolbar-";
        // line 2
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 2, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"sf-minitoolbar\" data-no-turbolink>
    <a href=\"#\" title=\"Show Symfony toolbar\" tabindex=\"-1\" id=\"sfToolbarMiniToggler-";
        // line 3
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" accesskey=\"D\">
        ";
        // line 4
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo "
    </a>
</div>
<div id=\"sfToolbarClearer-";
        // line 7
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"sf-toolbar-clearer\"></div>

<div id=\"sfToolbarMainContent-";
        // line 9
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"sf-toolbarreset clear-fix\" data-no-turbolink>
    ";
        // line 10
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["templates"]) || arraykeyexists("templates", $context) ? $context["templates"] : (function () { throw new TwigErrorRuntime('Variable "templates" does not exist.', 10, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["name"] => $context["template"]) {
            // line 11
            echo "        ";
            if (            $this->loadTemplate($context["template"], "WebProfilerBundle:Profiler:toolbar.html.twig", 11)->hasBlock("toolbar", $context)) {
                // line 12
                echo "            ";
                $internalb7781d910c63d3706dc0152a4692db074a65d13265c4ff6316691507f3445e3d = array("collector" => twiggetattribute($this->env, $this->getSourceContext(),                 // line 13
(isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 13, $this->getSourceContext()); })()), "getcollector", array(0 => $context["name"]), "method"), "profilerurl" =>                 // line 14
(isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 14, $this->getSourceContext()); })()), "token" => twiggetattribute($this->env, $this->getSourceContext(),                 // line 15
(isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 15, $this->getSourceContext()); })()), "token", array()), "name" =>                 // line 16
$context["name"], "profilermarkupversion" =>                 // line 17
(isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 17, $this->getSourceContext()); })()), "cspscriptnonce" =>                 // line 18
(isset($context["cspscriptnonce"]) || arraykeyexists("cspscriptnonce", $context) ? $context["cspscriptnonce"] : (function () { throw new TwigErrorRuntime('Variable "cspscriptnonce" does not exist.', 18, $this->getSourceContext()); })()), "cspstylenonce" =>                 // line 19
(isset($context["cspstylenonce"]) || arraykeyexists("cspstylenonce", $context) ? $context["cspstylenonce"] : (function () { throw new TwigErrorRuntime('Variable "cspstylenonce" does not exist.', 19, $this->getSourceContext()); })()));
                if (!isarray($internalb7781d910c63d3706dc0152a4692db074a65d13265c4ff6316691507f3445e3d)) {
                    throw new TwigErrorRuntime('Variables passed to the "with" tag must be a hash.');
                }
                $context['parent'] = $context;
                $context = arraymerge($context, $internalb7781d910c63d3706dc0152a4692db074a65d13265c4ff6316691507f3445e3d);
                // line 21
                echo "                ";
                $this->loadTemplate($context["template"], "WebProfilerBundle:Profiler:toolbar.html.twig", 21)->displayBlock("toolbar", $context);
                echo "
            ";
                $context = $context['parent'];
                // line 23
                echo "        ";
            }
            // line 24
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['name'], $context['template'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 25
        echo "
    <a class=\"hide-button\" id=\"sfToolbarHideButton-";
        // line 26
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 26, $this->getSourceContext()); })()), "html", null, true);
        echo "\" title=\"Close Toolbar\" tabindex=\"-1\" accesskey=\"D\">
        ";
        // line 27
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/close.svg");
        echo "
    </a>
</div>
<!-- END of Symfony Web Debug Toolbar -->
";
        
        $internal6636b3051512e9bb862edecb888cf72c59d2095d43dd03d0ed33e0ee2a877128->leave($internal6636b3051512e9bb862edecb888cf72c59d2095d43dd03d0ed33e0ee2a877128prof);

        
        $internalb69d9b097a2fd167596b8970867f3ea7098043e0450f8118c900853546e362a7->leave($internalb69d9b097a2fd167596b8970867f3ea7098043e0450f8118c900853546e362a7prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 27,  112 => 26,  109 => 25,  95 => 24,  92 => 23,  86 => 21,  79 => 19,  78 => 18,  77 => 17,  76 => 16,  75 => 15,  74 => 14,  73 => 13,  71 => 12,  68 => 11,  51 => 10,  47 => 9,  42 => 7,  36 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<!-- START of Symfony Web Debug Toolbar -->
<div id=\"sfMiniToolbar-{{ token }}\" class=\"sf-minitoolbar\" data-no-turbolink>
    <a href=\"#\" title=\"Show Symfony toolbar\" tabindex=\"-1\" id=\"sfToolbarMiniToggler-{{ token }}\" accesskey=\"D\">
        {{ include('@WebProfiler/Icon/symfony.svg') }}
    </a>
</div>
<div id=\"sfToolbarClearer-{{ token }}\" class=\"sf-toolbar-clearer\"></div>

<div id=\"sfToolbarMainContent-{{ token }}\" class=\"sf-toolbarreset clear-fix\" data-no-turbolink>
    {% for name, template in templates %}
        {% if block('toolbar', template) is defined %}
            {% with {
                collector: profile.getcollector(name),
                profilerurl: profilerurl,
                token: profile.token,
                name: name,
                profilermarkupversion: profilermarkupversion,
                cspscriptnonce: cspscriptnonce,
                cspstylenonce: cspstylenonce
              } %}
                {{ block('toolbar', template) }}
            {% endwith %}
        {% endif %}
    {% endfor %}

    <a class=\"hide-button\" id=\"sfToolbarHideButton-{{ token }}\" title=\"Close Toolbar\" tabindex=\"-1\" accesskey=\"D\">
        {{ include('@WebProfiler/Icon/close.svg') }}
    </a>
</div>
<!-- END of Symfony Web Debug Toolbar -->
", "WebProfilerBundle:Profiler:toolbar.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar.html.twig");
    }
}
