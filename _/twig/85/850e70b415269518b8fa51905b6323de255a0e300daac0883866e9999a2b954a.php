<?php

/* SonataAdminBundle:CRUD:baseaclmacro.html.twig */
class TwigTemplateb4540eeb57ad56038bda47862ff306fbc92f844a3606b0d01a1b9d2fde68f2e4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb04c444b4ddd37b83c2da86e70c6cfea4e122f8a8ab8ebc970db89f19a4c8c4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb04c444b4ddd37b83c2da86e70c6cfea4e122f8a8ab8ebc970db89f19a4c8c4e->enter($internalb04c444b4ddd37b83c2da86e70c6cfea4e122f8a8ab8ebc970db89f19a4c8c4eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseaclmacro.html.twig"));

        $internal400a0c3658bb91220c4190a23ad014b94f80794f775a6fa17460e76fb4982852 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal400a0c3658bb91220c4190a23ad014b94f80794f775a6fa17460e76fb4982852->enter($internal400a0c3658bb91220c4190a23ad014b94f80794f775a6fa17460e76fb4982852prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseaclmacro.html.twig"));

        // line 11
        echo "
";
        
        $internalb04c444b4ddd37b83c2da86e70c6cfea4e122f8a8ab8ebc970db89f19a4c8c4e->leave($internalb04c444b4ddd37b83c2da86e70c6cfea4e122f8a8ab8ebc970db89f19a4c8c4eprof);

        
        $internal400a0c3658bb91220c4190a23ad014b94f80794f775a6fa17460e76fb4982852->leave($internal400a0c3658bb91220c4190a23ad014b94f80794f775a6fa17460e76fb4982852prof);

    }

    // line 12
    public function macrorenderform($form = null, $permissions = null, $tdtype = null, $admin = null, $adminpool = null, $object = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "form" => $form,
            "permissions" => $permissions,
            "tdtype" => $tdtype,
            "admin" => $admin,
            "adminpool" => $adminpool,
            "object" => $object,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internalc48cace1a509f03f15b1a423016696b3462d54028bdc84830dd1258ae62bc4fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internalc48cace1a509f03f15b1a423016696b3462d54028bdc84830dd1258ae62bc4fe->enter($internalc48cace1a509f03f15b1a423016696b3462d54028bdc84830dd1258ae62bc4feprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderform"));

            $internal379a14c24413bd284a23bdc5a5f431b64a08bfd1ee81788f6fabce8172960e92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal379a14c24413bd284a23bdc5a5f431b64a08bfd1ee81788f6fabce8172960e92->enter($internal379a14c24413bd284a23bdc5a5f431b64a08bfd1ee81788f6fabce8172960e92prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderform"));

            // line 13
            echo "    <form class=\"form-horizontal\"
          action=\"";
            // line 14
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "generateUrl", array(0 => "acl", 1 => array("id" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 14, $this->getSourceContext()); })())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "uniqid", array()), "subclass" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 14, $this->getSourceContext()); })()), "request", array()), "get", array(0 => "subclass"), "method"))), "method"), "html", null, true);
            echo "\"
          ";
            // line 15
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 15, $this->getSourceContext()); })()), "vars", array()), "multipart", array())) {
                echo " enctype=\"multipart/form-data\"";
            }
            // line 16
            echo "          method=\"POST\"
            ";
            // line 17
            if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["adminpool"]) || arraykeyexists("adminpool", $context) ? $context["adminpool"] : (function () { throw new TwigErrorRuntime('Variable "adminpool" does not exist.', 17, $this->getSourceContext()); })()), "getOption", array(0 => "html5validate"), "method")) {
                echo "novalidate=\"novalidate\"";
            }
            // line 18
            echo "            >

        ";
            // line 20
            echo twiginclude($this->env, $context, "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig");
            echo "

        <div class=\"box box-success\">
            <div class=\"body table-responsive no-padding\">
                <table class=\"table\">
                    <colgroup>
                        <col style=\"width: 100%;\"/>
                        ";
            // line 27
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["permissions"]) || arraykeyexists("permissions", $context) ? $context["permissions"] : (function () { throw new TwigErrorRuntime('Variable "permissions" does not exist.', 27, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["permission"]) {
                // line 28
                echo "                            <col/>
                        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['permission'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 30
            echo "                    </colgroup>

                    ";
            // line 32
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 32, $this->getSourceContext()); })()), "children", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            foreach ($context['seq'] as $context["key"] => $context["child"]) {
                if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "vars", array()), "name", array()) != "token")) {
                    // line 33
                    echo "                        ";
                    if (((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()) == 0) || ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index0", array()) % 10) == 0))) {
                        // line 34
                        echo "                            <tr>
                                <th>";
                        // line 35
                        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["tdtype"]) || arraykeyexists("tdtype", $context) ? $context["tdtype"] : (function () { throw new TwigErrorRuntime('Variable "tdtype" does not exist.', 35, $this->getSourceContext()); })()), array(), "SonataAdminBundle"), "html", null, true);
                        echo "</th>
                                ";
                        // line 36
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable((isset($context["permissions"]) || arraykeyexists("permissions", $context) ? $context["permissions"] : (function () { throw new TwigErrorRuntime('Variable "permissions" does not exist.', 36, $this->getSourceContext()); })()));
                        foreach ($context['seq'] as $context["key"] => $context["permission"]) {
                            // line 37
                            echo "                                    <th class=\"text-right\">";
                            echo twigescapefilter($this->env, $context["permission"], "html", null, true);
                            echo "</th>
                                ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['key'], $context['permission'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 39
                        echo "                            </tr>
                        ";
                    }
                    // line 41
                    echo "
                        <tr>
                            <td>
                                ";
                    // line 44
                    $context["typeChild"] = ((twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "role", array(), "array", true, true)) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "role", array(), "array")) : (twiggetattribute($this->env, $this->getSourceContext(), $context["child"], "user", array(), "array")));
                    // line 45
                    echo "                                ";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["typeChild"]) || arraykeyexists("typeChild", $context) ? $context["typeChild"] : (function () { throw new TwigErrorRuntime('Variable "typeChild" does not exist.', 45, $this->getSourceContext()); })()), "vars", array()), "value", array()), "html", null, true);
                    echo "
                                ";
                    // line 46
                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["typeChild"]) || arraykeyexists("typeChild", $context) ? $context["typeChild"] : (function () { throw new TwigErrorRuntime('Variable "typeChild" does not exist.', 46, $this->getSourceContext()); })()), 'widget');
                    echo "
                            </td>
                            ";
                    // line 48
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable((isset($context["permissions"]) || arraykeyexists("permissions", $context) ? $context["permissions"] : (function () { throw new TwigErrorRuntime('Variable "permissions" does not exist.', 48, $this->getSourceContext()); })()));
                    foreach ($context['seq'] as $context["key"] => $context["permission"]) {
                        // line 49
                        echo "                                <td class=\"text-right\">";
                        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), $context["child"], $context["permission"], array(), "array"), 'widget', array("label" => false));
                        echo "</td>
                            ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['permission'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 51
                    echo "                        </tr>
                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 53
            echo "                </table>
            </div>
        </div>

        ";
            // line 57
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 57, $this->getSourceContext()); })()), "token", array()), 'row');
            echo "

        <div class=\"well well-small form-actions\">
            <input class=\"btn btn-primary\" type=\"submit\" name=\"btncreateandedit\" value=\"";
            // line 60
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnupdateacl", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
        </div>
    </form>
";
            
            $internal379a14c24413bd284a23bdc5a5f431b64a08bfd1ee81788f6fabce8172960e92->leave($internal379a14c24413bd284a23bdc5a5f431b64a08bfd1ee81788f6fabce8172960e92prof);

            
            $internalc48cace1a509f03f15b1a423016696b3462d54028bdc84830dd1258ae62bc4fe->leave($internalc48cace1a509f03f15b1a423016696b3462d54028bdc84830dd1258ae62bc4feprof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseaclmacro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 60,  191 => 57,  185 => 53,  174 => 51,  165 => 49,  161 => 48,  156 => 46,  151 => 45,  149 => 44,  144 => 41,  140 => 39,  131 => 37,  127 => 36,  123 => 35,  120 => 34,  117 => 33,  106 => 32,  102 => 30,  95 => 28,  91 => 27,  81 => 20,  77 => 18,  73 => 17,  70 => 16,  66 => 15,  62 => 14,  59 => 13,  36 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% macro renderform(form, permissions, tdtype, admin, adminpool, object) %}
    <form class=\"form-horizontal\"
          action=\"{{ admin.generateUrl('acl', {'id': admin.id(object), 'uniqid': admin.uniqid, 'subclass': app.request.get('subclass')}) }}\"
          {% if form.vars.multipart %} enctype=\"multipart/form-data\"{% endif %}
          method=\"POST\"
            {% if not adminpool.getOption('html5validate') %}novalidate=\"novalidate\"{% endif %}
            >

        {{ include('SonataAdminBundle:Helper:renderformdismissableerrors.html.twig') }}

        <div class=\"box box-success\">
            <div class=\"body table-responsive no-padding\">
                <table class=\"table\">
                    <colgroup>
                        <col style=\"width: 100%;\"/>
                        {% for permission in permissions %}
                            <col/>
                        {% endfor %}
                    </colgroup>

                    {% for child in form.children if child.vars.name != 'token' %}
                        {% if loop.index0 == 0 or loop.index0 % 10 == 0 %}
                            <tr>
                                <th>{{ tdtype|trans({}, 'SonataAdminBundle') }}</th>
                                {% for permission in permissions %}
                                    <th class=\"text-right\">{{ permission }}</th>
                                {% endfor %}
                            </tr>
                        {% endif %}

                        <tr>
                            <td>
                                {% set typeChild = child['role'] is defined ? child['role'] : child['user'] %}
                                {{ typeChild.vars.value }}
                                {{ formwidget(typeChild) }}
                            </td>
                            {% for permission in permissions %}
                                <td class=\"text-right\">{{ formwidget(child[permission], { label: false }) }}</td>
                            {% endfor %}
                        </tr>
                    {% endfor %}
                </table>
            </div>
        </div>

        {{ formrow(form.token) }}

        <div class=\"well well-small form-actions\">
            <input class=\"btn btn-primary\" type=\"submit\" name=\"btncreateandedit\" value=\"{{ 'btnupdateacl'|trans({}, 'SonataAdminBundle') }}\">
        </div>
    </form>
{% endmacro %}
", "SonataAdminBundle:CRUD:baseaclmacro.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseaclmacro.html.twig");
    }
}
