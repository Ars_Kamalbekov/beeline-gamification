<?php

/* @WebProfiler/Icon/search.svg */
class TwigTemplate08dcd8540b1dbce544e6a9a3e05af922b110167ae7a0e22c4f9dbb3b1bdec8d2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb37fe0873ed62a047ec8d58aaabb5a1b12d16b025cfb3751ce90a52ba59fc7f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb37fe0873ed62a047ec8d58aaabb5a1b12d16b025cfb3751ce90a52ba59fc7f5->enter($internalb37fe0873ed62a047ec8d58aaabb5a1b12d16b025cfb3751ce90a52ba59fc7f5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/search.svg"));

        $internal326ed37d07ee07d1ce1175ccc735548de4647c023f6a0360e05c60b03b4868aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal326ed37d07ee07d1ce1175ccc735548de4647c023f6a0360e05c60b03b4868aa->enter($internal326ed37d07ee07d1ce1175ccc735548de4647c023f6a0360e05c60b03b4868aaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/search.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M11.61,0.357c-4.4,0-7.98,3.58-7.98,7.98c0,1.696,0.526,3.308,1.524,4.679l-4.374,4.477
        c-0.238,0.238-0.369,0.554-0.369,0.891c0,0.336,0.131,0.653,0.369,0.891c0.238,0.238,0.554,0.369,0.891,0.369
        c0.336,0,0.653-0.131,0.893-0.371l4.372-4.475c1.369,0.996,2.98,1.521,4.674,1.521c4.4,0,7.98-3.58,7.98-7.98
        S16.01,0.357,11.61,0.357z M17.07,8.337c0,3.011-2.449,5.46-5.46,5.46c-3.011,0-5.46-2.449-5.46-5.46s2.449-5.46,5.46-5.46
        C14.62,2.877,17.07,5.326,17.07,8.337z\"/>
</svg>
";
        
        $internalb37fe0873ed62a047ec8d58aaabb5a1b12d16b025cfb3751ce90a52ba59fc7f5->leave($internalb37fe0873ed62a047ec8d58aaabb5a1b12d16b025cfb3751ce90a52ba59fc7f5prof);

        
        $internal326ed37d07ee07d1ce1175ccc735548de4647c023f6a0360e05c60b03b4868aa->leave($internal326ed37d07ee07d1ce1175ccc735548de4647c023f6a0360e05c60b03b4868aaprof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/search.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M11.61,0.357c-4.4,0-7.98,3.58-7.98,7.98c0,1.696,0.526,3.308,1.524,4.679l-4.374,4.477
        c-0.238,0.238-0.369,0.554-0.369,0.891c0,0.336,0.131,0.653,0.369,0.891c0.238,0.238,0.554,0.369,0.891,0.369
        c0.336,0,0.653-0.131,0.893-0.371l4.372-4.475c1.369,0.996,2.98,1.521,4.674,1.521c4.4,0,7.98-3.58,7.98-7.98
        S16.01,0.357,11.61,0.357z M17.07,8.337c0,3.011-2.449,5.46-5.46,5.46c-3.011,0-5.46-2.449-5.46-5.46s2.449-5.46,5.46-5.46
        C14.62,2.877,17.07,5.326,17.07,8.337z\"/>
</svg>
", "@WebProfiler/Icon/search.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/search.svg");
    }
}
