<?php

/* @Framework/Form/buttonwidget.html.php */
class TwigTemplatec52349fc25f29a579b05e48c491adc9b6c0da293564bc81f865a2656a424b4ba extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcd9bdb868339ee8b2475d1ea18395ab4272924966d706b464851487222736d18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcd9bdb868339ee8b2475d1ea18395ab4272924966d706b464851487222736d18->enter($internalcd9bdb868339ee8b2475d1ea18395ab4272924966d706b464851487222736d18prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonwidget.html.php"));

        $internale14a3c9ecdabdf48477f5e865a201009ed88d986cc8cb7dc27d021bc6088f8a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale14a3c9ecdabdf48477f5e865a201009ed88d986cc8cb7dc27d021bc6088f8a4->enter($internale14a3c9ecdabdf48477f5e865a201009ed88d986cc8cb7dc27d021bc6088f8a4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonwidget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$labelformat)
    ? strtr(\$labelformat, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'buttonattributes') ?>><?php echo \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$label, array(), \$translationdomain) : \$label) ?></button>
";
        
        $internalcd9bdb868339ee8b2475d1ea18395ab4272924966d706b464851487222736d18->leave($internalcd9bdb868339ee8b2475d1ea18395ab4272924966d706b464851487222736d18prof);

        
        $internale14a3c9ecdabdf48477f5e865a201009ed88d986cc8cb7dc27d021bc6088f8a4->leave($internale14a3c9ecdabdf48477f5e865a201009ed88d986cc8cb7dc27d021bc6088f8a4prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/buttonwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (!\$label) { \$label = isset(\$labelformat)
    ? strtr(\$labelformat, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'buttonattributes') ?>><?php echo \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$label, array(), \$translationdomain) : \$label) ?></button>
", "@Framework/Form/buttonwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/buttonwidget.html.php");
    }
}
