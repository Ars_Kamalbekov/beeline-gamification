<?php

/* @Twig/images/icon-minus-square-o.svg */
class TwigTemplatee212dc3599cc9129d5ed2f82f2a4e54dd90ef791229f9b3e3a2711eace5340f5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2d116dddcc6a2841f19705f48e23af85665508a14cc421730f53344ec717e0ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2d116dddcc6a2841f19705f48e23af85665508a14cc421730f53344ec717e0ee->enter($internal2d116dddcc6a2841f19705f48e23af85665508a14cc421730f53344ec717e0eeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        $internalc34de38c206e167ee1245318b1b3188e7a0f78eb47f7d91819b1c8c83aff9ea0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc34de38c206e167ee1245318b1b3188e7a0f78eb47f7d91819b1c8c83aff9ea0->enter($internalc34de38c206e167ee1245318b1b3188e7a0f78eb47f7d91819b1c8c83aff9ea0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $internal2d116dddcc6a2841f19705f48e23af85665508a14cc421730f53344ec717e0ee->leave($internal2d116dddcc6a2841f19705f48e23af85665508a14cc421730f53344ec717e0eeprof);

        
        $internalc34de38c206e167ee1245318b1b3188e7a0f78eb47f7d91819b1c8c83aff9ea0->leave($internalc34de38c206e167ee1245318b1b3188e7a0f78eb47f7d91819b1c8c83aff9ea0prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h832q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-minus-square-o.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square-o.svg");
    }
}
