<?php

/* SonataBlockBundle:Block:blockbase.html.twig */
class TwigTemplate8e60c00f1f7f2ba24bf843b31c4b6153716ff8fd4444004da94ee5c2558dbd82 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7cbc20c40ae31ed7eb650e9a431b1ecc1b03c76406cf9510f61d855b7d84db4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7cbc20c40ae31ed7eb650e9a431b1ecc1b03c76406cf9510f61d855b7d84db4c->enter($internal7cbc20c40ae31ed7eb650e9a431b1ecc1b03c76406cf9510f61d855b7d84db4cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockbase.html.twig"));

        $internal6489c07d475d00c7009ac4002123d09ffb7cb62aba686b730ea093bfa91bb42d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6489c07d475d00c7009ac4002123d09ffb7cb62aba686b730ea093bfa91bb42d->enter($internal6489c07d475d00c7009ac4002123d09ffb7cb62aba686b730ea093bfa91bb42dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockbase.html.twig"));

        // line 11
        echo "<div id=\"cms-block-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["block"]) || arraykeyexists("block", $context) ? $context["block"] : (function () { throw new TwigErrorRuntime('Variable "block" does not exist.', 11, $this->getSourceContext()); })()), "id", array()), "html", null, true);
        echo "\" class=\"cms-block cms-block-element\">
    ";
        // line 12
        $this->displayBlock('block', $context, $blocks);
        // line 13
        echo "</div>
";
        
        $internal7cbc20c40ae31ed7eb650e9a431b1ecc1b03c76406cf9510f61d855b7d84db4c->leave($internal7cbc20c40ae31ed7eb650e9a431b1ecc1b03c76406cf9510f61d855b7d84db4cprof);

        
        $internal6489c07d475d00c7009ac4002123d09ffb7cb62aba686b730ea093bfa91bb42d->leave($internal6489c07d475d00c7009ac4002123d09ffb7cb62aba686b730ea093bfa91bb42dprof);

    }

    // line 12
    public function blockblock($context, array $blocks = array())
    {
        $internalee0d73d52873e4cc849603c51c07e190848d632dcffe82d78dad7115b41812e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalee0d73d52873e4cc849603c51c07e190848d632dcffe82d78dad7115b41812e5->enter($internalee0d73d52873e4cc849603c51c07e190848d632dcffe82d78dad7115b41812e5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal883fcd8d99a5db1d2baaa88aef89be13a84d5615bb1678b474d2c1a57d69e0ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal883fcd8d99a5db1d2baaa88aef89be13a84d5615bb1678b474d2c1a57d69e0ee->enter($internal883fcd8d99a5db1d2baaa88aef89be13a84d5615bb1678b474d2c1a57d69e0eeprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        echo "EMPTY CONTENT";
        
        $internal883fcd8d99a5db1d2baaa88aef89be13a84d5615bb1678b474d2c1a57d69e0ee->leave($internal883fcd8d99a5db1d2baaa88aef89be13a84d5615bb1678b474d2c1a57d69e0eeprof);

        
        $internalee0d73d52873e4cc849603c51c07e190848d632dcffe82d78dad7115b41812e5->leave($internalee0d73d52873e4cc849603c51c07e190848d632dcffe82d78dad7115b41812e5prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockbase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 12,  33 => 13,  31 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<div id=\"cms-block-{{ block.id }}\" class=\"cms-block cms-block-element\">
    {% block block %}EMPTY CONTENT{% endblock %}
</div>
", "SonataBlockBundle:Block:blockbase.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockbase.html.twig");
    }
}
