<?php

/* FOSUserBundle:Group:editcontent.html.twig */
class TwigTemplatec8b15d8c72ce59f854f7ba87018c453ff840ee940ad7611cc42327bba0206ba1 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala697e8ef17a4d04dc0d44cf859bdabf322dbffa606e6514d9add96fcaa62be24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala697e8ef17a4d04dc0d44cf859bdabf322dbffa606e6514d9add96fcaa62be24->enter($internala697e8ef17a4d04dc0d44cf859bdabf322dbffa606e6514d9add96fcaa62be24prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:editcontent.html.twig"));

        $internalada3b3913d59c3a3f1073d3a138b1785903aefffabf59b920c65927e6cf3250b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalada3b3913d59c3a3f1073d3a138b1785903aefffabf59b920c65927e6cf3250b->enter($internalada3b3913d59c3a3f1073d3a138b1785903aefffabf59b920c65927e6cf3250bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:editcontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusergroupedit", array("groupName" => (isset($context["groupname"]) || arraykeyexists("groupname", $context) ? $context["groupname"] : (function () { throw new TwigErrorRuntime('Variable "groupname" does not exist.', 3, $this->getSourceContext()); })()))), "attr" => array("class" => "fosusergroupedit")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.edit.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internala697e8ef17a4d04dc0d44cf859bdabf322dbffa606e6514d9add96fcaa62be24->leave($internala697e8ef17a4d04dc0d44cf859bdabf322dbffa606e6514d9add96fcaa62be24prof);

        
        $internalada3b3913d59c3a3f1073d3a138b1785903aefffabf59b920c65927e6cf3250b->leave($internalada3b3913d59c3a3f1073d3a138b1785903aefffabf59b920c65927e6cf3250bprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:editcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, { 'action': path('fosusergroupedit', {'groupName': groupname}), 'attr': { 'class': 'fosusergroupedit' } }) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.edit.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:Group:editcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/editcontent.html.twig");
    }
}
