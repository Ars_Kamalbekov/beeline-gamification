<?php

/* SonataAdminBundle:CRUD:baseeditform.html.twig */
class TwigTemplate34ac7d8da295b822f2945c3f251ff224817560fc4b9874badf77ad700530378a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form' => array($this, 'blockform'),
            'sonataformactionurl' => array($this, 'blocksonataformactionurl'),
            'sonataformattributes' => array($this, 'blocksonataformattributes'),
            'sonataprefieldsets' => array($this, 'blocksonataprefieldsets'),
            'sonatatabcontent' => array($this, 'blocksonatatabcontent'),
            'sonatapostfieldsets' => array($this, 'blocksonatapostfieldsets'),
            'formactions' => array($this, 'blockformactions'),
            'sonataformactions' => array($this, 'blocksonataformactions'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2068a6106b89f03caa9bda3425d4cd8c65f652404e9823d34e7590c7d5db3787 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2068a6106b89f03caa9bda3425d4cd8c65f652404e9823d34e7590c7d5db3787->enter($internal2068a6106b89f03caa9bda3425d4cd8c65f652404e9823d34e7590c7d5db3787prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseeditform.html.twig"));

        $internal286e5dc84a7f1650481da493e391986c0ae8ef8489267b40ad8ed90da3e976eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal286e5dc84a7f1650481da493e391986c0ae8ef8489267b40ad8ed90da3e976eb->enter($internal286e5dc84a7f1650481da493e391986c0ae8ef8489267b40ad8ed90da3e976ebprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseeditform.html.twig"));

        // line 1
        $this->displayBlock('form', $context, $blocks);
        
        $internal2068a6106b89f03caa9bda3425d4cd8c65f652404e9823d34e7590c7d5db3787->leave($internal2068a6106b89f03caa9bda3425d4cd8c65f652404e9823d34e7590c7d5db3787prof);

        
        $internal286e5dc84a7f1650481da493e391986c0ae8ef8489267b40ad8ed90da3e976eb->leave($internal286e5dc84a7f1650481da493e391986c0ae8ef8489267b40ad8ed90da3e976ebprof);

    }

    public function blockform($context, array $blocks = array())
    {
        $internalc890eb56717c8eff272f6d2c54d3c9b6b05462833153b9497873eba980e9f3c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc890eb56717c8eff272f6d2c54d3c9b6b05462833153b9497873eba980e9f3c7->enter($internalc890eb56717c8eff272f6d2c54d3c9b6b05462833153b9497873eba980e9f3c7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        $internal33cd862ea493f147d4d4ed9f71d8f8c5f2450457b60d8d91033eaae365667217 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal33cd862ea493f147d4d4ed9f71d8f8c5f2450457b60d8d91033eaae365667217->enter($internal33cd862ea493f147d4d4ed9f71d8f8c5f2450457b60d8d91033eaae365667217prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        // line 2
        echo "    ";
        $context["formhelper"] = $this->loadTemplate("SonataAdminBundle:CRUD:baseeditformmacro.html.twig", "SonataAdminBundle:CRUD:baseeditform.html.twig", 2);
        // line 3
        echo "    ";
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.edit.form.top", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 3, $this->getSourceContext()); })()), "object" => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 3, $this->getSourceContext()); })()))));
        echo "

    ";
        // line 5
        $context["url"] = (( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 5, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 5, $this->getSourceContext()); })())), "method"))) ? ("edit") : ("create"));
        // line 6
        echo "
    ";
        // line 7
        if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 7, $this->getSourceContext()); })()), "hasRoute", array(0 => (isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 7, $this->getSourceContext()); })())), "method")) {
            // line 8
            echo "        <div>
            ";
            // line 9
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("formnotavailable", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </div>
    ";
        } else {
            // line 12
            echo "        <form
              ";
            // line 13
            if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 13, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "formtype"), "method") == "horizontal")) {
                echo "class=\"form-horizontal\"";
            }
            // line 14
            echo "              role=\"form\"
              action=\"";
            // line 15
            $this->displayBlock('sonataformactionurl', $context, $blocks);
            echo "\"
              ";
            // line 16
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), "vars", array()), "multipart", array())) {
                echo " enctype=\"multipart/form-data\"";
            }
            // line 17
            echo "              method=\"POST\"
              ";
            // line 18
            if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 18, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "html5validate"), "method")) {
                echo "novalidate=\"novalidate\"";
            }
            // line 19
            echo "              ";
            $this->displayBlock('sonataformattributes', $context, $blocks);
            // line 20
            echo "              >

            ";
            // line 22
            echo twiginclude($this->env, $context, "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig");
            echo "

            ";
            // line 24
            $this->displayBlock('sonataprefieldsets', $context, $blocks);
            // line 27
            echo "
            ";
            // line 28
            $this->displayBlock('sonatatabcontent', $context, $blocks);
            // line 60
            echo "
            ";
            // line 61
            $this->displayBlock('sonatapostfieldsets', $context, $blocks);
            // line 64
            echo "
            ";
            // line 65
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 65, $this->getSourceContext()); })()), 'rest');
            echo "

            ";
            // line 67
            $this->displayBlock('formactions', $context, $blocks);
            // line 111
            echo "        </form>
    ";
        }
        // line 113
        echo "
    ";
        // line 114
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.edit.form.bottom", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 114, $this->getSourceContext()); })()), "object" => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 114, $this->getSourceContext()); })()))));
        echo "

";
        
        $internal33cd862ea493f147d4d4ed9f71d8f8c5f2450457b60d8d91033eaae365667217->leave($internal33cd862ea493f147d4d4ed9f71d8f8c5f2450457b60d8d91033eaae365667217prof);

        
        $internalc890eb56717c8eff272f6d2c54d3c9b6b05462833153b9497873eba980e9f3c7->leave($internalc890eb56717c8eff272f6d2c54d3c9b6b05462833153b9497873eba980e9f3c7prof);

    }

    // line 15
    public function blocksonataformactionurl($context, array $blocks = array())
    {
        $internal75f889e9293ff9b087af0f2415cadfe8aa4c0b60114e06260588e4331c98aa66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal75f889e9293ff9b087af0f2415cadfe8aa4c0b60114e06260588e4331c98aa66->enter($internal75f889e9293ff9b087af0f2415cadfe8aa4c0b60114e06260588e4331c98aa66prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformactionurl"));

        $internal649ca0f826956924950ca8e1dc1cfee5d968547cb1fcc8cd4d4615c63dd562cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal649ca0f826956924950ca8e1dc1cfee5d968547cb1fcc8cd4d4615c63dd562cd->enter($internal649ca0f826956924950ca8e1dc1cfee5d968547cb1fcc8cd4d4615c63dd562cdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformactionurl"));

        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "generateUrl", array(0 => (isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 15, $this->getSourceContext()); })()), 1 => array("id" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "uniqid", array()), "subclass" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 15, $this->getSourceContext()); })()), "request", array()), "get", array(0 => "subclass"), "method"))), "method"), "html", null, true);
        
        $internal649ca0f826956924950ca8e1dc1cfee5d968547cb1fcc8cd4d4615c63dd562cd->leave($internal649ca0f826956924950ca8e1dc1cfee5d968547cb1fcc8cd4d4615c63dd562cdprof);

        
        $internal75f889e9293ff9b087af0f2415cadfe8aa4c0b60114e06260588e4331c98aa66->leave($internal75f889e9293ff9b087af0f2415cadfe8aa4c0b60114e06260588e4331c98aa66prof);

    }

    // line 19
    public function blocksonataformattributes($context, array $blocks = array())
    {
        $internalef20cf9d2b985f448b96d6e6f949952d3762b897318b183a66f95372a1847c87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalef20cf9d2b985f448b96d6e6f949952d3762b897318b183a66f95372a1847c87->enter($internalef20cf9d2b985f448b96d6e6f949952d3762b897318b183a66f95372a1847c87prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformattributes"));

        $internal05de7739fee53681b95f48adc7960e3ceb539a2ea56e75b51c4b27abf2dc728a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal05de7739fee53681b95f48adc7960e3ceb539a2ea56e75b51c4b27abf2dc728a->enter($internal05de7739fee53681b95f48adc7960e3ceb539a2ea56e75b51c4b27abf2dc728aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformattributes"));

        
        $internal05de7739fee53681b95f48adc7960e3ceb539a2ea56e75b51c4b27abf2dc728a->leave($internal05de7739fee53681b95f48adc7960e3ceb539a2ea56e75b51c4b27abf2dc728aprof);

        
        $internalef20cf9d2b985f448b96d6e6f949952d3762b897318b183a66f95372a1847c87->leave($internalef20cf9d2b985f448b96d6e6f949952d3762b897318b183a66f95372a1847c87prof);

    }

    // line 24
    public function blocksonataprefieldsets($context, array $blocks = array())
    {
        $internalee202b01a97acfe78cafff0f75ee9c09ef6ae6b40492b64ce1859ece5478d90d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalee202b01a97acfe78cafff0f75ee9c09ef6ae6b40492b64ce1859ece5478d90d->enter($internalee202b01a97acfe78cafff0f75ee9c09ef6ae6b40492b64ce1859ece5478d90dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataprefieldsets"));

        $internalc8945a5695202c5e5595c6826c831252832612f455c1fe89bc33a4efd643df1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc8945a5695202c5e5595c6826c831252832612f455c1fe89bc33a4efd643df1d->enter($internalc8945a5695202c5e5595c6826c831252832612f455c1fe89bc33a4efd643df1dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataprefieldsets"));

        // line 25
        echo "                <div class=\"row\">
            ";
        
        $internalc8945a5695202c5e5595c6826c831252832612f455c1fe89bc33a4efd643df1d->leave($internalc8945a5695202c5e5595c6826c831252832612f455c1fe89bc33a4efd643df1dprof);

        
        $internalee202b01a97acfe78cafff0f75ee9c09ef6ae6b40492b64ce1859ece5478d90d->leave($internalee202b01a97acfe78cafff0f75ee9c09ef6ae6b40492b64ce1859ece5478d90dprof);

    }

    // line 28
    public function blocksonatatabcontent($context, array $blocks = array())
    {
        $internal12ae1747a910eefe2d73971b5d6d27c24b1c7745d8af7719f6d91289557fe9de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal12ae1747a910eefe2d73971b5d6d27c24b1c7745d8af7719f6d91289557fe9de->enter($internal12ae1747a910eefe2d73971b5d6d27c24b1c7745d8af7719f6d91289557fe9deprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatabcontent"));

        $internal9dda5a54bbb9ca2bb6fc47e7185baa680c606991827257b328cef4a6a91129f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9dda5a54bbb9ca2bb6fc47e7185baa680c606991827257b328cef4a6a91129f2->enter($internal9dda5a54bbb9ca2bb6fc47e7185baa680c606991827257b328cef4a6a91129f2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatabcontent"));

        // line 29
        echo "                ";
        $context["hastab"] = (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 29, $this->getSourceContext()); })()), "formtabs", array())) == 1) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 29, $this->getSourceContext()); })()), "formtabs", array())), 0, array(), "array") != "default")) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 29, $this->getSourceContext()); })()), "formtabs", array())) > 1));
        // line 30
        echo "
                <div class=\"col-md-12\">
                    ";
        // line 32
        if ((isset($context["hastab"]) || arraykeyexists("hastab", $context) ? $context["hastab"] : (function () { throw new TwigErrorRuntime('Variable "hastab" does not exist.', 32, $this->getSourceContext()); })())) {
            // line 33
            echo "                        <div class=\"nav-tabs-custom\">
                            <ul class=\"nav nav-tabs\" role=\"tablist\">
                                ";
            // line 35
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 35, $this->getSourceContext()); })()), "formtabs", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["formtab"]) {
                // line 36
                echo "                                    <li";
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()) == 1)) {
                    echo " class=\"active\"";
                }
                echo "><a href=\"#tab";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\" data-toggle=\"tab\"><i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "translationdomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "translationdomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 36, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                echo "</a></li>
                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['formtab'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 38
            echo "                            </ul>
                            <div class=\"tab-content\">
                                ";
            // line 40
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 40, $this->getSourceContext()); })()), "formtabs", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["code"] => $context["formtab"]) {
                // line 41
                echo "                                    <div class=\"tab-pane fade";
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                    echo " in active";
                }
                echo "\" id=\"tab";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 41, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\">
                                        <div class=\"box-body  container-fluid\">
                                            <div class=\"sonata-ba-collapsed-fields\">
                                                ";
                // line 44
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "description", array()) != false)) {
                    // line 45
                    echo "                                                    <p>";
                    echo twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "description", array());
                    echo "</p>
                                                ";
                }
                // line 47
                echo "
                                                ";
                // line 48
                echo $context["formhelper"]->macrorendergroups((isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 48, $this->getSourceContext()); })()), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 48, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["formtab"], "groups", array(), "array"), (isset($context["hastab"]) || arraykeyexists("hastab", $context) ? $context["hastab"] : (function () { throw new TwigErrorRuntime('Variable "hastab" does not exist.', 48, $this->getSourceContext()); })()));
                echo "
                                            </div>
                                        </div>
                                    </div>
                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['code'], $context['formtab'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 53
            echo "                            </div>
                        </div>
                    ";
        } else {
            // line 56
            echo "                        ";
            echo $context["formhelper"]->macrorendergroups((isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 56, $this->getSourceContext()); })()), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 56, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 56, $this->getSourceContext()); })()), "formtabs", array()), "default", array(), "array"), "groups", array()), (isset($context["hastab"]) || arraykeyexists("hastab", $context) ? $context["hastab"] : (function () { throw new TwigErrorRuntime('Variable "hastab" does not exist.', 56, $this->getSourceContext()); })()));
            echo "
                    ";
        }
        // line 58
        echo "                </div>
            ";
        
        $internal9dda5a54bbb9ca2bb6fc47e7185baa680c606991827257b328cef4a6a91129f2->leave($internal9dda5a54bbb9ca2bb6fc47e7185baa680c606991827257b328cef4a6a91129f2prof);

        
        $internal12ae1747a910eefe2d73971b5d6d27c24b1c7745d8af7719f6d91289557fe9de->leave($internal12ae1747a910eefe2d73971b5d6d27c24b1c7745d8af7719f6d91289557fe9deprof);

    }

    // line 61
    public function blocksonatapostfieldsets($context, array $blocks = array())
    {
        $internale1d1b2f9fe14fefe4f6af6f06d7ef7443a80bdb4df29e87e083a2c7a09393ca6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale1d1b2f9fe14fefe4f6af6f06d7ef7443a80bdb4df29e87e083a2c7a09393ca6->enter($internale1d1b2f9fe14fefe4f6af6f06d7ef7443a80bdb4df29e87e083a2c7a09393ca6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapostfieldsets"));

        $internalb70f9751a43b2754385d5753827371438022fd0ff4208daecfe5c0cac6a35080 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb70f9751a43b2754385d5753827371438022fd0ff4208daecfe5c0cac6a35080->enter($internalb70f9751a43b2754385d5753827371438022fd0ff4208daecfe5c0cac6a35080prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapostfieldsets"));

        // line 62
        echo "                </div>
            ";
        
        $internalb70f9751a43b2754385d5753827371438022fd0ff4208daecfe5c0cac6a35080->leave($internalb70f9751a43b2754385d5753827371438022fd0ff4208daecfe5c0cac6a35080prof);

        
        $internale1d1b2f9fe14fefe4f6af6f06d7ef7443a80bdb4df29e87e083a2c7a09393ca6->leave($internale1d1b2f9fe14fefe4f6af6f06d7ef7443a80bdb4df29e87e083a2c7a09393ca6prof);

    }

    // line 67
    public function blockformactions($context, array $blocks = array())
    {
        $internala08458263f8b8b87ae18c2e535b233abde5f55264ecbcf791f04435d2ee0d1a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala08458263f8b8b87ae18c2e535b233abde5f55264ecbcf791f04435d2ee0d1a4->enter($internala08458263f8b8b87ae18c2e535b233abde5f55264ecbcf791f04435d2ee0d1a4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formactions"));

        $internal471bebd6dfa3fd32b5dce471232cff44505c7d0d6f5aaa713034132c29921893 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal471bebd6dfa3fd32b5dce471232cff44505c7d0d6f5aaa713034132c29921893->enter($internal471bebd6dfa3fd32b5dce471232cff44505c7d0d6f5aaa713034132c29921893prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formactions"));

        // line 68
        echo "                <div class=\"sonata-ba-form-actions well well-small form-actions\">
                ";
        // line 69
        $this->displayBlock('sonataformactions', $context, $blocks);
        // line 109
        echo "                </div>
            ";
        
        $internal471bebd6dfa3fd32b5dce471232cff44505c7d0d6f5aaa713034132c29921893->leave($internal471bebd6dfa3fd32b5dce471232cff44505c7d0d6f5aaa713034132c29921893prof);

        
        $internala08458263f8b8b87ae18c2e535b233abde5f55264ecbcf791f04435d2ee0d1a4->leave($internala08458263f8b8b87ae18c2e535b233abde5f55264ecbcf791f04435d2ee0d1a4prof);

    }

    // line 69
    public function blocksonataformactions($context, array $blocks = array())
    {
        $internal2f3cefb472ff20ef0dcf8d551ac03788ca7e944cafe8c0f37e7cf7246d5a5bda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2f3cefb472ff20ef0dcf8d551ac03788ca7e944cafe8c0f37e7cf7246d5a5bda->enter($internal2f3cefb472ff20ef0dcf8d551ac03788ca7e944cafe8c0f37e7cf7246d5a5bdaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformactions"));

        $internal83b2c2071669670d8123f23d2d18dd49d0f6d29f463214bc366182515ec00d34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal83b2c2071669670d8123f23d2d18dd49d0f6d29f463214bc366182515ec00d34->enter($internal83b2c2071669670d8123f23d2d18dd49d0f6d29f463214bc366182515ec00d34prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataformactions"));

        // line 70
        echo "                    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 70, $this->getSourceContext()); })()), "request", array()), "isxmlhttprequest", array())) {
            // line 71
            echo "                        ";
            if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 71, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 71, $this->getSourceContext()); })())), "method"))) {
                // line 72
                echo "                            <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdate\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnupdate", array(), "SonataAdminBundle"), "html", null, true);
                echo "</button>
                        ";
            } else {
                // line 74
                echo "                            <button type=\"submit\" class=\"btn btn-success\" name=\"btncreate\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btncreate", array(), "SonataAdminBundle"), "html", null, true);
                echo "</button>
                        ";
            }
            // line 76
            echo "                    ";
        } else {
            // line 77
            echo "                        ";
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 77, $this->getSourceContext()); })()), "supportsPreviewMode", array())) {
                // line 78
                echo "                            <button class=\"btn btn-info persist-preview\" name=\"btnpreview\" type=\"submit\">
                                <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
                                ";
                // line 80
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnpreview", array(), "SonataAdminBundle"), "html", null, true);
                echo "
                            </button>
                        ";
            }
            // line 83
            echo "                        ";
            if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 83, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 83, $this->getSourceContext()); })())), "method"))) {
                // line 84
                echo "                            <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdateandedit\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnupdateandeditagain", array(), "SonataAdminBundle"), "html", null, true);
                echo "</button>

                            ";
                // line 86
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 86, $this->getSourceContext()); })()), "hasRoute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 86, $this->getSourceContext()); })()), "hasAccess", array(0 => "list"), "method"))) {
                    // line 87
                    echo "                                <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdateandlist\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnupdateandreturntolist", array(), "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                            ";
                }
                // line 89
                echo "
                            ";
                // line 90
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 90, $this->getSourceContext()); })()), "hasRoute", array(0 => "delete"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 90, $this->getSourceContext()); })()), "hasAccess", array(0 => "delete", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 90, $this->getSourceContext()); })())), "method"))) {
                    // line 91
                    echo "                                ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("deleteor", array(), "SonataAdminBundle"), "html", null, true);
                    echo "
                                <a class=\"btn btn-danger\" href=\"";
                    // line 92
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 92, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 92, $this->getSourceContext()); })())), "method"), "html", null, true);
                    echo "\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i> ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkdelete", array(), "SonataAdminBundle"), "html", null, true);
                    echo "</a>
                            ";
                }
                // line 94
                echo "
                            ";
                // line 95
                if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 95, $this->getSourceContext()); })()), "isAclEnabled", array(), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 95, $this->getSourceContext()); })()), "hasRoute", array(0 => "acl"), "method")) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 95, $this->getSourceContext()); })()), "hasAccess", array(0 => "acl", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 95, $this->getSourceContext()); })())), "method"))) {
                    // line 96
                    echo "                                <a class=\"btn btn-info\" href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 96, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "acl", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 96, $this->getSourceContext()); })())), "method"), "html", null, true);
                    echo "\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i> ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkeditacl", array(), "SonataAdminBundle"), "html", null, true);
                    echo "</a>
                            ";
                }
                // line 98
                echo "                        ";
            } else {
                // line 99
                echo "                            ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 99, $this->getSourceContext()); })()), "hasroute", array(0 => "edit"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 99, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit"), "method"))) {
                    // line 100
                    echo "                                <button class=\"btn btn-success\" type=\"submit\" name=\"btncreateandedit\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btncreateandeditagain", array(), "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                            ";
                }
                // line 102
                echo "                            ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 102, $this->getSourceContext()); })()), "hasroute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 102, $this->getSourceContext()); })()), "hasAccess", array(0 => "list"), "method"))) {
                    // line 103
                    echo "                                <button type=\"submit\" class=\"btn btn-success\" name=\"btncreateandlist\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> ";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btncreateandreturntolist", array(), "SonataAdminBundle"), "html", null, true);
                    echo "</button>
                            ";
                }
                // line 105
                echo "                            <button class=\"btn btn-success\" type=\"submit\" name=\"btncreateandcreate\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btncreateandcreateanewone", array(), "SonataAdminBundle"), "html", null, true);
                echo "</button>
                        ";
            }
            // line 107
            echo "                    ";
        }
        // line 108
        echo "                ";
        
        $internal83b2c2071669670d8123f23d2d18dd49d0f6d29f463214bc366182515ec00d34->leave($internal83b2c2071669670d8123f23d2d18dd49d0f6d29f463214bc366182515ec00d34prof);

        
        $internal2f3cefb472ff20ef0dcf8d551ac03788ca7e944cafe8c0f37e7cf7246d5a5bda->leave($internal2f3cefb472ff20ef0dcf8d551ac03788ca7e944cafe8c0f37e7cf7246d5a5bdaprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseeditform.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  525 => 108,  522 => 107,  516 => 105,  510 => 103,  507 => 102,  501 => 100,  498 => 99,  495 => 98,  487 => 96,  485 => 95,  482 => 94,  475 => 92,  470 => 91,  468 => 90,  465 => 89,  459 => 87,  457 => 86,  451 => 84,  448 => 83,  442 => 80,  438 => 78,  435 => 77,  432 => 76,  426 => 74,  420 => 72,  417 => 71,  414 => 70,  405 => 69,  394 => 109,  392 => 69,  389 => 68,  380 => 67,  369 => 62,  360 => 61,  349 => 58,  343 => 56,  338 => 53,  319 => 48,  316 => 47,  310 => 45,  308 => 44,  295 => 41,  278 => 40,  274 => 38,  249 => 36,  232 => 35,  228 => 33,  226 => 32,  222 => 30,  219 => 29,  210 => 28,  199 => 25,  190 => 24,  173 => 19,  155 => 15,  142 => 114,  139 => 113,  135 => 111,  133 => 67,  128 => 65,  125 => 64,  123 => 61,  120 => 60,  118 => 28,  115 => 27,  113 => 24,  108 => 22,  104 => 20,  101 => 19,  97 => 18,  94 => 17,  90 => 16,  86 => 15,  83 => 14,  79 => 13,  76 => 12,  70 => 9,  67 => 8,  65 => 7,  62 => 6,  60 => 5,  54 => 3,  51 => 2,  33 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% block form %}
    {% import \"SonataAdminBundle:CRUD:baseeditformmacro.html.twig\" as formhelper %}
    {{ sonatablockrenderevent('sonata.admin.edit.form.top', { 'admin': admin, 'object': object }) }}

    {% set url = admin.id(object) is not null ? 'edit' : 'create' %}

    {% if not admin.hasRoute(url)%}
        <div>
            {{ \"formnotavailable\"|trans({}, \"SonataAdminBundle\") }}
        </div>
    {% else %}
        <form
              {% if sonataadmin.adminPool.getOption('formtype') == 'horizontal' %}class=\"form-horizontal\"{% endif %}
              role=\"form\"
              action=\"{% block sonataformactionurl %}{{ admin.generateUrl(url, {'id': admin.id(object), 'uniqid': admin.uniqid, 'subclass': app.request.get('subclass')}) }}{% endblock %}\"
              {% if form.vars.multipart %} enctype=\"multipart/form-data\"{% endif %}
              method=\"POST\"
              {% if not sonataadmin.adminPool.getOption('html5validate') %}novalidate=\"novalidate\"{% endif %}
              {% block sonataformattributes %}{% endblock %}
              >

            {{ include('SonataAdminBundle:Helper:renderformdismissableerrors.html.twig') }}

            {% block sonataprefieldsets %}
                <div class=\"row\">
            {% endblock %}

            {% block sonatatabcontent %}
                {% set hastab = ((admin.formtabs|length == 1 and admin.formtabs|keys[0] != 'default') or admin.formtabs|length > 1 ) %}

                <div class=\"col-md-12\">
                    {% if hastab %}
                        <div class=\"nav-tabs-custom\">
                            <ul class=\"nav nav-tabs\" role=\"tablist\">
                                {% for name, formtab in admin.formtabs %}
                                    <li{% if loop.index == 1 %} class=\"active\"{% endif %}><a href=\"#tab{{ admin.uniqid }}{{ loop.index }}\" data-toggle=\"tab\"><i class=\"fa fa-exclamation-circle has-errors hide\" aria-hidden=\"true\"></i> {{ formtab.label|trans({}, formtab.translationdomain ?: admin.translationDomain) }}</a></li>
                                {% endfor %}
                            </ul>
                            <div class=\"tab-content\">
                                {% for code, formtab in admin.formtabs %}
                                    <div class=\"tab-pane fade{% if loop.first %} in active{% endif %}\" id=\"tab{{ admin.uniqid }}{{ loop.index }}\">
                                        <div class=\"box-body  container-fluid\">
                                            <div class=\"sonata-ba-collapsed-fields\">
                                                {% if formtab.description != false %}
                                                    <p>{{ formtab.description|raw }}</p>
                                                {% endif %}

                                                {{ formhelper.rendergroups(admin, form, formtab['groups'], hastab) }}
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    {% else %}
                        {{ formhelper.rendergroups(admin, form, admin.formtabs['default'].groups, hastab) }}
                    {% endif %}
                </div>
            {% endblock %}

            {% block sonatapostfieldsets %}
                </div>
            {% endblock %}

            {{ formrest(form) }}

            {% block formactions %}
                <div class=\"sonata-ba-form-actions well well-small form-actions\">
                {% block sonataformactions %}
                    {% if app.request.isxmlhttprequest %}
                        {% if admin.id(object) is not null %}
                            <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdate\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> {{ 'btnupdate'|trans({}, 'SonataAdminBundle') }}</button>
                        {% else %}
                            <button type=\"submit\" class=\"btn btn-success\" name=\"btncreate\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i> {{ 'btncreate'|trans({}, 'SonataAdminBundle') }}</button>
                        {% endif %}
                    {% else %}
                        {% if admin.supportsPreviewMode %}
                            <button class=\"btn btn-info persist-preview\" name=\"btnpreview\" type=\"submit\">
                                <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
                                {{ 'btnpreview'|trans({}, 'SonataAdminBundle') }}
                            </button>
                        {% endif %}
                        {% if admin.id(object) is not null %}
                            <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdateandedit\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> {{ 'btnupdateandeditagain'|trans({}, 'SonataAdminBundle') }}</button>

                            {% if admin.hasRoute('list') and admin.hasAccess('list') %}
                                <button type=\"submit\" class=\"btn btn-success\" name=\"btnupdateandlist\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> {{ 'btnupdateandreturntolist'|trans({}, 'SonataAdminBundle') }}</button>
                            {% endif %}

                            {% if admin.hasRoute('delete') and admin.hasAccess('delete', object) %}
                                {{ 'deleteor'|trans({}, 'SonataAdminBundle') }}
                                <a class=\"btn btn-danger\" href=\"{{ admin.generateObjectUrl('delete', object) }}\"><i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i> {{ 'linkdelete'|trans({}, 'SonataAdminBundle') }}</a>
                            {% endif %}

                            {% if admin.isAclEnabled() and admin.hasRoute('acl') and admin.hasAccess('acl', object) %}
                                <a class=\"btn btn-info\" href=\"{{ admin.generateObjectUrl('acl', object) }}\"><i class=\"fa fa-users\" aria-hidden=\"true\"></i> {{ 'linkeditacl'|trans({}, 'SonataAdminBundle') }}</a>
                            {% endif %}
                        {% else %}
                            {% if admin.hasroute('edit') and admin.hasAccess('edit') %}
                                <button class=\"btn btn-success\" type=\"submit\" name=\"btncreateandedit\"><i class=\"fa fa-save\" aria-hidden=\"true\"></i> {{ 'btncreateandeditagain'|trans({}, 'SonataAdminBundle') }}</button>
                            {% endif %}
                            {% if admin.hasroute('list') and admin.hasAccess('list') %}
                                <button type=\"submit\" class=\"btn btn-success\" name=\"btncreateandlist\"><i class=\"fa fa-save\"></i> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> {{ 'btncreateandreturntolist'|trans({}, 'SonataAdminBundle') }}</button>
                            {% endif %}
                            <button class=\"btn btn-success\" type=\"submit\" name=\"btncreateandcreate\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i> {{ 'btncreateandcreateanewone'|trans({}, 'SonataAdminBundle') }}</button>
                        {% endif %}
                    {% endif %}
                {% endblock %}
                </div>
            {% endblock formactions %}
        </form>
    {% endif%}

    {{ sonatablockrenderevent('sonata.admin.edit.form.bottom', { 'admin': admin, 'object': object }) }}

{% endblock %}
", "SonataAdminBundle:CRUD:baseeditform.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseeditform.html.twig");
    }
}
