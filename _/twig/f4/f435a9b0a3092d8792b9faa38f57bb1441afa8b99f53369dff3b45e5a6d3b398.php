<?php

/* WebProfilerBundle:Collector:form.html.twig */
class TwigTemplatef6d07892d999bd0c39b77c72ec05c588a0dcd040c96976aed571a014c424a04f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:form.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'head' => array($this, 'blockhead'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald00c12443baacb4d0a01ca4a48a0fce42554697fc1adf42c160cdc0eddddf8d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald00c12443baacb4d0a01ca4a48a0fce42554697fc1adf42c160cdc0eddddf8d9->enter($internald00c12443baacb4d0a01ca4a48a0fce42554697fc1adf42c160cdc0eddddf8d9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:form.html.twig"));

        $internal1ed754e9ad880a5dda152c9ffa578f8f8bbfdc9b3c9f77f52f40e66ea8aa962f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1ed754e9ad880a5dda152c9ffa578f8f8bbfdc9b3c9f77f52f40e66ea8aa962f->enter($internal1ed754e9ad880a5dda152c9ffa578f8f8bbfdc9b3c9f77f52f40e66ea8aa962fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:form.html.twig"));

        // line 3
        $context["internalede47a63c9fb2bbbf220f1bed2bb1fb5f7c868ee360f6f872e8d4b40d0ef9a28"] = $this;
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald00c12443baacb4d0a01ca4a48a0fce42554697fc1adf42c160cdc0eddddf8d9->leave($internald00c12443baacb4d0a01ca4a48a0fce42554697fc1adf42c160cdc0eddddf8d9prof);

        
        $internal1ed754e9ad880a5dda152c9ffa578f8f8bbfdc9b3c9f77f52f40e66ea8aa962f->leave($internal1ed754e9ad880a5dda152c9ffa578f8f8bbfdc9b3c9f77f52f40e66ea8aa962fprof);

    }

    // line 5
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal83a6ef04d50091a2f5d062e5c1daada8e2b1d150d5d2dc219e1beef4e093e2d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal83a6ef04d50091a2f5d062e5c1daada8e2b1d150d5d2dc219e1beef4e093e2d8->enter($internal83a6ef04d50091a2f5d062e5c1daada8e2b1d150d5d2dc219e1beef4e093e2d8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalf88488a7c4acbb41cbdeb0ce7e68f1e99814789b83a7cc9f881829fa22e421d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf88488a7c4acbb41cbdeb0ce7e68f1e99814789b83a7cc9f881829fa22e421d4->enter($internalf88488a7c4acbb41cbdeb0ce7e68f1e99814789b83a7cc9f881829fa22e421d4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 6
        echo "    ";
        if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "data", array()), "nberrors", array()) > 0) || twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "data", array()), "forms", array())))) {
            // line 7
            echo "        ";
            $context["statuscolor"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 7, $this->getSourceContext()); })()), "data", array()), "nberrors", array())) ? ("red") : (""));
            // line 8
            echo "        ";
            obstart();
            // line 9
            echo "            ";
            echo twiginclude($this->env, $context, "@WebProfiler/Icon/form.svg");
            echo "
            <span class=\"sf-toolbar-value\">
                ";
            // line 11
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "data", array()), "nberrors", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "data", array()), "nberrors", array())) : (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "data", array()), "forms", array())))), "html", null, true);
            echo "
            </span>
        ";
            $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 14
            echo "
        ";
            // line 15
            obstart();
            // line 16
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Number of forms</b>
                <span class=\"sf-toolbar-status\">";
            // line 18
            echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "data", array()), "forms", array())), "html", null, true);
            echo "</span>
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Number of errors</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 22
            echo (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 22, $this->getSourceContext()); })()), "data", array()), "nberrors", array()) > 0)) ? ("red") : (""));
            echo "\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 22, $this->getSourceContext()); })()), "data", array()), "nberrors", array()), "html", null, true);
            echo "</span>
            </div>
        ";
            $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 25
            echo "
        ";
            // line 26
            echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 26, $this->getSourceContext()); })()), "status" => (isset($context["statuscolor"]) || arraykeyexists("statuscolor", $context) ? $context["statuscolor"] : (function () { throw new TwigErrorRuntime('Variable "statuscolor" does not exist.', 26, $this->getSourceContext()); })())));
            echo "
    ";
        }
        
        $internalf88488a7c4acbb41cbdeb0ce7e68f1e99814789b83a7cc9f881829fa22e421d4->leave($internalf88488a7c4acbb41cbdeb0ce7e68f1e99814789b83a7cc9f881829fa22e421d4prof);

        
        $internal83a6ef04d50091a2f5d062e5c1daada8e2b1d150d5d2dc219e1beef4e093e2d8->leave($internal83a6ef04d50091a2f5d062e5c1daada8e2b1d150d5d2dc219e1beef4e093e2d8prof);

    }

    // line 30
    public function blockmenu($context, array $blocks = array())
    {
        $internal11b47348a55736f01d11d40d173b3499625426523d1791c0662d65e28a59ed4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal11b47348a55736f01d11d40d173b3499625426523d1791c0662d65e28a59ed4e->enter($internal11b47348a55736f01d11d40d173b3499625426523d1791c0662d65e28a59ed4eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internal7264888b8cb3018f4ab0fbccff4509ca7e4c4cbbbfa6dae6481f1bb025e7b4b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7264888b8cb3018f4ab0fbccff4509ca7e4c4cbbbfa6dae6481f1bb025e7b4b6->enter($internal7264888b8cb3018f4ab0fbccff4509ca7e4c4cbbbfa6dae6481f1bb025e7b4b6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 31
        echo "    <span class=\"label label-status-";
        echo ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "data", array()), "nberrors", array())) ? ("error") : (""));
        echo " ";
        echo ((twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "data", array()), "forms", array()))) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">";
        // line 32
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/form.svg");
        echo "</span>
        <strong>Forms</strong>
        ";
        // line 34
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 34, $this->getSourceContext()); })()), "data", array()), "nberrors", array()) > 0)) {
            // line 35
            echo "            <span class=\"count\">
                <span>";
            // line 36
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 36, $this->getSourceContext()); })()), "data", array()), "nberrors", array()), "html", null, true);
            echo "</span>
            </span>
        ";
        }
        // line 39
        echo "    </span>
";
        
        $internal7264888b8cb3018f4ab0fbccff4509ca7e4c4cbbbfa6dae6481f1bb025e7b4b6->leave($internal7264888b8cb3018f4ab0fbccff4509ca7e4c4cbbbfa6dae6481f1bb025e7b4b6prof);

        
        $internal11b47348a55736f01d11d40d173b3499625426523d1791c0662d65e28a59ed4e->leave($internal11b47348a55736f01d11d40d173b3499625426523d1791c0662d65e28a59ed4eprof);

    }

    // line 42
    public function blockhead($context, array $blocks = array())
    {
        $internal5ae184eea0c91701397c1c34c27c4786dc3cf540def221d067d6bad12b8a1102 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5ae184eea0c91701397c1c34c27c4786dc3cf540def221d067d6bad12b8a1102->enter($internal5ae184eea0c91701397c1c34c27c4786dc3cf540def221d067d6bad12b8a1102prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        $internal6b453ae86f8035af7896f3c3255a62ffdc09270be912c2d89575b9c7c3d56119 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b453ae86f8035af7896f3c3255a62ffdc09270be912c2d89575b9c7c3d56119->enter($internal6b453ae86f8035af7896f3c3255a62ffdc09270be912c2d89575b9c7c3d56119prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        // line 43
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "

    <style>
        #tree-menu {
            float: left;
            padding-right: 10px;
            width: 230px;
        }
        #tree-menu ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        #tree-menu li {
            margin: 0;
            padding: 0;
            width: 100%;
        }
        #tree-menu .empty {
            border: 0;
            padding: 0;
        }
        #tree-details-container {
            border-left: 1px solid #DDD;
            margin-left: 250px;
            padding-left: 20px;
        }
        .tree-details {
            padding-bottom: 40px;
        }
        .tree-details h3 {
            font-size: 18px;
            position: relative;
        }

        .toggle-icon {
            display: inline-block;
            background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAgBAMAAADpp+X/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDgwx4LcKwAAAABVQTFRFAAAA////////////////ZmZm////bvjBwAAAAAV0Uk5TABZwsuCVEUjgAAAAAWJLR0QF+G/pxwAAAE1JREFUGNNjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBmwgTQgQGWgA7h2uIFwK+CWwp1BpHvYEqDuATEYkBlY3IOmBq6dCPcAAIT5Eg2IksjQAAAAAElFTkSuQmCC\") no-repeat top left #5eb5e0;
        }
        .closed .toggle-icon, .closed.toggle-icon {
            background-position: bottom left;
        }
        .toggle-icon.empty {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAZgBmAGYHukptAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhIf6CA40AAAAFRJREFUOMvtk7ENACEMA61vfx767MROWfO+AdGBHlNyTZrYUZRYDBII4NWE1pNdpFarfgLUbpDaBEgBYRiEVjsvDLa1l6O4Z3wkFWN+OfLKdpisOH/TlICzukmUJwAAAABJRU5ErkJggg==\");
        }

        .tree .tree-inner {
            cursor: pointer;
            padding: 5px 7px 5px 22px;
            position: relative;

        }
        .tree .toggle-button {
            /* provide a bigger clickable area than just 10x10px */
            width: 16px;
            height: 16px;
            margin-left: -18px;
        }
        .tree .toggle-icon {
            width: 10px;
            height: 10px;
            /* position the icon in the center of the clickable area */
            margin-left: 3px;
            margin-top: 3px;
            background-size: 10px 20px;
            background-color: #AAA;
        }
        .tree .toggle-icon.empty {
            width: 10px;
            height: 10px;
            position: absolute;
            top: 50%;
            margin-top: -5px;
            margin-left: -15px;
            background-size: 10px 10px;
        }
        .tree ul ul .tree-inner {
            padding-left: 37px;
        }
        .tree ul ul ul .tree-inner {
            padding-left: 52px;
        }
        .tree ul ul ul ul .tree-inner {
            padding-left: 67px;
        }
        .tree ul ul ul ul ul .tree-inner {
            padding-left: 82px;
        }
        .tree .tree-inner:hover {
            background: #dfdfdf;
        }
        .tree .tree-inner.active, .tree .tree-inner.active:hover {
            background: #E0E0E0;
            font-weight: bold;
        }
        .tree .tree-inner.active .toggle-icon, .tree .tree-inner:hover .toggle-icon, .tree .tree-inner.active:hover .toggle-icon {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAgBAMAAADpp+X/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhEYXWn+sAAAABhQTFRFAAAA39/f39/f39/f39/fZmZm39/f////gc3YPwAAAAV0Uk5TAAtAc6ZeVyCYAAAAAWJLR0QF+G/pxwAAAE1JREFUGNNjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBmwgXIgQGWgA7h2uIFwK+CWwp1BpHvYC6DuATEYkBlY3IOmBq6dCPcAADqLE4MnBi/fAAAAAElFTkSuQmCC\");
            background-color: #999;
        }
        .tree .tree-inner.active .toggle-icon.empty, .tree .tree-inner:hover .toggle-icon.empty, .tree .tree-inner.active:hover .toggle-icon.empty {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhoucSey4gAAABVQTFRFAAAA39/f39/f39/f39/fZmZm39/fD5Dx2AAAAAV0Uk5TAAtAc6ZeVyCYAAAAAWJLR0QF+G/pxwAAADJJREFUCNdjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBnIA3DtcAPhVsAthTkDAFOfBKW9C1iqAAAAAElFTkSuQmCC\");
        }
        .tree-details .toggle-icon {
            width: 16px;
            height: 16px;
            /* vertically center the button */
            position: absolute;
            top: 50%;
            margin-top: -9px;
            margin-left: 6px;
        }
        .badge-error {
            float: right;
            background: #B0413E;
            color: #FFF;
            padding: 1px 4px;
            font-size: 10px;
            font-weight: bold;
            vertical-align: middle;
        }
        .has-error {
            color: #B0413E;
        }
        .errors h3 {
            color: #B0413E;
        }
        .errors th {
            background: #B0413E;
            color: #FFF;
        }
        .errors .toggle-icon {
            background-color: #B0413E;
        }
        h3 a, h3 a:hover, h3 a:focus {
            color: inherit;
            text-decoration: inherit;
        }
    </style>
";
        
        $internal6b453ae86f8035af7896f3c3255a62ffdc09270be912c2d89575b9c7c3d56119->leave($internal6b453ae86f8035af7896f3c3255a62ffdc09270be912c2d89575b9c7c3d56119prof);

        
        $internal5ae184eea0c91701397c1c34c27c4786dc3cf540def221d067d6bad12b8a1102->leave($internal5ae184eea0c91701397c1c34c27c4786dc3cf540def221d067d6bad12b8a1102prof);

    }

    // line 183
    public function blockpanel($context, array $blocks = array())
    {
        $internal8a7279fdd23a223ddfcf699c2f8766d4946f27d1cf11442ac9336253606efb32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8a7279fdd23a223ddfcf699c2f8766d4946f27d1cf11442ac9336253606efb32->enter($internal8a7279fdd23a223ddfcf699c2f8766d4946f27d1cf11442ac9336253606efb32prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal05bc7cb2405d6976e8dba957fcb6564c885c99ced1ec04836954f4781a14d4d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal05bc7cb2405d6976e8dba957fcb6564c885c99ced1ec04836954f4781a14d4d6->enter($internal05bc7cb2405d6976e8dba957fcb6564c885c99ced1ec04836954f4781a14d4d6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 184
        echo "    <h2>Forms</h2>

    ";
        // line 186
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 186, $this->getSourceContext()); })()), "data", array()), "forms", array()))) {
            // line 187
            echo "        <div id=\"tree-menu\" class=\"tree\">
            <ul>
            ";
            // line 189
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 189, $this->getSourceContext()); })()), "data", array()), "forms", array()));
            foreach ($context['seq'] as $context["formName"] => $context["formData"]) {
                // line 190
                echo "                ";
                echo $context["internalede47a63c9fb2bbbf220f1bed2bb1fb5f7c868ee360f6f872e8d4b40d0ef9a28"]->macroformtreeentry($context["formName"], $context["formData"], true);
                echo "
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['formName'], $context['formData'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 192
            echo "            </ul>
        </div>

        <div id=\"tree-details-container\">
            ";
            // line 196
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 196, $this->getSourceContext()); })()), "data", array()), "forms", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["formName"] => $context["formData"]) {
                // line 197
                echo "                ";
                echo $context["internalede47a63c9fb2bbbf220f1bed2bb1fb5f7c868ee360f6f872e8d4b40d0ef9a28"]->macroformtreedetails($context["formName"], $context["formData"], twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 197, $this->getSourceContext()); })()), "data", array()), "formsbyhash", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array()));
                echo "
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['formName'], $context['formData'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 199
            echo "        </div>
    ";
        } else {
            // line 201
            echo "        <div class=\"empty\">
            <p>No forms were submitted for this request.</p>
        </div>
    ";
        }
        // line 205
        echo "
    <script>
    function Toggler(storage) {
        \"use strict\";

        var STORAGEKEY = 'sftoggledata',

            states = {},

            isCollapsed = function (button) {
                return Sfjs.hasClass(button, 'closed');
            },

            isExpanded = function (button) {
                return !isCollapsed(button);
            },

            expand = function (button) {
                var targetId = button.dataset.toggleTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Toggle target \" + targetId + \" does not exist\";
                }

                if (isCollapsed(button)) {
                    Sfjs.removeClass(button, 'closed');
                    Sfjs.removeClass(target, 'hidden');

                    states[targetId] = 1;
                    storage.setItem(STORAGEKEY, states);
                }
            },

            collapse = function (button) {
                var targetId = button.dataset.toggleTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Toggle target \" + targetId + \" does not exist\";
                }

                if (isExpanded(button)) {
                    Sfjs.addClass(button, 'closed');
                    Sfjs.addClass(target, 'hidden');

                    states[targetId] = 0;
                    storage.setItem(STORAGEKEY, states);
                }
            },

            toggle = function (button) {
                if (Sfjs.hasClass(button, 'closed')) {
                    expand(button);
                } else {
                    collapse(button);
                }
            },

            initButtons = function (buttons) {
                states = storage.getItem(STORAGEKEY, {});

                // must be an object, not an array or anything else
                // `typeof` returns \"object\" also for arrays, so the following
                // check must be done
                // see http://stackoverflow.com/questions/4775722/check-if-object-is-array
                if ('[object Object]' !== Object.prototype.toString.call(states)) {
                    states = {};
                }

                for (var i = 0, l = buttons.length; i < l; ++i) {
                    var targetId = buttons[i].dataset.toggleTargetId,
                        target = document.getElementById(targetId);

                    if (!target) {
                        throw \"Toggle target \" + targetId + \" does not exist\";
                    }

                    // correct the initial state of the button
                    if (Sfjs.hasClass(target, 'hidden')) {
                        Sfjs.addClass(buttons[i], 'closed');
                    }

                    // attach listener for expanding/collapsing the target
                    clickHandler(buttons[i], toggle);

                    if (states.hasOwnProperty(targetId)) {
                        // open or collapse based on stored data
                        if (0 === states[targetId]) {
                            collapse(buttons[i]);
                        } else {
                            expand(buttons[i]);
                        }
                    }
                }
            };

        return {
            initButtons: initButtons,

            toggle: toggle,

            isExpanded: isExpanded,

            isCollapsed: isCollapsed,

            expand: expand,

            collapse: collapse
        };
    }

    function JsonStorage(storage) {
        var setItem = function (key, data) {
                storage.setItem(key, JSON.stringify(data));
            },

            getItem = function (key, defaultValue) {
                var data = storage.getItem(key);

                if (null !== data) {
                    try {
                        return JSON.parse(data);
                    } catch(e) {
                    }
                }

                return defaultValue;
            };

        return {
            setItem: setItem,

            getItem: getItem
        };
    }

    function TabView() {
        \"use strict\";

        var activeTab = null,

            activeTarget = null,

            select = function (tab) {
                var targetId = tab.dataset.tabTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Tab target \" + targetId + \" does not exist\";
                }

                if (activeTab) {
                    Sfjs.removeClass(activeTab, 'active');
                }

                if (activeTarget) {
                    Sfjs.addClass(activeTarget, 'hidden');
                }

                Sfjs.addClass(tab, 'active');
                Sfjs.removeClass(target, 'hidden');

                activeTab = tab;
                activeTarget = target;
            },

            initTabs = function (tabs) {
                for (var i = 0, l = tabs.length; i < l; ++i) {
                    var targetId = tabs[i].dataset.tabTargetId,
                        target = document.getElementById(targetId);

                    if (!target) {
                        throw \"Tab target \" + targetId + \" does not exist\";
                    }

                    clickHandler(tabs[i], select);

                    Sfjs.addClass(target, 'hidden');
                }

                if (tabs.length > 0) {
                    select(tabs[0]);
                }
            };

        return {
            initTabs: initTabs,

            select: select
        };
    }

    var tabTarget = new TabView(),
        toggler = new Toggler(new JsonStorage(sessionStorage)),
        clickHandler = function (element, callback) {
            Sfjs.addEventListener(element, 'click', function (e) {
                if (!e) {
                    e = window.event;
                }

                callback(this);

                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                }

                e.stopPropagation();

                return false;
            });
        };

    tabTarget.initTabs(document.querySelectorAll('.tree .tree-inner'));
    toggler.initButtons(document.querySelectorAll('a.toggle-button'));
    </script>
";
        
        $internal05bc7cb2405d6976e8dba957fcb6564c885c99ced1ec04836954f4781a14d4d6->leave($internal05bc7cb2405d6976e8dba957fcb6564c885c99ced1ec04836954f4781a14d4d6prof);

        
        $internal8a7279fdd23a223ddfcf699c2f8766d4946f27d1cf11442ac9336253606efb32->leave($internal8a7279fdd23a223ddfcf699c2f8766d4946f27d1cf11442ac9336253606efb32prof);

    }

    // line 425
    public function macroformtreeentry($name = null, $data = null, $isroot = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "name" => $name,
            "data" => $data,
            "isroot" => $isroot,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal488575490f8d2ec878a9368d1caa24c662d4eb70606dd43caec6fb8b03abd943 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal488575490f8d2ec878a9368d1caa24c662d4eb70606dd43caec6fb8b03abd943->enter($internal488575490f8d2ec878a9368d1caa24c662d4eb70606dd43caec6fb8b03abd943prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "formtreeentry"));

            $internalb2ceecd3976fdc80e8921dea6eb8f9d342f7e4c9dfe3dcd9abad7252f237b340 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internalb2ceecd3976fdc80e8921dea6eb8f9d342f7e4c9dfe3dcd9abad7252f237b340->enter($internalb2ceecd3976fdc80e8921dea6eb8f9d342f7e4c9dfe3dcd9abad7252f237b340prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "formtreeentry"));

            // line 426
            echo "    ";
            $context["tree"] = $this;
            // line 427
            echo "    ";
            $context["haserror"] = (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "errors", array(), "any", true, true) && (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 427, $this->getSourceContext()); })()), "errors", array())) > 0));
            // line 428
            echo "    <li>
        <div class=\"tree-inner\" data-tab-target-id=\"";
            // line 429
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 429, $this->getSourceContext()); })()), "id", array()), "html", null, true);
            echo "-details\">
            ";
            // line 430
            if ((isset($context["haserror"]) || arraykeyexists("haserror", $context) ? $context["haserror"] : (function () { throw new TwigErrorRuntime('Variable "haserror" does not exist.', 430, $this->getSourceContext()); })())) {
                // line 431
                echo "                <div class=\"badge-error\">";
                echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 431, $this->getSourceContext()); })()), "errors", array())), "html", null, true);
                echo "</div>
            ";
            }
            // line 433
            echo "
            ";
            // line 434
            if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 434, $this->getSourceContext()); })()), "children", array()))) {
                // line 435
                echo "                <a class=\"toggle-button\" data-toggle-target-id=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 435, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-children\" href=\"#\"><span class=\"toggle-icon\"></span></a>
            ";
            } else {
                // line 437
                echo "                <div class=\"toggle-icon empty\"></div>
            ";
            }
            // line 439
            echo "
            <span ";
            // line 440
            if (((isset($context["haserror"]) || arraykeyexists("haserror", $context) ? $context["haserror"] : (function () { throw new TwigErrorRuntime('Variable "haserror" does not exist.', 440, $this->getSourceContext()); })()) || ((twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "haschildrenerror", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "haschildrenerror", array()), false)) : (false)))) {
                echo "class=\"has-error\"";
            }
            echo ">
                ";
            // line 441
            echo twigescapefilter($this->env, ((arraykeyexists("name", $context)) ? (twigdefaultfilter((isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 441, $this->getSourceContext()); })()), "(no name)")) : ("(no name)")), "html", null, true);
            echo "
            </span>
        </div>

        ";
            // line 445
            if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 445, $this->getSourceContext()); })()), "children", array()))) {
                // line 446
                echo "            <ul id=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 446, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-children\" ";
                if (( !(isset($context["isroot"]) || arraykeyexists("isroot", $context) ? $context["isroot"] : (function () { throw new TwigErrorRuntime('Variable "isroot" does not exist.', 446, $this->getSourceContext()); })()) &&  !((twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "haschildrenerror", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "haschildrenerror", array()), false)) : (false)))) {
                    echo "class=\"hidden\"";
                }
                echo ">
                ";
                // line 447
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 447, $this->getSourceContext()); })()), "children", array()));
                foreach ($context['seq'] as $context["childName"] => $context["childData"]) {
                    // line 448
                    echo "                    ";
                    echo $context["tree"]->macroformtreeentry($context["childName"], $context["childData"], false);
                    echo "
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['childName'], $context['childData'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 450
                echo "            </ul>
        ";
            }
            // line 452
            echo "    </li>
";
            
            $internalb2ceecd3976fdc80e8921dea6eb8f9d342f7e4c9dfe3dcd9abad7252f237b340->leave($internalb2ceecd3976fdc80e8921dea6eb8f9d342f7e4c9dfe3dcd9abad7252f237b340prof);

            
            $internal488575490f8d2ec878a9368d1caa24c662d4eb70606dd43caec6fb8b03abd943->leave($internal488575490f8d2ec878a9368d1caa24c662d4eb70606dd43caec6fb8b03abd943prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    // line 455
    public function macroformtreedetails($name = null, $data = null, $formsbyhash = null, $show = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "name" => $name,
            "data" => $data,
            "formsbyhash" => $formsbyhash,
            "show" => $show,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal6d1d3741f241a43fe32a7358e71c5a0ab40409fc4d849636df5b1ddaabf032b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal6d1d3741f241a43fe32a7358e71c5a0ab40409fc4d849636df5b1ddaabf032b2->enter($internal6d1d3741f241a43fe32a7358e71c5a0ab40409fc4d849636df5b1ddaabf032b2prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "formtreedetails"));

            $internalb2ed972f94afd5336eaf747aa2c1552c219c5f6a32c11c596ef1049257db09ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internalb2ed972f94afd5336eaf747aa2c1552c219c5f6a32c11c596ef1049257db09ac->enter($internalb2ed972f94afd5336eaf747aa2c1552c219c5f6a32c11c596ef1049257db09acprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "formtreedetails"));

            // line 456
            echo "    ";
            $context["tree"] = $this;
            // line 457
            echo "    <div class=\"tree-details";
            if ( !((arraykeyexists("show", $context)) ? (twigdefaultfilter((isset($context["show"]) || arraykeyexists("show", $context) ? $context["show"] : (function () { throw new TwigErrorRuntime('Variable "show" does not exist.', 457, $this->getSourceContext()); })()), false)) : (false))) {
                echo " hidden";
            }
            echo "\" ";
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "id", array(), "any", true, true)) {
                echo "id=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 457, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-details\"";
            }
            echo ">
        <h2 class=\"dump-inline\">
            ";
            // line 459
            echo twigescapefilter($this->env, ((arraykeyexists("name", $context)) ? (twigdefaultfilter((isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 459, $this->getSourceContext()); })()), "(no name)")) : ("(no name)")), "html", null, true);
            echo " ";
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "typeclass", array(), "any", true, true)) {
                echo "(";
                echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 459, $this->getSourceContext()); })()), "typeclass", array())));
                echo ")";
            }
            // line 460
            echo "        </h2>

        ";
            // line 462
            if ((twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "errors", array(), "any", true, true) && (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 462, $this->getSourceContext()); })()), "errors", array())) > 0))) {
                // line 463
                echo "        <div class=\"errors\">
            <h3>
                <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 465
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 465, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-errors\" href=\"#\">
                    Errors <span class=\"toggle-icon\"></span>
                </a>
            </h3>

            <table id=\"";
                // line 470
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 470, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-errors\">
                <thead>
                    <tr>
                        <th>Message</th>
                        <th>Origin</th>
                        <th>Cause</th>
                    </tr>
                </thead>
                <tbody>
                ";
                // line 479
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 479, $this->getSourceContext()); })()), "errors", array()));
                foreach ($context['seq'] as $context["key"] => $context["error"]) {
                    // line 480
                    echo "                <tr>
                    <td>";
                    // line 481
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "message", array()), "html", null, true);
                    echo "</td>
                    <td>
                        ";
                    // line 483
                    if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "origin", array()))) {
                        // line 484
                        echo "                            <em>This form.</em>
                        ";
                    } elseif ( !twiggetattribute($this->env, $this->getSourceContext(),                     // line 485
($context["formsbyhash"] ?? null), twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "origin", array()), array(), "array", true, true)) {
                        // line 486
                        echo "                            <em>Unknown.</em>
                        ";
                    } else {
                        // line 488
                        echo "                            ";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formsbyhash"]) || arraykeyexists("formsbyhash", $context) ? $context["formsbyhash"] : (function () { throw new TwigErrorRuntime('Variable "formsbyhash" does not exist.', 488, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "origin", array()), array(), "array"), "name", array()), "html", null, true);
                        echo "
                        ";
                    }
                    // line 490
                    echo "                    </td>
                    <td>
                        ";
                    // line 492
                    if (twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "trace", array())) {
                        // line 493
                        echo "                            <span class=\"newline\">Caused by:</span>
                            ";
                        // line 494
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "trace", array()));
                        foreach ($context['seq'] as $context["key"] => $context["stacked"]) {
                            // line 495
                            echo "                                ";
                            echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, $context["stacked"]));
                            echo "
                            ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['key'], $context['stacked'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 497
                        echo "                        ";
                    } else {
                        // line 498
                        echo "                            <em>Unknown.</em>
                        ";
                    }
                    // line 500
                    echo "                    </td>
                </tr>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['error'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 503
                echo "                </tbody>
            </table>
        </div>
        ";
            }
            // line 507
            echo "
        ";
            // line 508
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "defaultdata", array(), "any", true, true)) {
                // line 509
                echo "        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 510
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 510, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-defaultdata\" href=\"#\">
                Default Data <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"";
                // line 515
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 515, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-defaultdata\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Property</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Model Format</th>
                        <td>
                            ";
                // line 527
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "defaultdata", array(), "any", false, true), "model", array(), "any", true, true)) {
                    // line 528
                    echo "                                ";
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 528, $this->getSourceContext()); })()), "defaultdata", array()), "seek", array(0 => "model"), "method")));
                    echo "
                            ";
                } else {
                    // line 530
                    echo "                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            ";
                }
                // line 532
                echo "                        </td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Normalized Format</th>
                        <td>";
                // line 536
                echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 536, $this->getSourceContext()); })()), "defaultdata", array()), "seek", array(0 => "norm"), "method")));
                echo "</td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">View Format</th>
                        <td>
                            ";
                // line 541
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "defaultdata", array(), "any", false, true), "view", array(), "any", true, true)) {
                    // line 542
                    echo "                                ";
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 542, $this->getSourceContext()); })()), "defaultdata", array()), "seek", array(0 => "view"), "method")));
                    echo "
                            ";
                } else {
                    // line 544
                    echo "                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            ";
                }
                // line 546
                echo "                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        ";
            }
            // line 552
            echo "
        ";
            // line 553
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "submitteddata", array(), "any", true, true)) {
                // line 554
                echo "        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 555
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 555, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-submitteddata\" href=\"#\">
                Submitted Data <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"";
                // line 560
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 560, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-submitteddata\">
        ";
                // line 561
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "submitteddata", array(), "any", false, true), "norm", array(), "any", true, true)) {
                    // line 562
                    echo "            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Property</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">View Format</th>
                        <td>
                            ";
                    // line 573
                    if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "submitteddata", array(), "any", false, true), "view", array(), "any", true, true)) {
                        // line 574
                        echo "                                ";
                        echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 574, $this->getSourceContext()); })()), "submitteddata", array()), "seek", array(0 => "view"), "method")));
                        echo "
                            ";
                    } else {
                        // line 576
                        echo "                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            ";
                    }
                    // line 578
                    echo "                        </td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Normalized Format</th>
                        <td>";
                    // line 582
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 582, $this->getSourceContext()); })()), "submitteddata", array()), "seek", array(0 => "norm"), "method")));
                    echo "</td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Model Format</th>
                        <td>
                            ";
                    // line 587
                    if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "submitteddata", array(), "any", false, true), "model", array(), "any", true, true)) {
                        // line 588
                        echo "                                ";
                        echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 588, $this->getSourceContext()); })()), "submitteddata", array()), "seek", array(0 => "model"), "method")));
                        echo "
                            ";
                    } else {
                        // line 590
                        echo "                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            ";
                    }
                    // line 592
                    echo "                        </td>
                    </tr>
                </tbody>
            </table>
        ";
                } else {
                    // line 597
                    echo "            <div class=\"empty\">
                <p>This form was not submitted.</p>
            </div>
        ";
                }
                // line 601
                echo "        </div>
        ";
            }
            // line 603
            echo "
        ";
            // line 604
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "passedoptions", array(), "any", true, true)) {
                // line 605
                echo "        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 606
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 606, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-passedoptions\" href=\"#\">
                Passed Options <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"";
                // line 611
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 611, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-passedoptions\">
            ";
                // line 612
                if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 612, $this->getSourceContext()); })()), "passedoptions", array()))) {
                    // line 613
                    echo "            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Option</th>
                        <th>Passed Value</th>
                        <th>Resolved Value</th>
                    </tr>
                </thead>
                <tbody>
                ";
                    // line 622
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 622, $this->getSourceContext()); })()), "passedoptions", array()));
                    foreach ($context['seq'] as $context["option"] => $context["value"]) {
                        // line 623
                        echo "                <tr>
                    <th>";
                        // line 624
                        echo twigescapefilter($this->env, $context["option"], "html", null, true);
                        echo "</th>
                    <td>";
                        // line 625
                        echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, $context["value"]));
                        echo "</td>
                    <td>
                        ";
                        // line 628
                        echo "                        ";
                        $context["optionvalue"] = ((twiggetattribute($this->env, $this->getSourceContext(), $context["value"], "value", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["value"], "value", array()), $context["value"])) : ($context["value"]));
                        // line 629
                        echo "                        ";
                        $context["resolvedoptionvalue"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "resolvedoptions", array(), "any", false, true), $context["option"], array(), "array", false, true), "value", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "resolvedoptions", array(), "any", false, true), $context["option"], array(), "array", false, true), "value", array()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 629, $this->getSourceContext()); })()), "resolvedoptions", array()), $context["option"], array(), "array"))) : (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 629, $this->getSourceContext()); })()), "resolvedoptions", array()), $context["option"], array(), "array")));
                        // line 630
                        echo "                        ";
                        if (((isset($context["resolvedoptionvalue"]) || arraykeyexists("resolvedoptionvalue", $context) ? $context["resolvedoptionvalue"] : (function () { throw new TwigErrorRuntime('Variable "resolvedoptionvalue" does not exist.', 630, $this->getSourceContext()); })()) == (isset($context["optionvalue"]) || arraykeyexists("optionvalue", $context) ? $context["optionvalue"] : (function () { throw new TwigErrorRuntime('Variable "optionvalue" does not exist.', 630, $this->getSourceContext()); })()))) {
                            // line 631
                            echo "                            <em class=\"font-normal text-muted\">same as passed value</em>
                        ";
                        } else {
                            // line 633
                            echo "                            ";
                            echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 633, $this->getSourceContext()); })()), "resolvedoptions", array()), "seek", array(0 => $context["option"]), "method")));
                            echo "
                        ";
                        }
                        // line 635
                        echo "                    </td>
                </tr>
                ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['option'], $context['value'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 638
                    echo "                </tbody>
            </table>
            ";
                } else {
                    // line 641
                    echo "                <div class=\"empty\">
                    <p>No options where passed when constructing this form.</p>
                </div>
            ";
                }
                // line 645
                echo "        </div>
        ";
            }
            // line 647
            echo "
        ";
            // line 648
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "resolvedoptions", array(), "any", true, true)) {
                // line 649
                echo "        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 650
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 650, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-resolvedoptions\" href=\"#\">
                Resolved Options <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"";
                // line 655
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 655, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-resolvedoptions\" class=\"hidden\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Option</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                ";
                // line 664
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 664, $this->getSourceContext()); })()), "resolvedoptions", array()));
                foreach ($context['seq'] as $context["option"] => $context["value"]) {
                    // line 665
                    echo "                <tr>
                    <th scope=\"row\">";
                    // line 666
                    echo twigescapefilter($this->env, $context["option"], "html", null, true);
                    echo "</th>
                    <td>";
                    // line 667
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, $context["value"]));
                    echo "</td>
                </tr>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['option'], $context['value'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 670
                echo "                </tbody>
            </table>
        </div>
        ";
            }
            // line 674
            echo "
        ";
            // line 675
            if (twiggetattribute($this->env, $this->getSourceContext(), ($context["data"] ?? null), "viewvars", array(), "any", true, true)) {
                // line 676
                echo "        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"";
                // line 677
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 677, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-viewvars\" href=\"#\">
                View Variables <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"";
                // line 682
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 682, $this->getSourceContext()); })()), "id", array()), "html", null, true);
                echo "-viewvars\" class=\"hidden\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Variable</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                ";
                // line 691
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 691, $this->getSourceContext()); })()), "viewvars", array()));
                foreach ($context['seq'] as $context["variable"] => $context["value"]) {
                    // line 692
                    echo "                <tr>
                    <th scope=\"row\">";
                    // line 693
                    echo twigescapefilter($this->env, $context["variable"], "html", null, true);
                    echo "</th>
                    <td>";
                    // line 694
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, $context["value"]));
                    echo "</td>
                </tr>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['variable'], $context['value'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 697
                echo "                </tbody>
            </table>
        </div>
        ";
            }
            // line 701
            echo "    </div>

    ";
            // line 703
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 703, $this->getSourceContext()); })()), "children", array()));
            foreach ($context['seq'] as $context["childName"] => $context["childData"]) {
                // line 704
                echo "        ";
                echo $context["tree"]->macroformtreedetails($context["childName"], $context["childData"], (isset($context["formsbyhash"]) || arraykeyexists("formsbyhash", $context) ? $context["formsbyhash"] : (function () { throw new TwigErrorRuntime('Variable "formsbyhash" does not exist.', 704, $this->getSourceContext()); })()));
                echo "
    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['childName'], $context['childData'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            
            $internalb2ed972f94afd5336eaf747aa2c1552c219c5f6a32c11c596ef1049257db09ac->leave($internalb2ed972f94afd5336eaf747aa2c1552c219c5f6a32c11c596ef1049257db09acprof);

            
            $internal6d1d3741f241a43fe32a7358e71c5a0ab40409fc4d849636df5b1ddaabf032b2->leave($internal6d1d3741f241a43fe32a7358e71c5a0ab40409fc4d849636df5b1ddaabf032b2prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1266 => 704,  1262 => 703,  1258 => 701,  1252 => 697,  1243 => 694,  1239 => 693,  1236 => 692,  1232 => 691,  1220 => 682,  1212 => 677,  1209 => 676,  1207 => 675,  1204 => 674,  1198 => 670,  1189 => 667,  1185 => 666,  1182 => 665,  1178 => 664,  1166 => 655,  1158 => 650,  1155 => 649,  1153 => 648,  1150 => 647,  1146 => 645,  1140 => 641,  1135 => 638,  1127 => 635,  1121 => 633,  1117 => 631,  1114 => 630,  1111 => 629,  1108 => 628,  1103 => 625,  1099 => 624,  1096 => 623,  1092 => 622,  1081 => 613,  1079 => 612,  1075 => 611,  1067 => 606,  1064 => 605,  1062 => 604,  1059 => 603,  1055 => 601,  1049 => 597,  1042 => 592,  1038 => 590,  1032 => 588,  1030 => 587,  1022 => 582,  1016 => 578,  1012 => 576,  1006 => 574,  1004 => 573,  991 => 562,  989 => 561,  985 => 560,  977 => 555,  974 => 554,  972 => 553,  969 => 552,  961 => 546,  957 => 544,  951 => 542,  949 => 541,  941 => 536,  935 => 532,  931 => 530,  925 => 528,  923 => 527,  908 => 515,  900 => 510,  897 => 509,  895 => 508,  892 => 507,  886 => 503,  878 => 500,  874 => 498,  871 => 497,  862 => 495,  858 => 494,  855 => 493,  853 => 492,  849 => 490,  843 => 488,  839 => 486,  837 => 485,  834 => 484,  832 => 483,  827 => 481,  824 => 480,  820 => 479,  808 => 470,  800 => 465,  796 => 463,  794 => 462,  790 => 460,  782 => 459,  768 => 457,  765 => 456,  744 => 455,  728 => 452,  724 => 450,  715 => 448,  711 => 447,  702 => 446,  700 => 445,  693 => 441,  687 => 440,  684 => 439,  680 => 437,  674 => 435,  672 => 434,  669 => 433,  663 => 431,  661 => 430,  657 => 429,  654 => 428,  651 => 427,  648 => 426,  628 => 425,  400 => 205,  394 => 201,  390 => 199,  373 => 197,  356 => 196,  350 => 192,  341 => 190,  337 => 189,  333 => 187,  331 => 186,  327 => 184,  318 => 183,  168 => 43,  159 => 42,  148 => 39,  142 => 36,  139 => 35,  137 => 34,  132 => 32,  125 => 31,  116 => 30,  103 => 26,  100 => 25,  92 => 22,  85 => 18,  81 => 16,  79 => 15,  76 => 14,  70 => 11,  64 => 9,  61 => 8,  58 => 7,  55 => 6,  46 => 5,  36 => 1,  34 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% from self import formtreeentry, formtreedetails %}

{% block toolbar %}
    {% if collector.data.nberrors > 0 or collector.data.forms|length %}
        {% set statuscolor = collector.data.nberrors ? 'red' : '' %}
        {% set icon %}
            {{ include('@WebProfiler/Icon/form.svg') }}
            <span class=\"sf-toolbar-value\">
                {{ collector.data.nberrors ?: collector.data.forms|length }}
            </span>
        {% endset %}

        {% set text %}
            <div class=\"sf-toolbar-info-piece\">
                <b>Number of forms</b>
                <span class=\"sf-toolbar-status\">{{ collector.data.forms|length }}</span>
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Number of errors</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.data.nberrors > 0 ? 'red' }}\">{{ collector.data.nberrors }}</span>
            </div>
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl, status: statuscolor }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    <span class=\"label label-status-{{ collector.data.nberrors ? 'error' }} {{ collector.data.forms is empty ? 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/form.svg') }}</span>
        <strong>Forms</strong>
        {% if collector.data.nberrors > 0 %}
            <span class=\"count\">
                <span>{{ collector.data.nberrors }}</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block head %}
    {{ parent() }}

    <style>
        #tree-menu {
            float: left;
            padding-right: 10px;
            width: 230px;
        }
        #tree-menu ul {
            list-style: none;
            margin: 0;
            padding-left: 0;
        }
        #tree-menu li {
            margin: 0;
            padding: 0;
            width: 100%;
        }
        #tree-menu .empty {
            border: 0;
            padding: 0;
        }
        #tree-details-container {
            border-left: 1px solid #DDD;
            margin-left: 250px;
            padding-left: 20px;
        }
        .tree-details {
            padding-bottom: 40px;
        }
        .tree-details h3 {
            font-size: 18px;
            position: relative;
        }

        .toggle-icon {
            display: inline-block;
            background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAgBAMAAADpp+X/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDgwx4LcKwAAAABVQTFRFAAAA////////////////ZmZm////bvjBwAAAAAV0Uk5TABZwsuCVEUjgAAAAAWJLR0QF+G/pxwAAAE1JREFUGNNjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBmwgTQgQGWgA7h2uIFwK+CWwp1BpHvYEqDuATEYkBlY3IOmBq6dCPcAAIT5Eg2IksjQAAAAAElFTkSuQmCC\") no-repeat top left #5eb5e0;
        }
        .closed .toggle-icon, .closed.toggle-icon {
            background-position: bottom left;
        }
        .toggle-icon.empty {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAZgBmAGYHukptAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhIf6CA40AAAAFRJREFUOMvtk7ENACEMA61vfx767MROWfO+AdGBHlNyTZrYUZRYDBII4NWE1pNdpFarfgLUbpDaBEgBYRiEVjsvDLa1l6O4Z3wkFWN+OfLKdpisOH/TlICzukmUJwAAAABJRU5ErkJggg==\");
        }

        .tree .tree-inner {
            cursor: pointer;
            padding: 5px 7px 5px 22px;
            position: relative;

        }
        .tree .toggle-button {
            /* provide a bigger clickable area than just 10x10px */
            width: 16px;
            height: 16px;
            margin-left: -18px;
        }
        .tree .toggle-icon {
            width: 10px;
            height: 10px;
            /* position the icon in the center of the clickable area */
            margin-left: 3px;
            margin-top: 3px;
            background-size: 10px 20px;
            background-color: #AAA;
        }
        .tree .toggle-icon.empty {
            width: 10px;
            height: 10px;
            position: absolute;
            top: 50%;
            margin-top: -5px;
            margin-left: -15px;
            background-size: 10px 10px;
        }
        .tree ul ul .tree-inner {
            padding-left: 37px;
        }
        .tree ul ul ul .tree-inner {
            padding-left: 52px;
        }
        .tree ul ul ul ul .tree-inner {
            padding-left: 67px;
        }
        .tree ul ul ul ul ul .tree-inner {
            padding-left: 82px;
        }
        .tree .tree-inner:hover {
            background: #dfdfdf;
        }
        .tree .tree-inner.active, .tree .tree-inner.active:hover {
            background: #E0E0E0;
            font-weight: bold;
        }
        .tree .tree-inner.active .toggle-icon, .tree .tree-inner:hover .toggle-icon, .tree .tree-inner.active:hover .toggle-icon {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAgBAMAAADpp+X/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhEYXWn+sAAAABhQTFRFAAAA39/f39/f39/f39/fZmZm39/f////gc3YPwAAAAV0Uk5TAAtAc6ZeVyCYAAAAAWJLR0QF+G/pxwAAAE1JREFUGNNjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBmwgXIgQGWgA7h2uIFwK+CWwp1BpHvYC6DuATEYkBlY3IOmBq6dCPcAADqLE4MnBi/fAAAAAElFTkSuQmCC\");
            background-color: #999;
        }
        .tree .tree-inner.active .toggle-icon.empty, .tree .tree-inner:hover .toggle-icon.empty, .tree .tree-inner.active:hover .toggle-icon.empty {
            background-image: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3QweDhoucSey4gAAABVQTFRFAAAA39/f39/f39/f39/fZmZm39/fD5Dx2AAAAAV0Uk5TAAtAc6ZeVyCYAAAAAWJLR0QF+G/pxwAAADJJREFUCNdjSHMSYGBgUEljSGYAAzMGBwiDhUEBwmBiEIAwGBnIA3DtcAPhVsAthTkDAFOfBKW9C1iqAAAAAElFTkSuQmCC\");
        }
        .tree-details .toggle-icon {
            width: 16px;
            height: 16px;
            /* vertically center the button */
            position: absolute;
            top: 50%;
            margin-top: -9px;
            margin-left: 6px;
        }
        .badge-error {
            float: right;
            background: #B0413E;
            color: #FFF;
            padding: 1px 4px;
            font-size: 10px;
            font-weight: bold;
            vertical-align: middle;
        }
        .has-error {
            color: #B0413E;
        }
        .errors h3 {
            color: #B0413E;
        }
        .errors th {
            background: #B0413E;
            color: #FFF;
        }
        .errors .toggle-icon {
            background-color: #B0413E;
        }
        h3 a, h3 a:hover, h3 a:focus {
            color: inherit;
            text-decoration: inherit;
        }
    </style>
{% endblock %}

{% block panel %}
    <h2>Forms</h2>

    {% if collector.data.forms|length %}
        <div id=\"tree-menu\" class=\"tree\">
            <ul>
            {% for formName, formData in collector.data.forms %}
                {{ formtreeentry(formName, formData, true) }}
            {% endfor %}
            </ul>
        </div>

        <div id=\"tree-details-container\">
            {% for formName, formData in collector.data.forms %}
                {{ formtreedetails(formName, formData, collector.data.formsbyhash, loop.first) }}
            {% endfor %}
        </div>
    {% else %}
        <div class=\"empty\">
            <p>No forms were submitted for this request.</p>
        </div>
    {% endif %}

    <script>
    function Toggler(storage) {
        \"use strict\";

        var STORAGEKEY = 'sftoggledata',

            states = {},

            isCollapsed = function (button) {
                return Sfjs.hasClass(button, 'closed');
            },

            isExpanded = function (button) {
                return !isCollapsed(button);
            },

            expand = function (button) {
                var targetId = button.dataset.toggleTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Toggle target \" + targetId + \" does not exist\";
                }

                if (isCollapsed(button)) {
                    Sfjs.removeClass(button, 'closed');
                    Sfjs.removeClass(target, 'hidden');

                    states[targetId] = 1;
                    storage.setItem(STORAGEKEY, states);
                }
            },

            collapse = function (button) {
                var targetId = button.dataset.toggleTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Toggle target \" + targetId + \" does not exist\";
                }

                if (isExpanded(button)) {
                    Sfjs.addClass(button, 'closed');
                    Sfjs.addClass(target, 'hidden');

                    states[targetId] = 0;
                    storage.setItem(STORAGEKEY, states);
                }
            },

            toggle = function (button) {
                if (Sfjs.hasClass(button, 'closed')) {
                    expand(button);
                } else {
                    collapse(button);
                }
            },

            initButtons = function (buttons) {
                states = storage.getItem(STORAGEKEY, {});

                // must be an object, not an array or anything else
                // `typeof` returns \"object\" also for arrays, so the following
                // check must be done
                // see http://stackoverflow.com/questions/4775722/check-if-object-is-array
                if ('[object Object]' !== Object.prototype.toString.call(states)) {
                    states = {};
                }

                for (var i = 0, l = buttons.length; i < l; ++i) {
                    var targetId = buttons[i].dataset.toggleTargetId,
                        target = document.getElementById(targetId);

                    if (!target) {
                        throw \"Toggle target \" + targetId + \" does not exist\";
                    }

                    // correct the initial state of the button
                    if (Sfjs.hasClass(target, 'hidden')) {
                        Sfjs.addClass(buttons[i], 'closed');
                    }

                    // attach listener for expanding/collapsing the target
                    clickHandler(buttons[i], toggle);

                    if (states.hasOwnProperty(targetId)) {
                        // open or collapse based on stored data
                        if (0 === states[targetId]) {
                            collapse(buttons[i]);
                        } else {
                            expand(buttons[i]);
                        }
                    }
                }
            };

        return {
            initButtons: initButtons,

            toggle: toggle,

            isExpanded: isExpanded,

            isCollapsed: isCollapsed,

            expand: expand,

            collapse: collapse
        };
    }

    function JsonStorage(storage) {
        var setItem = function (key, data) {
                storage.setItem(key, JSON.stringify(data));
            },

            getItem = function (key, defaultValue) {
                var data = storage.getItem(key);

                if (null !== data) {
                    try {
                        return JSON.parse(data);
                    } catch(e) {
                    }
                }

                return defaultValue;
            };

        return {
            setItem: setItem,

            getItem: getItem
        };
    }

    function TabView() {
        \"use strict\";

        var activeTab = null,

            activeTarget = null,

            select = function (tab) {
                var targetId = tab.dataset.tabTargetId,
                    target = document.getElementById(targetId);

                if (!target) {
                    throw \"Tab target \" + targetId + \" does not exist\";
                }

                if (activeTab) {
                    Sfjs.removeClass(activeTab, 'active');
                }

                if (activeTarget) {
                    Sfjs.addClass(activeTarget, 'hidden');
                }

                Sfjs.addClass(tab, 'active');
                Sfjs.removeClass(target, 'hidden');

                activeTab = tab;
                activeTarget = target;
            },

            initTabs = function (tabs) {
                for (var i = 0, l = tabs.length; i < l; ++i) {
                    var targetId = tabs[i].dataset.tabTargetId,
                        target = document.getElementById(targetId);

                    if (!target) {
                        throw \"Tab target \" + targetId + \" does not exist\";
                    }

                    clickHandler(tabs[i], select);

                    Sfjs.addClass(target, 'hidden');
                }

                if (tabs.length > 0) {
                    select(tabs[0]);
                }
            };

        return {
            initTabs: initTabs,

            select: select
        };
    }

    var tabTarget = new TabView(),
        toggler = new Toggler(new JsonStorage(sessionStorage)),
        clickHandler = function (element, callback) {
            Sfjs.addEventListener(element, 'click', function (e) {
                if (!e) {
                    e = window.event;
                }

                callback(this);

                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    e.returnValue = false;
                }

                e.stopPropagation();

                return false;
            });
        };

    tabTarget.initTabs(document.querySelectorAll('.tree .tree-inner'));
    toggler.initButtons(document.querySelectorAll('a.toggle-button'));
    </script>
{% endblock %}

{% macro formtreeentry(name, data, isroot) %}
    {% import self as tree %}
    {% set haserror = data.errors is defined and data.errors|length > 0 %}
    <li>
        <div class=\"tree-inner\" data-tab-target-id=\"{{ data.id }}-details\">
            {% if haserror %}
                <div class=\"badge-error\">{{ data.errors|length }}</div>
            {% endif %}

            {% if data.children is not empty %}
                <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-children\" href=\"#\"><span class=\"toggle-icon\"></span></a>
            {% else %}
                <div class=\"toggle-icon empty\"></div>
            {% endif %}

            <span {% if haserror or data.haschildrenerror|default(false) %}class=\"has-error\"{% endif %}>
                {{ name|default('(no name)') }}
            </span>
        </div>

        {% if data.children is not empty %}
            <ul id=\"{{ data.id }}-children\" {% if not isroot and not data.haschildrenerror|default(false) %}class=\"hidden\"{% endif %}>
                {% for childName, childData in data.children %}
                    {{ tree.formtreeentry(childName, childData, false) }}
                {% endfor %}
            </ul>
        {% endif %}
    </li>
{% endmacro %}

{% macro formtreedetails(name, data, formsbyhash, show) %}
    {% import self as tree %}
    <div class=\"tree-details{% if not show|default(false) %} hidden{% endif %}\" {% if data.id is defined %}id=\"{{ data.id }}-details\"{% endif %}>
        <h2 class=\"dump-inline\">
            {{ name|default('(no name)') }} {% if data.typeclass is defined %}({{ profilerdump(data.typeclass) }}){% endif %}
        </h2>

        {% if data.errors is defined and data.errors|length > 0 %}
        <div class=\"errors\">
            <h3>
                <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-errors\" href=\"#\">
                    Errors <span class=\"toggle-icon\"></span>
                </a>
            </h3>

            <table id=\"{{ data.id }}-errors\">
                <thead>
                    <tr>
                        <th>Message</th>
                        <th>Origin</th>
                        <th>Cause</th>
                    </tr>
                </thead>
                <tbody>
                {% for error in data.errors %}
                <tr>
                    <td>{{ error.message }}</td>
                    <td>
                        {% if error.origin is empty %}
                            <em>This form.</em>
                        {% elseif formsbyhash[error.origin] is not defined %}
                            <em>Unknown.</em>
                        {% else %}
                            {{ formsbyhash[error.origin].name }}
                        {% endif %}
                    </td>
                    <td>
                        {% if error.trace %}
                            <span class=\"newline\">Caused by:</span>
                            {% for stacked in error.trace %}
                                {{ profilerdump(stacked) }}
                            {% endfor %}
                        {% else %}
                            <em>Unknown.</em>
                        {% endif %}
                    </td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
        {% endif %}

        {% if data.defaultdata is defined %}
        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-defaultdata\" href=\"#\">
                Default Data <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"{{ data.id }}-defaultdata\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Property</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Model Format</th>
                        <td>
                            {% if data.defaultdata.model is defined %}
                                {{ profilerdump(data.defaultdata.seek('model')) }}
                            {% else %}
                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            {% endif %}
                        </td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Normalized Format</th>
                        <td>{{ profilerdump(data.defaultdata.seek('norm')) }}</td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">View Format</th>
                        <td>
                            {% if data.defaultdata.view is defined %}
                                {{ profilerdump(data.defaultdata.seek('view')) }}
                            {% else %}
                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            {% endif %}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        {% endif %}

        {% if data.submitteddata is defined %}
        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-submitteddata\" href=\"#\">
                Submitted Data <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"{{ data.id }}-submitteddata\">
        {% if data.submitteddata.norm is defined %}
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Property</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">View Format</th>
                        <td>
                            {% if data.submitteddata.view is defined %}
                                {{ profilerdump(data.submitteddata.seek('view')) }}
                            {% else %}
                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            {% endif %}
                        </td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Normalized Format</th>
                        <td>{{ profilerdump(data.submitteddata.seek('norm')) }}</td>
                    </tr>
                    <tr>
                        <th class=\"font-normal\" scope=\"row\">Model Format</th>
                        <td>
                            {% if data.submitteddata.model is defined %}
                                {{ profilerdump(data.submitteddata.seek('model')) }}
                            {% else %}
                                <em class=\"font-normal text-muted\">same as normalized format</em>
                            {% endif %}
                        </td>
                    </tr>
                </tbody>
            </table>
        {% else %}
            <div class=\"empty\">
                <p>This form was not submitted.</p>
            </div>
        {% endif %}
        </div>
        {% endif %}

        {% if data.passedoptions is defined %}
        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-passedoptions\" href=\"#\">
                Passed Options <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"{{ data.id }}-passedoptions\">
            {% if data.passedoptions|length %}
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Option</th>
                        <th>Passed Value</th>
                        <th>Resolved Value</th>
                    </tr>
                </thead>
                <tbody>
                {% for option, value in data.passedoptions %}
                <tr>
                    <th>{{ option }}</th>
                    <td>{{ profilerdump(value) }}</td>
                    <td>
                        {# values can be stubs #}
                        {% set optionvalue = value.value|default(value) %}
                        {% set resolvedoptionvalue = data.resolvedoptions[option].value|default(data.resolvedoptions[option]) %}
                        {% if resolvedoptionvalue == optionvalue %}
                            <em class=\"font-normal text-muted\">same as passed value</em>
                        {% else %}
                            {{ profilerdump(data.resolvedoptions.seek(option)) }}
                        {% endif %}
                    </td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
            {% else %}
                <div class=\"empty\">
                    <p>No options where passed when constructing this form.</p>
                </div>
            {% endif %}
        </div>
        {% endif %}

        {% if data.resolvedoptions is defined %}
        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-resolvedoptions\" href=\"#\">
                Resolved Options <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"{{ data.id }}-resolvedoptions\" class=\"hidden\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Option</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                {% for option, value in data.resolvedoptions %}
                <tr>
                    <th scope=\"row\">{{ option }}</th>
                    <td>{{ profilerdump(value) }}</td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
        {% endif %}

        {% if data.viewvars is defined %}
        <h3>
            <a class=\"toggle-button\" data-toggle-target-id=\"{{ data.id }}-viewvars\" href=\"#\">
                View Variables <span class=\"toggle-icon\"></span>
            </a>
        </h3>

        <div id=\"{{ data.id }}-viewvars\" class=\"hidden\">
            <table>
                <thead>
                    <tr>
                        <th width=\"180\">Variable</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                {% for variable, value in data.viewvars %}
                <tr>
                    <th scope=\"row\">{{ variable }}</th>
                    <td>{{ profilerdump(value) }}</td>
                </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
        {% endif %}
    </div>

    {% for childName, childData in data.children %}
        {{ tree.formtreedetails(childName, childData, formsbyhash) }}
    {% endfor %}
{% endmacro %}
", "WebProfilerBundle:Collector:form.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/form.html.twig");
    }
}
