<?php

/* SonataAdminBundle:Button:historybutton.html.twig */
class TwigTemplate5acab757837a97d7f85736fca0ea91da28152c705b30fb692e3270a01e0ae2bc extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8b4173dd539745adb46683b3e74dbf72d303eca9a80a52159b37411144320a9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8b4173dd539745adb46683b3e74dbf72d303eca9a80a52159b37411144320a9f->enter($internal8b4173dd539745adb46683b3e74dbf72d303eca9a80a52159b37411144320a9fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:historybutton.html.twig"));

        $internal9c2163c1777b5c997ca60731090c7c09f42468a8c15d36b0d2421c85e0d42f59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9c2163c1777b5c997ca60731090c7c09f42468a8c15d36b0d2421c85e0d42f59->enter($internal9c2163c1777b5c997ca60731090c7c09f42468a8c15d36b0d2421c85e0d42f59prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:historybutton.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "canAccessObject", array(0 => "history", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "history"), "method"))) {
            // line 13
            echo "    <li>
        <a class=\"sonata-action-element\" href=\"";
            // line 14
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "history", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 14, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
            <i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
            ";
            // line 16
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionhistory", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </a>
    </li>
";
        }
        
        $internal8b4173dd539745adb46683b3e74dbf72d303eca9a80a52159b37411144320a9f->leave($internal8b4173dd539745adb46683b3e74dbf72d303eca9a80a52159b37411144320a9fprof);

        
        $internal9c2163c1777b5c997ca60731090c7c09f42468a8c15d36b0d2421c85e0d42f59->leave($internal9c2163c1777b5c997ca60731090c7c09f42468a8c15d36b0d2421c85e0d42f59prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Button:historybutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 16,  33 => 14,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.canAccessObject('history', object) and admin.hasRoute('history') %}
    <li>
        <a class=\"sonata-action-element\" href=\"{{ admin.generateObjectUrl('history', object) }}\">
            <i class=\"fa fa-archive\" aria-hidden=\"true\"></i>
            {{ 'linkactionhistory'|trans({}, 'SonataAdminBundle') }}
        </a>
    </li>
{% endif %}
", "SonataAdminBundle:Button:historybutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Button/historybutton.html.twig");
    }
}
