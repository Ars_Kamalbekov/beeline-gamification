<?php

/* SonataAdminBundle:Pager:simplepagerresults.html.twig */
class TwigTemplated783315fbb8e7761c51c525aa8b583265fcbe6920d1c5e748c27cb6680fb1163 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:baseresults.html.twig", "SonataAdminBundle:Pager:simplepagerresults.html.twig", 12);
        $this->blocks = array(
            'numresults' => array($this, 'blocknumresults'),
            'numpages' => array($this, 'blocknumpages'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:baseresults.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0bdb3ab4b86e516c088ba21ac453ebe4f1d9c3fdcace0a959841c8f50f735a86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0bdb3ab4b86e516c088ba21ac453ebe4f1d9c3fdcace0a959841c8f50f735a86->enter($internal0bdb3ab4b86e516c088ba21ac453ebe4f1d9c3fdcace0a959841c8f50f735a86prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simplepagerresults.html.twig"));

        $internal6ecfb5ba466dc8199727f041c80638d4762b90b2a20909881d425916386d2871 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6ecfb5ba466dc8199727f041c80638d4762b90b2a20909881d425916386d2871->enter($internal6ecfb5ba466dc8199727f041c80638d4762b90b2a20909881d425916386d2871prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simplepagerresults.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal0bdb3ab4b86e516c088ba21ac453ebe4f1d9c3fdcace0a959841c8f50f735a86->leave($internal0bdb3ab4b86e516c088ba21ac453ebe4f1d9c3fdcace0a959841c8f50f735a86prof);

        
        $internal6ecfb5ba466dc8199727f041c80638d4762b90b2a20909881d425916386d2871->leave($internal6ecfb5ba466dc8199727f041c80638d4762b90b2a20909881d425916386d2871prof);

    }

    // line 14
    public function blocknumresults($context, array $blocks = array())
    {
        $internaleeb514196146bbbfccb01faea67282107181cbef27134ef174f99eb7f3078403 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleeb514196146bbbfccb01faea67282107181cbef27134ef174f99eb7f3078403->enter($internaleeb514196146bbbfccb01faea67282107181cbef27134ef174f99eb7f3078403prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numresults"));

        $internal6a02d1775c020b5278625112c561982c6eb23e26b9b96fc7ceeb6785c716a775 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6a02d1775c020b5278625112c561982c6eb23e26b9b96fc7ceeb6785c716a775->enter($internal6a02d1775c020b5278625112c561982c6eb23e26b9b96fc7ceeb6785c716a775prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numresults"));

        // line 15
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastPage", array()) != twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 16
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("listresultscountprefix", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("listresultscount", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $internal6a02d1775c020b5278625112c561982c6eb23e26b9b96fc7ceeb6785c716a775->leave($internal6a02d1775c020b5278625112c561982c6eb23e26b9b96fc7ceeb6785c716a775prof);

        
        $internaleeb514196146bbbfccb01faea67282107181cbef27134ef174f99eb7f3078403->leave($internaleeb514196146bbbfccb01faea67282107181cbef27134ef174f99eb7f3078403prof);

    }

    // line 22
    public function blocknumpages($context, array $blocks = array())
    {
        $internal290305b8932bb817fb70f89f42f7c5d9efc83840825fd779385a831730bd28e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal290305b8932bb817fb70f89f42f7c5d9efc83840825fd779385a831730bd28e5->enter($internal290305b8932bb817fb70f89f42f7c5d9efc83840825fd779385a831730bd28e5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numpages"));

        $internal0200beccb6c215b9f11c5942027e871faa32943b411c645fbcf2d5347695a5b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0200beccb6c215b9f11c5942027e871faa32943b411c645fbcf2d5347695a5b0->enter($internal0200beccb6c215b9f11c5942027e871faa32943b411c645fbcf2d5347695a5b0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numpages"));

        // line 23
        echo "    ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo "
    /
    ";
        // line 25
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastPage", array()) != twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 26
            echo "        ?
    ";
        } else {
            // line 28
            echo "        ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
            echo "
    ";
        }
        // line 30
        echo "    &nbsp;-&nbsp;
";
        
        $internal0200beccb6c215b9f11c5942027e871faa32943b411c645fbcf2d5347695a5b0->leave($internal0200beccb6c215b9f11c5942027e871faa32943b411c645fbcf2d5347695a5b0prof);

        
        $internal290305b8932bb817fb70f89f42f7c5d9efc83840825fd779385a831730bd28e5->leave($internal290305b8932bb817fb70f89f42f7c5d9efc83840825fd779385a831730bd28e5prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:simplepagerresults.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 30,  94 => 28,  90 => 26,  88 => 25,  82 => 23,  73 => 22,  62 => 19,  59 => 18,  53 => 16,  50 => 15,  41 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:baseresults.html.twig' %}

{% block numresults %}
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        {{ 'listresultscountprefix'|trans({}, 'SonataAdminBundle') }}
    {% endif %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}listresultscount{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block numpages %}
    {{ admin.datagrid.pager.page }}
    /
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        ?
    {% else %}
        {{ admin.datagrid.pager.lastpage }}
    {% endif %}
    &nbsp;-&nbsp;
{% endblock %}
", "SonataAdminBundle:Pager:simplepagerresults.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Pager/simplepagerresults.html.twig");
    }
}
