<?php

/* WebProfilerBundle:Profiler:results.html.twig */
class TwigTemplate40ca9f4954092cd0080c3d5d479be8b258ee9dfccd2dbc300b7a8ae3e9af88a7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:results.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'blocksummary'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal27a332987247dfb7860a8cc60f4fc15e653bc740aebc1b4d43731c65725fe6db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal27a332987247dfb7860a8cc60f4fc15e653bc740aebc1b4d43731c65725fe6db->enter($internal27a332987247dfb7860a8cc60f4fc15e653bc740aebc1b4d43731c65725fe6dbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:results.html.twig"));

        $internalc50fc0a4315d925044e78e6ff31f8514a77695a77cf01e00bdba5fbf2a7d5b53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc50fc0a4315d925044e78e6ff31f8514a77695a77cf01e00bdba5fbf2a7d5b53->enter($internalc50fc0a4315d925044e78e6ff31f8514a77695a77cf01e00bdba5fbf2a7d5b53prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:results.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal27a332987247dfb7860a8cc60f4fc15e653bc740aebc1b4d43731c65725fe6db->leave($internal27a332987247dfb7860a8cc60f4fc15e653bc740aebc1b4d43731c65725fe6dbprof);

        
        $internalc50fc0a4315d925044e78e6ff31f8514a77695a77cf01e00bdba5fbf2a7d5b53->leave($internalc50fc0a4315d925044e78e6ff31f8514a77695a77cf01e00bdba5fbf2a7d5b53prof);

    }

    // line 3
    public function blocksummary($context, array $blocks = array())
    {
        $internal25e86c3b7488279a62027d3018dcbc69496ce1761f52333d0285de8df0dc07d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal25e86c3b7488279a62027d3018dcbc69496ce1761f52333d0285de8df0dc07d1->enter($internal25e86c3b7488279a62027d3018dcbc69496ce1761f52333d0285de8df0dc07d1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        $internal47e0ae8f0c4aa5029fbeb0d0680cd9e43731721627a02536c86d7eb6afa54d51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal47e0ae8f0c4aa5029fbeb0d0680cd9e43731721627a02536c86d7eb6afa54d51->enter($internal47e0ae8f0c4aa5029fbeb0d0680cd9e43731721627a02536c86d7eb6afa54d51prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        // line 4
        echo "    <div class=\"status\">
        <div class=\"container\">
            <h2>Profile Search</h2>
        </div>
    </div>
";
        
        $internal47e0ae8f0c4aa5029fbeb0d0680cd9e43731721627a02536c86d7eb6afa54d51->leave($internal47e0ae8f0c4aa5029fbeb0d0680cd9e43731721627a02536c86d7eb6afa54d51prof);

        
        $internal25e86c3b7488279a62027d3018dcbc69496ce1761f52333d0285de8df0dc07d1->leave($internal25e86c3b7488279a62027d3018dcbc69496ce1761f52333d0285de8df0dc07d1prof);

    }

    // line 11
    public function blockpanel($context, array $blocks = array())
    {
        $internalefc6f203bc0a61eccc815df19cc7ac958f2ab03552338447f91b3fe8f626331d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalefc6f203bc0a61eccc815df19cc7ac958f2ab03552338447f91b3fe8f626331d->enter($internalefc6f203bc0a61eccc815df19cc7ac958f2ab03552338447f91b3fe8f626331dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal7538ba8c3bdb0eeb7186d60f3483a616c0d6752e58067e35eb5253883197df37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7538ba8c3bdb0eeb7186d60f3483a616c0d6752e58067e35eb5253883197df37->enter($internal7538ba8c3bdb0eeb7186d60f3483a616c0d6752e58067e35eb5253883197df37prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 12
        echo "    <h2>";
        echo twigescapefilter($this->env, (((isset($context["tokens"]) || arraykeyexists("tokens", $context) ? $context["tokens"] : (function () { throw new TwigErrorRuntime('Variable "tokens" does not exist.', 12, $this->getSourceContext()); })())) ? (twiglengthfilter($this->env, (isset($context["tokens"]) || arraykeyexists("tokens", $context) ? $context["tokens"] : (function () { throw new TwigErrorRuntime('Variable "tokens" does not exist.', 12, $this->getSourceContext()); })()))) : ("No")), "html", null, true);
        echo " results found</h2>

    ";
        // line 14
        if ((isset($context["tokens"]) || arraykeyexists("tokens", $context) ? $context["tokens"] : (function () { throw new TwigErrorRuntime('Variable "tokens" does not exist.', 14, $this->getSourceContext()); })())) {
            // line 15
            echo "        <table id=\"search-results\">
            <thead>
                <tr>
                    <th scope=\"col\" class=\"text-center\">Status</th>
                    <th scope=\"col\">IP</th>
                    <th scope=\"col\">Method</th>
                    <th scope=\"col\">URL</th>
                    <th scope=\"col\">Time</th>
                    <th scope=\"col\">Token</th>
                </tr>
            </thead>
            <tbody>
                ";
            // line 27
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["tokens"]) || arraykeyexists("tokens", $context) ? $context["tokens"] : (function () { throw new TwigErrorRuntime('Variable "tokens" does not exist.', 27, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["result"]) {
                // line 28
                echo "                    ";
                $context["cssclass"] = (((((twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array()), 0)) : (0)) > 399)) ? ("status-error") : ((((((twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array()), 0)) : (0)) > 299)) ? ("status-warning") : ("status-success"))));
                // line 29
                echo "
                    <tr>
                        <td class=\"text-center\">
                            <span class=\"label ";
                // line 32
                echo twigescapefilter($this->env, (isset($context["cssclass"]) || arraykeyexists("cssclass", $context) ? $context["cssclass"] : (function () { throw new TwigErrorRuntime('Variable "cssclass" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "statuscode", array()), "n/a")) : ("n/a")), "html", null, true);
                echo "</span>
                        </td>
                        <td>
                            <span class=\"nowrap\">";
                // line 35
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "ip", array()), "html", null, true);
                echo "</span>
                            ";
                // line 36
                if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 36, $this->getSourceContext()); })()), "session", array()))) {
                    // line 37
                    echo "                                <a href=\"";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearchresults", twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 37, $this->getSourceContext()); })()), "query", array()), "all", array()), array("ip" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "ip", array()), "token" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "token", array())))), "html", null, true);
                    echo "\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">";
                    // line 38
                    echo twiginclude($this->env, $context, "@WebProfiler/Icon/search.svg");
                    echo "</span>
                                </a>
                            ";
                }
                // line 41
                echo "                        </td>
                        <td>
                            ";
                // line 43
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "method", array()), "html", null, true);
                echo "
                            ";
                // line 44
                if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 44, $this->getSourceContext()); })()), "session", array()))) {
                    // line 45
                    echo "                                <a href=\"";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearchresults", twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 45, $this->getSourceContext()); })()), "query", array()), "all", array()), array("method" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "method", array()), "token" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "token", array())))), "html", null, true);
                    echo "\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">";
                    // line 46
                    echo twiginclude($this->env, $context, "@WebProfiler/Icon/search.svg");
                    echo "</span>
                                </a>
                            ";
                }
                // line 49
                echo "                        </td>
                        <td class=\"break-long-words\">
                            ";
                // line 51
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "url", array()), "html", null, true);
                echo "
                            ";
                // line 52
                if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 52, $this->getSourceContext()); })()), "session", array()))) {
                    // line 53
                    echo "                                <a href=\"";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearchresults", twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 53, $this->getSourceContext()); })()), "query", array()), "all", array()), array("url" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "url", array()), "token" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "token", array())))), "html", null, true);
                    echo "\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">";
                    // line 54
                    echo twiginclude($this->env, $context, "@WebProfiler/Icon/search.svg");
                    echo "</span>
                                </a>
                            ";
                }
                // line 57
                echo "                        </td>
                        <td class=\"text-small\">
                            <span class=\"nowrap\">";
                // line 59
                echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "time", array()), "d-M-Y"), "html", null, true);
                echo "</span>
                            <span class=\"nowrap newline\">";
                // line 60
                echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "time", array()), "H:i:s"), "html", null, true);
                echo "</span>
                        </td>
                        <td class=\"nowrap\"><a href=\"";
                // line 62
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "token", array()))), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["result"], "token", array()), "html", null, true);
                echo "</a></td>
                    </tr>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['result'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 65
            echo "            </tbody>
        </table>
    ";
        } else {
            // line 68
            echo "        <div class=\"empty\">
            <p>The query returned no result.</p>
        </div>
    ";
        }
        // line 72
        echo "
";
        
        $internal7538ba8c3bdb0eeb7186d60f3483a616c0d6752e58067e35eb5253883197df37->leave($internal7538ba8c3bdb0eeb7186d60f3483a616c0d6752e58067e35eb5253883197df37prof);

        
        $internalefc6f203bc0a61eccc815df19cc7ac958f2ab03552338447f91b3fe8f626331d->leave($internalefc6f203bc0a61eccc815df19cc7ac958f2ab03552338447f91b3fe8f626331dprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 72,  225 => 68,  220 => 65,  201 => 62,  196 => 60,  192 => 59,  188 => 57,  182 => 54,  177 => 53,  175 => 52,  171 => 51,  167 => 49,  161 => 46,  156 => 45,  154 => 44,  150 => 43,  146 => 41,  140 => 38,  135 => 37,  133 => 36,  129 => 35,  121 => 32,  116 => 29,  113 => 28,  96 => 27,  82 => 15,  80 => 14,  74 => 12,  65 => 11,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block summary %}
    <div class=\"status\">
        <div class=\"container\">
            <h2>Profile Search</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ tokens ? tokens|length : 'No' }} results found</h2>

    {% if tokens %}
        <table id=\"search-results\">
            <thead>
                <tr>
                    <th scope=\"col\" class=\"text-center\">Status</th>
                    <th scope=\"col\">IP</th>
                    <th scope=\"col\">Method</th>
                    <th scope=\"col\">URL</th>
                    <th scope=\"col\">Time</th>
                    <th scope=\"col\">Token</th>
                </tr>
            </thead>
            <tbody>
                {% for result in tokens %}
                    {% set cssclass = result.statuscode|default(0) > 399 ? 'status-error' : result.statuscode|default(0) > 299 ? 'status-warning' : 'status-success' %}

                    <tr>
                        <td class=\"text-center\">
                            <span class=\"label {{ cssclass }}\">{{ result.statuscode|default('n/a') }}</span>
                        </td>
                        <td>
                            <span class=\"nowrap\">{{ result.ip }}</span>
                            {% if request.session is not null %}
                                <a href=\"{{ path('profilersearchresults', request.query.all|merge({'ip': result.ip, 'token': result.token})) }}\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">{{ include('@WebProfiler/Icon/search.svg') }}</span>
                                </a>
                            {% endif %}
                        </td>
                        <td>
                            {{ result.method }}
                            {% if request.session is not null %}
                                <a href=\"{{ path('profilersearchresults', request.query.all|merge({'method': result.method, 'token': result.token})) }}\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">{{ include('@WebProfiler/Icon/search.svg') }}</span>
                                </a>
                            {% endif %}
                        </td>
                        <td class=\"break-long-words\">
                            {{ result.url }}
                            {% if request.session is not null %}
                                <a href=\"{{ path('profilersearchresults', request.query.all|merge({'url': result.url, 'token': result.token})) }}\" title=\"Search\">
                                    <span title=\"Search\" class=\"sf-icon sf-search\">{{ include('@WebProfiler/Icon/search.svg') }}</span>
                                </a>
                            {% endif %}
                        </td>
                        <td class=\"text-small\">
                            <span class=\"nowrap\">{{ result.time|date('d-M-Y') }}</span>
                            <span class=\"nowrap newline\">{{ result.time|date('H:i:s') }}</span>
                        </td>
                        <td class=\"nowrap\"><a href=\"{{ path('profiler', { token: result.token }) }}\">{{ result.token }}</a></td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>
    {% else %}
        <div class=\"empty\">
            <p>The query returned no result.</p>
        </div>
    {% endif %}

{% endblock %}
", "WebProfilerBundle:Profiler:results.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/results.html.twig");
    }
}
