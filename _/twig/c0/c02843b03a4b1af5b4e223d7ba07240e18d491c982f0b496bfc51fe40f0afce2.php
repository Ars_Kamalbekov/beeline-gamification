<?php

/* @Framework/Form/formrest.html.php */
class TwigTemplatef925495ea867336522d4ef0cbab40b7aad9844ab58798750160f49fb16f17818 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald48d19730296501316601a8f4d28e8543943790a0571ec8ee630242f9d28f7b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald48d19730296501316601a8f4d28e8543943790a0571ec8ee630242f9d28f7b5->enter($internald48d19730296501316601a8f4d28e8543943790a0571ec8ee630242f9d28f7b5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrest.html.php"));

        $internal002b9273f6071af5ba1bb1735b230df6df8df0472332d354dc38a7de152d94ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal002b9273f6071af5ba1bb1735b230df6df8df0472332d354dc38a7de152d94ae->enter($internal002b9273f6071af5ba1bb1735b230df6df8df0472332d354dc38a7de152d94aeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $internald48d19730296501316601a8f4d28e8543943790a0571ec8ee630242f9d28f7b5->leave($internald48d19730296501316601a8f4d28e8543943790a0571ec8ee630242f9d28f7b5prof);

        
        $internal002b9273f6071af5ba1bb1735b230df6df8df0472332d354dc38a7de152d94ae->leave($internal002b9273f6071af5ba1bb1735b230df6df8df0472332d354dc38a7de152d94aeprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formrest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/formrest.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formrest.html.php");
    }
}
