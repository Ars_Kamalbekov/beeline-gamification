<?php

/* KnpMenuBundle::menu.html.twig */
class TwigTemplatef7496512dbb56ee8ea326cc85aca2a0a59189e0de195048de2519657e0aef7c4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knpmenu.html.twig", "KnpMenuBundle::menu.html.twig", 1);
        $this->blocks = array(
            'label' => array($this, 'blocklabel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knpmenu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal049ba689c8693563c7f7948a63b22c667047ac42cd397f89fba16bde64f7345f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal049ba689c8693563c7f7948a63b22c667047ac42cd397f89fba16bde64f7345f->enter($internal049ba689c8693563c7f7948a63b22c667047ac42cd397f89fba16bde64f7345fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $internal8f945667f7b3ed5066e16ded70fdf009d8b9f7b296cf99d9badbf62e0ae72bd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8f945667f7b3ed5066e16ded70fdf009d8b9f7b296cf99d9badbf62e0ae72bd8->enter($internal8f945667f7b3ed5066e16ded70fdf009d8b9f7b296cf99d9badbf62e0ae72bd8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "KnpMenuBundle::menu.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal049ba689c8693563c7f7948a63b22c667047ac42cd397f89fba16bde64f7345f->leave($internal049ba689c8693563c7f7948a63b22c667047ac42cd397f89fba16bde64f7345fprof);

        
        $internal8f945667f7b3ed5066e16ded70fdf009d8b9f7b296cf99d9badbf62e0ae72bd8->leave($internal8f945667f7b3ed5066e16ded70fdf009d8b9f7b296cf99d9badbf62e0ae72bd8prof);

    }

    // line 3
    public function blocklabel($context, array $blocks = array())
    {
        $internald87dc1d693e098fcc02a62071d90b55dddcfa39a4f1c054b070fd513c5218d52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald87dc1d693e098fcc02a62071d90b55dddcfa39a4f1c054b070fd513c5218d52->enter($internald87dc1d693e098fcc02a62071d90b55dddcfa39a4f1c054b070fd513c5218d52prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal6b8d10324c1bfb795cc29c2137e62a47dd389600d43aa13edca5c2b49720b4ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b8d10324c1bfb795cc29c2137e62a47dd389600d43aa13edca5c2b49720b4ae->enter($internal6b8d10324c1bfb795cc29c2137e62a47dd389600d43aa13edca5c2b49720b4aeprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 4
        $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 4, $this->getSourceContext()); })()), "extra", array(0 => "translationdomain", 1 => "messages"), "method");
        // line 5
        $context["label"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 5, $this->getSourceContext()); })()), "label", array());
        // line 6
        if ( !((isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 6, $this->getSourceContext()); })()) === false)) {
            // line 7
            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 7, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 7, $this->getSourceContext()); })()), "extra", array(0 => "translationparams", 1 => array()), "method"), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 7, $this->getSourceContext()); })()));
        }
        // line 9
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 9, $this->getSourceContext()); })()), "allowsafelabels", array()) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 9, $this->getSourceContext()); })()), "extra", array(0 => "safelabel", 1 => false), "method"))) {
            echo (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 9, $this->getSourceContext()); })());
        } else {
            echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
        }
        
        $internal6b8d10324c1bfb795cc29c2137e62a47dd389600d43aa13edca5c2b49720b4ae->leave($internal6b8d10324c1bfb795cc29c2137e62a47dd389600d43aa13edca5c2b49720b4aeprof);

        
        $internald87dc1d693e098fcc02a62071d90b55dddcfa39a4f1c054b070fd513c5218d52->leave($internald87dc1d693e098fcc02a62071d90b55dddcfa39a4f1c054b070fd513c5218d52prof);

    }

    public function getTemplateName()
    {
        return "KnpMenuBundle::menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'knpmenu.html.twig' %}

{% block label %}
    {%- set translationdomain = item.extra('translationdomain', 'messages') -%}
    {%- set label = item.label -%}
    {%- if translationdomain is not same as(false) -%}
        {%- set label = label|trans(item.extra('translationparams', {}), translationdomain) -%}
    {%- endif -%}
    {%- if options.allowsafelabels and item.extra('safelabel', false) %}{{ label|raw }}{% else %}{{ label }}{% endif -%}
{% endblock %}
", "KnpMenuBundle::menu.html.twig", "/var/www/html/beeline-gamification/vendor/knplabs/knp-menu-bundle/Resources/views/menu.html.twig");
    }
}
