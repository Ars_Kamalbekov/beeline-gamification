<?php

/* FOSUserBundle:Group:list.html.twig */
class TwigTemplate15f387980dc216115357ada5d2eb1ab3fea716a95e86ce279d646754d0e25b29 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal900729c6e3b1267fd871a23d961f7155f579e823e4de048738125353b121b25e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal900729c6e3b1267fd871a23d961f7155f579e823e4de048738125353b121b25e->enter($internal900729c6e3b1267fd871a23d961f7155f579e823e4de048738125353b121b25eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $internal6b91e0a8549b8262f7402545a64cabf89f1dc5f593e36181ecc9a51e73bb719a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b91e0a8549b8262f7402545a64cabf89f1dc5f593e36181ecc9a51e73bb719a->enter($internal6b91e0a8549b8262f7402545a64cabf89f1dc5f593e36181ecc9a51e73bb719aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal900729c6e3b1267fd871a23d961f7155f579e823e4de048738125353b121b25e->leave($internal900729c6e3b1267fd871a23d961f7155f579e823e4de048738125353b121b25eprof);

        
        $internal6b91e0a8549b8262f7402545a64cabf89f1dc5f593e36181ecc9a51e73bb719a->leave($internal6b91e0a8549b8262f7402545a64cabf89f1dc5f593e36181ecc9a51e73bb719aprof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal7ec4bbcfe75618bde1a0c388c4e5a4cb200279a9f9a1e40bbedd2662e5912bd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7ec4bbcfe75618bde1a0c388c4e5a4cb200279a9f9a1e40bbedd2662e5912bd7->enter($internal7ec4bbcfe75618bde1a0c388c4e5a4cb200279a9f9a1e40bbedd2662e5912bd7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal424e1997af497327c548b60ed1b22b294fb654531264452797a50aaea8dee80c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal424e1997af497327c548b60ed1b22b294fb654531264452797a50aaea8dee80c->enter($internal424e1997af497327c548b60ed1b22b294fb654531264452797a50aaea8dee80cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/listcontent.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $internal424e1997af497327c548b60ed1b22b294fb654531264452797a50aaea8dee80c->leave($internal424e1997af497327c548b60ed1b22b294fb654531264452797a50aaea8dee80cprof);

        
        $internal7ec4bbcfe75618bde1a0c388c4e5a4cb200279a9f9a1e40bbedd2662e5912bd7->leave($internal7ec4bbcfe75618bde1a0c388c4e5a4cb200279a9f9a1e40bbedd2662e5912bd7prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Group/listcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Group:list.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}
