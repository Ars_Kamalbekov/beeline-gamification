<?php

/* WebProfilerBundle:Collector:logger.html.twig */
class TwigTemplatefdaeac407d83690e71e583300b1b54d535ca1cc7bd5213a0d4015e6d85cf74cf extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:logger.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf5291e816db573b4976839b6d3893962d5a0644d1f7a627d8e440233013fa334 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf5291e816db573b4976839b6d3893962d5a0644d1f7a627d8e440233013fa334->enter($internalf5291e816db573b4976839b6d3893962d5a0644d1f7a627d8e440233013fa334prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:logger.html.twig"));

        $internal5e7571077e27ffef1c9ca2ba2c1d8532a468f9bada63ab9d6ff8ae0c168e2b96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5e7571077e27ffef1c9ca2ba2c1d8532a468f9bada63ab9d6ff8ae0c168e2b96->enter($internal5e7571077e27ffef1c9ca2ba2c1d8532a468f9bada63ab9d6ff8ae0c168e2b96prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:logger.html.twig"));

        // line 3
        $context["helper"] = $this;
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf5291e816db573b4976839b6d3893962d5a0644d1f7a627d8e440233013fa334->leave($internalf5291e816db573b4976839b6d3893962d5a0644d1f7a627d8e440233013fa334prof);

        
        $internal5e7571077e27ffef1c9ca2ba2c1d8532a468f9bada63ab9d6ff8ae0c168e2b96->leave($internal5e7571077e27ffef1c9ca2ba2c1d8532a468f9bada63ab9d6ff8ae0c168e2b96prof);

    }

    // line 5
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal8cc382648cc135649d09a1a3049b3750b4735e1c60bafafd9a5e03e40ed78d5f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8cc382648cc135649d09a1a3049b3750b4735e1c60bafafd9a5e03e40ed78d5f->enter($internal8cc382648cc135649d09a1a3049b3750b4735e1c60bafafd9a5e03e40ed78d5fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internal89aa388c79d92c8b7664956bfbe4786cd8161805ee4a407cd46ed5db33ca040c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal89aa388c79d92c8b7664956bfbe4786cd8161805ee4a407cd46ed5db33ca040c->enter($internal89aa388c79d92c8b7664956bfbe4786cd8161805ee4a407cd46ed5db33ca040cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 6
        echo "    ";
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "counterrors", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "countdeprecations", array())) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "countwarnings", array()))) {
            // line 7
            echo "        ";
            obstart();
            // line 8
            echo "            ";
            $context["statuscolor"] = ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 8, $this->getSourceContext()); })()), "counterrors", array())) ? ("red") : ("yellow"));
            // line 9
            echo "            ";
            echo twiginclude($this->env, $context, "@WebProfiler/Icon/logger.svg");
            echo "
            <span class=\"sf-toolbar-value\">";
            // line 10
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "counterrors", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "counterrors", array())) : ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "countdeprecations", array()) + twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "countwarnings", array())))), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 12
            echo "
        ";
            // line 13
            obstart();
            // line 14
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Errors</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 16
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "counterrors", array())) ? ("red") : (""));
            echo "\">";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "counterrors", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "counterrors", array()), 0)) : (0)), "html", null, true);
            echo "</span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Warnings</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 21
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 21, $this->getSourceContext()); })()), "countwarnings", array())) ? ("yellow") : (""));
            echo "\">";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countwarnings", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countwarnings", array()), 0)) : (0)), "html", null, true);
            echo "</span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Deprecations</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-";
            // line 26
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 26, $this->getSourceContext()); })()), "countdeprecations", array())) ? ("yellow") : (""));
            echo "\">";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countdeprecations", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countdeprecations", array()), 0)) : (0)), "html", null, true);
            echo "</span>
            </div>
        ";
            $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 29
            echo "
        ";
            // line 30
            echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 30, $this->getSourceContext()); })()), "status" => (isset($context["statuscolor"]) || arraykeyexists("statuscolor", $context) ? $context["statuscolor"] : (function () { throw new TwigErrorRuntime('Variable "statuscolor" does not exist.', 30, $this->getSourceContext()); })())));
            echo "
    ";
        }
        
        $internal89aa388c79d92c8b7664956bfbe4786cd8161805ee4a407cd46ed5db33ca040c->leave($internal89aa388c79d92c8b7664956bfbe4786cd8161805ee4a407cd46ed5db33ca040cprof);

        
        $internal8cc382648cc135649d09a1a3049b3750b4735e1c60bafafd9a5e03e40ed78d5f->leave($internal8cc382648cc135649d09a1a3049b3750b4735e1c60bafafd9a5e03e40ed78d5fprof);

    }

    // line 34
    public function blockmenu($context, array $blocks = array())
    {
        $internal97d6910ea90893e14cee203b110b4264eb4d9d6c2c37eb385a57c9bcd57db245 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal97d6910ea90893e14cee203b110b4264eb4d9d6c2c37eb385a57c9bcd57db245->enter($internal97d6910ea90893e14cee203b110b4264eb4d9d6c2c37eb385a57c9bcd57db245prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internalac8978193e26411423856197a3689e50335e72411a88a8de5dfa8f2f2db964c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalac8978193e26411423856197a3689e50335e72411a88a8de5dfa8f2f2db964c1->enter($internalac8978193e26411423856197a3689e50335e72411a88a8de5dfa8f2f2db964c1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 35
        echo "    <span class=\"label label-status-";
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 35, $this->getSourceContext()); })()), "counterrors", array())) ? ("error") : ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 35, $this->getSourceContext()); })()), "countdeprecations", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 35, $this->getSourceContext()); })()), "countwarnings", array()))) ? ("warning") : (""))));
        echo " ";
        echo ((twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 35, $this->getSourceContext()); })()), "logs", array()))) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">";
        // line 36
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/logger.svg");
        echo "</span>
        <strong>Logs</strong>
        ";
        // line 38
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 38, $this->getSourceContext()); })()), "counterrors", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 38, $this->getSourceContext()); })()), "countdeprecations", array())) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 38, $this->getSourceContext()); })()), "countwarnings", array()))) {
            // line 39
            echo "            <span class=\"count\">
                <span>";
            // line 40
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "counterrors", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "counterrors", array())) : ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "countdeprecations", array()) + twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 40, $this->getSourceContext()); })()), "countwarnings", array())))), "html", null, true);
            echo "</span>
            </span>
        ";
        }
        // line 43
        echo "    </span>
";
        
        $internalac8978193e26411423856197a3689e50335e72411a88a8de5dfa8f2f2db964c1->leave($internalac8978193e26411423856197a3689e50335e72411a88a8de5dfa8f2f2db964c1prof);

        
        $internal97d6910ea90893e14cee203b110b4264eb4d9d6c2c37eb385a57c9bcd57db245->leave($internal97d6910ea90893e14cee203b110b4264eb4d9d6c2c37eb385a57c9bcd57db245prof);

    }

    // line 46
    public function blockpanel($context, array $blocks = array())
    {
        $internalea693da5b1e9c35cd896f64608098fbfdf0a8aa711ffe46f3d89e4fd9b7d3a67 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalea693da5b1e9c35cd896f64608098fbfdf0a8aa711ffe46f3d89e4fd9b7d3a67->enter($internalea693da5b1e9c35cd896f64608098fbfdf0a8aa711ffe46f3d89e4fd9b7d3a67prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internale290371df61fec3e96a304cc72abc3a3496caf28df5fa0b25ba6296c4693f711 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale290371df61fec3e96a304cc72abc3a3496caf28df5fa0b25ba6296c4693f711->enter($internale290371df61fec3e96a304cc72abc3a3496caf28df5fa0b25ba6296c4693f711prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 47
        echo "    <h2>Log Messages</h2>

    ";
        // line 49
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 49, $this->getSourceContext()); })()), "logs", array()))) {
            // line 50
            echo "        <div class=\"empty\">
            <p>No log messages available.</p>
        </div>
    ";
        } else {
            // line 54
            echo "        ";
            // line 55
            echo "        ";
            list($context["deprecationlogs"], $context["debuglogs"], $context["infoanderrorlogs"], $context["silencedlogs"]) =             array(array(), array(), array(), array());
            // line 56
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 56, $this->getSourceContext()); })()), "logs", array()));
            foreach ($context['seq'] as $context["key"] => $context["log"]) {
                // line 57
                echo "            ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "scream", array(), "any", true, true) &&  !twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "scream", array()))) {
                    // line 58
                    echo "                ";
                    $context["deprecationlogs"] = twigarraymerge((isset($context["deprecationlogs"]) || arraykeyexists("deprecationlogs", $context) ? $context["deprecationlogs"] : (function () { throw new TwigErrorRuntime('Variable "deprecationlogs" does not exist.', 58, $this->getSourceContext()); })()), array(0 => $context["log"]));
                    // line 59
                    echo "            ";
                } elseif ((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "scream", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "scream", array()))) {
                    // line 60
                    echo "                ";
                    $context["silencedlogs"] = twigarraymerge((isset($context["silencedlogs"]) || arraykeyexists("silencedlogs", $context) ? $context["silencedlogs"] : (function () { throw new TwigErrorRuntime('Variable "silencedlogs" does not exist.', 60, $this->getSourceContext()); })()), array(0 => $context["log"]));
                    // line 61
                    echo "            ";
                } elseif ((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "priorityName", array()) == "DEBUG")) {
                    // line 62
                    echo "                ";
                    $context["debuglogs"] = twigarraymerge((isset($context["debuglogs"]) || arraykeyexists("debuglogs", $context) ? $context["debuglogs"] : (function () { throw new TwigErrorRuntime('Variable "debuglogs" does not exist.', 62, $this->getSourceContext()); })()), array(0 => $context["log"]));
                    // line 63
                    echo "            ";
                } else {
                    // line 64
                    echo "                ";
                    $context["infoanderrorlogs"] = twigarraymerge((isset($context["infoanderrorlogs"]) || arraykeyexists("infoanderrorlogs", $context) ? $context["infoanderrorlogs"] : (function () { throw new TwigErrorRuntime('Variable "infoanderrorlogs" does not exist.', 64, $this->getSourceContext()); })()), array(0 => $context["log"]));
                    // line 65
                    echo "            ";
                }
                // line 66
                echo "        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['log'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 67
            echo "
        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Info. &amp; Errors <span class=\"badge status-";
            // line 70
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 70, $this->getSourceContext()); })()), "counterrors", array())) ? ("error") : (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 70, $this->getSourceContext()); })()), "countwarnings", array())) ? ("warning") : (""))));
            echo "\">";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 70, $this->getSourceContext()); })()), "counterrors", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 70, $this->getSourceContext()); })()), "counterrors", array())) : (twiglengthfilter($this->env, (isset($context["infoanderrorlogs"]) || arraykeyexists("infoanderrorlogs", $context) ? $context["infoanderrorlogs"] : (function () { throw new TwigErrorRuntime('Variable "infoanderrorlogs" does not exist.', 70, $this->getSourceContext()); })())))), "html", null, true);
            echo "</span></h3>
                <p class=\"text-muted\">Informational and error log messages generated during the execution of the application.</p>

                <div class=\"tab-content\">
                    ";
            // line 74
            if (twigtestempty((isset($context["infoanderrorlogs"]) || arraykeyexists("infoanderrorlogs", $context) ? $context["infoanderrorlogs"] : (function () { throw new TwigErrorRuntime('Variable "infoanderrorlogs" does not exist.', 74, $this->getSourceContext()); })()))) {
                // line 75
                echo "                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    ";
            } else {
                // line 79
                echo "                        ";
                echo $context["helper"]->macrorendertable((isset($context["infoanderrorlogs"]) || arraykeyexists("infoanderrorlogs", $context) ? $context["infoanderrorlogs"] : (function () { throw new TwigErrorRuntime('Variable "infoanderrorlogs" does not exist.', 79, $this->getSourceContext()); })()), "info", true);
                echo "
                    ";
            }
            // line 81
            echo "                </div>
            </div>

            <div class=\"tab\">
                ";
            // line 87
            echo "                <h3 class=\"tab-title\">Deprecations <span class=\"badge status-";
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 87, $this->getSourceContext()); })()), "countdeprecations", array())) ? ("warning") : (""));
            echo "\">";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countdeprecations", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countdeprecations", array()), 0)) : (0)), "html", null, true);
            echo "</span></h3>
                <p class=\"text-muted\">Log messages generated by using features marked as deprecated.</p>

                <div class=\"tab-content\">
                    ";
            // line 91
            if (twigtestempty((isset($context["deprecationlogs"]) || arraykeyexists("deprecationlogs", $context) ? $context["deprecationlogs"] : (function () { throw new TwigErrorRuntime('Variable "deprecationlogs" does not exist.', 91, $this->getSourceContext()); })()))) {
                // line 92
                echo "                        <div class=\"empty\">
                            <p>There are no log messages about deprecated features.</p>
                        </div>
                    ";
            } else {
                // line 96
                echo "                        ";
                echo $context["helper"]->macrorendertable((isset($context["deprecationlogs"]) || arraykeyexists("deprecationlogs", $context) ? $context["deprecationlogs"] : (function () { throw new TwigErrorRuntime('Variable "deprecationlogs" does not exist.', 96, $this->getSourceContext()); })()), "deprecation", false, true);
                echo "
                    ";
            }
            // line 98
            echo "                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Debug <span class=\"badge\">";
            // line 102
            echo twigescapefilter($this->env, twiglengthfilter($this->env, (isset($context["debuglogs"]) || arraykeyexists("debuglogs", $context) ? $context["debuglogs"] : (function () { throw new TwigErrorRuntime('Variable "debuglogs" does not exist.', 102, $this->getSourceContext()); })())), "html", null, true);
            echo "</span></h3>
                <p class=\"text-muted\">Unimportant log messages generated during the execution of the application.</p>

                <div class=\"tab-content\">
                    ";
            // line 106
            if (twigtestempty((isset($context["debuglogs"]) || arraykeyexists("debuglogs", $context) ? $context["debuglogs"] : (function () { throw new TwigErrorRuntime('Variable "debuglogs" does not exist.', 106, $this->getSourceContext()); })()))) {
                // line 107
                echo "                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    ";
            } else {
                // line 111
                echo "                        ";
                echo $context["helper"]->macrorendertable((isset($context["debuglogs"]) || arraykeyexists("debuglogs", $context) ? $context["debuglogs"] : (function () { throw new TwigErrorRuntime('Variable "debuglogs" does not exist.', 111, $this->getSourceContext()); })()), "debug");
                echo "
                    ";
            }
            // line 113
            echo "                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">PHP Notices <span class=\"badge\">";
            // line 117
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countscreams", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "countscreams", array()), 0)) : (0)), "html", null, true);
            echo "</span></h3>
                <p class=\"text-muted\">Log messages generated by PHP notices silenced with the @ operator.</p>

                <div class=\"tab-content\">
                    ";
            // line 121
            if (twigtestempty((isset($context["silencedlogs"]) || arraykeyexists("silencedlogs", $context) ? $context["silencedlogs"] : (function () { throw new TwigErrorRuntime('Variable "silencedlogs" does not exist.', 121, $this->getSourceContext()); })()))) {
                // line 122
                echo "                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    ";
            } else {
                // line 126
                echo "                        ";
                echo $context["helper"]->macrorendertable((isset($context["silencedlogs"]) || arraykeyexists("silencedlogs", $context) ? $context["silencedlogs"] : (function () { throw new TwigErrorRuntime('Variable "silencedlogs" does not exist.', 126, $this->getSourceContext()); })()), "silenced");
                echo "
                    ";
            }
            // line 128
            echo "                </div>
            </div>

            ";
            // line 131
            $context["compilerLogTotal"] = 0;
            // line 132
            echo "            ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 132, $this->getSourceContext()); })()), "compilerLogs", array()));
            foreach ($context['seq'] as $context["key"] => $context["logs"]) {
                // line 133
                echo "                ";
                $context["compilerLogTotal"] = ((isset($context["compilerLogTotal"]) || arraykeyexists("compilerLogTotal", $context) ? $context["compilerLogTotal"] : (function () { throw new TwigErrorRuntime('Variable "compilerLogTotal" does not exist.', 133, $this->getSourceContext()); })()) + twiglengthfilter($this->env, $context["logs"]));
                // line 134
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['logs'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 135
            echo "            <div class=\"tab\">
                <h3 class=\"tab-title\">Container <span class=\"badge\">";
            // line 136
            echo twigescapefilter($this->env, (isset($context["compilerLogTotal"]) || arraykeyexists("compilerLogTotal", $context) ? $context["compilerLogTotal"] : (function () { throw new TwigErrorRuntime('Variable "compilerLogTotal" does not exist.', 136, $this->getSourceContext()); })()), "html", null, true);
            echo "</span></h3>
                <p class=\"text-muted\">Log messages generated during the compilation of the service container.</p>

                <div class=\"tab-content\">
                    ";
            // line 140
            if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 140, $this->getSourceContext()); })()), "compilerLogs", array()))) {
                // line 141
                echo "                        <div class=\"empty\">
                            <p>There are no compiler log messages.</p>
                        </div>
                    ";
            } else {
                // line 145
                echo "                        <table class=\"logs\">
                            <thead>
                                <tr>
                                    <th class=\"full-width\">Class</th>
                                    <th>Messages</th>
                                </tr>
                            </thead>

                            <tbody>
                                ";
                // line 154
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 154, $this->getSourceContext()); })()), "compilerLogs", array()));
                $context['loop'] = array(
                  'parent' => $context['parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                    $length = count($context['seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['seq'] as $context["class"] => $context["logs"]) {
                    // line 155
                    echo "                                    <tr class=\"\">
                                        <td class=\"font-normal\">
                                            ";
                    // line 157
                    $context["contextid"] = ("context-compiler-" . twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()));
                    // line 158
                    echo "
                                             <a class=\"btn btn-link sf-toggle\" data-toggle-selector=\"#";
                    // line 159
                    echo twigescapefilter($this->env, (isset($context["contextid"]) || arraykeyexists("contextid", $context) ? $context["contextid"] : (function () { throw new TwigErrorRuntime('Variable "contextid" does not exist.', 159, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" data-toggle-alt-content=\"";
                    echo twigescapefilter($this->env, $context["class"], "html", null, true);
                    echo "\">";
                    echo twigescapefilter($this->env, $context["class"], "html", null, true);
                    echo "</a>

                                             <div id=\"";
                    // line 161
                    echo twigescapefilter($this->env, (isset($context["contextid"]) || arraykeyexists("contextid", $context) ? $context["contextid"] : (function () { throw new TwigErrorRuntime('Variable "contextid" does not exist.', 161, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" class=\"context sf-toggle-content sf-toggle-hidden\">
                                                <ul>
                                                ";
                    // line 163
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable($context["logs"]);
                    foreach ($context['seq'] as $context["key"] => $context["log"]) {
                        // line 164
                        echo "                                                    <li>";
                        echo $this->env->getExtension('Symfony\Bundle\WebProfilerBundle\Twig\WebProfilerExtension')->dumpLog($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "message", array()));
                        echo "</li>
                                                ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['log'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 166
                    echo "                                                </ul>
                                            </div>
                                        </td>
                                        <td class=\"font-normal text-right\">";
                    // line 169
                    echo twigescapefilter($this->env, twiglengthfilter($this->env, $context["logs"]), "html", null, true);
                    echo "</td>
                                    </tr>
                                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['class'], $context['logs'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 172
                echo "                            </tbody>
                        </table>
                    ";
            }
            // line 175
            echo "                </div>
            </div>

        </div>
    ";
        }
        
        $internale290371df61fec3e96a304cc72abc3a3496caf28df5fa0b25ba6296c4693f711->leave($internale290371df61fec3e96a304cc72abc3a3496caf28df5fa0b25ba6296c4693f711prof);

        
        $internalea693da5b1e9c35cd896f64608098fbfdf0a8aa711ffe46f3d89e4fd9b7d3a67->leave($internalea693da5b1e9c35cd896f64608098fbfdf0a8aa711ffe46f3d89e4fd9b7d3a67prof);

    }

    // line 182
    public function macrorendertable($logs = null, $category = "", $showlevel = false, $isdeprecation = false, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "logs" => $logs,
            "category" => $category,
            "showlevel" => $showlevel,
            "isdeprecation" => $isdeprecation,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internalebbcdcbfaddd5b4069f912679236875681174eb33544ec0126f0fbe517662538 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internalebbcdcbfaddd5b4069f912679236875681174eb33544ec0126f0fbe517662538->enter($internalebbcdcbfaddd5b4069f912679236875681174eb33544ec0126f0fbe517662538prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            $internal144579910b069d6bedcc303b35078c39aa1c86dda1f302d1ec528bab5e7615f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal144579910b069d6bedcc303b35078c39aa1c86dda1f302d1ec528bab5e7615f6->enter($internal144579910b069d6bedcc303b35078c39aa1c86dda1f302d1ec528bab5e7615f6prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            // line 183
            echo "    ";
            $context["helper"] = $this;
            // line 184
            echo "    ";
            $context["channelisdefined"] = twiggetattribute($this->env, $this->getSourceContext(), twigfirst($this->env, (isset($context["logs"]) || arraykeyexists("logs", $context) ? $context["logs"] : (function () { throw new TwigErrorRuntime('Variable "logs" does not exist.', 184, $this->getSourceContext()); })())), "channel", array(), "any", true, true);
            // line 185
            echo "
    <table class=\"logs\">
        <thead>
            <tr>
                <th>";
            // line 189
            echo (((isset($context["showlevel"]) || arraykeyexists("showlevel", $context) ? $context["showlevel"] : (function () { throw new TwigErrorRuntime('Variable "showlevel" does not exist.', 189, $this->getSourceContext()); })())) ? ("Level") : ("Time"));
            echo "</th>
                ";
            // line 190
            if ((isset($context["channelisdefined"]) || arraykeyexists("channelisdefined", $context) ? $context["channelisdefined"] : (function () { throw new TwigErrorRuntime('Variable "channelisdefined" does not exist.', 190, $this->getSourceContext()); })())) {
                echo "<th>Channel</th>";
            }
            // line 191
            echo "                <th class=\"full-width\">Message</th>
            </tr>
        </thead>

        <tbody>
            ";
            // line 196
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["logs"]) || arraykeyexists("logs", $context) ? $context["logs"] : (function () { throw new TwigErrorRuntime('Variable "logs" does not exist.', 196, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["log"]) {
                // line 197
                echo "                ";
                $context["cssclass"] = (((isset($context["isdeprecation"]) || arraykeyexists("isdeprecation", $context) ? $context["isdeprecation"] : (function () { throw new TwigErrorRuntime('Variable "isdeprecation" does not exist.', 197, $this->getSourceContext()); })())) ? ("") : (((twiginfilter(twiggetattribute($this->env, $this->getSourceContext(),                 // line 198
$context["log"], "priorityName", array()), array(0 => "CRITICAL", 1 => "ERROR", 2 => "ALERT", 3 => "EMERGENCY"))) ? ("status-error") : ((((twiggetattribute($this->env, $this->getSourceContext(),                 // line 199
$context["log"], "priorityName", array()) == "WARNING")) ? ("status-warning") : (""))))));
                // line 201
                echo "                <tr class=\"";
                echo twigescapefilter($this->env, (isset($context["cssclass"]) || arraykeyexists("cssclass", $context) ? $context["cssclass"] : (function () { throw new TwigErrorRuntime('Variable "cssclass" does not exist.', 201, $this->getSourceContext()); })()), "html", null, true);
                echo "\">
                    <td class=\"font-normal text-small\" nowrap>
                        ";
                // line 203
                if ((isset($context["showlevel"]) || arraykeyexists("showlevel", $context) ? $context["showlevel"] : (function () { throw new TwigErrorRuntime('Variable "showlevel" does not exist.', 203, $this->getSourceContext()); })())) {
                    // line 204
                    echo "                            <span class=\"colored text-bold\">";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "priorityName", array()), "html", null, true);
                    echo "</span>
                        ";
                }
                // line 206
                echo "                        <span class=\"text-muted newline\">";
                echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "timestamp", array()), "H:i:s"), "html", null, true);
                echo "</span>
                    </td>

                    ";
                // line 209
                if ((isset($context["channelisdefined"]) || arraykeyexists("channelisdefined", $context) ? $context["channelisdefined"] : (function () { throw new TwigErrorRuntime('Variable "channelisdefined" does not exist.', 209, $this->getSourceContext()); })())) {
                    // line 210
                    echo "                        <td class=\"font-normal text-small text-bold\" nowrap>
                            ";
                    // line 211
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "channel", array()), "html", null, true);
                    echo "
                            ";
                    // line 212
                    if ((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "errorCount", array(), "any", true, true) && (twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "errorCount", array()) > 1))) {
                        // line 213
                        echo "                                <span class=\"text-muted\">(";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "errorCount", array()), "html", null, true);
                        echo " times)</span>
                            ";
                    }
                    // line 215
                    echo "                        </td>

                    ";
                }
                // line 218
                echo "
                    <td class=\"font-normal\">";
                // line 219
                echo $context["helper"]->macrorenderlogmessage((isset($context["category"]) || arraykeyexists("category", $context) ? $context["category"] : (function () { throw new TwigErrorRuntime('Variable "category" does not exist.', 219, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), $context["log"]);
                echo "</td>
                </tr>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['log'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 222
            echo "        </tbody>
    </table>
";
            
            $internal144579910b069d6bedcc303b35078c39aa1c86dda1f302d1ec528bab5e7615f6->leave($internal144579910b069d6bedcc303b35078c39aa1c86dda1f302d1ec528bab5e7615f6prof);

            
            $internalebbcdcbfaddd5b4069f912679236875681174eb33544ec0126f0fbe517662538->leave($internalebbcdcbfaddd5b4069f912679236875681174eb33544ec0126f0fbe517662538prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    // line 226
    public function macrorenderlogmessage($category = null, $logindex = null, $log = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "category" => $category,
            "logindex" => $logindex,
            "log" => $log,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal66bc9ba9832d924735b6ae381545c6b4cb0b714c58bd0f944f4435ca09e1d417 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal66bc9ba9832d924735b6ae381545c6b4cb0b714c58bd0f944f4435ca09e1d417->enter($internal66bc9ba9832d924735b6ae381545c6b4cb0b714c58bd0f944f4435ca09e1d417prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderlogmessage"));

            $internal7745d6c1148ecdfebaf859e43a68baef131ac8293b67cdaf6ceb7b77657e1481 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal7745d6c1148ecdfebaf859e43a68baef131ac8293b67cdaf6ceb7b77657e1481->enter($internal7745d6c1148ecdfebaf859e43a68baef131ac8293b67cdaf6ceb7b77657e1481prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderlogmessage"));

            // line 227
            echo "    ";
            $context["hascontext"] = (twiggetattribute($this->env, $this->getSourceContext(), ($context["log"] ?? null), "context", array(), "any", true, true) &&  !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 227, $this->getSourceContext()); })()), "context", array())));
            // line 228
            echo "    ";
            $context["hastrace"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["log"] ?? null), "context", array(), "any", false, true), "exception", array(), "any", false, true), "trace", array(), "any", true, true);
            // line 229
            echo "
    ";
            // line 230
            if ( !(isset($context["hascontext"]) || arraykeyexists("hascontext", $context) ? $context["hascontext"] : (function () { throw new TwigErrorRuntime('Variable "hascontext" does not exist.', 230, $this->getSourceContext()); })())) {
                // line 231
                echo "        ";
                echo $this->env->getExtension('Symfony\Bundle\WebProfilerBundle\Twig\WebProfilerExtension')->dumpLog($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 231, $this->getSourceContext()); })()), "message", array()));
                echo "
    ";
            } else {
                // line 233
                echo "        ";
                echo $this->env->getExtension('Symfony\Bundle\WebProfilerBundle\Twig\WebProfilerExtension')->dumpLog($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 233, $this->getSourceContext()); })()), "message", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 233, $this->getSourceContext()); })()), "context", array()));
                echo "

        <div class=\"text-small font-normal\">
            ";
                // line 236
                $context["contextid"] = ((("context-" . (isset($context["category"]) || arraykeyexists("category", $context) ? $context["category"] : (function () { throw new TwigErrorRuntime('Variable "category" does not exist.', 236, $this->getSourceContext()); })())) . "-") . (isset($context["logindex"]) || arraykeyexists("logindex", $context) ? $context["logindex"] : (function () { throw new TwigErrorRuntime('Variable "logindex" does not exist.', 236, $this->getSourceContext()); })()));
                // line 237
                echo "            <a class=\"btn btn-link text-small sf-toggle\" data-toggle-selector=\"#";
                echo twigescapefilter($this->env, (isset($context["contextid"]) || arraykeyexists("contextid", $context) ? $context["contextid"] : (function () { throw new TwigErrorRuntime('Variable "contextid" does not exist.', 237, $this->getSourceContext()); })()), "html", null, true);
                echo "\" data-toggle-alt-content=\"Hide context\">Show context</a>

            ";
                // line 239
                if ((isset($context["hastrace"]) || arraykeyexists("hastrace", $context) ? $context["hastrace"] : (function () { throw new TwigErrorRuntime('Variable "hastrace" does not exist.', 239, $this->getSourceContext()); })())) {
                    // line 240
                    echo "                &nbsp;&nbsp;
                ";
                    // line 241
                    $context["traceid"] = ((("trace-" . (isset($context["category"]) || arraykeyexists("category", $context) ? $context["category"] : (function () { throw new TwigErrorRuntime('Variable "category" does not exist.', 241, $this->getSourceContext()); })())) . "-") . (isset($context["logindex"]) || arraykeyexists("logindex", $context) ? $context["logindex"] : (function () { throw new TwigErrorRuntime('Variable "logindex" does not exist.', 241, $this->getSourceContext()); })()));
                    // line 242
                    echo "                <a class=\"btn btn-link text-small sf-toggle\" data-toggle-selector=\"#";
                    echo twigescapefilter($this->env, (isset($context["traceid"]) || arraykeyexists("traceid", $context) ? $context["traceid"] : (function () { throw new TwigErrorRuntime('Variable "traceid" does not exist.', 242, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" data-toggle-alt-content=\"Hide trace\">Show trace</a>
            ";
                }
                // line 244
                echo "        </div>

        <div id=\"";
                // line 246
                echo twigescapefilter($this->env, (isset($context["contextid"]) || arraykeyexists("contextid", $context) ? $context["contextid"] : (function () { throw new TwigErrorRuntime('Variable "contextid" does not exist.', 246, $this->getSourceContext()); })()), "html", null, true);
                echo "\" class=\"context sf-toggle-content sf-toggle-hidden\">
            ";
                // line 247
                echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 247, $this->getSourceContext()); })()), "context", array()), 1));
                echo "
        </div>

        ";
                // line 250
                if ((isset($context["hastrace"]) || arraykeyexists("hastrace", $context) ? $context["hastrace"] : (function () { throw new TwigErrorRuntime('Variable "hastrace" does not exist.', 250, $this->getSourceContext()); })())) {
                    // line 251
                    echo "            <div id=\"";
                    echo twigescapefilter($this->env, (isset($context["traceid"]) || arraykeyexists("traceid", $context) ? $context["traceid"] : (function () { throw new TwigErrorRuntime('Variable "traceid" does not exist.', 251, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" class=\"context sf-toggle-content sf-toggle-hidden\">
                ";
                    // line 252
                    echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["log"]) || arraykeyexists("log", $context) ? $context["log"] : (function () { throw new TwigErrorRuntime('Variable "log" does not exist.', 252, $this->getSourceContext()); })()), "context", array()), "exception", array()), "trace", array()), 1));
                    echo "
            </div>
        ";
                }
                // line 255
                echo "    ";
            }
            
            $internal7745d6c1148ecdfebaf859e43a68baef131ac8293b67cdaf6ceb7b77657e1481->leave($internal7745d6c1148ecdfebaf859e43a68baef131ac8293b67cdaf6ceb7b77657e1481prof);

            
            $internal66bc9ba9832d924735b6ae381545c6b4cb0b714c58bd0f944f4435ca09e1d417->leave($internal66bc9ba9832d924735b6ae381545c6b4cb0b714c58bd0f944f4435ca09e1d417prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:logger.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  732 => 255,  726 => 252,  721 => 251,  719 => 250,  713 => 247,  709 => 246,  705 => 244,  699 => 242,  697 => 241,  694 => 240,  692 => 239,  686 => 237,  684 => 236,  677 => 233,  671 => 231,  669 => 230,  666 => 229,  663 => 228,  660 => 227,  640 => 226,  623 => 222,  606 => 219,  603 => 218,  598 => 215,  592 => 213,  590 => 212,  586 => 211,  583 => 210,  581 => 209,  574 => 206,  568 => 204,  566 => 203,  560 => 201,  558 => 199,  557 => 198,  555 => 197,  538 => 196,  531 => 191,  527 => 190,  523 => 189,  517 => 185,  514 => 184,  511 => 183,  490 => 182,  475 => 175,  470 => 172,  453 => 169,  448 => 166,  439 => 164,  435 => 163,  430 => 161,  421 => 159,  418 => 158,  416 => 157,  412 => 155,  395 => 154,  384 => 145,  378 => 141,  376 => 140,  369 => 136,  366 => 135,  360 => 134,  357 => 133,  352 => 132,  350 => 131,  345 => 128,  339 => 126,  333 => 122,  331 => 121,  324 => 117,  318 => 113,  312 => 111,  306 => 107,  304 => 106,  297 => 102,  291 => 98,  285 => 96,  279 => 92,  277 => 91,  267 => 87,  261 => 81,  255 => 79,  249 => 75,  247 => 74,  238 => 70,  233 => 67,  227 => 66,  224 => 65,  221 => 64,  218 => 63,  215 => 62,  212 => 61,  209 => 60,  206 => 59,  203 => 58,  200 => 57,  195 => 56,  192 => 55,  190 => 54,  184 => 50,  182 => 49,  178 => 47,  169 => 46,  158 => 43,  152 => 40,  149 => 39,  147 => 38,  142 => 36,  135 => 35,  126 => 34,  113 => 30,  110 => 29,  102 => 26,  92 => 21,  82 => 16,  78 => 14,  76 => 13,  73 => 12,  68 => 10,  63 => 9,  60 => 8,  57 => 7,  54 => 6,  45 => 5,  35 => 1,  33 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% import self as helper %}

{% block toolbar %}
    {% if collector.counterrors or collector.countdeprecations or collector.countwarnings %}
        {% set icon %}
            {% set statuscolor = collector.counterrors ? 'red' : 'yellow' %}
            {{ include('@WebProfiler/Icon/logger.svg') }}
            <span class=\"sf-toolbar-value\">{{ collector.counterrors ?: (collector.countdeprecations + collector.countwarnings) }}</span>
        {% endset %}

        {% set text %}
            <div class=\"sf-toolbar-info-piece\">
                <b>Errors</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.counterrors ? 'red' }}\">{{ collector.counterrors|default(0) }}</span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Warnings</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.countwarnings ? 'yellow' }}\">{{ collector.countwarnings|default(0) }}</span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Deprecations</b>
                <span class=\"sf-toolbar-status sf-toolbar-status-{{ collector.countdeprecations ? 'yellow' }}\">{{ collector.countdeprecations|default(0) }}</span>
            </div>
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl, status: statuscolor }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    <span class=\"label label-status-{{ collector.counterrors ? 'error' : collector.countdeprecations or collector.countwarnings ? 'warning' }} {{ collector.logs is empty ? 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/logger.svg') }}</span>
        <strong>Logs</strong>
        {% if collector.counterrors or collector.countdeprecations or collector.countwarnings %}
            <span class=\"count\">
                <span>{{ collector.counterrors ?: (collector.countdeprecations + collector.countwarnings) }}</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Log Messages</h2>

    {% if collector.logs is empty %}
        <div class=\"empty\">
            <p>No log messages available.</p>
        </div>
    {% else %}
        {# sort collected logs in groups #}
        {% set deprecationlogs, debuglogs, infoanderrorlogs, silencedlogs = [], [], [], [] %}
        {% for log in collector.logs %}
            {% if log.scream is defined and not log.scream %}
                {% set deprecationlogs = deprecationlogs|merge([log]) %}
            {% elseif log.scream is defined and log.scream %}
                {% set silencedlogs = silencedlogs|merge([log]) %}
            {% elseif log.priorityName == 'DEBUG' %}
                {% set debuglogs = debuglogs|merge([log]) %}
            {% else %}
                {% set infoanderrorlogs = infoanderrorlogs|merge([log]) %}
            {% endif %}
        {% endfor %}

        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Info. &amp; Errors <span class=\"badge status-{{ collector.counterrors ? 'error' : collector.countwarnings ? 'warning' }}\">{{ collector.counterrors ?: infoanderrorlogs|length }}</span></h3>
                <p class=\"text-muted\">Informational and error log messages generated during the execution of the application.</p>

                <div class=\"tab-content\">
                    {% if infoanderrorlogs is empty %}
                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    {% else %}
                        {{ helper.rendertable(infoanderrorlogs, 'info', true) }}
                    {% endif %}
                </div>
            </div>

            <div class=\"tab\">
                {# 'deprecationlogs|length' is not used because deprecations are
                now grouped and the group count doesn't match the message count #}
                <h3 class=\"tab-title\">Deprecations <span class=\"badge status-{{ collector.countdeprecations ? 'warning' }}\">{{ collector.countdeprecations|default(0) }}</span></h3>
                <p class=\"text-muted\">Log messages generated by using features marked as deprecated.</p>

                <div class=\"tab-content\">
                    {% if deprecationlogs is empty %}
                        <div class=\"empty\">
                            <p>There are no log messages about deprecated features.</p>
                        </div>
                    {% else %}
                        {{ helper.rendertable(deprecationlogs, 'deprecation', false, true) }}
                    {% endif %}
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Debug <span class=\"badge\">{{ debuglogs|length }}</span></h3>
                <p class=\"text-muted\">Unimportant log messages generated during the execution of the application.</p>

                <div class=\"tab-content\">
                    {% if debuglogs is empty %}
                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    {% else %}
                        {{ helper.rendertable(debuglogs, 'debug') }}
                    {% endif %}
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">PHP Notices <span class=\"badge\">{{ collector.countscreams|default(0) }}</span></h3>
                <p class=\"text-muted\">Log messages generated by PHP notices silenced with the @ operator.</p>

                <div class=\"tab-content\">
                    {% if silencedlogs is empty %}
                        <div class=\"empty\">
                            <p>There are no log messages of this level.</p>
                        </div>
                    {% else %}
                        {{ helper.rendertable(silencedlogs, 'silenced') }}
                    {% endif %}
                </div>
            </div>

            {% set compilerLogTotal = 0 %}
            {% for logs in collector.compilerLogs %}
                {% set compilerLogTotal = compilerLogTotal + logs|length %}
            {% endfor %}
            <div class=\"tab\">
                <h3 class=\"tab-title\">Container <span class=\"badge\">{{ compilerLogTotal }}</span></h3>
                <p class=\"text-muted\">Log messages generated during the compilation of the service container.</p>

                <div class=\"tab-content\">
                    {% if collector.compilerLogs is empty %}
                        <div class=\"empty\">
                            <p>There are no compiler log messages.</p>
                        </div>
                    {% else %}
                        <table class=\"logs\">
                            <thead>
                                <tr>
                                    <th class=\"full-width\">Class</th>
                                    <th>Messages</th>
                                </tr>
                            </thead>

                            <tbody>
                                {% for class, logs in collector.compilerLogs %}
                                    <tr class=\"\">
                                        <td class=\"font-normal\">
                                            {% set contextid = 'context-compiler-' ~ loop.index %}

                                             <a class=\"btn btn-link sf-toggle\" data-toggle-selector=\"#{{ contextid }}\" data-toggle-alt-content=\"{{ class }}\">{{ class }}</a>

                                             <div id=\"{{ contextid }}\" class=\"context sf-toggle-content sf-toggle-hidden\">
                                                <ul>
                                                {% for log in logs %}
                                                    <li>{{ profilerdumplog(log.message) }}</li>
                                                {% endfor %}
                                                </ul>
                                            </div>
                                        </td>
                                        <td class=\"font-normal text-right\">{{ logs|length }}</td>
                                    </tr>
                                {% endfor %}
                            </tbody>
                        </table>
                    {% endif %}
                </div>
            </div>

        </div>
    {% endif %}
{% endblock %}

{% macro rendertable(logs, category = '', showlevel = false, isdeprecation = false) %}
    {% import self as helper %}
    {% set channelisdefined = (logs|first).channel is defined %}

    <table class=\"logs\">
        <thead>
            <tr>
                <th>{{ showlevel ? 'Level' : 'Time' }}</th>
                {% if channelisdefined %}<th>Channel</th>{% endif %}
                <th class=\"full-width\">Message</th>
            </tr>
        </thead>

        <tbody>
            {% for log in logs %}
                {% set cssclass = isdeprecation ? ''
                    : log.priorityName in ['CRITICAL', 'ERROR', 'ALERT', 'EMERGENCY'] ? 'status-error'
                    : log.priorityName == 'WARNING' ? 'status-warning'
                %}
                <tr class=\"{{ cssclass }}\">
                    <td class=\"font-normal text-small\" nowrap>
                        {% if showlevel %}
                            <span class=\"colored text-bold\">{{ log.priorityName }}</span>
                        {% endif %}
                        <span class=\"text-muted newline\">{{ log.timestamp|date('H:i:s') }}</span>
                    </td>

                    {% if channelisdefined %}
                        <td class=\"font-normal text-small text-bold\" nowrap>
                            {{ log.channel }}
                            {% if log.errorCount is defined and log.errorCount > 1 %}
                                <span class=\"text-muted\">({{ log.errorCount }} times)</span>
                            {% endif %}
                        </td>

                    {% endif %}

                    <td class=\"font-normal\">{{ helper.renderlogmessage(category, loop.index, log) }}</td>
                </tr>
            {% endfor %}
        </tbody>
    </table>
{% endmacro %}

{% macro renderlogmessage(category, logindex, log) %}
    {% set hascontext = log.context is defined and log.context is not empty %}
    {% set hastrace = log.context.exception.trace is defined %}

    {% if not hascontext %}
        {{ profilerdumplog(log.message) }}
    {% else %}
        {{ profilerdumplog(log.message, log.context) }}

        <div class=\"text-small font-normal\">
            {% set contextid = 'context-' ~ category ~ '-' ~ logindex %}
            <a class=\"btn btn-link text-small sf-toggle\" data-toggle-selector=\"#{{ contextid }}\" data-toggle-alt-content=\"Hide context\">Show context</a>

            {% if hastrace %}
                &nbsp;&nbsp;
                {% set traceid = 'trace-' ~ category ~ '-' ~ logindex %}
                <a class=\"btn btn-link text-small sf-toggle\" data-toggle-selector=\"#{{ traceid }}\" data-toggle-alt-content=\"Hide trace\">Show trace</a>
            {% endif %}
        </div>

        <div id=\"{{ contextid }}\" class=\"context sf-toggle-content sf-toggle-hidden\">
            {{ profilerdump(log.context, maxDepth=1) }}
        </div>

        {% if hastrace %}
            <div id=\"{{ traceid }}\" class=\"context sf-toggle-content sf-toggle-hidden\">
                {{ profilerdump(log.context.exception.trace, maxDepth=1) }}
            </div>
        {% endif %}
    {% endif %}
{% endmacro %}
", "WebProfilerBundle:Collector:logger.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/logger.html.twig");
    }
}
