<?php

/* TwigBundle:Exception:error.html.twig */
class TwigTemplated7d5caf33cfd077bc770a239095d7f1ec1de7b0702162c75d85572d28f45d6f7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal06d696276af51349a5451590392a3ea7f3033cd10fef6f1f18920e57e78e59a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal06d696276af51349a5451590392a3ea7f3033cd10fef6f1f18920e57e78e59a1->enter($internal06d696276af51349a5451590392a3ea7f3033cd10fef6f1f18920e57e78e59a1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.html.twig"));

        $internal51fbaeb64cf79ef65d4d3f5e8f489b800f55edb1b078d1bddc6bfc468cb9ffbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal51fbaeb64cf79ef65d4d3f5e8f489b800f55edb1b078d1bddc6bfc468cb9ffbf->enter($internal51fbaeb64cf79ef65d4d3f5e8f489b800f55edb1b078d1bddc6bfc468cb9ffbfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twigescapefilter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <title>An Error Occurred: ";
        // line 5
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "</title>
    </head>
    <body>
        <h1>Oops! An Error Occurred</h1>
        <h2>The server returned a \"";
        // line 9
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 9, $this->getSourceContext()); })()), "html", null, true);
        echo "\".</h2>

        <div>
            Something is broken. Please let us know what you were doing when this error occurred.
            We will fix it as soon as possible. Sorry for any inconvenience caused.
        </div>
    </body>
</html>
";
        
        $internal06d696276af51349a5451590392a3ea7f3033cd10fef6f1f18920e57e78e59a1->leave($internal06d696276af51349a5451590392a3ea7f3033cd10fef6f1f18920e57e78e59a1prof);

        
        $internal51fbaeb64cf79ef65d4d3f5e8f489b800f55edb1b078d1bddc6bfc468cb9ffbf->leave($internal51fbaeb64cf79ef65d4d3f5e8f489b800f55edb1b078d1bddc6bfc468cb9ffbfprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 9,  34 => 5,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ charset }}\" />
        <title>An Error Occurred: {{ statustext }}</title>
    </head>
    <body>
        <h1>Oops! An Error Occurred</h1>
        <h2>The server returned a \"{{ statuscode }} {{ statustext }}\".</h2>

        <div>
            Something is broken. Please let us know what you were doing when this error occurred.
            We will fix it as soon as possible. Sorry for any inconvenience caused.
        </div>
    </body>
</html>
", "TwigBundle:Exception:error.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.html.twig");
    }
}
