<?php

/* @Framework/Form/hiddenrow.html.php */
class TwigTemplate37d39620cd56f677fd089766db290066156822bde67a14a08ee512c7155589af extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb551c2df2996eedcfb7e965e1afc75727cd9f2bf80ab44eb34d27d14c5969a11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb551c2df2996eedcfb7e965e1afc75727cd9f2bf80ab44eb34d27d14c5969a11->enter($internalb551c2df2996eedcfb7e965e1afc75727cd9f2bf80ab44eb34d27d14c5969a11prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/hiddenrow.html.php"));

        $internal7585ecefe072d1e43b9173d16476391b24ccd2b995048fc9441fbf31387bd1b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7585ecefe072d1e43b9173d16476391b24ccd2b995048fc9441fbf31387bd1b8->enter($internal7585ecefe072d1e43b9173d16476391b24ccd2b995048fc9441fbf31387bd1b8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/hiddenrow.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $internalb551c2df2996eedcfb7e965e1afc75727cd9f2bf80ab44eb34d27d14c5969a11->leave($internalb551c2df2996eedcfb7e965e1afc75727cd9f2bf80ab44eb34d27d14c5969a11prof);

        
        $internal7585ecefe072d1e43b9173d16476391b24ccd2b995048fc9441fbf31387bd1b8->leave($internal7585ecefe072d1e43b9173d16476391b24ccd2b995048fc9441fbf31387bd1b8prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hiddenrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hiddenrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hiddenrow.html.php");
    }
}
