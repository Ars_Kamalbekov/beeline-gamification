<?php

/* SonataBlockBundle:Block:blockcoremenu.html.twig */
class TwigTemplate4c39b7c1991892d4bb768cc1d0e78d228baab8486eb7f9d812d6bc399f9c7925 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockcoremenu.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale2bceb34284816ad655ee4cf69659d81b906ba167760144e8331ae47d62f4aa0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale2bceb34284816ad655ee4cf69659d81b906ba167760144e8331ae47d62f4aa0->enter($internale2bceb34284816ad655ee4cf69659d81b906ba167760144e8331ae47d62f4aa0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoremenu.html.twig"));

        $internal401eefca152eb1b40f591f99a8f1805fcac415289ecf2da1d2a5b532da76ea9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal401eefca152eb1b40f591f99a8f1805fcac415289ecf2da1d2a5b532da76ea9b->enter($internal401eefca152eb1b40f591f99a8f1805fcac415289ecf2da1d2a5b532da76ea9bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoremenu.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internale2bceb34284816ad655ee4cf69659d81b906ba167760144e8331ae47d62f4aa0->leave($internale2bceb34284816ad655ee4cf69659d81b906ba167760144e8331ae47d62f4aa0prof);

        
        $internal401eefca152eb1b40f591f99a8f1805fcac415289ecf2da1d2a5b532da76ea9b->leave($internal401eefca152eb1b40f591f99a8f1805fcac415289ecf2da1d2a5b532da76ea9bprof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal23292f99309d45e131edc50f496546fcf5d00134b77d0712caf3b3b33aaeb6dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal23292f99309d45e131edc50f496546fcf5d00134b77d0712caf3b3b33aaeb6dd->enter($internal23292f99309d45e131edc50f496546fcf5d00134b77d0712caf3b3b33aaeb6ddprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internale1857b569dc6e6826220daf1ce8946d5da3b791395de4d5cd2b03ab6f555d377 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale1857b569dc6e6826220daf1ce8946d5da3b791395de4d5cd2b03ab6f555d377->enter($internale1857b569dc6e6826220daf1ce8946d5da3b791395de4d5cd2b03ab6f555d377prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render((isset($context["menu"]) || arraykeyexists("menu", $context) ? $context["menu"] : (function () { throw new TwigErrorRuntime('Variable "menu" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["menuoptions"]) || arraykeyexists("menuoptions", $context) ? $context["menuoptions"] : (function () { throw new TwigErrorRuntime('Variable "menuoptions" does not exist.', 15, $this->getSourceContext()); })()));
        echo "
";
        
        $internale1857b569dc6e6826220daf1ce8946d5da3b791395de4d5cd2b03ab6f555d377->leave($internale1857b569dc6e6826220daf1ce8946d5da3b791395de4d5cd2b03ab6f555d377prof);

        
        $internal23292f99309d45e131edc50f496546fcf5d00134b77d0712caf3b3b33aaeb6dd->leave($internal23292f99309d45e131edc50f496546fcf5d00134b77d0712caf3b3b33aaeb6ddprof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockcoremenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    {{ knpmenurender(menu, menuoptions) }}
{% endblock %}
", "SonataBlockBundle:Block:blockcoremenu.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockcoremenu.html.twig");
    }
}
