<?php

/* SonataAdminBundle:CRUD/Association:listonetoone.html.twig */
class TwigTemplate27c54c870bca0db4529f8e61575bef5ff038f91ba021b7a085f7635787469b21 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD/Association:listonetoone.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal29dbd199b8cd1769423cd3d05035a90a1e9f13dcd2de284990fe51383aaa2e16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29dbd199b8cd1769423cd3d05035a90a1e9f13dcd2de284990fe51383aaa2e16->enter($internal29dbd199b8cd1769423cd3d05035a90a1e9f13dcd2de284990fe51383aaa2e16prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listonetoone.html.twig"));

        $internal37ec3819bce9e2358f48d3c9299ae30be14974d6621c09853ab380b6f514b387 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal37ec3819bce9e2358f48d3c9299ae30be14974d6621c09853ab380b6f514b387->enter($internal37ec3819bce9e2358f48d3c9299ae30be14974d6621c09853ab380b6f514b387prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listonetoone.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal29dbd199b8cd1769423cd3d05035a90a1e9f13dcd2de284990fe51383aaa2e16->leave($internal29dbd199b8cd1769423cd3d05035a90a1e9f13dcd2de284990fe51383aaa2e16prof);

        
        $internal37ec3819bce9e2358f48d3c9299ae30be14974d6621c09853ab380b6f514b387->leave($internal37ec3819bce9e2358f48d3c9299ae30be14974d6621c09853ab380b6f514b387prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalfc745be5028151367831f24d764af63526fffea89eccf8935eba53f62b72225f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfc745be5028151367831f24d764af63526fffea89eccf8935eba53f62b72225f->enter($internalfc745be5028151367831f24d764af63526fffea89eccf8935eba53f62b72225fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal97babe7410b4673a8d6662a809535e91dfc9dde339bd7d63566b2fe748869946 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal97babe7410b4673a8d6662a809535e91dfc9dde339bd7d63566b2fe748869946->enter($internal97babe7410b4673a8d6662a809535e91dfc9dde339bd7d63566b2fe748869946prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasAssociationAdmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "associationadmin", array()), "id", array(0 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 18
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())), "method"))) {
            // line 20
            echo "        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })())), "html", null, true);
            echo "</a>
    ";
        } else {
            // line 22
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 22, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })())), "html", null, true);
            echo "
    ";
        }
        
        $internal97babe7410b4673a8d6662a809535e91dfc9dde339bd7d63566b2fe748869946->leave($internal97babe7410b4673a8d6662a809535e91dfc9dde339bd7d63566b2fe748869946prof);

        
        $internalfc745be5028151367831f24d764af63526fffea89eccf8935eba53f62b72225f->leave($internalfc745be5028151367831f24d764af63526fffea89eccf8935eba53f62b72225fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:listonetoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 22,  57 => 20,  55 => 19,  54 => 18,  53 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasAssociationAdmin
    and fielddescription.associationadmin.id(value)
    and fielddescription.associationadmin.hasRoute(routename)
    and fielddescription.associationadmin.hasAccess(routename, value) %}
        <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, value, fielddescription.options.route.parameters) }}\">{{ value|renderrelationelement(fielddescription) }}</a>
    {% else %}
        {{ value|renderrelationelement(fielddescription) }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD/Association:listonetoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/listonetoone.html.twig");
    }
}
