<?php

/* FOSUserBundle:Resetting:request.html.twig */
class TwigTemplate703b21c660f656ecc3efefd5c7d2ccc98a196bfc370e306d169a07391790f157 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf12912c8908cbf74d702665684e426e8b58fe28dbf98245fd9a35ca391264617 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf12912c8908cbf74d702665684e426e8b58fe28dbf98245fd9a35ca391264617->enter($internalf12912c8908cbf74d702665684e426e8b58fe28dbf98245fd9a35ca391264617prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $internald8c6f519fa980add00fd68242c04c5e29a4980291f06c41026ffeaaa7fc279e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald8c6f519fa980add00fd68242c04c5e29a4980291f06c41026ffeaaa7fc279e4->enter($internald8c6f519fa980add00fd68242c04c5e29a4980291f06c41026ffeaaa7fc279e4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf12912c8908cbf74d702665684e426e8b58fe28dbf98245fd9a35ca391264617->leave($internalf12912c8908cbf74d702665684e426e8b58fe28dbf98245fd9a35ca391264617prof);

        
        $internald8c6f519fa980add00fd68242c04c5e29a4980291f06c41026ffeaaa7fc279e4->leave($internald8c6f519fa980add00fd68242c04c5e29a4980291f06c41026ffeaaa7fc279e4prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal1e38797dfeb66d1df3c9c59c8b321ba8f1dc6e59c4a620ab5be65580f897cdeb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1e38797dfeb66d1df3c9c59c8b321ba8f1dc6e59c4a620ab5be65580f897cdeb->enter($internal1e38797dfeb66d1df3c9c59c8b321ba8f1dc6e59c4a620ab5be65580f897cdebprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internald05fa8e4d074265682d83e59c6a251b985929b38396bd822f2644a36cc730e17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald05fa8e4d074265682d83e59c6a251b985929b38396bd822f2644a36cc730e17->enter($internald05fa8e4d074265682d83e59c6a251b985929b38396bd822f2644a36cc730e17prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/requestcontent.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $internald05fa8e4d074265682d83e59c6a251b985929b38396bd822f2644a36cc730e17->leave($internald05fa8e4d074265682d83e59c6a251b985929b38396bd822f2644a36cc730e17prof);

        
        $internal1e38797dfeb66d1df3c9c59c8b321ba8f1dc6e59c4a620ab5be65580f897cdeb->leave($internal1e38797dfeb66d1df3c9c59c8b321ba8f1dc6e59c4a620ab5be65580f897cdebprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Resetting/requestcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Resetting:request.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/request.html.twig");
    }
}
