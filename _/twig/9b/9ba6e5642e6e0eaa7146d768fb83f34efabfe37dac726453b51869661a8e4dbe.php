<?php

/* @Framework/Form/numberwidget.html.php */
class TwigTemplateda9bf3cd0acdae21491bcdd03e13709135cf118ffd543637b2d82b0c0583aeb2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9c490be074439ff82d84649cbe7db8ea8d9c98735e7107d6c3b569a01b90cba3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9c490be074439ff82d84649cbe7db8ea8d9c98735e7107d6c3b569a01b90cba3->enter($internal9c490be074439ff82d84649cbe7db8ea8d9c98735e7107d6c3b569a01b90cba3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/numberwidget.html.php"));

        $internala782ceb0305128eb13318f56294f6119c57c5f05d2adb9bb3ba8b3be2a925474 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala782ceb0305128eb13318f56294f6119c57c5f05d2adb9bb3ba8b3be2a925474->enter($internala782ceb0305128eb13318f56294f6119c57c5f05d2adb9bb3ba8b3be2a925474prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/numberwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $internal9c490be074439ff82d84649cbe7db8ea8d9c98735e7107d6c3b569a01b90cba3->leave($internal9c490be074439ff82d84649cbe7db8ea8d9c98735e7107d6c3b569a01b90cba3prof);

        
        $internala782ceb0305128eb13318f56294f6119c57c5f05d2adb9bb3ba8b3be2a925474->leave($internala782ceb0305128eb13318f56294f6119c57c5f05d2adb9bb3ba8b3be2a925474prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/numberwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/numberwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/numberwidget.html.php");
    }
}
