<?php

/* @Framework/Form/datewidget.html.php */
class TwigTemplate9b7d14ac8e953b2e0c7021bf205819150d8b3f9104d25fec6ec3d4d908928980 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale6e65f9d000628fd4b1f366ea194b618ef6e7439e115e90b6ebeb7c294327f9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale6e65f9d000628fd4b1f366ea194b618ef6e7439e115e90b6ebeb7c294327f9e->enter($internale6e65f9d000628fd4b1f366ea194b618ef6e7439e115e90b6ebeb7c294327f9eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/datewidget.html.php"));

        $internal81006456c7f6c139172e9d588741e858257dd5202cb0599135438d9c3c4b0fcd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81006456c7f6c139172e9d588741e858257dd5202cb0599135438d9c3c4b0fcd->enter($internal81006456c7f6c139172e9d588741e858257dd5202cb0599135438d9c3c4b0fcdprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/datewidget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php echo strreplace(array('";
        // line 5
        echo twigescapefilter($this->env, (isset($context["year"]) || arraykeyexists("year", $context) ? $context["year"] : (function () { throw new TwigErrorRuntime('Variable "year" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "', '";
        echo twigescapefilter($this->env, (isset($context["month"]) || arraykeyexists("month", $context) ? $context["month"] : (function () { throw new TwigErrorRuntime('Variable "month" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "', '";
        echo twigescapefilter($this->env, (isset($context["day"]) || arraykeyexists("day", $context) ? $context["day"] : (function () { throw new TwigErrorRuntime('Variable "day" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "'), array(
            \$view['form']->widget(\$form['year']),
            \$view['form']->widget(\$form['month']),
            \$view['form']->widget(\$form['day']),
        ), \$datepattern) ?>
    </div>
<?php endif ?>
";
        
        $internale6e65f9d000628fd4b1f366ea194b618ef6e7439e115e90b6ebeb7c294327f9e->leave($internale6e65f9d000628fd4b1f366ea194b618ef6e7439e115e90b6ebeb7c294327f9eprof);

        
        $internal81006456c7f6c139172e9d588741e858257dd5202cb0599135438d9c3c4b0fcd->leave($internal81006456c7f6c139172e9d588741e858257dd5202cb0599135438d9c3c4b0fcdprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datewidget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php echo strreplace(array('{{ year }}', '{{ month }}', '{{ day }}'), array(
            \$view['form']->widget(\$form['year']),
            \$view['form']->widget(\$form['month']),
            \$view['form']->widget(\$form['day']),
        ), \$datepattern) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datewidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datewidget.html.php");
    }
}
