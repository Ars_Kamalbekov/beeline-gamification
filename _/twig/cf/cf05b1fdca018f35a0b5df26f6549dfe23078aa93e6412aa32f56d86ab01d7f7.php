<?php

/* SonataBlockBundle:Block:blocksidemenutemplate.html.twig */
class TwigTemplatec5cb78f770496bb426312006d4a049ec9515ff8c55ef6d4151815469b51bb9b0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("knpmenu.html.twig", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig", 12);
        $this->blocks = array(
            'list' => array($this, 'blocklist'),
            'item' => array($this, 'blockitem'),
            'linkElement' => array($this, 'blocklinkElement'),
            'spanElement' => array($this, 'blockspanElement'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knpmenu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal29538690bd53fd6708901b725e4b43d7dbc7cbad542ca6f8b5de78d03fcb32ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29538690bd53fd6708901b725e4b43d7dbc7cbad542ca6f8b5de78d03fcb32ee->enter($internal29538690bd53fd6708901b725e4b43d7dbc7cbad542ca6f8b5de78d03fcb32eeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig"));

        $internal5e9b4b38b1b9def19df608ed6137e569de15d86fea622a41fcf1d0daf2c6e7aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5e9b4b38b1b9def19df608ed6137e569de15d86fea622a41fcf1d0daf2c6e7aa->enter($internal5e9b4b38b1b9def19df608ed6137e569de15d86fea622a41fcf1d0daf2c6e7aaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal29538690bd53fd6708901b725e4b43d7dbc7cbad542ca6f8b5de78d03fcb32ee->leave($internal29538690bd53fd6708901b725e4b43d7dbc7cbad542ca6f8b5de78d03fcb32eeprof);

        
        $internal5e9b4b38b1b9def19df608ed6137e569de15d86fea622a41fcf1d0daf2c6e7aa->leave($internal5e9b4b38b1b9def19df608ed6137e569de15d86fea622a41fcf1d0daf2c6e7aaprof);

    }

    // line 14
    public function blocklist($context, array $blocks = array())
    {
        $internale2e5d90bf4ac8253b368baf4516aeda33e50f7ae86dcdd6e4d814070c36317af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale2e5d90bf4ac8253b368baf4516aeda33e50f7ae86dcdd6e4d814070c36317af->enter($internale2e5d90bf4ac8253b368baf4516aeda33e50f7ae86dcdd6e4d814070c36317afprof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        $internal829dbf332082c9851687428e6b121970c287ea57f93a9725757eca4b03ecb26a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal829dbf332082c9851687428e6b121970c287ea57f93a9725757eca4b03ecb26a->enter($internal829dbf332082c9851687428e6b121970c287ea57f93a9725757eca4b03ecb26aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        // line 15
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig", 15);
        // line 16
        echo "    ";
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 16, $this->getSourceContext()); })()), "hasChildren", array()) &&  !(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 16, $this->getSourceContext()); })()), "depth", array()) === 0)) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 16, $this->getSourceContext()); })()), "displayChildren", array()))) {
            // line 17
            echo "        <div";
            echo $context["macros"]->macroattributes((isset($context["listAttributes"]) || arraykeyexists("listAttributes", $context) ? $context["listAttributes"] : (function () { throw new TwigErrorRuntime('Variable "listAttributes" does not exist.', 17, $this->getSourceContext()); })()));
            echo ">
            ";
            // line 18
            $this->displayBlock("children", $context, $blocks);
            echo "
        </div>
    ";
        }
        
        $internal829dbf332082c9851687428e6b121970c287ea57f93a9725757eca4b03ecb26a->leave($internal829dbf332082c9851687428e6b121970c287ea57f93a9725757eca4b03ecb26aprof);

        
        $internale2e5d90bf4ac8253b368baf4516aeda33e50f7ae86dcdd6e4d814070c36317af->leave($internale2e5d90bf4ac8253b368baf4516aeda33e50f7ae86dcdd6e4d814070c36317afprof);

    }

    // line 23
    public function blockitem($context, array $blocks = array())
    {
        $internalaf7db8274575a4417cdbf5f07d594a353cad4fa6c3fa6abcdce15aef78043899 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalaf7db8274575a4417cdbf5f07d594a353cad4fa6c3fa6abcdce15aef78043899->enter($internalaf7db8274575a4417cdbf5f07d594a353cad4fa6c3fa6abcdce15aef78043899prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        $internal3828488d94822f21f965c82409efdcccd9476dbf42602bca76a053b7cf5e2c9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3828488d94822f21f965c82409efdcccd9476dbf42602bca76a053b7cf5e2c9c->enter($internal3828488d94822f21f965c82409efdcccd9476dbf42602bca76a053b7cf5e2c9cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        // line 24
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig", 24);
        // line 25
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 25, $this->getSourceContext()); })()), "displayed", array())) {
            // line 26
            echo "        ";
            // line 27
            $context["classes"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 27, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 27, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 28
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 28, $this->getSourceContext()); })()), "isCurrent", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 28, $this->getSourceContext()); })())), "method")) {
                // line 29
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 29, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 29, $this->getSourceContext()); })()), "currentClass", array())));
            } elseif (twiggetattribute($this->env, $this->getSourceContext(),             // line 30
(isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 30, $this->getSourceContext()); })()), "isAncestor", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 30, $this->getSourceContext()); })()), 1 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 30, $this->getSourceContext()); })()), "depth", array())), "method")) {
                // line 31
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 31, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 31, $this->getSourceContext()); })()), "ancestorClass", array())));
            }
            // line 33
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 33, $this->getSourceContext()); })()), "actsLikeFirst", array())) {
                // line 34
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 34, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 34, $this->getSourceContext()); })()), "firstClass", array())));
            }
            // line 36
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 36, $this->getSourceContext()); })()), "actsLikeLast", array())) {
                // line 37
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 37, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 37, $this->getSourceContext()); })()), "lastClass", array())));
            }
            // line 39
            $context["attributes"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 39, $this->getSourceContext()); })()), "attributes", array());
            // line 40
            if ( !twigtestempty((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 40, $this->getSourceContext()); })()))) {
                // line 41
                $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 41, $this->getSourceContext()); })()), array("class" => twigjoinfilter((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 41, $this->getSourceContext()); })()), " ")));
            }
            // line 43
            echo "        ";
            // line 44
            if (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 44, $this->getSourceContext()); })()), "uri", array())) && ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 44, $this->getSourceContext()); })()), "current", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 44, $this->getSourceContext()); })()), "currentAsLink", array())))) {
                // line 45
                echo "            ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 47
                echo "            ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 49
            echo "        ";
            // line 50
            $context["childrenClasses"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 50, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 50, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 51
            $context["childrenClasses"] = twigarraymerge((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 51, $this->getSourceContext()); })()), array(0 => ("menulevel" . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 51, $this->getSourceContext()); })()), "level", array()))));
            // line 52
            $context["listAttributes"] = twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 52, $this->getSourceContext()); })()), "childrenAttributes", array()), array("class" => twigjoinfilter((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 52, $this->getSourceContext()); })()), " ")));
            // line 53
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    ";
        }
        
        $internal3828488d94822f21f965c82409efdcccd9476dbf42602bca76a053b7cf5e2c9c->leave($internal3828488d94822f21f965c82409efdcccd9476dbf42602bca76a053b7cf5e2c9cprof);

        
        $internalaf7db8274575a4417cdbf5f07d594a353cad4fa6c3fa6abcdce15aef78043899->leave($internalaf7db8274575a4417cdbf5f07d594a353cad4fa6c3fa6abcdce15aef78043899prof);

    }

    // line 57
    public function blocklinkElement($context, array $blocks = array())
    {
        $internalef1c44f48158c226be58d5f01c6dca660ad593b0eab92912dd7f5862cc21fe1e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalef1c44f48158c226be58d5f01c6dca660ad593b0eab92912dd7f5862cc21fe1e->enter($internalef1c44f48158c226be58d5f01c6dca660ad593b0eab92912dd7f5862cc21fe1eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        $internal81336d4b42d5e7689410988550ce39812e3d22313c825089b9fb4afaa0f0f3b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81336d4b42d5e7689410988550ce39812e3d22313c825089b9fb4afaa0f0f3b2->enter($internal81336d4b42d5e7689410988550ce39812e3d22313c825089b9fb4afaa0f0f3b2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 57, $this->getSourceContext()); })()), "uri", array()), "html", null, true);
        echo "\"";
        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["macros"]) || arraykeyexists("macros", $context) ? $context["macros"] : (function () { throw new TwigErrorRuntime('Variable "macros" does not exist.', 57, $this->getSourceContext()); })()), "attributes", array(0 => twigarraymerge(twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 57, $this->getSourceContext()); })()), "attributes", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 57, $this->getSourceContext()); })()), "linkAttributes", array())), (isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 57, $this->getSourceContext()); })()))), "method");
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>";
        
        $internal81336d4b42d5e7689410988550ce39812e3d22313c825089b9fb4afaa0f0f3b2->leave($internal81336d4b42d5e7689410988550ce39812e3d22313c825089b9fb4afaa0f0f3b2prof);

        
        $internalef1c44f48158c226be58d5f01c6dca660ad593b0eab92912dd7f5862cc21fe1e->leave($internalef1c44f48158c226be58d5f01c6dca660ad593b0eab92912dd7f5862cc21fe1eprof);

    }

    // line 59
    public function blockspanElement($context, array $blocks = array())
    {
        $internal8705faf56e718f8f3d02ff6445a8b2fc03f22eabb878f8e78e94d7918d1078b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8705faf56e718f8f3d02ff6445a8b2fc03f22eabb878f8e78e94d7918d1078b1->enter($internal8705faf56e718f8f3d02ff6445a8b2fc03f22eabb878f8e78e94d7918d1078b1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        $internaldd66ce39d02a0b0d7275474676485bade56137d1484659f13d27e5754649662c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldd66ce39d02a0b0d7275474676485bade56137d1484659f13d27e5754649662c->enter($internaldd66ce39d02a0b0d7275474676485bade56137d1484659f13d27e5754649662cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        echo "<div";
        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["macros"]) || arraykeyexists("macros", $context) ? $context["macros"] : (function () { throw new TwigErrorRuntime('Variable "macros" does not exist.', 59, $this->getSourceContext()); })()), "attributes", array(0 => twigarraymerge(twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 59, $this->getSourceContext()); })()), "attributes", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 59, $this->getSourceContext()); })()), "labelAttributes", array())), (isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 59, $this->getSourceContext()); })()))), "method");
        echo "><h4 class=\"list-group-item-heading\">";
        $this->displayBlock("label", $context, $blocks);
        echo "</h4></div>";
        
        $internaldd66ce39d02a0b0d7275474676485bade56137d1484659f13d27e5754649662c->leave($internaldd66ce39d02a0b0d7275474676485bade56137d1484659f13d27e5754649662cprof);

        
        $internal8705faf56e718f8f3d02ff6445a8b2fc03f22eabb878f8e78e94d7918d1078b1->leave($internal8705faf56e718f8f3d02ff6445a8b2fc03f22eabb878f8e78e94d7918d1078b1prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blocksidemenutemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 59,  154 => 57,  140 => 53,  138 => 52,  136 => 51,  134 => 50,  132 => 49,  128 => 47,  124 => 45,  122 => 44,  120 => 43,  117 => 41,  115 => 40,  113 => 39,  110 => 37,  108 => 36,  105 => 34,  103 => 33,  100 => 31,  98 => 30,  96 => 29,  94 => 28,  92 => 27,  90 => 26,  87 => 25,  85 => 24,  76 => 23,  62 => 18,  57 => 17,  54 => 16,  52 => 15,  43 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'knpmenu.html.twig' %}

{% block list %}
{% import 'knpmenu.html.twig' as macros %}
    {% if item.hasChildren and options.depth is not same as(0) and item.displayChildren %}
        <div{{ macros.attributes(listAttributes) }}>
            {{ block('children') }}
        </div>
    {% endif %}
{% endblock %}

{% block item %}
{% import 'knpmenu.html.twig' as macros %}
    {% if item.displayed %}
        {# building the class of the item #}
        {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}
        {%- if matcher.isCurrent(item) %}
            {%- set classes = classes|merge([options.currentClass]) %}
        {%- elseif matcher.isAncestor(item, options.depth) %}
            {%- set classes = classes|merge([options.ancestorClass]) %}
        {%- endif %}
        {%- if item.actsLikeFirst %}
            {%- set classes = classes|merge([options.firstClass]) %}
        {%- endif %}
        {%- if item.actsLikeLast %}
            {%- set classes = classes|merge([options.lastClass]) %}
        {%- endif %}
        {%- set attributes = item.attributes %}
        {%- if classes is not empty %}
            {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
        {%- endif %}
        {# displaying the item #}
        {%- if item.uri is not empty and (not item.current or options.currentAsLink) %}
            {{ block('linkElement') }}
        {%- else %}
            {{ block('spanElement') }}
        {%- endif %}
        {# render the list of children#}
        {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}
        {%- set childrenClasses = childrenClasses|merge(['menulevel' ~ item.level]) %}
        {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}
        {{ block('list') }}
    {% endif %}
{% endblock %}

{% block linkElement %}<a href=\"{{ item.uri }}\"{{ macros.attributes(item.attributes|merge(item.linkAttributes)|merge(attributes)) }}>{{ block('label') }}</a>{% endblock %}

{% block spanElement %}<div{{ macros.attributes(item.attributes|merge(item.labelAttributes)|merge(attributes)) }}><h4 class=\"list-group-item-heading\">{{ block('label') }}</h4></div>{% endblock %}
", "SonataBlockBundle:Block:blocksidemenutemplate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blocksidemenutemplate.html.twig");
    }
}
