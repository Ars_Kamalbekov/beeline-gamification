<?php

/* @Framework/Form/formend.html.php */
class TwigTemplate600f63cd281a9e60726933cd853393230768ba6e8e2b342e407497871fbfe6f0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal85bca3ada73c2de548e7469c999955c8061f9a6d7f0cf6cbc79a3327ffc8a088 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal85bca3ada73c2de548e7469c999955c8061f9a6d7f0cf6cbc79a3327ffc8a088->enter($internal85bca3ada73c2de548e7469c999955c8061f9a6d7f0cf6cbc79a3327ffc8a088prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formend.html.php"));

        $internal8c9ac77fa9535d177c86600357ee8b5babe1e82576bddcfd62d1be45123161b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8c9ac77fa9535d177c86600357ee8b5babe1e82576bddcfd62d1be45123161b0->enter($internal8c9ac77fa9535d177c86600357ee8b5babe1e82576bddcfd62d1be45123161b0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formend.html.php"));

        // line 1
        echo "<?php if (!isset(\$renderrest) || \$renderrest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $internal85bca3ada73c2de548e7469c999955c8061f9a6d7f0cf6cbc79a3327ffc8a088->leave($internal85bca3ada73c2de548e7469c999955c8061f9a6d7f0cf6cbc79a3327ffc8a088prof);

        
        $internal8c9ac77fa9535d177c86600357ee8b5babe1e82576bddcfd62d1be45123161b0->leave($internal8c9ac77fa9535d177c86600357ee8b5babe1e82576bddcfd62d1be45123161b0prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formend.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (!isset(\$renderrest) || \$renderrest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/formend.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formend.html.php");
    }
}
