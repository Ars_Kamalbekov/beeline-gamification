<?php

/* @WebProfiler/Icon/memory.svg */
class TwigTemplate5bcf5c2823640c56e91f74b4ce9967736e53cb9ab03ffacaf9baeb65f0807432 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4f8325478bf3a73d976d7e4a2389549f404a4ca43ed6cde5f733b4291a751a84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4f8325478bf3a73d976d7e4a2389549f404a4ca43ed6cde5f733b4291a751a84->enter($internal4f8325478bf3a73d976d7e4a2389549f404a4ca43ed6cde5f733b4291a751a84prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/memory.svg"));

        $internal2031d1e3af9a27cef2d0a0d1e98e69f0eac09bb7609b9ba74c0fda5015fda608 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2031d1e3af9a27cef2d0a0d1e98e69f0eac09bb7609b9ba74c0fda5015fda608->enter($internal2031d1e3af9a27cef2d0a0d1e98e69f0eac09bb7609b9ba74c0fda5015fda608prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/memory.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M6,18.9V15h12v3.9c0,0.7-0.2,1.1-1,1.1H7C6.2,20,6,19.6,6,18.9z M20,1C20,1,20,1,20,1c-0.6,0-1,0.5-1,1.1
    l0,18c0,0.5-0.4,0.9-0.9,0.9H5.9C5.4,21,5,20.6,5,20.1l0-18C5,1.5,4.6,1,4,1c0,0,0,0,0,0C3.5,1,3,1.5,3,2.1l0,18
    C3,21.7,4.3,23,5.9,23h12.2c1.6,0,2.9-1.3,2.9-2.9l0-18C21,1.5,20.6,1,20,1z M18,9H6v5h12V9z\"/>
</svg>
";
        
        $internal4f8325478bf3a73d976d7e4a2389549f404a4ca43ed6cde5f733b4291a751a84->leave($internal4f8325478bf3a73d976d7e4a2389549f404a4ca43ed6cde5f733b4291a751a84prof);

        
        $internal2031d1e3af9a27cef2d0a0d1e98e69f0eac09bb7609b9ba74c0fda5015fda608->leave($internal2031d1e3af9a27cef2d0a0d1e98e69f0eac09bb7609b9ba74c0fda5015fda608prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/memory.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M6,18.9V15h12v3.9c0,0.7-0.2,1.1-1,1.1H7C6.2,20,6,19.6,6,18.9z M20,1C20,1,20,1,20,1c-0.6,0-1,0.5-1,1.1
    l0,18c0,0.5-0.4,0.9-0.9,0.9H5.9C5.4,21,5,20.6,5,20.1l0-18C5,1.5,4.6,1,4,1c0,0,0,0,0,0C3.5,1,3,1.5,3,2.1l0,18
    C3,21.7,4.3,23,5.9,23h12.2c1.6,0,2.9-1.3,2.9-2.9l0-18C21,1.5,20.6,1,20,1z M18,9H6v5h12V9z\"/>
</svg>
", "@WebProfiler/Icon/memory.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/memory.svg");
    }
}
