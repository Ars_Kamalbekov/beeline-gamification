<?php

/* SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig */
class TwigTemplate54b2185b874cdddaf8202340d25fe4f49adf485f1b7fa39907236bafeb3b96b0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'relationlink' => array($this, 'blockrelationlink'),
            'relationvalue' => array($this, 'blockrelationvalue'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal644d0996390ceddebbdbc4193478d71e77d3aefa276bbc3b4b25479790cc508b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal644d0996390ceddebbdbc4193478d71e77d3aefa276bbc3b4b25479790cc508b->enter($internal644d0996390ceddebbdbc4193478d71e77d3aefa276bbc3b4b25479790cc508bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig"));

        $internal06bfea9a8b45e2a66c3fdd2a135f569e3e7241bc5d83e165f265f3c9249f4ade = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal06bfea9a8b45e2a66c3fdd2a135f569e3e7241bc5d83e165f265f3c9249f4ade->enter($internal06bfea9a8b45e2a66c3fdd2a135f569e3e7241bc5d83e165f265f3c9249f4adeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal644d0996390ceddebbdbc4193478d71e77d3aefa276bbc3b4b25479790cc508b->leave($internal644d0996390ceddebbdbc4193478d71e77d3aefa276bbc3b4b25479790cc508bprof);

        
        $internal06bfea9a8b45e2a66c3fdd2a135f569e3e7241bc5d83e165f265f3c9249f4ade->leave($internal06bfea9a8b45e2a66c3fdd2a135f569e3e7241bc5d83e165f265f3c9249f4adeprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal581603f9db610974433aaf392dd84e56debe7ae563c7876177389129aae3c1c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal581603f9db610974433aaf392dd84e56debe7ae563c7876177389129aae3c1c4->enter($internal581603f9db610974433aaf392dd84e56debe7ae563c7876177389129aae3c1c4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal944b70af51b6eee18d06dd17f190aedd2bf87c70a1e454e0b4f8d1d58b3f0823 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal944b70af51b6eee18d06dd17f190aedd2bf87c70a1e454e0b4f8d1d58b3f0823->enter($internal944b70af51b6eee18d06dd17f190aedd2bf87c70a1e454e0b4f8d1d58b3f0823prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 16, $this->getSourceContext()); })())), "method"))) {
            // line 17
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 18
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })()), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relationlink", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relationvalue", $context, $blocks);
                }
                // line 23
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 26, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relationvalue", $context, $blocks);
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 28
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 29
            echo "    ";
        }
        
        $internal944b70af51b6eee18d06dd17f190aedd2bf87c70a1e454e0b4f8d1d58b3f0823->leave($internal944b70af51b6eee18d06dd17f190aedd2bf87c70a1e454e0b4f8d1d58b3f0823prof);

        
        $internal581603f9db610974433aaf392dd84e56debe7ae563c7876177389129aae3c1c4->leave($internal581603f9db610974433aaf392dd84e56debe7ae563c7876177389129aae3c1c4prof);

    }

    // line 32
    public function blockrelationlink($context, array $blocks = array())
    {
        $internalbb48f153655fa66c45cb2bd37b6f56a9b8c75a5121c51c1adb8399b17f9a57cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbb48f153655fa66c45cb2bd37b6f56a9b8c75a5121c51c1adb8399b17f9a57cb->enter($internalbb48f153655fa66c45cb2bd37b6f56a9b8c75a5121c51c1adb8399b17f9a57cbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        $internalf982937b48d73c8bf3359f54309426637de99de66b8879cbc9c3bcc6abb11e99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf982937b48d73c8bf3359f54309426637de99de66b8879cbc9c3bcc6abb11e99->enter($internalf982937b48d73c8bf3359f54309426637de99de66b8879cbc9c3bcc6abb11e99prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        // line 33
        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 33, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 33, $this->getSourceContext()); })()), 1 => (isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 33, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 33, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 34
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 34, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })())), "html", null, true);
        // line 35
        echo "</a>";
        
        $internalf982937b48d73c8bf3359f54309426637de99de66b8879cbc9c3bcc6abb11e99->leave($internalf982937b48d73c8bf3359f54309426637de99de66b8879cbc9c3bcc6abb11e99prof);

        
        $internalbb48f153655fa66c45cb2bd37b6f56a9b8c75a5121c51c1adb8399b17f9a57cb->leave($internalbb48f153655fa66c45cb2bd37b6f56a9b8c75a5121c51c1adb8399b17f9a57cbprof);

    }

    // line 38
    public function blockrelationvalue($context, array $blocks = array())
    {
        $internale8d52726e81608800c1d834f1108250ff46421b6999f491f27db2a68301c5989 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale8d52726e81608800c1d834f1108250ff46421b6999f491f27db2a68301c5989->enter($internale8d52726e81608800c1d834f1108250ff46421b6999f491f27db2a68301c5989prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        $internaldac31849a296d8eee694e748e4cca4739e9b89ede53acc3d572d908d882fc4b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldac31849a296d8eee694e748e4cca4739e9b89ede53acc3d572d908d882fc4b7->enter($internaldac31849a296d8eee694e748e4cca4739e9b89ede53acc3d572d908d882fc4b7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        // line 39
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 39, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 39, $this->getSourceContext()); })())), "html", null, true);
        
        $internaldac31849a296d8eee694e748e4cca4739e9b89ede53acc3d572d908d882fc4b7->leave($internaldac31849a296d8eee694e748e4cca4739e9b89ede53acc3d572d908d882fc4b7prof);

        
        $internale8d52726e81608800c1d834f1108250ff46421b6999f491f27db2a68301c5989->leave($internale8d52726e81608800c1d834f1108250ff46421b6999f491f27db2a68301c5989prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 39,  177 => 38,  167 => 35,  165 => 34,  161 => 33,  152 => 32,  141 => 29,  127 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasassociationadmin and fielddescription.associationadmin.hasRoute(routename) %}
        {% for element in value %}
            {%- if fielddescription.associationadmin.hasAccess(routename, element) -%}
                {{ block('relationlink') }}
            {%- else -%}
                {{ block('relationvalue') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value %}
            {{ block('relationvalue') }}{% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relationlink -%}
    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
        {{- element|renderrelationelement(fielddescription) -}}
    </a>
{%- endblock -%}

{%- block relationvalue -%}
    {{- element|renderrelationelement(fielddescription) -}}
{%- endblock -%}
", "SonataDoctrineORMAdminBundle:CRUD:listormonetomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/listormonetomany.html.twig");
    }
}
