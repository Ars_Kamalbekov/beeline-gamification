<?php

/* @Framework/Form/formlabel.html.php */
class TwigTemplate89f46101038343584709dae3170c8b03ab2b21d99d66c44808e0f1b8da4fb97e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfa12900ee8fd207ff8f2daf1923d480bd7de4d25f349bc169fdbdcc50dffce88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfa12900ee8fd207ff8f2daf1923d480bd7de4d25f349bc169fdbdcc50dffce88->enter($internalfa12900ee8fd207ff8f2daf1923d480bd7de4d25f349bc169fdbdcc50dffce88prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formlabel.html.php"));

        $internalcce1f570d102573879865173afc3a6d9531969a7eac0923f31b2f716564e63b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcce1f570d102573879865173afc3a6d9531969a7eac0923f31b2f716564e63b4->enter($internalcce1f570d102573879865173afc3a6d9531969a7eac0923f31b2f716564e63b4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formlabel.html.php"));

        // line 1
        echo "<?php if (false !== \$label): ?>
<?php if (\$required) { \$labelattr['class'] = trim((isset(\$labelattr['class']) ? \$labelattr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$labelattr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$labelformat)
    ? strtr(\$labelformat, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label<?php if (\$labelattr) { echo ' '.\$view['form']->block(\$form, 'attributes', array('attr' => \$labelattr)); } ?>><?php echo \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$label, array(), \$translationdomain) : \$label) ?></label>
<?php endif ?>
";
        
        $internalfa12900ee8fd207ff8f2daf1923d480bd7de4d25f349bc169fdbdcc50dffce88->leave($internalfa12900ee8fd207ff8f2daf1923d480bd7de4d25f349bc169fdbdcc50dffce88prof);

        
        $internalcce1f570d102573879865173afc3a6d9531969a7eac0923f31b2f716564e63b4->leave($internalcce1f570d102573879865173afc3a6d9531969a7eac0923f31b2f716564e63b4prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formlabel.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (false !== \$label): ?>
<?php if (\$required) { \$labelattr['class'] = trim((isset(\$labelattr['class']) ? \$labelattr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$labelattr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$labelformat)
    ? strtr(\$labelformat, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label<?php if (\$labelattr) { echo ' '.\$view['form']->block(\$form, 'attributes', array('attr' => \$labelattr)); } ?>><?php echo \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$label, array(), \$translationdomain) : \$label) ?></label>
<?php endif ?>
", "@Framework/Form/formlabel.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formlabel.html.php");
    }
}
