<?php

/* @Swiftmailer/Collector/icon.svg */
class TwigTemplated5d89f8c1aed7fd60d0631e4d2062b79928e25195a7e6dee89ab10e66fb21150 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc349a7874dba78008273a2774946be254c4d146ad1fa5c870fe3856c4c25eb2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc349a7874dba78008273a2774946be254c4d146ad1fa5c870fe3856c4c25eb2a->enter($internalc349a7874dba78008273a2774946be254c4d146ad1fa5c870fe3856c4c25eb2aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Swiftmailer/Collector/icon.svg"));

        $internala3c06e9e2e55b59372184b12b986716c406477daf84f246fa10b408d40216fdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala3c06e9e2e55b59372184b12b986716c406477daf84f246fa10b408d40216fdc->enter($internala3c06e9e2e55b59372184b12b986716c406477daf84f246fa10b408d40216fdcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Swiftmailer/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M22,4.9C22,3.9,21.1,3,20.1,3H3.9C2.9,3,2,3.9,2,4.9v13.1C2,19.1,2.9,20,3.9,20h16.1c1.1,0,1.9-0.9,1.9-1.9V4.9z M8.3,14.1l-3.1,3.1c-0.2,0.2-0.5,0.3-0.7,0.3S4,17.4,3.8,17.2c-0.4-0.4-0.4-1,0-1.4l3.1-3.1c0.4-0.4,1-0.4,1.4,0S8.7,13.7,8.3,14.1z M20.4,17.2c-0.2,0.2-0.5,0.3-0.7,0.3s-0.5-0.1-0.7-0.3l-3.1-3.1c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l3.1,3.1C20.8,16.2,20.8,16.8,20.4,17.2z M20.4,7.2l-7.6,7.6c-0.2,0.2-0.5,0.3-0.7,0.3s-0.5-0.1-0.7-0.3L3.8,7.2c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l6.9,6.9L19,5.8c0.4-0.4,1-0.4,1.4,0S20.8,6.8,20.4,7.2z\"/>
</svg>
";
        
        $internalc349a7874dba78008273a2774946be254c4d146ad1fa5c870fe3856c4c25eb2a->leave($internalc349a7874dba78008273a2774946be254c4d146ad1fa5c870fe3856c4c25eb2aprof);

        
        $internala3c06e9e2e55b59372184b12b986716c406477daf84f246fa10b408d40216fdc->leave($internala3c06e9e2e55b59372184b12b986716c406477daf84f246fa10b408d40216fdcprof);

    }

    public function getTemplateName()
    {
        return "@Swiftmailer/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M22,4.9C22,3.9,21.1,3,20.1,3H3.9C2.9,3,2,3.9,2,4.9v13.1C2,19.1,2.9,20,3.9,20h16.1c1.1,0,1.9-0.9,1.9-1.9V4.9z M8.3,14.1l-3.1,3.1c-0.2,0.2-0.5,0.3-0.7,0.3S4,17.4,3.8,17.2c-0.4-0.4-0.4-1,0-1.4l3.1-3.1c0.4-0.4,1-0.4,1.4,0S8.7,13.7,8.3,14.1z M20.4,17.2c-0.2,0.2-0.5,0.3-0.7,0.3s-0.5-0.1-0.7-0.3l-3.1-3.1c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l3.1,3.1C20.8,16.2,20.8,16.8,20.4,17.2z M20.4,7.2l-7.6,7.6c-0.2,0.2-0.5,0.3-0.7,0.3s-0.5-0.1-0.7-0.3L3.8,7.2c-0.4-0.4-0.4-1,0-1.4s1-0.4,1.4,0l6.9,6.9L19,5.8c0.4-0.4,1-0.4,1.4,0S20.8,6.8,20.4,7.2z\"/>
</svg>
", "@Swiftmailer/Collector/icon.svg", "/var/www/html/beeline-gamification/vendor/symfony/swiftmailer-bundle/Resources/views/Collector/icon.svg");
    }
}
