<?php

/* SonataAdminBundle:CRUD:showchoice.html.twig */
class TwigTemplate0cda97d96f73f58ed8c88202ff8ff4f4b0da769cb5992f6572292a9d1376858a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 11
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showchoice.html.twig", 11);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7f1a95dc60490142acdbb5fe24c02f8d9e6f76742f6e1f3d11042337f4912596 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7f1a95dc60490142acdbb5fe24c02f8d9e6f76742f6e1f3d11042337f4912596->enter($internal7f1a95dc60490142acdbb5fe24c02f8d9e6f76742f6e1f3d11042337f4912596prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showchoice.html.twig"));

        $internal2ca23772b9562b45b330fc080497902008e2b4fa36fc09d667b5952f2c5655ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2ca23772b9562b45b330fc080497902008e2b4fa36fc09d667b5952f2c5655ec->enter($internal2ca23772b9562b45b330fc080497902008e2b4fa36fc09d667b5952f2c5655ecprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showchoice.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal7f1a95dc60490142acdbb5fe24c02f8d9e6f76742f6e1f3d11042337f4912596->leave($internal7f1a95dc60490142acdbb5fe24c02f8d9e6f76742f6e1f3d11042337f4912596prof);

        
        $internal2ca23772b9562b45b330fc080497902008e2b4fa36fc09d667b5952f2c5655ec->leave($internal2ca23772b9562b45b330fc080497902008e2b4fa36fc09d667b5952f2c5655ecprof);

    }

    // line 13
    public function blockfield($context, array $blocks = array())
    {
        $internal235c3e474223f4e85ff0b3ed418038d7f923c96129a2fe5e9cb47e2acc0c8f93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal235c3e474223f4e85ff0b3ed418038d7f923c96129a2fe5e9cb47e2acc0c8f93->enter($internal235c3e474223f4e85ff0b3ed418038d7f923c96129a2fe5e9cb47e2acc0c8f93prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal7868fe0ab2acb900d7d52a080815c11a38105cef5605ef04cbc266c2df798f1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7868fe0ab2acb900d7d52a080815c11a38105cef5605ef04cbc266c2df798f1e->enter($internal7868fe0ab2acb900d7d52a080815c11a38105cef5605ef04cbc266c2df798f1eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 14
        obstart();
        // line 15
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "choices", array(), "any", true, true)) {
            // line 16
            echo "        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "multiple", array(), "any", true, true) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "multiple", array()) == true)) && twigtestiterable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })())))) {
                // line 17
                echo "
            ";
                // line 18
                $context["result"] = "";
                // line 19
                echo "            ";
                $context["delimiter"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "delimiter", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "delimiter", array()), ", ")) : (", "));
                // line 20
                echo "
            ";
                // line 21
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })()));
                foreach ($context['seq'] as $context["key"] => $context["val"]) {
                    // line 22
                    echo "                ";
                    if ( !twigtestempty((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 22, $this->getSourceContext()); })()))) {
                        // line 23
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 23, $this->getSourceContext()); })()) . (isset($context["delimiter"]) || arraykeyexists("delimiter", $context) ? $context["delimiter"] : (function () { throw new TwigErrorRuntime('Variable "delimiter" does not exist.', 23, $this->getSourceContext()); })()));
                        // line 24
                        echo "                ";
                    }
                    // line 25
                    echo "
                ";
                    // line 26
                    if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "choices", array(), "any", false, true), $context["val"], array(), "array", true, true)) {
                        // line 27
                        echo "                    ";
                        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
                            // line 28
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 28, $this->getSourceContext()); })()) . twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 28, $this->getSourceContext()); })()), "options", array()), "choices", array()), $context["val"], array(), "array"));
                            // line 29
                            echo "                    ";
                        } else {
                            // line 30
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 30, $this->getSourceContext()); })()) . $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 30, $this->getSourceContext()); })()), "options", array()), "choices", array()), $context["val"], array(), "array"), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 30, $this->getSourceContext()); })()), "options", array()), "catalogue", array())));
                            // line 31
                            echo "                    ";
                        }
                        // line 32
                        echo "                ";
                    } else {
                        // line 33
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 33, $this->getSourceContext()); })()) . $context["val"]);
                        // line 34
                        echo "                ";
                    }
                    // line 35
                    echo "            ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['val'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 36
                echo "
            ";
                // line 37
                $context["value"] = (isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 37, $this->getSourceContext()); })());
                // line 38
                echo "
        ";
            } elseif (twiginfilter(            // line 39
(isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 39, $this->getSourceContext()); })()), twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 39, $this->getSourceContext()); })()), "options", array()), "choices", array())))) {
                // line 40
                echo "            ";
                if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
                    // line 41
                    echo "                ";
                    $context["value"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 41, $this->getSourceContext()); })()), "options", array()), "choices", array()), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 41, $this->getSourceContext()); })()), array(), "array");
                    // line 42
                    echo "            ";
                } else {
                    // line 43
                    echo "                ";
                    $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 43, $this->getSourceContext()); })()), "options", array()), "choices", array()), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 43, $this->getSourceContext()); })()), array(), "array"), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 43, $this->getSourceContext()); })()), "options", array()), "catalogue", array()));
                    // line 44
                    echo "            ";
                }
                // line 45
                echo "        ";
            }
            // line 46
            echo "    ";
        }
        // line 47
        echo "
    ";
        // line 48
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 48, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
            // line 49
            echo "        ";
            echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 49, $this->getSourceContext()); })());
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 51, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        // line 53
        echo "
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal7868fe0ab2acb900d7d52a080815c11a38105cef5605ef04cbc266c2df798f1e->leave($internal7868fe0ab2acb900d7d52a080815c11a38105cef5605ef04cbc266c2df798f1eprof);

        
        $internal235c3e474223f4e85ff0b3ed418038d7f923c96129a2fe5e9cb47e2acc0c8f93->leave($internal235c3e474223f4e85ff0b3ed418038d7f923c96129a2fe5e9cb47e2acc0c8f93prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showchoice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 53,  158 => 51,  152 => 49,  150 => 48,  147 => 47,  144 => 46,  141 => 45,  138 => 44,  135 => 43,  132 => 42,  129 => 41,  126 => 40,  124 => 39,  121 => 38,  119 => 37,  116 => 36,  110 => 35,  107 => 34,  104 => 33,  101 => 32,  98 => 31,  95 => 30,  92 => 29,  89 => 28,  86 => 27,  84 => 26,  81 => 25,  78 => 24,  75 => 23,  72 => 22,  68 => 21,  65 => 20,  62 => 19,  60 => 18,  57 => 17,  54 => 16,  51 => 15,  49 => 14,  40 => 13,  11 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field%}
{% spaceless %}
    {% if fielddescription.options.choices  is defined %}
        {% if fielddescription.options.multiple is defined and fielddescription.options.multiple==true and value is iterable %}

            {% set result = '' %}
            {% set delimiter = fielddescription.options.delimiter|default(', ') %}

            {% for val in value %}
                {% if result is not empty %}
                    {% set result = result ~ delimiter %}
                {% endif %}

                {% if fielddescription.options.choices[val] is defined %}
                    {% if fielddescription.options.catalogue is not defined %}
                        {% set result = result ~ fielddescription.options.choices[val] %}
                    {% else %}
                        {% set result = result ~ fielddescription.options.choices[val]|trans({}, fielddescription.options.catalogue) %}
                    {% endif %}
                {% else %}
                    {% set result = result ~ val %}
                {% endif %}
            {% endfor %}

            {% set value = result %}

        {% elseif value in fielddescription.options.choices|keys %}
            {% if fielddescription.options.catalogue is not defined %}
                {% set value = fielddescription.options.choices[value] %}
            {% else %}
                {% set value = fielddescription.options.choices[value]|trans({}, fielddescription.options.catalogue) %}
            {% endif %}
        {% endif %}
    {% endif %}

    {% if fielddescription.options.safe %}
        {{ value|raw }}
    {% else %}
        {{ value }}
    {% endif %}

{% endspaceless %}
{% endblock %}
", "SonataAdminBundle:CRUD:showchoice.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showchoice.html.twig");
    }
}
