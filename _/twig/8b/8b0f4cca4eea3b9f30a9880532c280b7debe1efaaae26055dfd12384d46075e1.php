<?php

/* SonataAdminBundle:CRUD/Association:editmanyscript.html.twig */
class TwigTemplate2158a25bd7eff65fd962b73337cd4ba824417e4c119bc90b760769dd15e9bcb9 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfe2b969a31f2006ac5d6d915782a742614d365dcad86aa3174cc7ebfadcd6d4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfe2b969a31f2006ac5d6d915782a742614d365dcad86aa3174cc7ebfadcd6d4a->enter($internalfe2b969a31f2006ac5d6d915782a742614d365dcad86aa3174cc7ebfadcd6d4aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmanyscript.html.twig"));

        $internal57015e3a7cc1642fe84cd358fe7d2d20e672b1619c026f6fe90d8606ecf44aec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal57015e3a7cc1642fe84cd358fe7d2d20e672b1619c026f6fe90d8606ecf44aec->enter($internal57015e3a7cc1642fe84cd358fe7d2d20e672b1619c026f6fe90d8606ecf44aecprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmanyscript.html.twig"));

        // line 11
        echo "

";
        // line 18
        echo "
";
        // line 20
        echo "
";
        // line 21
        $context["associationadmin"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 21, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array());
        // line 22
        echo "
<!-- edit many association -->

<script type=\"text/javascript\">

    ";
        // line 32
        echo "
    var fielddialogformlistlink";
        // line 33
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 33, $this->getSourceContext()); })());
        echo " = function(event) {
        initializepopup";
        // line 34
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 34, $this->getSourceContext()); })());
        echo "();

        var target = jQuery(this);

        if (target.attr('href').length === 0 || target.attr('href') === '#') {
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[";
        // line 45
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 45, $this->getSourceContext()); })());
        echo "|fielddialogformlistlink] handle link click in a list');

        var element = jQuery(this).parents('#fielddialog";
        // line 47
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 47, $this->getSourceContext()); })());
        echo " .sonata-ba-list-field');

        // the user does not click on a row column
        if (element.length == 0) {
            Admin.log('[";
        // line 51
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 51, $this->getSourceContext()); })());
        echo "|fielddialogformlistlink] the user does not click on a row column, make ajax call to retrieve inner html');
            // make a recursive call (ie: reset the filter)
            jQuery.ajax({
                type: 'GET',
                url: jQuery(this).attr('href'),
                dataType: 'html',
                success: function(html) {
                    Admin.log('[";
        // line 58
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 58, $this->getSourceContext()); })());
        echo "|fielddialogformlistlink] callback success, attach valid js event');

                    fielddialogcontent";
        // line 60
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 60, $this->getSourceContext()); })());
        echo ".html(html);
                    fielddialogformlisthandleaction";
        // line 61
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 61, $this->getSourceContext()); })());
        echo "();

                    Admin.sharedsetup(fielddialog";
        // line 63
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 63, $this->getSourceContext()); })());
        echo ");
                }
            });

            return;
        }

        Admin.log('[";
        // line 70
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 70, $this->getSourceContext()); })());
        echo "|fielddialogformlistlink] the user select one element, update input and hide the modal');

        jQuery('#";
        // line 72
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 72, $this->getSourceContext()); })());
        echo "').val(element.attr('objectId'));
        jQuery('#";
        // line 73
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 73, $this->getSourceContext()); })());
        echo "').trigger('change');

        fielddialog";
        // line 75
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 75, $this->getSourceContext()); })());
        echo ".modal('hide');
    };

    // this function handle action on the modal list when inside a selected list
    var fielddialogformlisthandleaction";
        // line 79
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 79, $this->getSourceContext()); })());
        echo "  =  function() {
        Admin.log('[";
        // line 80
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 80, $this->getSourceContext()); })());
        echo "|fielddialogformlisthandleaction] attaching valid js event');

        // capture the submit event to make an ajax call, ie : POST data to the
        // related create admin
        jQuery('a', fielddialog";
        // line 84
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 84, $this->getSourceContext()); })());
        echo ").on('click', fielddialogformlistlink";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 84, $this->getSourceContext()); })());
        echo ");
        jQuery('form', fielddialog";
        // line 85
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 85, $this->getSourceContext()); })());
        echo ").on('submit', function(event) {
            event.preventDefault();

            var form = jQuery(this);

            Admin.log('[";
        // line 90
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 90, $this->getSourceContext()); })());
        echo "|fielddialogformlisthandleaction] catching submit event, sending ajax request');

            jQuery(form).ajaxSubmit({
                type: form.attr('method'),
                url: form.attr('action'),
                dataType: 'html',
                data: {xmlhttprequest: true},
                success: function(html) {

                    Admin.log('[";
        // line 99
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 99, $this->getSourceContext()); })());
        echo "|fielddialogformlisthandleaction] form submit success, restoring event');

                    fielddialogcontent";
        // line 101
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 101, $this->getSourceContext()); })());
        echo ".html(html);
                    fielddialogformlisthandleaction";
        // line 102
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 102, $this->getSourceContext()); })());
        echo "();

                    Admin.sharedsetup(fielddialog";
        // line 104
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 104, $this->getSourceContext()); })());
        echo ");
                }
            });
        });
    };

    // handle the list link
    var fielddialogformlist";
        // line 111
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 111, $this->getSourceContext()); })());
        echo " = function(event) {

        initializepopup";
        // line 113
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 113, $this->getSourceContext()); })());
        echo "();

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[";
        // line 118
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 118, $this->getSourceContext()); })());
        echo "|fielddialogformlist] open the list modal');

        var a = jQuery(this);

        fielddialogcontent";
        // line 122
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 122, $this->getSourceContext()); })());
        echo ".html('');

        // retrieve the form element from the related admin generator
        jQuery.ajax({
            url: a.attr('href'),
            dataType: 'html',
            success: function(html) {

                Admin.log('[";
        // line 130
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 130, $this->getSourceContext()); })());
        echo "|fielddialogformlist] retrieving the list content');

                // populate the popup container
                fielddialogcontent";
        // line 133
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 133, $this->getSourceContext()); })());
        echo ".html(html);

                fielddialogtitle";
        // line 135
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 135, $this->getSourceContext()); })());
        echo ".html(\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 135, $this->getSourceContext()); })()), "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 135, $this->getSourceContext()); })()), "translationdomain", array()));
        echo "\");

                Admin.sharedsetup(fielddialog";
        // line 137
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 137, $this->getSourceContext()); })());
        echo ");

                fielddialogformlisthandleaction";
        // line 139
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 139, $this->getSourceContext()); })());
        echo "();

                // open the dialog in modal mode
                fielddialog";
        // line 142
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 142, $this->getSourceContext()); })());
        echo ".modal();

                Admin.setuplistmodal(fielddialog";
        // line 144
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 144, $this->getSourceContext()); })());
        echo ");
            }
        });
    };

    // handle the add link
    var fielddialogformadd";
        // line 150
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 150, $this->getSourceContext()); })());
        echo " = function(event) {
        initializepopup";
        // line 151
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 151, $this->getSourceContext()); })());
        echo "();

        event.preventDefault();
        event.stopPropagation();

        var a = jQuery(this);

        fielddialogcontent";
        // line 158
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 158, $this->getSourceContext()); })());
        echo ".html('');

        Admin.log('[";
        // line 160
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 160, $this->getSourceContext()); })());
        echo "|fielddialogformadd] add link action');

        // retrieve the form element from the related admin generator
        jQuery.ajax({
            url: a.attr('href'),
            dataType: 'html',
            success: function(html) {

                Admin.log('[";
        // line 168
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 168, $this->getSourceContext()); })());
        echo "|fielddialogformadd] ajax success', fielddialog";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 168, $this->getSourceContext()); })());
        echo ");

                // populate the popup container
                fielddialogcontent";
        // line 171
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 171, $this->getSourceContext()); })());
        echo ".html(html);
                fielddialogtitle";
        // line 172
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 172, $this->getSourceContext()); })());
        echo ".html(\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 172, $this->getSourceContext()); })()), "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 172, $this->getSourceContext()); })()), "translationdomain", array()));
        echo "\");

                Admin.sharedsetup(fielddialog";
        // line 174
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 174, $this->getSourceContext()); })());
        echo ");

                // capture the submit event to make an ajax call, ie : POST data to the
                // related create admin
                jQuery('a', fielddialog";
        // line 178
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 178, $this->getSourceContext()); })());
        echo ").on('click', fielddialogformaction";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 178, $this->getSourceContext()); })());
        echo ");
                jQuery('form', fielddialog";
        // line 179
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 179, $this->getSourceContext()); })());
        echo ").on('submit', fielddialogformaction";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 179, $this->getSourceContext()); })());
        echo ");

                // open the dialog in modal mode
                fielddialog";
        // line 182
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 182, $this->getSourceContext()); })());
        echo ".modal();

                Admin.setuplistmodal(fielddialog";
        // line 184
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 184, $this->getSourceContext()); })());
        echo ");
            }
        });
    };

    // handle the post data
    var fielddialogformaction";
        // line 190
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 190, $this->getSourceContext()); })());
        echo " = function(event) {

        var element = jQuery(this);

        // return if the link is an anchor inside the same page
        if (
            this.nodeName === 'A'
            && (
                element.attr('href').length === 0
                || element.attr('href')[0] === '#'
                || element.attr('href').substr(0, 11) === 'javascript:'
            )
        ) {
            Admin.log('[";
        // line 203
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 203, $this->getSourceContext()); })());
        echo "|fielddialogformaction] element is an anchor or a script, skipping action', this);
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[";
        // line 210
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 210, $this->getSourceContext()); })());
        echo "|fielddialogformaction] action catch', this);

        initializepopup";
        // line 212
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 212, $this->getSourceContext()); })());
        echo "();

        if (this.nodeName == 'FORM') {
            var url  = element.attr('action');
            var type = element.attr('method');
        } else if (this.nodeName == 'A') {
            var url  = element.attr('href');
            var type = 'GET';
        } else {
            alert('unexpected element : @' + this.nodeName + '@');
            return;
        }

        if (element.hasClass('sonata-ba-action')) {
            Admin.log('[";
        // line 226
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 226, $this->getSourceContext()); })());
        echo "|fielddialogformaction] reserved action stop catch all events');
            return;
        }

        var data = {
            xmlhttprequest: true
        }

        var form = jQuery(this);

        Admin.log('[";
        // line 236
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 236, $this->getSourceContext()); })());
        echo "|fielddialogformaction] execute ajax call');

        // the ajax post
        jQuery(form).ajaxSubmit({
            url: url,
            type: type,
            data: data,
            success: function(data) {
                Admin.log('[";
        // line 244
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 244, $this->getSourceContext()); })());
        echo "|fielddialogformaction] ajax success');

                // if the crud action return ok, then the element has been added
                // so the widget container must be refresh with the last option available
                if (typeof data != 'string' && data.result == 'ok') {
                    fielddialog";
        // line 249
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 249, $this->getSourceContext()); })());
        echo ".modal('hide');

                    ";
        // line 251
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 251, $this->getSourceContext()); })()), "edit", array()) == "list")) {
            // line 252
            echo "                        ";
            // line 256
            echo "                        jQuery('#";
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 256, $this->getSourceContext()); })());
            echo "').val(data.objectId);
                        jQuery('#";
            // line 257
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 257, $this->getSourceContext()); })());
            echo "').change();

                    ";
        } else {
            // line 260
            echo "
                        // reload the form element
                        jQuery('#fieldwidget";
            // line 262
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 262, $this->getSourceContext()); })());
            echo "').closest('form').ajaxSubmit({
                            url: '";
            // line 263
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminretrieveformelement", array("elementId" =>             // line 264
(isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 264, $this->getSourceContext()); })()), "subclass" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 265
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 265, $this->getSourceContext()); })()), "admin", array()), "getActiveSubclassCode", array(), "method"), "objectId" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 266
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 266, $this->getSourceContext()); })()), "admin", array()), "root", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 266, $this->getSourceContext()); })()), "admin", array()), "root", array()), "subject", array())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 267
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 267, $this->getSourceContext()); })()), "admin", array()), "root", array()), "uniqid", array()), "code" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 268
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 268, $this->getSourceContext()); })()), "admin", array()), "root", array()), "code", array())));
            // line 269
            echo "',
                            data: {xmlhttprequest: true },
                            dataType: 'html',
                            type: 'POST',
                            success: function(html) {
                                jQuery('#fieldcontainer";
            // line 274
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 274, $this->getSourceContext()); })());
            echo "').replaceWith(html);
                                var newElement = jQuery('#";
            // line 275
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 275, $this->getSourceContext()); })());
            echo " [value=\"' + data.objectId + '\"]');
                                if (newElement.is(\"input\")) {
                                    newElement.attr('checked', 'checked');
                                } else {
                                    newElement.attr('selected', 'selected');
                                }

                                jQuery('#fieldcontainer";
            // line 282
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 282, $this->getSourceContext()); })());
            echo "').trigger('sonata-admin-append-form-element');
                            }
                        });

                    ";
        }
        // line 287
        echo "
                    return;
                }

                // otherwise, display form error
                fielddialogcontent";
        // line 292
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 292, $this->getSourceContext()); })());
        echo ".html(data);

                Admin.sharedsetup(fielddialog";
        // line 294
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 294, $this->getSourceContext()); })());
        echo ");

                // reattach the event
                jQuery('form', fielddialog";
        // line 297
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 297, $this->getSourceContext()); })());
        echo ").submit(fielddialogformaction";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 297, $this->getSourceContext()); })());
        echo ");
            }
        });

        return false;
    };

    var fielddialog";
        // line 304
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 304, $this->getSourceContext()); })());
        echo "         = false;
    var fielddialogcontent";
        // line 305
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 305, $this->getSourceContext()); })());
        echo " = false;
    var fielddialogtitle";
        // line 306
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 306, $this->getSourceContext()); })());
        echo "   = false;

    function initializepopup";
        // line 308
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 308, $this->getSourceContext()); })());
        echo "() {
        // initialize component
        if (!fielddialog";
        // line 310
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 310, $this->getSourceContext()); })());
        echo ") {
            fielddialog";
        // line 311
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 311, $this->getSourceContext()); })());
        echo "         = jQuery(\"#fielddialog";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 311, $this->getSourceContext()); })());
        echo "\");
            fielddialogcontent";
        // line 312
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 312, $this->getSourceContext()); })());
        echo " = jQuery(\".modal-body\", \"#fielddialog";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 312, $this->getSourceContext()); })());
        echo "\");
            fielddialogtitle";
        // line 313
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 313, $this->getSourceContext()); })());
        echo "   = jQuery(\".modal-title\", \"#fielddialog";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 313, $this->getSourceContext()); })());
        echo "\");

            // move the dialog as a child of the root element, nested form breaks html ...
            jQuery(document.body).append(fielddialog";
        // line 316
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 316, $this->getSourceContext()); })());
        echo ");

            Admin.log('[";
        // line 318
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 318, $this->getSourceContext()); })());
        echo "|fielddialog] move dialog container as a document child');
        }
    }

    ";
        // line 325
        echo "    // this function initializes the popup
    // this can be only done this way as popup can be cascaded
    function startfielddialogformadd";
        // line 327
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 327, $this->getSourceContext()); })());
        echo "(link) {

        // remove the html event
        link.onclick = null;

        initializepopup";
        // line 332
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 332, $this->getSourceContext()); })());
        echo "();

        // add the jQuery event to the a element
        jQuery(link)
            .click(fielddialogformadd";
        // line 336
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 336, $this->getSourceContext()); })());
        echo ")
            .trigger('click')
        ;

        return false;
    }

    if (fielddialog";
        // line 343
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 343, $this->getSourceContext()); })());
        echo ") {
        Admin.sharedsetup(fielddialog";
        // line 344
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 344, $this->getSourceContext()); })());
        echo ");
    }

    ";
        // line 347
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 347, $this->getSourceContext()); })()), "edit", array()) == "list")) {
            // line 348
            echo "        ";
            // line 351
            echo "        // this function initializes the popup
        // this can be only done this way as popup can be cascaded
        function startfielddialogformlist";
            // line 353
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 353, $this->getSourceContext()); })());
            echo "(link) {

            link.onclick = null;

            initializepopup";
            // line 357
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 357, $this->getSourceContext()); })());
            echo "();

            // add the jQuery event to the a element
            jQuery(link)
                .click(fielddialogformlist";
            // line 361
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 361, $this->getSourceContext()); })());
            echo ")
                .trigger('click')
            ;

            return false;
        }

        function removeselectedelement";
            // line 368
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 368, $this->getSourceContext()); })());
            echo "(link) {

            link.onclick = null;

            jQuery(link)
                .click(fieldremoveelement";
            // line 373
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 373, $this->getSourceContext()); })());
            echo ")
                .trigger('click')
            ;

            return false;
        }

        function fieldremoveelement";
            // line 380
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 380, $this->getSourceContext()); })());
            echo "(event) {
            event.preventDefault();

            if (jQuery('#";
            // line 383
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 383, $this->getSourceContext()); })());
            echo " option').get(0)) {
                jQuery('#";
            // line 384
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 384, $this->getSourceContext()); })());
            echo "').attr('selectedIndex', '-1').children(\"option:selected\").attr(\"selected\", false);
            }

            jQuery('#";
            // line 387
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 387, $this->getSourceContext()); })());
            echo "').val('');
            jQuery('#";
            // line 388
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 388, $this->getSourceContext()); })());
            echo "').trigger('change');

            return false;
        }
        ";
            // line 395
            echo "
        // update the label
        jQuery('#";
            // line 397
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 397, $this->getSourceContext()); })());
            echo "').on('change', function(event) {

            Admin.log('[";
            // line 399
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 399, $this->getSourceContext()); })());
            echo "|on:change] update the label');

            jQuery('#fieldwidget";
            // line 401
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 401, $this->getSourceContext()); })());
            echo "').html(\"<span><img src=\\\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/sonataadmin/ajax-loader.gif");
            echo "\\\" style=\\\"vertical-align: middle; margin-right: 10px\\\"/>";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("loadinginformation", array(), "SonataAdminBundle");
            echo "</span>\");
            jQuery.ajax({
                type: 'GET',
                url: '";
            // line 404
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminshortobjectinformation", array("objectId" => "OBJECTID", "uniqid" => twiggetattribute($this->env, $this->getSourceContext(),             // line 406
(isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 406, $this->getSourceContext()); })()), "uniqid", array()), "code" => twiggetattribute($this->env, $this->getSourceContext(),             // line 407
(isset($context["associationadmin"]) || arraykeyexists("associationadmin", $context) ? $context["associationadmin"] : (function () { throw new TwigErrorRuntime('Variable "associationadmin" does not exist.', 407, $this->getSourceContext()); })()), "code", array()), "linkParameters" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 408
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 408, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "linkparameters", array())));
            // line 409
            echo "'.replace('OBJECTID', jQuery(this).val()),
                dataType: 'html',
                success: function(html) {
                    jQuery('#fieldwidget";
            // line 412
            echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 412, $this->getSourceContext()); })());
            echo "').html(html);
                }
            });
        });

    ";
        }
        // line 418
        echo "

</script>
<!-- / edit many association -->

";
        
        $internalfe2b969a31f2006ac5d6d915782a742614d365dcad86aa3174cc7ebfadcd6d4a->leave($internalfe2b969a31f2006ac5d6d915782a742614d365dcad86aa3174cc7ebfadcd6d4aprof);

        
        $internal57015e3a7cc1642fe84cd358fe7d2d20e672b1619c026f6fe90d8606ecf44aec->leave($internal57015e3a7cc1642fe84cd358fe7d2d20e672b1619c026f6fe90d8606ecf44aecprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editmanyscript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  738 => 418,  729 => 412,  724 => 409,  722 => 408,  721 => 407,  720 => 406,  719 => 404,  709 => 401,  704 => 399,  699 => 397,  695 => 395,  688 => 388,  684 => 387,  678 => 384,  674 => 383,  668 => 380,  658 => 373,  650 => 368,  640 => 361,  633 => 357,  626 => 353,  622 => 351,  620 => 348,  618 => 347,  612 => 344,  608 => 343,  598 => 336,  591 => 332,  583 => 327,  579 => 325,  572 => 318,  567 => 316,  559 => 313,  553 => 312,  547 => 311,  543 => 310,  538 => 308,  533 => 306,  529 => 305,  525 => 304,  513 => 297,  507 => 294,  502 => 292,  495 => 287,  487 => 282,  477 => 275,  473 => 274,  466 => 269,  464 => 268,  463 => 267,  462 => 266,  461 => 265,  460 => 264,  459 => 263,  455 => 262,  451 => 260,  445 => 257,  440 => 256,  438 => 252,  436 => 251,  431 => 249,  423 => 244,  412 => 236,  399 => 226,  382 => 212,  377 => 210,  367 => 203,  351 => 190,  342 => 184,  337 => 182,  329 => 179,  323 => 178,  316 => 174,  309 => 172,  305 => 171,  297 => 168,  286 => 160,  281 => 158,  271 => 151,  267 => 150,  258 => 144,  253 => 142,  247 => 139,  242 => 137,  235 => 135,  230 => 133,  224 => 130,  213 => 122,  206 => 118,  198 => 113,  193 => 111,  183 => 104,  178 => 102,  174 => 101,  169 => 99,  157 => 90,  149 => 85,  143 => 84,  136 => 80,  132 => 79,  125 => 75,  120 => 73,  116 => 72,  111 => 70,  101 => 63,  96 => 61,  92 => 60,  87 => 58,  77 => 51,  70 => 47,  65 => 45,  51 => 34,  47 => 33,  44 => 32,  37 => 22,  35 => 21,  32 => 20,  29 => 18,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}


{#

This code manages the many-to-[one|many] association field popup

#}

{% autoescape false %}

{% set associationadmin = sonataadmin.fielddescription.associationadmin %}

<!-- edit many association -->

<script type=\"text/javascript\">

    {#
      handle link click in a list :
        - if the parent has an objectId defined then the related input get updated
        - if the parent has NO object then an ajax request is made to refresh the popup
    #}

    var fielddialogformlistlink{{ id }} = function(event) {
        initializepopup{{ id }}();

        var target = jQuery(this);

        if (target.attr('href').length === 0 || target.attr('href') === '#') {
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[{{ id }}|fielddialogformlistlink] handle link click in a list');

        var element = jQuery(this).parents('#fielddialog{{ id }} .sonata-ba-list-field');

        // the user does not click on a row column
        if (element.length == 0) {
            Admin.log('[{{ id }}|fielddialogformlistlink] the user does not click on a row column, make ajax call to retrieve inner html');
            // make a recursive call (ie: reset the filter)
            jQuery.ajax({
                type: 'GET',
                url: jQuery(this).attr('href'),
                dataType: 'html',
                success: function(html) {
                    Admin.log('[{{ id }}|fielddialogformlistlink] callback success, attach valid js event');

                    fielddialogcontent{{ id }}.html(html);
                    fielddialogformlisthandleaction{{ id }}();

                    Admin.sharedsetup(fielddialog{{ id }});
                }
            });

            return;
        }

        Admin.log('[{{ id }}|fielddialogformlistlink] the user select one element, update input and hide the modal');

        jQuery('#{{ id }}').val(element.attr('objectId'));
        jQuery('#{{ id }}').trigger('change');

        fielddialog{{ id }}.modal('hide');
    };

    // this function handle action on the modal list when inside a selected list
    var fielddialogformlisthandleaction{{ id }}  =  function() {
        Admin.log('[{{ id }}|fielddialogformlisthandleaction] attaching valid js event');

        // capture the submit event to make an ajax call, ie : POST data to the
        // related create admin
        jQuery('a', fielddialog{{ id }}).on('click', fielddialogformlistlink{{ id }});
        jQuery('form', fielddialog{{ id }}).on('submit', function(event) {
            event.preventDefault();

            var form = jQuery(this);

            Admin.log('[{{ id }}|fielddialogformlisthandleaction] catching submit event, sending ajax request');

            jQuery(form).ajaxSubmit({
                type: form.attr('method'),
                url: form.attr('action'),
                dataType: 'html',
                data: {xmlhttprequest: true},
                success: function(html) {

                    Admin.log('[{{ id }}|fielddialogformlisthandleaction] form submit success, restoring event');

                    fielddialogcontent{{ id }}.html(html);
                    fielddialogformlisthandleaction{{ id }}();

                    Admin.sharedsetup(fielddialog{{ id }});
                }
            });
        });
    };

    // handle the list link
    var fielddialogformlist{{ id }} = function(event) {

        initializepopup{{ id }}();

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[{{ id }}|fielddialogformlist] open the list modal');

        var a = jQuery(this);

        fielddialogcontent{{ id }}.html('');

        // retrieve the form element from the related admin generator
        jQuery.ajax({
            url: a.attr('href'),
            dataType: 'html',
            success: function(html) {

                Admin.log('[{{ id }}|fielddialogformlist] retrieving the list content');

                // populate the popup container
                fielddialogcontent{{ id }}.html(html);

                fielddialogtitle{{ id }}.html(\"{{ associationadmin.label|trans({}, associationadmin.translationdomain) }}\");

                Admin.sharedsetup(fielddialog{{ id }});

                fielddialogformlisthandleaction{{ id }}();

                // open the dialog in modal mode
                fielddialog{{ id }}.modal();

                Admin.setuplistmodal(fielddialog{{ id }});
            }
        });
    };

    // handle the add link
    var fielddialogformadd{{ id }} = function(event) {
        initializepopup{{ id }}();

        event.preventDefault();
        event.stopPropagation();

        var a = jQuery(this);

        fielddialogcontent{{ id }}.html('');

        Admin.log('[{{ id }}|fielddialogformadd] add link action');

        // retrieve the form element from the related admin generator
        jQuery.ajax({
            url: a.attr('href'),
            dataType: 'html',
            success: function(html) {

                Admin.log('[{{ id }}|fielddialogformadd] ajax success', fielddialog{{ id }});

                // populate the popup container
                fielddialogcontent{{ id }}.html(html);
                fielddialogtitle{{ id }}.html(\"{{ associationadmin.label|trans({}, associationadmin.translationdomain) }}\");

                Admin.sharedsetup(fielddialog{{ id }});

                // capture the submit event to make an ajax call, ie : POST data to the
                // related create admin
                jQuery('a', fielddialog{{ id }}).on('click', fielddialogformaction{{ id }});
                jQuery('form', fielddialog{{ id }}).on('submit', fielddialogformaction{{ id }});

                // open the dialog in modal mode
                fielddialog{{ id }}.modal();

                Admin.setuplistmodal(fielddialog{{ id }});
            }
        });
    };

    // handle the post data
    var fielddialogformaction{{ id }} = function(event) {

        var element = jQuery(this);

        // return if the link is an anchor inside the same page
        if (
            this.nodeName === 'A'
            && (
                element.attr('href').length === 0
                || element.attr('href')[0] === '#'
                || element.attr('href').substr(0, 11) === 'javascript:'
            )
        ) {
            Admin.log('[{{ id }}|fielddialogformaction] element is an anchor or a script, skipping action', this);
            return;
        }

        event.preventDefault();
        event.stopPropagation();

        Admin.log('[{{ id }}|fielddialogformaction] action catch', this);

        initializepopup{{ id }}();

        if (this.nodeName == 'FORM') {
            var url  = element.attr('action');
            var type = element.attr('method');
        } else if (this.nodeName == 'A') {
            var url  = element.attr('href');
            var type = 'GET';
        } else {
            alert('unexpected element : @' + this.nodeName + '@');
            return;
        }

        if (element.hasClass('sonata-ba-action')) {
            Admin.log('[{{ id }}|fielddialogformaction] reserved action stop catch all events');
            return;
        }

        var data = {
            xmlhttprequest: true
        }

        var form = jQuery(this);

        Admin.log('[{{ id }}|fielddialogformaction] execute ajax call');

        // the ajax post
        jQuery(form).ajaxSubmit({
            url: url,
            type: type,
            data: data,
            success: function(data) {
                Admin.log('[{{ id }}|fielddialogformaction] ajax success');

                // if the crud action return ok, then the element has been added
                // so the widget container must be refresh with the last option available
                if (typeof data != 'string' && data.result == 'ok') {
                    fielddialog{{ id }}.modal('hide');

                    {% if sonataadmin.edit == 'list' %}
                        {#
                           in this case we update the hidden input, and call the change event to
                           retrieve the post information
                        #}
                        jQuery('#{{ id }}').val(data.objectId);
                        jQuery('#{{ id }}').change();

                    {% else %}

                        // reload the form element
                        jQuery('#fieldwidget{{ id }}').closest('form').ajaxSubmit({
                            url: '{{ path('sonataadminretrieveformelement', {
                                'elementId': id,
                                'subclass':  sonataadmin.admin.getActiveSubclassCode(),
                                'objectId':  sonataadmin.admin.root.id(sonataadmin.admin.root.subject),
                                'uniqid':    sonataadmin.admin.root.uniqid,
                                'code':      sonataadmin.admin.root.code
                            }) }}',
                            data: {xmlhttprequest: true },
                            dataType: 'html',
                            type: 'POST',
                            success: function(html) {
                                jQuery('#fieldcontainer{{ id }}').replaceWith(html);
                                var newElement = jQuery('#{{ id }} [value=\"' + data.objectId + '\"]');
                                if (newElement.is(\"input\")) {
                                    newElement.attr('checked', 'checked');
                                } else {
                                    newElement.attr('selected', 'selected');
                                }

                                jQuery('#fieldcontainer{{ id }}').trigger('sonata-admin-append-form-element');
                            }
                        });

                    {% endif %}

                    return;
                }

                // otherwise, display form error
                fielddialogcontent{{ id }}.html(data);

                Admin.sharedsetup(fielddialog{{ id }});

                // reattach the event
                jQuery('form', fielddialog{{ id }}).submit(fielddialogformaction{{ id }});
            }
        });

        return false;
    };

    var fielddialog{{ id }}         = false;
    var fielddialogcontent{{ id }} = false;
    var fielddialogtitle{{ id }}   = false;

    function initializepopup{{ id }}() {
        // initialize component
        if (!fielddialog{{ id }}) {
            fielddialog{{ id }}         = jQuery(\"#fielddialog{{ id }}\");
            fielddialogcontent{{ id }} = jQuery(\".modal-body\", \"#fielddialog{{ id }}\");
            fielddialogtitle{{ id }}   = jQuery(\".modal-title\", \"#fielddialog{{ id }}\");

            // move the dialog as a child of the root element, nested form breaks html ...
            jQuery(document.body).append(fielddialog{{ id }});

            Admin.log('[{{ id }}|fielddialog] move dialog container as a document child');
        }
    }

    {#
        This code is used to defined the \"add\" popup
    #}
    // this function initializes the popup
    // this can be only done this way as popup can be cascaded
    function startfielddialogformadd{{ id }}(link) {

        // remove the html event
        link.onclick = null;

        initializepopup{{ id }}();

        // add the jQuery event to the a element
        jQuery(link)
            .click(fielddialogformadd{{ id }})
            .trigger('click')
        ;

        return false;
    }

    if (fielddialog{{ id }}) {
        Admin.sharedsetup(fielddialog{{ id }});
    }

    {% if sonataadmin.edit == 'list' %}
        {#
            This code is used to defined the \"list\" popup
        #}
        // this function initializes the popup
        // this can be only done this way as popup can be cascaded
        function startfielddialogformlist{{ id }}(link) {

            link.onclick = null;

            initializepopup{{ id }}();

            // add the jQuery event to the a element
            jQuery(link)
                .click(fielddialogformlist{{ id }})
                .trigger('click')
            ;

            return false;
        }

        function removeselectedelement{{ id }}(link) {

            link.onclick = null;

            jQuery(link)
                .click(fieldremoveelement{{ id}})
                .trigger('click')
            ;

            return false;
        }

        function fieldremoveelement{{ id }}(event) {
            event.preventDefault();

            if (jQuery('#{{ id }} option').get(0)) {
                jQuery('#{{ id }}').attr('selectedIndex', '-1').children(\"option:selected\").attr(\"selected\", false);
            }

            jQuery('#{{ id }}').val('');
            jQuery('#{{ id }}').trigger('change');

            return false;
        }
        {#
          attach onchange event on the input
        #}

        // update the label
        jQuery('#{{ id }}').on('change', function(event) {

            Admin.log('[{{ id }}|on:change] update the label');

            jQuery('#fieldwidget{{ id }}').html(\"<span><img src=\\\"{{ asset('bundles/sonataadmin/ajax-loader.gif') }}\\\" style=\\\"vertical-align: middle; margin-right: 10px\\\"/>{{ 'loadinginformation'|trans([], 'SonataAdminBundle') }}</span>\");
            jQuery.ajax({
                type: 'GET',
                url: '{{ path('sonataadminshortobjectinformation', {
                    'objectId': 'OBJECTID',
                    'uniqid': associationadmin.uniqid,
                    'code': associationadmin.code,
                    'linkParameters': sonataadmin.fielddescription.options.linkparameters
                })}}'.replace('OBJECTID', jQuery(this).val()),
                dataType: 'html',
                success: function(html) {
                    jQuery('#fieldwidget{{ id }}').html(html);
                }
            });
        });

    {% endif %}


</script>
<!-- / edit many association -->

{% endautoescape %}
", "SonataAdminBundle:CRUD/Association:editmanyscript.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editmanyscript.html.twig");
    }
}
