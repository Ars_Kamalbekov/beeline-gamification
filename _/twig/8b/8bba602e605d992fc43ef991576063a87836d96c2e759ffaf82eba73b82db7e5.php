<?php

/* @WebProfiler/Icon/no.svg */
class TwigTemplate7e958c789c08497fac7d3696efe488af89f104af77ce2894c54189424889a0dc extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9b5d66743c807ac514c3fb10a1ce55acd7edd3865d0fb79b33d1fe88375994e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9b5d66743c807ac514c3fb10a1ce55acd7edd3865d0fb79b33d1fe88375994e0->enter($internal9b5d66743c807ac514c3fb10a1ce55acd7edd3865d0fb79b33d1fe88375994e0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        $internalf0b553a75d27a2c03933085ee4b00b31f9c3e151d4e9fd0c3418ad36396c5a19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf0b553a75d27a2c03933085ee4b00b31f9c3e151d4e9fd0c3418ad36396c5a19->enter($internalf0b553a75d27a2c03933085ee4b00b31f9c3e151d4e9fd0c3418ad36396c5a19prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
";
        
        $internal9b5d66743c807ac514c3fb10a1ce55acd7edd3865d0fb79b33d1fe88375994e0->leave($internal9b5d66743c807ac514c3fb10a1ce55acd7edd3865d0fb79b33d1fe88375994e0prof);

        
        $internalf0b553a75d27a2c03933085ee4b00b31f9c3e151d4e9fd0c3418ad36396c5a19->leave($internalf0b553a75d27a2c03933085ee4b00b31f9c3e151d4e9fd0c3418ad36396c5a19prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/no.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
", "@WebProfiler/Icon/no.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/no.svg");
    }
}
