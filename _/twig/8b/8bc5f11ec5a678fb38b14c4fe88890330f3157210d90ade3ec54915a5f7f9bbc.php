<?php

/* FOSUserBundle:Profile:showcontent.html.twig */
class TwigTemplate73a2fb01174ddb04022388b43cab5ffc17bc6d989cded01ee45a670453c6ef2b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7a2435a802b6a6bdf462c31ebdc08b2afb6850d151a0ab94d1b8716432acd4ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7a2435a802b6a6bdf462c31ebdc08b2afb6850d151a0ab94d1b8716432acd4ec->enter($internal7a2435a802b6a6bdf462c31ebdc08b2afb6850d151a0ab94d1b8716432acd4ecprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:showcontent.html.twig"));

        $internal8437469b0281d89ccbcfddd84dd6764a4be09b455d5d9a383fbc9e92df3e80f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8437469b0281d89ccbcfddd84dd6764a4be09b455d5d9a383fbc9e92df3e80f5->enter($internal8437469b0281d89ccbcfddd84dd6764a4be09b455d5d9a383fbc9e92df3e80f5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:showcontent.html.twig"));

        // line 2
        echo "
<div class=\"fosuserusershow\">
    <p>";
        // line 4
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "html", null, true);
        echo "</p>
    <p>";
        // line 5
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 5, $this->getSourceContext()); })()), "email", array()), "html", null, true);
        echo "</p>
</div>
";
        
        $internal7a2435a802b6a6bdf462c31ebdc08b2afb6850d151a0ab94d1b8716432acd4ec->leave($internal7a2435a802b6a6bdf462c31ebdc08b2afb6850d151a0ab94d1b8716432acd4ecprof);

        
        $internal8437469b0281d89ccbcfddd84dd6764a4be09b455d5d9a383fbc9e92df3e80f5->leave($internal8437469b0281d89ccbcfddd84dd6764a4be09b455d5d9a383fbc9e92df3e80f5prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:showcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  29 => 4,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

<div class=\"fosuserusershow\">
    <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
</div>
", "FOSUserBundle:Profile:showcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/showcontent.html.twig");
    }
}
