<?php

/* @Framework/Form/radiowidget.html.php */
class TwigTemplated2262a9a21750216ce5d12bb953547113ee71219ed1ddb580c9ac24955536e00 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal75d7aee635b600d12eec688d25f6e54c40f10665605115b813b08212126b4a21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal75d7aee635b600d12eec688d25f6e54c40f10665605115b813b08212126b4a21->enter($internal75d7aee635b600d12eec688d25f6e54c40f10665605115b813b08212126b4a21prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/radiowidget.html.php"));

        $internal2c431d7f0270d159afcd7cdac7b2244032c8da24b0879a4b63493ad66a13a7d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2c431d7f0270d159afcd7cdac7b2244032c8da24b0879a4b63493ad66a13a7d6->enter($internal2c431d7f0270d159afcd7cdac7b2244032c8da24b0879a4b63493ad66a13a7d6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/radiowidget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $internal75d7aee635b600d12eec688d25f6e54c40f10665605115b813b08212126b4a21->leave($internal75d7aee635b600d12eec688d25f6e54c40f10665605115b813b08212126b4a21prof);

        
        $internal2c431d7f0270d159afcd7cdac7b2244032c8da24b0879a4b63493ad66a13a7d6->leave($internal2c431d7f0270d159afcd7cdac7b2244032c8da24b0879a4b63493ad66a13a7d6prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radiowidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radiowidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radiowidget.html.php");
    }
}
