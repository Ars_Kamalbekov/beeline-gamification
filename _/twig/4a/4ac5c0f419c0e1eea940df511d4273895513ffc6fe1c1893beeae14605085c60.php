<?php

/* SonataAdminBundle:Button:showbutton.html.twig */
class TwigTemplate51e9073e7d9bdc9e810f8e172b78285c00d4fb71fa3050e0432304054a3fce53 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbeea5841dd52c0ba29e0cb8637a26bbaabaa76771981aeb05b96e4aa5491ddfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbeea5841dd52c0ba29e0cb8637a26bbaabaa76771981aeb05b96e4aa5491ddfd->enter($internalbeea5841dd52c0ba29e0cb8637a26bbaabaa76771981aeb05b96e4aa5491ddfdprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:showbutton.html.twig"));

        $internalac6b019144ab59d57ab0d784cc8a1ba1eebe4af243939e684ffd46f9c75f4067 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalac6b019144ab59d57ab0d784cc8a1ba1eebe4af243939e684ffd46f9c75f4067->enter($internalac6b019144ab59d57ab0d784cc8a1ba1eebe4af243939e684ffd46f9c75f4067prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:showbutton.html.twig"));

        // line 11
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "canAccessObject", array(0 => "show", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 11, $this->getSourceContext()); })())), "method") && (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "show", array())) > 0)) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "hasRoute", array(0 => "show"), "method"))) {
            // line 12
            echo "    <li>
        <a class=\"sonata-action-element\" href=\"";
            // line 13
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "show", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 13, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
            <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
            ";
            // line 15
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionshow", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </a>
    </li>
";
        }
        
        $internalbeea5841dd52c0ba29e0cb8637a26bbaabaa76771981aeb05b96e4aa5491ddfd->leave($internalbeea5841dd52c0ba29e0cb8637a26bbaabaa76771981aeb05b96e4aa5491ddfdprof);

        
        $internalac6b019144ab59d57ab0d784cc8a1ba1eebe4af243939e684ffd46f9c75f4067->leave($internalac6b019144ab59d57ab0d784cc8a1ba1eebe4af243939e684ffd46f9c75f4067prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Button:showbutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 15,  30 => 13,  27 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% if admin.canAccessObject('show', object) and admin.show|length > 0 and admin.hasRoute('show') %}
    <li>
        <a class=\"sonata-action-element\" href=\"{{ admin.generateObjectUrl('show', object) }}\">
            <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
            {{ 'linkactionshow'|trans({}, 'SonataAdminBundle') }}
        </a>
    </li>
{% endif %}
", "SonataAdminBundle:Button:showbutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Button/showbutton.html.twig");
    }
}
