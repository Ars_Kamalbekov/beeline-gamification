<?php

/* FOSUserBundle:Group:new.html.twig */
class TwigTemplatec5b1b523ca1956b18864ecd56403e158ea04ed4cfecdc42d4d2ecb1f60f667be extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal64bf705b58a86714eeb971ed3f8381a8be68901afa626227cbe557943d9266d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal64bf705b58a86714eeb971ed3f8381a8be68901afa626227cbe557943d9266d3->enter($internal64bf705b58a86714eeb971ed3f8381a8be68901afa626227cbe557943d9266d3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $internal279ba38ff02d79c19e8baf2f715d224f10e7ef8cba741e69de515b276dd65a53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal279ba38ff02d79c19e8baf2f715d224f10e7ef8cba741e69de515b276dd65a53->enter($internal279ba38ff02d79c19e8baf2f715d224f10e7ef8cba741e69de515b276dd65a53prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal64bf705b58a86714eeb971ed3f8381a8be68901afa626227cbe557943d9266d3->leave($internal64bf705b58a86714eeb971ed3f8381a8be68901afa626227cbe557943d9266d3prof);

        
        $internal279ba38ff02d79c19e8baf2f715d224f10e7ef8cba741e69de515b276dd65a53->leave($internal279ba38ff02d79c19e8baf2f715d224f10e7ef8cba741e69de515b276dd65a53prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internale5d904a6f9603258fe0957ba339b370b65700d7d9c60483af8d462af4bca5944 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale5d904a6f9603258fe0957ba339b370b65700d7d9c60483af8d462af4bca5944->enter($internale5d904a6f9603258fe0957ba339b370b65700d7d9c60483af8d462af4bca5944prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal91a1cda502dfc8fe0c18d32c347940253c1d3dd821f215d92b1905a4c317c334 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal91a1cda502dfc8fe0c18d32c347940253c1d3dd821f215d92b1905a4c317c334->enter($internal91a1cda502dfc8fe0c18d32c347940253c1d3dd821f215d92b1905a4c317c334prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/newcontent.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $internal91a1cda502dfc8fe0c18d32c347940253c1d3dd821f215d92b1905a4c317c334->leave($internal91a1cda502dfc8fe0c18d32c347940253c1d3dd821f215d92b1905a4c317c334prof);

        
        $internale5d904a6f9603258fe0957ba339b370b65700d7d9c60483af8d462af4bca5944->leave($internale5d904a6f9603258fe0957ba339b370b65700d7d9c60483af8d462af4bca5944prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Group/newcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Group:new.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}
