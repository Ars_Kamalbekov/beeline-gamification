<?php

/* @Framework/FormTable/buttonrow.html.php */
class TwigTemplate4de5778cac5bdcfa4cbdcd9f1aff2d59cd81ca0b37916b586a653d397669873b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaled6412dc618f154876a408fc086747e739095b70d1271221fbd86aecae66c36b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaled6412dc618f154876a408fc086747e739095b70d1271221fbd86aecae66c36b->enter($internaled6412dc618f154876a408fc086747e739095b70d1271221fbd86aecae66c36bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/buttonrow.html.php"));

        $internal9d4255112018a197402c6dfb945852af2ae884e1d632a8231c58df66e5a33916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9d4255112018a197402c6dfb945852af2ae884e1d632a8231c58df66e5a33916->enter($internal9d4255112018a197402c6dfb945852af2ae884e1d632a8231c58df66e5a33916prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/FormTable/buttonrow.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $internaled6412dc618f154876a408fc086747e739095b70d1271221fbd86aecae66c36b->leave($internaled6412dc618f154876a408fc086747e739095b70d1271221fbd86aecae66c36bprof);

        
        $internal9d4255112018a197402c6dfb945852af2ae884e1d632a8231c58df66e5a33916->leave($internal9d4255112018a197402c6dfb945852af2ae884e1d632a8231c58df66e5a33916prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/buttonrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/buttonrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/buttonrow.html.php");
    }
}
