<?php

/* SonataAdminBundle:CRUD:listarray.html.twig */
class TwigTemplatee9406a8db32b143bf6f1b8a14bb333ece08106de59e66b1f04adab839e33b8a6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listarray.html.twig", 13);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal32234e02e5511647c88683798ab3c197c5255ba4f8696f4fc65cd1e6c16055f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal32234e02e5511647c88683798ab3c197c5255ba4f8696f4fc65cd1e6c16055f5->enter($internal32234e02e5511647c88683798ab3c197c5255ba4f8696f4fc65cd1e6c16055f5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listarray.html.twig"));

        $internal0b0230c2ec71ed8f4d53b87002c466b9509dd4212da57b713989300ff34e3538 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0b0230c2ec71ed8f4d53b87002c466b9509dd4212da57b713989300ff34e3538->enter($internal0b0230c2ec71ed8f4d53b87002c466b9509dd4212da57b713989300ff34e3538prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listarray.html.twig"));

        // line 11
        $context["list"] = $this->loadTemplate("SonataAdminBundle:CRUD:basearraymacro.html.twig", "SonataAdminBundle:CRUD:listarray.html.twig", 11);
        // line 13
        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal32234e02e5511647c88683798ab3c197c5255ba4f8696f4fc65cd1e6c16055f5->leave($internal32234e02e5511647c88683798ab3c197c5255ba4f8696f4fc65cd1e6c16055f5prof);

        
        $internal0b0230c2ec71ed8f4d53b87002c466b9509dd4212da57b713989300ff34e3538->leave($internal0b0230c2ec71ed8f4d53b87002c466b9509dd4212da57b713989300ff34e3538prof);

    }

    // line 15
    public function blockfield($context, array $blocks = array())
    {
        $internal4fe1f90cb89709c89deb236a94794e6c3b1725a66edab410ea1e2e0b8291dc90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4fe1f90cb89709c89deb236a94794e6c3b1725a66edab410ea1e2e0b8291dc90->enter($internal4fe1f90cb89709c89deb236a94794e6c3b1725a66edab410ea1e2e0b8291dc90prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal24dc571607ae086d95369b8fe6da905c760fff017e53def6ad5dc8c6f3d56d2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal24dc571607ae086d95369b8fe6da905c760fff017e53def6ad5dc8c6f3d56d2f->enter($internal24dc571607ae086d95369b8fe6da905c760fff017e53def6ad5dc8c6f3d56d2fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["list"]->macrorenderarray((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "inline", array(), "any", true, true) || twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "inline", array())));
        echo "
";
        
        $internal24dc571607ae086d95369b8fe6da905c760fff017e53def6ad5dc8c6f3d56d2f->leave($internal24dc571607ae086d95369b8fe6da905c760fff017e53def6ad5dc8c6f3d56d2fprof);

        
        $internal4fe1f90cb89709c89deb236a94794e6c3b1725a66edab410ea1e2e0b8291dc90->leave($internal4fe1f90cb89709c89deb236a94794e6c3b1725a66edab410ea1e2e0b8291dc90prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listarray.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  42 => 15,  32 => 13,  30 => 11,  18 => 13,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:basearraymacro.html.twig' as list %}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {{ list.renderarray(value, fielddescription.options.inline is not defined or fielddescription.options.inline) }}
{% endblock %}
", "SonataAdminBundle:CRUD:listarray.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listarray.html.twig");
    }
}
