<?php

/* SonataCoreBundle:Form:datepicker.html.twig */
class TwigTemplatec7e2f241f1f87bc092380a457a3917c5d7c052835c4dbd9bf8e8313e4fa401c3 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonatatypedatepickerwidgethtml' => array($this, 'blocksonatatypedatepickerwidgethtml'),
            'sonatatypedatepickerwidget' => array($this, 'blocksonatatypedatepickerwidget'),
            'sonatatypedatetimepickerwidgethtml' => array($this, 'blocksonatatypedatetimepickerwidgethtml'),
            'sonatatypedatetimepickerwidget' => array($this, 'blocksonatatypedatetimepickerwidget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala88066f19a2adb6b26955488813d3a3f31fdb3cc61b96d4ba0706df4515f4756 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala88066f19a2adb6b26955488813d3a3f31fdb3cc61b96d4ba0706df4515f4756->enter($internala88066f19a2adb6b26955488813d3a3f31fdb3cc61b96d4ba0706df4515f4756prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:Form:datepicker.html.twig"));

        $internal0aed75cf089c702bbb7cd008504cfc76c6763fe16f0ed19a5752e84cbefea1bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0aed75cf089c702bbb7cd008504cfc76c6763fe16f0ed19a5752e84cbefea1bc->enter($internal0aed75cf089c702bbb7cd008504cfc76c6763fe16f0ed19a5752e84cbefea1bcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataCoreBundle:Form:datepicker.html.twig"));

        // line 11
        $this->displayBlock('sonatatypedatepickerwidgethtml', $context, $blocks);
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('sonatatypedatepickerwidget', $context, $blocks);
        // line 39
        echo "
";
        // line 40
        $this->displayBlock('sonatatypedatetimepickerwidgethtml', $context, $blocks);
        // line 51
        echo "
";
        // line 52
        $this->displayBlock('sonatatypedatetimepickerwidget', $context, $blocks);
        
        $internala88066f19a2adb6b26955488813d3a3f31fdb3cc61b96d4ba0706df4515f4756->leave($internala88066f19a2adb6b26955488813d3a3f31fdb3cc61b96d4ba0706df4515f4756prof);

        
        $internal0aed75cf089c702bbb7cd008504cfc76c6763fe16f0ed19a5752e84cbefea1bc->leave($internal0aed75cf089c702bbb7cd008504cfc76c6763fe16f0ed19a5752e84cbefea1bcprof);

    }

    // line 11
    public function blocksonatatypedatepickerwidgethtml($context, array $blocks = array())
    {
        $internal11574768c71cbd624fa06180891a5101cd8d1fb21db63e6f2420664deec6956e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal11574768c71cbd624fa06180891a5101cd8d1fb21db63e6f2420664deec6956e->enter($internal11574768c71cbd624fa06180891a5101cd8d1fb21db63e6f2420664deec6956eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatepickerwidgethtml"));

        $internalf8d4b2d16120f88f970f22e31fbc16464dd55f43ed8e96d7bc46f289325ba166 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf8d4b2d16120f88f970f22e31fbc16464dd55f43ed8e96d7bc46f289325ba166->enter($internalf8d4b2d16120f88f970f22e31fbc16464dd55f43ed8e96d7bc46f289325ba166prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatepickerwidgethtml"));

        // line 12
        echo "    ";
        if ((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 12, $this->getSourceContext()); })())) {
            // line 13
            echo "        <div class='input-group date' id='dp";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
            echo "'>
    ";
        }
        // line 15
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 15, $this->getSourceContext()); })()), array("data-date-format" => (isset($context["momentformat"]) || arraykeyexists("momentformat", $context) ? $context["momentformat"] : (function () { throw new TwigErrorRuntime('Variable "momentformat" does not exist.', 15, $this->getSourceContext()); })())));
        // line 16
        echo "    ";
        $this->displayBlock("datewidget", $context, $blocks);
        echo "
    ";
        // line 17
        if ((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 17, $this->getSourceContext()); })())) {
            // line 18
            echo "            <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    ";
        }
        
        $internalf8d4b2d16120f88f970f22e31fbc16464dd55f43ed8e96d7bc46f289325ba166->leave($internalf8d4b2d16120f88f970f22e31fbc16464dd55f43ed8e96d7bc46f289325ba166prof);

        
        $internal11574768c71cbd624fa06180891a5101cd8d1fb21db63e6f2420664deec6956e->leave($internal11574768c71cbd624fa06180891a5101cd8d1fb21db63e6f2420664deec6956eprof);

    }

    // line 23
    public function blocksonatatypedatepickerwidget($context, array $blocks = array())
    {
        $internal7ce49c1d2df892a60547c09fee79993a332c3d3babcb59d35a59375346a6ec42 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7ce49c1d2df892a60547c09fee79993a332c3d3babcb59d35a59375346a6ec42->enter($internal7ce49c1d2df892a60547c09fee79993a332c3d3babcb59d35a59375346a6ec42prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatepickerwidget"));

        $internale5f04a336ea88170a3da17665b4539b29f4541d8e294016a40456936c5f31c17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale5f04a336ea88170a3da17665b4539b29f4541d8e294016a40456936c5f31c17->enter($internale5f04a336ea88170a3da17665b4539b29f4541d8e294016a40456936c5f31c17prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatepickerwidget"));

        // line 24
        echo "    ";
        obstart();
        // line 25
        echo "        ";
        if ((isset($context["wrapfieldswithaddons"]) || arraykeyexists("wrapfieldswithaddons", $context) ? $context["wrapfieldswithaddons"] : (function () { throw new TwigErrorRuntime('Variable "wrapfieldswithaddons" does not exist.', 25, $this->getSourceContext()); })())) {
            // line 26
            echo "            <div class=\"input-group\">
                ";
            // line 27
            $this->displayBlock("sonatatypedatepickerwidgethtml", $context, $blocks);
            echo "
            </div>
        ";
        } else {
            // line 30
            echo "            ";
            $this->displayBlock("sonatatypedatepickerwidgethtml", $context, $blocks);
            echo "
        ";
        }
        // line 32
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#";
        // line 34
        echo (((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 34, $this->getSourceContext()); })())) ? ("dp") : (""));
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 34, $this->getSourceContext()); })()), "html", null, true);
        echo "').datetimepicker(";
        echo jsonencode((isset($context["dpoptions"]) || arraykeyexists("dpoptions", $context) ? $context["dpoptions"] : (function () { throw new TwigErrorRuntime('Variable "dpoptions" does not exist.', 34, $this->getSourceContext()); })()));
        echo ");
            });
        </script>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internale5f04a336ea88170a3da17665b4539b29f4541d8e294016a40456936c5f31c17->leave($internale5f04a336ea88170a3da17665b4539b29f4541d8e294016a40456936c5f31c17prof);

        
        $internal7ce49c1d2df892a60547c09fee79993a332c3d3babcb59d35a59375346a6ec42->leave($internal7ce49c1d2df892a60547c09fee79993a332c3d3babcb59d35a59375346a6ec42prof);

    }

    // line 40
    public function blocksonatatypedatetimepickerwidgethtml($context, array $blocks = array())
    {
        $internalf1e4d0e83dd8d18cc24cc29b31183009be1b6fde82f2236455e7a3bcda7c5c9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf1e4d0e83dd8d18cc24cc29b31183009be1b6fde82f2236455e7a3bcda7c5c9e->enter($internalf1e4d0e83dd8d18cc24cc29b31183009be1b6fde82f2236455e7a3bcda7c5c9eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatetimepickerwidgethtml"));

        $internalc69971ecc01fd65ec4df6f482ffd64a7c06107b3009282c25d50d5a916b683dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc69971ecc01fd65ec4df6f482ffd64a7c06107b3009282c25d50d5a916b683dc->enter($internalc69971ecc01fd65ec4df6f482ffd64a7c06107b3009282c25d50d5a916b683dcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatetimepickerwidgethtml"));

        // line 41
        echo "    ";
        if ((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 41, $this->getSourceContext()); })())) {
            // line 42
            echo "        <div class='input-group date' id='dtp";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 42, $this->getSourceContext()); })()), "html", null, true);
            echo "'>
    ";
        }
        // line 44
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 44, $this->getSourceContext()); })()), array("data-date-format" => (isset($context["momentformat"]) || arraykeyexists("momentformat", $context) ? $context["momentformat"] : (function () { throw new TwigErrorRuntime('Variable "momentformat" does not exist.', 44, $this->getSourceContext()); })())));
        // line 45
        echo "    ";
        $this->displayBlock("datetimewidget", $context, $blocks);
        echo "
    ";
        // line 46
        if ((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 46, $this->getSourceContext()); })())) {
            // line 47
            echo "          <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    ";
        }
        
        $internalc69971ecc01fd65ec4df6f482ffd64a7c06107b3009282c25d50d5a916b683dc->leave($internalc69971ecc01fd65ec4df6f482ffd64a7c06107b3009282c25d50d5a916b683dcprof);

        
        $internalf1e4d0e83dd8d18cc24cc29b31183009be1b6fde82f2236455e7a3bcda7c5c9e->leave($internalf1e4d0e83dd8d18cc24cc29b31183009be1b6fde82f2236455e7a3bcda7c5c9eprof);

    }

    // line 52
    public function blocksonatatypedatetimepickerwidget($context, array $blocks = array())
    {
        $internal3a5678cccf2b542ee36879010518d84437d2f38aa1771a887eee7d20e81acaa4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a5678cccf2b542ee36879010518d84437d2f38aa1771a887eee7d20e81acaa4->enter($internal3a5678cccf2b542ee36879010518d84437d2f38aa1771a887eee7d20e81acaa4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatetimepickerwidget"));

        $internal9a7a80dccba568e9862add9b9c8eb66a785e8b017c8ffc372e5835e2db0016bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9a7a80dccba568e9862add9b9c8eb66a785e8b017c8ffc372e5835e2db0016bf->enter($internal9a7a80dccba568e9862add9b9c8eb66a785e8b017c8ffc372e5835e2db0016bfprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypedatetimepickerwidget"));

        // line 53
        echo "    ";
        obstart();
        // line 54
        echo "        ";
        if ((isset($context["wrapfieldswithaddons"]) || arraykeyexists("wrapfieldswithaddons", $context) ? $context["wrapfieldswithaddons"] : (function () { throw new TwigErrorRuntime('Variable "wrapfieldswithaddons" does not exist.', 54, $this->getSourceContext()); })())) {
            // line 55
            echo "            <div class=\"input-group\">
                ";
            // line 56
            $this->displayBlock("sonatatypedatetimepickerwidgethtml", $context, $blocks);
            echo "
            </div>
        ";
        } else {
            // line 59
            echo "            ";
            $this->displayBlock("sonatatypedatetimepickerwidgethtml", $context, $blocks);
            echo "
        ";
        }
        // line 61
        echo "        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#";
        // line 63
        echo (((isset($context["datepickerusebutton"]) || arraykeyexists("datepickerusebutton", $context) ? $context["datepickerusebutton"] : (function () { throw new TwigErrorRuntime('Variable "datepickerusebutton" does not exist.', 63, $this->getSourceContext()); })())) ? ("dtp") : (""));
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 63, $this->getSourceContext()); })()), "html", null, true);
        echo "').datetimepicker(";
        echo jsonencode((isset($context["dpoptions"]) || arraykeyexists("dpoptions", $context) ? $context["dpoptions"] : (function () { throw new TwigErrorRuntime('Variable "dpoptions" does not exist.', 63, $this->getSourceContext()); })()));
        echo ");
            });
        </script>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal9a7a80dccba568e9862add9b9c8eb66a785e8b017c8ffc372e5835e2db0016bf->leave($internal9a7a80dccba568e9862add9b9c8eb66a785e8b017c8ffc372e5835e2db0016bfprof);

        
        $internal3a5678cccf2b542ee36879010518d84437d2f38aa1771a887eee7d20e81acaa4->leave($internal3a5678cccf2b542ee36879010518d84437d2f38aa1771a887eee7d20e81acaa4prof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:Form:datepicker.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  222 => 63,  218 => 61,  212 => 59,  206 => 56,  203 => 55,  200 => 54,  197 => 53,  188 => 52,  175 => 47,  173 => 46,  168 => 45,  165 => 44,  159 => 42,  156 => 41,  147 => 40,  129 => 34,  125 => 32,  119 => 30,  113 => 27,  110 => 26,  107 => 25,  104 => 24,  95 => 23,  82 => 18,  80 => 17,  75 => 16,  72 => 15,  66 => 13,  63 => 12,  54 => 11,  44 => 52,  41 => 51,  39 => 40,  36 => 39,  34 => 23,  31 => 22,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonatatypedatepickerwidgethtml %}
    {% if datepickerusebutton %}
        <div class='input-group date' id='dp{{ id }}'>
    {% endif %}
    {% set attr = attr|merge({'data-date-format': momentformat}) %}
    {{ block('datewidget') }}
    {% if datepickerusebutton %}
            <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    {% endif %}
{% endblock sonatatypedatepickerwidgethtml %}

{% block sonatatypedatepickerwidget %}
    {% spaceless %}
        {% if wrapfieldswithaddons %}
            <div class=\"input-group\">
                {{ block('sonatatypedatepickerwidgethtml') }}
            </div>
        {% else %}
            {{ block('sonatatypedatepickerwidgethtml') }}
        {% endif %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#{{ datepickerusebutton ? 'dp' : '' }}{{ id }}').datetimepicker({{ dpoptions|jsonencode|raw }});
            });
        </script>
    {% endspaceless %}
{% endblock sonatatypedatepickerwidget %}

{% block sonatatypedatetimepickerwidgethtml %}
    {% if datepickerusebutton %}
        <div class='input-group date' id='dtp{{ id }}'>
    {% endif %}
    {% set attr = attr|merge({'data-date-format': momentformat}) %}
    {{ block('datetimewidget') }}
    {% if datepickerusebutton %}
          <span class=\"input-group-addon\"><span class=\"fa fa-calendar\"></span></span>
        </div>
    {% endif %}
{% endblock sonatatypedatetimepickerwidgethtml %}

{% block sonatatypedatetimepickerwidget %}
    {% spaceless %}
        {% if wrapfieldswithaddons %}
            <div class=\"input-group\">
                {{ block('sonatatypedatetimepickerwidgethtml') }}
            </div>
        {% else %}
            {{ block('sonatatypedatetimepickerwidgethtml') }}
        {% endif %}
        <script type=\"text/javascript\">
            jQuery(function (\$) {
                \$('#{{ datepickerusebutton ? 'dtp' : '' }}{{ id }}').datetimepicker({{ dpoptions|jsonencode|raw }});
            });
        </script>
    {% endspaceless %}
{% endblock sonatatypedatetimepickerwidget %}
", "SonataCoreBundle:Form:datepicker.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/core-bundle/Resources/views/Form/datepicker.html.twig");
    }
}
