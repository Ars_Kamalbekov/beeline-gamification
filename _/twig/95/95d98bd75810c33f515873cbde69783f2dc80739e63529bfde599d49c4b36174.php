<?php

/* SonataAdminBundle:CRUD:editinteger.html.twig */
class TwigTemplate2c8627ccf530bc8b51a17d7153ba8344c09cd2e726ef1ef7488116d6eed5dee6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:editinteger.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale7b90716ff88afd77f4960e8882257faa9b13ec367c0be6b14b96db0bc17a799 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale7b90716ff88afd77f4960e8882257faa9b13ec367c0be6b14b96db0bc17a799->enter($internale7b90716ff88afd77f4960e8882257faa9b13ec367c0be6b14b96db0bc17a799prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editinteger.html.twig"));

        $internal3349db5cdcc991810ead07975fdb2c2395d1f2a68e63d5bca9831330ee5f310f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3349db5cdcc991810ead07975fdb2c2395d1f2a68e63d5bca9831330ee5f310f->enter($internal3349db5cdcc991810ead07975fdb2c2395d1f2a68e63d5bca9831330ee5f310fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editinteger.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internale7b90716ff88afd77f4960e8882257faa9b13ec367c0be6b14b96db0bc17a799->leave($internale7b90716ff88afd77f4960e8882257faa9b13ec367c0be6b14b96db0bc17a799prof);

        
        $internal3349db5cdcc991810ead07975fdb2c2395d1f2a68e63d5bca9831330ee5f310f->leave($internal3349db5cdcc991810ead07975fdb2c2395d1f2a68e63d5bca9831330ee5f310fprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal3d17ce3f130986f63b1fbb267a2db4baf720dc0ff3c9a7a79eaa39fdbbbc230e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3d17ce3f130986f63b1fbb267a2db4baf720dc0ff3c9a7a79eaa39fdbbbc230e->enter($internal3d17ce3f130986f63b1fbb267a2db4baf720dc0ff3c9a7a79eaa39fdbbbc230eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal7965059f12fb6a97d92678ec358385e3d817ca55d0b05ed66e6b83156c33a6fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7965059f12fb6a97d92678ec358385e3d817ca55d0b05ed66e6b83156c33a6fd->enter($internal7965059f12fb6a97d92678ec358385e3d817ca55d0b05ed66e6b83156c33a6fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $internal7965059f12fb6a97d92678ec358385e3d817ca55d0b05ed66e6b83156c33a6fd->leave($internal7965059f12fb6a97d92678ec358385e3d817ca55d0b05ed66e6b83156c33a6fdprof);

        
        $internal3d17ce3f130986f63b1fbb267a2db4baf720dc0ff3c9a7a79eaa39fdbbbc230e->leave($internal3d17ce3f130986f63b1fbb267a2db4baf720dc0ff3c9a7a79eaa39fdbbbc230eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editinteger.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block field %}{{ formwidget(fieldelement, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:editinteger.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editinteger.html.twig");
    }
}
