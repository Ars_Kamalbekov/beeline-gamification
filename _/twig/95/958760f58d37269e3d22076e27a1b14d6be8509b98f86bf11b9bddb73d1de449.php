<?php

/* SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig */
class TwigTemplate4d4c1b836c7ebea87cc1dfe74118ce39e253ba3009bc2bbe2e61c5acc3b9c48b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonatamosaicbackground' => array($this, 'blocksonatamosaicbackground'),
            'sonatamosaicdefaultview' => array($this, 'blocksonatamosaicdefaultview'),
            'sonatamosaichoverview' => array($this, 'blocksonatamosaichoverview'),
            'sonatamosaicdescription' => array($this, 'blocksonatamosaicdescription'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8180da019b6ef18bbba4f7f36b83611a123740e4ac45e3b2e623ac91802a5b59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8180da019b6ef18bbba4f7f36b83611a123740e4ac45e3b2e623ac91802a5b59->enter($internal8180da019b6ef18bbba4f7f36b83611a123740e4ac45e3b2e623ac91802a5b59prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig"));

        $internal6b80094a0168cbcf4d69ef5651730bacc80430f36ace417616d1e0fbce64bea8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b80094a0168cbcf4d69ef5651730bacc80430f36ace417616d1e0fbce64bea8->enter($internal6b80094a0168cbcf4d69ef5651730bacc80430f36ace417616d1e0fbce64bea8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig"));

        // line 11
        echo "
<!--
This template can be customized to match your needs. You should only extends the template and used the differents block to customize the view:
    - sonatamosaicdefaultview
    - sonatamosaichoverview
    - sonatamosaicdescription
-->

<tr>
    <td colspan=\"";
        // line 20
        echo twigescapefilter($this->env, (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "list", array()), "elements", array())) - ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array())) ? ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "action"), "method") + twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "batch"), "method"))) : (0))), "html", null, true);
        echo "\">
        <div class=\"row\">
            ";
        // line 22
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 22, $this->getSourceContext()); })()), "datagrid", array()), "results", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["object"]) {
            // line 23
            echo "                ";
            $context["meta"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "getObjectMetadata", array(0 => $context["object"]), "method");
            // line 24
            echo "
                <div class=\"col-xs-6 col-sm-3 mosaic-box sonata-ba-list-field-batch sonata-ba-list-field\" objectId=\"";
            // line 25
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "id", array(0 => $context["object"]), "method"), "html", null, true);
            echo "\">

                    <div class=\"mosaic-box-outter\">
                        <div class=\"mosaic-inner-box\">
                            ";
            // line 32
            echo "
                            <div class=\"mosaic-inner-box-default\">
                                ";
            // line 34
            $this->displayBlock('sonatamosaicbackground', $context, $blocks);
            // line 37
            echo "                                ";
            $this->displayBlock('sonatamosaicdefaultview', $context, $blocks);
            // line 40
            echo "                            </div>

                            ";
            // line 46
            echo "                            <div class=\"mosaic-inner-box-hover\">
                                ";
            // line 47
            $this->displayBlock('sonatamosaichoverview', $context, $blocks);
            // line 51
            echo "                            </div>
                        </div>

                        ";
            // line 55
            echo "                        ";
            $context["exportformats"] = ((arraykeyexists("exportformats", $context)) ? (twigdefaultfilter((isset($context["exportformats"]) || arraykeyexists("exportformats", $context) ? $context["exportformats"] : (function () { throw new TwigErrorRuntime('Variable "exportformats" does not exist.', 55, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 55, $this->getSourceContext()); })()), "getExportFormats", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 55, $this->getSourceContext()); })()), "getExportFormats", array())));
            // line 56
            echo "
                        <div class=\"mosaic-inner-text\">
                            ";
            // line 58
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 58, $this->getSourceContext()); })()), "hasRoute", array(0 => "batch"), "method") && (twiglengthfilter($this->env, (isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 58, $this->getSourceContext()); })())) > 0)) || ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 58, $this->getSourceContext()); })()), "hasRoute", array(0 => "export"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 58, $this->getSourceContext()); })()), "hasAccess", array(0 => "export"), "method")) && twiglengthfilter($this->env, (isset($context["exportformats"]) || arraykeyexists("exportformats", $context) ? $context["exportformats"] : (function () { throw new TwigErrorRuntime('Variable "exportformats" does not exist.', 58, $this->getSourceContext()); })()))))) {
                // line 59
                echo "                                <input type=\"checkbox\" name=\"idx[]\" value=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 59, $this->getSourceContext()); })()), "id", array(0 => $context["object"]), "method"), "html", null, true);
                echo "\">
                            ";
            } else {
                // line 61
                echo "                                &nbsp;
                            ";
            }
            // line 63
            echo "
                            ";
            // line 64
            $this->displayBlock('sonatamosaicdescription', $context, $blocks);
            // line 73
            echo "                        </div>
                    </div>
                </div>

                ";
            // line 77
            if (((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()) % 4) == 0)) {
                // line 78
                echo "                    <div class=\"clearfix hidden-xs\"></div>
                ";
            }
            // line 80
            echo "                ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()) % 2) == 0)) {
                // line 81
                echo "                    <div class=\"clearfix visible-xs\"></div>
                ";
            }
            // line 83
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['object'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 84
        echo "        </div>
    </td>
</tr>
";
        
        $internal8180da019b6ef18bbba4f7f36b83611a123740e4ac45e3b2e623ac91802a5b59->leave($internal8180da019b6ef18bbba4f7f36b83611a123740e4ac45e3b2e623ac91802a5b59prof);

        
        $internal6b80094a0168cbcf4d69ef5651730bacc80430f36ace417616d1e0fbce64bea8->leave($internal6b80094a0168cbcf4d69ef5651730bacc80430f36ace417616d1e0fbce64bea8prof);

    }

    // line 34
    public function blocksonatamosaicbackground($context, array $blocks = array())
    {
        $internal5e2b5c67f7902f4198deb3150ffa418da6443a74b0688246e4839269971c533b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5e2b5c67f7902f4198deb3150ffa418da6443a74b0688246e4839269971c533b->enter($internal5e2b5c67f7902f4198deb3150ffa418da6443a74b0688246e4839269971c533bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicbackground"));

        $internal4ecc6cc3781753351a37fd52e643a3f7ad6c217d1d55561ab04bedf9eba4d603 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4ecc6cc3781753351a37fd52e643a3f7ad6c217d1d55561ab04bedf9eba4d603->enter($internal4ecc6cc3781753351a37fd52e643a3f7ad6c217d1d55561ab04bedf9eba4d603prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicbackground"));

        // line 35
        echo "                                    <img src=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["meta"]) || arraykeyexists("meta", $context) ? $context["meta"] : (function () { throw new TwigErrorRuntime('Variable "meta" does not exist.', 35, $this->getSourceContext()); })()), "image", array()), "html", null, true);
        echo "\" alt=\"\" />
                                ";
        
        $internal4ecc6cc3781753351a37fd52e643a3f7ad6c217d1d55561ab04bedf9eba4d603->leave($internal4ecc6cc3781753351a37fd52e643a3f7ad6c217d1d55561ab04bedf9eba4d603prof);

        
        $internal5e2b5c67f7902f4198deb3150ffa418da6443a74b0688246e4839269971c533b->leave($internal5e2b5c67f7902f4198deb3150ffa418da6443a74b0688246e4839269971c533bprof);

    }

    // line 37
    public function blocksonatamosaicdefaultview($context, array $blocks = array())
    {
        $internal2a28dab24e92b4f9b1f5db59e2cd2123532984ecc4a807a2f708a31b0b4a9e93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2a28dab24e92b4f9b1f5db59e2cd2123532984ecc4a807a2f708a31b0b4a9e93->enter($internal2a28dab24e92b4f9b1f5db59e2cd2123532984ecc4a807a2f708a31b0b4a9e93prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicdefaultview"));

        $internal59af940573d3b0c3373251d0b2646697da655d458114d72b2f915e4b401924cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal59af940573d3b0c3373251d0b2646697da655d458114d72b2f915e4b401924cc->enter($internal59af940573d3b0c3373251d0b2646697da655d458114d72b2f915e4b401924ccprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicdefaultview"));

        // line 38
        echo "                                    <span class=\"mosaic-box-label label label-primary pull-right\">#";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 38, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "</span>
                                ";
        
        $internal59af940573d3b0c3373251d0b2646697da655d458114d72b2f915e4b401924cc->leave($internal59af940573d3b0c3373251d0b2646697da655d458114d72b2f915e4b401924ccprof);

        
        $internal2a28dab24e92b4f9b1f5db59e2cd2123532984ecc4a807a2f708a31b0b4a9e93->leave($internal2a28dab24e92b4f9b1f5db59e2cd2123532984ecc4a807a2f708a31b0b4a9e93prof);

    }

    // line 47
    public function blocksonatamosaichoverview($context, array $blocks = array())
    {
        $internala3b31f7f55d20b7738ab408d06f8b39ce841b4fb66631e65cde9b4f7f1f34e3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala3b31f7f55d20b7738ab408d06f8b39ce841b4fb66631e65cde9b4f7f1f34e3f->enter($internala3b31f7f55d20b7738ab408d06f8b39ce841b4fb66631e65cde9b4f7f1f34e3fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaichoverview"));

        $internal4a19b8ee6400d77bd4801c8dd80c01e81aa46b8d48ea071defca16a3bd0ece3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4a19b8ee6400d77bd4801c8dd80c01e81aa46b8d48ea071defca16a3bd0ece3a->enter($internal4a19b8ee6400d77bd4801c8dd80c01e81aa46b8d48ea071defca16a3bd0ece3aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaichoverview"));

        // line 48
        echo "                                    <span class=\"mosaic-box-label label label-primary pull-right\">#";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 48, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 48, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "</span>
                                    ";
        // line 49
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["meta"]) || arraykeyexists("meta", $context) ? $context["meta"] : (function () { throw new TwigErrorRuntime('Variable "meta" does not exist.', 49, $this->getSourceContext()); })()), "description", array()), "html", null, true);
        echo "
                                ";
        
        $internal4a19b8ee6400d77bd4801c8dd80c01e81aa46b8d48ea071defca16a3bd0ece3a->leave($internal4a19b8ee6400d77bd4801c8dd80c01e81aa46b8d48ea071defca16a3bd0ece3aprof);

        
        $internala3b31f7f55d20b7738ab408d06f8b39ce841b4fb66631e65cde9b4f7f1f34e3f->leave($internala3b31f7f55d20b7738ab408d06f8b39ce841b4fb66631e65cde9b4f7f1f34e3fprof);

    }

    // line 64
    public function blocksonatamosaicdescription($context, array $blocks = array())
    {
        $internal02413beab3578a6be891ad0649d237562ae1ef6f30ad195516a6b299efe53864 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal02413beab3578a6be891ad0649d237562ae1ef6f30ad195516a6b299efe53864->enter($internal02413beab3578a6be891ad0649d237562ae1ef6f30ad195516a6b299efe53864prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicdescription"));

        $internalb9f6c1a2845c78f041f4019c51f56b9978bd05628cc824966771f4f40834da7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb9f6c1a2845c78f041f4019c51f56b9978bd05628cc824966771f4f40834da7e->enter($internalb9f6c1a2845c78f041f4019c51f56b9978bd05628cc824966771f4f40834da7eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatamosaicdescription"));

        // line 65
        echo "                                ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 65, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 65, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 65, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method"))) {
            // line 66
            echo "                                    <a class=\"mosaic-inner-link\" href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 66, $this->getSourceContext()); })()), "generateUrl", array(0 => "edit", 1 => array("id" => $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getUrlsafeIdentifier((isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 66, $this->getSourceContext()); })()), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 66, $this->getSourceContext()); })())))), "method"), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["meta"]) || arraykeyexists("meta", $context) ? $context["meta"] : (function () { throw new TwigErrorRuntime('Variable "meta" does not exist.', 66, $this->getSourceContext()); })()), "title", array()), 40), "html", null, true);
            echo "</a>
                                ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(),         // line 67
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 67, $this->getSourceContext()); })()), "hasAccess", array(0 => "show", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 67, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 67, $this->getSourceContext()); })()), "hasRoute", array(0 => "show"), "method"))) {
            // line 68
            echo "                                    <a class=\"mosaic-inner-link\" href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 68, $this->getSourceContext()); })()), "generateUrl", array(0 => "show", 1 => array("id" => $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getUrlsafeIdentifier((isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 68, $this->getSourceContext()); })()), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 68, $this->getSourceContext()); })())))), "method"), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["meta"]) || arraykeyexists("meta", $context) ? $context["meta"] : (function () { throw new TwigErrorRuntime('Variable "meta" does not exist.', 68, $this->getSourceContext()); })()), "title", array()), 40), "html", null, true);
            echo "</a>
                                ";
        } else {
            // line 70
            echo "                                    ";
            echo twigescapefilter($this->env, twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["meta"]) || arraykeyexists("meta", $context) ? $context["meta"] : (function () { throw new TwigErrorRuntime('Variable "meta" does not exist.', 70, $this->getSourceContext()); })()), "title", array()), 40), "html", null, true);
            echo "
                                ";
        }
        // line 72
        echo "                            ";
        
        $internalb9f6c1a2845c78f041f4019c51f56b9978bd05628cc824966771f4f40834da7e->leave($internalb9f6c1a2845c78f041f4019c51f56b9978bd05628cc824966771f4f40834da7eprof);

        
        $internal02413beab3578a6be891ad0649d237562ae1ef6f30ad195516a6b299efe53864->leave($internal02413beab3578a6be891ad0649d237562ae1ef6f30ad195516a6b299efe53864prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  273 => 72,  267 => 70,  259 => 68,  257 => 67,  250 => 66,  247 => 65,  238 => 64,  226 => 49,  221 => 48,  212 => 47,  199 => 38,  190 => 37,  177 => 35,  168 => 34,  155 => 84,  141 => 83,  137 => 81,  134 => 80,  130 => 78,  128 => 77,  122 => 73,  120 => 64,  117 => 63,  113 => 61,  107 => 59,  105 => 58,  101 => 56,  98 => 55,  93 => 51,  91 => 47,  88 => 46,  84 => 40,  81 => 37,  79 => 34,  75 => 32,  68 => 25,  65 => 24,  62 => 23,  45 => 22,  40 => 20,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<!--
This template can be customized to match your needs. You should only extends the template and used the differents block to customize the view:
    - sonatamosaicdefaultview
    - sonatamosaichoverview
    - sonatamosaicdescription
-->

<tr>
    <td colspan=\"{{ admin.list.elements|length - (app.request.isXmlHttpRequest ? (admin.list.has('action') + admin.list.has('batch')) : 0) }}\">
        <div class=\"row\">
            {% for object in admin.datagrid.results %}
                {% set meta = admin.getObjectMetadata(object) %}

                <div class=\"col-xs-6 col-sm-3 mosaic-box sonata-ba-list-field-batch sonata-ba-list-field\" objectId=\"{{ admin.id(object) }}\">

                    <div class=\"mosaic-box-outter\">
                        <div class=\"mosaic-inner-box\">
                            {#
                                This box will be display when the mouse is not on the box
                            #}

                            <div class=\"mosaic-inner-box-default\">
                                {% block sonatamosaicbackground %}
                                    <img src=\"{{ meta.image }}\" alt=\"\" />
                                {% endblock %}
                                {% block sonatamosaicdefaultview %}
                                    <span class=\"mosaic-box-label label label-primary pull-right\">#{{ admin.id(object) }}</span>
                                {% endblock %}
                            </div>

                            {#
                                This box will be display when the mouse is on the box
                                You can add more description
                            #}
                            <div class=\"mosaic-inner-box-hover\">
                                {% block sonatamosaichoverview %}
                                    <span class=\"mosaic-box-label label label-primary pull-right\">#{{ admin.id(object) }}</span>
                                    {{ meta.description }}
                                {% endblock %}
                            </div>
                        </div>

                        {# NEXTMAJOR : remove this assignment #}
                        {% set exportformats = exportformats|default(admin.getExportFormats) %}

                        <div class=\"mosaic-inner-text\">
                            {% if (admin.hasRoute('batch') and batchactions|length > 0) or (admin.hasRoute('export') and admin.hasAccess('export') and exportformats|length) %}
                                <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\">
                            {% else %}
                                &nbsp;
                            {% endif %}

                            {% block sonatamosaicdescription %}
                                {% if admin.hasAccess('edit', object) and admin.hasRoute('edit') %}
                                    <a class=\"mosaic-inner-link\" href=\"{{ admin.generateUrl('edit', {'id' : object|sonataurlsafeid(admin) }) }}\">{{ meta.title|truncate(40) }}</a>
                                {% elseif admin.hasAccess('show', object) and admin.hasRoute('show') %}
                                    <a class=\"mosaic-inner-link\" href=\"{{ admin.generateUrl('show', {'id' : object|sonataurlsafeid(admin) }) }}\">{{ meta.title|truncate(40) }}</a>
                                {% else %}
                                    {{ meta.title|truncate(40) }}
                                {% endif %}
                            {% endblock %}
                        </div>
                    </div>
                </div>

                {% if loop.index % 4 == 0 %}
                    <div class=\"clearfix hidden-xs\"></div>
                {% endif %}
                {% if loop.index % 2 == 0 %}
                    <div class=\"clearfix visible-xs\"></div>
                {% endif %}
            {% endfor %}
        </div>
    </td>
</tr>
", "SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listouterrowsmosaic.html.twig");
    }
}
