<?php

/* SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig */
class TwigTemplate108bacb335b5898a2e8a708b0399a9cc0a7ffbec9f609ee5b1a0dee622cc9349 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4af57564444cc772d0cecb136fd5d16d8be8a568f49366b39655f0faa26829eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4af57564444cc772d0cecb136fd5d16d8be8a568f49366b39655f0faa26829eb->enter($internal4af57564444cc772d0cecb136fd5d16d8be8a568f49366b39655f0faa26829ebprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig"));

        $internal0e4cd91ec3980ee7da559960952e3c27b3eb1edfb5c395af1d8a3dd6c4258019 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0e4cd91ec3980ee7da559960952e3c27b3eb1edfb5c395af1d8a3dd6c4258019->enter($internal0e4cd91ec3980ee7da559960952e3c27b3eb1edfb5c395af1d8a3dd6c4258019prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig"));

        // line 11
        echo "
";
        // line 12
        echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["revision"]) || arraykeyexists("revision", $context) ? $context["revision"] : (function () { throw new TwigErrorRuntime('Variable "revision" does not exist.', 12, $this->getSourceContext()); })()), "timestamp", array())), "html", null, true);
        echo "
";
        
        $internal4af57564444cc772d0cecb136fd5d16d8be8a568f49366b39655f0faa26829eb->leave($internal4af57564444cc772d0cecb136fd5d16d8be8a568f49366b39655f0faa26829ebprof);

        
        $internal0e4cd91ec3980ee7da559960952e3c27b3eb1edfb5c395af1d8a3dd6c4258019->leave($internal0e4cd91ec3980ee7da559960952e3c27b3eb1edfb5c395af1d8a3dd6c4258019prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{{ revision.timestamp|date }}
", "SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/historyrevisiontimestamp.html.twig");
    }
}
