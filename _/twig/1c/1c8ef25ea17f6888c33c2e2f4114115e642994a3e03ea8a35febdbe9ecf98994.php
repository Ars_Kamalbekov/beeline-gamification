<?php

/* SonataAdminBundle:CRUD:baselistinnerrow.html.twig */
class TwigTemplate0e96702b1c2546946c9b6c1ee970a01d0e61932452a3513c28cfda3f296e655b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalea008c770d74fce0bcd51b6f3afd178a85f5b8f16a23dfa443721fc5cedf4971 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalea008c770d74fce0bcd51b6f3afd178a85f5b8f16a23dfa443721fc5cedf4971->enter($internalea008c770d74fce0bcd51b6f3afd178a85f5b8f16a23dfa443721fc5cedf4971prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistinnerrow.html.twig"));

        $internalc4c56cb31606a07b7815e98e4097d49919c653fa13bbddc9ebcd298880040d61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc4c56cb31606a07b7815e98e4097d49919c653fa13bbddc9ebcd298880040d61->enter($internalc4c56cb31606a07b7815e98e4097d49919c653fa13bbddc9ebcd298880040d61prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistinnerrow.html.twig"));

        // line 11
        echo "
";
        // line 12
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "list", array()), "elements", array()));
        foreach ($context['seq'] as $context["key"] => $context["fielddescription"]) {
            // line 13
            echo "    ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "name", array()) == "action") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 13, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
                // line 14
                echo "        ";
                // line 15
                echo "    ";
            } elseif (((twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "getOption", array(0 => "ajaxhidden"), "method") == true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 15, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
                // line 16
                echo "        ";
                // line 17
                echo "    ";
            } else {
                // line 18
                echo "        ";
                echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderListElement($this->env, (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 18, $this->getSourceContext()); })()), $context["fielddescription"]);
                echo "
    ";
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['fielddescription'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internalea008c770d74fce0bcd51b6f3afd178a85f5b8f16a23dfa443721fc5cedf4971->leave($internalea008c770d74fce0bcd51b6f3afd178a85f5b8f16a23dfa443721fc5cedf4971prof);

        
        $internalc4c56cb31606a07b7815e98e4097d49919c653fa13bbddc9ebcd298880040d61->leave($internalc4c56cb31606a07b7815e98e4097d49919c653fa13bbddc9ebcd298880040d61prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baselistinnerrow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 18,  42 => 17,  40 => 16,  37 => 15,  35 => 14,  32 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% for fielddescription in admin.list.elements %}
    {% if fielddescription.name == 'action' and app.request.isXmlHttpRequest %}
        {# Action buttons disabled in ajax view! #}
    {% elseif fielddescription.getOption('ajaxhidden') == true and app.request.isXmlHttpRequest %}
        {# Disable fields with 'ajaxhidden' option set to true #}
    {% else %}
        {{ object|renderlistelement(fielddescription) }}
    {% endif %}
{% endfor %}
", "SonataAdminBundle:CRUD:baselistinnerrow.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baselistinnerrow.html.twig");
    }
}
