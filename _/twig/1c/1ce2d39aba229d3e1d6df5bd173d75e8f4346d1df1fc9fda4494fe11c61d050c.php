<?php

/* @Framework/Form/formenctype.html.php */
class TwigTemplate14fbc10fd236263cf797fb3b88b4a9051bda298252f4e40e7ce3b77295ba70f7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2caf38307fde1f0e58832b19d99a017bd92bb9bfa40b6cc1362233fd1e2b4b36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2caf38307fde1f0e58832b19d99a017bd92bb9bfa40b6cc1362233fd1e2b4b36->enter($internal2caf38307fde1f0e58832b19d99a017bd92bb9bfa40b6cc1362233fd1e2b4b36prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formenctype.html.php"));

        $internalc6fe5cc16a4e7709f634a30ed4a711aac31ccf434036a0a8e57f5ea3f20b515b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc6fe5cc16a4e7709f634a30ed4a711aac31ccf434036a0a8e57f5ea3f20b515b->enter($internalc6fe5cc16a4e7709f634a30ed4a711aac31ccf434036a0a8e57f5ea3f20b515bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formenctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $internal2caf38307fde1f0e58832b19d99a017bd92bb9bfa40b6cc1362233fd1e2b4b36->leave($internal2caf38307fde1f0e58832b19d99a017bd92bb9bfa40b6cc1362233fd1e2b4b36prof);

        
        $internalc6fe5cc16a4e7709f634a30ed4a711aac31ccf434036a0a8e57f5ea3f20b515b->leave($internalc6fe5cc16a4e7709f634a30ed4a711aac31ccf434036a0a8e57f5ea3f20b515bprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formenctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/formenctype.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formenctype.html.php");
    }
}
