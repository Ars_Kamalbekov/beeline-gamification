<?php

/* TwigBundle:Exception:trace.txt.twig */
class TwigTemplateca41007118b457019ddb5d25ec7c0508fb57649e4ae8a61f509aae4f0f2de4c4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal951f232e553b74e125aec6223ee574fbcef2fdbb496845b4b6fb69d6f7c9b272 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal951f232e553b74e125aec6223ee574fbcef2fdbb496845b4b6fb69d6f7c9b272->enter($internal951f232e553b74e125aec6223ee574fbcef2fdbb496845b4b6fb69d6f7c9b272prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:trace.txt.twig"));

        $internale6e099a15f39aeb5d59d6f7fc1d9e11eae18fd1536238265e58107694522aa2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale6e099a15f39aeb5d59d6f7fc1d9e11eae18fd1536238265e58107694522aa2c->enter($internale6e099a15f39aeb5d59d6f7fc1d9e11eae18fd1536238265e58107694522aa2cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:trace.txt.twig"));

        // line 1
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 1, $this->getSourceContext()); })()), "function", array())) {
            // line 2
            echo "at ";
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 2, $this->getSourceContext()); })()), "class", array()) . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 2, $this->getSourceContext()); })()), "type", array())) . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 2, $this->getSourceContext()); })()), "function", array()));
            echo "(";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatArgs(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 2, $this->getSourceContext()); })()), "args", array()));
            echo ")";
        }
        // line 4
        if (( !twigtestempty(((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", array()), "")) : (""))) &&  !twigtestempty(((twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", array()), "")) : (""))))) {
            // line 5
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "function", array())) ? ("
     (") : ("at "));
            echo twigreplacefilter(striptags($this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatFile(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "file", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "line", array()))), array((" at line " . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "line", array())) => ""));
            echo ":";
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "line", array());
            echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || arraykeyexists("trace", $context) ? $context["trace"] : (function () { throw new TwigErrorRuntime('Variable "trace" does not exist.', 5, $this->getSourceContext()); })()), "function", array())) ? (")") : (""));
        }
        
        $internal951f232e553b74e125aec6223ee574fbcef2fdbb496845b4b6fb69d6f7c9b272->leave($internal951f232e553b74e125aec6223ee574fbcef2fdbb496845b4b6fb69d6f7c9b272prof);

        
        $internale6e099a15f39aeb5d59d6f7fc1d9e11eae18fd1536238265e58107694522aa2c->leave($internale6e099a15f39aeb5d59d6f7fc1d9e11eae18fd1536238265e58107694522aa2cprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:trace.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 5,  34 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% if trace.function %}
at {{ trace.class ~ trace.type ~ trace.function }}({{ trace.args|formatargs }})
{%- endif -%}
{% if trace.file|default('') is not empty and trace.line|default('') is not empty %}
  {{- trace.function ? '\\n     (' : 'at '}}{{ trace.file|formatfile(trace.line)|striptags|replace({ (' at line ' ~ trace.line): '' }) }}:{{ trace.line }}{{ trace.function ? ')' }}
{%- endif %}
", "TwigBundle:Exception:trace.txt.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/trace.txt.twig");
    }
}
