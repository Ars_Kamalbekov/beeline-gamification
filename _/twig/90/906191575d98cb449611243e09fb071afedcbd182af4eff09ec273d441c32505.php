<?php

/* SonataAdminBundle:CRUD:editstring.html.twig */
class TwigTemplate740be910978a1ec4aeb03c3b67424f06dd3734b94c3ff0370c4e12ba5fac090a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:editstring.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal5b8ba8ecfbc37af610f2e6d67cff71dda9735e4a356d0c525a32b97449fc3cda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5b8ba8ecfbc37af610f2e6d67cff71dda9735e4a356d0c525a32b97449fc3cda->enter($internal5b8ba8ecfbc37af610f2e6d67cff71dda9735e4a356d0c525a32b97449fc3cdaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editstring.html.twig"));

        $internal9d1e9dd4e409d004bf762ee00df2c9be1c1ed62fa08c8837f0c9fe5550f634ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9d1e9dd4e409d004bf762ee00df2c9be1c1ed62fa08c8837f0c9fe5550f634ec->enter($internal9d1e9dd4e409d004bf762ee00df2c9be1c1ed62fa08c8837f0c9fe5550f634ecprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editstring.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal5b8ba8ecfbc37af610f2e6d67cff71dda9735e4a356d0c525a32b97449fc3cda->leave($internal5b8ba8ecfbc37af610f2e6d67cff71dda9735e4a356d0c525a32b97449fc3cdaprof);

        
        $internal9d1e9dd4e409d004bf762ee00df2c9be1c1ed62fa08c8837f0c9fe5550f634ec->leave($internal9d1e9dd4e409d004bf762ee00df2c9be1c1ed62fa08c8837f0c9fe5550f634ecprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal7e589d03c457638ab9c220aa1cbdf704a1f182ff88a4c56c7e835d88d169b9e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7e589d03c457638ab9c220aa1cbdf704a1f182ff88a4c56c7e835d88d169b9e3->enter($internal7e589d03c457638ab9c220aa1cbdf704a1f182ff88a4c56c7e835d88d169b9e3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalbb87ff17d7b3b7bc502ba7db476830fdbabc3839190abefa51d8682dbe80f2ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbb87ff17d7b3b7bc502ba7db476830fdbabc3839190abefa51d8682dbe80f2ed->enter($internalbb87ff17d7b3b7bc502ba7db476830fdbabc3839190abefa51d8682dbe80f2edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $internalbb87ff17d7b3b7bc502ba7db476830fdbabc3839190abefa51d8682dbe80f2ed->leave($internalbb87ff17d7b3b7bc502ba7db476830fdbabc3839190abefa51d8682dbe80f2edprof);

        
        $internal7e589d03c457638ab9c220aa1cbdf704a1f182ff88a4c56c7e835d88d169b9e3->leave($internal7e589d03c457638ab9c220aa1cbdf704a1f182ff88a4c56c7e835d88d169b9e3prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editstring.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block field %}{{ formwidget(fieldelement, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:editstring.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editstring.html.twig");
    }
}
