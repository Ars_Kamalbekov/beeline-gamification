<?php

/* SonataAdminBundle:CRUD:editfile.html.twig */
class TwigTemplate9b8d047346a1e42447ef007a72d6f6aae473b466748d869ec43aa6c383d1cc0e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:editfile.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal244b7ef21767cc3015e5bbfa41a6997baad6a74b79105b3a8eb25c914fb6592f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal244b7ef21767cc3015e5bbfa41a6997baad6a74b79105b3a8eb25c914fb6592f->enter($internal244b7ef21767cc3015e5bbfa41a6997baad6a74b79105b3a8eb25c914fb6592fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editfile.html.twig"));

        $internald897374b7c63d9f1431c4247a4ed01b4b56462684cf4f4ea6bb9f53c04f99a2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald897374b7c63d9f1431c4247a4ed01b4b56462684cf4f4ea6bb9f53c04f99a2c->enter($internald897374b7c63d9f1431c4247a4ed01b4b56462684cf4f4ea6bb9f53c04f99a2cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editfile.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal244b7ef21767cc3015e5bbfa41a6997baad6a74b79105b3a8eb25c914fb6592f->leave($internal244b7ef21767cc3015e5bbfa41a6997baad6a74b79105b3a8eb25c914fb6592fprof);

        
        $internald897374b7c63d9f1431c4247a4ed01b4b56462684cf4f4ea6bb9f53c04f99a2c->leave($internald897374b7c63d9f1431c4247a4ed01b4b56462684cf4f4ea6bb9f53c04f99a2cprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalcd1eb0658360b53dfbbd688aaf3c9c46f6d012d0d2f80a0353829b034c808b6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcd1eb0658360b53dfbbd688aaf3c9c46f6d012d0d2f80a0353829b034c808b6a->enter($internalcd1eb0658360b53dfbbd688aaf3c9c46f6d012d0d2f80a0353829b034c808b6aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalf15f6fb6bf9449d04c1e27ce238feab169ef18773be466cacbece71ef39a8e6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf15f6fb6bf9449d04c1e27ce238feab169ef18773be466cacbece71ef39a8e6a->enter($internalf15f6fb6bf9449d04c1e27ce238feab169ef18773be466cacbece71ef39a8e6aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 14, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        
        $internalf15f6fb6bf9449d04c1e27ce238feab169ef18773be466cacbece71ef39a8e6a->leave($internalf15f6fb6bf9449d04c1e27ce238feab169ef18773be466cacbece71ef39a8e6aprof);

        
        $internalcd1eb0658360b53dfbbd688aaf3c9c46f6d012d0d2f80a0353829b034c808b6a->leave($internalcd1eb0658360b53dfbbd688aaf3c9c46f6d012d0d2f80a0353829b034c808b6aprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editfile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block field %}{{ formwidget(fieldelement, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:editfile.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editfile.html.twig");
    }
}
