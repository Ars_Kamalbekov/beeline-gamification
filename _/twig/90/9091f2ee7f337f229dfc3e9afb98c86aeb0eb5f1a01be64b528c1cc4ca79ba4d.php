<?php

/* @SonataBlock/Profiler/icon.svg */
class TwigTemplate988c43e1a52f57989ed570e2c8f001d62484a8e0426cd0a56d6f11b09795e5c5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal439f6127bc5b93f5f45d93149791a1cae27ff8b9e303eb87c9d3bf704d83ff77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal439f6127bc5b93f5f45d93149791a1cae27ff8b9e303eb87c9d3bf704d83ff77->enter($internal439f6127bc5b93f5f45d93149791a1cae27ff8b9e303eb87c9d3bf704d83ff77prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        $internal42b0f794d5cd538b39440c607b2439d7eabb28dfbb06299041b00d81fbe52149 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal42b0f794d5cd538b39440c607b2439d7eabb28dfbb06299041b00d81fbe52149->enter($internal42b0f794d5cd538b39440c607b2439d7eabb28dfbb06299041b00d81fbe52149prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        // line 1
        echo "<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
";
        
        $internal439f6127bc5b93f5f45d93149791a1cae27ff8b9e303eb87c9d3bf704d83ff77->leave($internal439f6127bc5b93f5f45d93149791a1cae27ff8b9e303eb87c9d3bf704d83ff77prof);

        
        $internal42b0f794d5cd538b39440c607b2439d7eabb28dfbb06299041b00d81fbe52149->leave($internal42b0f794d5cd538b39440c607b2439d7eabb28dfbb06299041b00d81fbe52149prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Profiler/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
", "@SonataBlock/Profiler/icon.svg", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Profiler/icon.svg");
    }
}
