<?php

/* SonataAdminBundle:CRUD:baseeditformmacro.html.twig */
class TwigTemplate397c866309b70af396836f9734b86dd914e02ed802123671e577d3a86c9ebc95 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal209decfafe00769740aecc38deb699fcd7bae229f8f7e4ed5905e562cf0f05f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal209decfafe00769740aecc38deb699fcd7bae229f8f7e4ed5905e562cf0f05f8->enter($internal209decfafe00769740aecc38deb699fcd7bae229f8f7e4ed5905e562cf0f05f8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseeditformmacro.html.twig"));

        $internal2cec57f65bf509940fc605f70859819d11508ad97685533539e18b1b7bf48415 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2cec57f65bf509940fc605f70859819d11508ad97685533539e18b1b7bf48415->enter($internal2cec57f65bf509940fc605f70859819d11508ad97685533539e18b1b7bf48415prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseeditformmacro.html.twig"));

        
        $internal209decfafe00769740aecc38deb699fcd7bae229f8f7e4ed5905e562cf0f05f8->leave($internal209decfafe00769740aecc38deb699fcd7bae229f8f7e4ed5905e562cf0f05f8prof);

        
        $internal2cec57f65bf509940fc605f70859819d11508ad97685533539e18b1b7bf48415->leave($internal2cec57f65bf509940fc605f70859819d11508ad97685533539e18b1b7bf48415prof);

    }

    // line 1
    public function macrorendergroups($admin = null, $form = null, $groups = null, $hastab = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "admin" => $admin,
            "form" => $form,
            "groups" => $groups,
            "hastab" => $hastab,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal56e5229dd176bfa8c5741111f5ae6d6f1ba8f1c42686ffc3c29efaf7ead8b19a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal56e5229dd176bfa8c5741111f5ae6d6f1ba8f1c42686ffc3c29efaf7ead8b19a->enter($internal56e5229dd176bfa8c5741111f5ae6d6f1ba8f1c42686ffc3c29efaf7ead8b19aprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendergroups"));

            $internal73ad45f3c166342bd19c656e2e9cca22d0db8d994c4a3992f35b46747a7ba8f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal73ad45f3c166342bd19c656e2e9cca22d0db8d994c4a3992f35b46747a7ba8f3->enter($internal73ad45f3c166342bd19c656e2e9cca22d0db8d994c4a3992f35b46747a7ba8f3prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendergroups"));

            // line 2
            echo "    <div class=\"row\">

    ";
            // line 4
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 4, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["code"]) {
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "formgroups", array(), "any", false, true), $context["code"], array(), "array", true, true)) {
                    // line 5
                    echo "        ";
                    $context["formgroup"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 5, $this->getSourceContext()); })()), "formgroups", array()), $context["code"], array(), "array");
                    // line 6
                    echo "
        <div class=\"";
                    // line 7
                    echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["formgroup"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["formgroup"] ?? null), "class", array()), "col-md-12")) : ("col-md-12")), "html", null, true);
                    echo "\">
            <div class=\"";
                    // line 8
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 8, $this->getSourceContext()); })()), "boxclass", array()), "html", null, true);
                    echo "\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        ";
                    // line 11
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 11, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 11, $this->getSourceContext()); })()), "translationdomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 11, $this->getSourceContext()); })()), "translationdomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                    echo "
                    </h4>
                </div>
                <div class=\"box-body\">
                    <div class=\"sonata-ba-collapsed-fields\">
                        ";
                    // line 16
                    if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 16, $this->getSourceContext()); })()), "description", array())) {
                        // line 17
                        echo "                            <p>";
                        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 17, $this->getSourceContext()); })()), "description", array());
                        echo "</p>
                        ";
                    }
                    // line 19
                    echo "
                        ";
                    // line 20
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["formgroup"]) || arraykeyexists("formgroup", $context) ? $context["formgroup"] : (function () { throw new TwigErrorRuntime('Variable "formgroup" does not exist.', 20, $this->getSourceContext()); })()), "fields", array()));
                    $context['iterated'] = false;
                    foreach ($context['seq'] as $context["key"] => $context["fieldname"]) {
                        if (twiggetattribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), $context["fieldname"], array(), "array", true, true)) {
                            // line 21
                            echo "                            ";
                            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 21, $this->getSourceContext()); })()), $context["fieldname"], array(), "array"), 'row');
                            echo "
                        ";
                            $context['iterated'] = true;
                        }
                    }
                    if (!$context['iterated']) {
                        // line 23
                        echo "                            <em>";
                        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("messageformgroupempty", array(), "SonataAdminBundle"), "html", null, true);
                        echo "</em>
                        ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['fieldname'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 25
                    echo "                    </div>
                </div>
            </div>
        </div>
    ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['code'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 30
            echo "    </div>
";
            
            $internal73ad45f3c166342bd19c656e2e9cca22d0db8d994c4a3992f35b46747a7ba8f3->leave($internal73ad45f3c166342bd19c656e2e9cca22d0db8d994c4a3992f35b46747a7ba8f3prof);

            
            $internal56e5229dd176bfa8c5741111f5ae6d6f1ba8f1c42686ffc3c29efaf7ead8b19a->leave($internal56e5229dd176bfa8c5741111f5ae6d6f1ba8f1c42686ffc3c29efaf7ead8b19aprof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseeditformmacro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 30,  122 => 25,  113 => 23,  104 => 21,  98 => 20,  95 => 19,  89 => 17,  87 => 16,  79 => 11,  73 => 8,  69 => 7,  66 => 6,  63 => 5,  58 => 4,  54 => 2,  33 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% macro rendergroups(admin, form, groups, hastab) %}
    <div class=\"row\">

    {% for code in groups if admin.formgroups[code] is defined %}
        {% set formgroup = admin.formgroups[code] %}

        <div class=\"{{ formgroup.class|default('col-md-12') }}\">
            <div class=\"{{ formgroup.boxclass }}\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        {{ formgroup.label|trans({}, formgroup.translationdomain ?: admin.translationDomain) }}
                    </h4>
                </div>
                <div class=\"box-body\">
                    <div class=\"sonata-ba-collapsed-fields\">
                        {% if formgroup.description %}
                            <p>{{ formgroup.description|raw }}</p>
                        {% endif %}

                        {% for fieldname in formgroup.fields if form[fieldname] is defined %}
                            {{ formrow(form[fieldname])}}
                        {% else %}
                            <em>{{ 'messageformgroupempty'|trans({}, 'SonataAdminBundle') }}</em>
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    {% endfor %}
    </div>
{% endmacro %}
", "SonataAdminBundle:CRUD:baseeditformmacro.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseeditformmacro.html.twig");
    }
}
