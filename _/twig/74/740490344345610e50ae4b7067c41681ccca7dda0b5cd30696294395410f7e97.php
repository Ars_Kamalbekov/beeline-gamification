<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class TwigTemplate802e37cf39c27cb3ca75a5ebb2fe3583350f4b13b4672e4871ae6443dc231acc extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'blocksummary'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb27f7ca5a20fe7943d7f7e1f1d2433e0ccd1604a407bf9f6ff22aeb22f626cc8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb27f7ca5a20fe7943d7f7e1f1d2433e0ccd1604a407bf9f6ff22aeb22f626cc8->enter($internalb27f7ca5a20fe7943d7f7e1f1d2433e0ccd1604a407bf9f6ff22aeb22f626cc8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $internalf0eb37643be8e856171b6dbcd1b35ca65e2ca22798914cdbe35d05788cab1018 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf0eb37643be8e856171b6dbcd1b35ca65e2ca22798914cdbe35d05788cab1018->enter($internalf0eb37643be8e856171b6dbcd1b35ca65e2ca22798914cdbe35d05788cab1018prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("notoken" => array("status" => "error", "title" => (((((        // line 6
arraykeyexists("token", $context)) ? (twigdefaultfilter((isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
arraykeyexists("token", $context)) ? (twigdefaultfilter((isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((arraykeyexists("token", $context)) ? (twigdefaultfilter((isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb27f7ca5a20fe7943d7f7e1f1d2433e0ccd1604a407bf9f6ff22aeb22f626cc8->leave($internalb27f7ca5a20fe7943d7f7e1f1d2433e0ccd1604a407bf9f6ff22aeb22f626cc8prof);

        
        $internalf0eb37643be8e856171b6dbcd1b35ca65e2ca22798914cdbe35d05788cab1018->leave($internalf0eb37643be8e856171b6dbcd1b35ca65e2ca22798914cdbe35d05788cab1018prof);

    }

    // line 11
    public function blocksummary($context, array $blocks = array())
    {
        $internal723ed6ebda14e2a7c9b0ba9ea0194f14712889387ca6dc81a8a67d53c9071946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal723ed6ebda14e2a7c9b0ba9ea0194f14712889387ca6dc81a8a67d53c9071946->enter($internal723ed6ebda14e2a7c9b0ba9ea0194f14712889387ca6dc81a8a67d53c9071946prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        $internalcb39631d74b5a37a81fec5a65a74be096abb659f01b9602f24ddeeb5d91432d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcb39631d74b5a37a81fec5a65a74be096abb659f01b9602f24ddeeb5d91432d5->enter($internalcb39631d74b5a37a81fec5a65a74be096abb659f01b9602f24ddeeb5d91432d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || arraykeyexists("messages", $context) ? $context["messages"] : (function () { throw new TwigErrorRuntime('Variable "messages" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["about"]) || arraykeyexists("about", $context) ? $context["about"] : (function () { throw new TwigErrorRuntime('Variable "about" does not exist.', 12, $this->getSourceContext()); })()), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twigescapefilter($this->env, twigtitlestringfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || arraykeyexists("messages", $context) ? $context["messages"] : (function () { throw new TwigErrorRuntime('Variable "messages" does not exist.', 14, $this->getSourceContext()); })()), (isset($context["about"]) || arraykeyexists("about", $context) ? $context["about"] : (function () { throw new TwigErrorRuntime('Variable "about" does not exist.', 14, $this->getSourceContext()); })()), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $internalcb39631d74b5a37a81fec5a65a74be096abb659f01b9602f24ddeeb5d91432d5->leave($internalcb39631d74b5a37a81fec5a65a74be096abb659f01b9602f24ddeeb5d91432d5prof);

        
        $internal723ed6ebda14e2a7c9b0ba9ea0194f14712889387ca6dc81a8a67d53c9071946->leave($internal723ed6ebda14e2a7c9b0ba9ea0194f14712889387ca6dc81a8a67d53c9071946prof);

    }

    // line 19
    public function blockpanel($context, array $blocks = array())
    {
        $internal9d69bb4cb8fd34685966f723c7249a4f102ce8b21990d9705cf5ccb25047acd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9d69bb4cb8fd34685966f723c7249a4f102ce8b21990d9705cf5ccb25047acd8->enter($internal9d69bb4cb8fd34685966f723c7249a4f102ce8b21990d9705cf5ccb25047acd8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal67b8e2c2bbe27744c7c08ceaa7265e1447964e029a1fea2776c5adeb816ef697 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal67b8e2c2bbe27744c7c08ceaa7265e1447964e029a1fea2776c5adeb816ef697->enter($internal67b8e2c2bbe27744c7c08ceaa7265e1447964e029a1fea2776c5adeb816ef697prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || arraykeyexists("messages", $context) ? $context["messages"] : (function () { throw new TwigErrorRuntime('Variable "messages" does not exist.', 20, $this->getSourceContext()); })()), (isset($context["about"]) || arraykeyexists("about", $context) ? $context["about"] : (function () { throw new TwigErrorRuntime('Variable "about" does not exist.', 20, $this->getSourceContext()); })()), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || arraykeyexists("messages", $context) ? $context["messages"] : (function () { throw new TwigErrorRuntime('Variable "messages" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["about"]) || arraykeyexists("about", $context) ? $context["about"] : (function () { throw new TwigErrorRuntime('Variable "about" does not exist.', 21, $this->getSourceContext()); })()), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $internal67b8e2c2bbe27744c7c08ceaa7265e1447964e029a1fea2776c5adeb816ef697->leave($internal67b8e2c2bbe27744c7c08ceaa7265e1447964e029a1fea2776c5adeb816ef697prof);

        
        $internal9d69bb4cb8fd34685966f723c7249a4f102ce8b21990d9705cf5ccb25047acd8->leave($internal9d69bb4cb8fd34685966f723c7249a4f102ce8b21990d9705cf5ccb25047acd8prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'notoken' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
