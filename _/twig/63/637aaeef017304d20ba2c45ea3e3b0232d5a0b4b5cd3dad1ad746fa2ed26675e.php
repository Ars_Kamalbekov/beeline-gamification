<?php

/* SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig */
class TwigTemplate817dc685761755bf155ee0acab33fde79642955f40f9d6f4dcb96651aece5f48 extends TwigTemplate
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb9a3bb23eff94728f8602aa80fed285ec3e46394bd50a6b05f6e4f452ad6d349 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb9a3bb23eff94728f8602aa80fed285ec3e46394bd50a6b05f6e4f452ad6d349->enter($internalb9a3bb23eff94728f8602aa80fed285ec3e46394bd50a6b05f6e4f452ad6d349prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig"));

        $internal35ee2a57c13352cb4c66ce6166c64cb3321340c9e98ba8687d70b22d7d602df3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal35ee2a57c13352cb4c66ce6166c64cb3321340c9e98ba8687d70b22d7d602df3->enter($internal35ee2a57c13352cb4c66ce6166c64cb3321340c9e98ba8687d70b22d7d602df3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb9a3bb23eff94728f8602aa80fed285ec3e46394bd50a6b05f6e4f452ad6d349->leave($internalb9a3bb23eff94728f8602aa80fed285ec3e46394bd50a6b05f6e4f452ad6d349prof);

        
        $internal35ee2a57c13352cb4c66ce6166c64cb3321340c9e98ba8687d70b22d7d602df3->leave($internal35ee2a57c13352cb4c66ce6166c64cb3321340c9e98ba8687d70b22d7d602df3prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}
", "SonataAdminBundle:CRUD:editsonatatypeimmutablearray.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editsonatatypeimmutablearray.html.twig");
    }
}
