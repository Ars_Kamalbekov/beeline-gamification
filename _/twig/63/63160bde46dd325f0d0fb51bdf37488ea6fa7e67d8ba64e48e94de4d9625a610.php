<?php

/* SonataAdminBundle:Form/Type:sonatatypemodelautocomplete.html.twig */
class TwigTemplate7fb8e7fbec47a554d67f046e9314684be231ddcdae2990d143d5e85a94b49a79 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonatatypemodelautocompleteajaxrequestparameters' => array($this, 'blocksonatatypemodelautocompleteajaxrequestparameters'),
            'sonatatypemodelautocompletedropdownitemformat' => array($this, 'blocksonatatypemodelautocompletedropdownitemformat'),
            'sonatatypemodelautocompleteselectionformat' => array($this, 'blocksonatatypemodelautocompleteselectionformat'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8e0fabff1975527c35ed49660b5a5ca184de8458c9c9a6dfa426288a34f9f6ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8e0fabff1975527c35ed49660b5a5ca184de8458c9c9a6dfa426288a34f9f6ac->enter($internal8e0fabff1975527c35ed49660b5a5ca184de8458c9c9a6dfa426288a34f9f6acprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form/Type:sonatatypemodelautocomplete.html.twig"));

        $internal6cd8f6da5925b34b44c34790ed3813b8c3f56c9c8f9709c7968efca513218916 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6cd8f6da5925b34b44c34790ed3813b8c3f56c9c8f9709c7968efca513218916->enter($internal6cd8f6da5925b34b44c34790ed3813b8c3f56c9c8f9709c7968efca513218916prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form/Type:sonatatypemodelautocomplete.html.twig"));

        // line 11
        obstart();
        // line 12
        echo "
    <input type=\"text\" id=\"";
        // line 13
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 13, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinput\" value=\"\"";
        // line 14
        if ((arraykeyexists("readonly", $context) && (isset($context["readonly"]) || arraykeyexists("readonly", $context) ? $context["readonly"] : (function () { throw new TwigErrorRuntime('Variable "readonly" does not exist.', 14, $this->getSourceContext()); })()))) {
            echo " readonly=\"readonly\"";
        }
        // line 15
        if ((isset($context["disabled"]) || arraykeyexists("disabled", $context) ? $context["disabled"] : (function () { throw new TwigErrorRuntime('Variable "disabled" does not exist.', 15, $this->getSourceContext()); })())) {
            echo " disabled=\"disabled\"";
        }
        // line 16
        if ((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 16, $this->getSourceContext()); })())) {
            echo " required=\"required\"";
        }
        // line 17
        echo "    />

    <select id=\"";
        // line 19
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinputv4\" data-sonata-select2=\"false\"";
        // line 20
        if ((arraykeyexists("readonly", $context) && (isset($context["readonly"]) || arraykeyexists("readonly", $context) ? $context["readonly"] : (function () { throw new TwigErrorRuntime('Variable "readonly" does not exist.', 20, $this->getSourceContext()); })()))) {
            echo " readonly=\"readonly\"";
        }
        // line 21
        if ((isset($context["disabled"]) || arraykeyexists("disabled", $context) ? $context["disabled"] : (function () { throw new TwigErrorRuntime('Variable "disabled" does not exist.', 21, $this->getSourceContext()); })())) {
            echo " disabled=\"disabled\"";
        }
        // line 22
        if ((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 22, $this->getSourceContext()); })())) {
            echo " required=\"required\"";
        }
        // line 23
        echo "    >";
        // line 24
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 24, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["idx"] => $context["val"]) {
            if ((($context["idx"] . "") != "labels")) {
                // line 25
                echo "<option value=\"";
                echo twigescapefilter($this->env, $context["val"], "html", null, true);
                echo "\" selected>";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 25, $this->getSourceContext()); })()), "labels", array(), "array"), $context["idx"], array(), "array"), "html", null, true);
                echo "</option>";
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['idx'], $context['val'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 27
        echo "</select>

    <div id=\"";
        // line 29
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 29, $this->getSourceContext()); })()), "html", null, true);
        echo "hiddeninputswrap\">
        ";
        // line 30
        if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 30, $this->getSourceContext()); })())) {
            // line 31
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 31, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["idx"] => $context["val"]) {
                if ((($context["idx"] . "") != "labels")) {
                    // line 32
                    echo "<input type=\"hidden\" name=\"";
                    echo twigescapefilter($this->env, (isset($context["fullname"]) || arraykeyexists("fullname", $context) ? $context["fullname"] : (function () { throw new TwigErrorRuntime('Variable "fullname" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
                    echo "[]\"";
                    if ((isset($context["disabled"]) || arraykeyexists("disabled", $context) ? $context["disabled"] : (function () { throw new TwigErrorRuntime('Variable "disabled" does not exist.', 32, $this->getSourceContext()); })())) {
                        echo " disabled=\"disabled\"";
                    }
                    echo " value=\"";
                    echo twigescapefilter($this->env, $context["val"], "html", null, true);
                    echo "\">";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['idx'], $context['val'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        } else {
            // line 35
            echo "<input type=\"hidden\" name=\"";
            echo twigescapefilter($this->env, (isset($context["fullname"]) || arraykeyexists("fullname", $context) ? $context["fullname"] : (function () { throw new TwigErrorRuntime('Variable "fullname" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
            echo "\"";
            if ((isset($context["disabled"]) || arraykeyexists("disabled", $context) ? $context["disabled"] : (function () { throw new TwigErrorRuntime('Variable "disabled" does not exist.', 35, $this->getSourceContext()); })())) {
                echo " disabled=\"disabled\"";
            }
            echo " value=\"";
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["value"] ?? null), 0, array(), "array", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["value"] ?? null), 0, array(), "array"), "")) : ("")), "html", null, true);
            echo "\">
        ";
        }
        // line 37
        echo "</div>

    <script>
        jQuery(function (\$) {
            // Select2 v3 does not used same input as v4.
            // NEXTMAJOR: Remove this BC layer while upgrading to v4.
            var usedInputRef = window.Select2 ? '#";
        // line 43
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 43, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinput' : '#";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 43, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinputv4';
            var unusedInputRef = window.Select2 ? '#";
        // line 44
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 44, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinputv4' : '#";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 44, $this->getSourceContext()); })()), "html", null, true);
        echo "autocompleteinput';

            \$(unusedInputRef).remove();
            var autocompleteInput = \$(usedInputRef);

            var select2Options = {";
        // line 50
        $context["allowClearPlaceholder"] = ((( !(isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 50, $this->getSourceContext()); })()) &&  !(isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 50, $this->getSourceContext()); })()))) ? (" ") : (""));
        // line 51
        echo "placeholder: '";
        echo twigescapefilter($this->env, (((isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 51, $this->getSourceContext()); })())) ? ((isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 51, $this->getSourceContext()); })())) : ((isset($context["allowClearPlaceholder"]) || arraykeyexists("allowClearPlaceholder", $context) ? $context["allowClearPlaceholder"] : (function () { throw new TwigErrorRuntime('Variable "allowClearPlaceholder" does not exist.', 51, $this->getSourceContext()); })()))), "html", null, true);
        echo "', // allowClear needs placeholder to work properly
                allowClear: ";
        // line 52
        echo (((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 52, $this->getSourceContext()); })())) ? ("false") : ("true"));
        echo ",
                enable: ";
        // line 53
        echo (((isset($context["disabled"]) || arraykeyexists("disabled", $context) ? $context["disabled"] : (function () { throw new TwigErrorRuntime('Variable "disabled" does not exist.', 53, $this->getSourceContext()); })())) ? ("false") : ("true"));
        echo ",
                readonly: ";
        // line 54
        echo ((((arraykeyexists("readonly", $context) && (isset($context["readonly"]) || arraykeyexists("readonly", $context) ? $context["readonly"] : (function () { throw new TwigErrorRuntime('Variable "readonly" does not exist.', 54, $this->getSourceContext()); })())) || (twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "readonly", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 54, $this->getSourceContext()); })()), "readonly", array())))) ? ("true") : ("false"));
        echo ",
                minimumInputLength: ";
        // line 55
        echo twigescapefilter($this->env, (isset($context["minimuminputlength"]) || arraykeyexists("minimuminputlength", $context) ? $context["minimuminputlength"] : (function () { throw new TwigErrorRuntime('Variable "minimuminputlength" does not exist.', 55, $this->getSourceContext()); })()), "html", null, true);
        echo ",
                multiple: ";
        // line 56
        echo (((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 56, $this->getSourceContext()); })())) ? ("true") : ("false"));
        echo ",
                width: function() {
                    // Select2 v3 and v4 BC. If window.Select2 is defined, then the v3 is installed.
                    // NEXTMAJOR: Remove Select2 v3 support.
                    return Admin.getselect2width(window.Select2 ? this.element : jQuery(this));
                },
                dropdownAutoWidth: ";
        // line 62
        echo (((isset($context["dropdownautowidth"]) || arraykeyexists("dropdownautowidth", $context) ? $context["dropdownautowidth"] : (function () { throw new TwigErrorRuntime('Variable "dropdownautowidth" does not exist.', 62, $this->getSourceContext()); })())) ? ("true") : ("false"));
        echo ",
                containerCssClass: '";
        // line 63
        echo twigescapefilter($this->env, ((isset($context["containercssclass"]) || arraykeyexists("containercssclass", $context) ? $context["containercssclass"] : (function () { throw new TwigErrorRuntime('Variable "containercssclass" does not exist.', 63, $this->getSourceContext()); })()) . " form-control"), "html", null, true);
        echo "',
                dropdownCssClass: '";
        // line 64
        echo twigescapefilter($this->env, (isset($context["dropdowncssclass"]) || arraykeyexists("dropdowncssclass", $context) ? $context["dropdowncssclass"] : (function () { throw new TwigErrorRuntime('Variable "dropdowncssclass" does not exist.', 64, $this->getSourceContext()); })()), "html", null, true);
        echo "',
                ajax: {
                    url:  '";
        // line 66
        echo twigescapefilter($this->env, (((isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 66, $this->getSourceContext()); })())) ? ((isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 66, $this->getSourceContext()); })())) : (twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["route"]) || arraykeyexists("route", $context) ? $context["route"] : (function () { throw new TwigErrorRuntime('Variable "route" does not exist.', 66, $this->getSourceContext()); })()), "name", array()), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["route"] ?? null), "parameters", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["route"] ?? null), "parameters", array()), array())) : (array()))), "js"))), "html", null, true);
        echo "',
                    dataType: 'json',
                    quietMillis: ";
        // line 68
        echo twigescapefilter($this->env, (isset($context["quietmillis"]) || arraykeyexists("quietmillis", $context) ? $context["quietmillis"] : (function () { throw new TwigErrorRuntime('Variable "quietmillis" does not exist.', 68, $this->getSourceContext()); })()), "html", null, true);
        echo ",
                    cache: ";
        // line 69
        echo (((isset($context["cache"]) || arraykeyexists("cache", $context) ? $context["cache"] : (function () { throw new TwigErrorRuntime('Variable "cache" does not exist.', 69, $this->getSourceContext()); })())) ? ("true") : ("false"));
        echo ",
                    data: function (term, page) { // page is the one-based page number tracked by Select2
                        // Select2 v4 got a \"params\" unique argument
                        // NEXTMAJOR: Remove this BC layer.
                        if (typeof page === 'undefined') {
                            page = typeof term.page !== 'undefined' ? term.page : 1;
                            term = term.term;
                        }

                        ";
        // line 78
        $this->displayBlock('sonatatypemodelautocompleteajaxrequestparameters', $context, $blocks);
        // line 118
        echo "                    },
                },
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };

            // Select2 v3/v4 special options.
            // NEXTMAJOR: Remove this BC layer while upgrading to v4.
            var templateResult = function (item) {
                return ";
        // line 126
        $this->displayBlock('sonatatypemodelautocompletedropdownitemformat', $context, $blocks);
        echo ";// format of one dropdown item
            };
            var templateSelection = function (item) {
                // Select2 v4 BC select pre-selection.
                if (typeof item.label === 'undefined') {
                    item.label = item.text;
                }
                return ";
        // line 133
        $this->displayBlock('sonatatypemodelautocompleteselectionformat', $context, $blocks);
        echo ";// format selected item '<b>'+item.label+'</b>';
            };

            if (window.Select2) {
                select2Options.initSelection = function (element, callback) {
                    callback(element.val());
                };
                select2Options.ajax.results = function (data, page) {
                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return {results: data.items, more: data.more};
                };
                select2Options.formatResult = templateResult;
                select2Options.formatSelection = templateSelection;
            } else {
                select2Options.ajax.processResults = function (data, params) {
                    return {
                        results: data.items,
                        pagination: {
                            more: data.more
                        }
                    };
                };
                select2Options.templateResult = templateResult;
                select2Options.templateSelection = templateSelection;
            }
            // END Select2 v3/v4 special options

            autocompleteInput.select2(select2Options);

            // Events structure is different between v3 and v4
            // NEXTMAJOR: Remove BC layer.
            var boundEvents = window.Select2 ? 'change' : 'select2:select select2:unselect';
            autocompleteInput.on(boundEvents, function(e) {
                if (e.type === 'select2:select') {
                    e.added = e.params.data;
                }
                if (e.type === 'select2:unselect') {
                    e.removed = e.params.data;
                }

                // console.log('change '+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));

                // remove input
                if (undefined !== e.removed && null !== e.removed) {
                    var removedItems = e.removed;

                    ";
        // line 179
        if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 179, $this->getSourceContext()); })())) {
            // line 180
            echo "                        if(!\$.isArray(removedItems)) {
                            removedItems = [removedItems];
                        }

                        var length = removedItems.length;
                        for (var i = 0; i < length; i++) {
                            el = removedItems[i];
                            \$('#";
            // line 187
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 187, $this->getSourceContext()); })()), "html", null, true);
            echo "hiddeninputswrap input:hidden[value=\"'+el.id+'\"]').remove();
                        }";
        } else {
            // line 190
            echo "\$('#";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 190, $this->getSourceContext()); })()), "html", null, true);
            echo "hiddeninputswrap input:hidden').val('');";
        }
        // line 192
        echo "                }

                // add new input
                var el = null;
                if (undefined !== e.added) {

                    var addedItems = e.added;

                    ";
        // line 200
        if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 200, $this->getSourceContext()); })())) {
            // line 201
            echo "                        if(!\$.isArray(addedItems)) {
                            addedItems = [addedItems];
                        }

                        var length = addedItems.length;
                        for (var i = 0; i < length; i++) {
                            el = addedItems[i];
                            \$('#";
            // line 208
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 208, $this->getSourceContext()); })()), "html", null, true);
            echo "hiddeninputswrap').append('<input type=\"hidden\" name=\"";
            echo twigescapefilter($this->env, (isset($context["fullname"]) || arraykeyexists("fullname", $context) ? $context["fullname"] : (function () { throw new TwigErrorRuntime('Variable "fullname" does not exist.', 208, $this->getSourceContext()); })()), "html", null, true);
            echo "[]\" value=\"'+el.id+'\" />');
                        }";
        } else {
            // line 211
            echo "\$('#";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 211, $this->getSourceContext()); })()), "html", null, true);
            echo "hiddeninputswrap input:hidden').val(addedItems.id);";
        }
        // line 213
        echo "                }
            });

            // Initialise the autocomplete
            var data = [];";
        // line 219
        if ( !twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 219, $this->getSourceContext()); })()))) {
            // line 220
            echo "data =";
            if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 220, $this->getSourceContext()); })())) {
                echo "[";
            }
            // line 221
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 221, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            foreach ($context['seq'] as $context["idx"] => $context["val"]) {
                if ((($context["idx"] . "") != "labels")) {
                    // line 222
                    if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                        echo ", ";
                    }
                    // line 223
                    echo "{id: '";
                    echo twigescapefilter($this->env, twigescapefilter($this->env, $context["val"], "js"), "html", null, true);
                    echo "', label:'";
                    echo twigescapefilter($this->env, twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 223, $this->getSourceContext()); })()), "labels", array(), "array"), $context["idx"], array(), "array"), "js"), "html", null, true);
                    echo "'}";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['idx'], $context['val'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 225
            if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 225, $this->getSourceContext()); })())) {
                echo "]";
            }
            echo ";
            ";
        }
        // line 228
        echo "// Select2 v3 data populate.
            // NEXTMAJOR: Remove while dropping v3 support.
            if (window.Select2 && (undefined==data.length || 0<data.length)) { // Leave placeholder if no data set
                autocompleteInput.select2('data', data);
            }

            // remove unneeded autocomplete text input before form submit
            \$(usedInputRef).closest('form').submit(function()
            {
                \$(usedInputRef).remove();
                return true;
            });
        });
    </script>
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal8e0fabff1975527c35ed49660b5a5ca184de8458c9c9a6dfa426288a34f9f6ac->leave($internal8e0fabff1975527c35ed49660b5a5ca184de8458c9c9a6dfa426288a34f9f6acprof);

        
        $internal6cd8f6da5925b34b44c34790ed3813b8c3f56c9c8f9709c7968efca513218916->leave($internal6cd8f6da5925b34b44c34790ed3813b8c3f56c9c8f9709c7968efca513218916prof);

    }

    // line 78
    public function blocksonatatypemodelautocompleteajaxrequestparameters($context, array $blocks = array())
    {
        $internal6a41b70886e3731c57e4bd5cf7cf3287262b2dcf7ca872acaba9e0d879340ee8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6a41b70886e3731c57e4bd5cf7cf3287262b2dcf7ca872acaba9e0d879340ee8->enter($internal6a41b70886e3731c57e4bd5cf7cf3287262b2dcf7ca872acaba9e0d879340ee8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompleteajaxrequestparameters"));

        $internalfa4204a3c160c3831a0cb1775c117a93c72b98d384ee5b370c59b27a513dd7bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfa4204a3c160c3831a0cb1775c117a93c72b98d384ee5b370c59b27a513dd7bc->enter($internalfa4204a3c160c3831a0cb1775c117a93c72b98d384ee5b370c59b27a513dd7bcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompleteajaxrequestparameters"));

        // line 79
        echo "                        return {
                                //search term
                                '";
        // line 81
        echo twigescapefilter($this->env, (isset($context["reqparamnamesearch"]) || arraykeyexists("reqparamnamesearch", $context) ? $context["reqparamnamesearch"] : (function () { throw new TwigErrorRuntime('Variable "reqparamnamesearch" does not exist.', 81, $this->getSourceContext()); })()), "html", null, true);
        echo "': term,

                                // page size
                                '";
        // line 84
        echo twigescapefilter($this->env, (isset($context["reqparamnameitemsperpage"]) || arraykeyexists("reqparamnameitemsperpage", $context) ? $context["reqparamnameitemsperpage"] : (function () { throw new TwigErrorRuntime('Variable "reqparamnameitemsperpage" does not exist.', 84, $this->getSourceContext()); })()), "html", null, true);
        echo "': ";
        echo twigescapefilter($this->env, (isset($context["itemsperpage"]) || arraykeyexists("itemsperpage", $context) ? $context["itemsperpage"] : (function () { throw new TwigErrorRuntime('Variable "itemsperpage" does not exist.', 84, $this->getSourceContext()); })()), "html", null, true);
        echo ",

                                // page number
                                '";
        // line 87
        echo twigescapefilter($this->env, (isset($context["reqparamnamepagenumber"]) || arraykeyexists("reqparamnamepagenumber", $context) ? $context["reqparamnamepagenumber"] : (function () { throw new TwigErrorRuntime('Variable "reqparamnamepagenumber" does not exist.', 87, $this->getSourceContext()); })()), "html", null, true);
        echo "': page,

                                // admin
                                ";
        // line 90
        if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 90, $this->getSourceContext()); })()), "admin", array()))) {
            // line 91
            echo "                                    'uniqid': '";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 91, $this->getSourceContext()); })()), "admin", array()), "uniqid", array()), "html", null, true);
            echo "',
                                    'admincode': '";
            // line 92
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 92, $this->getSourceContext()); })()), "admin", array()), "code", array()), "html", null, true);
            echo "',
                                ";
        } elseif (        // line 93
(isset($context["admincode"]) || arraykeyexists("admincode", $context) ? $context["admincode"] : (function () { throw new TwigErrorRuntime('Variable "admincode" does not exist.', 93, $this->getSourceContext()); })())) {
            // line 94
            echo "                                    'admincode':  '";
            echo twigescapefilter($this->env, (isset($context["admincode"]) || arraykeyexists("admincode", $context) ? $context["admincode"] : (function () { throw new TwigErrorRuntime('Variable "admincode" does not exist.', 94, $this->getSourceContext()); })()), "html", null, true);
            echo "',
                                ";
        }
        // line 96
        echo "
                                 // subclass
                                ";
        // line 98
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 98, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "subclass"), "method")) {
            // line 99
            echo "                                    'subclass': '";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 99, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "subclass"), "method"), "html", null, true);
            echo "',
                                ";
        }
        // line 101
        echo "
                                ";
        // line 102
        if (((isset($context["context"]) || arraykeyexists("context", $context) ? $context["context"] : (function () { throw new TwigErrorRuntime('Variable "context" does not exist.', 102, $this->getSourceContext()); })()) == "filter")) {
            // line 103
            echo "                                    'field':  '";
            echo twigescapefilter($this->env, twigreplacefilter((isset($context["fullname"]) || arraykeyexists("fullname", $context) ? $context["fullname"] : (function () { throw new TwigErrorRuntime('Variable "fullname" does not exist.', 103, $this->getSourceContext()); })()), array("filter[" => "", "][value]" => "", "" => ".")), "html", null, true);
            echo "',
                                    'context': 'filter'
                                ";
        } else {
            // line 106
            echo "                                    'field':  '";
            echo twigescapefilter($this->env, (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 106, $this->getSourceContext()); })()), "html", null, true);
            echo "'
                                ";
        }
        // line 108
        echo "
                                // other parameters
                                ";
        // line 110
        if ( !twigtestempty((isset($context["reqparams"]) || arraykeyexists("reqparams", $context) ? $context["reqparams"] : (function () { throw new TwigErrorRuntime('Variable "reqparams" does not exist.', 110, $this->getSourceContext()); })()))) {
            echo ",";
            // line 111
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["reqparams"]) || arraykeyexists("reqparams", $context) ? $context["reqparams"] : (function () { throw new TwigErrorRuntime('Variable "reqparams" does not exist.', 111, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["value"]) {
                // line 112
                echo "'";
                echo twigescapefilter($this->env, twigescapefilter($this->env, $context["key"], "js"), "html", null, true);
                echo "': '";
                echo twigescapefilter($this->env, twigescapefilter($this->env, $context["value"], "js"), "html", null, true);
                echo "'";
                // line 113
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['value'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        }
        // line 116
        echo "                        };
                        ";
        
        $internalfa4204a3c160c3831a0cb1775c117a93c72b98d384ee5b370c59b27a513dd7bc->leave($internalfa4204a3c160c3831a0cb1775c117a93c72b98d384ee5b370c59b27a513dd7bcprof);

        
        $internal6a41b70886e3731c57e4bd5cf7cf3287262b2dcf7ca872acaba9e0d879340ee8->leave($internal6a41b70886e3731c57e4bd5cf7cf3287262b2dcf7ca872acaba9e0d879340ee8prof);

    }

    // line 126
    public function blocksonatatypemodelautocompletedropdownitemformat($context, array $blocks = array())
    {
        $internal0707a91c48e05a92736a87d4275f42f3eec7a5e5389f7e3f90d7806a44c4b02f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0707a91c48e05a92736a87d4275f42f3eec7a5e5389f7e3f90d7806a44c4b02f->enter($internal0707a91c48e05a92736a87d4275f42f3eec7a5e5389f7e3f90d7806a44c4b02fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletedropdownitemformat"));

        $internal2f359354fa4b4a3dc2b31f8ac479b311801399ff161715e0ab39761a74c71bfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2f359354fa4b4a3dc2b31f8ac479b311801399ff161715e0ab39761a74c71bfb->enter($internal2f359354fa4b4a3dc2b31f8ac479b311801399ff161715e0ab39761a74c71bfbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletedropdownitemformat"));

        echo "'<div class=\"";
        echo twigescapefilter($this->env, (isset($context["dropdownitemcssclass"]) || arraykeyexists("dropdownitemcssclass", $context) ? $context["dropdownitemcssclass"] : (function () { throw new TwigErrorRuntime('Variable "dropdownitemcssclass" does not exist.', 126, $this->getSourceContext()); })()), "html", null, true);
        echo "\">'+item.label+'<\\/div>'";
        
        $internal2f359354fa4b4a3dc2b31f8ac479b311801399ff161715e0ab39761a74c71bfb->leave($internal2f359354fa4b4a3dc2b31f8ac479b311801399ff161715e0ab39761a74c71bfbprof);

        
        $internal0707a91c48e05a92736a87d4275f42f3eec7a5e5389f7e3f90d7806a44c4b02f->leave($internal0707a91c48e05a92736a87d4275f42f3eec7a5e5389f7e3f90d7806a44c4b02fprof);

    }

    // line 133
    public function blocksonatatypemodelautocompleteselectionformat($context, array $blocks = array())
    {
        $internala28db131cd3561526f1ed4981beaca4a633fe4d3afee00d20fc4d555ebd53f10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala28db131cd3561526f1ed4981beaca4a633fe4d3afee00d20fc4d555ebd53f10->enter($internala28db131cd3561526f1ed4981beaca4a633fe4d3afee00d20fc4d555ebd53f10prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompleteselectionformat"));

        $internal679b340593e32f9bcbc89e03fc8a831339dc1f14e300f8bb2aa11a765a1a9625 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal679b340593e32f9bcbc89e03fc8a831339dc1f14e300f8bb2aa11a765a1a9625->enter($internal679b340593e32f9bcbc89e03fc8a831339dc1f14e300f8bb2aa11a765a1a9625prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompleteselectionformat"));

        echo "item.label";
        
        $internal679b340593e32f9bcbc89e03fc8a831339dc1f14e300f8bb2aa11a765a1a9625->leave($internal679b340593e32f9bcbc89e03fc8a831339dc1f14e300f8bb2aa11a765a1a9625prof);

        
        $internala28db131cd3561526f1ed4981beaca4a633fe4d3afee00d20fc4d555ebd53f10->leave($internala28db131cd3561526f1ed4981beaca4a633fe4d3afee00d20fc4d555ebd53f10prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Form/Type:sonatatypemodelautocomplete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  577 => 133,  557 => 126,  546 => 116,  529 => 113,  523 => 112,  506 => 111,  503 => 110,  499 => 108,  493 => 106,  486 => 103,  484 => 102,  481 => 101,  475 => 99,  473 => 98,  469 => 96,  463 => 94,  461 => 93,  457 => 92,  452 => 91,  450 => 90,  444 => 87,  436 => 84,  430 => 81,  426 => 79,  417 => 78,  392 => 228,  385 => 225,  371 => 223,  367 => 222,  356 => 221,  351 => 220,  349 => 219,  343 => 213,  338 => 211,  331 => 208,  322 => 201,  320 => 200,  310 => 192,  305 => 190,  300 => 187,  291 => 180,  289 => 179,  240 => 133,  230 => 126,  220 => 118,  218 => 78,  206 => 69,  202 => 68,  197 => 66,  192 => 64,  188 => 63,  184 => 62,  175 => 56,  171 => 55,  167 => 54,  163 => 53,  159 => 52,  154 => 51,  152 => 50,  142 => 44,  136 => 43,  128 => 37,  116 => 35,  100 => 32,  95 => 31,  93 => 30,  89 => 29,  85 => 27,  74 => 25,  69 => 24,  67 => 23,  63 => 22,  59 => 21,  55 => 20,  52 => 19,  48 => 17,  44 => 16,  40 => 15,  36 => 14,  33 => 13,  30 => 12,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% spaceless %}

    <input type=\"text\" id=\"{{ id }}autocompleteinput\" value=\"\"
        {%- if readonly is defined and readonly %} readonly=\"readonly\"{% endif -%}
        {%- if disabled %} disabled=\"disabled\"{% endif -%}
        {%- if required %} required=\"required\"{% endif %}
    />

    <select id=\"{{ id }}autocompleteinputv4\" data-sonata-select2=\"false\"
        {%- if readonly is defined and readonly %} readonly=\"readonly\"{% endif -%}
        {%- if disabled %} disabled=\"disabled\"{% endif -%}
        {%- if required %} required=\"required\"{% endif %}
    >
        {%- for idx, val  in value if idx~'' != 'labels' -%}
            <option value=\"{{ val }}\" selected>{{ value['labels'][idx] }}</option>
        {%- endfor -%}
    </select>

    <div id=\"{{ id }}hiddeninputswrap\">
        {% if multiple -%}
            {%- for idx, val in value if idx~'' != 'labels' -%}
                <input type=\"hidden\" name=\"{{ fullname }}[]\" {%- if disabled %} disabled=\"disabled\"{% endif %} value=\"{{ val }}\">
            {%- endfor -%}
        {% else -%}
            <input type=\"hidden\" name=\"{{ fullname }}\" {%- if disabled %} disabled=\"disabled\"{% endif %} value=\"{{ value[0]|default('') }}\">
        {% endif -%}
    </div>

    <script>
        jQuery(function (\$) {
            // Select2 v3 does not used same input as v4.
            // NEXTMAJOR: Remove this BC layer while upgrading to v4.
            var usedInputRef = window.Select2 ? '#{{ id }}autocompleteinput' : '#{{ id }}autocompleteinputv4';
            var unusedInputRef = window.Select2 ? '#{{ id }}autocompleteinputv4' : '#{{ id }}autocompleteinput';

            \$(unusedInputRef).remove();
            var autocompleteInput = \$(usedInputRef);

            var select2Options = {
                {%- set allowClearPlaceholder = (not multiple and not required) ? ' ' : '' -%}
                placeholder: '{{ placeholder ?: allowClearPlaceholder }}', // allowClear needs placeholder to work properly
                allowClear: {{ required ? 'false' : 'true' }},
                enable: {{ disabled ? 'false' : 'true' }},
                readonly: {{ readonly is defined and readonly or attr.readonly is defined and attr.readonly ? 'true' : 'false' }},
                minimumInputLength: {{ minimuminputlength }},
                multiple: {{ multiple ? 'true' : 'false' }},
                width: function() {
                    // Select2 v3 and v4 BC. If window.Select2 is defined, then the v3 is installed.
                    // NEXTMAJOR: Remove Select2 v3 support.
                    return Admin.getselect2width(window.Select2 ? this.element : jQuery(this));
                },
                dropdownAutoWidth: {{ dropdownautowidth ? 'true' : 'false' }},
                containerCssClass: '{{ containercssclass ~ ' form-control' }}',
                dropdownCssClass: '{{ dropdowncssclass }}',
                ajax: {
                    url:  '{{ url ?: path(route.name, route.parameters|default([]))|e('js') }}',
                    dataType: 'json',
                    quietMillis: {{ quietmillis }},
                    cache: {{ cache ? 'true' : 'false' }},
                    data: function (term, page) { // page is the one-based page number tracked by Select2
                        // Select2 v4 got a \"params\" unique argument
                        // NEXTMAJOR: Remove this BC layer.
                        if (typeof page === 'undefined') {
                            page = typeof term.page !== 'undefined' ? term.page : 1;
                            term = term.term;
                        }

                        {% block sonatatypemodelautocompleteajaxrequestparameters %}
                        return {
                                //search term
                                '{{ reqparamnamesearch }}': term,

                                // page size
                                '{{ reqparamnameitemsperpage }}': {{ itemsperpage }},

                                // page number
                                '{{ reqparamnamepagenumber }}': page,

                                // admin
                                {% if sonataadmin.admin is not null %}
                                    'uniqid': '{{ sonataadmin.admin.uniqid }}',
                                    'admincode': '{{ sonataadmin.admin.code }}',
                                {% elseif admincode %}
                                    'admincode':  '{{ admincode }}',
                                {% endif %}

                                 // subclass
                                {% if app.request.query.get('subclass') %}
                                    'subclass': '{{ app.request.query.get('subclass') }}',
                                {% endif %}

                                {% if context == 'filter' %}
                                    'field':  '{{ fullname|replace({'filter[': '', '][value]': '', '':'.'}) }}',
                                    'context': 'filter'
                                {% else %}
                                    'field':  '{{ name }}'
                                {% endif %}

                                // other parameters
                                {% if reqparams is not empty %},
                                    {%- for key, value in reqparams -%}
                                        '{{- key|e('js') -}}': '{{- value|e('js') -}}'
                                        {%- if not loop.last -%}, {% endif -%}
                                    {%- endfor -%}
                                {% endif %}
                        };
                        {% endblock %}
                    },
                },
                escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
            };

            // Select2 v3/v4 special options.
            // NEXTMAJOR: Remove this BC layer while upgrading to v4.
            var templateResult = function (item) {
                return {% block sonatatypemodelautocompletedropdownitemformat %}'<div class=\"{{ dropdownitemcssclass }}\">'+item.label+'<\\/div>'{% endblock %};// format of one dropdown item
            };
            var templateSelection = function (item) {
                // Select2 v4 BC select pre-selection.
                if (typeof item.label === 'undefined') {
                    item.label = item.text;
                }
                return {% block sonatatypemodelautocompleteselectionformat %}item.label{% endblock %};// format selected item '<b>'+item.label+'</b>';
            };

            if (window.Select2) {
                select2Options.initSelection = function (element, callback) {
                    callback(element.val());
                };
                select2Options.ajax.results = function (data, page) {
                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return {results: data.items, more: data.more};
                };
                select2Options.formatResult = templateResult;
                select2Options.formatSelection = templateSelection;
            } else {
                select2Options.ajax.processResults = function (data, params) {
                    return {
                        results: data.items,
                        pagination: {
                            more: data.more
                        }
                    };
                };
                select2Options.templateResult = templateResult;
                select2Options.templateSelection = templateSelection;
            }
            // END Select2 v3/v4 special options

            autocompleteInput.select2(select2Options);

            // Events structure is different between v3 and v4
            // NEXTMAJOR: Remove BC layer.
            var boundEvents = window.Select2 ? 'change' : 'select2:select select2:unselect';
            autocompleteInput.on(boundEvents, function(e) {
                if (e.type === 'select2:select') {
                    e.added = e.params.data;
                }
                if (e.type === 'select2:unselect') {
                    e.removed = e.params.data;
                }

                // console.log('change '+JSON.stringify({val:e.val, added:e.added, removed:e.removed}));

                // remove input
                if (undefined !== e.removed && null !== e.removed) {
                    var removedItems = e.removed;

                    {% if multiple %}
                        if(!\$.isArray(removedItems)) {
                            removedItems = [removedItems];
                        }

                        var length = removedItems.length;
                        for (var i = 0; i < length; i++) {
                            el = removedItems[i];
                            \$('#{{ id }}hiddeninputswrap input:hidden[value=\"'+el.id+'\"]').remove();
                        }
                    {%- else -%}
                        \$('#{{ id }}hiddeninputswrap input:hidden').val('');
                    {%- endif %}
                }

                // add new input
                var el = null;
                if (undefined !== e.added) {

                    var addedItems = e.added;

                    {% if multiple %}
                        if(!\$.isArray(addedItems)) {
                            addedItems = [addedItems];
                        }

                        var length = addedItems.length;
                        for (var i = 0; i < length; i++) {
                            el = addedItems[i];
                            \$('#{{ id }}hiddeninputswrap').append('<input type=\"hidden\" name=\"{{ fullname }}[]\" value=\"'+el.id+'\" />');
                        }
                    {%- else -%}
                        \$('#{{ id }}hiddeninputswrap input:hidden').val(addedItems.id);
                    {%- endif %}
                }
            });

            // Initialise the autocomplete
            var data = [];

            {%- if value is not empty -%}
                data = {%- if multiple -%}[ {%- endif -%}
                {%- for idx, val  in value if idx~'' != 'labels' -%}
                    {%- if not loop.first -%}, {% endif -%}
                    {id: '{{ val|e('js') }}', label:'{{ value['labels'][idx]|e('js') }}'}
                {%- endfor -%}
                {%- if multiple -%} ] {%- endif -%};
            {% endif -%}

            // Select2 v3 data populate.
            // NEXTMAJOR: Remove while dropping v3 support.
            if (window.Select2 && (undefined==data.length || 0<data.length)) { // Leave placeholder if no data set
                autocompleteInput.select2('data', data);
            }

            // remove unneeded autocomplete text input before form submit
            \$(usedInputRef).closest('form').submit(function()
            {
                \$(usedInputRef).remove();
                return true;
            });
        });
    </script>
{% endspaceless %}
", "SonataAdminBundle:Form/Type:sonatatypemodelautocomplete.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Form/Type/sonatatypemodelautocomplete.html.twig");
    }
}
