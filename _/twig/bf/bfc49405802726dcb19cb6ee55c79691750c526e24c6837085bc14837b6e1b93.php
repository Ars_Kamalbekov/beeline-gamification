<?php

/* SonataAdminBundle:CRUD:liststring.html.twig */
class TwigTemplate92aa22795e35f9915887a6d4333ba97e1914d66a52cebe4bfb70e2fb300f80d4 extends TwigTemplate
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:liststring.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale201869c29b14376be68809b0842f9076704f18e3098bdfcd8a2ce1311a8e37f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale201869c29b14376be68809b0842f9076704f18e3098bdfcd8a2ce1311a8e37f->enter($internale201869c29b14376be68809b0842f9076704f18e3098bdfcd8a2ce1311a8e37fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:liststring.html.twig"));

        $internalb4129b555876a835bd91e6699c8c60b1e107b384afbcd371418b220e23f1ad20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb4129b555876a835bd91e6699c8c60b1e107b384afbcd371418b220e23f1ad20->enter($internalb4129b555876a835bd91e6699c8c60b1e107b384afbcd371418b220e23f1ad20prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:liststring.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internale201869c29b14376be68809b0842f9076704f18e3098bdfcd8a2ce1311a8e37f->leave($internale201869c29b14376be68809b0842f9076704f18e3098bdfcd8a2ce1311a8e37fprof);

        
        $internalb4129b555876a835bd91e6699c8c60b1e107b384afbcd371418b220e23f1ad20->leave($internalb4129b555876a835bd91e6699c8c60b1e107b384afbcd371418b220e23f1ad20prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:liststring.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}
", "SonataAdminBundle:CRUD:liststring.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/liststring.html.twig");
    }
}
