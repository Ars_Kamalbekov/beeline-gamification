<?php

/* SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig */
class TwigTemplate022541db8ba9b4f9f7c5c4b4650ce4ba1d4bc24add7bead366d22057635b915c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal5c6d2ddf2c37f27babf7a8d39180e4283a7f108942328ba5f2e04b5b5b881a30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5c6d2ddf2c37f27babf7a8d39180e4283a7f108942328ba5f2e04b5b5b881a30->enter($internal5c6d2ddf2c37f27babf7a8d39180e4283a7f108942328ba5f2e04b5b5b881a30prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig"));

        $internal8a328e43f5ec23c3398cb72a68c25aecbd64fc00c237019a314cebcac9ccfc23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8a328e43f5ec23c3398cb72a68c25aecbd64fc00c237019a314cebcac9ccfc23->enter($internal8a328e43f5ec23c3398cb72a68c25aecbd64fc00c237019a314cebcac9ccfc23prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig"));

        // line 11
        echo "<script type=\"text/javascript\">
    jQuery('div#fieldcontainer";
        // line 12
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " tbody.sonata-ba-tbody').first().sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> tr',
        stop: applypositionvalue";
        // line 16
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo "
    });

    function applypositionvalue";
        // line 19
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
        echo "() {
        // update the input value position
        jQuery('div#fieldcontainer";
        // line 21
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo " tbody.sonata-ba-tbody td.sonata-ba-td-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 21, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo "').each(function(index, element) {
            // remove the sortable handler and put it back
            jQuery('span.sonata-ba-sortable-handler', element).remove();
            jQuery(element).append('<span class=\"sonata-ba-sortable-handler ui-icon ui-icon-grip-solid-horizontal\"></span>');
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer";
        // line 28
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 28, $this->getSourceContext()); })()), "html", null, true);
        echo " tbody.sonata-ba-tbody td.sonata-ba-td-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 28, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 28, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo " input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-";
        // line 34
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 34, $this->getSourceContext()); })()), "html", null, true);
        echo "').bind('sonata.addelement', function() {
        applypositionvalue";
        // line 35
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
        echo "();
        jQuery('div#fieldcontainer";
        // line 36
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 36, $this->getSourceContext()); })()), "html", null, true);
        echo " tbody.sonata-ba-tbody').sortable('refresh');
    });

    applypositionvalue";
        // line 39
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
        echo "();
</script>
";
        
        $internal5c6d2ddf2c37f27babf7a8d39180e4283a7f108942328ba5f2e04b5b5b881a30->leave($internal5c6d2ddf2c37f27babf7a8d39180e4283a7f108942328ba5f2e04b5b5b881a30prof);

        
        $internal8a328e43f5ec23c3398cb72a68c25aecbd64fc00c237019a314cebcac9ccfc23->leave($internal8a328e43f5ec23c3398cb72a68c25aecbd64fc00c237019a314cebcac9ccfc23prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 39,  81 => 36,  77 => 35,  73 => 34,  60 => 28,  46 => 21,  41 => 19,  35 => 16,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<script type=\"text/javascript\">
    jQuery('div#fieldcontainer{{ id }} tbody.sonata-ba-tbody').first().sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> tr',
        stop: applypositionvalue{{ id }}
    });

    function applypositionvalue{{ id }}() {
        // update the input value position
        jQuery('div#fieldcontainer{{ id }} tbody.sonata-ba-tbody td.sonata-ba-td-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }}').each(function(index, element) {
            // remove the sortable handler and put it back
            jQuery('span.sonata-ba-sortable-handler', element).remove();
            jQuery(element).append('<span class=\"sonata-ba-sortable-handler ui-icon ui-icon-grip-solid-horizontal\"></span>');
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer{{ id }} tbody.sonata-ba-tbody td.sonata-ba-td-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }} input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-{{ id }}').bind('sonata.addelement', function() {
        applypositionvalue{{ id }}();
        jQuery('div#fieldcontainer{{ id }} tbody.sonata-ba-tbody').sortable('refresh');
    });

    applypositionvalue{{ id }}();
</script>
", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editormonetomanysortablescripttable.html.twig");
    }
}
