<?php

/* @Framework/Form/choicewidgetexpanded.html.php */
class TwigTemplated2f03c9432a9a485739ef22c9a7a3e5a1cf9993b03fb5a970f2b40d3c5f1f0de extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalae08e3202e1277c5fc4cc453547371e44d24eea8683663d03912682aa102c517 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalae08e3202e1277c5fc4cc453547371e44d24eea8683663d03912682aa102c517->enter($internalae08e3202e1277c5fc4cc453547371e44d24eea8683663d03912682aa102c517prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetexpanded.html.php"));

        $internal15473b54db34c24c6d4ae7d57bdeb4407b66516c038d5273da7641ef6d97929c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal15473b54db34c24c6d4ae7d57bdeb4407b66516c038d5273da7641ef6d97929c->enter($internal15473b54db34c24c6d4ae7d57bdeb4407b66516c038d5273da7641ef6d97929cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetexpanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translationdomain' => \$choicetranslationdomain)) ?>
<?php endforeach ?>
</div>
";
        
        $internalae08e3202e1277c5fc4cc453547371e44d24eea8683663d03912682aa102c517->leave($internalae08e3202e1277c5fc4cc453547371e44d24eea8683663d03912682aa102c517prof);

        
        $internal15473b54db34c24c6d4ae7d57bdeb4407b66516c038d5273da7641ef6d97929c->leave($internal15473b54db34c24c6d4ae7d57bdeb4407b66516c038d5273da7641ef6d97929cprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choicewidgetexpanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translationdomain' => \$choicetranslationdomain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choicewidgetexpanded.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choicewidgetexpanded.html.php");
    }
}
