<?php

/* SonataAdminBundle:CRUD:listactiondelete.html.twig */
class TwigTemplateb8c0867997870f63e12114536f14ba66e28a804efed56123595de00366bd616c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfd63ccbbd6b6ba5a291d1c4ad96892b531117328ee833d06846ace878f70631f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfd63ccbbd6b6ba5a291d1c4ad96892b531117328ee833d06846ace878f70631f->enter($internalfd63ccbbd6b6ba5a291d1c4ad96892b531117328ee833d06846ace878f70631fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactiondelete.html.twig"));

        $internal302427bcbe146c8a818cfc003cfe4ee4f6cb652c9f59cb72c7fd11278a463340 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal302427bcbe146c8a818cfc003cfe4ee4f6cb652c9f59cb72c7fd11278a463340->enter($internal302427bcbe146c8a818cfc003cfe4ee4f6cb652c9f59cb72c7fd11278a463340prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactiondelete.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasAccess", array(0 => "delete", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "delete"), "method"))) {
            // line 13
            echo "    <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 13, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default deletelink\" title=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actiondelete", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        ";
            // line 15
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actiondelete", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    </a>
";
        }
        
        $internalfd63ccbbd6b6ba5a291d1c4ad96892b531117328ee833d06846ace878f70631f->leave($internalfd63ccbbd6b6ba5a291d1c4ad96892b531117328ee833d06846ace878f70631fprof);

        
        $internal302427bcbe146c8a818cfc003cfe4ee4f6cb652c9f59cb72c7fd11278a463340->leave($internal302427bcbe146c8a818cfc003cfe4ee4f6cb652c9f59cb72c7fd11278a463340prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listactiondelete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 15,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.hasAccess('delete', object) and admin.hasRoute('delete') %}
    <a href=\"{{ admin.generateObjectUrl('delete', object) }}\" class=\"btn btn-sm btn-default deletelink\" title=\"{{ 'actiondelete'|trans({}, 'SonataAdminBundle') }}\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        {{ 'actiondelete'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endif %}
", "SonataAdminBundle:CRUD:listactiondelete.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listactiondelete.html.twig");
    }
}
