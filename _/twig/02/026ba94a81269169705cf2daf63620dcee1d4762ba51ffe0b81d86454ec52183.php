<?php

/* @Framework/Form/widgetattributes.html.php */
class TwigTemplate5cbb379c0bc2fe4d9d012b201d699384e87ee7540d39af4e5358678fc0c24374 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldcf3e4d58cf8edceeb4af5290516c3fd7d1555313f04801ce47dde7f11997e5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldcf3e4d58cf8edceeb4af5290516c3fd7d1555313f04801ce47dde7f11997e5c->enter($internaldcf3e4d58cf8edceeb4af5290516c3fd7d1555313f04801ce47dde7f11997e5cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/widgetattributes.html.php"));

        $internal4feb0828f5db809abff827edf574d78dc0faa7481b140ae1cf9f883f78fa14a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4feb0828f5db809abff827edf574d78dc0faa7481b140ae1cf9f883f78fa14a3->enter($internal4feb0828f5db809abff827edf574d78dc0faa7481b140ae1cf9f883f78fa14a3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/widgetattributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$fullname) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $internaldcf3e4d58cf8edceeb4af5290516c3fd7d1555313f04801ce47dde7f11997e5c->leave($internaldcf3e4d58cf8edceeb4af5290516c3fd7d1555313f04801ce47dde7f11997e5cprof);

        
        $internal4feb0828f5db809abff827edf574d78dc0faa7481b140ae1cf9f883f78fa14a3->leave($internal4feb0828f5db809abff827edf574d78dc0faa7481b140ae1cf9f883f78fa14a3prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widgetattributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$fullname) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widgetattributes.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widgetattributes.html.php");
    }
}
