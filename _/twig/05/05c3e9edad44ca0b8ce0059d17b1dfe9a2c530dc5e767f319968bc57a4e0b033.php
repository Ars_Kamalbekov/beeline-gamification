<?php

/* @Framework/Form/urlwidget.html.php */
class TwigTemplate32cf5ad9b72f1ddf972093048152648dd591e038e75a8e9f23773fcfa32e481d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7beb470724ababd378bbc28f6a8f1b78c159f18a6a56d41e42d8b1bb55994346 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7beb470724ababd378bbc28f6a8f1b78c159f18a6a56d41e42d8b1bb55994346->enter($internal7beb470724ababd378bbc28f6a8f1b78c159f18a6a56d41e42d8b1bb55994346prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/urlwidget.html.php"));

        $internalc363d723ec3a474fc6acdedd9152f55e3c34bfeac6618076cf6181927f5ade55 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc363d723ec3a474fc6acdedd9152f55e3c34bfeac6618076cf6181927f5ade55->enter($internalc363d723ec3a474fc6acdedd9152f55e3c34bfeac6618076cf6181927f5ade55prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/urlwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $internal7beb470724ababd378bbc28f6a8f1b78c159f18a6a56d41e42d8b1bb55994346->leave($internal7beb470724ababd378bbc28f6a8f1b78c159f18a6a56d41e42d8b1bb55994346prof);

        
        $internalc363d723ec3a474fc6acdedd9152f55e3c34bfeac6618076cf6181927f5ade55->leave($internalc363d723ec3a474fc6acdedd9152f55e3c34bfeac6618076cf6181927f5ade55prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/urlwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/urlwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/urlwidget.html.php");
    }
}
