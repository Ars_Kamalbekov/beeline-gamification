<?php

/* SonataAdminBundle:Block:blockadminlist.html.twig */
class TwigTemplate3f7757d12fc2b512a902b756de52bb9c81baa7c9258f79fc1b6a150180c50eb8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataAdminBundle:Block:blockadminlist.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal87b89635839e2ef8fd78ec5f7ecb4ac2d7745142a37ea3468360b5a5977bb365 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal87b89635839e2ef8fd78ec5f7ecb4ac2d7745142a37ea3468360b5a5977bb365->enter($internal87b89635839e2ef8fd78ec5f7ecb4ac2d7745142a37ea3468360b5a5977bb365prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockadminlist.html.twig"));

        $internalfd369e517b35071a437374e500f11ace957e84b4369eca693bcaad76907a1de9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfd369e517b35071a437374e500f11ace957e84b4369eca693bcaad76907a1de9->enter($internalfd369e517b35071a437374e500f11ace957e84b4369eca693bcaad76907a1de9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockadminlist.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal87b89635839e2ef8fd78ec5f7ecb4ac2d7745142a37ea3468360b5a5977bb365->leave($internal87b89635839e2ef8fd78ec5f7ecb4ac2d7745142a37ea3468360b5a5977bb365prof);

        
        $internalfd369e517b35071a437374e500f11ace957e84b4369eca693bcaad76907a1de9->leave($internalfd369e517b35071a437374e500f11ace957e84b4369eca693bcaad76907a1de9prof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal0b08abcb89b068be15fb66441058e4baa1b4691786c2594339a1eca6c9546182 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0b08abcb89b068be15fb66441058e4baa1b4691786c2594339a1eca6c9546182->enter($internal0b08abcb89b068be15fb66441058e4baa1b4691786c2594339a1eca6c9546182prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal528baafe555e62f36e925d62eb91e5662b424dede0c8f5d82cbf127017ed1d6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal528baafe555e62f36e925d62eb91e5662b424dede0c8f5d82cbf127017ed1d6b->enter($internal528baafe555e62f36e925d62eb91e5662b424dede0c8f5d82cbf127017ed1d6bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 15, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["group"]) {
            // line 16
            echo "        ";
            $context["display"] = (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array())) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLESUPERADMIN"));
            // line 17
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array()));
            foreach ($context['seq'] as $context["key"] => $context["role"]) {
                if ( !(isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 17, $this->getSourceContext()); })())) {
                    // line 18
                    echo "            ";
                    $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                    // line 19
                    echo "        ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['role'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 20
            echo "
        ";
            // line 21
            if ((isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 21, $this->getSourceContext()); })())) {
                // line 22
                echo "            <div class=\"box\">
                <div class=\"box-header\">
                    <h3 class=\"box-title\">";
                // line 24
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "labelcatalogue", array())), "html", null, true);
                echo "</h3>
                </div>
                <div class=\"box-body\">
                    <table class=\"table table-hover\">
                        <tbody>
                            ";
                // line 29
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "items", array()));
                $context['loop'] = array(
                  'parent' => $context['parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                    $length = count($context['seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['seq'] as $context["key"] => $context["admin"]) {
                    // line 30
                    echo "                                ";
                    if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "dashboardActions", array())) > 0)) {
                        // line 31
                        echo "                                            <tr>
                                                <td class=\"sonata-ba-list-label\" width=\"40%\">
                                                    ";
                        // line 33
                        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "translationdomain", array())), "html", null, true);
                        echo "
                                                </td>
                                                <td>
                                                    <div class=\"btn-group\">
                                                        ";
                        // line 37
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "dashboardActions", array()));
                        $context['loop'] = array(
                          'parent' => $context['parent'],
                          'index0' => 0,
                          'index'  => 1,
                          'first'  => true,
                        );
                        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                            $length = count($context['seq']);
                            $context['loop']['revindex0'] = $length - 1;
                            $context['loop']['revindex'] = $length;
                            $context['loop']['length'] = $length;
                            $context['loop']['last'] = 1 === $length;
                        }
                        foreach ($context['seq'] as $context["key"] => $context["action"]) {
                            // line 38
                            echo "                                                            ";
                            $this->loadTemplate(((twiggetattribute($this->env, $this->getSourceContext(), $context["action"], "template", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["action"], "template", array()), "SonataAdminBundle:CRUD:dashboardaction.html.twig")) : ("SonataAdminBundle:CRUD:dashboardaction.html.twig")), "SonataAdminBundle:Block:blockadminlist.html.twig", 38)->display(arraymerge($context, array("action" => $context["action"])));
                            // line 39
                            echo "                                                        ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                            if (isset($context['loop']['length'])) {
                                --$context['loop']['revindex0'];
                                --$context['loop']['revindex'];
                                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                            }
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['key'], $context['action'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 40
                        echo "                                                    </div>
                                                </td>
                                            </tr>
                                ";
                    }
                    // line 44
                    echo "                            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['admin'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 45
                echo "                        </tbody>
                    </table>
                </div>
            </div>
        ";
            }
            // line 50
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['group'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internal528baafe555e62f36e925d62eb91e5662b424dede0c8f5d82cbf127017ed1d6b->leave($internal528baafe555e62f36e925d62eb91e5662b424dede0c8f5d82cbf127017ed1d6bprof);

        
        $internal0b08abcb89b068be15fb66441058e4baa1b4691786c2594339a1eca6c9546182->leave($internal0b08abcb89b068be15fb66441058e4baa1b4691786c2594339a1eca6c9546182prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Block:blockadminlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 50,  187 => 45,  173 => 44,  167 => 40,  153 => 39,  150 => 38,  133 => 37,  126 => 33,  122 => 31,  119 => 30,  102 => 29,  94 => 24,  90 => 22,  88 => 21,  85 => 20,  78 => 19,  75 => 18,  69 => 17,  66 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    {% for group in groups %}
        {% set display = (group.roles is empty or isgranted('ROLESUPERADMIN') ) %}
        {% for role in group.roles if not display %}
            {% set display = isgranted(role)%}
        {% endfor %}

        {% if display %}
            <div class=\"box\">
                <div class=\"box-header\">
                    <h3 class=\"box-title\">{{ group.label|trans({}, group.labelcatalogue) }}</h3>
                </div>
                <div class=\"box-body\">
                    <table class=\"table table-hover\">
                        <tbody>
                            {% for admin in group.items %}
                                {% if admin.dashboardActions|length > 0 %}
                                            <tr>
                                                <td class=\"sonata-ba-list-label\" width=\"40%\">
                                                    {{ admin.label|trans({}, admin.translationdomain) }}
                                                </td>
                                                <td>
                                                    <div class=\"btn-group\">
                                                        {% for action in admin.dashboardActions %}
                                                            {% include action.template|default('SonataAdminBundle:CRUD:dashboardaction.html.twig') with {'action': action} %}
                                                        {% endfor %}
                                                    </div>
                                                </td>
                                            </tr>
                                {% endif %}
                            {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        {% endif %}
    {% endfor %}
{% endblock %}
", "SonataAdminBundle:Block:blockadminlist.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Block/blockadminlist.html.twig");
    }
}
