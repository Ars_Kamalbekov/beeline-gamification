<?php

/* @WebProfiler/Icon/router.svg */
class TwigTemplate22f5843b9cafa329c7e4bc1209e66218f436e27ed4aaed245b83430904a15ae4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb42b4fdca6356d58b28a4115df355be6010d76f37cd6ccbc56e1d775655cc301 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb42b4fdca6356d58b28a4115df355be6010d76f37cd6ccbc56e1d775655cc301->enter($internalb42b4fdca6356d58b28a4115df355be6010d76f37cd6ccbc56e1d775655cc301prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/router.svg"));

        $internal02090b23b4d79306137069574b9e92a836b838695d6a1478362729021e2c4445 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal02090b23b4d79306137069574b9e92a836b838695d6a1478362729021e2c4445->enter($internal02090b23b4d79306137069574b9e92a836b838695d6a1478362729021e2c4445prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/router.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M13,3v18c0,1.1-0.9,2-2,2s-2-0.9-2-2V3c0-1.1,0.9-2,2-2S13,1.9,13,3z M23.2,4.6l-1.8-1.4
    C21.2,2.9,20.8,3,20.4,3h-1.3H14v2.1V8h5.1h1.3c0.4,0,0.8-0.3,1.1-0.5l1.8-1.6C23.6,5.6,23.6,4.9,23.2,4.6z M19.5,9.4
    C19.2,9.1,18.8,9,18.4,9h-0.3H14v2.6V14h4.1h0.3c0.4,0,0.8-0.1,1.1-0.3l1.8-1.5c0.4-0.3,0.4-0.9,0-1.3L19.5,9.4z M3.5,7
    C3.1,7,2.8,7,2.5,7.3L0.7,8.8c-0.4,0.3-0.4,0.9,0,1.3l1.8,1.6C2.8,11.9,3.1,12,3.5,12h0.3H8V9.4V7H3.9H3.5z\"/>
</svg>
";
        
        $internalb42b4fdca6356d58b28a4115df355be6010d76f37cd6ccbc56e1d775655cc301->leave($internalb42b4fdca6356d58b28a4115df355be6010d76f37cd6ccbc56e1d775655cc301prof);

        
        $internal02090b23b4d79306137069574b9e92a836b838695d6a1478362729021e2c4445->leave($internal02090b23b4d79306137069574b9e92a836b838695d6a1478362729021e2c4445prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/router.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M13,3v18c0,1.1-0.9,2-2,2s-2-0.9-2-2V3c0-1.1,0.9-2,2-2S13,1.9,13,3z M23.2,4.6l-1.8-1.4
    C21.2,2.9,20.8,3,20.4,3h-1.3H14v2.1V8h5.1h1.3c0.4,0,0.8-0.3,1.1-0.5l1.8-1.6C23.6,5.6,23.6,4.9,23.2,4.6z M19.5,9.4
    C19.2,9.1,18.8,9,18.4,9h-0.3H14v2.6V14h4.1h0.3c0.4,0,0.8-0.1,1.1-0.3l1.8-1.5c0.4-0.3,0.4-0.9,0-1.3L19.5,9.4z M3.5,7
    C3.1,7,2.8,7,2.5,7.3L0.7,8.8c-0.4,0.3-0.4,0.9,0,1.3l1.8,1.6C2.8,11.9,3.1,12,3.5,12h0.3H8V9.4V7H3.9H3.5z\"/>
</svg>
", "@WebProfiler/Icon/router.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/router.svg");
    }
}
