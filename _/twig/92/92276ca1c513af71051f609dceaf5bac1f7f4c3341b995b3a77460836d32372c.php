<?php

/* SonataAdminBundle:CRUD:listouterrowslist.html.twig */
class TwigTemplate51c3899c826ac3486d4fe05fc42eccece4dec7f432318429ae70a0a754333781 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb5372b4f93538c6fe138ef3e2e5ffa169e05e1a4c5b126b1d0efd844f3b0833c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb5372b4f93538c6fe138ef3e2e5ffa169e05e1a4c5b126b1d0efd844f3b0833c->enter($internalb5372b4f93538c6fe138ef3e2e5ffa169e05e1a4c5b126b1d0efd844f3b0833cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listouterrowslist.html.twig"));

        $internal13873f2b29f2dcac417fbc915157d3888f3e84ad9e3150c8a3e00cda06e140bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal13873f2b29f2dcac417fbc915157d3888f3e84ad9e3150c8a3e00cda06e140bd->enter($internal13873f2b29f2dcac417fbc915157d3888f3e84ad9e3150c8a3e00cda06e140bdprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listouterrowslist.html.twig"));

        // line 11
        echo "
";
        // line 12
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "datagrid", array()), "results", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["object"]) {
            // line 13
            echo "    <tr>
        ";
            // line 14
            $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "getTemplate", array(0 => "innerlistrow"), "method"), "SonataAdminBundle:CRUD:listouterrowslist.html.twig", 14)->display($context);
            // line 15
            echo "    </tr>
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['object'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internalb5372b4f93538c6fe138ef3e2e5ffa169e05e1a4c5b126b1d0efd844f3b0833c->leave($internalb5372b4f93538c6fe138ef3e2e5ffa169e05e1a4c5b126b1d0efd844f3b0833cprof);

        
        $internal13873f2b29f2dcac417fbc915157d3888f3e84ad9e3150c8a3e00cda06e140bd->leave($internal13873f2b29f2dcac417fbc915157d3888f3e84ad9e3150c8a3e00cda06e140bdprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listouterrowslist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 15,  48 => 14,  45 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% for object in admin.datagrid.results %}
    <tr>
        {% include admin.getTemplate('innerlistrow') %}
    </tr>
{% endfor %}
", "SonataAdminBundle:CRUD:listouterrowslist.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listouterrowslist.html.twig");
    }
}
