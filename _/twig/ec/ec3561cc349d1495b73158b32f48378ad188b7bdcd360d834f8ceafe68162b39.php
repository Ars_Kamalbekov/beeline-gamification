<?php

/* TwigBundle:Exception:logs.html.twig */
class TwigTemplate8fcced15d21878ef88ace3e06b930378a6f81df6aaa0d97c9267ec974d025b1e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7699ee5770d2f7e04e730bbc438e40944b4ff31bc9d1dbc8d8d0980a27899f58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7699ee5770d2f7e04e730bbc438e40944b4ff31bc9d1dbc8d8d0980a27899f58->enter($internal7699ee5770d2f7e04e730bbc438e40944b4ff31bc9d1dbc8d8d0980a27899f58prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:logs.html.twig"));

        $internal615d4efeaf303ff7bdcf33a13463aaf06b51d985a53265919754aa75f166029a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal615d4efeaf303ff7bdcf33a13463aaf06b51d985a53265919754aa75f166029a->enter($internal615d4efeaf303ff7bdcf33a13463aaf06b51d985a53265919754aa75f166029aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:logs.html.twig"));

        // line 1
        $context["channelisdefined"] = twiggetattribute($this->env, $this->getSourceContext(), twigfirst($this->env, (isset($context["logs"]) || arraykeyexists("logs", $context) ? $context["logs"] : (function () { throw new TwigErrorRuntime('Variable "logs" does not exist.', 1, $this->getSourceContext()); })())), "channel", array(), "any", true, true);
        // line 2
        echo "
<table class=\"logs\">
    <thead>
        <tr>
            <th>Level</th>
            ";
        // line 7
        if ((isset($context["channelisdefined"]) || arraykeyexists("channelisdefined", $context) ? $context["channelisdefined"] : (function () { throw new TwigErrorRuntime('Variable "channelisdefined" does not exist.', 7, $this->getSourceContext()); })())) {
            echo "<th>Channel</th>";
        }
        // line 8
        echo "            <th class=\"full-width\">Message</th>
        </tr>
    </thead>

    <tbody>
    ";
        // line 13
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["logs"]) || arraykeyexists("logs", $context) ? $context["logs"] : (function () { throw new TwigErrorRuntime('Variable "logs" does not exist.', 13, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["log"]) {
            // line 14
            echo "        <tr class=\"status-";
            echo (((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "priority", array()) >= 400)) ? ("error") : ((((twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "priority", array()) >= 300)) ? ("warning") : ("normal"))));
            echo "\">
            <td class=\"text-small\" nowrap>
                <span class=\"colored text-bold\">";
            // line 16
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "priorityName", array()), "html", null, true);
            echo "</span>
                <span class=\"text-muted newline\">";
            // line 17
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "timestamp", array()), "H:i:s"), "html", null, true);
            echo "</span>
            </td>
            ";
            // line 19
            if ((isset($context["channelisdefined"]) || arraykeyexists("channelisdefined", $context) ? $context["channelisdefined"] : (function () { throw new TwigErrorRuntime('Variable "channelisdefined" does not exist.', 19, $this->getSourceContext()); })())) {
                // line 20
                echo "                <td class=\"text-small text-bold nowrap\">
                    ";
                // line 21
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "channel", array()), "html", null, true);
                echo "
                </td>
            ";
            }
            // line 24
            echo "            <td>";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatLogMessage(twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "message", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["log"], "context", array()));
            echo "</td>
        </tr>
    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['log'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 27
        echo "    </tbody>
</table>
";
        
        $internal7699ee5770d2f7e04e730bbc438e40944b4ff31bc9d1dbc8d8d0980a27899f58->leave($internal7699ee5770d2f7e04e730bbc438e40944b4ff31bc9d1dbc8d8d0980a27899f58prof);

        
        $internal615d4efeaf303ff7bdcf33a13463aaf06b51d985a53265919754aa75f166029a->leave($internal615d4efeaf303ff7bdcf33a13463aaf06b51d985a53265919754aa75f166029aprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:logs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 27,  75 => 24,  69 => 21,  66 => 20,  64 => 19,  59 => 17,  55 => 16,  49 => 14,  45 => 13,  38 => 8,  34 => 7,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% set channelisdefined = (logs|first).channel is defined %}

<table class=\"logs\">
    <thead>
        <tr>
            <th>Level</th>
            {% if channelisdefined %}<th>Channel</th>{% endif %}
            <th class=\"full-width\">Message</th>
        </tr>
    </thead>

    <tbody>
    {% for log in logs %}
        <tr class=\"status-{{ log.priority >= 400 ? 'error' : log.priority >= 300 ? 'warning' : 'normal' }}\">
            <td class=\"text-small\" nowrap>
                <span class=\"colored text-bold\">{{ log.priorityName }}</span>
                <span class=\"text-muted newline\">{{ log.timestamp|date('H:i:s') }}</span>
            </td>
            {% if channelisdefined %}
                <td class=\"text-small text-bold nowrap\">
                    {{ log.channel }}
                </td>
            {% endif %}
            <td>{{ log.message|formatlogmessage(log.context) }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>
", "TwigBundle:Exception:logs.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/logs.html.twig");
    }
}
