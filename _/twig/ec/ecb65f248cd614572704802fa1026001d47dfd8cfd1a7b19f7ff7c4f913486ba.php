<?php

/* SonataAdminBundle:CRUD/Association:editonetomanysortablescripttabs.html.twig */
class TwigTemplate9657e51d0476fe345a0569cd65e8a63c8e535aa7be589dfd011eb7fe9e005097 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf1c21865213b64f623798d469b605a249494429c8f2b46b5aa70a9ab839748e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf1c21865213b64f623798d469b605a249494429c8f2b46b5aa70a9ab839748e1->enter($internalf1c21865213b64f623798d469b605a249494429c8f2b46b5aa70a9ab839748e1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanysortablescripttabs.html.twig"));

        $internal49527b02098f7fa5fd3c61b57078a8eb520cd7aa02277cd387eeb29bcf2aeedb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal49527b02098f7fa5fd3c61b57078a8eb520cd7aa02277cd387eeb29bcf2aeedb->enter($internal49527b02098f7fa5fd3c61b57078a8eb520cd7aa02277cd387eeb29bcf2aeedbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanysortablescripttabs.html.twig"));

        // line 11
        echo "<script type=\"text/javascript\">
    jQuery('div#fieldcontainer";
        // line 12
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs').sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> div',
        stop: applypositionvalue";
        // line 16
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo "
    });

    function applypositionvalue";
        // line 19
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
        echo "() {
        // update the input value position
        jQuery('div#fieldcontainer";
        // line 21
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs .sonata-ba-field-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 21, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo "').each(function(index, element) {
            // remove the sortable handler and put it back
            var parent = jQuery(element).closest('.nav-tabs-custom');
            var tabs = parent.find('> .nav-tabs');
            jQuery('.sonata-ba-sortable-handler', tabs).remove();
            \$('<li class=\"sonata-ba-sortable-handler pull-right\"></li>')
                    .prepend('<span class=\"ui-icon ui-icon-grip-solid-horizontal\"></span>')
                    .appendTo(tabs);
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer";
        // line 32
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs .sonata-ba-field-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 32, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo " input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-";
        // line 38
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 38, $this->getSourceContext()); })()), "html", null, true);
        echo "').bind('sonata.addelement', function() {
        applypositionvalue";
        // line 39
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
        echo "();
        jQuery('div#fieldcontainer";
        // line 40
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs').sortable('refresh');
    });

    applypositionvalue";
        // line 43
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 43, $this->getSourceContext()); })()), "html", null, true);
        echo "();
</script>
";
        
        $internalf1c21865213b64f623798d469b605a249494429c8f2b46b5aa70a9ab839748e1->leave($internalf1c21865213b64f623798d469b605a249494429c8f2b46b5aa70a9ab839748e1prof);

        
        $internal49527b02098f7fa5fd3c61b57078a8eb520cd7aa02277cd387eeb29bcf2aeedb->leave($internal49527b02098f7fa5fd3c61b57078a8eb520cd7aa02277cd387eeb29bcf2aeedbprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editonetomanysortablescripttabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 43,  85 => 40,  81 => 39,  77 => 38,  64 => 32,  46 => 21,  41 => 19,  35 => 16,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<script type=\"text/javascript\">
    jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs').sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> div',
        stop: applypositionvalue{{ id }}
    });

    function applypositionvalue{{ id }}() {
        // update the input value position
        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs .sonata-ba-field-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }}').each(function(index, element) {
            // remove the sortable handler and put it back
            var parent = jQuery(element).closest('.nav-tabs-custom');
            var tabs = parent.find('> .nav-tabs');
            jQuery('.sonata-ba-sortable-handler', tabs).remove();
            \$('<li class=\"sonata-ba-sortable-handler pull-right\"></li>')
                    .prepend('<span class=\"ui-icon ui-icon-grip-solid-horizontal\"></span>')
                    .appendTo(tabs);
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs .sonata-ba-field-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }} input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-{{ id }}').bind('sonata.addelement', function() {
        applypositionvalue{{ id }}();
        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs').sortable('refresh');
    });

    applypositionvalue{{ id }}();
</script>
", "SonataAdminBundle:CRUD/Association:editonetomanysortablescripttabs.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editonetomanysortablescripttabs.html.twig");
    }
}
