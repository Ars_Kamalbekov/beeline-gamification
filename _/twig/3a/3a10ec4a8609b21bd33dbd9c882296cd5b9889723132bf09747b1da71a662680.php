<?php

/* @Framework/Form/integerwidget.html.php */
class TwigTemplatecee590426c7f2ff582d19ac0fef8384e1b6564323e8a8d7dcf27178d4fe5197c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal16ab8cf98bfe60aaa1a11b8c102556f8e6906a6a65002062d03a9e208812b299 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal16ab8cf98bfe60aaa1a11b8c102556f8e6906a6a65002062d03a9e208812b299->enter($internal16ab8cf98bfe60aaa1a11b8c102556f8e6906a6a65002062d03a9e208812b299prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/integerwidget.html.php"));

        $internal12468e1dc34aeffa55699818475f9b6ffb69bc67dd55f007683ce71901647f76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal12468e1dc34aeffa55699818475f9b6ffb69bc67dd55f007683ce71901647f76->enter($internal12468e1dc34aeffa55699818475f9b6ffb69bc67dd55f007683ce71901647f76prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/integerwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $internal16ab8cf98bfe60aaa1a11b8c102556f8e6906a6a65002062d03a9e208812b299->leave($internal16ab8cf98bfe60aaa1a11b8c102556f8e6906a6a65002062d03a9e208812b299prof);

        
        $internal12468e1dc34aeffa55699818475f9b6ffb69bc67dd55f007683ce71901647f76->leave($internal12468e1dc34aeffa55699818475f9b6ffb69bc67dd55f007683ce71901647f76prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integerwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integerwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integerwidget.html.php");
    }
}
