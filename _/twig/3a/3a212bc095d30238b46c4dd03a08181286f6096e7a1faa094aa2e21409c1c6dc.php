<?php

/* SonataAdminBundle:Pager:baseresults.html.twig */
class TwigTemplate51bb115d4189f67dc423986eb4224d387c998bde43d28d66624a726e6780ffb0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'numpages' => array($this, 'blocknumpages'),
            'numresults' => array($this, 'blocknumresults'),
            'maxperpage' => array($this, 'blockmaxperpage'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldeb77c86560b1e73f0dc457e73c306d73e43c9ac804adb55dcd9aa0852213731 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldeb77c86560b1e73f0dc457e73c306d73e43c9ac804adb55dcd9aa0852213731->enter($internaldeb77c86560b1e73f0dc457e73c306d73e43c9ac804adb55dcd9aa0852213731prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:baseresults.html.twig"));

        $internal18402ae147a0092d92809d2e4bbd590fe5d825254889daccdfb6920e3501dadc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal18402ae147a0092d92809d2e4bbd590fe5d825254889daccdfb6920e3501dadc->enter($internal18402ae147a0092d92809d2e4bbd590fe5d825254889daccdfb6920e3501dadcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:baseresults.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('numpages', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('numresults', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('maxperpage', $context, $blocks);
        
        $internaldeb77c86560b1e73f0dc457e73c306d73e43c9ac804adb55dcd9aa0852213731->leave($internaldeb77c86560b1e73f0dc457e73c306d73e43c9ac804adb55dcd9aa0852213731prof);

        
        $internal18402ae147a0092d92809d2e4bbd590fe5d825254889daccdfb6920e3501dadc->leave($internal18402ae147a0092d92809d2e4bbd590fe5d825254889daccdfb6920e3501dadcprof);

    }

    // line 12
    public function blocknumpages($context, array $blocks = array())
    {
        $internal6be2b4c36e433bf2e92e2d04d7db43630d9e9406e3694f53d15432e53368bb88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6be2b4c36e433bf2e92e2d04d7db43630d9e9406e3694f53d15432e53368bb88->enter($internal6be2b4c36e433bf2e92e2d04d7db43630d9e9406e3694f53d15432e53368bb88prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numpages"));

        $internal8405bc808fb46a73fd25972fb1d43d6d7f6ba57ae7907eb52f314381bc4a3e2e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8405bc808fb46a73fd25972fb1d43d6d7f6ba57ae7907eb52f314381bc4a3e2e->enter($internal8405bc808fb46a73fd25972fb1d43d6d7f6ba57ae7907eb52f314381bc4a3e2eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "numpages"));

        // line 13
        echo "    ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo " / ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
        echo "
    &nbsp;-&nbsp;
";
        
        $internal8405bc808fb46a73fd25972fb1d43d6d7f6ba57ae7907eb52f314381bc4a3e2e->leave($internal8405bc808fb46a73fd25972fb1d43d6d7f6ba57ae7907eb52f314381bc4a3e2eprof);

        
        $internal6be2b4c36e433bf2e92e2d04d7db43630d9e9406e3694f53d15432e53368bb88->leave($internal6be2b4c36e433bf2e92e2d04d7db43630d9e9406e3694f53d15432e53368bb88prof);

    }

    // line 17
    public function blocknumresults($context, array $blocks = array())
    {
        $internal6fc98fe8dadb9b3816658f3780e5f9c8e26e04ba092545149c78eebb01e4abba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6fc98fe8dadb9b3816658f3780e5f9c8e26e04ba092545149c78eebb01e4abba->enter($internal6fc98fe8dadb9b3816658f3780e5f9c8e26e04ba092545149c78eebb01e4abbaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "numresults"));

        $internalba3697c9c07435378f9ba5eb7a889663bb770c16837d769ef8a5aad837a90573 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalba3697c9c07435378f9ba5eb7a889663bb770c16837d769ef8a5aad837a90573->enter($internalba3697c9c07435378f9ba5eb7a889663bb770c16837d769ef8a5aad837a90573prof = new TwigProfilerProfile($this->getTemplateName(), "block", "numresults"));

        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("listresultscount", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $internalba3697c9c07435378f9ba5eb7a889663bb770c16837d769ef8a5aad837a90573->leave($internalba3697c9c07435378f9ba5eb7a889663bb770c16837d769ef8a5aad837a90573prof);

        
        $internal6fc98fe8dadb9b3816658f3780e5f9c8e26e04ba092545149c78eebb01e4abba->leave($internal6fc98fe8dadb9b3816658f3780e5f9c8e26e04ba092545149c78eebb01e4abbaprof);

    }

    // line 22
    public function blockmaxperpage($context, array $blocks = array())
    {
        $internal4c53f4597fd7da38a379669ca2a2b0b59910e6a3e633cff47d399ab1237b322e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4c53f4597fd7da38a379669ca2a2b0b59910e6a3e633cff47d399ab1237b322e->enter($internal4c53f4597fd7da38a379669ca2a2b0b59910e6a3e633cff47d399ab1237b322eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "maxperpage"));

        $internal8381e867768a5337ac6e8927d9a8a1950e0adac08ef41a1ad68b1ce1b77f3a4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8381e867768a5337ac6e8927d9a8a1950e0adac08ef41a1ad68b1ce1b77f3a4f->enter($internal8381e867768a5337ac6e8927d9a8a1950e0adac08ef41a1ad68b1ce1b77f3a4fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "maxperpage"));

        // line 23
        echo "    <label class=\"control-label\" for=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "perpage\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("labelperpage", array(), "SonataAdminBundle");
        echo "</label>
    <select class=\"per-page small form-control\" id=\"";
        // line 24
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 24, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "perpage\" style=\"width: auto\">
        ";
        // line 25
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "getperpageoptions", array()));
        foreach ($context['seq'] as $context["key"] => $context["perpage"]) {
            // line 26
            echo "            <option ";
            if (($context["perpage"] == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "maxperpage", array()))) {
                echo "selected=\"selected\"";
            }
            echo " value=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => array("filter" => twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "datagrid", array()), "values", array()), array("page" => 1, "perpage" => $context["perpage"])))), "method"), "html", null, true);
            echo "\">";
            // line 27
            echo twigescapefilter($this->env, $context["perpage"], "html", null, true);
            // line 28
            echo "</option>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['perpage'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 30
        echo "    </select>
";
        
        $internal8381e867768a5337ac6e8927d9a8a1950e0adac08ef41a1ad68b1ce1b77f3a4f->leave($internal8381e867768a5337ac6e8927d9a8a1950e0adac08ef41a1ad68b1ce1b77f3a4fprof);

        
        $internal4c53f4597fd7da38a379669ca2a2b0b59910e6a3e633cff47d399ab1237b322e->leave($internal4c53f4597fd7da38a379669ca2a2b0b59910e6a3e633cff47d399ab1237b322eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:baseresults.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  140 => 30,  133 => 28,  131 => 27,  123 => 26,  119 => 25,  115 => 24,  108 => 23,  99 => 22,  88 => 19,  85 => 18,  76 => 17,  60 => 13,  51 => 12,  41 => 22,  38 => 21,  36 => 17,  33 => 16,  31 => 12,  28 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block numpages %}
    {{ admin.datagrid.pager.page }} / {{ admin.datagrid.pager.lastpage }}
    &nbsp;-&nbsp;
{% endblock %}

{% block numresults %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}listresultscount{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block maxperpage %}
    <label class=\"control-label\" for=\"{{ admin.uniqid }}perpage\">{% trans from 'SonataAdminBundle' %}labelperpage{% endtrans %}</label>
    <select class=\"per-page small form-control\" id=\"{{ admin.uniqid }}perpage\" style=\"width: auto\">
        {% for perpage in admin.getperpageoptions %}
            <option {% if perpage == admin.datagrid.pager.maxperpage %}selected=\"selected\"{% endif %} value=\"{{ admin.generateUrl('list', {'filter': admin.datagrid.values|merge({'page': 1, 'perpage': perpage})}) }}\">
                {{- perpage -}}
            </option>
        {% endfor %}
    </select>
{% endblock %}
", "SonataAdminBundle:Pager:baseresults.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Pager/baseresults.html.twig");
    }
}
