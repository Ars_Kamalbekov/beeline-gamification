<?php

/* SonataAdminBundle:CRUD/Association:listmanytomany.html.twig */
class TwigTemplate269318b26ef98de1e2d6ced2d0aece524d3696f970f43bda63d8edaf72f53785 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'relationlink' => array($this, 'blockrelationlink'),
            'relationvalue' => array($this, 'blockrelationvalue'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD/Association:listmanytomany.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal07ffdd771dde48bc0ad25578871e3dca686b9c6d1a9f92c1f416897f8a87197f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal07ffdd771dde48bc0ad25578871e3dca686b9c6d1a9f92c1f416897f8a87197f->enter($internal07ffdd771dde48bc0ad25578871e3dca686b9c6d1a9f92c1f416897f8a87197fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listmanytomany.html.twig"));

        $internalfc7e8b12a3a2fd09584deb050e24560755ff26d278e3cd1f200ca8cf95fecd4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfc7e8b12a3a2fd09584deb050e24560755ff26d278e3cd1f200ca8cf95fecd4d->enter($internalfc7e8b12a3a2fd09584deb050e24560755ff26d278e3cd1f200ca8cf95fecd4dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listmanytomany.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal07ffdd771dde48bc0ad25578871e3dca686b9c6d1a9f92c1f416897f8a87197f->leave($internal07ffdd771dde48bc0ad25578871e3dca686b9c6d1a9f92c1f416897f8a87197fprof);

        
        $internalfc7e8b12a3a2fd09584deb050e24560755ff26d278e3cd1f200ca8cf95fecd4d->leave($internalfc7e8b12a3a2fd09584deb050e24560755ff26d278e3cd1f200ca8cf95fecd4dprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal93eea60ef699cc01947600e8f260e7b2986ebd3188ddb63c5ee386f4c637828a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal93eea60ef699cc01947600e8f260e7b2986ebd3188ddb63c5ee386f4c637828a->enter($internal93eea60ef699cc01947600e8f260e7b2986ebd3188ddb63c5ee386f4c637828aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal3ffeb1b9747dc154da2607c8053a57451c968c98cda694e64ba8095ff833f763 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3ffeb1b9747dc154da2607c8053a57451c968c98cda694e64ba8095ff833f763->enter($internal3ffeb1b9747dc154da2607c8053a57451c968c98cda694e64ba8095ff833f763prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 16, $this->getSourceContext()); })())), "method"))) {
            // line 17
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 18
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })()), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relationlink", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relationvalue", $context, $blocks);
                }
                // line 23
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 26, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relationvalue", $context, $blocks);
                echo "
            ";
                // line 28
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 29
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 30
            echo "    ";
        }
        
        $internal3ffeb1b9747dc154da2607c8053a57451c968c98cda694e64ba8095ff833f763->leave($internal3ffeb1b9747dc154da2607c8053a57451c968c98cda694e64ba8095ff833f763prof);

        
        $internal93eea60ef699cc01947600e8f260e7b2986ebd3188ddb63c5ee386f4c637828a->leave($internal93eea60ef699cc01947600e8f260e7b2986ebd3188ddb63c5ee386f4c637828aprof);

    }

    // line 33
    public function blockrelationlink($context, array $blocks = array())
    {
        $internalf906a6c018f7fda0a83d191a3db24faee57c4e7d1e8ffdc58e4d0daf5d649d15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf906a6c018f7fda0a83d191a3db24faee57c4e7d1e8ffdc58e4d0daf5d649d15->enter($internalf906a6c018f7fda0a83d191a3db24faee57c4e7d1e8ffdc58e4d0daf5d649d15prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        $internal274814a252c96c8317fc701a971b6385312d189da733d5c9abd7cc66dc6ab346 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal274814a252c96c8317fc701a971b6385312d189da733d5c9abd7cc66dc6ab346->enter($internal274814a252c96c8317fc701a971b6385312d189da733d5c9abd7cc66dc6ab346prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        // line 34
        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 34, $this->getSourceContext()); })()), 1 => (isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 34, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 35
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 35, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 35, $this->getSourceContext()); })())), "html", null, true);
        // line 36
        echo "</a>";
        
        $internal274814a252c96c8317fc701a971b6385312d189da733d5c9abd7cc66dc6ab346->leave($internal274814a252c96c8317fc701a971b6385312d189da733d5c9abd7cc66dc6ab346prof);

        
        $internalf906a6c018f7fda0a83d191a3db24faee57c4e7d1e8ffdc58e4d0daf5d649d15->leave($internalf906a6c018f7fda0a83d191a3db24faee57c4e7d1e8ffdc58e4d0daf5d649d15prof);

    }

    // line 39
    public function blockrelationvalue($context, array $blocks = array())
    {
        $internal2158a933ed9e5176b3fe52f403eb850d079d136ab6331f51218e410c111e0680 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2158a933ed9e5176b3fe52f403eb850d079d136ab6331f51218e410c111e0680->enter($internal2158a933ed9e5176b3fe52f403eb850d079d136ab6331f51218e410c111e0680prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        $internal2795282a0e2b1f6d2ad93fbfebb4654e1bfd1b264be689411bf348813dbb9597 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2795282a0e2b1f6d2ad93fbfebb4654e1bfd1b264be689411bf348813dbb9597->enter($internal2795282a0e2b1f6d2ad93fbfebb4654e1bfd1b264be689411bf348813dbb9597prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        // line 40
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 40, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 40, $this->getSourceContext()); })())), "html", null, true);
        
        $internal2795282a0e2b1f6d2ad93fbfebb4654e1bfd1b264be689411bf348813dbb9597->leave($internal2795282a0e2b1f6d2ad93fbfebb4654e1bfd1b264be689411bf348813dbb9597prof);

        
        $internal2158a933ed9e5176b3fe52f403eb850d079d136ab6331f51218e410c111e0680->leave($internal2158a933ed9e5176b3fe52f403eb850d079d136ab6331f51218e410c111e0680prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:listmanytomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 40,  180 => 39,  170 => 36,  168 => 35,  164 => 34,  155 => 33,  144 => 30,  130 => 29,  126 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasassociationadmin and fielddescription.associationadmin.hasRoute(routename) %}
        {% for element in value %}
            {%- if fielddescription.associationadmin.hasAccess(routename, element) -%}
                {{ block('relationlink') }}
            {%- else -%}
                {{ block('relationvalue') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value%}
            {{ block('relationvalue') }}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relationlink -%}
    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
        {{- element|renderrelationelement(fielddescription) -}}
    </a>
{%- endblock -%}

{%- block relationvalue -%}
    {{- element|renderrelationelement(fielddescription) -}}
{%- endblock -%}
", "SonataAdminBundle:CRUD/Association:listmanytomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/listmanytomany.html.twig");
    }
}
