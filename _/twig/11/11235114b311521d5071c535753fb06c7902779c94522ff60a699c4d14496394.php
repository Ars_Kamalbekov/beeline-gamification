<?php

/* SonataAdminBundle:CRUD:showboolean.html.twig */
class TwigTemplatece73da30adb7ac7c1d55880167433d288ee419a94703425faae068f38a95ab72 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showboolean.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald29af10cbae729b89dd2e63b8bda1ddee723ae2a1018ca0075c79fca2c03a92c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald29af10cbae729b89dd2e63b8bda1ddee723ae2a1018ca0075c79fca2c03a92c->enter($internald29af10cbae729b89dd2e63b8bda1ddee723ae2a1018ca0075c79fca2c03a92cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showboolean.html.twig"));

        $internal371961aab91e1044b2d8b7fbab089935320c237a4a30c49b4dee034754f8e104 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal371961aab91e1044b2d8b7fbab089935320c237a4a30c49b4dee034754f8e104->enter($internal371961aab91e1044b2d8b7fbab089935320c237a4a30c49b4dee034754f8e104prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showboolean.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald29af10cbae729b89dd2e63b8bda1ddee723ae2a1018ca0075c79fca2c03a92c->leave($internald29af10cbae729b89dd2e63b8bda1ddee723ae2a1018ca0075c79fca2c03a92cprof);

        
        $internal371961aab91e1044b2d8b7fbab089935320c237a4a30c49b4dee034754f8e104->leave($internal371961aab91e1044b2d8b7fbab089935320c237a4a30c49b4dee034754f8e104prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal35953fce388791ebd4c3cab60ff5e6690d804a4f4dc6a61b46dc2732234852c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal35953fce388791ebd4c3cab60ff5e6690d804a4f4dc6a61b46dc2732234852c8->enter($internal35953fce388791ebd4c3cab60ff5e6690d804a4f4dc6a61b46dc2732234852c8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internald4aa64843cf8a9c0ae93f6ef82b93a5f435c54f7fc58cc26ba15bde91d6d16a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald4aa64843cf8a9c0ae93f6ef82b93a5f435c54f7fc58cc26ba15bde91d6d16a8->enter($internald4aa64843cf8a9c0ae93f6ef82b93a5f435c54f7fc58cc26ba15bde91d6d16a8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:displayboolean.html.twig", "SonataAdminBundle:CRUD:showboolean.html.twig", 15)->display($context);
        
        $internald4aa64843cf8a9c0ae93f6ef82b93a5f435c54f7fc58cc26ba15bde91d6d16a8->leave($internald4aa64843cf8a9c0ae93f6ef82b93a5f435c54f7fc58cc26ba15bde91d6d16a8prof);

        
        $internal35953fce388791ebd4c3cab60ff5e6690d804a4f4dc6a61b46dc2732234852c8->leave($internal35953fce388791ebd4c3cab60ff5e6690d804a4f4dc6a61b46dc2732234852c8prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showboolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {%- include 'SonataAdminBundle:CRUD:displayboolean.html.twig' -%}
{% endblock %}
", "SonataAdminBundle:CRUD:showboolean.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showboolean.html.twig");
    }
}
