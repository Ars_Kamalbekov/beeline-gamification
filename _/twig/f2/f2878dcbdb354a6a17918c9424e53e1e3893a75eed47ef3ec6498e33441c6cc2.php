<?php

/* @Framework/Form/collectionwidget.html.php */
class TwigTemplate18be9f7a9978c1efc4e2cc35e5f3a89d68bc51c1bc92975f19b2486672d9f722 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal34a00ce4aecf1fc56fde4f19543d11884125283e1ad0538dd50e9fec00a67c18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal34a00ce4aecf1fc56fde4f19543d11884125283e1ad0538dd50e9fec00a67c18->enter($internal34a00ce4aecf1fc56fde4f19543d11884125283e1ad0538dd50e9fec00a67c18prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/collectionwidget.html.php"));

        $internalbf6da03c8ad5dd60f6a3778e44ae6e46b0033ece41465c766e6b8c6bb709e88d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbf6da03c8ad5dd60f6a3778e44ae6e46b0033ece41465c766e6b8c6bb709e88d->enter($internalbf6da03c8ad5dd60f6a3778e44ae6e46b0033ece41465c766e6b8c6bb709e88dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/collectionwidget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $internal34a00ce4aecf1fc56fde4f19543d11884125283e1ad0538dd50e9fec00a67c18->leave($internal34a00ce4aecf1fc56fde4f19543d11884125283e1ad0538dd50e9fec00a67c18prof);

        
        $internalbf6da03c8ad5dd60f6a3778e44ae6e46b0033ece41465c766e6b8c6bb709e88d->leave($internalbf6da03c8ad5dd60f6a3778e44ae6e46b0033ece41465c766e6b8c6bb709e88dprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collectionwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collectionwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collectionwidget.html.php");
    }
}
