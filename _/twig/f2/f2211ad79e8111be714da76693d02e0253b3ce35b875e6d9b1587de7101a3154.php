<?php

/* SonataBlockBundle:Profiler:block.html.twig */
class TwigTemplate09db44490cd08d7e3390bdd1336bfe5c63b127eec4ead9436658af2930dd40e5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "SonataBlockBundle:Profiler:block.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
            'table' => array($this, 'blocktable'),
            'tablev2' => array($this, 'blocktablev2'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal15fce62987e2aa7d89107a85c91dca6c68d9d9ba4cf8278e3fa90dbf53f539ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal15fce62987e2aa7d89107a85c91dca6c68d9d9ba4cf8278e3fa90dbf53f539ef->enter($internal15fce62987e2aa7d89107a85c91dca6c68d9d9ba4cf8278e3fa90dbf53f539efprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Profiler:block.html.twig"));

        $internalbb99b0d9408176290188fae1e5acb273a77e110413dbbe8fc5e9099bfa386ac4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbb99b0d9408176290188fae1e5acb273a77e110413dbbe8fc5e9099bfa386ac4->enter($internalbb99b0d9408176290188fae1e5acb273a77e110413dbbe8fc5e9099bfa386ac4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Profiler:block.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal15fce62987e2aa7d89107a85c91dca6c68d9d9ba4cf8278e3fa90dbf53f539ef->leave($internal15fce62987e2aa7d89107a85c91dca6c68d9d9ba4cf8278e3fa90dbf53f539efprof);

        
        $internalbb99b0d9408176290188fae1e5acb273a77e110413dbbe8fc5e9099bfa386ac4->leave($internalbb99b0d9408176290188fae1e5acb273a77e110413dbbe8fc5e9099bfa386ac4prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internale516b3267d1dd48541dc13007dfc1b55a17f0e324c85b6732006cfb611347087 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale516b3267d1dd48541dc13007dfc1b55a17f0e324c85b6732006cfb611347087->enter($internale516b3267d1dd48541dc13007dfc1b55a17f0e324c85b6732006cfb611347087prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalc2be14ebcdab8ae544a402940d00aa5d42e2aba95fdb46aa6eb994a5e8ef1ea0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc2be14ebcdab8ae544a402940d00aa5d42e2aba95fdb46aa6eb994a5e8ef1ea0->enter($internalc2be14ebcdab8ae544a402940d00aa5d42e2aba95fdb46aa6eb994a5e8ef1ea0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        $context["profilermarkupversion"] = ((arraykeyexists("profilermarkupversion", $context)) ? (twigdefaultfilter((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 4, $this->getSourceContext()); })()), 1)) : (1));
        // line 5
        echo "
    <div class=\"sf-toolbar-block\">
        ";
        // line 7
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 7, $this->getSourceContext()); })()) == 1)) {
            // line 8
            echo "            <div class=\"sf-toolbar-icon\">
                <a href=\"";
            // line 9
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 9, $this->getSourceContext()); })()), "panel" => (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 9, $this->getSourceContext()); })()))), "html", null, true);
            echo "\">
                    ";
            // line 10
            echo "<span style=\"width:0px; height: 28px; vertical-align: middle;\"></span>
                    <span class=\"sf-toolbar-status\">";
            // line 11
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 11, $this->getSourceContext()); })()), "getTotalBlock", array(), "method"), "html", null, true);
            echo "</span> blocks
                </a>
            </div>
        ";
        } else {
            // line 15
            echo "            <a href=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 15, $this->getSourceContext()); })()), "panel" => (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 15, $this->getSourceContext()); })()))), "html", null, true);
            echo "\">
                <div class=\"sf-toolbar-icon\">
                    ";
            // line 17
            echo twiginclude($this->env, $context, "@SonataBlock/Profiler/icon.svg");
            echo "
                    <span class=\"sf-toolbar-value\">";
            // line 18
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "getTotalBlock", array(), "method"), "html", null, true);
            echo "</span>
                </div>
            </a>
        ";
        }
        // line 22
        echo "
        <div class=\"sf-toolbar-info\">
            <div class=\"sf-toolbar-info-piece\">
                <b>Real Blocks</b>
                <span>";
        // line 26
        echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 26, $this->getSourceContext()); })()), "realBlocks", array())), "html", null, true);
        echo "</span>
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Containers</b>
                <span>";
        // line 30
        echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 30, $this->getSourceContext()); })()), "containers", array())), "html", null, true);
        echo "</span>
            </div>
            ";
        // line 32
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 32, $this->getSourceContext()); })()) == 1)) {
            // line 33
            echo "                ";
            // line 35
            echo "                <div class=\"sf-toolbar-info-piece\">
                    <b>Total Blocks</b>
                    <span>";
            // line 37
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 37, $this->getSourceContext()); })()), "getTotalBlock", array(), "method"), "html", null, true);
            echo "</span>
                </div>
            ";
        }
        // line 40
        echo "            <div class=\"sf-toolbar-info-piece\">
                ";
        // line 41
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 41, $this->getSourceContext()); })()) == 1)) {
            echo "<hr />";
        }
        // line 42
        echo "                <b>Events</b>
                <span>";
        // line 43
        echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 43, $this->getSourceContext()); })()), "events", array())), "html", null, true);
        echo "</span>
            </div>
        </div>
    </div>
";
        
        $internalc2be14ebcdab8ae544a402940d00aa5d42e2aba95fdb46aa6eb994a5e8ef1ea0->leave($internalc2be14ebcdab8ae544a402940d00aa5d42e2aba95fdb46aa6eb994a5e8ef1ea0prof);

        
        $internale516b3267d1dd48541dc13007dfc1b55a17f0e324c85b6732006cfb611347087->leave($internale516b3267d1dd48541dc13007dfc1b55a17f0e324c85b6732006cfb611347087prof);

    }

    // line 49
    public function blockmenu($context, array $blocks = array())
    {
        $internal204db166f9fc7638bbf29b497080820bce02f634f191a8d5ea5180b30e238d0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal204db166f9fc7638bbf29b497080820bce02f634f191a8d5ea5180b30e238d0a->enter($internal204db166f9fc7638bbf29b497080820bce02f634f191a8d5ea5180b30e238d0aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internal329b31619e627c5a5ef62cc54912e51a7de08c04e68723515d9f067be05eb640 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal329b31619e627c5a5ef62cc54912e51a7de08c04e68723515d9f067be05eb640->enter($internal329b31619e627c5a5ef62cc54912e51a7de08c04e68723515d9f067be05eb640prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 50
        echo "    <span class=\"label\">
        <span class=\"icon\">
            ";
        // line 52
        echo twiginclude($this->env, $context, "@SonataBlock/Profiler/icon.svg");
        echo "
        </span>
        <strong>Blocks";
        // line 54
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 54, $this->getSourceContext()); })()), "events", array())) > 0)) {
            echo "<strong>*</strong>";
        }
        echo "</strong>
        <span class=\"count\">
            <span>";
        // line 56
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 56, $this->getSourceContext()); })()), "getTotalBlock", array(), "method"), "html", null, true);
        echo "</span>
        </span>
    </span>
";
        
        $internal329b31619e627c5a5ef62cc54912e51a7de08c04e68723515d9f067be05eb640->leave($internal329b31619e627c5a5ef62cc54912e51a7de08c04e68723515d9f067be05eb640prof);

        
        $internal204db166f9fc7638bbf29b497080820bce02f634f191a8d5ea5180b30e238d0a->leave($internal204db166f9fc7638bbf29b497080820bce02f634f191a8d5ea5180b30e238d0aprof);

    }

    // line 61
    public function blockpanel($context, array $blocks = array())
    {
        $internalbddb8c059c99cf2dda4aa711811111263bc318d9928d08bf44c84ccd65327b64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbddb8c059c99cf2dda4aa711811111263bc318d9928d08bf44c84ccd65327b64->enter($internalbddb8c059c99cf2dda4aa711811111263bc318d9928d08bf44c84ccd65327b64prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal1e575b7356cbc166fffcb1942e7ac96482b31aff82762376b596849663fc8243 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1e575b7356cbc166fffcb1942e7ac96482b31aff82762376b596849663fc8243->enter($internal1e575b7356cbc166fffcb1942e7ac96482b31aff82762376b596849663fc8243prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 62
        echo "    ";
        // line 63
        echo "    ";
        $context["profilermarkupversion"] = ((arraykeyexists("profilermarkupversion", $context)) ? (twigdefaultfilter((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 63, $this->getSourceContext()); })()), 1)) : (1));
        // line 64
        echo "
    <h2>Events Blocks</h2>
    <table>
        <tr>
            <th>code name</th>
            <th>listener tag</th>
            <th>Block types</th>
            <th>Listeners</th>
        </tr>

        ";
        // line 74
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 74, $this->getSourceContext()); })()), "events", array()));
        foreach ($context['seq'] as $context["key"] => $context["event"]) {
            // line 75
            echo "            <tr>
                <td>";
            // line 76
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "templatecode", array(), "array"), "html", null, true);
            echo "</td>
                <td>";
            // line 77
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "eventname", array(), "array"), "html", null, true);
            echo "</td>
                <td>
                    ";
            // line 79
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "blocks", array(), "array"));
            $context['iterated'] = false;
            foreach ($context['seq'] as $context["key"] => $context["type"]) {
                // line 80
                echo "                        ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["type"], 1, array()), "html", null, true);
                echo " (id:";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["type"], 0, array()), "html", null, true);
                echo ")
                    ";
                $context['iterated'] = true;
            }
            if (!$context['iterated']) {
                // line 82
                echo "                        no block returned
                    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['type'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 84
            echo "                </td>
                <td>
                    ";
            // line 86
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["event"], "listeners", array(), "array"));
            $context['iterated'] = false;
            foreach ($context['seq'] as $context["key"] => $context["listener"]) {
                // line 87
                echo "                        ";
                echo twigescapefilter($this->env, $context["listener"], "html", null, true);
                echo "
                    ";
                $context['iterated'] = true;
            }
            if (!$context['iterated']) {
                // line 89
                echo "                        no listener registered
                    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['listener'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 91
            echo "                </td>
            </tr>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['event'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 94
        echo "    </table>

    <h2>Real Blocks</h2>
    ";
        // line 97
        $context["blocks"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 97, $this->getSourceContext()); })()), "realBlocks", array());
        // line 98
        echo "    ";
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 98, $this->getSourceContext()); })()) == 1)) {
            // line 99
            echo "        ";
            $this->displayBlock("table", $context, $blocks);
            echo "
    ";
        } else {
            // line 101
            echo "        <div class=\"tab-content\">
            ";
            // line 102
            $this->displayBlock("tablev2", $context, $blocks);
            echo "
        </div>
    ";
        }
        // line 105
        echo "
    <h2>Containers Blocks</h2>
    ";
        // line 107
        $context["blocks"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 107, $this->getSourceContext()); })()), "containers", array());
        // line 108
        echo "    ";
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 108, $this->getSourceContext()); })()) == 1)) {
            // line 109
            echo "        ";
            $this->displayBlock("table", $context, $blocks);
            echo "
    ";
        } else {
            // line 111
            echo "        <div class=\"tab-content\">
            ";
            // line 112
            $this->displayBlock("tablev2", $context, $blocks);
            echo "
        </div>
    ";
        }
        // line 115
        echo "
";
        
        $internal1e575b7356cbc166fffcb1942e7ac96482b31aff82762376b596849663fc8243->leave($internal1e575b7356cbc166fffcb1942e7ac96482b31aff82762376b596849663fc8243prof);

        
        $internalbddb8c059c99cf2dda4aa711811111263bc318d9928d08bf44c84ccd65327b64->leave($internalbddb8c059c99cf2dda4aa711811111263bc318d9928d08bf44c84ccd65327b64prof);

    }

    // line 118
    public function blocktable($context, array $blocks = array())
    {
        $internal5c372b24dea8f89d565171ea2da163222732f4c58c2c1443d48d69f320ab547b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5c372b24dea8f89d565171ea2da163222732f4c58c2c1443d48d69f320ab547b->enter($internal5c372b24dea8f89d565171ea2da163222732f4c58c2c1443d48d69f320ab547bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "table"));

        $internal5c96d218fb8b9cf47e38c377e37df586bee9149c3bf44a911922fe32966f96be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5c96d218fb8b9cf47e38c377e37df586bee9149c3bf44a911922fe32966f96be->enter($internal5c96d218fb8b9cf47e38c377e37df586bee9149c3bf44a911922fe32966f96beprof = new TwigProfilerProfile($this->getTemplateName(), "block", "table"));

        // line 119
        echo "    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Type</th>
            <th>Mem. (diff)</th>
            <th>Mem. (peak)</th>
            <th>Duration</th>
        </tr>
        ";
        // line 128
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 128, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["id"] => $context["block"]) {
            // line 129
            echo "
            ";
            // line 130
            $context["rowspan"] = 1;
            // line 131
            echo "            ";
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "handler", array())) {
                // line 132
                echo "                ";
                $context["rowspan"] = ((isset($context["rowspan"]) || arraykeyexists("rowspan", $context) ? $context["rowspan"] : (function () { throw new TwigErrorRuntime('Variable "rowspan" does not exist.', 132, $this->getSourceContext()); })()) + 1);
                // line 133
                echo "            ";
            }
            // line 134
            echo "
            ";
            // line 135
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "js", array())) > 0) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "css", array())) > 0))) {
                // line 136
                echo "                ";
                $context["rowspan"] = ((isset($context["rowspan"]) || arraykeyexists("rowspan", $context) ? $context["rowspan"] : (function () { throw new TwigErrorRuntime('Variable "rowspan" does not exist.', 136, $this->getSourceContext()); })()) + 1);
                // line 137
                echo "            ";
            }
            // line 138
            echo "            <tr>
                <th style=\"vertical-align: top\" rowspan=\"";
            // line 139
            echo twigescapefilter($this->env, (isset($context["rowspan"]) || arraykeyexists("rowspan", $context) ? $context["rowspan"] : (function () { throw new TwigErrorRuntime('Variable "rowspan" does not exist.', 139, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, $context["id"], "html", null, true);
            echo "</th>
                <td>";
            // line 140
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 141
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 142
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memoryend", array()) - twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memorystart", array())) / 1000), 0), "html", null, true);
            echo " Kb</td>
                <td>";
            // line 143
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, (twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memorypeak", array()) / 1000), 0), "html", null, true);
            echo " Kb</td>
                <td>";
            // line 144
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "duration", array()), 2), "html", null, true);
            echo " ms</td>
            </tr>

            ";
            // line 147
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "handler", array())) {
                // line 148
                echo "                <tr style=\"vertical-align: top\">
                    <td colspan=\"3\">
                        Cache Keys: <pre>";
                // line 150
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "keys", array())), "html", null, true);
                echo "</pre> <br />
                        Contextual Keys: <pre>";
                // line 151
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "contextualkeys", array())), "html", null, true);
                echo "</pre>
                    </td>
                    <td colspan=\"2\">
                        TTL: ";
                // line 154
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "ttl", array()), "html", null, true);
                echo "s. <br />
                        Lifetime: ";
                // line 155
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "lifetime", array()), "html", null, true);
                echo "s. <br />
                        Backend: ";
                // line 156
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "handler", array()), "html", null, true);
                echo " <br />
                        Loading from cache: ";
                // line 157
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "fromcache", array())) {
                    echo "YES";
                } else {
                    echo "NO";
                }
                echo " <br />
                    </td>
                </tr>
            ";
            }
            // line 161
            echo "
            ";
            // line 162
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "js", array())) > 0) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "css", array())) > 0))) {
                // line 163
                echo "                <tr>
                    <td colspan=\"5\">
                        Javascripts: <pre>";
                // line 165
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "js", array())), "html", null, true);
                echo "</pre><br />
                        Stylesheets: <pre>";
                // line 166
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "css", array())), "html", null, true);
                echo "</pre>
                    </td>
                </tr>
            ";
            }
            // line 170
            echo "
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['id'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 172
        echo "    </table>
";
        
        $internal5c96d218fb8b9cf47e38c377e37df586bee9149c3bf44a911922fe32966f96be->leave($internal5c96d218fb8b9cf47e38c377e37df586bee9149c3bf44a911922fe32966f96beprof);

        
        $internal5c372b24dea8f89d565171ea2da163222732f4c58c2c1443d48d69f320ab547b->leave($internal5c372b24dea8f89d565171ea2da163222732f4c58c2c1443d48d69f320ab547bprof);

    }

    // line 175
    public function blocktablev2($context, array $blocks = array())
    {
        $internal6b547e74a7fabcc1623219f087104708a495d659fd583ca577840de097d6df94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6b547e74a7fabcc1623219f087104708a495d659fd583ca577840de097d6df94->enter($internal6b547e74a7fabcc1623219f087104708a495d659fd583ca577840de097d6df94prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablev2"));

        $internale3874744dc6c09d0f0d0d841fb8bba01aa6debf4ac2e2fb7a05fec21a6a01cc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale3874744dc6c09d0f0d0d841fb8bba01aa6debf4ac2e2fb7a05fec21a6a01cc8->enter($internale3874744dc6c09d0f0d0d841fb8bba01aa6debf4ac2e2fb7a05fec21a6a01cc8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablev2"));

        // line 176
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 176, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["id"] => $context["block"]) {
            // line 177
            echo "        <table>
            <thead>
            <tr>
                <th colspan=\"2\">Block ";
            // line 180
            echo twigescapefilter($this->env, $context["id"], "html", null, true);
            echo "</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Name</th>
                <td>";
            // line 186
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "name", array()), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>Type</th>
                <td>";
            // line 190
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "html", null, true);
            echo "</td>
            </tr>
            <tr>
                <th>Mem. diff / Mem. peak / Duration</th>
                <td>";
            // line 194
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memoryend", array()) - twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memorystart", array())) / 1000), 0), "html", null, true);
            echo " Kb / ";
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, (twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "memorypeak", array()) / 1000), 0), "html", null, true);
            echo " Kb / ";
            echo twigescapefilter($this->env, twignumberformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "duration", array()), 2), "html", null, true);
            echo " ms</td>
            </tr>

            ";
            // line 197
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "handler", array())) {
                // line 198
                echo "                <tr>
                    <th>Cache backend</th>
                    <td>
                        ";
                // line 201
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "handler", array()), "html", null, true);
                echo " - Loading from cache: ";
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "fromcache", array())) {
                    echo "YES";
                } else {
                    echo "NO";
                }
                // line 202
                echo "                    </td>
                </tr>
                <tr>
                    <th>Cache TTL / Lifetime</th>
                    <td>
                        ";
                // line 207
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "ttl", array()), "html", null, true);
                echo "s. / ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "lifetime", array()), "html", null, true);
                echo "s
                    </td>
                </tr>
                <tr>
                    <th>
                        Cache Informations
                    </th>
                    <td>
                        Cache Keys: <pre>";
                // line 215
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "keys", array())), "html", null, true);
                echo "</pre> <br />
                        Contextual Keys: <pre>";
                // line 216
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "cache", array()), "contextualkeys", array())), "html", null, true);
                echo "</pre> <br />
                    </td>
                </tr>
            ";
            }
            // line 220
            echo "
            ";
            // line 221
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "js", array())) > 0) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "css", array())) > 0))) {
                // line 222
                echo "                <tr>
                    <th>Assets</th>
                    <td>
                        Javascripts: <pre>";
                // line 225
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "js", array())), "html", null, true);
                echo "</pre><br />
                        Stylesheets: <pre>";
                // line 226
                echo twigescapefilter($this->env, jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "assets", array()), "css", array())), "html", null, true);
                echo "</pre>
                    </td>
                </tr>
            ";
            }
            // line 230
            echo "            </tbody>
        </table>
    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['id'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internale3874744dc6c09d0f0d0d841fb8bba01aa6debf4ac2e2fb7a05fec21a6a01cc8->leave($internale3874744dc6c09d0f0d0d841fb8bba01aa6debf4ac2e2fb7a05fec21a6a01cc8prof);

        
        $internal6b547e74a7fabcc1623219f087104708a495d659fd583ca577840de097d6df94->leave($internal6b547e74a7fabcc1623219f087104708a495d659fd583ca577840de097d6df94prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Profiler:block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  624 => 230,  617 => 226,  613 => 225,  608 => 222,  606 => 221,  603 => 220,  596 => 216,  592 => 215,  579 => 207,  572 => 202,  564 => 201,  559 => 198,  557 => 197,  547 => 194,  540 => 190,  533 => 186,  524 => 180,  519 => 177,  514 => 176,  505 => 175,  494 => 172,  487 => 170,  480 => 166,  476 => 165,  472 => 163,  470 => 162,  467 => 161,  456 => 157,  452 => 156,  448 => 155,  444 => 154,  438 => 151,  434 => 150,  430 => 148,  428 => 147,  422 => 144,  418 => 143,  414 => 142,  410 => 141,  406 => 140,  400 => 139,  397 => 138,  394 => 137,  391 => 136,  389 => 135,  386 => 134,  383 => 133,  380 => 132,  377 => 131,  375 => 130,  372 => 129,  368 => 128,  357 => 119,  348 => 118,  337 => 115,  331 => 112,  328 => 111,  322 => 109,  319 => 108,  317 => 107,  313 => 105,  307 => 102,  304 => 101,  298 => 99,  295 => 98,  293 => 97,  288 => 94,  280 => 91,  273 => 89,  265 => 87,  260 => 86,  256 => 84,  249 => 82,  239 => 80,  234 => 79,  229 => 77,  225 => 76,  222 => 75,  218 => 74,  206 => 64,  203 => 63,  201 => 62,  192 => 61,  178 => 56,  171 => 54,  166 => 52,  162 => 50,  153 => 49,  138 => 43,  135 => 42,  131 => 41,  128 => 40,  122 => 37,  118 => 35,  116 => 33,  114 => 32,  109 => 30,  102 => 26,  96 => 22,  89 => 18,  85 => 17,  79 => 15,  72 => 11,  69 => 10,  65 => 9,  62 => 8,  60 => 7,  56 => 5,  53 => 4,  44 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% set profilermarkupversion = profilermarkupversion|default(1) %}

    <div class=\"sf-toolbar-block\">
        {% if profilermarkupversion == 1 %}
            <div class=\"sf-toolbar-icon\">
                <a href=\"{{ path('profiler', { 'token': token, 'panel': name }) }}\">
                    {# fake image span #}<span style=\"width:0px; height: 28px; vertical-align: middle;\"></span>
                    <span class=\"sf-toolbar-status\">{{ collector.getTotalBlock() }}</span> blocks
                </a>
            </div>
        {% else %}
            <a href=\"{{ path('profiler', { 'token': token, 'panel': name }) }}\">
                <div class=\"sf-toolbar-icon\">
                    {{ include('@SonataBlock/Profiler/icon.svg') }}
                    <span class=\"sf-toolbar-value\">{{ collector.getTotalBlock() }}</span>
                </div>
            </a>
        {% endif %}

        <div class=\"sf-toolbar-info\">
            <div class=\"sf-toolbar-info-piece\">
                <b>Real Blocks</b>
                <span>{{ collector.realBlocks|length }}</span>
            </div>
            <div class=\"sf-toolbar-info-piece\">
                <b>Containers</b>
                <span>{{ collector.containers|length }}</span>
            </div>
            {% if profilermarkupversion == 1 %}
                {# don't show the total number of blocks in the info in the new design,
                it's already shown in the toolbar #}
                <div class=\"sf-toolbar-info-piece\">
                    <b>Total Blocks</b>
                    <span>{{ collector.getTotalBlock() }}</span>
                </div>
            {% endif %}
            <div class=\"sf-toolbar-info-piece\">
                {% if profilermarkupversion == 1 %}<hr />{% endif %}
                <b>Events</b>
                <span>{{ collector.events|length }}</span>
            </div>
        </div>
    </div>
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">
            {{ include('@SonataBlock/Profiler/icon.svg') }}
        </span>
        <strong>Blocks{% if collector.events|length > 0 %}<strong>*</strong>{% endif %}</strong>
        <span class=\"count\">
            <span>{{ collector.getTotalBlock() }}</span>
        </span>
    </span>
{% endblock %}

{% block panel %}
    {# NEXTMAJOR : remove this when Symfony Requirement will be bumped to 2.8+ #}
    {% set profilermarkupversion = profilermarkupversion|default(1) %}

    <h2>Events Blocks</h2>
    <table>
        <tr>
            <th>code name</th>
            <th>listener tag</th>
            <th>Block types</th>
            <th>Listeners</th>
        </tr>

        {% for event in collector.events %}
            <tr>
                <td>{{ event['templatecode'] }}</td>
                <td>{{ event['eventname'] }}</td>
                <td>
                    {% for type in event['blocks'] %}
                        {{ type.1 }} (id:{{ type.0 }})
                    {% else %}
                        no block returned
                    {% endfor %}
                </td>
                <td>
                    {% for listener in event['listeners'] %}
                        {{ listener }}
                    {% else %}
                        no listener registered
                    {% endfor %}
                </td>
            </tr>
        {% endfor %}
    </table>

    <h2>Real Blocks</h2>
    {% set blocks = collector.realBlocks %}
    {% if profilermarkupversion == 1 %}
        {{ block('table') }}
    {% else %}
        <div class=\"tab-content\">
            {{ block('tablev2') }}
        </div>
    {% endif %}

    <h2>Containers Blocks</h2>
    {% set blocks = collector.containers %}
    {% if profilermarkupversion == 1 %}
        {{ block('table') }}
    {% else %}
        <div class=\"tab-content\">
            {{ block('tablev2') }}
        </div>
    {% endif %}

{% endblock %}

{% block table %}
    <table>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Type</th>
            <th>Mem. (diff)</th>
            <th>Mem. (peak)</th>
            <th>Duration</th>
        </tr>
        {% for id, block in blocks %}

            {% set rowspan = 1 %}
            {% if block.cache.handler %}
                {% set rowspan = rowspan + 1 %}
            {% endif %}

            {% if block.assets.js|length > 0 or block.assets.css|length > 0 %}
                {% set rowspan = rowspan + 1 %}
            {% endif %}
            <tr>
                <th style=\"vertical-align: top\" rowspan=\"{{ rowspan }}\">{{ id }}</th>
                <td>{{ block.name }}</td>
                <td>{{ block.type }}</td>
                <td>{{ ((block.memoryend-block.memorystart)/1000)|numberformat(0) }} Kb</td>
                <td>{{ (block.memorypeak/1000)|numberformat(0) }} Kb</td>
                <td>{{ block.duration|numberformat(2) }} ms</td>
            </tr>

            {% if block.cache.handler %}
                <tr style=\"vertical-align: top\">
                    <td colspan=\"3\">
                        Cache Keys: <pre>{{ block.cache.keys|jsonencode() }}</pre> <br />
                        Contextual Keys: <pre>{{ block.cache.contextualkeys|jsonencode() }}</pre>
                    </td>
                    <td colspan=\"2\">
                        TTL: {{ block.cache.ttl }}s. <br />
                        Lifetime: {{ block.cache.lifetime }}s. <br />
                        Backend: {{ block.cache.handler }} <br />
                        Loading from cache: {% if block.cache.fromcache %}YES{% else %}NO{% endif %} <br />
                    </td>
                </tr>
            {% endif %}

            {% if block.assets.js|length > 0 or block.assets.css|length > 0  %}
                <tr>
                    <td colspan=\"5\">
                        Javascripts: <pre>{{ block.assets.js|jsonencode() }}</pre><br />
                        Stylesheets: <pre>{{ block.assets.css|jsonencode() }}</pre>
                    </td>
                </tr>
            {% endif %}

        {% endfor %}
    </table>
{% endblock %}

{% block tablev2 %}
    {% for id, block in blocks %}
        <table>
            <thead>
            <tr>
                <th colspan=\"2\">Block {{ id }}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>Name</th>
                <td>{{ block.name }}</td>
            </tr>
            <tr>
                <th>Type</th>
                <td>{{ block.type }}</td>
            </tr>
            <tr>
                <th>Mem. diff / Mem. peak / Duration</th>
                <td>{{ ((block.memoryend-block.memorystart)/1000)|numberformat(0) }} Kb / {{ (block.memorypeak/1000)|numberformat(0) }} Kb / {{ block.duration|numberformat(2) }} ms</td>
            </tr>

            {% if block.cache.handler %}
                <tr>
                    <th>Cache backend</th>
                    <td>
                        {{ block.cache.handler }} - Loading from cache: {% if block.cache.fromcache %}YES{% else %}NO{% endif %}
                    </td>
                </tr>
                <tr>
                    <th>Cache TTL / Lifetime</th>
                    <td>
                        {{ block.cache.ttl }}s. / {{ block.cache.lifetime }}s
                    </td>
                </tr>
                <tr>
                    <th>
                        Cache Informations
                    </th>
                    <td>
                        Cache Keys: <pre>{{ block.cache.keys|jsonencode() }}</pre> <br />
                        Contextual Keys: <pre>{{ block.cache.contextualkeys|jsonencode() }}</pre> <br />
                    </td>
                </tr>
            {% endif %}

            {% if block.assets.js|length > 0 or block.assets.css|length > 0  %}
                <tr>
                    <th>Assets</th>
                    <td>
                        Javascripts: <pre>{{ block.assets.js|jsonencode() }}</pre><br />
                        Stylesheets: <pre>{{ block.assets.css|jsonencode() }}</pre>
                    </td>
                </tr>
            {% endif %}
            </tbody>
        </table>
    {% endfor %}
{% endblock %}
", "SonataBlockBundle:Profiler:block.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Profiler/block.html.twig");
    }
}
