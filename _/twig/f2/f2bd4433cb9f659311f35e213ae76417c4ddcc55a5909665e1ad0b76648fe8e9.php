<?php

/* @Twig/images/icon-minus-square.svg */
class TwigTemplatee4ab01faa4137839e7f58b6a0b51b75848ffdc35b6b1d87732d087d0a08123fe extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9ae975fe95d1bbf948549edcbc36a34d53a192d130327813a32c7a9ecd879fd4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9ae975fe95d1bbf948549edcbc36a34d53a192d130327813a32c7a9ecd879fd4->enter($internal9ae975fe95d1bbf948549edcbc36a34d53a192d130327813a32c7a9ecd879fd4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $internal6c2882ea8e203387fbf235c4cc328fc3819a1e84c6e87500b92a7ed04304e204 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6c2882ea8e203387fbf235c4cc328fc3819a1e84c6e87500b92a7ed04304e204->enter($internal6c2882ea8e203387fbf235c4cc328fc3819a1e84c6e87500b92a7ed04304e204prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $internal9ae975fe95d1bbf948549edcbc36a34d53a192d130327813a32c7a9ecd879fd4->leave($internal9ae975fe95d1bbf948549edcbc36a34d53a192d130327813a32c7a9ecd879fd4prof);

        
        $internal6c2882ea8e203387fbf235c4cc328fc3819a1e84c6e87500b92a7ed04304e204->leave($internal6c2882ea8e203387fbf235c4cc328fc3819a1e84c6e87500b92a7ed04304e204prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
