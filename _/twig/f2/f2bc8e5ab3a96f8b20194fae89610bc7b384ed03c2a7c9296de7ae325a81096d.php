<?php

/* FOSUserBundle:Resetting:requestcontent.html.twig */
class TwigTemplate137231925bb4dce0254bf0174d9b7d84167a83a8c0a8d71d478750d0c1b4553a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8d3c7f3ea73671af42b774c158ecf8a523e82944703050bcbcd39c171bdb2415 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8d3c7f3ea73671af42b774c158ecf8a523e82944703050bcbcd39c171bdb2415->enter($internal8d3c7f3ea73671af42b774c158ecf8a523e82944703050bcbcd39c171bdb2415prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:requestcontent.html.twig"));

        $internal5920c3a2e3e71ac45f6f16933cda4b732bcddb3303c067e5a1086f4df0a79ac5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5920c3a2e3e71ac45f6f16933cda4b732bcddb3303c067e5a1086f4df0a79ac5->enter($internal5920c3a2e3e71ac45f6f16933cda4b732bcddb3303c067e5a1086f4df0a79ac5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:requestcontent.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserresettingsendemail");
        echo "\" method=\"POST\" class=\"fosuserresettingrequest\">
    <div>
        <label for=\"username\">";
        // line 5
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"";
        // line 9
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $internal8d3c7f3ea73671af42b774c158ecf8a523e82944703050bcbcd39c171bdb2415->leave($internal8d3c7f3ea73671af42b774c158ecf8a523e82944703050bcbcd39c171bdb2415prof);

        
        $internal5920c3a2e3e71ac45f6f16933cda4b732bcddb3303c067e5a1086f4df0a79ac5->leave($internal5920c3a2e3e71ac45f6f16933cda4b732bcddb3303c067e5a1086f4df0a79ac5prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:requestcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 9,  33 => 5,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

<form action=\"{{ path('fosuserresettingsendemail') }}\" method=\"POST\" class=\"fosuserresettingrequest\">
    <div>
        <label for=\"username\">{{ 'resetting.request.username'|trans }}</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"{{ 'resetting.request.submit'|trans }}\" />
    </div>
</form>
", "FOSUserBundle:Resetting:requestcontent.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/requestcontent.html.twig");
    }
}
