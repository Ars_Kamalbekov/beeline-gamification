<?php

/* SonataAdminBundle:CRUD:dashboardactioncreate.html.twig */
class TwigTemplatec2579630e6049e680c3632431134f33fd7a5e49ce5b096b9e0b2fd48778ff20e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal32898401c77c41447bb503c06a0995bdce01059891306929edbd0764db50d7ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal32898401c77c41447bb503c06a0995bdce01059891306929edbd0764db50d7ff->enter($internal32898401c77c41447bb503c06a0995bdce01059891306929edbd0764db50d7ffprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:dashboardactioncreate.html.twig"));

        $internalf4961b59f1bdf5f6a4a39ca241ee205777765631a30e90ae2e33ac1a847b8e0e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf4961b59f1bdf5f6a4a39ca241ee205777765631a30e90ae2e33ac1a847b8e0e->enter($internalf4961b59f1bdf5f6a4a39ca241ee205777765631a30e90ae2e33ac1a847b8e0eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:dashboardactioncreate.html.twig"));

        // line 1
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 1, $this->getSourceContext()); })()), "subClasses", array()))) {
            // line 2
            echo "    <a class=\"btn btn-link btn-flat\" href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 2, $this->getSourceContext()); })()), "url", array()), "html", null, true);
            echo "\">
        <i class=\"fa fa-";
            // line 3
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 3, $this->getSourceContext()); })()), "icon", array()), "html", null, true);
            echo "\" aria-hidden=\"true\"></i>
        ";
            // line 4
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 4, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
            echo "
    </a>
";
        } else {
            // line 7
            echo "    <a class=\"btn btn-link btn-flat dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
        <i class=\"fa fa-";
            // line 8
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 8, $this->getSourceContext()); })()), "icon", array()), "html", null, true);
            echo "\" aria-hidden=\"true\"></i>
        ";
            // line 9
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 9, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
            echo "
        <span class=\"caret\"></span>
    </a>
    <ul class=\"dropdown-menu\">
        ";
            // line 13
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "subclasses", array())));
            foreach ($context['seq'] as $context["key"] => $context["subclass"]) {
                // line 14
                echo "            <li>
                <a href=\"";
                // line 15
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "generateUrl", array(0 => "create", 1 => array("subclass" => $context["subclass"])), "method"), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["subclass"], array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
                echo "</a>
            </li>
        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['subclass'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 18
            echo "    </ul>
";
        }
        
        $internal32898401c77c41447bb503c06a0995bdce01059891306929edbd0764db50d7ff->leave($internal32898401c77c41447bb503c06a0995bdce01059891306929edbd0764db50d7ffprof);

        
        $internalf4961b59f1bdf5f6a4a39ca241ee205777765631a30e90ae2e33ac1a847b8e0e->leave($internalf4961b59f1bdf5f6a4a39ca241ee205777765631a30e90ae2e33ac1a847b8e0eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:dashboardactioncreate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 18,  63 => 15,  60 => 14,  56 => 13,  49 => 9,  45 => 8,  42 => 7,  36 => 4,  32 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% if admin.subClasses is empty %}
    <a class=\"btn btn-link btn-flat\" href=\"{{ action.url }}\">
        <i class=\"fa fa-{{ action.icon }}\" aria-hidden=\"true\"></i>
        {{ action.label|trans({}, action.translationdomain|default('SonataAdminBundle')) }}
    </a>
{% else %}
    <a class=\"btn btn-link btn-flat dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
        <i class=\"fa fa-{{ action.icon }}\" aria-hidden=\"true\"></i>
        {{ action.label|trans({}, action.translationdomain|default('SonataAdminBundle')) }}
        <span class=\"caret\"></span>
    </a>
    <ul class=\"dropdown-menu\">
        {% for subclass in admin.subclasses|keys %}
            <li>
                <a href=\"{{ admin.generateUrl('create', {'subclass': subclass}) }}\">{{ subclass|trans({}, action.translationdomain|default('SonataAdminBundle')) }}</a>
            </li>
        {% endfor %}
    </ul>
{% endif %}
", "SonataAdminBundle:CRUD:dashboardactioncreate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/dashboardactioncreate.html.twig");
    }
}
