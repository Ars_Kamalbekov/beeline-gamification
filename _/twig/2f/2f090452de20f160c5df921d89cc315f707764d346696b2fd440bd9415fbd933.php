<?php

/* TwigBundle:Exception:error.xml.twig */
class TwigTemplateb002aee00ef767166559c3a28b7d145b3241af20996469a5cc5aa77dd3a913c0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1e8441cc6431ad4b5cc9f9908186b529f0c54ef6026142f74e53c237a3cf966f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1e8441cc6431ad4b5cc9f9908186b529f0c54ef6026142f74e53c237a3cf966f->enter($internal1e8441cc6431ad4b5cc9f9908186b529f0c54ef6026142f74e53c237a3cf966fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $internal702828486da10e4538bd4995e4536651aaa39f891c129816ce55cc41f8988adb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal702828486da10e4538bd4995e4536651aaa39f891c129816ce55cc41f8988adb->enter($internal702828486da10e4538bd4995e4536651aaa39f891c129816ce55cc41f8988adbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twigescapefilter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" message=\"";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" />
";
        
        $internal1e8441cc6431ad4b5cc9f9908186b529f0c54ef6026142f74e53c237a3cf966f->leave($internal1e8441cc6431ad4b5cc9f9908186b529f0c54ef6026142f74e53c237a3cf966fprof);

        
        $internal702828486da10e4538bd4995e4536651aaa39f891c129816ce55cc41f8988adb->leave($internal702828486da10e4538bd4995e4536651aaa39f891c129816ce55cc41f8988adbprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?xml version=\"1.0\" encoding=\"{{ charset }}\" ?>

<error code=\"{{ statuscode }}\" message=\"{{ statustext }}\" />
", "TwigBundle:Exception:error.xml.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}
