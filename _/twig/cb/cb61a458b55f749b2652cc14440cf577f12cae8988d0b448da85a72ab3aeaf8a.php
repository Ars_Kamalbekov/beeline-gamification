<?php

/* SonataBlockBundle:Block:blockexceptiondebug.html.twig */
class TwigTemplate031553328bb8b27f6538586e7b749d7fcad8abd96887cf26045591761790c807 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockexceptiondebug.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6316e64031d0dbb0f4c7461279700786286ea7699cb396dda90c8c0ccf16e187 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6316e64031d0dbb0f4c7461279700786286ea7699cb396dda90c8c0ccf16e187->enter($internal6316e64031d0dbb0f4c7461279700786286ea7699cb396dda90c8c0ccf16e187prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockexceptiondebug.html.twig"));

        $internal8ba49a362d55c96d69578bf39712145763d5b1ea965e577b59313901076fb370 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8ba49a362d55c96d69578bf39712145763d5b1ea965e577b59313901076fb370->enter($internal8ba49a362d55c96d69578bf39712145763d5b1ea965e577b59313901076fb370prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockexceptiondebug.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal6316e64031d0dbb0f4c7461279700786286ea7699cb396dda90c8c0ccf16e187->leave($internal6316e64031d0dbb0f4c7461279700786286ea7699cb396dda90c8c0ccf16e187prof);

        
        $internal8ba49a362d55c96d69578bf39712145763d5b1ea965e577b59313901076fb370->leave($internal8ba49a362d55c96d69578bf39712145763d5b1ea965e577b59313901076fb370prof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal380f2ef86be3ab885a192f962b3047110312ecb985c8cb85d2fe56b3e23fae3d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal380f2ef86be3ab885a192f962b3047110312ecb985c8cb85d2fe56b3e23fae3d->enter($internal380f2ef86be3ab885a192f962b3047110312ecb985c8cb85d2fe56b3e23fae3dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal56c400ce908d9fad5338f66b225d56ec31f59911b285f75c1ba217bfdb79a9b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal56c400ce908d9fad5338f66b225d56ec31f59911b285f75c1ba217bfdb79a9b2->enter($internal56c400ce908d9fad5338f66b225d56ec31f59911b285f75c1ba217bfdb79a9b2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\" ";
        if ((isset($context["forceStyle"]) || arraykeyexists("forceStyle", $context) ? $context["forceStyle"] : (function () { throw new TwigErrorRuntime('Variable "forceStyle" does not exist.', 15, $this->getSourceContext()); })())) {
            echo "style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"";
        }
        echo ">

        ";
        // line 18
        echo "        ";
        if ((isset($context["forceStyle"]) || arraykeyexists("forceStyle", $context) ? $context["forceStyle"] : (function () { throw new TwigErrorRuntime('Variable "forceStyle" does not exist.', 18, $this->getSourceContext()); })())) {
            // line 19
            echo "            <link href=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exceptionlayout.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"";
            // line 20
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        ";
        }
        // line 22
        echo "        ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "SonataBlockBundle:Block:blockexceptiondebug.html.twig", 22)->display($context);
        // line 23
        echo "    </div>
";
        
        $internal56c400ce908d9fad5338f66b225d56ec31f59911b285f75c1ba217bfdb79a9b2->leave($internal56c400ce908d9fad5338f66b225d56ec31f59911b285f75c1ba217bfdb79a9b2prof);

        
        $internal380f2ef86be3ab885a192f962b3047110312ecb985c8cb85d2fe56b3e23fae3d->leave($internal380f2ef86be3ab885a192f962b3047110312ecb985c8cb85d2fe56b3e23fae3dprof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockexceptiondebug.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  69 => 22,  64 => 20,  59 => 19,  56 => 18,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    <div class=\"cms-block-exception\" {% if forceStyle %}style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"{% endif %}>

        {# this is dirty but the alternative would require a new block-optimized exception css #}
        {% if forceStyle %}
            <link href=\"{{ asset('bundles/framework/css/exceptionlayout.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"{{ asset('bundles/framework/css/exception.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        {% endif %}
        {% include 'TwigBundle:Exception:exception.html.twig' %}
    </div>
{% endblock %}
", "SonataBlockBundle:Block:blockexceptiondebug.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockexceptiondebug.html.twig");
    }
}
