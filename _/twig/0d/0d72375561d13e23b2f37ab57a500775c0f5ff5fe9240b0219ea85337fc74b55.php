<?php

/* @Framework/Form/buttonrow.html.php */
class TwigTemplate385f49f04fd1a8aa52535055c20aaf4e7faf1e87f94872fecdee70b5db489f28 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1482874275a7daf2933a72ecc1a90d622b2a74750ae9337983fcfce1a2af3e6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1482874275a7daf2933a72ecc1a90d622b2a74750ae9337983fcfce1a2af3e6b->enter($internal1482874275a7daf2933a72ecc1a90d622b2a74750ae9337983fcfce1a2af3e6bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonrow.html.php"));

        $internald6c3c2110d45434737050bee3cd4ec169aa9ab443ae2e5dc260e8b3ebea7e910 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald6c3c2110d45434737050bee3cd4ec169aa9ab443ae2e5dc260e8b3ebea7e910->enter($internald6c3c2110d45434737050bee3cd4ec169aa9ab443ae2e5dc260e8b3ebea7e910prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/buttonrow.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $internal1482874275a7daf2933a72ecc1a90d622b2a74750ae9337983fcfce1a2af3e6b->leave($internal1482874275a7daf2933a72ecc1a90d622b2a74750ae9337983fcfce1a2af3e6bprof);

        
        $internald6c3c2110d45434737050bee3cd4ec169aa9ab443ae2e5dc260e8b3ebea7e910->leave($internald6c3c2110d45434737050bee3cd4ec169aa9ab443ae2e5dc260e8b3ebea7e910prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/buttonrow.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/buttonrow.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/buttonrow.html.php");
    }
}
