<?php

/* SonataAdminBundle:CRUD:basestandardeditfield.html.twig */
class TwigTemplate52b2deec8ae7c3bbadcc37cc647ee910bc6f9a8c5b094498db1068228e44f0d8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'blocklabel'),
            'field' => array($this, 'blockfield'),
            'help' => array($this, 'blockhelp'),
            'errors' => array($this, 'blockerrors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal4ae51331c8bfd3a8a7369e53d1a8ba89641011834947a6d96dadc36c0010a801 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4ae51331c8bfd3a8a7369e53d1a8ba89641011834947a6d96dadc36c0010a801->enter($internal4ae51331c8bfd3a8a7369e53d1a8ba89641011834947a6d96dadc36c0010a801prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basestandardeditfield.html.twig"));

        $internal7881b6b4084386b87a6feb91a8665927260c25c9061d44a8c7a8be09b30e905d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7881b6b4084386b87a6feb91a8665927260c25c9061d44a8c7a8be09b30e905d->enter($internal7881b6b4084386b87a6feb91a8665927260c25c9061d44a8c7a8be09b30e905dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basestandardeditfield.html.twig"));

        // line 11
        echo "
<div class=\"form-group";
        // line 12
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 12, $this->getSourceContext()); })()), "var", array()), "errors", array())) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 12, $this->getSourceContext()); })()), "vars", array()), "id", array()), "html", null, true);
        echo "\">
    ";
        // line 13
        $this->displayBlock('label', $context, $blocks);
        // line 20
        echo "
    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-";
        // line 21
        echo twigescapefilter($this->env, (isset($context["edit"]) || arraykeyexists("edit", $context) ? $context["edit"] : (function () { throw new TwigErrorRuntime('Variable "edit" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, (isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 21, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">

        ";
        // line 23
        $this->displayBlock('field', $context, $blocks);
        // line 24
        echo "
        ";
        // line 25
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 25, $this->getSourceContext()); })()), "help", array())) {
            // line 26
            echo "            <span class=\"help-block\">";
            $this->displayBlock('help', $context, $blocks);
            echo "</span>
        ";
        }
        // line 28
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 30
        $this->displayBlock('errors', $context, $blocks);
        // line 31
        echo "        </div>

    </div>
</div>
";
        
        $internal4ae51331c8bfd3a8a7369e53d1a8ba89641011834947a6d96dadc36c0010a801->leave($internal4ae51331c8bfd3a8a7369e53d1a8ba89641011834947a6d96dadc36c0010a801prof);

        
        $internal7881b6b4084386b87a6feb91a8665927260c25c9061d44a8c7a8be09b30e905d->leave($internal7881b6b4084386b87a6feb91a8665927260c25c9061d44a8c7a8be09b30e905dprof);

    }

    // line 13
    public function blocklabel($context, array $blocks = array())
    {
        $internalb819695f1adc37549699dd7c845e86fcbedc62fc41283e2860e3e378b0eca22d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb819695f1adc37549699dd7c845e86fcbedc62fc41283e2860e3e378b0eca22d->enter($internalb819695f1adc37549699dd7c845e86fcbedc62fc41283e2860e3e378b0eca22dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internalf7c46f933bc9a91de26ff6abba6c7d531308449f39cc41f1ccb71a458014e1a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf7c46f933bc9a91de26ff6abba6c7d531308449f39cc41f1ccb71a458014e1a1->enter($internalf7c46f933bc9a91de26ff6abba6c7d531308449f39cc41f1ccb71a458014e1a1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 14
        echo "        ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 15
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 15, $this->getSourceContext()); })()), 'label', (twigtestempty($label = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "name", array())) ? array() : array("label" => $label)));
            echo "
        ";
        } else {
            // line 17
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 17, $this->getSourceContext()); })()), 'label');
            echo "
        ";
        }
        // line 19
        echo "    ";
        
        $internalf7c46f933bc9a91de26ff6abba6c7d531308449f39cc41f1ccb71a458014e1a1->leave($internalf7c46f933bc9a91de26ff6abba6c7d531308449f39cc41f1ccb71a458014e1a1prof);

        
        $internalb819695f1adc37549699dd7c845e86fcbedc62fc41283e2860e3e378b0eca22d->leave($internalb819695f1adc37549699dd7c845e86fcbedc62fc41283e2860e3e378b0eca22dprof);

    }

    // line 23
    public function blockfield($context, array $blocks = array())
    {
        $internal8c3a4f4469c0012801f704905b750a8cfc35d78204f6023cbf1fbb628492f808 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8c3a4f4469c0012801f704905b750a8cfc35d78204f6023cbf1fbb628492f808->enter($internal8c3a4f4469c0012801f704905b750a8cfc35d78204f6023cbf1fbb628492f808prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internald2f45fcfae02de86c8bcc86d5d4fb0381c74813968c08cdca88f5a39fbda0afc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald2f45fcfae02de86c8bcc86d5d4fb0381c74813968c08cdca88f5a39fbda0afc->enter($internald2f45fcfae02de86c8bcc86d5d4fb0381c74813968c08cdca88f5a39fbda0afcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 23, $this->getSourceContext()); })()), 'widget');
        
        $internald2f45fcfae02de86c8bcc86d5d4fb0381c74813968c08cdca88f5a39fbda0afc->leave($internald2f45fcfae02de86c8bcc86d5d4fb0381c74813968c08cdca88f5a39fbda0afcprof);

        
        $internal8c3a4f4469c0012801f704905b750a8cfc35d78204f6023cbf1fbb628492f808->leave($internal8c3a4f4469c0012801f704905b750a8cfc35d78204f6023cbf1fbb628492f808prof);

    }

    // line 26
    public function blockhelp($context, array $blocks = array())
    {
        $internaled63726b96d666115145cfcf1c86c9f3d1e5e6be1ffd3014c30174cb2fa225fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaled63726b96d666115145cfcf1c86c9f3d1e5e6be1ffd3014c30174cb2fa225fb->enter($internaled63726b96d666115145cfcf1c86c9f3d1e5e6be1ffd3014c30174cb2fa225fbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "help"));

        $internal5a58387efed9bc12ba54aabec2948041efae9404b9c8ce87e7bda23fe57fb71b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5a58387efed9bc12ba54aabec2948041efae9404b9c8ce87e7bda23fe57fb71b->enter($internal5a58387efed9bc12ba54aabec2948041efae9404b9c8ce87e7bda23fe57fb71bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "help"));

        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 26, $this->getSourceContext()); })()), "help", array());
        
        $internal5a58387efed9bc12ba54aabec2948041efae9404b9c8ce87e7bda23fe57fb71b->leave($internal5a58387efed9bc12ba54aabec2948041efae9404b9c8ce87e7bda23fe57fb71bprof);

        
        $internaled63726b96d666115145cfcf1c86c9f3d1e5e6be1ffd3014c30174cb2fa225fb->leave($internaled63726b96d666115145cfcf1c86c9f3d1e5e6be1ffd3014c30174cb2fa225fbprof);

    }

    // line 30
    public function blockerrors($context, array $blocks = array())
    {
        $internal15d4849c4f732a2ec542e9d266b2397991c33e3a545e3596c8dae1f1bd861dff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal15d4849c4f732a2ec542e9d266b2397991c33e3a545e3596c8dae1f1bd861dff->enter($internal15d4849c4f732a2ec542e9d266b2397991c33e3a545e3596c8dae1f1bd861dffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        $internal75a56688364172c1b3d70676aa995fcadf4c20700ace40e6d82a35bf0c944b11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal75a56688364172c1b3d70676aa995fcadf4c20700ace40e6d82a35bf0c944b11->enter($internal75a56688364172c1b3d70676aa995fcadf4c20700ace40e6d82a35bf0c944b11prof = new TwigProfilerProfile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 30, $this->getSourceContext()); })()), 'errors');
        
        $internal75a56688364172c1b3d70676aa995fcadf4c20700ace40e6d82a35bf0c944b11->leave($internal75a56688364172c1b3d70676aa995fcadf4c20700ace40e6d82a35bf0c944b11prof);

        
        $internal15d4849c4f732a2ec542e9d266b2397991c33e3a545e3596c8dae1f1bd861dff->leave($internal15d4849c4f732a2ec542e9d266b2397991c33e3a545e3596c8dae1f1bd861dffprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:basestandardeditfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 30,  141 => 26,  123 => 23,  113 => 19,  107 => 17,  101 => 15,  98 => 14,  89 => 13,  75 => 31,  73 => 30,  69 => 28,  63 => 26,  61 => 25,  58 => 24,  56 => 23,  45 => 21,  42 => 20,  40 => 13,  32 => 12,  29 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div class=\"form-group{% if fieldelement.var.errors|length > 0%} has-error{%endif%}\" id=\"sonata-ba-field-container-{{ fieldelement.vars.id }}\">
    {% block label %}
        {% if fielddescription.options.name is defined %}
            {{ formlabel(fieldelement, fielddescription.options.name) }}
        {% else %}
            {{ formlabel(fieldelement) }}
        {% endif %}
    {% endblock %}

    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if fieldelement.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">

        {% block field %}{{ formwidget(fieldelement) }}{% endblock %}

        {% if fielddescription.help %}
            <span class=\"help-block\">{% block help %}{{ fielddescription.help|raw }}{% endblock %}</span>
        {% endif %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ formerrors(fieldelement) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:basestandardeditfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/basestandardeditfield.html.twig");
    }
}
