<?php

/* SonataAdminBundle:Helper:renderformdismissableerrors.html.twig */
class TwigTemplate4f23031b082a10c9c6e24c43f6b947960ca2a36e9ee405789eff7351f6ff0bf6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0658fcc794a2724a935361ea2e4fd0462efeb187602967f7d30c49894b3df6d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0658fcc794a2724a935361ea2e4fd0462efeb187602967f7d30c49894b3df6d2->enter($internal0658fcc794a2724a935361ea2e4fd0462efeb187602967f7d30c49894b3df6d2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig"));

        $internal6446900a6c4f866109171fd00495cbd3b7714ec4efffaef0c315aff77f5f8a3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6446900a6c4f866109171fd00495cbd3b7714ec4efffaef0c315aff77f5f8a3f->enter($internal6446900a6c4f866109171fd00495cbd3b7714ec4efffaef0c315aff77f5f8a3fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig"));

        // line 1
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 1, $this->getSourceContext()); })()), "vars", array()), "errors", array()));
        foreach ($context['seq'] as $context["key"] => $context["error"]) {
            // line 2
            echo "    <div class=\"alert alert-danger alert-dismissable\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
        ";
            // line 4
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "message", array()), "html", null, true);
            echo "
    </div>
";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['error'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internal0658fcc794a2724a935361ea2e4fd0462efeb187602967f7d30c49894b3df6d2->leave($internal0658fcc794a2724a935361ea2e4fd0462efeb187602967f7d30c49894b3df6d2prof);

        
        $internal6446900a6c4f866109171fd00495cbd3b7714ec4efffaef0c315aff77f5f8a3f->leave($internal6446900a6c4f866109171fd00495cbd3b7714ec4efffaef0c315aff77f5f8a3fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  29 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% for error in form.vars.errors %}
    <div class=\"alert alert-danger alert-dismissable\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
        {{ error.message }}
    </div>
{% endfor %}
", "SonataAdminBundle:Helper:renderformdismissableerrors.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Helper/renderformdismissableerrors.html.twig");
    }
}
