<?php

/* SonataAdminBundle:CRUD/Association:editonetomanyinlinetabs.html.twig */
class TwigTemplatedba5c8671775b68034eb18b2c7de3ebc47e231c96924399dc62c776583aad411 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7883afa381135a7419e195442c60385e6643bf1655180e9b5684c2dc8081b572 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7883afa381135a7419e195442c60385e6643bf1655180e9b5684c2dc8081b572->enter($internal7883afa381135a7419e195442c60385e6643bf1655180e9b5684c2dc8081b572prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetabs.html.twig"));

        $internale932d1c44c617877069ba6883258007926406d9a9da4bcb424325b48f015a4ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale932d1c44c617877069ba6883258007926406d9a9da4bcb424325b48f015a4ac->enter($internale932d1c44c617877069ba6883258007926406d9a9da4bcb424325b48f015a4acprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetabs.html.twig"));

        // line 11
        echo "<div class=\"sonata-ba-tabs\">
    ";
        // line 12
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), "children", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["nestedgroupfield"]) {
            // line 13
            echo "        <div>
            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\">
                    ";
            // line 16
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationAdmin"]) || arraykeyexists("associationAdmin", $context) ? $context["associationAdmin"] : (function () { throw new TwigErrorRuntime('Variable "associationAdmin" does not exist.', 16, $this->getSourceContext()); })()), "formgroups", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["formgroup"]) {
                // line 17
                echo "                        <li class=\"";
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                    echo "active";
                }
                echo "\">
                            <a
                                href=\"#";
                // line 19
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationAdmin"]) || arraykeyexists("associationAdmin", $context) ? $context["associationAdmin"] : (function () { throw new TwigErrorRuntime('Variable "associationAdmin" does not exist.', 19, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "parent", array()), "loop", array()), "index", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\"
                                data-toggle=\"tab\"
                            >
                                <i class=\"icon-exclamation-sign has-errors hide\"></i>
                                ";
                // line 23
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationAdmin"]) || arraykeyexists("associationAdmin", $context) ? $context["associationAdmin"] : (function () { throw new TwigErrorRuntime('Variable "associationAdmin" does not exist.', 23, $this->getSourceContext()); })()), "trans", array(0 => $context["name"], 1 => array(), 2 => twiggetattribute($this->env, $this->getSourceContext(), $context["formgroup"], "translationdomain", array())), "method"), "html", null, true);
                echo "
                            </a>
                        </li>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['formgroup'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 27
            echo "                </ul>

                <div class=\"tab-content\">
                    ";
            // line 30
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationAdmin"]) || arraykeyexists("associationAdmin", $context) ? $context["associationAdmin"] : (function () { throw new TwigErrorRuntime('Variable "associationAdmin" does not exist.', 30, $this->getSourceContext()); })()), "formgroups", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["formgroup"]) {
                // line 31
                echo "                        <div
                            class=\"tab-pane ";
                // line 32
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                    echo "active";
                }
                echo "\"
                            id=\"";
                // line 33
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["associationAdmin"]) || arraykeyexists("associationAdmin", $context) ? $context["associationAdmin"] : (function () { throw new TwigErrorRuntime('Variable "associationAdmin" does not exist.', 33, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "parent", array()), "loop", array()), "index", array()), "html", null, true);
                echo "";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                echo "\"
                        >
                            <fieldset>
                                <div class=\"sonata-ba-collapsed-fields\">
                                    ";
                // line 37
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["formgroup"], "fields", array()));
                foreach ($context['seq'] as $context["key"] => $context["fieldname"]) {
                    // line 38
                    echo "                                        ";
                    $context["nestedfield"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "children", array()), $context["fieldname"], array(), "array");
                    // line 39
                    echo "                                        <div class=\"sonata-ba-field-";
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
                    echo "-";
                    echo twigescapefilter($this->env, $context["fieldname"], "html", null, true);
                    echo "\">
                                            ";
                    // line 40
                    if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["associationAdmin"] ?? null), "formfielddescriptions", array(), "any", false, true), $context["fieldname"], array(), "array", true, true)) {
                        // line 41
                        echo "                                                ";
                        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["nestedfield"]) || arraykeyexists("nestedfield", $context) ? $context["nestedfield"] : (function () { throw new TwigErrorRuntime('Variable "nestedfield" does not exist.', 41, $this->getSourceContext()); })()), 'row', array("inline" => "natural", "edit" => "inline"));
                        // line 44
                        echo "
                                                ";
                        // line 45
                        $context["dummy"] = twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "setrendered", array());
                        // line 46
                        echo "                                            ";
                    } else {
                        // line 47
                        echo "                                                ";
                        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["nestedfield"]) || arraykeyexists("nestedfield", $context) ? $context["nestedfield"] : (function () { throw new TwigErrorRuntime('Variable "nestedfield" does not exist.', 47, $this->getSourceContext()); })()), 'row');
                        echo "
                                            ";
                    }
                    // line 49
                    echo "                                        </div>
                                    ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['fieldname'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 51
                echo "                                </div>
                            </fieldset>
                        </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['formgroup'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 55
            echo "                </div>
            </div>

            ";
            // line 58
            if (twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "delete", array(), "array", true, true)) {
                // line 59
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), $context["nestedgroupfield"], "delete", array(), "array"), 'row', array("label" => "actiondelete", "translationdomain" => "SonataAdminBundle"));
                echo "
            ";
            }
            // line 61
            echo "        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['nestedgroupfield'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 63
        echo "</div>
";
        
        $internal7883afa381135a7419e195442c60385e6643bf1655180e9b5684c2dc8081b572->leave($internal7883afa381135a7419e195442c60385e6643bf1655180e9b5684c2dc8081b572prof);

        
        $internale932d1c44c617877069ba6883258007926406d9a9da4bcb424325b48f015a4ac->leave($internale932d1c44c617877069ba6883258007926406d9a9da4bcb424325b48f015a4acprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editonetomanyinlinetabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 63,  216 => 61,  210 => 59,  208 => 58,  203 => 55,  186 => 51,  179 => 49,  173 => 47,  170 => 46,  168 => 45,  165 => 44,  162 => 41,  160 => 40,  153 => 39,  150 => 38,  146 => 37,  135 => 33,  129 => 32,  126 => 31,  109 => 30,  104 => 27,  86 => 23,  75 => 19,  67 => 17,  50 => 16,  45 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<div class=\"sonata-ba-tabs\">
    {% for nestedgroupfield in form.children %}
        <div>
            <div class=\"nav-tabs-custom\">
                <ul class=\"nav nav-tabs\">
                    {% for name, formgroup in associationAdmin.formgroups %}
                        <li class=\"{% if loop.first %}active{% endif %}\">
                            <a
                                href=\"#{{ associationAdmin.uniqid }}{{ loop.parent.loop.index }}{{ loop.index }}\"
                                data-toggle=\"tab\"
                            >
                                <i class=\"icon-exclamation-sign has-errors hide\"></i>
                                {{ associationAdmin.trans(name, {}, formgroup.translationdomain) }}
                            </a>
                        </li>
                    {% endfor %}
                </ul>

                <div class=\"tab-content\">
                    {% for name, formgroup in associationAdmin.formgroups %}
                        <div
                            class=\"tab-pane {% if loop.first %}active{% endif %}\"
                            id=\"{{ associationAdmin.uniqid }}{{ loop.parent.loop.index }}{{ loop.index }}\"
                        >
                            <fieldset>
                                <div class=\"sonata-ba-collapsed-fields\">
                                    {% for fieldname in formgroup.fields %}
                                        {% set nestedfield = nestedgroupfield.children[fieldname] %}
                                        <div class=\"sonata-ba-field-{{ id }}-{{ fieldname }}\">
                                            {% if associationAdmin.formfielddescriptions[fieldname] is defined %}
                                                {{ formrow(nestedfield, {
                                                    'inline': 'natural',
                                                    'edit'  : 'inline'
                                                }) }}
                                                {% set dummy = nestedgroupfield.setrendered %}
                                            {% else %}
                                                {{ formrow(nestedfield) }}
                                            {% endif %}
                                        </div>
                                    {% endfor %}
                                </div>
                            </fieldset>
                        </div>
                    {% endfor %}
                </div>
            </div>

            {% if nestedgroupfield['delete'] is defined %}
                {{ formrow(nestedgroupfield['delete'], {'label': 'actiondelete', 'translationdomain': 'SonataAdminBundle'}) }}
            {% endif %}
        </div>
    {% endfor %}
</div>
", "SonataAdminBundle:CRUD/Association:editonetomanyinlinetabs.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editonetomanyinlinetabs.html.twig");
    }
}
