<?php

/* SonataAdminBundle:CRUD:dashboardaction.html.twig */
class TwigTemplate4e09475fd7a542567a434c3df8a1ff43c42b45967fad0d439f68f0006b03985f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9ab18e424958c975e8ebe442f1e7ad7689bcc9e107902b70a41deaae7039d3fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9ab18e424958c975e8ebe442f1e7ad7689bcc9e107902b70a41deaae7039d3fc->enter($internal9ab18e424958c975e8ebe442f1e7ad7689bcc9e107902b70a41deaae7039d3fcprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:dashboardaction.html.twig"));

        $internal1b7c76a541607d975f6e1a2f1f3271d67c9471875a74901d2ec62f02992ddfde = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1b7c76a541607d975f6e1a2f1f3271d67c9471875a74901d2ec62f02992ddfde->enter($internal1b7c76a541607d975f6e1a2f1f3271d67c9471875a74901d2ec62f02992ddfdeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:dashboardaction.html.twig"));

        // line 1
        echo "<a class=\"btn btn-link btn-flat\" href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 1, $this->getSourceContext()); })()), "url", array()), "html", null, true);
        echo "\">
    <i class=\"fa fa-";
        // line 2
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 2, $this->getSourceContext()); })()), "icon", array()), "html", null, true);
        echo "\" aria-hidden=\"true\"></i>
    ";
        // line 3
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 3, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["action"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
        echo "
</a>
";
        
        $internal9ab18e424958c975e8ebe442f1e7ad7689bcc9e107902b70a41deaae7039d3fc->leave($internal9ab18e424958c975e8ebe442f1e7ad7689bcc9e107902b70a41deaae7039d3fcprof);

        
        $internal1b7c76a541607d975f6e1a2f1f3271d67c9471875a74901d2ec62f02992ddfde->leave($internal1b7c76a541607d975f6e1a2f1f3271d67c9471875a74901d2ec62f02992ddfdeprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:dashboardaction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 3,  30 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<a class=\"btn btn-link btn-flat\" href=\"{{ action.url }}\">
    <i class=\"fa fa-{{ action.icon }}\" aria-hidden=\"true\"></i>
    {{ action.label|trans({}, action.translationdomain|default('SonataAdminBundle')) }}
</a>
", "SonataAdminBundle:CRUD:dashboardaction.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/dashboardaction.html.twig");
    }
}
