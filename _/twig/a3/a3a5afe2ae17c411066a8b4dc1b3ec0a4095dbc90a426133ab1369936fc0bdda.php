<?php

/* @Framework/Form/emailwidget.html.php */
class TwigTemplate2e34fa4d32675eebb2482910462f4a21159d3a5d411089426d3b4b855c4bbefb extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal080dbc5a628685db01ca5226f20181e8008ff0403c7ce1a44dd645d4280cd427 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal080dbc5a628685db01ca5226f20181e8008ff0403c7ce1a44dd645d4280cd427->enter($internal080dbc5a628685db01ca5226f20181e8008ff0403c7ce1a44dd645d4280cd427prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/emailwidget.html.php"));

        $internal9fadc7eebe6eab8b6fe8e13d709a850c9a03f30b3badb503709f96b07d57aa36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9fadc7eebe6eab8b6fe8e13d709a850c9a03f30b3badb503709f96b07d57aa36->enter($internal9fadc7eebe6eab8b6fe8e13d709a850c9a03f30b3badb503709f96b07d57aa36prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/emailwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $internal080dbc5a628685db01ca5226f20181e8008ff0403c7ce1a44dd645d4280cd427->leave($internal080dbc5a628685db01ca5226f20181e8008ff0403c7ce1a44dd645d4280cd427prof);

        
        $internal9fadc7eebe6eab8b6fe8e13d709a850c9a03f30b3badb503709f96b07d57aa36->leave($internal9fadc7eebe6eab8b6fe8e13d709a850c9a03f30b3badb503709f96b07d57aa36prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/emailwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/emailwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/emailwidget.html.php");
    }
}
