<?php

/* TwigBundle:Exception:exceptionfull.html.twig */
class TwigTemplate6ded606f9c507e947b997fe4be9e94e75220a65f03da6a9dfe455b8e28db1274 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exceptionfull.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'blockhead'),
            'title' => array($this, 'blocktitle'),
            'body' => array($this, 'blockbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc8f6ce14ecff1cac120197c9091391993936057fee0168eb1fe0f306f0dd3a91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc8f6ce14ecff1cac120197c9091391993936057fee0168eb1fe0f306f0dd3a91->enter($internalc8f6ce14ecff1cac120197c9091391993936057fee0168eb1fe0f306f0dd3a91prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exceptionfull.html.twig"));

        $internalcf7402eb1c3ce1804f3446b761ca3ed65b8cc9b5a2d418ca4da7e83ad70f4323 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcf7402eb1c3ce1804f3446b761ca3ed65b8cc9b5a2d418ca4da7e83ad70f4323->enter($internalcf7402eb1c3ce1804f3446b761ca3ed65b8cc9b5a2d418ca4da7e83ad70f4323prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exceptionfull.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc8f6ce14ecff1cac120197c9091391993936057fee0168eb1fe0f306f0dd3a91->leave($internalc8f6ce14ecff1cac120197c9091391993936057fee0168eb1fe0f306f0dd3a91prof);

        
        $internalcf7402eb1c3ce1804f3446b761ca3ed65b8cc9b5a2d418ca4da7e83ad70f4323->leave($internalcf7402eb1c3ce1804f3446b761ca3ed65b8cc9b5a2d418ca4da7e83ad70f4323prof);

    }

    // line 3
    public function blockhead($context, array $blocks = array())
    {
        $internal3c20d2dc715ede3374432ebefa58c3249475423b9363d7b351df147a1fa04319 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3c20d2dc715ede3374432ebefa58c3249475423b9363d7b351df147a1fa04319->enter($internal3c20d2dc715ede3374432ebefa58c3249475423b9363d7b351df147a1fa04319prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        $internala62277f706b523b307fd52599fe1777a75058ed4744f4e666c8cd4bed80c630f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala62277f706b523b307fd52599fe1777a75058ed4744f4e666c8cd4bed80c630f->enter($internala62277f706b523b307fd52599fe1777a75058ed4744f4e666c8cd4bed80c630fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $internala62277f706b523b307fd52599fe1777a75058ed4744f4e666c8cd4bed80c630f->leave($internala62277f706b523b307fd52599fe1777a75058ed4744f4e666c8cd4bed80c630fprof);

        
        $internal3c20d2dc715ede3374432ebefa58c3249475423b9363d7b351df147a1fa04319->leave($internal3c20d2dc715ede3374432ebefa58c3249475423b9363d7b351df147a1fa04319prof);

    }

    // line 136
    public function blocktitle($context, array $blocks = array())
    {
        $internal55a8437428fb6e9531bd13923d0ad4628755a82760f4a3716d39822b8957805c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal55a8437428fb6e9531bd13923d0ad4628755a82760f4a3716d39822b8957805c->enter($internal55a8437428fb6e9531bd13923d0ad4628755a82760f4a3716d39822b8957805cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internalf55a02b7b33a7d58716ef663979cc6741b9331f3b4c7416884c6b666bd210ab8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf55a02b7b33a7d58716ef663979cc6741b9331f3b4c7416884c6b666bd210ab8->enter($internalf55a02b7b33a7d58716ef663979cc6741b9331f3b4c7416884c6b666bd210ab8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $internalf55a02b7b33a7d58716ef663979cc6741b9331f3b4c7416884c6b666bd210ab8->leave($internalf55a02b7b33a7d58716ef663979cc6741b9331f3b4c7416884c6b666bd210ab8prof);

        
        $internal55a8437428fb6e9531bd13923d0ad4628755a82760f4a3716d39822b8957805c->leave($internal55a8437428fb6e9531bd13923d0ad4628755a82760f4a3716d39822b8957805cprof);

    }

    // line 140
    public function blockbody($context, array $blocks = array())
    {
        $internal3b391cef64f89253eac57e9c86cb3393b102f25c957672b933fc22bce11f2a89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3b391cef64f89253eac57e9c86cb3393b102f25c957672b933fc22bce11f2a89->enter($internal3b391cef64f89253eac57e9c86cb3393b102f25c957672b933fc22bce11f2a89prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internal762f0ff8986c40b8607c54baad3b3b6ba2d3da430282ad6c3ad919d416d88204 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal762f0ff8986c40b8607c54baad3b3b6ba2d3da430282ad6c3ad919d416d88204->enter($internal762f0ff8986c40b8607c54baad3b3b6ba2d3da430282ad6c3ad919d416d88204prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exceptionfull.html.twig", 141)->display($context);
        
        $internal762f0ff8986c40b8607c54baad3b3b6ba2d3da430282ad6c3ad919d416d88204->leave($internal762f0ff8986c40b8607c54baad3b3b6ba2d3da430282ad6c3ad919d416d88204prof);

        
        $internal3b391cef64f89253eac57e9c86cb3393b102f25c957672b933fc22bce11f2a89->leave($internal3b391cef64f89253eac57e9c86cb3393b102f25c957672b933fc22bce11f2a89prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exceptionfull.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ statuscode }} {{ statustext }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exceptionfull.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exceptionfull.html.twig");
    }
}
