<?php

/* TwigBundle:Exception:exception.html.twig */
class TwigTemplateacce53e49404ddfcd0b5b67793341bfae79290d9e05197b0b51b4203d8cb4513 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldfdc98ef2fe5f30389f70826b9445b0d6448ad9d9a1a6f7d8370990536639706 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldfdc98ef2fe5f30389f70826b9445b0d6448ad9d9a1a6f7d8370990536639706->enter($internaldfdc98ef2fe5f30389f70826b9445b0d6448ad9d9a1a6f7d8370990536639706prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.html.twig"));

        $internalf6c148832e28bf2e58afa30efeb558c04e56985a7549878344def138547763e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf6c148832e28bf2e58afa30efeb558c04e56985a7549878344def138547763e1->enter($internalf6c148832e28bf2e58afa30efeb558c04e56985a7549878344def138547763e1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.html.twig"));

        // line 1
        echo "<div class=\"exception-summary ";
        echo ((twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "message", array()))) ? ("exception-without-message") : (""));
        echo "\">
    <div class=\"exception-metadata\">
        <div class=\"container\">
            <h2 class=\"exception-hierarchy\">
                ";
        // line 5
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twigreversefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 5, $this->getSourceContext()); })()), "allPrevious", array())));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["previousException"]) {
            // line 6
            echo "                    ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->abbrClass(twiggetattribute($this->env, $this->getSourceContext(), $context["previousException"], "class", array()));
            echo "
                    <span class=\"icon\">";
            // line 7
            echo twiginclude($this->env, $context, "@Twig/images/chevron-right.svg");
            echo "</span>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['previousException'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 9
        echo "                ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->abbrClass(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 9, $this->getSourceContext()); })()), "class", array()));
        echo "
            </h2>
            <h2 class=\"exception-http\">
                HTTP ";
        // line 12
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo "</small>
            </h2>
        </div>
    </div>

    <div class=\"exception-message-wrapper\">
        <div class=\"container\">
            <h1 class=\"break-long-words exception-message ";
        // line 19
        echo (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 19, $this->getSourceContext()); })()), "message", array())) > 180)) ? ("long") : (""));
        echo "\">";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatFileFromText(nl2br(twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 20, $this->getSourceContext()); })()), "message", array()), "html", null, true)));
        // line 21
        echo "</h1>

            <div class=\"exception-illustration hidden-xs-down\">
                ";
        // line 24
        echo twiginclude($this->env, $context, "@Twig/images/symfony-ghost.svg");
        echo "
            </div>
        </div>
    </div>
</div>

<div class=\"container\">
    <div class=\"sf-tabs\">
        <div class=\"tab\">
            ";
        // line 33
        $context["exceptionasarray"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 33, $this->getSourceContext()); })()), "toarray", array());
        // line 34
        echo "            ";
        $context["exceptionswithusercode"] = array();
        // line 35
        echo "            ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 35, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["i"] => $context["e"]) {
            // line 36
            echo "                ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["e"], "trace", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["trace"]) {
                // line 37
                echo "                    ";
                if (((( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array())) && !twiginfilter("/vendor/", twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array()))) && !twiginfilter("/var/cache/", twiggetattribute($this->env, $this->getSourceContext(), $context["trace"], "file", array()))) &&  !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array()))) {
                    // line 38
                    echo "                        ";
                    $context["exceptionswithusercode"] = twigarraymerge((isset($context["exceptionswithusercode"]) || arraykeyexists("exceptionswithusercode", $context) ? $context["exceptionswithusercode"] : (function () { throw new TwigErrorRuntime('Variable "exceptionswithusercode" does not exist.', 38, $this->getSourceContext()); })()), array(0 => $context["i"]));
                    // line 39
                    echo "                    ";
                }
                // line 40
                echo "                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['trace'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 41
            echo "            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['i'], $context['e'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 42
        echo "            <h3 class=\"tab-title\">
                ";
        // line 43
        if ((twiglengthfilter($this->env, (isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 43, $this->getSourceContext()); })())) > 1)) {
            // line 44
            echo "                    Exceptions <span class=\"badge\">";
            echo twigescapefilter($this->env, twiglengthfilter($this->env, (isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 44, $this->getSourceContext()); })())), "html", null, true);
            echo "</span>
                ";
        } else {
            // line 46
            echo "                    Exception
                ";
        }
        // line 48
        echo "            </h3>

            <div class=\"tab-content\">
                ";
        // line 51
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 51, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["i"] => $context["e"]) {
            // line 52
            echo "                    ";
            echo twiginclude($this->env, $context, "@Twig/Exception/traces.html.twig", array("exception" => $context["e"], "index" => twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "expand" => (twiginfilter($context["i"], (isset($context["exceptionswithusercode"]) || arraykeyexists("exceptionswithusercode", $context) ? $context["exceptionswithusercode"] : (function () { throw new TwigErrorRuntime('Variable "exceptionswithusercode" does not exist.', 52, $this->getSourceContext()); })())) || (twigtestempty((isset($context["exceptionswithusercode"]) || arraykeyexists("exceptionswithusercode", $context) ? $context["exceptionswithusercode"] : (function () { throw new TwigErrorRuntime('Variable "exceptionswithusercode" does not exist.', 52, $this->getSourceContext()); })())) && twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())))), false);
            echo "
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['i'], $context['e'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 54
        echo "            </div>
        </div>

        <div class=\"tab ";
        // line 57
        echo ((twigtestempty((isset($context["logger"]) || arraykeyexists("logger", $context) ? $context["logger"] : (function () { throw new TwigErrorRuntime('Variable "logger" does not exist.', 57, $this->getSourceContext()); })()))) ? ("disabled") : (""));
        echo "\">
            <h3 class=\"tab-title\">
                Logs
                ";
        // line 60
        if ((((twiggetattribute($this->env, $this->getSourceContext(), ($context["logger"] ?? null), "counterrors", array(), "any", true, true) &&  !(null === twiggetattribute($this->env, $this->getSourceContext(), ($context["logger"] ?? null), "counterrors", array())))) ? (twiggetattribute($this->env, $this->getSourceContext(), ($context["logger"] ?? null), "counterrors", array())) : (false))) {
            echo "<span class=\"badge status-error\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["logger"]) || arraykeyexists("logger", $context) ? $context["logger"] : (function () { throw new TwigErrorRuntime('Variable "logger" does not exist.', 60, $this->getSourceContext()); })()), "counterrors", array()), "html", null, true);
            echo "</span>";
        }
        // line 61
        echo "            </h3>

            <div class=\"tab-content\">
                ";
        // line 64
        if ((isset($context["logger"]) || arraykeyexists("logger", $context) ? $context["logger"] : (function () { throw new TwigErrorRuntime('Variable "logger" does not exist.', 64, $this->getSourceContext()); })())) {
            // line 65
            echo "                    ";
            echo twiginclude($this->env, $context, "@Twig/Exception/logs.html.twig", array("logs" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["logger"]) || arraykeyexists("logger", $context) ? $context["logger"] : (function () { throw new TwigErrorRuntime('Variable "logger" does not exist.', 65, $this->getSourceContext()); })()), "logs", array())), false);
            echo "
                ";
        } else {
            // line 67
            echo "                    <div class=\"empty\">
                        <p>No log messages</p>
                    </div>
                ";
        }
        // line 71
        echo "            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">
                ";
        // line 76
        if ((twiglengthfilter($this->env, (isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 76, $this->getSourceContext()); })())) > 1)) {
            // line 77
            echo "                    Stack Traces <span class=\"badge\">";
            echo twigescapefilter($this->env, twiglengthfilter($this->env, (isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 77, $this->getSourceContext()); })())), "html", null, true);
            echo "</span>
                ";
        } else {
            // line 79
            echo "                    Stack Trace
                ";
        }
        // line 81
        echo "            </h3>

            <div class=\"tab-content\">
                ";
        // line 84
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["exceptionasarray"]) || arraykeyexists("exceptionasarray", $context) ? $context["exceptionasarray"] : (function () { throw new TwigErrorRuntime('Variable "exceptionasarray" does not exist.', 84, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["e"]) {
            // line 85
            echo "                    ";
            echo twiginclude($this->env, $context, "@Twig/Exception/tracestext.html.twig", array("exception" => $context["e"], "index" => twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "numexceptions" => twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "length", array())), false);
            echo "
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['e'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 87
        echo "            </div>
        </div>

        ";
        // line 90
        if ( !twigtestempty((isset($context["currentContent"]) || arraykeyexists("currentContent", $context) ? $context["currentContent"] : (function () { throw new TwigErrorRuntime('Variable "currentContent" does not exist.', 90, $this->getSourceContext()); })()))) {
            // line 91
            echo "        <div class=\"tab\">
            <h3 class=\"tab-title\">Output content</h3>

            <div class=\"tab-content\">
                ";
            // line 95
            echo twigescapefilter($this->env, (isset($context["currentContent"]) || arraykeyexists("currentContent", $context) ? $context["currentContent"] : (function () { throw new TwigErrorRuntime('Variable "currentContent" does not exist.', 95, $this->getSourceContext()); })()), "html", null, true);
            echo "
            </div>
        </div>
        ";
        }
        // line 99
        echo "    </div>
</div>
";
        
        $internaldfdc98ef2fe5f30389f70826b9445b0d6448ad9d9a1a6f7d8370990536639706->leave($internaldfdc98ef2fe5f30389f70826b9445b0d6448ad9d9a1a6f7d8370990536639706prof);

        
        $internalf6c148832e28bf2e58afa30efeb558c04e56985a7549878344def138547763e1->leave($internalf6c148832e28bf2e58afa30efeb558c04e56985a7549878344def138547763e1prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 99,  330 => 95,  324 => 91,  322 => 90,  317 => 87,  300 => 85,  283 => 84,  278 => 81,  274 => 79,  268 => 77,  266 => 76,  259 => 71,  253 => 67,  247 => 65,  245 => 64,  240 => 61,  234 => 60,  228 => 57,  223 => 54,  206 => 52,  189 => 51,  184 => 48,  180 => 46,  174 => 44,  172 => 43,  169 => 42,  163 => 41,  149 => 40,  146 => 39,  143 => 38,  140 => 37,  122 => 36,  117 => 35,  114 => 34,  112 => 33,  100 => 24,  95 => 21,  93 => 20,  90 => 19,  78 => 12,  71 => 9,  55 => 7,  50 => 6,  33 => 5,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div class=\"exception-summary {{ exception.message is empty ? 'exception-without-message' }}\">
    <div class=\"exception-metadata\">
        <div class=\"container\">
            <h2 class=\"exception-hierarchy\">
                {% for previousException in exception.allPrevious|reverse %}
                    {{ previousException.class|abbrclass }}
                    <span class=\"icon\">{{ include('@Twig/images/chevron-right.svg') }}</span>
                {% endfor %}
                {{ exception.class|abbrclass }}
            </h2>
            <h2 class=\"exception-http\">
                HTTP {{ statuscode }} <small>{{ statustext }}</small>
            </h2>
        </div>
    </div>

    <div class=\"exception-message-wrapper\">
        <div class=\"container\">
            <h1 class=\"break-long-words exception-message {{ exception.message|length > 180 ? 'long' }}\">
                {{- exception.message|nl2br|formatfilefromtext -}}
            </h1>

            <div class=\"exception-illustration hidden-xs-down\">
                {{ include('@Twig/images/symfony-ghost.svg') }}
            </div>
        </div>
    </div>
</div>

<div class=\"container\">
    <div class=\"sf-tabs\">
        <div class=\"tab\">
            {% set exceptionasarray = exception.toarray %}
            {% set exceptionswithusercode = [] %}
            {% for i, e in exceptionasarray %}
                {% for trace in e.trace %}
                    {% if (trace.file is not empty) and ('/vendor/' not in trace.file) and ('/var/cache/' not in trace.file) and not loop.last %}
                        {% set exceptionswithusercode = exceptionswithusercode|merge([i]) %}
                    {% endif %}
                {% endfor %}
            {% endfor %}
            <h3 class=\"tab-title\">
                {% if exceptionasarray|length > 1 %}
                    Exceptions <span class=\"badge\">{{ exceptionasarray|length }}</span>
                {% else %}
                    Exception
                {% endif %}
            </h3>

            <div class=\"tab-content\">
                {% for i, e in exceptionasarray %}
                    {{ include('@Twig/Exception/traces.html.twig', { exception: e, index: loop.index, expand: i in exceptionswithusercode or (exceptionswithusercode is empty and loop.first) }, withcontext = false) }}
                {% endfor %}
            </div>
        </div>

        <div class=\"tab {{ logger is empty ? 'disabled' }}\">
            <h3 class=\"tab-title\">
                Logs
                {% if logger.counterrors ?? false %}<span class=\"badge status-error\">{{ logger.counterrors }}</span>{% endif %}
            </h3>

            <div class=\"tab-content\">
                {% if logger %}
                    {{ include('@Twig/Exception/logs.html.twig', { logs: logger.logs }, withcontext = false)  }}
                {% else %}
                    <div class=\"empty\">
                        <p>No log messages</p>
                    </div>
                {% endif %}
            </div>
        </div>

        <div class=\"tab\">
            <h3 class=\"tab-title\">
                {% if exceptionasarray|length > 1 %}
                    Stack Traces <span class=\"badge\">{{ exceptionasarray|length }}</span>
                {% else %}
                    Stack Trace
                {% endif %}
            </h3>

            <div class=\"tab-content\">
                {% for e in exceptionasarray %}
                    {{ include('@Twig/Exception/tracestext.html.twig', { exception: e, index: loop.index, numexceptions: loop.length }, withcontext = false) }}
                {% endfor %}
            </div>
        </div>

        {% if currentContent is not empty %}
        <div class=\"tab\">
            <h3 class=\"tab-title\">Output content</h3>

            <div class=\"tab-content\">
                {{ currentContent }}
            </div>
        </div>
        {% endif %}
    </div>
</div>
", "TwigBundle:Exception:exception.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.html.twig");
    }
}
