<?php

/* @Framework/Form/formerrors.html.php */
class TwigTemplatee6ea364fd9e2b51093af6d02ef0978eacadb12c62619770c8fd207b74cbead05 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala4f213e40fae72686200ac1c63808279a2eaa6dcd604113d7240ff70de7ad4ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala4f213e40fae72686200ac1c63808279a2eaa6dcd604113d7240ff70de7ad4ef->enter($internala4f213e40fae72686200ac1c63808279a2eaa6dcd604113d7240ff70de7ad4efprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formerrors.html.php"));

        $internal62ae3655d4a7130691b1bbf415a4cbb23a7b24b11f2fd091d4fab0d48cefae92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal62ae3655d4a7130691b1bbf415a4cbb23a7b24b11f2fd091d4fab0d48cefae92->enter($internal62ae3655d4a7130691b1bbf415a4cbb23a7b24b11f2fd091d4fab0d48cefae92prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formerrors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $internala4f213e40fae72686200ac1c63808279a2eaa6dcd604113d7240ff70de7ad4ef->leave($internala4f213e40fae72686200ac1c63808279a2eaa6dcd604113d7240ff70de7ad4efprof);

        
        $internal62ae3655d4a7130691b1bbf415a4cbb23a7b24b11f2fd091d4fab0d48cefae92->leave($internal62ae3655d4a7130691b1bbf415a4cbb23a7b24b11f2fd091d4fab0d48cefae92prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formerrors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/formerrors.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formerrors.html.php");
    }
}
