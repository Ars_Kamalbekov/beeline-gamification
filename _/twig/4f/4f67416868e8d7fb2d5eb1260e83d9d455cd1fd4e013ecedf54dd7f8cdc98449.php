<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class TwigTemplate4ab62843c657b0e554ef1e90f8ac25f3306178cd41c1c49efc17a2bbb359cb4c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald93a6aa1435e1e105f4ee10e7ddc6bf337a19e888f9fe6237234898a937805b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald93a6aa1435e1e105f4ee10e7ddc6bf337a19e888f9fe6237234898a937805b6->enter($internald93a6aa1435e1e105f4ee10e7ddc6bf337a19e888f9fe6237234898a937805b6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $internal534530c1bb8782e33747eeb5d3fd810b7524c0e046a37b562410135d3f16a6da = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal534530c1bb8782e33747eeb5d3fd810b7524c0e046a37b562410135d3f16a6da->enter($internal534530c1bb8782e33747eeb5d3fd810b7524c0e046a37b562410135d3f16a6daprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald93a6aa1435e1e105f4ee10e7ddc6bf337a19e888f9fe6237234898a937805b6->leave($internald93a6aa1435e1e105f4ee10e7ddc6bf337a19e888f9fe6237234898a937805b6prof);

        
        $internal534530c1bb8782e33747eeb5d3fd810b7524c0e046a37b562410135d3f16a6da->leave($internal534530c1bb8782e33747eeb5d3fd810b7524c0e046a37b562410135d3f16a6daprof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal5ed48d63afa2e9299296b1404ae9643e23e49714e39c87da2c4254664f4f8b9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5ed48d63afa2e9299296b1404ae9643e23e49714e39c87da2c4254664f4f8b9b->enter($internal5ed48d63afa2e9299296b1404ae9643e23e49714e39c87da2c4254664f4f8b9bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal57207baf64ef21c0d0a6fce3be0e573cab2072b39884f773dfa19e66a4f985f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal57207baf64ef21c0d0a6fce3be0e573cab2072b39884f773dfa19e66a4f985f4->enter($internal57207baf64ef21c0d0a6fce3be0e573cab2072b39884f773dfa19e66a4f985f4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/resetcontent.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $internal57207baf64ef21c0d0a6fce3be0e573cab2072b39884f773dfa19e66a4f985f4->leave($internal57207baf64ef21c0d0a6fce3be0e573cab2072b39884f773dfa19e66a4f985f4prof);

        
        $internal5ed48d63afa2e9299296b1404ae9643e23e49714e39c87da2c4254664f4f8b9b->leave($internal5ed48d63afa2e9299296b1404ae9643e23e49714e39c87da2c4254664f4f8b9bprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Resetting/resetcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Resetting:reset.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/reset.html.twig");
    }
}
