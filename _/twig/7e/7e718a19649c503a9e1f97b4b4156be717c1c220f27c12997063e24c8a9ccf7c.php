<?php

/* @WebProfiler/Icon/ajax.svg */
class TwigTemplate50b65aa1e22a0889af9b8e1b0ac20fb263ef0f9299fb5a9db2aeba6b36fe7eca extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal66dddf11401626cf05ea0f9215feec78bdab847b98949f7e2ea74a3ea3ec0f65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal66dddf11401626cf05ea0f9215feec78bdab847b98949f7e2ea74a3ea3ec0f65->enter($internal66dddf11401626cf05ea0f9215feec78bdab847b98949f7e2ea74a3ea3ec0f65prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/ajax.svg"));

        $internal6099781c9c66cbaf577f6013b098b21a669e7332aeab72fe9458d503ecf816c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6099781c9c66cbaf577f6013b098b21a669e7332aeab72fe9458d503ecf816c1->enter($internal6099781c9c66cbaf577f6013b098b21a669e7332aeab72fe9458d503ecf816c1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/ajax.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M9.8,18l-3.8,4.4c-0.3,0.3-0.8,0.4-1.1,0L1,18c-0.4-0.5-0.1-1,0.5-1H3V6.4C3,3.8,5.5,2,8.2,2h3.9
    c1.1,0,2,0.9,2,2s-0.9,2-2,2H8.2C7.7,6,7,6,7,6.4V17h2.2C9.8,17,10.2,17.5,9.8,18z M23,6l-3.8-4.5c-0.3-0.3-0.8-0.3-1.1,0L14.2,6
    c-0.4,0.5-0.1,1,0.5,1H17v10.6c0,0.4-0.7,0.4-1.2,0.4h-3.9c-1.1,0-2,0.9-2,2s0.9,2,2,2h3.9c2.6,0,5.2-1.8,5.2-4.4V7h1.5
    C23.1,7,23.4,6.5,23,6z\"/>
</svg>
";
        
        $internal66dddf11401626cf05ea0f9215feec78bdab847b98949f7e2ea74a3ea3ec0f65->leave($internal66dddf11401626cf05ea0f9215feec78bdab847b98949f7e2ea74a3ea3ec0f65prof);

        
        $internal6099781c9c66cbaf577f6013b098b21a669e7332aeab72fe9458d503ecf816c1->leave($internal6099781c9c66cbaf577f6013b098b21a669e7332aeab72fe9458d503ecf816c1prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/ajax.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M9.8,18l-3.8,4.4c-0.3,0.3-0.8,0.4-1.1,0L1,18c-0.4-0.5-0.1-1,0.5-1H3V6.4C3,3.8,5.5,2,8.2,2h3.9
    c1.1,0,2,0.9,2,2s-0.9,2-2,2H8.2C7.7,6,7,6,7,6.4V17h2.2C9.8,17,10.2,17.5,9.8,18z M23,6l-3.8-4.5c-0.3-0.3-0.8-0.3-1.1,0L14.2,6
    c-0.4,0.5-0.1,1,0.5,1H17v10.6c0,0.4-0.7,0.4-1.2,0.4h-3.9c-1.1,0-2,0.9-2,2s0.9,2,2,2h3.9c2.6,0,5.2-1.8,5.2-4.4V7h1.5
    C23.1,7,23.4,6.5,23,6z\"/>
</svg>
", "@WebProfiler/Icon/ajax.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/ajax.svg");
    }
}
