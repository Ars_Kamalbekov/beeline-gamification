<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class TwigTemplate28fc6b36d191a0d4a1a4f35bff7401b59bffbb54a3754b915d6ceafb62cfaa3b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala5be057abdd7c3a5d165c797e7cdc5a53769bf9b6c53dd74f9e080c362883856 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala5be057abdd7c3a5d165c797e7cdc5a53769bf9b6c53dd74f9e080c362883856->enter($internala5be057abdd7c3a5d165c797e7cdc5a53769bf9b6c53dd74f9e080c362883856prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $internald051a057e81d4ecc005205f853a96d7f120f2b21e482b1fee4125ef4b6f7b3e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald051a057e81d4ecc005205f853a96d7f120f2b21e482b1fee4125ef4b6f7b3e5->enter($internald051a057e81d4ecc005205f853a96d7f120f2b21e482b1fee4125ef4b6f7b3e5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $internala5be057abdd7c3a5d165c797e7cdc5a53769bf9b6c53dd74f9e080c362883856->leave($internala5be057abdd7c3a5d165c797e7cdc5a53769bf9b6c53dd74f9e080c362883856prof);

        
        $internald051a057e81d4ecc005205f853a96d7f120f2b21e482b1fee4125ef4b6f7b3e5->leave($internald051a057e81d4ecc005205f853a96d7f120f2b21e482b1fee4125ef4b6f7b3e5prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
