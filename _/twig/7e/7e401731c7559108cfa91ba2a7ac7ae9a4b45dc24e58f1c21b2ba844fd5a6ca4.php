<?php

/* SonataAdminBundle:CRUD:listbatch.html.twig */
class TwigTemplatea40d21698caac274b078dc4c7ca3040486339008a905570b2f7eb7e13c56773f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listbatch.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc78478747438090552c9d9c024197bfde2b8d0a17b23bd4edc3daf80980a2c38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc78478747438090552c9d9c024197bfde2b8d0a17b23bd4edc3daf80980a2c38->enter($internalc78478747438090552c9d9c024197bfde2b8d0a17b23bd4edc3daf80980a2c38prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listbatch.html.twig"));

        $internalb8f926231d7c28e9799cbf0e128c259fa43de4b7c80d78ed9d73a16d14330745 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb8f926231d7c28e9799cbf0e128c259fa43de4b7c80d78ed9d73a16d14330745->enter($internalb8f926231d7c28e9799cbf0e128c259fa43de4b7c80d78ed9d73a16d14330745prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listbatch.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc78478747438090552c9d9c024197bfde2b8d0a17b23bd4edc3daf80980a2c38->leave($internalc78478747438090552c9d9c024197bfde2b8d0a17b23bd4edc3daf80980a2c38prof);

        
        $internalb8f926231d7c28e9799cbf0e128c259fa43de4b7c80d78ed9d73a16d14330745->leave($internalb8f926231d7c28e9799cbf0e128c259fa43de4b7c80d78ed9d73a16d14330745prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal302123cff904073d070d6ca05470077450e19d61da51b2efc26da5ce3cbc4f12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal302123cff904073d070d6ca05470077450e19d61da51b2efc26da5ce3cbc4f12->enter($internal302123cff904073d070d6ca05470077450e19d61da51b2efc26da5ce3cbc4f12prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal4fcc4e84273f24af944319d85926937adcf3be0bc804dd09c19c85155bb8dc48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4fcc4e84273f24af944319d85926937adcf3be0bc804dd09c19c85155bb8dc48->enter($internal4fcc4e84273f24af944319d85926937adcf3be0bc804dd09c19c85155bb8dc48prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <input type=\"checkbox\" name=\"idx[]\" value=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 15, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
";
        
        $internal4fcc4e84273f24af944319d85926937adcf3be0bc804dd09c19c85155bb8dc48->leave($internal4fcc4e84273f24af944319d85926937adcf3be0bc804dd09c19c85155bb8dc48prof);

        
        $internal302123cff904073d070d6ca05470077450e19d61da51b2efc26da5ce3cbc4f12->leave($internal302123cff904073d070d6ca05470077450e19d61da51b2efc26da5ce3cbc4f12prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listbatch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\">
{% endblock %}
", "SonataAdminBundle:CRUD:listbatch.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listbatch.html.twig");
    }
}
