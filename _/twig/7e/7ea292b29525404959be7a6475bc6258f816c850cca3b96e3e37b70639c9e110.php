<?php

/* @Framework/Form/formwidget.html.php */
class TwigTemplatee71db588431c232e60a49f34fb369731fbfdde9755f02615c9e6eabfaca44506 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcebb6cf9ceddb892f249cc61ef7eeae3788cfc7ebef464115e89819883f0bed1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcebb6cf9ceddb892f249cc61ef7eeae3788cfc7ebef464115e89819883f0bed1->enter($internalcebb6cf9ceddb892f249cc61ef7eeae3788cfc7ebef464115e89819883f0bed1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidget.html.php"));

        $internal26c840e3f25323f6c417c205be7b4bf1e69753b143189a25bc82ca88b4d17fce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal26c840e3f25323f6c417c205be7b4bf1e69753b143189a25bc82ca88b4d17fce->enter($internal26c840e3f25323f6c417c205be7b4bf1e69753b143189a25bc82ca88b4d17fceprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formwidget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'formwidgetcompound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'formwidgetsimple')?>
<?php endif ?>
";
        
        $internalcebb6cf9ceddb892f249cc61ef7eeae3788cfc7ebef464115e89819883f0bed1->leave($internalcebb6cf9ceddb892f249cc61ef7eeae3788cfc7ebef464115e89819883f0bed1prof);

        
        $internal26c840e3f25323f6c417c205be7b4bf1e69753b143189a25bc82ca88b4d17fce->leave($internal26c840e3f25323f6c417c205be7b4bf1e69753b143189a25bc82ca88b4d17fceprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'formwidgetcompound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'formwidgetsimple')?>
<?php endif ?>
", "@Framework/Form/formwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formwidget.html.php");
    }
}
