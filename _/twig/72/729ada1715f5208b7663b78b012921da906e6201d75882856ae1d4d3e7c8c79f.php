<?php

/* FOSUserBundle:Registration:registercontent.html.twig */
class TwigTemplatee1a6911d29f1e78fd4aab33befe077a921ae8524e19e927ae6576d89cc6a1932 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal13c01ca30edd003af84e8661956d765baf95fec8d0e5149bace4e0801984ceb8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal13c01ca30edd003af84e8661956d765baf95fec8d0e5149bace4e0801984ceb8->enter($internal13c01ca30edd003af84e8661956d765baf95fec8d0e5149bace4e0801984ceb8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:registercontent.html.twig"));

        $internal974a051ef476cf79478a1f330f80012ede6b4feeaa3a77560074ee298bface10 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal974a051ef476cf79478a1f330f80012ede6b4feeaa3a77560074ee298bface10->enter($internal974a051ef476cf79478a1f330f80012ede6b4feeaa3a77560074ee298bface10prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:registercontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserregistrationregister"), "attr" => array("class" => "fosuserregistrationregister")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internal13c01ca30edd003af84e8661956d765baf95fec8d0e5149bace4e0801984ceb8->leave($internal13c01ca30edd003af84e8661956d765baf95fec8d0e5149bace4e0801984ceb8prof);

        
        $internal974a051ef476cf79478a1f330f80012ede6b4feeaa3a77560074ee298bface10->leave($internal974a051ef476cf79478a1f330f80012ede6b4feeaa3a77560074ee298bface10prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:registercontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, {'method': 'post', 'action': path('fosuserregistrationregister'), 'attr': {'class': 'fosuserregistrationregister'}}) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'registration.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:Registration:registercontent.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Registration/registercontent.html.twig");
    }
}
