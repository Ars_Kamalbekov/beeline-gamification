<?php

/* TwigBundle:Exception:exception.css.twig */
class TwigTemplate4b6ec600105fdbf016705120b9c0ead55027215b220837b5c2586a4c745b12a7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal59bd5307096f7b23fefcd86964fd1aefbf520dc451985441c993330e8ef1ecc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal59bd5307096f7b23fefcd86964fd1aefbf520dc451985441c993330e8ef1ecc5->enter($internal59bd5307096f7b23fefcd86964fd1aefbf520dc451985441c993330e8ef1ecc5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $internalf44f10dbda8bdeab4cb7b1242cb69303919ecb367ad6a466f824d79ae32e3345 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf44f10dbda8bdeab4cb7b1242cb69303919ecb367ad6a466f824d79ae32e3345->enter($internalf44f10dbda8bdeab4cb7b1242cb69303919ecb367ad6a466f824d79ae32e3345prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twiginclude($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $internal59bd5307096f7b23fefcd86964fd1aefbf520dc451985441c993330e8ef1ecc5->leave($internal59bd5307096f7b23fefcd86964fd1aefbf520dc451985441c993330e8ef1ecc5prof);

        
        $internalf44f10dbda8bdeab4cb7b1242cb69303919ecb367ad6a466f824d79ae32e3345->leave($internalf44f10dbda8bdeab4cb7b1242cb69303919ecb367ad6a466f824d79ae32e3345prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
