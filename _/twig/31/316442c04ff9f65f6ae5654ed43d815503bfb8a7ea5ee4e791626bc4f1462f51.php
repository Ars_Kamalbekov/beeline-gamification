<?php

/* SonataAdminBundle:CRUD:emaillink.html.twig */
class TwigTemplate861dacb26c986e2294409fa9e261b1bd58a465e4996b4c154e095c0467bf3c70 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldbb49c40127ab31d6d64a71cd14296709d6ef4e9e9b4aa6590b0e9bf0aba5521 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldbb49c40127ab31d6d64a71cd14296709d6ef4e9e9b4aa6590b0e9bf0aba5521->enter($internaldbb49c40127ab31d6d64a71cd14296709d6ef4e9e9b4aa6590b0e9bf0aba5521prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:emaillink.html.twig"));

        $internal9c06eaa7718c9617f9db5ba9cbf4dcd133333724062bb025fd483fa2f107a820 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9c06eaa7718c9617f9db5ba9cbf4dcd133333724062bb025fd483fa2f107a820->enter($internal9c06eaa7718c9617f9db5ba9cbf4dcd133333724062bb025fd483fa2f107a820prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:emaillink.html.twig"));

        // line 2
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 2, $this->getSourceContext()); })()))) {
            // line 3
            echo "&nbsp;";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 4
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "asstring", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 4, $this->getSourceContext()); })()), "options", array()), "asstring", array()))) {
            // line 5
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        } else {
            // line 7
            $context["parameters"] = array();
            // line 8
            echo "    ";
            $context["subject"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "subject", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "subject", array()), "")) : (""));
            // line 9
            echo "    ";
            $context["body"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "body", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "body", array()), "")) : (""));
            // line 10
            echo "
    ";
            // line 11
            if ( !twigtestempty((isset($context["subject"]) || arraykeyexists("subject", $context) ? $context["subject"] : (function () { throw new TwigErrorRuntime('Variable "subject" does not exist.', 11, $this->getSourceContext()); })()))) {
                // line 12
                echo "        ";
                $context["parameters"] = twigarraymerge((isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 12, $this->getSourceContext()); })()), array("subject" => (isset($context["subject"]) || arraykeyexists("subject", $context) ? $context["subject"] : (function () { throw new TwigErrorRuntime('Variable "subject" does not exist.', 12, $this->getSourceContext()); })())));
                // line 13
                echo "    ";
            }
            // line 14
            echo "    ";
            if ( !twigtestempty((isset($context["body"]) || arraykeyexists("body", $context) ? $context["body"] : (function () { throw new TwigErrorRuntime('Variable "body" does not exist.', 14, $this->getSourceContext()); })()))) {
                // line 15
                echo "        ";
                $context["parameters"] = twigarraymerge((isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 15, $this->getSourceContext()); })()), array("body" => (isset($context["body"]) || arraykeyexists("body", $context) ? $context["body"] : (function () { throw new TwigErrorRuntime('Variable "body" does not exist.', 15, $this->getSourceContext()); })())));
                // line 16
                echo "    ";
            }
            // line 17
            echo "
    <a href=\"mailto:";
            // line 18
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), "html", null, true);
            if ((twiglengthfilter($this->env, (isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 18, $this->getSourceContext()); })())) > 0)) {
                echo "?";
                echo twigescapefilter($this->env, twigurlencodefilter((isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 18, $this->getSourceContext()); })())), "html", null, true);
            }
            echo "\">";
            // line 19
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
            // line 20
            echo "</a>";
        }
        
        $internaldbb49c40127ab31d6d64a71cd14296709d6ef4e9e9b4aa6590b0e9bf0aba5521->leave($internaldbb49c40127ab31d6d64a71cd14296709d6ef4e9e9b4aa6590b0e9bf0aba5521prof);

        
        $internal9c06eaa7718c9617f9db5ba9cbf4dcd133333724062bb025fd483fa2f107a820->leave($internal9c06eaa7718c9617f9db5ba9cbf4dcd133333724062bb025fd483fa2f107a820prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:emaillink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 20,  72 => 19,  65 => 18,  62 => 17,  59 => 16,  56 => 15,  53 => 14,  50 => 13,  47 => 12,  45 => 11,  42 => 10,  39 => 9,  36 => 8,  34 => 7,  31 => 5,  29 => 4,  27 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("
{%- if value is empty -%}
    &nbsp;
{%- elseif fielddescription.options.asstring is defined and fielddescription.options.asstring -%}
    {{ value }}
{%- else -%}
    {% set parameters = {} %}
    {% set subject = fielddescription.options.subject|default('') %}
    {% set body = fielddescription.options.body|default('') %}

    {% if subject is not empty %}
        {% set parameters = parameters|merge({'subject': subject}) %}
    {% endif %}
    {% if body is not empty %}
        {% set parameters = parameters|merge({'body': body}) %}
    {% endif %}

    <a href=\"mailto:{{ value }}{% if parameters|length > 0 %}?{{- parameters|urlencode -}}{% endif %}\">
        {{- value -}}
    </a>
{%- endif -%}
", "SonataAdminBundle:CRUD:emaillink.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/emaillink.html.twig");
    }
}
