<?php

/* SonataAdminBundle:CRUD:showdate.html.twig */
class TwigTemplate79cc8f099ffa645108dd76abcc22d95d3809f3b654849f5118c848c1fe309cfd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showdate.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal30d9694cc436b56404d5e616a2b9f85194e84f126bb7ae8033ec84601d1b0f81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal30d9694cc436b56404d5e616a2b9f85194e84f126bb7ae8033ec84601d1b0f81->enter($internal30d9694cc436b56404d5e616a2b9f85194e84f126bb7ae8033ec84601d1b0f81prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showdate.html.twig"));

        $internal44caa216f2fcec32a49d6298088f14a9e594242d0fe37c926a87f73f665e46e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal44caa216f2fcec32a49d6298088f14a9e594242d0fe37c926a87f73f665e46e4->enter($internal44caa216f2fcec32a49d6298088f14a9e594242d0fe37c926a87f73f665e46e4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showdate.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal30d9694cc436b56404d5e616a2b9f85194e84f126bb7ae8033ec84601d1b0f81->leave($internal30d9694cc436b56404d5e616a2b9f85194e84f126bb7ae8033ec84601d1b0f81prof);

        
        $internal44caa216f2fcec32a49d6298088f14a9e594242d0fe37c926a87f73f665e46e4->leave($internal44caa216f2fcec32a49d6298088f14a9e594242d0fe37c926a87f73f665e46e4prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal51914db78786b191da4c54f7c91951ca0668fc371d0263f60fd7d41aba421ef6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal51914db78786b191da4c54f7c91951ca0668fc371d0263f60fd7d41aba421ef6->enter($internal51914db78786b191da4c54f7c91951ca0668fc371d0263f60fd7d41aba421ef6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalaa0977f861edf521f31eaa7cffbc0a31703eb79144dd92431b9d4f78c12f44ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaa0977f861edf521f31eaa7cffbc0a31703eb79144dd92431b9d4f78c12f44ef->enter($internalaa0977f861edf521f31eaa7cffbc0a31703eb79144dd92431b9d4f78c12f44efprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), "F j, Y"), "html", null, true);
        }
        
        $internalaa0977f861edf521f31eaa7cffbc0a31703eb79144dd92431b9d4f78c12f44ef->leave($internalaa0977f861edf521f31eaa7cffbc0a31703eb79144dd92431b9d4f78c12f44efprof);

        
        $internal51914db78786b191da4c54f7c91951ca0668fc371d0263f60fd7d41aba421ef6->leave($internal51914db78786b191da4c54f7c91951ca0668fc371d0263f60fd7d41aba421ef6prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showdate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif fielddescription.options.format is defined -%}
        {{ value|date(fielddescription.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:showdate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showdate.html.twig");
    }
}
