<?php

/* @Framework/Form/datetimewidget.html.php */
class TwigTemplate4f675457c98b5dfcf1b01d2dae7eb9d5166f9575405bb7c33c888f64560cc3da extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9446c81240ac686a2d5fa1c278a95ee75b8f07fb16a13b04226b7d0196ddc5db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9446c81240ac686a2d5fa1c278a95ee75b8f07fb16a13b04226b7d0196ddc5db->enter($internal9446c81240ac686a2d5fa1c278a95ee75b8f07fb16a13b04226b7d0196ddc5dbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/datetimewidget.html.php"));

        $internal1edeb993a72791e28eeabb6b655946e37c82230dbd2b8aa3b774be748685a208 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1edeb993a72791e28eeabb6b655946e37c82230dbd2b8aa3b774be748685a208->enter($internal1edeb993a72791e28eeabb6b655946e37c82230dbd2b8aa3b774be748685a208prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/datetimewidget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $internal9446c81240ac686a2d5fa1c278a95ee75b8f07fb16a13b04226b7d0196ddc5db->leave($internal9446c81240ac686a2d5fa1c278a95ee75b8f07fb16a13b04226b7d0196ddc5dbprof);

        
        $internal1edeb993a72791e28eeabb6b655946e37c82230dbd2b8aa3b774be748685a208->leave($internal1edeb993a72791e28eeabb6b655946e37c82230dbd2b8aa3b774be748685a208prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetimewidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetimewidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetimewidget.html.php");
    }
}
