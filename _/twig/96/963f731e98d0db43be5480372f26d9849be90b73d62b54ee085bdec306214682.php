<?php

/* @Framework/Form/attributes.html.php */
class TwigTemplatea7ef6d6055e3cbacb6888671280b9151aa065be55e914b12ab4c335cf18c26ee extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale3df3d32ef96f358b50c60b1c13f089d37e00ebdecb551e956e3ca78616bec48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale3df3d32ef96f358b50c60b1c13f089d37e00ebdecb551e956e3ca78616bec48->enter($internale3df3d32ef96f358b50c60b1c13f089d37e00ebdecb551e956e3ca78616bec48prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $internal071c1288063aa6f1cee7e084d501f1f47891db24583ad9eeb4014e81f2488949 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal071c1288063aa6f1cee7e084d501f1f47891db24583ad9eeb4014e81f2488949->enter($internal071c1288063aa6f1cee7e084d501f1f47891db24583ad9eeb4014e81f2488949prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$v, array(), \$translationdomain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $internale3df3d32ef96f358b50c60b1c13f089d37e00ebdecb551e956e3ca78616bec48->leave($internale3df3d32ef96f358b50c60b1c13f089d37e00ebdecb551e956e3ca78616bec48prof);

        
        $internal071c1288063aa6f1cee7e084d501f1f47891db24583ad9eeb4014e81f2488949->leave($internal071c1288063aa6f1cee7e084d501f1f47891db24583ad9eeb4014e81f2488949prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php foreach (\$attr as \$k => \$v): ?>
<?php if ('placeholder' === \$k || 'title' === \$k): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(false !== \$translationdomain ? \$view['translator']->trans(\$v, array(), \$translationdomain) : \$v)) ?>
<?php elseif (true === \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (false !== \$v): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/attributes.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
