<?php

/* SonataAdminBundle:CRUD:actionbuttons.html.twig */
class TwigTemplate9939490f56f1e7d1c4cf5274980935a472f8d9302b5658c7628d85ae085326ba extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2c4902edbb036262df8243d3ab622a09744b1f52f08823ed11799e008371ad01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2c4902edbb036262df8243d3ab622a09744b1f52f08823ed11799e008371ad01->enter($internal2c4902edbb036262df8243d3ab622a09744b1f52f08823ed11799e008371ad01prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:actionbuttons.html.twig"));

        $internal461e2366e4aa361422474049213c07224c1f93cb7622c25fdc3d5be034d4bc40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal461e2366e4aa361422474049213c07224c1f93cb7622c25fdc3d5be034d4bc40->enter($internal461e2366e4aa361422474049213c07224c1f93cb7622c25fdc3d5be034d4bc40prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:actionbuttons.html.twig"));

        // line 11
        obstart();
        // line 12
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getActionButtons", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 12, $this->getSourceContext()); })()), 1 => ((arraykeyexists("object", $context)) ? (twigdefaultfilter((isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })()), null)) : (null))), "method"));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["item"]) {
            // line 13
            echo "        ";
            if (twiggetattribute($this->env, $this->getSourceContext(), $context["item"], "template", array(), "any", true, true)) {
                // line 14
                echo "            ";
                $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), $context["item"], "template", array()), "SonataAdminBundle:CRUD:actionbuttons.html.twig", 14)->display($context);
                // line 15
                echo "        ";
            }
            // line 16
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['item'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal2c4902edbb036262df8243d3ab622a09744b1f52f08823ed11799e008371ad01->leave($internal2c4902edbb036262df8243d3ab622a09744b1f52f08823ed11799e008371ad01prof);

        
        $internal461e2366e4aa361422474049213c07224c1f93cb7622c25fdc3d5be034d4bc40->leave($internal461e2366e4aa361422474049213c07224c1f93cb7622c25fdc3d5be034d4bc40prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:actionbuttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 16,  51 => 15,  48 => 14,  45 => 13,  27 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% spaceless %}
    {% for item in admin.getActionButtons(action, object|default(null)) %}
        {% if item.template is defined %}
            {% include item.template %}
        {% endif %}
    {% endfor %}
{% endspaceless %}
", "SonataAdminBundle:CRUD:actionbuttons.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/actionbuttons.html.twig");
    }
}
