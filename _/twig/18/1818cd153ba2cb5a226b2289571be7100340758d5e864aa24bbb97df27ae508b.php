<?php

/* TwigBundle:Exception:exception.rdf.twig */
class TwigTemplate5728caa0324b025ca5265cbcdff6da6077035a6aedbeda57bccfe2b688a1e875 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal87bdf4dcf887f321355ee11d546cec8226dc923189db8a546be0934d6bb225e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal87bdf4dcf887f321355ee11d546cec8226dc923189db8a546be0934d6bb225e3->enter($internal87bdf4dcf887f321355ee11d546cec8226dc923189db8a546be0934d6bb225e3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $internal494955d582210e1464689adff11e49417a58359bbd3e8e87e39409783538a343 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal494955d582210e1464689adff11e49417a58359bbd3e8e87e39409783538a343->enter($internal494955d582210e1464689adff11e49417a58359bbd3e8e87e39409783538a343prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twiginclude($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $internal87bdf4dcf887f321355ee11d546cec8226dc923189db8a546be0934d6bb225e3->leave($internal87bdf4dcf887f321355ee11d546cec8226dc923189db8a546be0934d6bb225e3prof);

        
        $internal494955d582210e1464689adff11e49417a58359bbd3e8e87e39409783538a343->leave($internal494955d582210e1464689adff11e49417a58359bbd3e8e87e39409783538a343prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
