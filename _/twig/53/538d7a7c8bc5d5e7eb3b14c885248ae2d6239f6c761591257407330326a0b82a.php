<?php

/* @WebProfiler/Icon/twig.svg */
class TwigTemplate0b4c7624999ed970dc68a6522e40324f04ba132a0082f44188b3d8aee791ca27 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal62606132d2ea53da01bec148c0c02486fc41487e9feb90bed80b1b95ae7e0411 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal62606132d2ea53da01bec148c0c02486fc41487e9feb90bed80b1b95ae7e0411->enter($internal62606132d2ea53da01bec148c0c02486fc41487e9feb90bed80b1b95ae7e0411prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        $internalbdcc9e737d009adfae2b15a3479681166617dcae4aba6f029dae1433aee1ae86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbdcc9e737d009adfae2b15a3479681166617dcae4aba6f029dae1433aee1ae86->enter($internalbdcc9e737d009adfae2b15a3479681166617dcae4aba6f029dae1433aee1ae86prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/twig.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
";
        
        $internal62606132d2ea53da01bec148c0c02486fc41487e9feb90bed80b1b95ae7e0411->leave($internal62606132d2ea53da01bec148c0c02486fc41487e9feb90bed80b1b95ae7e0411prof);

        
        $internalbdcc9e737d009adfae2b15a3479681166617dcae4aba6f029dae1433aee1ae86->leave($internalbdcc9e737d009adfae2b15a3479681166617dcae4aba6f029dae1433aee1ae86prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/twig.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M20.1,1H3.9C2.3,1,1,2.3,1,3.9v16.3C1,21.7,2.3,23,3.9,23h16.3c1.6,0,2.9-1.3,2.9-2.9V3.9
    C23,2.3,21.7,1,20.1,1z M21,20.1c0,0.5-0.4,0.9-0.9,0.9H3.9C3.4,21,3,20.6,3,20.1V3.9C3,3.4,3.4,3,3.9,3h16.3C20.6,3,21,3.4,21,3.9
    V20.1z M5,5h14v3H5V5z M5,10h3v9H5V10z M10,10h9v9h-9V10z\"/>
</svg>
", "@WebProfiler/Icon/twig.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/twig.svg");
    }
}
