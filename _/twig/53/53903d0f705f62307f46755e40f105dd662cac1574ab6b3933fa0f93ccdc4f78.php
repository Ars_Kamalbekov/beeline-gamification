<?php

/* WebProfilerBundle:Collector:twig.html.twig */
class TwigTemplate4a490284336130c6602b9365ddaad59ac38d669585b4f2169bd6936c31c82a24 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:twig.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal72b1f21313746512ffb300fa14379a1974c99116fd94f2fededb943232f819ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal72b1f21313746512ffb300fa14379a1974c99116fd94f2fededb943232f819ce->enter($internal72b1f21313746512ffb300fa14379a1974c99116fd94f2fededb943232f819ceprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:twig.html.twig"));

        $internal8ac219107c37d4d70760aab1f2ce79bef4e6bb46cb6da21f2322881f90dcf031 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8ac219107c37d4d70760aab1f2ce79bef4e6bb46cb6da21f2322881f90dcf031->enter($internal8ac219107c37d4d70760aab1f2ce79bef4e6bb46cb6da21f2322881f90dcf031prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:twig.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal72b1f21313746512ffb300fa14379a1974c99116fd94f2fededb943232f819ce->leave($internal72b1f21313746512ffb300fa14379a1974c99116fd94f2fededb943232f819ceprof);

        
        $internal8ac219107c37d4d70760aab1f2ce79bef4e6bb46cb6da21f2322881f90dcf031->leave($internal8ac219107c37d4d70760aab1f2ce79bef4e6bb46cb6da21f2322881f90dcf031prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internale60756217f84abe92586f925a1340b9dbc43b6af64ec982ad0b8db0430420643 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale60756217f84abe92586f925a1340b9dbc43b6af64ec982ad0b8db0430420643->enter($internale60756217f84abe92586f925a1340b9dbc43b6af64ec982ad0b8db0430420643prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internal08d0d05419bbf84b9170245e4fd38fbf1b24d948ccc29bb26b6658e39ad1f87f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal08d0d05419bbf84b9170245e4fd38fbf1b24d948ccc29bb26b6658e39ad1f87f->enter($internal08d0d05419bbf84b9170245e4fd38fbf1b24d948ccc29bb26b6658e39ad1f87fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        $context["time"] = ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "templatecount", array())) ? (sprintf("%0.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "time", array()))) : ("n/a"));
        // line 5
        echo "    ";
        obstart();
        // line 6
        echo "        ";
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/twig.svg");
        echo "
        <span class=\"sf-toolbar-value\">";
        // line 7
        echo twigescapefilter($this->env, (isset($context["time"]) || arraykeyexists("time", $context) ? $context["time"] : (function () { throw new TwigErrorRuntime('Variable "time" does not exist.', 7, $this->getSourceContext()); })()), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-label\">ms</span>
    ";
        $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 10
        echo "
    ";
        // line 11
        obstart();
        // line 12
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Render Time</b>
            <span>";
        // line 14
        echo twigescapefilter($this->env, (isset($context["time"]) || arraykeyexists("time", $context) ? $context["time"] : (function () { throw new TwigErrorRuntime('Variable "time" does not exist.', 14, $this->getSourceContext()); })()), "html", null, true);
        echo " ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Template Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 18
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "templatecount", array()), "html", null, true);
        echo "</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Block Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 22
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 22, $this->getSourceContext()); })()), "blockcount", array()), "html", null, true);
        echo "</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Macro Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 26
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 26, $this->getSourceContext()); })()), "macrocount", array()), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 30, $this->getSourceContext()); })())));
        echo "
";
        
        $internal08d0d05419bbf84b9170245e4fd38fbf1b24d948ccc29bb26b6658e39ad1f87f->leave($internal08d0d05419bbf84b9170245e4fd38fbf1b24d948ccc29bb26b6658e39ad1f87fprof);

        
        $internale60756217f84abe92586f925a1340b9dbc43b6af64ec982ad0b8db0430420643->leave($internale60756217f84abe92586f925a1340b9dbc43b6af64ec982ad0b8db0430420643prof);

    }

    // line 33
    public function blockmenu($context, array $blocks = array())
    {
        $internal282a6334c85064fc2a7058775f164bfffb66f885a6642683343a10661aec86eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal282a6334c85064fc2a7058775f164bfffb66f885a6642683343a10661aec86eb->enter($internal282a6334c85064fc2a7058775f164bfffb66f885a6642683343a10661aec86ebprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internalffd6e97157bd39ab8961188faaa3d93c548d65bdaf268676bfb0223fb079583f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalffd6e97157bd39ab8961188faaa3d93c548d65bdaf268676bfb0223fb079583f->enter($internalffd6e97157bd39ab8961188faaa3d93c548d65bdaf268676bfb0223fb079583fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 34
        echo "    <span class=\"label\">
        <span class=\"icon\">";
        // line 35
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/twig.svg");
        echo "</span>
        <strong>Twig</strong>
    </span>
";
        
        $internalffd6e97157bd39ab8961188faaa3d93c548d65bdaf268676bfb0223fb079583f->leave($internalffd6e97157bd39ab8961188faaa3d93c548d65bdaf268676bfb0223fb079583fprof);

        
        $internal282a6334c85064fc2a7058775f164bfffb66f885a6642683343a10661aec86eb->leave($internal282a6334c85064fc2a7058775f164bfffb66f885a6642683343a10661aec86ebprof);

    }

    // line 40
    public function blockpanel($context, array $blocks = array())
    {
        $internal55558be892c0edee27bf90a7e2113b540bf83b6ad2a11735f3c6100370ba4728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal55558be892c0edee27bf90a7e2113b540bf83b6ad2a11735f3c6100370ba4728->enter($internal55558be892c0edee27bf90a7e2113b540bf83b6ad2a11735f3c6100370ba4728prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internalcc550fc147943a731698cbebebdbf94736d3e3bb82ef400d68e491b61e2c3902 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcc550fc147943a731698cbebebdbf94736d3e3bb82ef400d68e491b61e2c3902->enter($internalcc550fc147943a731698cbebebdbf94736d3e3bb82ef400d68e491b61e2c3902prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 41
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 41, $this->getSourceContext()); })()), "templatecount", array()) == 0)) {
            // line 42
            echo "        <h2>Twig</h2>

        <div class=\"empty\">
            <p>No Twig templates were rendered for this request.</p>
        </div>
    ";
        } else {
            // line 48
            echo "        <h2>Twig Metrics</h2>

        <div class=\"metrics\">
            <div class=\"metric\">
                <span class=\"value\">";
            // line 52
            echo twigescapefilter($this->env, sprintf("%0.0f", twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 52, $this->getSourceContext()); })()), "time", array())), "html", null, true);
            echo " <span class=\"unit\">ms</span></span>
                <span class=\"label\">Render time</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 57
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 57, $this->getSourceContext()); })()), "templatecount", array()), "html", null, true);
            echo "</span>
                <span class=\"label\">Template calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 62
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 62, $this->getSourceContext()); })()), "blockcount", array()), "html", null, true);
            echo "</span>
                <span class=\"label\">Block calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 67
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 67, $this->getSourceContext()); })()), "macrocount", array()), "html", null, true);
            echo "</span>
                <span class=\"label\">Macro calls</span>
            </div>
        </div>

        <p class=\"help\">
            Render time includes sub-requests rendering time (if any).
        </p>

        <h2>Rendered Templates</h2>

        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Template Name</th>
                    <th scope=\"col\">Render Count</th>
                </tr>
            </thead>
            <tbody>
            ";
            // line 86
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 86, $this->getSourceContext()); })()), "templates", array()));
            foreach ($context['seq'] as $context["template"] => $context["count"]) {
                // line 87
                echo "                <tr>
                    <td>";
                // line 88
                echo twigescapefilter($this->env, $context["template"], "html", null, true);
                echo "</td>
                    <td class=\"font-normal\">";
                // line 89
                echo twigescapefilter($this->env, $context["count"], "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['template'], $context['count'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 92
            echo "            </tbody>
        </table>

        <h2>Rendering Call Graph</h2>

        <div id=\"twig-dump\">
            ";
            // line 98
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 98, $this->getSourceContext()); })()), "htmlcallgraph", array()), "html", null, true);
            echo "
        </div>
    ";
        }
        
        $internalcc550fc147943a731698cbebebdbf94736d3e3bb82ef400d68e491b61e2c3902->leave($internalcc550fc147943a731698cbebebdbf94736d3e3bb82ef400d68e491b61e2c3902prof);

        
        $internal55558be892c0edee27bf90a7e2113b540bf83b6ad2a11735f3c6100370ba4728->leave($internal55558be892c0edee27bf90a7e2113b540bf83b6ad2a11735f3c6100370ba4728prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:twig.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 98,  237 => 92,  228 => 89,  224 => 88,  221 => 87,  217 => 86,  195 => 67,  187 => 62,  179 => 57,  171 => 52,  165 => 48,  157 => 42,  154 => 41,  145 => 40,  131 => 35,  128 => 34,  119 => 33,  107 => 30,  104 => 29,  98 => 26,  91 => 22,  84 => 18,  77 => 14,  73 => 12,  71 => 11,  68 => 10,  62 => 7,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set time = collector.templatecount ? '%0.0f'|format(collector.time) : 'n/a' %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/twig.svg') }}
        <span class=\"sf-toolbar-value\">{{ time }}</span>
        <span class=\"sf-toolbar-label\">ms</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b>Render Time</b>
            <span>{{ time }} ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Template Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.templatecount }}</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Block Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.blockcount }}</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Macro Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.macrocount }}</span>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl }) }}
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/twig.svg') }}</span>
        <strong>Twig</strong>
    </span>
{% endblock %}

{% block panel %}
    {% if collector.templatecount == 0 %}
        <h2>Twig</h2>

        <div class=\"empty\">
            <p>No Twig templates were rendered for this request.</p>
        </div>
    {% else %}
        <h2>Twig Metrics</h2>

        <div class=\"metrics\">
            <div class=\"metric\">
                <span class=\"value\">{{ '%0.0f'|format(collector.time) }} <span class=\"unit\">ms</span></span>
                <span class=\"label\">Render time</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.templatecount }}</span>
                <span class=\"label\">Template calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.blockcount }}</span>
                <span class=\"label\">Block calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.macrocount }}</span>
                <span class=\"label\">Macro calls</span>
            </div>
        </div>

        <p class=\"help\">
            Render time includes sub-requests rendering time (if any).
        </p>

        <h2>Rendered Templates</h2>

        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Template Name</th>
                    <th scope=\"col\">Render Count</th>
                </tr>
            </thead>
            <tbody>
            {% for template, count in collector.templates %}
                <tr>
                    <td>{{ template }}</td>
                    <td class=\"font-normal\">{{ count }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>

        <h2>Rendering Call Graph</h2>

        <div id=\"twig-dump\">
            {{ collector.htmlcallgraph }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:twig.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/twig.html.twig");
    }
}
