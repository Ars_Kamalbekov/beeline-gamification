<?php

/* SonataAdminBundle:CRUD:listaction.html.twig */
class TwigTemplate547a98fc6e4c0f64f0cf9b638dc10bc5a5bb30b2a0a5990e67c67a7fc58f4d37 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listaction.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal34c0ef03c305620229720028e2b7cf094a39c7a56b50ea297ca35ace8e0e5b2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal34c0ef03c305620229720028e2b7cf094a39c7a56b50ea297ca35ace8e0e5b2c->enter($internal34c0ef03c305620229720028e2b7cf094a39c7a56b50ea297ca35ace8e0e5b2cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listaction.html.twig"));

        $internalccaa9686fced392363402da9aaf1b42d547d2ad2e8dbbe203b32dd70419404d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalccaa9686fced392363402da9aaf1b42d547d2ad2e8dbbe203b32dd70419404d1->enter($internalccaa9686fced392363402da9aaf1b42d547d2ad2e8dbbe203b32dd70419404d1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listaction.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal34c0ef03c305620229720028e2b7cf094a39c7a56b50ea297ca35ace8e0e5b2c->leave($internal34c0ef03c305620229720028e2b7cf094a39c7a56b50ea297ca35ace8e0e5b2cprof);

        
        $internalccaa9686fced392363402da9aaf1b42d547d2ad2e8dbbe203b32dd70419404d1->leave($internalccaa9686fced392363402da9aaf1b42d547d2ad2e8dbbe203b32dd70419404d1prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal1305ef92fc0015463aaef236afbc302e82dc7c0772879d37f08c9ff8f716db75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1305ef92fc0015463aaef236afbc302e82dc7c0772879d37f08c9ff8f716db75->enter($internal1305ef92fc0015463aaef236afbc302e82dc7c0772879d37f08c9ff8f716db75prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal17954113cdec1f95886d7f3cede1a0cf9da3b6eb4016f4b3e3de912c65f1e9ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal17954113cdec1f95886d7f3cede1a0cf9da3b6eb4016f4b3e3de912c65f1e9ff->enter($internal17954113cdec1f95886d7f3cede1a0cf9da3b6eb4016f4b3e3de912c65f1e9ffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <div class=\"btn-group\">
        ";
        // line 16
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "actions", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["actions"]) {
            // line 17
            echo "            ";
            $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), $context["actions"], "template", array()), "SonataAdminBundle:CRUD:listaction.html.twig", 17)->display($context);
            // line 18
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['actions'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 19
        echo "    </div>
";
        
        $internal17954113cdec1f95886d7f3cede1a0cf9da3b6eb4016f4b3e3de912c65f1e9ff->leave($internal17954113cdec1f95886d7f3cede1a0cf9da3b6eb4016f4b3e3de912c65f1e9ffprof);

        
        $internal1305ef92fc0015463aaef236afbc302e82dc7c0772879d37f08c9ff8f716db75->leave($internal1305ef92fc0015463aaef236afbc302e82dc7c0772879d37f08c9ff8f716db75prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listaction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 19,  71 => 18,  68 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    <div class=\"btn-group\">
        {% for actions in fielddescription.options.actions %}
            {% include actions.template %}
        {% endfor %}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:listaction.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listaction.html.twig");
    }
}
