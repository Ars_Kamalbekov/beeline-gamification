<?php

/* SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig */
class TwigTemplatee4b10b9bab4d64db10b491684c8415622344cd99623241f1fad65a50cc26cdcd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf4ba23f21e9b74ef1eab5b46db991a0340794bc5604a29097ad9aa5105b3a7c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf4ba23f21e9b74ef1eab5b46db991a0340794bc5604a29097ad9aa5105b3a7c9->enter($internalf4ba23f21e9b74ef1eab5b46db991a0340794bc5604a29097ad9aa5105b3a7c9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig"));

        $internaldd39e4327bb2092ec0a2fe0b0ab6ac1347a54fb19e1a0d589a3452a298fbd349 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldd39e4327bb2092ec0a2fe0b0ab6ac1347a54fb19e1a0d589a3452a298fbd349->enter($internaldd39e4327bb2092ec0a2fe0b0ab6ac1347a54fb19e1a0d589a3452a298fbd349prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig"));

        // line 11
        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 11, $this->getSourceContext()); })()), "fielddescription", array()), "hasassociationadmin", array())) {
            // line 12
            echo "    ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 12, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 13
                echo "        ";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 13, $this->getSourceContext()); })()), "fielddescription", array())), "html", null, true);
                echo "
    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        } else {
            // line 16
            echo "
    <div id=\"fieldcontainer";
            // line 17
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 17, $this->getSourceContext()); })()), "html", null, true);
            echo "\" class=\"field-container\">
        <span id=\"fieldwidget";
            // line 18
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 18, $this->getSourceContext()); })()), "html", null, true);
            echo "\" >
            ";
            // line 19
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 19, $this->getSourceContext()); })()), "edit", array()) == "inline")) {
                // line 20
                echo "                ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 20, $this->getSourceContext()); })()), "inline", array()) == "table")) {
                    // line 21
                    echo "                    ";
                    if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 21, $this->getSourceContext()); })()), "children", array())) > 0)) {
                        // line 22
                        echo "                        ";
                        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetomanyinlinetable.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 22)->display($context);
                        // line 23
                        echo "                    ";
                    }
                    // line 24
                    echo "                ";
                } elseif ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 24, $this->getSourceContext()); })()), "children", array())) > 0)) {
                    // line 25
                    echo "                    ";
                    $context["associationAdmin"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 25, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array());
                    // line 26
                    echo "                    ";
                    $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetomanyinlinetabs.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 26)->display($context);
                    // line 27
                    echo "
                ";
                }
                // line 29
                echo "            ";
            } else {
                // line 30
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 30, $this->getSourceContext()); })()), 'widget');
                echo "
            ";
            }
            // line 32
            echo "
        </span>

        ";
            // line 35
            $context["displaycreatebutton"] = (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 35, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasroute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 36
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 36, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "CREATE"), "method")) &&             // line 37
(isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 37, $this->getSourceContext()); })())) && ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 39
($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "limit", array(), "any", true, true) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(),             // line 40
(isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 40, $this->getSourceContext()); })()), "children", array())) < twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 40, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "limit", array()))));
            // line 42
            echo "
        ";
            // line 43
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 43, $this->getSourceContext()); })()), "edit", array()) == "inline")) {
                // line 44
                echo "
            ";
                // line 45
                if ((isset($context["displaycreatebutton"]) || arraykeyexists("displaycreatebutton", $context) ? $context["displaycreatebutton"] : (function () { throw new TwigErrorRuntime('Variable "displaycreatebutton" does not exist.', 45, $this->getSourceContext()); })())) {
                    // line 46
                    echo "                <span id=\"fieldactions";
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 46, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" >
                    <a
                        href=\"";
                    // line 48
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 48, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 50
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 50, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    // line 51
                    echo "\"
                        onclick=\"return startfieldretrieve";
                    // line 52
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 52, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"";
                    // line 54
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 54, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 54, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                    >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
                    // line 57
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 57, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 57, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                    </a>
                </span>
            ";
                }
                // line 61
                echo "
            ";
                // line 63
                echo "            ";
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "sortable", array(), "any", true, true)) {
                    // line 64
                    echo "                ";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 64, $this->getSourceContext()); })()), "inline", array()) == "table")) {
                        // line 65
                        echo "                    ";
                        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 65)->display($context);
                        // line 66
                        echo "                ";
                    } else {
                        // line 67
                        echo "                    ";
                        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 67)->display($context);
                        // line 68
                        echo "                ";
                    }
                    // line 69
                    echo "            ";
                }
                // line 70
                echo "
            ";
                // line 72
                echo "            ";
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 72)->display($context);
                // line 73
                echo "
        ";
            } else {
                // line 75
                echo "            <span id=\"fieldactions";
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 75, $this->getSourceContext()); })()), "html", null, true);
                echo "\" >
                ";
                // line 76
                if ((isset($context["displaycreatebutton"]) || arraykeyexists("displaycreatebutton", $context) ? $context["displaycreatebutton"] : (function () { throw new TwigErrorRuntime('Variable "displaycreatebutton" does not exist.', 76, $this->getSourceContext()); })())) {
                    // line 77
                    echo "                    <a
                        href=\"";
                    // line 78
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 78, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 80
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 80, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    // line 81
                    echo "\"
                        onclick=\"return startfielddialogformadd";
                    // line 82
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 82, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"";
                    // line 84
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 84, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 84, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                    >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
                    // line 87
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 87, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 87, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 90
                echo "            </span>

            ";
                // line 92
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 92)->display($context);
                // line 93
                echo "
            ";
                // line 94
                $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", 94)->display($context);
                // line 95
                echo "        ";
            }
            // line 96
            echo "    </div>
";
        }
        
        $internalf4ba23f21e9b74ef1eab5b46db991a0340794bc5604a29097ad9aa5105b3a7c9->leave($internalf4ba23f21e9b74ef1eab5b46db991a0340794bc5604a29097ad9aa5105b3a7c9prof);

        
        $internaldd39e4327bb2092ec0a2fe0b0ab6ac1347a54fb19e1a0d589a3452a298fbd349->leave($internaldd39e4327bb2092ec0a2fe0b0ab6ac1347a54fb19e1a0d589a3452a298fbd349prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 96,  218 => 95,  216 => 94,  213 => 93,  211 => 92,  207 => 90,  201 => 87,  195 => 84,  190 => 82,  187 => 81,  185 => 80,  184 => 78,  181 => 77,  179 => 76,  174 => 75,  170 => 73,  167 => 72,  164 => 70,  161 => 69,  158 => 68,  155 => 67,  152 => 66,  149 => 65,  146 => 64,  143 => 63,  140 => 61,  133 => 57,  127 => 54,  122 => 52,  119 => 51,  117 => 50,  116 => 48,  110 => 46,  108 => 45,  105 => 44,  103 => 43,  100 => 42,  98 => 40,  97 => 39,  96 => 37,  95 => 36,  94 => 35,  89 => 32,  83 => 30,  80 => 29,  76 => 27,  73 => 26,  70 => 25,  67 => 24,  64 => 23,  61 => 22,  58 => 21,  55 => 20,  53 => 19,  49 => 18,  45 => 17,  42 => 16,  32 => 13,  27 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% if not sonataadmin.fielddescription.hasassociationadmin %}
    {% for element in value %}
        {{ element|renderrelationelement(sonataadmin.fielddescription) }}
    {% endfor %}
{% else %}

    <div id=\"fieldcontainer{{ id }}\" class=\"field-container\">
        <span id=\"fieldwidget{{ id }}\" >
            {% if sonataadmin.edit == 'inline' %}
                {% if sonataadmin.inline == 'table' %}
                    {% if form.children|length > 0 %}
                        {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetomanyinlinetable.html.twig' %}
                    {% endif %}
                {% elseif form.children|length > 0 %}
                    {% set associationAdmin = sonataadmin.fielddescription.associationadmin %}
                    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetomanyinlinetabs.html.twig' %}

                {% endif %}
            {% else %}
                {{ formwidget(form) }}
            {% endif %}

        </span>

        {% set displaycreatebutton = sonataadmin.fielddescription.associationadmin.hasroute('create')
            and sonataadmin.fielddescription.associationadmin.isGranted('CREATE')
            and btnadd
            and (
                sonataadmin.fielddescription.options.limit is not defined or
                form.children|length < sonataadmin.fielddescription.options.limit
            ) %}

        {% if sonataadmin.edit == 'inline' %}

            {% if displaycreatebutton %}
                <span id=\"fieldactions{{ id }}\" >
                    <a
                        href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl(
                            'create',
                            sonataadmin.fielddescription.getOption('linkparameters', {})
                        ) }}\"
                        onclick=\"return startfieldretrieve{{ id }}(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                    >
                        <i class=\"fa fa-plus-circle\"></i>
                        {{ btnadd|trans({}, btncatalogue) }}
                    </a>
                </span>
            {% endif %}

            {# add code for the sortable options #}
            {% if sonataadmin.fielddescription.options.sortable is defined %}
                {% if sonataadmin.inline == 'table' %}
                    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttable.html.twig' %}
                {% else %}
                    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig' %}
                {% endif %}
            {% endif %}

            {# include association code #}
            {% include 'SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig' %}

        {% else %}
            <span id=\"fieldactions{{ id }}\" >
                {% if displaycreatebutton %}
                    <a
                        href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl(
                            'create',
                            sonataadmin.fielddescription.getOption('linkparameters', {})
                        ) }}\"
                        onclick=\"return startfielddialogformadd{{ id }}(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                    >
                        <i class=\"fa fa-plus-circle\"></i>
                        {{ btnadd|trans({}, btncatalogue) }}
                    </a>
                {% endif %}
            </span>

            {% include 'SonataDoctrineORMAdminBundle:CRUD:editmodal.html.twig' %}

            {% include 'SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig' %}
        {% endif %}
    </div>
{% endif %}
", "SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editormonetomany.html.twig");
    }
}
