<?php

/* FOSUserBundle:Registration:checkemail.html.twig */
class TwigTemplate933790f4c1a99db3c8321d0bbe7bfb4aed2df20e4525979d2684729e66990264 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:checkemail.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9d0ea674c2d562b72d564d8f6c8b7a24f6b6b55fc5c9d39e99b8d6b1701a0966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9d0ea674c2d562b72d564d8f6c8b7a24f6b6b55fc5c9d39e99b8d6b1701a0966->enter($internal9d0ea674c2d562b72d564d8f6c8b7a24f6b6b55fc5c9d39e99b8d6b1701a0966prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkemail.html.twig"));

        $internal01153870dd9b2eb302dfac03497213a3c470637c7375c3f941d22240231eedd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal01153870dd9b2eb302dfac03497213a3c470637c7375c3f941d22240231eedd2->enter($internal01153870dd9b2eb302dfac03497213a3c470637c7375c3f941d22240231eedd2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Registration:checkemail.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal9d0ea674c2d562b72d564d8f6c8b7a24f6b6b55fc5c9d39e99b8d6b1701a0966->leave($internal9d0ea674c2d562b72d564d8f6c8b7a24f6b6b55fc5c9d39e99b8d6b1701a0966prof);

        
        $internal01153870dd9b2eb302dfac03497213a3c470637c7375c3f941d22240231eedd2->leave($internal01153870dd9b2eb302dfac03497213a3c470637c7375c3f941d22240231eedd2prof);

    }

    // line 5
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internaldd998099ef66b34b996013cebc48920c134d7a1b612069e4aa11eecf0be7e98c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldd998099ef66b34b996013cebc48920c134d7a1b612069e4aa11eecf0be7e98c->enter($internaldd998099ef66b34b996013cebc48920c134d7a1b612069e4aa11eecf0be7e98cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal02ac518efb6938a9376a40765c665170f79cad7c0cfaa73feadfc369bca012a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal02ac518efb6938a9376a40765c665170f79cad7c0cfaa73feadfc369bca012a8->enter($internal02ac518efb6938a9376a40765c665170f79cad7c0cfaa73feadfc369bca012a8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 6
        echo "    <p>";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.checkemail", array("%email%" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["user"]) || arraykeyexists("user", $context) ? $context["user"] : (function () { throw new TwigErrorRuntime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $internal02ac518efb6938a9376a40765c665170f79cad7c0cfaa73feadfc369bca012a8->leave($internal02ac518efb6938a9376a40765c665170f79cad7c0cfaa73feadfc369bca012a8prof);

        
        $internaldd998099ef66b34b996013cebc48920c134d7a1b612069e4aa11eecf0be7e98c->leave($internaldd998099ef66b34b996013cebc48920c134d7a1b612069e4aa11eecf0be7e98cprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:checkemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% transdefaultdomain 'FOSUserBundle' %}

{% block fosusercontent %}
    <p>{{ 'registration.checkemail'|trans({'%email%': user.email}) }}</p>
{% endblock fosusercontent %}
", "FOSUserBundle:Registration:checkemail.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Registration/checkemail.html.twig");
    }
}
