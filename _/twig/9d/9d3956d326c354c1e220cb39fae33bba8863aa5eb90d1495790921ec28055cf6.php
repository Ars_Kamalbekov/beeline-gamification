<?php

/* knpmenu.html.twig */
class TwigTemplate29180ec6caf1458c89877403dae8089b3f891d5badfbd72f65d7a8546027fca5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knpmenubase.html.twig", "knpmenu.html.twig", 1);
        $this->blocks = array(
            'compressedroot' => array($this, 'blockcompressedroot'),
            'root' => array($this, 'blockroot'),
            'list' => array($this, 'blocklist'),
            'children' => array($this, 'blockchildren'),
            'item' => array($this, 'blockitem'),
            'linkElement' => array($this, 'blocklinkElement'),
            'spanElement' => array($this, 'blockspanElement'),
            'label' => array($this, 'blocklabel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knpmenubase.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc7c57b8c276952628e7cc104775b9e84c9e842268803b58f965d73354af49607 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc7c57b8c276952628e7cc104775b9e84c9e842268803b58f965d73354af49607->enter($internalc7c57b8c276952628e7cc104775b9e84c9e842268803b58f965d73354af49607prof = new TwigProfilerProfile($this->getTemplateName(), "template", "knpmenu.html.twig"));

        $internal549dc19fc58f4521bf8e445a5a5e1a2869cf93f32a02280b5d9b838f963d6189 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal549dc19fc58f4521bf8e445a5a5e1a2869cf93f32a02280b5d9b838f963d6189->enter($internal549dc19fc58f4521bf8e445a5a5e1a2869cf93f32a02280b5d9b838f963d6189prof = new TwigProfilerProfile($this->getTemplateName(), "template", "knpmenu.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalc7c57b8c276952628e7cc104775b9e84c9e842268803b58f965d73354af49607->leave($internalc7c57b8c276952628e7cc104775b9e84c9e842268803b58f965d73354af49607prof);

        
        $internal549dc19fc58f4521bf8e445a5a5e1a2869cf93f32a02280b5d9b838f963d6189->leave($internal549dc19fc58f4521bf8e445a5a5e1a2869cf93f32a02280b5d9b838f963d6189prof);

    }

    // line 11
    public function blockcompressedroot($context, array $blocks = array())
    {
        $internal86a4c4f6f6c9c7a326d264fe0827987dc25ef6c69768aac27d1eccfbaefdba89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal86a4c4f6f6c9c7a326d264fe0827987dc25ef6c69768aac27d1eccfbaefdba89->enter($internal86a4c4f6f6c9c7a326d264fe0827987dc25ef6c69768aac27d1eccfbaefdba89prof = new TwigProfilerProfile($this->getTemplateName(), "block", "compressedroot"));

        $internal85c6f8872602b0d1c02f216005eb24a42d8d3988fc79270934fcbec73e6e877f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal85c6f8872602b0d1c02f216005eb24a42d8d3988fc79270934fcbec73e6e877f->enter($internal85c6f8872602b0d1c02f216005eb24a42d8d3988fc79270934fcbec73e6e877fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "compressedroot"));

        // line 12
        obstart();
        // line 13
        $this->displayBlock("root", $context, $blocks);
        echo "
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal85c6f8872602b0d1c02f216005eb24a42d8d3988fc79270934fcbec73e6e877f->leave($internal85c6f8872602b0d1c02f216005eb24a42d8d3988fc79270934fcbec73e6e877fprof);

        
        $internal86a4c4f6f6c9c7a326d264fe0827987dc25ef6c69768aac27d1eccfbaefdba89->leave($internal86a4c4f6f6c9c7a326d264fe0827987dc25ef6c69768aac27d1eccfbaefdba89prof);

    }

    // line 17
    public function blockroot($context, array $blocks = array())
    {
        $internalcf0519888d974ad718a6f29cc71e06b7f2457d13e23e246b61b21d01b82d5205 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcf0519888d974ad718a6f29cc71e06b7f2457d13e23e246b61b21d01b82d5205->enter($internalcf0519888d974ad718a6f29cc71e06b7f2457d13e23e246b61b21d01b82d5205prof = new TwigProfilerProfile($this->getTemplateName(), "block", "root"));

        $internal379b7536b1b4ff37e035ed4a27fd43aebe7fcbc4ce4d31232f07f252222fc76f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal379b7536b1b4ff37e035ed4a27fd43aebe7fcbc4ce4d31232f07f252222fc76f->enter($internal379b7536b1b4ff37e035ed4a27fd43aebe7fcbc4ce4d31232f07f252222fc76fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "root"));

        // line 18
        $context["listAttributes"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 18, $this->getSourceContext()); })()), "childrenAttributes", array());
        // line 19
        $this->displayBlock("list", $context, $blocks);
        
        $internal379b7536b1b4ff37e035ed4a27fd43aebe7fcbc4ce4d31232f07f252222fc76f->leave($internal379b7536b1b4ff37e035ed4a27fd43aebe7fcbc4ce4d31232f07f252222fc76fprof);

        
        $internalcf0519888d974ad718a6f29cc71e06b7f2457d13e23e246b61b21d01b82d5205->leave($internalcf0519888d974ad718a6f29cc71e06b7f2457d13e23e246b61b21d01b82d5205prof);

    }

    // line 22
    public function blocklist($context, array $blocks = array())
    {
        $internal6946f69916b2e4b4cd79254f6608c97245d91891b66b10a4cc0d053017e317d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6946f69916b2e4b4cd79254f6608c97245d91891b66b10a4cc0d053017e317d5->enter($internal6946f69916b2e4b4cd79254f6608c97245d91891b66b10a4cc0d053017e317d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        $internalea4fad6ce685dd7b12751bd96973e54f16aa34bcef530d658765dabcb8e21ee3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalea4fad6ce685dd7b12751bd96973e54f16aa34bcef530d658765dabcb8e21ee3->enter($internalea4fad6ce685dd7b12751bd96973e54f16aa34bcef530d658765dabcb8e21ee3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        // line 23
        if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 23, $this->getSourceContext()); })()), "hasChildren", array()) &&  !(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 23, $this->getSourceContext()); })()), "depth", array()) === 0)) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 23, $this->getSourceContext()); })()), "displayChildren", array()))) {
            // line 24
            echo "    ";
            $context["knpmenu"] = $this;
            // line 25
            echo "    <ul";
            echo $context["knpmenu"]->macroattributes((isset($context["listAttributes"]) || arraykeyexists("listAttributes", $context) ? $context["listAttributes"] : (function () { throw new TwigErrorRuntime('Variable "listAttributes" does not exist.', 25, $this->getSourceContext()); })()));
            echo ">
        ";
            // line 26
            $this->displayBlock("children", $context, $blocks);
            echo "
    </ul>
";
        }
        
        $internalea4fad6ce685dd7b12751bd96973e54f16aa34bcef530d658765dabcb8e21ee3->leave($internalea4fad6ce685dd7b12751bd96973e54f16aa34bcef530d658765dabcb8e21ee3prof);

        
        $internal6946f69916b2e4b4cd79254f6608c97245d91891b66b10a4cc0d053017e317d5->leave($internal6946f69916b2e4b4cd79254f6608c97245d91891b66b10a4cc0d053017e317d5prof);

    }

    // line 31
    public function blockchildren($context, array $blocks = array())
    {
        $internal3de7fbbc13a1777e4d79f4974089bc63e7ccc761b736d1449c2f05872c17d5b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3de7fbbc13a1777e4d79f4974089bc63e7ccc761b736d1449c2f05872c17d5b4->enter($internal3de7fbbc13a1777e4d79f4974089bc63e7ccc761b736d1449c2f05872c17d5b4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "children"));

        $internal49d52f733cb86990e919023c118bd0187d59015ef913f5856635e51fbdf79831 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal49d52f733cb86990e919023c118bd0187d59015ef913f5856635e51fbdf79831->enter($internal49d52f733cb86990e919023c118bd0187d59015ef913f5856635e51fbdf79831prof = new TwigProfilerProfile($this->getTemplateName(), "block", "children"));

        // line 33
        $context["currentOptions"] = (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 33, $this->getSourceContext()); })());
        // line 34
        $context["currentItem"] = (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 34, $this->getSourceContext()); })());
        // line 36
        if ( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 36, $this->getSourceContext()); })()), "depth", array()))) {
            // line 37
            $context["options"] = twigarraymerge((isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 37, $this->getSourceContext()); })()), array("depth" => (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentOptions"]) || arraykeyexists("currentOptions", $context) ? $context["currentOptions"] : (function () { throw new TwigErrorRuntime('Variable "currentOptions" does not exist.', 37, $this->getSourceContext()); })()), "depth", array()) - 1)));
        }
        // line 40
        if (( !(null === twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 40, $this->getSourceContext()); })()), "matchingDepth", array())) && (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 40, $this->getSourceContext()); })()), "matchingDepth", array()) > 0))) {
            // line 41
            $context["options"] = twigarraymerge((isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 41, $this->getSourceContext()); })()), array("matchingDepth" => (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentOptions"]) || arraykeyexists("currentOptions", $context) ? $context["currentOptions"] : (function () { throw new TwigErrorRuntime('Variable "currentOptions" does not exist.', 41, $this->getSourceContext()); })()), "matchingDepth", array()) - 1)));
        }
        // line 43
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentItem"]) || arraykeyexists("currentItem", $context) ? $context["currentItem"] : (function () { throw new TwigErrorRuntime('Variable "currentItem" does not exist.', 43, $this->getSourceContext()); })()), "children", array()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["item"]) {
            // line 44
            echo "    ";
            $this->displayBlock("item", $context, $blocks);
            echo "
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['item'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 47
        $context["item"] = (isset($context["currentItem"]) || arraykeyexists("currentItem", $context) ? $context["currentItem"] : (function () { throw new TwigErrorRuntime('Variable "currentItem" does not exist.', 47, $this->getSourceContext()); })());
        // line 48
        $context["options"] = (isset($context["currentOptions"]) || arraykeyexists("currentOptions", $context) ? $context["currentOptions"] : (function () { throw new TwigErrorRuntime('Variable "currentOptions" does not exist.', 48, $this->getSourceContext()); })());
        
        $internal49d52f733cb86990e919023c118bd0187d59015ef913f5856635e51fbdf79831->leave($internal49d52f733cb86990e919023c118bd0187d59015ef913f5856635e51fbdf79831prof);

        
        $internal3de7fbbc13a1777e4d79f4974089bc63e7ccc761b736d1449c2f05872c17d5b4->leave($internal3de7fbbc13a1777e4d79f4974089bc63e7ccc761b736d1449c2f05872c17d5b4prof);

    }

    // line 51
    public function blockitem($context, array $blocks = array())
    {
        $internald8e34ca4fc6a9c2ad815af82330065736216fb6c649326b37053af7f9659f7a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald8e34ca4fc6a9c2ad815af82330065736216fb6c649326b37053af7f9659f7a4->enter($internald8e34ca4fc6a9c2ad815af82330065736216fb6c649326b37053af7f9659f7a4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        $internal436b89989db51f32c85a3f6ade5fdf160102721b6cc9c3620bfa2028391a910a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal436b89989db51f32c85a3f6ade5fdf160102721b6cc9c3620bfa2028391a910a->enter($internal436b89989db51f32c85a3f6ade5fdf160102721b6cc9c3620bfa2028391a910aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        // line 52
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 52, $this->getSourceContext()); })()), "displayed", array())) {
            // line 54
            $context["classes"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 54, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 54, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 55
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 55, $this->getSourceContext()); })()), "isCurrent", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 55, $this->getSourceContext()); })())), "method")) {
                // line 56
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 56, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 56, $this->getSourceContext()); })()), "currentClass", array())));
            } elseif (twiggetattribute($this->env, $this->getSourceContext(),             // line 57
(isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 57, $this->getSourceContext()); })()), "isAncestor", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 57, $this->getSourceContext()); })()), 1 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 57, $this->getSourceContext()); })()), "matchingDepth", array())), "method")) {
                // line 58
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 58, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 58, $this->getSourceContext()); })()), "ancestorClass", array())));
            }
            // line 60
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 60, $this->getSourceContext()); })()), "actsLikeFirst", array())) {
                // line 61
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 61, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 61, $this->getSourceContext()); })()), "firstClass", array())));
            }
            // line 63
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 63, $this->getSourceContext()); })()), "actsLikeLast", array())) {
                // line 64
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 64, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 64, $this->getSourceContext()); })()), "lastClass", array())));
            }
            // line 66
            echo "
    ";
            // line 68
            echo "    ";
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 68, $this->getSourceContext()); })()), "hasChildren", array()) &&  !(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 68, $this->getSourceContext()); })()), "depth", array()) === 0))) {
                // line 69
                echo "        ";
                if (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 69, $this->getSourceContext()); })()), "branchclass", array())) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 69, $this->getSourceContext()); })()), "displayChildren", array()))) {
                    // line 70
                    $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 70, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 70, $this->getSourceContext()); })()), "branchclass", array())));
                    // line 71
                    echo "        ";
                }
                // line 72
                echo "    ";
            } elseif ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 72, $this->getSourceContext()); })()), "leafclass", array()))) {
                // line 73
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 73, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 73, $this->getSourceContext()); })()), "leafclass", array())));
            }
            // line 76
            $context["attributes"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 76, $this->getSourceContext()); })()), "attributes", array());
            // line 77
            if ( !twigtestempty((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 77, $this->getSourceContext()); })()))) {
                // line 78
                $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 78, $this->getSourceContext()); })()), array("class" => twigjoinfilter((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 78, $this->getSourceContext()); })()), " ")));
            }
            // line 81
            echo "    ";
            $context["knpmenu"] = $this;
            // line 82
            echo "    <li";
            echo $context["knpmenu"]->macroattributes((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 82, $this->getSourceContext()); })()));
            echo ">";
            // line 83
            if (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 83, $this->getSourceContext()); })()), "uri", array())) && ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 83, $this->getSourceContext()); })()), "isCurrent", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 83, $this->getSourceContext()); })())), "method") || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 83, $this->getSourceContext()); })()), "currentAsLink", array())))) {
                // line 84
                echo "        ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 86
                echo "        ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 89
            $context["childrenClasses"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 89, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 89, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 90
            $context["childrenClasses"] = twigarraymerge((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 90, $this->getSourceContext()); })()), array(0 => ("menulevel" . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 90, $this->getSourceContext()); })()), "level", array()))));
            // line 91
            $context["listAttributes"] = twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 91, $this->getSourceContext()); })()), "childrenAttributes", array()), array("class" => twigjoinfilter((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 91, $this->getSourceContext()); })()), " ")));
            // line 92
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    </li>
";
        }
        
        $internal436b89989db51f32c85a3f6ade5fdf160102721b6cc9c3620bfa2028391a910a->leave($internal436b89989db51f32c85a3f6ade5fdf160102721b6cc9c3620bfa2028391a910aprof);

        
        $internald8e34ca4fc6a9c2ad815af82330065736216fb6c649326b37053af7f9659f7a4->leave($internald8e34ca4fc6a9c2ad815af82330065736216fb6c649326b37053af7f9659f7a4prof);

    }

    // line 97
    public function blocklinkElement($context, array $blocks = array())
    {
        $internalec04496e80bd7436bd009c2cc69d3ebc8b8a778026c3fbe5ca35c1e73ee6f3e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalec04496e80bd7436bd009c2cc69d3ebc8b8a778026c3fbe5ca35c1e73ee6f3e9->enter($internalec04496e80bd7436bd009c2cc69d3ebc8b8a778026c3fbe5ca35c1e73ee6f3e9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        $internal6e02515f26ebc0a24cabf6a0a7013e68419ca05eeace0bc17935ccc5b77c3d6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6e02515f26ebc0a24cabf6a0a7013e68419ca05eeace0bc17935ccc5b77c3d6b->enter($internal6e02515f26ebc0a24cabf6a0a7013e68419ca05eeace0bc17935ccc5b77c3d6bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        $context["knpmenu"] = $this;
        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 97, $this->getSourceContext()); })()), "uri", array()), "html", null, true);
        echo "\"";
        echo $context["knpmenu"]->macroattributes(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 97, $this->getSourceContext()); })()), "linkAttributes", array()));
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>";
        
        $internal6e02515f26ebc0a24cabf6a0a7013e68419ca05eeace0bc17935ccc5b77c3d6b->leave($internal6e02515f26ebc0a24cabf6a0a7013e68419ca05eeace0bc17935ccc5b77c3d6bprof);

        
        $internalec04496e80bd7436bd009c2cc69d3ebc8b8a778026c3fbe5ca35c1e73ee6f3e9->leave($internalec04496e80bd7436bd009c2cc69d3ebc8b8a778026c3fbe5ca35c1e73ee6f3e9prof);

    }

    // line 99
    public function blockspanElement($context, array $blocks = array())
    {
        $internal2185ab0ef2aa30b28b24e3f6df6730cecfc76ffb24887a7b99caba91f2615cfe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2185ab0ef2aa30b28b24e3f6df6730cecfc76ffb24887a7b99caba91f2615cfe->enter($internal2185ab0ef2aa30b28b24e3f6df6730cecfc76ffb24887a7b99caba91f2615cfeprof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        $internala19a25dbea1207b052dbfacbabfc9da059bf8c8dfafb293bf0a313098637fb06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala19a25dbea1207b052dbfacbabfc9da059bf8c8dfafb293bf0a313098637fb06->enter($internala19a25dbea1207b052dbfacbabfc9da059bf8c8dfafb293bf0a313098637fb06prof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        $context["knpmenu"] = $this;
        echo "<span";
        echo $context["knpmenu"]->macroattributes(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 99, $this->getSourceContext()); })()), "labelAttributes", array()));
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>";
        
        $internala19a25dbea1207b052dbfacbabfc9da059bf8c8dfafb293bf0a313098637fb06->leave($internala19a25dbea1207b052dbfacbabfc9da059bf8c8dfafb293bf0a313098637fb06prof);

        
        $internal2185ab0ef2aa30b28b24e3f6df6730cecfc76ffb24887a7b99caba91f2615cfe->leave($internal2185ab0ef2aa30b28b24e3f6df6730cecfc76ffb24887a7b99caba91f2615cfeprof);

    }

    // line 101
    public function blocklabel($context, array $blocks = array())
    {
        $internal788517bb409329de59b39e44ef04dd8c8f36231691f34dcd76538476b0fb009b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal788517bb409329de59b39e44ef04dd8c8f36231691f34dcd76538476b0fb009b->enter($internal788517bb409329de59b39e44ef04dd8c8f36231691f34dcd76538476b0fb009bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal7bb74b754c213f98f61a0384d9691e56a0061d2e3147b4ff0d1a194fb0428bb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7bb74b754c213f98f61a0384d9691e56a0061d2e3147b4ff0d1a194fb0428bb9->enter($internal7bb74b754c213f98f61a0384d9691e56a0061d2e3147b4ff0d1a194fb0428bb9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 101, $this->getSourceContext()); })()), "allowsafelabels", array()) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 101, $this->getSourceContext()); })()), "getExtra", array(0 => "safelabel", 1 => false), "method"))) {
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 101, $this->getSourceContext()); })()), "label", array());
        } else {
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 101, $this->getSourceContext()); })()), "label", array()), "html", null, true);
        }
        
        $internal7bb74b754c213f98f61a0384d9691e56a0061d2e3147b4ff0d1a194fb0428bb9->leave($internal7bb74b754c213f98f61a0384d9691e56a0061d2e3147b4ff0d1a194fb0428bb9prof);

        
        $internal788517bb409329de59b39e44ef04dd8c8f36231691f34dcd76538476b0fb009b->leave($internal788517bb409329de59b39e44ef04dd8c8f36231691f34dcd76538476b0fb009bprof);

    }

    // line 3
    public function macroattributes($attributes = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "attributes" => $attributes,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal6ee60f34dda0df2218b7d6fec60d2926970679e1bd60514a5d9f1bf1264c1f32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal6ee60f34dda0df2218b7d6fec60d2926970679e1bd60514a5d9f1bf1264c1f32->enter($internal6ee60f34dda0df2218b7d6fec60d2926970679e1bd60514a5d9f1bf1264c1f32prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "attributes"));

            $internal9f94da987bc4465d8af182b01901cbd9fd4a532f2d0acd5af6778735c650c043 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal9f94da987bc4465d8af182b01901cbd9fd4a532f2d0acd5af6778735c650c043->enter($internal9f94da987bc4465d8af182b01901cbd9fd4a532f2d0acd5af6778735c650c043prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "attributes"));

            // line 4
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 4, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["name"] => $context["value"]) {
                // line 5
                if (( !(null === $context["value"]) &&  !($context["value"] === false))) {
                    // line 6
                    echo sprintf(" %s=\"%s\"", $context["name"], ((($context["value"] === true)) ? (twigescapefilter($this->env, $context["name"])) : (twigescapefilter($this->env, $context["value"]))));
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['value'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            
            $internal9f94da987bc4465d8af182b01901cbd9fd4a532f2d0acd5af6778735c650c043->leave($internal9f94da987bc4465d8af182b01901cbd9fd4a532f2d0acd5af6778735c650c043prof);

            
            $internal6ee60f34dda0df2218b7d6fec60d2926970679e1bd60514a5d9f1bf1264c1f32->leave($internal6ee60f34dda0df2218b7d6fec60d2926970679e1bd60514a5d9f1bf1264c1f32prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "knpmenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 6,  383 => 5,  379 => 4,  361 => 3,  339 => 101,  316 => 99,  291 => 97,  276 => 92,  274 => 91,  272 => 90,  270 => 89,  266 => 86,  262 => 84,  260 => 83,  256 => 82,  253 => 81,  250 => 78,  248 => 77,  246 => 76,  243 => 73,  240 => 72,  237 => 71,  235 => 70,  232 => 69,  229 => 68,  226 => 66,  223 => 64,  221 => 63,  218 => 61,  216 => 60,  213 => 58,  211 => 57,  209 => 56,  207 => 55,  205 => 54,  203 => 52,  194 => 51,  184 => 48,  182 => 47,  165 => 44,  148 => 43,  145 => 41,  143 => 40,  140 => 37,  138 => 36,  136 => 34,  134 => 33,  125 => 31,  111 => 26,  106 => 25,  103 => 24,  101 => 23,  92 => 22,  82 => 19,  80 => 18,  71 => 17,  58 => 13,  56 => 12,  47 => 11,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'knpmenubase.html.twig' %}

{% macro attributes(attributes) %}
{% for name, value in attributes %}
    {%- if value is not none and value is not same as(false) -%}
        {{- ' %s=\"%s\"'|format(name, value is same as(true) ? name|e : value|e)|raw -}}
    {%- endif -%}
{%- endfor -%}
{% endmacro %}

{% block compressedroot %}
{% spaceless %}
{{ block('root') }}
{% endspaceless %}
{% endblock %}

{% block root %}
{% set listAttributes = item.childrenAttributes %}
{{ block('list') -}}
{% endblock %}

{% block list %}
{% if item.hasChildren and options.depth is not same as(0) and item.displayChildren %}
    {% import self as knpmenu %}
    <ul{{ knpmenu.attributes(listAttributes) }}>
        {{ block('children') }}
    </ul>
{% endif %}
{% endblock %}

{% block children %}
{# save current variables #}
{% set currentOptions = options %}
{% set currentItem = item %}
{# update the depth for children #}
{% if options.depth is not none %}
{% set options = options|merge({'depth': currentOptions.depth - 1}) %}
{% endif %}
{# update the matchingDepth for children #}
{% if options.matchingDepth is not none and options.matchingDepth > 0 %}
{% set options = options|merge({'matchingDepth': currentOptions.matchingDepth - 1}) %}
{% endif %}
{% for item in currentItem.children %}
    {{ block('item') }}
{% endfor %}
{# restore current variables #}
{% set item = currentItem %}
{% set options = currentOptions %}
{% endblock %}

{% block item %}
{% if item.displayed %}
{# building the class of the item #}
    {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}
    {%- if matcher.isCurrent(item) %}
        {%- set classes = classes|merge([options.currentClass]) %}
    {%- elseif matcher.isAncestor(item, options.matchingDepth) %}
        {%- set classes = classes|merge([options.ancestorClass]) %}
    {%- endif %}
    {%- if item.actsLikeFirst %}
        {%- set classes = classes|merge([options.firstClass]) %}
    {%- endif %}
    {%- if item.actsLikeLast %}
        {%- set classes = classes|merge([options.lastClass]) %}
    {%- endif %}

    {# Mark item as \"leaf\" (no children) or as \"branch\" (has children that are displayed) #}
    {% if item.hasChildren and options.depth is not same as(0) %}
        {% if options.branchclass is not empty and item.displayChildren %}
            {%- set classes = classes|merge([options.branchclass]) %}
        {% endif %}
    {% elseif options.leafclass is not empty %}
        {%- set classes = classes|merge([options.leafclass]) %}
    {%- endif %}

    {%- set attributes = item.attributes %}
    {%- if classes is not empty %}
        {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
    {%- endif %}
{# displaying the item #}
    {% import self as knpmenu %}
    <li{{ knpmenu.attributes(attributes) }}>
        {%- if item.uri is not empty and (not matcher.isCurrent(item) or options.currentAsLink) %}
        {{ block('linkElement') }}
        {%- else %}
        {{ block('spanElement') }}
        {%- endif %}
{# render the list of children#}
        {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}
        {%- set childrenClasses = childrenClasses|merge(['menulevel' ~ item.level]) %}
        {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}
        {{ block('list') }}
    </li>
{% endif %}
{% endblock %}

{% block linkElement %}{% import self as knpmenu %}<a href=\"{{ item.uri }}\"{{ knpmenu.attributes(item.linkAttributes) }}>{{ block('label') }}</a>{% endblock %}

{% block spanElement %}{% import self as knpmenu %}<span{{ knpmenu.attributes(item.labelAttributes) }}>{{ block('label') }}</span>{% endblock %}

{% block label %}{% if options.allowsafelabels and item.getExtra('safelabel', false) %}{{ item.label|raw }}{% else %}{{ item.label }}{% endif %}{% endblock %}
", "knpmenu.html.twig", "/var/www/html/beeline-gamification/vendor/knplabs/knp-menu/src/Knp/Menu/Resources/views/knpmenu.html.twig");
    }
}
