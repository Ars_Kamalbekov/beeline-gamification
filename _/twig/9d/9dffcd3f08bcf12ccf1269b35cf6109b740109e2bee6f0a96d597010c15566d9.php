<?php

/* knpmenubase.html.twig */
class TwigTemplate12d8247652c42208fb6d4c9ec8b9f2706237db182b7b5ce7a2403dcbe4bac48d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal217ff641fb5306b12a8df982c2d19b8a7da2ae5641c96471748553928062d7ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal217ff641fb5306b12a8df982c2d19b8a7da2ae5641c96471748553928062d7ff->enter($internal217ff641fb5306b12a8df982c2d19b8a7da2ae5641c96471748553928062d7ffprof = new TwigProfilerProfile($this->getTemplateName(), "template", "knpmenubase.html.twig"));

        $internald8a2e2dec93883893cf278a853d91274726d9e618ee0aa33c04308353df1daf1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald8a2e2dec93883893cf278a853d91274726d9e618ee0aa33c04308353df1daf1->enter($internald8a2e2dec93883893cf278a853d91274726d9e618ee0aa33c04308353df1daf1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "knpmenubase.html.twig"));

        // line 1
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 1, $this->getSourceContext()); })()), "compressed", array())) {
            $this->displayBlock("compressedroot", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $internal217ff641fb5306b12a8df982c2d19b8a7da2ae5641c96471748553928062d7ff->leave($internal217ff641fb5306b12a8df982c2d19b8a7da2ae5641c96471748553928062d7ffprof);

        
        $internald8a2e2dec93883893cf278a853d91274726d9e618ee0aa33c04308353df1daf1->leave($internald8a2e2dec93883893cf278a853d91274726d9e618ee0aa33c04308353df1daf1prof);

    }

    public function getTemplateName()
    {
        return "knpmenubase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% if options.compressed %}{{ block('compressedroot') }}{% else %}{{ block('root') }}{% endif %}
", "knpmenubase.html.twig", "/var/www/html/beeline-gamification/vendor/knplabs/knp-menu/src/Knp/Menu/Resources/views/knpmenubase.html.twig");
    }
}
