<?php

/* SonataAdminBundle:Block:blockstats.html.twig */
class TwigTemplate1241999c60f3cb3a4727559cb2bda85e3da5d49dbf727b948addd022f80a77a2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataAdminBundle:Block:blockstats.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal23e5069bdd991df78a3cbd05da1811af0ac7467bbfb1a3a94795990ea0e33253 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal23e5069bdd991df78a3cbd05da1811af0ac7467bbfb1a3a94795990ea0e33253->enter($internal23e5069bdd991df78a3cbd05da1811af0ac7467bbfb1a3a94795990ea0e33253prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockstats.html.twig"));

        $internalaedaea3a198b177bfd841b8ce888b1e1c3187e2ef535ff1b553a39a660e256a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaedaea3a198b177bfd841b8ce888b1e1c3187e2ef535ff1b553a39a660e256a2->enter($internalaedaea3a198b177bfd841b8ce888b1e1c3187e2ef535ff1b553a39a660e256a2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Block:blockstats.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal23e5069bdd991df78a3cbd05da1811af0ac7467bbfb1a3a94795990ea0e33253->leave($internal23e5069bdd991df78a3cbd05da1811af0ac7467bbfb1a3a94795990ea0e33253prof);

        
        $internalaedaea3a198b177bfd841b8ce888b1e1c3187e2ef535ff1b553a39a660e256a2->leave($internalaedaea3a198b177bfd841b8ce888b1e1c3187e2ef535ff1b553a39a660e256a2prof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal2058aee0067946c3baf8976d78fc75c0c45ccfe53dd32a5dfa54771dd13562af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2058aee0067946c3baf8976d78fc75c0c45ccfe53dd32a5dfa54771dd13562af->enter($internal2058aee0067946c3baf8976d78fc75c0c45ccfe53dd32a5dfa54771dd13562afprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internalcac4fae466d9846a00bf5720d47f4ebecccd585eb6db0d38762d4daeeb1f4faf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcac4fae466d9846a00bf5720d47f4ebecccd585eb6db0d38762d4daeeb1f4faf->enter($internalcac4fae466d9846a00bf5720d47f4ebecccd585eb6db0d38762d4daeeb1f4fafprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <!-- small box -->
    <div class=\"small-box ";
        // line 16
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 16, $this->getSourceContext()); })()), "color", array()), "html", null, true);
        echo "\">
        <div class=\"inner\">
            <h3>";
        // line 18
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["pager"]) || arraykeyexists("pager", $context) ? $context["pager"] : (function () { throw new TwigErrorRuntime('Variable "pager" does not exist.', 18, $this->getSourceContext()); })()), "count", array(), "method"), "html", null, true);
        echo "</h3>

            <p>";
        // line 20
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 20, $this->getSourceContext()); })()), "text", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 20, $this->getSourceContext()); })()), "translationDomain", array())), "html", null, true);
        echo "</p>
        </div>
        <div class=\"icon\">
            <i class=\"fa ";
        // line 23
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 23, $this->getSourceContext()); })()), "icon", array()), "html", null, true);
        echo "\"></i>
        </div>
        <a href=\"";
        // line 25
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => array("filter" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["settings"]) || arraykeyexists("settings", $context) ? $context["settings"] : (function () { throw new TwigErrorRuntime('Variable "settings" does not exist.', 25, $this->getSourceContext()); })()), "filters", array()))), "method"), "html", null, true);
        echo "\" class=\"small-box-footer\">
            ";
        // line 26
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("statsviewmore", array(), "SonataAdminBundle"), "html", null, true);
        echo " <i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i>
        </a>
    </div>
";
        
        $internalcac4fae466d9846a00bf5720d47f4ebecccd585eb6db0d38762d4daeeb1f4faf->leave($internalcac4fae466d9846a00bf5720d47f4ebecccd585eb6db0d38762d4daeeb1f4fafprof);

        
        $internal2058aee0067946c3baf8976d78fc75c0c45ccfe53dd32a5dfa54771dd13562af->leave($internal2058aee0067946c3baf8976d78fc75c0c45ccfe53dd32a5dfa54771dd13562afprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Block:blockstats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 26,  72 => 25,  67 => 23,  61 => 20,  56 => 18,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    <!-- small box -->
    <div class=\"small-box {{ settings.color }}\">
        <div class=\"inner\">
            <h3>{{ pager.count() }}</h3>

            <p>{{ settings.text|trans({}, admin.translationDomain) }}</p>
        </div>
        <div class=\"icon\">
            <i class=\"fa {{ settings.icon }}\"></i>
        </div>
        <a href=\"{{ admin.generateUrl('list', {filter: settings.filters}) }}\" class=\"small-box-footer\">
            {{ 'statsviewmore'|trans({}, 'SonataAdminBundle') }} <i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i>
        </a>
    </div>
{% endblock %}
", "SonataAdminBundle:Block:blockstats.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Block/blockstats.html.twig");
    }
}
