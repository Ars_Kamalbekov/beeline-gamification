<?php

/* TwigBundle:Exception:exception.json.twig */
class TwigTemplate38ef70ac9aef682180b743ce0e2c8f4eaafc023303036168e6f1abc26ba40934 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcbb2daa9cd1bc495cdb9c1b95ff85aa88d2a0e2fe8b51fa11304b35119c65663 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcbb2daa9cd1bc495cdb9c1b95ff85aa88d2a0e2fe8b51fa11304b35119c65663->enter($internalcbb2daa9cd1bc495cdb9c1b95ff85aa88d2a0e2fe8b51fa11304b35119c65663prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $internal1ded3ff71482c11230358c07adc7f1289a1d5df5f6750a79ce11aef8e0ca063c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1ded3ff71482c11230358c07adc7f1289a1d5df5f6750a79ce11aef8e0ca063c->enter($internal1ded3ff71482c11230358c07adc7f1289a1d5df5f6750a79ce11aef8e0ca063cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo jsonencode(array("error" => array("code" => (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 1, $this->getSourceContext()); })()), "exception" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "toarray", array()))));
        echo "
";
        
        $internalcbb2daa9cd1bc495cdb9c1b95ff85aa88d2a0e2fe8b51fa11304b35119c65663->leave($internalcbb2daa9cd1bc495cdb9c1b95ff85aa88d2a0e2fe8b51fa11304b35119c65663prof);

        
        $internal1ded3ff71482c11230358c07adc7f1289a1d5df5f6750a79ce11aef8e0ca063c->leave($internal1ded3ff71482c11230358c07adc7f1289a1d5df5f6750a79ce11aef8e0ca063cprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{{ { 'error': { 'code': statuscode, 'message': statustext, 'exception': exception.toarray } }|jsonencode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
