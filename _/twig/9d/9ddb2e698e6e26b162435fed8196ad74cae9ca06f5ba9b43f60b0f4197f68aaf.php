<?php

/* SonataAdminBundle:CRUD:editarray.html.twig */
class TwigTemplate94c2da762256582aa0c248659b8c7c1cb6be7b8c2bd796452e8c8febba687d92 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:editarray.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb1cf5e2932d235c92c1b182bce1468b1610385607dd824f6e76ab0fcb5b37025 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb1cf5e2932d235c92c1b182bce1468b1610385607dd824f6e76ab0fcb5b37025->enter($internalb1cf5e2932d235c92c1b182bce1468b1610385607dd824f6e76ab0fcb5b37025prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editarray.html.twig"));

        $internalaa7ae77d1bd031ba1b6f0ef9519677a1042a156c0ecced53fab5247982a79b96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaa7ae77d1bd031ba1b6f0ef9519677a1042a156c0ecced53fab5247982a79b96->enter($internalaa7ae77d1bd031ba1b6f0ef9519677a1042a156c0ecced53fab5247982a79b96prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:editarray.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb1cf5e2932d235c92c1b182bce1468b1610385607dd824f6e76ab0fcb5b37025->leave($internalb1cf5e2932d235c92c1b182bce1468b1610385607dd824f6e76ab0fcb5b37025prof);

        
        $internalaa7ae77d1bd031ba1b6f0ef9519677a1042a156c0ecced53fab5247982a79b96->leave($internalaa7ae77d1bd031ba1b6f0ef9519677a1042a156c0ecced53fab5247982a79b96prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal944de884176869628ec06760525d11fe499f336146dea52f2a7f14452753bd47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal944de884176869628ec06760525d11fe499f336146dea52f2a7f14452753bd47->enter($internal944de884176869628ec06760525d11fe499f336146dea52f2a7f14452753bd47prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalba191cc92e45ee7730878cef7f70a7c38998bd743c044b211673a72a5f9aa7fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalba191cc92e45ee7730878cef7f70a7c38998bd743c044b211673a72a5f9aa7fe->enter($internalba191cc92e45ee7730878cef7f70a7c38998bd743c044b211673a72a5f9aa7feprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <span class=\"edit\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["fieldelement"]) || arraykeyexists("fieldelement", $context) ? $context["fieldelement"] : (function () { throw new TwigErrorRuntime('Variable "fieldelement" does not exist.', 16, $this->getSourceContext()); })()), 'widget', array("attr" => array("class" => "title")));
        echo "
    </span>
";
        
        $internalba191cc92e45ee7730878cef7f70a7c38998bd743c044b211673a72a5f9aa7fe->leave($internalba191cc92e45ee7730878cef7f70a7c38998bd743c044b211673a72a5f9aa7feprof);

        
        $internal944de884176869628ec06760525d11fe499f336146dea52f2a7f14452753bd47->leave($internal944de884176869628ec06760525d11fe499f336146dea52f2a7f14452753bd47prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:editarray.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block field %}
    <span class=\"edit\">
        {{ formwidget(fieldelement, {'attr': {'class' : 'title'}}) }}
    </span>
{% endblock %}
", "SonataAdminBundle:CRUD:editarray.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/editarray.html.twig");
    }
}
