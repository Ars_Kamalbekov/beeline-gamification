<?php

/* SonataAdminBundle:CRUD:acl.html.twig */
class TwigTemplatec80a62b1123ff2ebc835a9f26c9c476916619c976253f0794e805c9709abdf4b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseacl.html.twig", "SonataAdminBundle:CRUD:acl.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseacl.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal659bfb15740f11ef7e4f884fe4c175c7ed4648fc775df31fce39cf2bdf447217 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal659bfb15740f11ef7e4f884fe4c175c7ed4648fc775df31fce39cf2bdf447217->enter($internal659bfb15740f11ef7e4f884fe4c175c7ed4648fc775df31fce39cf2bdf447217prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $internal721ee4be88a76ccf6df119dc505f262426b3359d4292174150385eee8fd336e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal721ee4be88a76ccf6df119dc505f262426b3359d4292174150385eee8fd336e2->enter($internal721ee4be88a76ccf6df119dc505f262426b3359d4292174150385eee8fd336e2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal659bfb15740f11ef7e4f884fe4c175c7ed4648fc775df31fce39cf2bdf447217->leave($internal659bfb15740f11ef7e4f884fe4c175c7ed4648fc775df31fce39cf2bdf447217prof);

        
        $internal721ee4be88a76ccf6df119dc505f262426b3359d4292174150385eee8fd336e2->leave($internal721ee4be88a76ccf6df119dc505f262426b3359d4292174150385eee8fd336e2prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseacl.html.twig' %}
", "SonataAdminBundle:CRUD:acl.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/acl.html.twig");
    }
}
