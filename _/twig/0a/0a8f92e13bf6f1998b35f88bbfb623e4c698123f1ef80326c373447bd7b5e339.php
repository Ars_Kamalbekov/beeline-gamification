<?php

/* @Framework/Form/checkboxwidget.html.php */
class TwigTemplate6abc6594bb747e91d008a2dd87618209e9ad7931b833321c9f40566ce8d4d936 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal5349e24dac5ed51e732fd7a9b80a623974a4f0cf1f17a0aa3e0b838a23b9555f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5349e24dac5ed51e732fd7a9b80a623974a4f0cf1f17a0aa3e0b838a23b9555f->enter($internal5349e24dac5ed51e732fd7a9b80a623974a4f0cf1f17a0aa3e0b838a23b9555fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/checkboxwidget.html.php"));

        $internala81f561d0503512548a244249ae920dcd85501c2042a51dcb1a5cfb749ada67e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala81f561d0503512548a244249ae920dcd85501c2042a51dcb1a5cfb749ada67e->enter($internala81f561d0503512548a244249ae920dcd85501c2042a51dcb1a5cfb749ada67eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/checkboxwidget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $internal5349e24dac5ed51e732fd7a9b80a623974a4f0cf1f17a0aa3e0b838a23b9555f->leave($internal5349e24dac5ed51e732fd7a9b80a623974a4f0cf1f17a0aa3e0b838a23b9555fprof);

        
        $internala81f561d0503512548a244249ae920dcd85501c2042a51dcb1a5cfb749ada67e->leave($internala81f561d0503512548a244249ae920dcd85501c2042a51dcb1a5cfb749ada67eprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkboxwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widgetattributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkboxwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkboxwidget.html.php");
    }
}
