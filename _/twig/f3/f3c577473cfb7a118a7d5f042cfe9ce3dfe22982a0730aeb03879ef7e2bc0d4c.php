<?php

/* SonataAdminBundle:Form:formadminfields.html.twig */
class TwigTemplate69d57f2dfb9a2ca9e661735fadd79d5fcd1d50205baae5739cb2f792a32bd4cc extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("formdivlayout.html.twig", "SonataAdminBundle:Form:formadminfields.html.twig", 12);
        $this->blocks = array(
            'formerrors' => array($this, 'blockformerrors'),
            'sonatahelp' => array($this, 'blocksonatahelp'),
            'formwidget' => array($this, 'blockformwidget'),
            'formwidgetsimple' => array($this, 'blockformwidgetsimple'),
            'textareawidget' => array($this, 'blocktextareawidget'),
            'moneywidget' => array($this, 'blockmoneywidget'),
            'percentwidget' => array($this, 'blockpercentwidget'),
            'checkboxwidget' => array($this, 'blockcheckboxwidget'),
            'radiowidget' => array($this, 'blockradiowidget'),
            'formlabel' => array($this, 'blockformlabel'),
            'checkboxlabel' => array($this, 'blockcheckboxlabel'),
            'radiolabel' => array($this, 'blockradiolabel'),
            'checkboxradiolabel' => array($this, 'blockcheckboxradiolabel'),
            'choicewidgetexpanded' => array($this, 'blockchoicewidgetexpanded'),
            'choicewidgetcollapsed' => array($this, 'blockchoicewidgetcollapsed'),
            'datewidget' => array($this, 'blockdatewidget'),
            'timewidget' => array($this, 'blocktimewidget'),
            'datetimewidget' => array($this, 'blockdatetimewidget'),
            'formrow' => array($this, 'blockformrow'),
            'checkboxrow' => array($this, 'blockcheckboxrow'),
            'radiorow' => array($this, 'blockradiorow'),
            'sonatatypenativecollectionwidgetrow' => array($this, 'blocksonatatypenativecollectionwidgetrow'),
            'sonatatypenativecollectionwidget' => array($this, 'blocksonatatypenativecollectionwidget'),
            'sonatatypeimmutablearraywidget' => array($this, 'blocksonatatypeimmutablearraywidget'),
            'sonatatypeimmutablearraywidgetrow' => array($this, 'blocksonatatypeimmutablearraywidgetrow'),
            'sonatatypemodelautocompletewidget' => array($this, 'blocksonatatypemodelautocompletewidget'),
            'sonatatypechoicefieldmaskwidget' => array($this, 'blocksonatatypechoicefieldmaskwidget'),
            'sonatatypechoicemultiplesortable' => array($this, 'blocksonatatypechoicemultiplesortable'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "formdivlayout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaleda515b98820e1d322b390200860e9935f0ea045f768120eea0cee3ec4aa9834 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleda515b98820e1d322b390200860e9935f0ea045f768120eea0cee3ec4aa9834->enter($internaleda515b98820e1d322b390200860e9935f0ea045f768120eea0cee3ec4aa9834prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:formadminfields.html.twig"));

        $internalba0f3448d6b3d8cb4334a3b855b45311bd548d317c79a99afe110f188336098d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalba0f3448d6b3d8cb4334a3b855b45311bd548d317c79a99afe110f188336098d->enter($internalba0f3448d6b3d8cb4334a3b855b45311bd548d317c79a99afe110f188336098dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Form:formadminfields.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internaleda515b98820e1d322b390200860e9935f0ea045f768120eea0cee3ec4aa9834->leave($internaleda515b98820e1d322b390200860e9935f0ea045f768120eea0cee3ec4aa9834prof);

        
        $internalba0f3448d6b3d8cb4334a3b855b45311bd548d317c79a99afe110f188336098d->leave($internalba0f3448d6b3d8cb4334a3b855b45311bd548d317c79a99afe110f188336098dprof);

    }

    // line 14
    public function blockformerrors($context, array $blocks = array())
    {
        $internal9781a1e47f7c89d1ed7bf8f16f8dd0ed6094b4072a31585d6296cf5ea7aca7f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9781a1e47f7c89d1ed7bf8f16f8dd0ed6094b4072a31585d6296cf5ea7aca7f1->enter($internal9781a1e47f7c89d1ed7bf8f16f8dd0ed6094b4072a31585d6296cf5ea7aca7f1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formerrors"));

        $internal2b599903e6176139c86f75ac6428f57527e9261a42bc193abb48cff8aebb19ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2b599903e6176139c86f75ac6428f57527e9261a42bc193abb48cff8aebb19ed->enter($internal2b599903e6176139c86f75ac6428f57527e9261a42bc193abb48cff8aebb19edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formerrors"));

        // line 15
        if ((twiglengthfilter($this->env, (isset($context["errors"]) || arraykeyexists("errors", $context) ? $context["errors"] : (function () { throw new TwigErrorRuntime('Variable "errors" does not exist.', 15, $this->getSourceContext()); })())) > 0)) {
            // line 16
            echo "        ";
            if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), "parent", array())) {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 17
            echo "            <ul class=\"list-unstyled\">
                ";
            // line 18
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["errors"]) || arraykeyexists("errors", $context) ? $context["errors"] : (function () { throw new TwigErrorRuntime('Variable "errors" does not exist.', 18, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["error"]) {
                // line 19
                echo "                    <li><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["error"], "message", array()), "html", null, true);
                echo "</li>
                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['error'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 21
            echo "            </ul>
        ";
            // line 22
            if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 22, $this->getSourceContext()); })()), "parent", array())) {
                echo "</div>";
            }
            // line 23
            echo "    ";
        }
        
        $internal2b599903e6176139c86f75ac6428f57527e9261a42bc193abb48cff8aebb19ed->leave($internal2b599903e6176139c86f75ac6428f57527e9261a42bc193abb48cff8aebb19edprof);

        
        $internal9781a1e47f7c89d1ed7bf8f16f8dd0ed6094b4072a31585d6296cf5ea7aca7f1->leave($internal9781a1e47f7c89d1ed7bf8f16f8dd0ed6094b4072a31585d6296cf5ea7aca7f1prof);

    }

    // line 26
    public function blocksonatahelp($context, array $blocks = array())
    {
        $internalf67d319acc9fdf321e5b6ca092f34ceb6d58cfa61041d970e7a5fae68726bcbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf67d319acc9fdf321e5b6ca092f34ceb6d58cfa61041d970e7a5fae68726bcbf->enter($internalf67d319acc9fdf321e5b6ca092f34ceb6d58cfa61041d970e7a5fae68726bcbfprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatahelp"));

        $internalb22c0827e5eb0486e4db84c957a8dd7196ae4504605283aaad1b68f79bc164ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb22c0827e5eb0486e4db84c957a8dd7196ae4504605283aaad1b68f79bc164ed->enter($internalb22c0827e5eb0486e4db84c957a8dd7196ae4504605283aaad1b68f79bc164edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatahelp"));

        // line 27
        obstart();
        // line 28
        if ((arraykeyexists("sonatahelp", $context) && (isset($context["sonatahelp"]) || arraykeyexists("sonatahelp", $context) ? $context["sonatahelp"] : (function () { throw new TwigErrorRuntime('Variable "sonatahelp" does not exist.', 28, $this->getSourceContext()); })()))) {
            // line 29
            echo "    <span class=\"help-block sonata-ba-field-widget-help\">";
            echo (isset($context["sonatahelp"]) || arraykeyexists("sonatahelp", $context) ? $context["sonatahelp"] : (function () { throw new TwigErrorRuntime('Variable "sonatahelp" does not exist.', 29, $this->getSourceContext()); })());
            echo "</span>
";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internalb22c0827e5eb0486e4db84c957a8dd7196ae4504605283aaad1b68f79bc164ed->leave($internalb22c0827e5eb0486e4db84c957a8dd7196ae4504605283aaad1b68f79bc164edprof);

        
        $internalf67d319acc9fdf321e5b6ca092f34ceb6d58cfa61041d970e7a5fae68726bcbf->leave($internalf67d319acc9fdf321e5b6ca092f34ceb6d58cfa61041d970e7a5fae68726bcbfprof);

    }

    // line 34
    public function blockformwidget($context, array $blocks = array())
    {
        $internalb39ffd5a2d883fb6043e6b237e2c1f37c29acfae5d1f38082980236656afd882 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb39ffd5a2d883fb6043e6b237e2c1f37c29acfae5d1f38082980236656afd882->enter($internalb39ffd5a2d883fb6043e6b237e2c1f37c29acfae5d1f38082980236656afd882prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidget"));

        $internalddcb9a9924932a8c76d688ea13504f079f7d9282b6998236fc83ae8c2539468b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalddcb9a9924932a8c76d688ea13504f079f7d9282b6998236fc83ae8c2539468b->enter($internalddcb9a9924932a8c76d688ea13504f079f7d9282b6998236fc83ae8c2539468bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidget"));

        // line 35
        $this->displayParentBlock("formwidget", $context, $blocks);
        echo "
    ";
        // line 36
        $this->displayBlock("sonatahelp", $context, $blocks);
        
        $internalddcb9a9924932a8c76d688ea13504f079f7d9282b6998236fc83ae8c2539468b->leave($internalddcb9a9924932a8c76d688ea13504f079f7d9282b6998236fc83ae8c2539468bprof);

        
        $internalb39ffd5a2d883fb6043e6b237e2c1f37c29acfae5d1f38082980236656afd882->leave($internalb39ffd5a2d883fb6043e6b237e2c1f37c29acfae5d1f38082980236656afd882prof);

    }

    // line 39
    public function blockformwidgetsimple($context, array $blocks = array())
    {
        $internal15204db7466d67390291b8f1ac38480d921c7bb558d4320eb3f7422c8d4b3c5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal15204db7466d67390291b8f1ac38480d921c7bb558d4320eb3f7422c8d4b3c5d->enter($internal15204db7466d67390291b8f1ac38480d921c7bb558d4320eb3f7422c8d4b3c5dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidgetsimple"));

        $internalda4034decc2add90e51db9f6df30351f8d708308ade62d514cf6c82c2b32cffa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalda4034decc2add90e51db9f6df30351f8d708308ade62d514cf6c82c2b32cffa->enter($internalda4034decc2add90e51db9f6df30351f8d708308ade62d514cf6c82c2b32cffaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formwidgetsimple"));

        // line 40
        echo "    ";
        $context["type"] = ((arraykeyexists("type", $context)) ? (twigdefaultfilter((isset($context["type"]) || arraykeyexists("type", $context) ? $context["type"] : (function () { throw new TwigErrorRuntime('Variable "type" does not exist.', 40, $this->getSourceContext()); })()), "text")) : ("text"));
        // line 41
        echo "    ";
        if (((isset($context["type"]) || arraykeyexists("type", $context) ? $context["type"] : (function () { throw new TwigErrorRuntime('Variable "type" does not exist.', 41, $this->getSourceContext()); })()) != "file")) {
            // line 42
            echo "        ";
            $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 42, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
            // line 43
            echo "    ";
        }
        // line 44
        echo "    ";
        $this->displayParentBlock("formwidgetsimple", $context, $blocks);
        echo "
";
        
        $internalda4034decc2add90e51db9f6df30351f8d708308ade62d514cf6c82c2b32cffa->leave($internalda4034decc2add90e51db9f6df30351f8d708308ade62d514cf6c82c2b32cffaprof);

        
        $internal15204db7466d67390291b8f1ac38480d921c7bb558d4320eb3f7422c8d4b3c5d->leave($internal15204db7466d67390291b8f1ac38480d921c7bb558d4320eb3f7422c8d4b3c5dprof);

    }

    // line 47
    public function blocktextareawidget($context, array $blocks = array())
    {
        $internal34fa1a6f239017918ea1e92f572ffdd771b568960759c6ae6912c6c2488a75d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal34fa1a6f239017918ea1e92f572ffdd771b568960759c6ae6912c6c2488a75d2->enter($internal34fa1a6f239017918ea1e92f572ffdd771b568960759c6ae6912c6c2488a75d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "textareawidget"));

        $internalc72ffd7ec23a058acad74980d139ab15fdc3a26f327b39a1a639cf06f774cbb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc72ffd7ec23a058acad74980d139ab15fdc3a26f327b39a1a639cf06f774cbb3->enter($internalc72ffd7ec23a058acad74980d139ab15fdc3a26f327b39a1a639cf06f774cbb3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "textareawidget"));

        // line 48
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 48, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
        // line 49
        echo "    ";
        $this->displayParentBlock("textareawidget", $context, $blocks);
        echo "
";
        
        $internalc72ffd7ec23a058acad74980d139ab15fdc3a26f327b39a1a639cf06f774cbb3->leave($internalc72ffd7ec23a058acad74980d139ab15fdc3a26f327b39a1a639cf06f774cbb3prof);

        
        $internal34fa1a6f239017918ea1e92f572ffdd771b568960759c6ae6912c6c2488a75d2->leave($internal34fa1a6f239017918ea1e92f572ffdd771b568960759c6ae6912c6c2488a75d2prof);

    }

    // line 52
    public function blockmoneywidget($context, array $blocks = array())
    {
        $internal9172768ccb3217a2a994d3f7a05f63fe35c29ab9ccb68aa19b6f53486d87bfa3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9172768ccb3217a2a994d3f7a05f63fe35c29ab9ccb68aa19b6f53486d87bfa3->enter($internal9172768ccb3217a2a994d3f7a05f63fe35c29ab9ccb68aa19b6f53486d87bfa3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "moneywidget"));

        $internalb50be2533130966fb4ce179414e7788d8fd66ba9c4aa4d82e982b9da05ca182a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb50be2533130966fb4ce179414e7788d8fd66ba9c4aa4d82e982b9da05ca182a->enter($internalb50be2533130966fb4ce179414e7788d8fd66ba9c4aa4d82e982b9da05ca182aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "moneywidget"));

        // line 53
        if (((isset($context["moneypattern"]) || arraykeyexists("moneypattern", $context) ? $context["moneypattern"] : (function () { throw new TwigErrorRuntime('Variable "moneypattern" does not exist.', 53, $this->getSourceContext()); })()) == "{{ widget }}")) {
            // line 54
            $this->displayBlock("formwidgetsimple", $context, $blocks);
        } else {
            // line 56
            echo "        ";
            $context["currencySymbol"] = twigtrimfilter(twigreplacefilter((isset($context["moneypattern"]) || arraykeyexists("moneypattern", $context) ? $context["moneypattern"] : (function () { throw new TwigErrorRuntime('Variable "moneypattern" does not exist.', 56, $this->getSourceContext()); })()), array("{{ widget }}" => "")));
            // line 57
            echo "        ";
            if (pregmatch("/^{{ widget }}/", (isset($context["moneypattern"]) || arraykeyexists("moneypattern", $context) ? $context["moneypattern"] : (function () { throw new TwigErrorRuntime('Variable "moneypattern" does not exist.', 57, $this->getSourceContext()); })()))) {
                // line 58
                echo "            <div class=\"input-group\">";
                // line 59
                $this->displayBlock("formwidgetsimple", $context, $blocks);
                // line 60
                echo "<span class=\"input-group-addon\">";
                echo twigescapefilter($this->env, (isset($context["currencySymbol"]) || arraykeyexists("currencySymbol", $context) ? $context["currencySymbol"] : (function () { throw new TwigErrorRuntime('Variable "currencySymbol" does not exist.', 60, $this->getSourceContext()); })()), "html", null, true);
                echo "</span>
            </div>
        ";
            } elseif (pregmatch("/{{ widget }}\$/",             // line 62
(isset($context["moneypattern"]) || arraykeyexists("moneypattern", $context) ? $context["moneypattern"] : (function () { throw new TwigErrorRuntime('Variable "moneypattern" does not exist.', 62, $this->getSourceContext()); })()))) {
                // line 63
                echo "            <div class=\"input-group\">
                <span class=\"input-group-addon\">";
                // line 64
                echo twigescapefilter($this->env, (isset($context["currencySymbol"]) || arraykeyexists("currencySymbol", $context) ? $context["currencySymbol"] : (function () { throw new TwigErrorRuntime('Variable "currencySymbol" does not exist.', 64, $this->getSourceContext()); })()), "html", null, true);
                echo "</span>";
                // line 65
                $this->displayBlock("formwidgetsimple", $context, $blocks);
                // line 66
                echo "</div>
        ";
            }
            // line 68
            echo "    ";
        }
        
        $internalb50be2533130966fb4ce179414e7788d8fd66ba9c4aa4d82e982b9da05ca182a->leave($internalb50be2533130966fb4ce179414e7788d8fd66ba9c4aa4d82e982b9da05ca182aprof);

        
        $internal9172768ccb3217a2a994d3f7a05f63fe35c29ab9ccb68aa19b6f53486d87bfa3->leave($internal9172768ccb3217a2a994d3f7a05f63fe35c29ab9ccb68aa19b6f53486d87bfa3prof);

    }

    // line 71
    public function blockpercentwidget($context, array $blocks = array())
    {
        $internal7e83ce317e0cb183e9f63a689b41e992a404dcfbd94df6f5a72367db5df28a04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7e83ce317e0cb183e9f63a689b41e992a404dcfbd94df6f5a72367db5df28a04->enter($internal7e83ce317e0cb183e9f63a689b41e992a404dcfbd94df6f5a72367db5df28a04prof = new TwigProfilerProfile($this->getTemplateName(), "block", "percentwidget"));

        $internal5d685f4d8ff8dc40db78c91ef527015e25c08f91d832eac1f6a92b3cc92b7ee6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5d685f4d8ff8dc40db78c91ef527015e25c08f91d832eac1f6a92b3cc92b7ee6->enter($internal5d685f4d8ff8dc40db78c91ef527015e25c08f91d832eac1f6a92b3cc92b7ee6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "percentwidget"));

        // line 72
        echo "    ";
        obstart();
        // line 73
        echo "        ";
        $context["type"] = ((arraykeyexists("type", $context)) ? (twigdefaultfilter((isset($context["type"]) || arraykeyexists("type", $context) ? $context["type"] : (function () { throw new TwigErrorRuntime('Variable "type" does not exist.', 73, $this->getSourceContext()); })()), "text")) : ("text"));
        // line 74
        echo "        <div class=\"input-group\">
            ";
        // line 75
        $this->displayBlock("formwidgetsimple", $context, $blocks);
        echo "
            <span class=\"input-group-addon\">%</span>
        </div>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal5d685f4d8ff8dc40db78c91ef527015e25c08f91d832eac1f6a92b3cc92b7ee6->leave($internal5d685f4d8ff8dc40db78c91ef527015e25c08f91d832eac1f6a92b3cc92b7ee6prof);

        
        $internal7e83ce317e0cb183e9f63a689b41e992a404dcfbd94df6f5a72367db5df28a04->leave($internal7e83ce317e0cb183e9f63a689b41e992a404dcfbd94df6f5a72367db5df28a04prof);

    }

    // line 81
    public function blockcheckboxwidget($context, array $blocks = array())
    {
        $internalc0fa95edb12c6b2cc9af714d0ac947e14e1f5efa8f8173f89705eea7c3c50e0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc0fa95edb12c6b2cc9af714d0ac947e14e1f5efa8f8173f89705eea7c3c50e0c->enter($internalc0fa95edb12c6b2cc9af714d0ac947e14e1f5efa8f8173f89705eea7c3c50e0cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxwidget"));

        $internal09aca24ddc98d23324a92b7d2f17ad3cbb612fc2774caf9672e42e78d91c7a79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal09aca24ddc98d23324a92b7d2f17ad3cbb612fc2774caf9672e42e78d91c7a79->enter($internal09aca24ddc98d23324a92b7d2f17ad3cbb612fc2774caf9672e42e78d91c7a79prof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxwidget"));

        // line 82
        $context["parentlabelclass"] = ((arraykeyexists("parentlabelclass", $context)) ? (twigdefaultfilter((isset($context["parentlabelclass"]) || arraykeyexists("parentlabelclass", $context) ? $context["parentlabelclass"] : (function () { throw new TwigErrorRuntime('Variable "parentlabelclass" does not exist.', 82, $this->getSourceContext()); })()), "")) : (""));
        // line 83
        if (twiginfilter("checkbox-inline", (isset($context["parentlabelclass"]) || arraykeyexists("parentlabelclass", $context) ? $context["parentlabelclass"] : (function () { throw new TwigErrorRuntime('Variable "parentlabelclass" does not exist.', 83, $this->getSourceContext()); })()))) {
            // line 84
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 84, $this->getSourceContext()); })()), 'label', array("widget" => $this->renderParentBlock("checkboxwidget", $context, $blocks)));
        } else {
            // line 86
            echo "<div class=\"checkbox\">";
            // line 87
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 87, $this->getSourceContext()); })()), 'label', array("widget" => $this->renderParentBlock("checkboxwidget", $context, $blocks)));
            // line 88
            echo "</div>";
        }
        
        $internal09aca24ddc98d23324a92b7d2f17ad3cbb612fc2774caf9672e42e78d91c7a79->leave($internal09aca24ddc98d23324a92b7d2f17ad3cbb612fc2774caf9672e42e78d91c7a79prof);

        
        $internalc0fa95edb12c6b2cc9af714d0ac947e14e1f5efa8f8173f89705eea7c3c50e0c->leave($internalc0fa95edb12c6b2cc9af714d0ac947e14e1f5efa8f8173f89705eea7c3c50e0cprof);

    }

    // line 92
    public function blockradiowidget($context, array $blocks = array())
    {
        $internalcb48d1cbcf5304374dd36f5cd3514c2a0fc3c41bb73b83f1606d4e6af8746e5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcb48d1cbcf5304374dd36f5cd3514c2a0fc3c41bb73b83f1606d4e6af8746e5a->enter($internalcb48d1cbcf5304374dd36f5cd3514c2a0fc3c41bb73b83f1606d4e6af8746e5aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiowidget"));

        $internal6d44071169b665130e71bbbd5c39cce4fe1d2ba16f9da737aeb15afa3a0e4d3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6d44071169b665130e71bbbd5c39cce4fe1d2ba16f9da737aeb15afa3a0e4d3e->enter($internal6d44071169b665130e71bbbd5c39cce4fe1d2ba16f9da737aeb15afa3a0e4d3eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiowidget"));

        // line 93
        $context["parentlabelclass"] = ((arraykeyexists("parentlabelclass", $context)) ? (twigdefaultfilter((isset($context["parentlabelclass"]) || arraykeyexists("parentlabelclass", $context) ? $context["parentlabelclass"] : (function () { throw new TwigErrorRuntime('Variable "parentlabelclass" does not exist.', 93, $this->getSourceContext()); })()), "")) : (""));
        // line 94
        if (twiginfilter("radio-inline", (isset($context["parentlabelclass"]) || arraykeyexists("parentlabelclass", $context) ? $context["parentlabelclass"] : (function () { throw new TwigErrorRuntime('Variable "parentlabelclass" does not exist.', 94, $this->getSourceContext()); })()))) {
            // line 95
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 95, $this->getSourceContext()); })()), 'label', array("widget" => $this->renderParentBlock("radiowidget", $context, $blocks)));
        } else {
            // line 97
            echo "<div class=\"radio\">";
            // line 98
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 98, $this->getSourceContext()); })()), 'label', array("widget" => $this->renderParentBlock("radiowidget", $context, $blocks)));
            // line 99
            echo "</div>";
        }
        
        $internal6d44071169b665130e71bbbd5c39cce4fe1d2ba16f9da737aeb15afa3a0e4d3e->leave($internal6d44071169b665130e71bbbd5c39cce4fe1d2ba16f9da737aeb15afa3a0e4d3eprof);

        
        $internalcb48d1cbcf5304374dd36f5cd3514c2a0fc3c41bb73b83f1606d4e6af8746e5a->leave($internalcb48d1cbcf5304374dd36f5cd3514c2a0fc3c41bb73b83f1606d4e6af8746e5aprof);

    }

    // line 104
    public function blockformlabel($context, array $blocks = array())
    {
        $internalaa32df9ea9ba263494a353e3b66dfd0ec178446b3774bf961e8d0454e7de82e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalaa32df9ea9ba263494a353e3b66dfd0ec178446b3774bf961e8d0454e7de82e9->enter($internalaa32df9ea9ba263494a353e3b66dfd0ec178446b3774bf961e8d0454e7de82e9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formlabel"));

        $internal558f4d74a47bd782315826584d13183960d3a9c229150cc11d6efeceab199115 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal558f4d74a47bd782315826584d13183960d3a9c229150cc11d6efeceab199115->enter($internal558f4d74a47bd782315826584d13183960d3a9c229150cc11d6efeceab199115prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formlabel"));

        // line 105
        obstart();
        // line 106
        echo "    ";
        if (( !((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 106, $this->getSourceContext()); })()) === false) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 106, $this->getSourceContext()); })()), "options", array()), "formtype", array(), "array") == "horizontal"))) {
            // line 107
            echo "        ";
            $context["labelclass"] = "col-sm-3";
            // line 108
            echo "    ";
        }
        // line 109
        echo "
    ";
        // line 110
        $context["labelclass"] = (((arraykeyexists("labelclass", $context)) ? (twigdefaultfilter((isset($context["labelclass"]) || arraykeyexists("labelclass", $context) ? $context["labelclass"] : (function () { throw new TwigErrorRuntime('Variable "labelclass" does not exist.', 110, $this->getSourceContext()); })()), "")) : ("")) . " control-label");
        // line 111
        echo "
    ";
        // line 112
        if ( !((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 112, $this->getSourceContext()); })()) === false)) {
            // line 113
            echo "        ";
            $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 113, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array()), "")) : ("")) . (isset($context["labelclass"]) || arraykeyexists("labelclass", $context) ? $context["labelclass"] : (function () { throw new TwigErrorRuntime('Variable "labelclass" does not exist.', 113, $this->getSourceContext()); })()))));
            // line 114
            echo "
        ";
            // line 115
            if ( !(isset($context["compound"]) || arraykeyexists("compound", $context) ? $context["compound"] : (function () { throw new TwigErrorRuntime('Variable "compound" does not exist.', 115, $this->getSourceContext()); })())) {
                // line 116
                echo "            ";
                $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 116, $this->getSourceContext()); })()), array("for" => (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 116, $this->getSourceContext()); })())));
                // line 117
                echo "        ";
            }
            // line 118
            echo "        ";
            if ((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 118, $this->getSourceContext()); })())) {
                // line 119
                echo "            ";
                $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 119, $this->getSourceContext()); })()), array("class" => twigtrimfilter((((twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array()), "")) : ("")) . " required"))));
                // line 120
                echo "        ";
            }
            // line 121
            echo "
        ";
            // line 122
            if (twigtestempty((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 122, $this->getSourceContext()); })()))) {
                // line 123
                if ((arraykeyexists("labelformat", $context) &&  !twigtestempty((isset($context["labelformat"]) || arraykeyexists("labelformat", $context) ? $context["labelformat"] : (function () { throw new TwigErrorRuntime('Variable "labelformat" does not exist.', 123, $this->getSourceContext()); })())))) {
                    // line 124
                    $context["label"] = twigreplacefilter((isset($context["labelformat"]) || arraykeyexists("labelformat", $context) ? $context["labelformat"] : (function () { throw new TwigErrorRuntime('Variable "labelformat" does not exist.', 124, $this->getSourceContext()); })()), array("%name%" =>                     // line 125
(isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 125, $this->getSourceContext()); })()), "%id%" =>                     // line 126
(isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 126, $this->getSourceContext()); })())));
                } else {
                    // line 129
                    $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 129, $this->getSourceContext()); })()));
                }
            }
            // line 132
            echo "
        <label";
            // line 133
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 133, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twigescapefilter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twigescapefilter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['attrname'], $context['attrvalue'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            echo ">
            ";
            // line 134
            if (((isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 134, $this->getSourceContext()); })()) === false)) {
                // line 135
                echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 135, $this->getSourceContext()); })()), "html", null, true);
            } elseif ( !twiggetattribute($this->env, $this->getSourceContext(),             // line 136
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 136, $this->getSourceContext()); })()), "admin", array())) {
                // line 137
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 137, $this->getSourceContext()); })()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 137, $this->getSourceContext()); })())), "html", null, true);
            } else {
                // line 139
                echo "                ";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 139, $this->getSourceContext()); })()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 139, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 139, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 139, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                echo "
            ";
            }
            // line 141
            echo "        </label>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal558f4d74a47bd782315826584d13183960d3a9c229150cc11d6efeceab199115->leave($internal558f4d74a47bd782315826584d13183960d3a9c229150cc11d6efeceab199115prof);

        
        $internalaa32df9ea9ba263494a353e3b66dfd0ec178446b3774bf961e8d0454e7de82e9->leave($internalaa32df9ea9ba263494a353e3b66dfd0ec178446b3774bf961e8d0454e7de82e9prof);

    }

    // line 146
    public function blockcheckboxlabel($context, array $blocks = array())
    {
        $internal8c36f3cb4a2fc2499f70e2cef7ed711d0436fef60453a0159f41329cec00cf17 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8c36f3cb4a2fc2499f70e2cef7ed711d0436fef60453a0159f41329cec00cf17->enter($internal8c36f3cb4a2fc2499f70e2cef7ed711d0436fef60453a0159f41329cec00cf17prof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxlabel"));

        $internal12e0e412c82868c73a33fd6259db094f8880f5fd46d17461aa6762ca524ed9fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal12e0e412c82868c73a33fd6259db094f8880f5fd46d17461aa6762ca524ed9fd->enter($internal12e0e412c82868c73a33fd6259db094f8880f5fd46d17461aa6762ca524ed9fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxlabel"));

        // line 147
        $this->displayBlock("checkboxradiolabel", $context, $blocks);
        
        $internal12e0e412c82868c73a33fd6259db094f8880f5fd46d17461aa6762ca524ed9fd->leave($internal12e0e412c82868c73a33fd6259db094f8880f5fd46d17461aa6762ca524ed9fdprof);

        
        $internal8c36f3cb4a2fc2499f70e2cef7ed711d0436fef60453a0159f41329cec00cf17->leave($internal8c36f3cb4a2fc2499f70e2cef7ed711d0436fef60453a0159f41329cec00cf17prof);

    }

    // line 150
    public function blockradiolabel($context, array $blocks = array())
    {
        $internal49fb5446178588dbb16d618b57121e826826b64c81d87ebc3e67328b248b4d24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal49fb5446178588dbb16d618b57121e826826b64c81d87ebc3e67328b248b4d24->enter($internal49fb5446178588dbb16d618b57121e826826b64c81d87ebc3e67328b248b4d24prof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiolabel"));

        $internal615121a9cc88c4c4ee9818be825bca41865e7d5c4ed33472dc711522252cab3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal615121a9cc88c4c4ee9818be825bca41865e7d5c4ed33472dc711522252cab3f->enter($internal615121a9cc88c4c4ee9818be825bca41865e7d5c4ed33472dc711522252cab3fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiolabel"));

        // line 151
        $this->displayBlock("checkboxradiolabel", $context, $blocks);
        
        $internal615121a9cc88c4c4ee9818be825bca41865e7d5c4ed33472dc711522252cab3f->leave($internal615121a9cc88c4c4ee9818be825bca41865e7d5c4ed33472dc711522252cab3fprof);

        
        $internal49fb5446178588dbb16d618b57121e826826b64c81d87ebc3e67328b248b4d24->leave($internal49fb5446178588dbb16d618b57121e826826b64c81d87ebc3e67328b248b4d24prof);

    }

    // line 154
    public function blockcheckboxradiolabel($context, array $blocks = array())
    {
        $internalc40581706192ca63eeb9e1cd0938616db40c9dc7bfa1c18d0fe57acb87e3eedc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc40581706192ca63eeb9e1cd0938616db40c9dc7bfa1c18d0fe57acb87e3eedc->enter($internalc40581706192ca63eeb9e1cd0938616db40c9dc7bfa1c18d0fe57acb87e3eedcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxradiolabel"));

        $internal490674697f9ca27e0f8fd9fff2b9d82d22cd09ed554a9c03cbccc5f46c530a5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal490674697f9ca27e0f8fd9fff2b9d82d22cd09ed554a9c03cbccc5f46c530a5b->enter($internal490674697f9ca27e0f8fd9fff2b9d82d22cd09ed554a9c03cbccc5f46c530a5bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxradiolabel"));

        // line 155
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 155, $this->getSourceContext()); })()), "admin", array())) {
            // line 156
            echo "        ";
            $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 156, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array());
            // line 157
            echo "    ";
        }
        // line 158
        echo "    ";
        // line 159
        echo "    ";
        if (arraykeyexists("widget", $context)) {
            // line 160
            echo "        ";
            if ((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 160, $this->getSourceContext()); })())) {
                // line 161
                echo "            ";
                $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 161, $this->getSourceContext()); })()), array("class" => twigtrimfilter((((twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array()), "")) : ("")) . " required"))));
                // line 162
                echo "        ";
            }
            // line 163
            echo "        ";
            if (arraykeyexists("parentlabelclass", $context)) {
                // line 164
                echo "            ";
                $context["labelattr"] = twigarraymerge((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 164, $this->getSourceContext()); })()), array("class" => twigtrimfilter(((((twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["labelattr"] ?? null), "class", array()), "")) : ("")) . " ") . (isset($context["parentlabelclass"]) || arraykeyexists("parentlabelclass", $context) ? $context["parentlabelclass"] : (function () { throw new TwigErrorRuntime('Variable "parentlabelclass" does not exist.', 164, $this->getSourceContext()); })())))));
                // line 165
                echo "        ";
            }
            // line 166
            echo "        ";
            if (( !((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 166, $this->getSourceContext()); })()) === false) && twigtestempty((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 166, $this->getSourceContext()); })())))) {
                // line 167
                echo "            ";
                $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 167, $this->getSourceContext()); })()));
                // line 168
                echo "        ";
            }
            // line 169
            echo "        <label";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["labelattr"]) || arraykeyexists("labelattr", $context) ? $context["labelattr"] : (function () { throw new TwigErrorRuntime('Variable "labelattr" does not exist.', 169, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twigescapefilter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twigescapefilter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['attrname'], $context['attrvalue'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            echo ">";
            // line 170
            echo (isset($context["widget"]) || arraykeyexists("widget", $context) ? $context["widget"] : (function () { throw new TwigErrorRuntime('Variable "widget" does not exist.', 170, $this->getSourceContext()); })());
            // line 171
            if ( !((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 171, $this->getSourceContext()); })()) === false)) {
                // line 172
                echo "<span class=\"control-labeltext\">";
                // line 173
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 173, $this->getSourceContext()); })()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 173, $this->getSourceContext()); })())), "html", null, true);
                // line 174
                echo "</span>";
            }
            // line 176
            echo "</label>
    ";
        }
        
        $internal490674697f9ca27e0f8fd9fff2b9d82d22cd09ed554a9c03cbccc5f46c530a5b->leave($internal490674697f9ca27e0f8fd9fff2b9d82d22cd09ed554a9c03cbccc5f46c530a5bprof);

        
        $internalc40581706192ca63eeb9e1cd0938616db40c9dc7bfa1c18d0fe57acb87e3eedc->leave($internalc40581706192ca63eeb9e1cd0938616db40c9dc7bfa1c18d0fe57acb87e3eedcprof);

    }

    // line 180
    public function blockchoicewidgetexpanded($context, array $blocks = array())
    {
        $internala764a81e55e47a14eb9ffe4c41f0578f64f00672cf5a82ee6826e75785d7c426 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala764a81e55e47a14eb9ffe4c41f0578f64f00672cf5a82ee6826e75785d7c426->enter($internala764a81e55e47a14eb9ffe4c41f0578f64f00672cf5a82ee6826e75785d7c426prof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetexpanded"));

        $internal0e0ea62fb060f55052275510138c1909741eee79fc6752a2d7f5e516b9d64e92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0e0ea62fb060f55052275510138c1909741eee79fc6752a2d7f5e516b9d64e92->enter($internal0e0ea62fb060f55052275510138c1909741eee79fc6752a2d7f5e516b9d64e92prof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetexpanded"));

        // line 181
        obstart();
        // line 182
        echo "    ";
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 182, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " list-unstyled")));
        // line 183
        echo "    <ul ";
        $this->displayBlock("widgetcontainerattributes", $context, $blocks);
        echo ">
    ";
        // line 184
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 184, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["child"]) {
            // line 185
            echo "        <li>
            ";
            // line 186
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("horizontal" => false, "horizontalinputwrapperclass" => ""));
            echo " ";
            // line 187
            echo "        </li>
    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 189
        echo "    </ul>
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal0e0ea62fb060f55052275510138c1909741eee79fc6752a2d7f5e516b9d64e92->leave($internal0e0ea62fb060f55052275510138c1909741eee79fc6752a2d7f5e516b9d64e92prof);

        
        $internala764a81e55e47a14eb9ffe4c41f0578f64f00672cf5a82ee6826e75785d7c426->leave($internala764a81e55e47a14eb9ffe4c41f0578f64f00672cf5a82ee6826e75785d7c426prof);

    }

    // line 193
    public function blockchoicewidgetcollapsed($context, array $blocks = array())
    {
        $internal97f50357cd506023fdd6ea0c99668b51c37e0e52ddaaf9a0f1dd929094f5d0fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal97f50357cd506023fdd6ea0c99668b51c37e0e52ddaaf9a0f1dd929094f5d0fd->enter($internal97f50357cd506023fdd6ea0c99668b51c37e0e52ddaaf9a0f1dd929094f5d0fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetcollapsed"));

        $internal615154f99101594687b27d3b64df3bd886a7eb3178b711719d0242e3cd7fad0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal615154f99101594687b27d3b64df3bd886a7eb3178b711719d0242e3cd7fad0c->enter($internal615154f99101594687b27d3b64df3bd886a7eb3178b711719d0242e3cd7fad0cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "choicewidgetcollapsed"));

        // line 194
        obstart();
        // line 195
        echo "    ";
        if ((((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 195, $this->getSourceContext()); })()) && arraykeyexists("placeholder", $context)) && (null === (isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 195, $this->getSourceContext()); })())))) {
            // line 196
            echo "        ";
            $context["required"] = false;
            // line 197
            echo "    ";
        } elseif (((((((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 197, $this->getSourceContext()); })()) && arraykeyexists("emptyvalue", $context)) && arraykeyexists("emptyvalueinchoices", $context)) && (null === (isset($context["emptyvalue"]) || arraykeyexists("emptyvalue", $context) ? $context["emptyvalue"] : (function () { throw new TwigErrorRuntime('Variable "emptyvalue" does not exist.', 197, $this->getSourceContext()); })()))) &&  !(isset($context["emptyvalueinchoices"]) || arraykeyexists("emptyvalueinchoices", $context) ? $context["emptyvalueinchoices"] : (function () { throw new TwigErrorRuntime('Variable "emptyvalueinchoices" does not exist.', 197, $this->getSourceContext()); })())) &&  !(isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 197, $this->getSourceContext()); })()))) {
            // line 198
            echo "        ";
            $context["required"] = false;
            // line 199
            echo "    ";
        }
        // line 200
        echo "
    ";
        // line 201
        $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 201, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control")));
        // line 202
        echo "    ";
        if (((arraykeyexists("sortable", $context) && (isset($context["sortable"]) || arraykeyexists("sortable", $context) ? $context["sortable"] : (function () { throw new TwigErrorRuntime('Variable "sortable" does not exist.', 202, $this->getSourceContext()); })())) && (isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 202, $this->getSourceContext()); })()))) {
            // line 203
            echo "        ";
            $this->displayBlock("sonatatypechoicemultiplesortable", $context, $blocks);
            echo "
    ";
        } else {
            // line 205
            echo "        <select ";
            $this->displayBlock("widgetattributes", $context, $blocks);
            if ((isset($context["multiple"]) || arraykeyexists("multiple", $context) ? $context["multiple"] : (function () { throw new TwigErrorRuntime('Variable "multiple" does not exist.', 205, $this->getSourceContext()); })())) {
                echo " multiple=\"multiple\"";
            }
            echo " >
            ";
            // line 206
            if ((arraykeyexists("emptyvalue", $context) &&  !(null === (isset($context["emptyvalue"]) || arraykeyexists("emptyvalue", $context) ? $context["emptyvalue"] : (function () { throw new TwigErrorRuntime('Variable "emptyvalue" does not exist.', 206, $this->getSourceContext()); })())))) {
                // line 207
                echo "                <option value=\"\"";
                if (((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 207, $this->getSourceContext()); })()) && twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 207, $this->getSourceContext()); })())))) {
                    echo " selected=\"selected\"";
                }
                echo ">
                    ";
                // line 208
                if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 208, $this->getSourceContext()); })()), "admin", array())) {
                    // line 209
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["emptyvalue"]) || arraykeyexists("emptyvalue", $context) ? $context["emptyvalue"] : (function () { throw new TwigErrorRuntime('Variable "emptyvalue" does not exist.', 209, $this->getSourceContext()); })()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 209, $this->getSourceContext()); })())), "html", null, true);
                } else {
                    // line 211
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["emptyvalue"]) || arraykeyexists("emptyvalue", $context) ? $context["emptyvalue"] : (function () { throw new TwigErrorRuntime('Variable "emptyvalue" does not exist.', 211, $this->getSourceContext()); })()), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 211, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())), "html", null, true);
                }
                // line 213
                echo "                </option>
            ";
            } elseif ((            // line 214
arraykeyexists("placeholder", $context) &&  !(null === (isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 214, $this->getSourceContext()); })())))) {
                // line 215
                echo "                <option value=\"\"";
                if (((isset($context["required"]) || arraykeyexists("required", $context) ? $context["required"] : (function () { throw new TwigErrorRuntime('Variable "required" does not exist.', 215, $this->getSourceContext()); })()) && twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 215, $this->getSourceContext()); })())))) {
                    echo " selected=\"selected\"";
                }
                echo ">
                    ";
                // line 216
                if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 216, $this->getSourceContext()); })()), "admin", array())) {
                    // line 217
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 217, $this->getSourceContext()); })()), array(), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 217, $this->getSourceContext()); })())), "html", null, true);
                } else {
                    // line 219
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["placeholder"]) || arraykeyexists("placeholder", $context) ? $context["placeholder"] : (function () { throw new TwigErrorRuntime('Variable "placeholder" does not exist.', 219, $this->getSourceContext()); })()), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 219, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())), "html", null, true);
                }
                // line 221
                echo "                </option>
            ";
            }
            // line 223
            echo "            ";
            if ((twiglengthfilter($this->env, (isset($context["preferredchoices"]) || arraykeyexists("preferredchoices", $context) ? $context["preferredchoices"] : (function () { throw new TwigErrorRuntime('Variable "preferredchoices" does not exist.', 223, $this->getSourceContext()); })())) > 0)) {
                // line 224
                echo "                ";
                $context["options"] = (isset($context["preferredchoices"]) || arraykeyexists("preferredchoices", $context) ? $context["preferredchoices"] : (function () { throw new TwigErrorRuntime('Variable "preferredchoices" does not exist.', 224, $this->getSourceContext()); })());
                // line 225
                echo "                ";
                $this->displayBlock("choicewidgetoptions", $context, $blocks);
                echo "
                ";
                // line 226
                if ((twiglengthfilter($this->env, (isset($context["choices"]) || arraykeyexists("choices", $context) ? $context["choices"] : (function () { throw new TwigErrorRuntime('Variable "choices" does not exist.', 226, $this->getSourceContext()); })())) > 0)) {
                    // line 227
                    echo "                    <option disabled=\"disabled\">";
                    echo twigescapefilter($this->env, (isset($context["separator"]) || arraykeyexists("separator", $context) ? $context["separator"] : (function () { throw new TwigErrorRuntime('Variable "separator" does not exist.', 227, $this->getSourceContext()); })()), "html", null, true);
                    echo "</option>
                ";
                }
                // line 229
                echo "            ";
            }
            // line 230
            echo "            ";
            $context["options"] = (isset($context["choices"]) || arraykeyexists("choices", $context) ? $context["choices"] : (function () { throw new TwigErrorRuntime('Variable "choices" does not exist.', 230, $this->getSourceContext()); })());
            // line 231
            echo "            ";
            $this->displayBlock("choicewidgetoptions", $context, $blocks);
            echo "
        </select>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal615154f99101594687b27d3b64df3bd886a7eb3178b711719d0242e3cd7fad0c->leave($internal615154f99101594687b27d3b64df3bd886a7eb3178b711719d0242e3cd7fad0cprof);

        
        $internal97f50357cd506023fdd6ea0c99668b51c37e0e52ddaaf9a0f1dd929094f5d0fd->leave($internal97f50357cd506023fdd6ea0c99668b51c37e0e52ddaaf9a0f1dd929094f5d0fdprof);

    }

    // line 237
    public function blockdatewidget($context, array $blocks = array())
    {
        $internaldae83ca3ff09286dd415a2e8fcd11ef93357944894f01d195deed4f15314db25 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldae83ca3ff09286dd415a2e8fcd11ef93357944894f01d195deed4f15314db25->enter($internaldae83ca3ff09286dd415a2e8fcd11ef93357944894f01d195deed4f15314db25prof = new TwigProfilerProfile($this->getTemplateName(), "block", "datewidget"));

        $internal57761a86127bae049b2338f0ff9a907b4e70f17cebb31fc310bc65f3b308d23b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal57761a86127bae049b2338f0ff9a907b4e70f17cebb31fc310bc65f3b308d23b->enter($internal57761a86127bae049b2338f0ff9a907b4e70f17cebb31fc310bc65f3b308d23bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "datewidget"));

        // line 238
        obstart();
        // line 239
        echo "    ";
        if (((isset($context["widget"]) || arraykeyexists("widget", $context) ? $context["widget"] : (function () { throw new TwigErrorRuntime('Variable "widget" does not exist.', 239, $this->getSourceContext()); })()) == "singletext")) {
            // line 240
            echo "        ";
            $this->displayBlock("formwidgetsimple", $context, $blocks);
            echo "
    ";
        } else {
            // line 242
            echo "        ";
            if (( !arraykeyexists("row", $context) || ((isset($context["row"]) || arraykeyexists("row", $context) ? $context["row"] : (function () { throw new TwigErrorRuntime('Variable "row" does not exist.', 242, $this->getSourceContext()); })()) == true))) {
                // line 243
                echo "            ";
                $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 243, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " row")));
                // line 244
                echo "        ";
            }
            // line 245
            echo "        ";
            $context["inputwrapperclass"] = ((arraykeyexists("inputwrapperclass", $context)) ? (twigdefaultfilter((isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 245, $this->getSourceContext()); })()), "col-sm-4")) : ("col-sm-4"));
            // line 246
            echo "        <div ";
            $this->displayBlock("widgetcontainerattributes", $context, $blocks);
            echo ">
            ";
            // line 247
            echo twigreplacefilter((isset($context["datepattern"]) || arraykeyexists("datepattern", $context) ? $context["datepattern"] : (function () { throw new TwigErrorRuntime('Variable "datepattern" does not exist.', 247, $this->getSourceContext()); })()), array("{{ year }}" => (((("<div class=\"" .             // line 248
(isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 248, $this->getSourceContext()); })())) . "\">") . $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 248, $this->getSourceContext()); })()), "year", array()), 'widget')) . "</div>"), "{{ month }}" => (((("<div class=\"" .             // line 249
(isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 249, $this->getSourceContext()); })())) . "\">") . $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 249, $this->getSourceContext()); })()), "month", array()), 'widget')) . "</div>"), "{{ day }}" => (((("<div class=\"" .             // line 250
(isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 250, $this->getSourceContext()); })())) . "\">") . $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 250, $this->getSourceContext()); })()), "day", array()), 'widget')) . "</div>")));
            // line 251
            echo "
        </div>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal57761a86127bae049b2338f0ff9a907b4e70f17cebb31fc310bc65f3b308d23b->leave($internal57761a86127bae049b2338f0ff9a907b4e70f17cebb31fc310bc65f3b308d23bprof);

        
        $internaldae83ca3ff09286dd415a2e8fcd11ef93357944894f01d195deed4f15314db25->leave($internaldae83ca3ff09286dd415a2e8fcd11ef93357944894f01d195deed4f15314db25prof);

    }

    // line 257
    public function blocktimewidget($context, array $blocks = array())
    {
        $internal30bdf6b49b25f2979592db48e92ab217403f2402c96ae8827c24670ecc429dae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal30bdf6b49b25f2979592db48e92ab217403f2402c96ae8827c24670ecc429dae->enter($internal30bdf6b49b25f2979592db48e92ab217403f2402c96ae8827c24670ecc429daeprof = new TwigProfilerProfile($this->getTemplateName(), "block", "timewidget"));

        $internal2fc560ffecfd74664ce5c7ce857d3f2c3d55e8d2a366556118c91951de616409 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2fc560ffecfd74664ce5c7ce857d3f2c3d55e8d2a366556118c91951de616409->enter($internal2fc560ffecfd74664ce5c7ce857d3f2c3d55e8d2a366556118c91951de616409prof = new TwigProfilerProfile($this->getTemplateName(), "block", "timewidget"));

        // line 258
        obstart();
        // line 259
        echo "    ";
        if (((isset($context["widget"]) || arraykeyexists("widget", $context) ? $context["widget"] : (function () { throw new TwigErrorRuntime('Variable "widget" does not exist.', 259, $this->getSourceContext()); })()) == "singletext")) {
            // line 260
            echo "        ";
            $this->displayBlock("formwidgetsimple", $context, $blocks);
            echo "
    ";
        } else {
            // line 262
            echo "        ";
            if (( !arraykeyexists("row", $context) || ((isset($context["row"]) || arraykeyexists("row", $context) ? $context["row"] : (function () { throw new TwigErrorRuntime('Variable "row" does not exist.', 262, $this->getSourceContext()); })()) == true))) {
                // line 263
                echo "            ";
                $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 263, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " row")));
                // line 264
                echo "        ";
            }
            // line 265
            echo "        ";
            $context["inputwrapperclass"] = ((arraykeyexists("inputwrapperclass", $context)) ? (twigdefaultfilter((isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 265, $this->getSourceContext()); })()), "col-sm-6")) : ("col-sm-6"));
            // line 266
            echo "        <div ";
            $this->displayBlock("widgetcontainerattributes", $context, $blocks);
            echo ">
            <div class=\"";
            // line 267
            echo twigescapefilter($this->env, (isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 267, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
                ";
            // line 268
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 268, $this->getSourceContext()); })()), "hour", array()), 'widget');
            echo "
            </div>
            ";
            // line 270
            if ((isset($context["withminutes"]) || arraykeyexists("withminutes", $context) ? $context["withminutes"] : (function () { throw new TwigErrorRuntime('Variable "withminutes" does not exist.', 270, $this->getSourceContext()); })())) {
                // line 271
                echo "                <div class=\"";
                echo twigescapefilter($this->env, (isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 271, $this->getSourceContext()); })()), "html", null, true);
                echo "\">
                    ";
                // line 272
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 272, $this->getSourceContext()); })()), "minute", array()), 'widget');
                echo "
                </div>
            ";
            }
            // line 275
            echo "            ";
            if ((isset($context["withseconds"]) || arraykeyexists("withseconds", $context) ? $context["withseconds"] : (function () { throw new TwigErrorRuntime('Variable "withseconds" does not exist.', 275, $this->getSourceContext()); })())) {
                // line 276
                echo "                <div class=\"";
                echo twigescapefilter($this->env, (isset($context["inputwrapperclass"]) || arraykeyexists("inputwrapperclass", $context) ? $context["inputwrapperclass"] : (function () { throw new TwigErrorRuntime('Variable "inputwrapperclass" does not exist.', 276, $this->getSourceContext()); })()), "html", null, true);
                echo "\">
                    ";
                // line 277
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 277, $this->getSourceContext()); })()), "second", array()), 'widget');
                echo "
                </div>
            ";
            }
            // line 280
            echo "        </div>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal2fc560ffecfd74664ce5c7ce857d3f2c3d55e8d2a366556118c91951de616409->leave($internal2fc560ffecfd74664ce5c7ce857d3f2c3d55e8d2a366556118c91951de616409prof);

        
        $internal30bdf6b49b25f2979592db48e92ab217403f2402c96ae8827c24670ecc429dae->leave($internal30bdf6b49b25f2979592db48e92ab217403f2402c96ae8827c24670ecc429daeprof);

    }

    // line 285
    public function blockdatetimewidget($context, array $blocks = array())
    {
        $internald5782cefb32ab367142a00735777d8c2ed8898ab79e8f51562b154eb786df975 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald5782cefb32ab367142a00735777d8c2ed8898ab79e8f51562b154eb786df975->enter($internald5782cefb32ab367142a00735777d8c2ed8898ab79e8f51562b154eb786df975prof = new TwigProfilerProfile($this->getTemplateName(), "block", "datetimewidget"));

        $internal3d35bd33ad46dbd46c9d11c79e7df5cbc6c001811e34b18780714ff48f0dd4a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3d35bd33ad46dbd46c9d11c79e7df5cbc6c001811e34b18780714ff48f0dd4a2->enter($internal3d35bd33ad46dbd46c9d11c79e7df5cbc6c001811e34b18780714ff48f0dd4a2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "datetimewidget"));

        // line 286
        obstart();
        // line 287
        echo "    ";
        if (((isset($context["widget"]) || arraykeyexists("widget", $context) ? $context["widget"] : (function () { throw new TwigErrorRuntime('Variable "widget" does not exist.', 287, $this->getSourceContext()); })()) == "singletext")) {
            // line 288
            echo "        ";
            $this->displayBlock("formwidgetsimple", $context, $blocks);
            echo "
    ";
        } else {
            // line 290
            echo "        ";
            $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 290, $this->getSourceContext()); })()), array("class" => (((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : ("")) . " row")));
            // line 291
            echo "        <div ";
            $this->displayBlock("widgetcontainerattributes", $context, $blocks);
            echo ">
            ";
            // line 292
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 292, $this->getSourceContext()); })()), "date", array()), 'errors');
            echo "
            ";
            // line 293
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 293, $this->getSourceContext()); })()), "time", array()), 'errors');
            echo "

            ";
            // line 295
            if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 295, $this->getSourceContext()); })()), "date", array()), "vars", array()), "widget", array()) == "singletext")) {
                // line 296
                echo "                <div class=\"col-sm-2\">
                    ";
                // line 297
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 297, $this->getSourceContext()); })()), "date", array()), 'widget');
                echo "
                </div>
            ";
            } else {
                // line 300
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 300, $this->getSourceContext()); })()), "date", array()), 'widget', array("row" => false, "inputwrapperclass" => "col-sm-2"));
                echo "
            ";
            }
            // line 302
            echo "
            ";
            // line 303
            if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 303, $this->getSourceContext()); })()), "time", array()), "vars", array()), "widget", array()) == "singletext")) {
                // line 304
                echo "                <div class=\"col-sm-2\">
                    ";
                // line 305
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 305, $this->getSourceContext()); })()), "time", array()), 'widget');
                echo "
                </div>
            ";
            } else {
                // line 308
                echo "                ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 308, $this->getSourceContext()); })()), "time", array()), 'widget', array("row" => false, "inputwrapperclass" => "col-sm-2"));
                echo "
            ";
            }
            // line 310
            echo "        </div>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal3d35bd33ad46dbd46c9d11c79e7df5cbc6c001811e34b18780714ff48f0dd4a2->leave($internal3d35bd33ad46dbd46c9d11c79e7df5cbc6c001811e34b18780714ff48f0dd4a2prof);

        
        $internald5782cefb32ab367142a00735777d8c2ed8898ab79e8f51562b154eb786df975->leave($internald5782cefb32ab367142a00735777d8c2ed8898ab79e8f51562b154eb786df975prof);

    }

    // line 315
    public function blockformrow($context, array $blocks = array())
    {
        $internal4057c4861f90e301e4dde1381c7866bff8877d3ebc23d167b1ebd277b7014061 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4057c4861f90e301e4dde1381c7866bff8877d3ebc23d167b1ebd277b7014061->enter($internal4057c4861f90e301e4dde1381c7866bff8877d3ebc23d167b1ebd277b7014061prof = new TwigProfilerProfile($this->getTemplateName(), "block", "formrow"));

        $internal5bafdad5708cfc6ca3e91feadd65bd0dd11a4b7b4761fcacc1137856d736933b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5bafdad5708cfc6ca3e91feadd65bd0dd11a4b7b4761fcacc1137856d736933b->enter($internal5bafdad5708cfc6ca3e91feadd65bd0dd11a4b7b4761fcacc1137856d736933bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "formrow"));

        // line 316
        echo "    ";
        $context["showlabel"] = ((arraykeyexists("showlabel", $context)) ? (twigdefaultfilter((isset($context["showlabel"]) || arraykeyexists("showlabel", $context) ? $context["showlabel"] : (function () { throw new TwigErrorRuntime('Variable "showlabel" does not exist.', 316, $this->getSourceContext()); })()), true)) : (true));
        // line 317
        echo "    <div class=\"form-group";
        if ((twiglengthfilter($this->env, (isset($context["errors"]) || arraykeyexists("errors", $context) ? $context["errors"] : (function () { throw new TwigErrorRuntime('Variable "errors" does not exist.', 317, $this->getSourceContext()); })())) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 317, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        ";
        // line 318
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", true, true)) {
            // line 319
            echo "            ";
            $context["label"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "name", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "name", array()), (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 319, $this->getSourceContext()); })()))) : ((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 319, $this->getSourceContext()); })())));
            // line 320
            echo "        ";
        }
        // line 321
        echo "
        ";
        // line 322
        $context["divclass"] = "sonata-ba-field";
        // line 323
        echo "
        ";
        // line 324
        if (((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 324, $this->getSourceContext()); })()) === false)) {
            // line 325
            echo "            ";
            $context["divclass"] = ((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 325, $this->getSourceContext()); })()) . " sonata-collection-row-without-label");
            // line 326
            echo "        ";
        }
        // line 327
        echo "
        ";
        // line 328
        if ((arraykeyexists("sonataadmin", $context) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 328, $this->getSourceContext()); })()), "options", array()), "formtype", array(), "array") == "horizontal"))) {
            // line 329
            echo "            ";
            // line 330
            echo "            ";
            if ((((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 330, $this->getSourceContext()); })()) === false) || twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "vars", array(), "any", false, true), "checked", array(), "any", true, true))) {
                // line 331
                echo "                ";
                if (twiginfilter("collection", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 331, $this->getSourceContext()); })()), "parent", array()), "vars", array()), "blockprefixes", array()))) {
                    // line 332
                    echo "                    ";
                    $context["divclass"] = ((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 332, $this->getSourceContext()); })()) . " col-sm-12");
                    // line 333
                    echo "                ";
                } else {
                    // line 334
                    echo "                    ";
                    $context["divclass"] = ((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 334, $this->getSourceContext()); })()) . " col-sm-9 col-sm-offset-3");
                    // line 335
                    echo "                ";
                }
                // line 336
                echo "            ";
            } else {
                // line 337
                echo "                ";
                $context["divclass"] = ((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 337, $this->getSourceContext()); })()) . " col-sm-9");
                // line 338
                echo "            ";
            }
            // line 339
            echo "        ";
        }
        // line 340
        echo "
        ";
        // line 341
        if ((isset($context["showlabel"]) || arraykeyexists("showlabel", $context) ? $context["showlabel"] : (function () { throw new TwigErrorRuntime('Variable "showlabel" does not exist.', 341, $this->getSourceContext()); })())) {
            // line 342
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 342, $this->getSourceContext()); })()), 'label', (twigtestempty($label = ((arraykeyexists("label", $context)) ? (twigdefaultfilter((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 342, $this->getSourceContext()); })()), null)) : (null))) ? array() : array("label" => $label)));
            echo "
        ";
        }
        // line 344
        echo "
        ";
        // line 345
        if ((arraykeyexists("sonataadmin", $context) && (isset($context["sonataadminenabled"]) || arraykeyexists("sonataadminenabled", $context) ? $context["sonataadminenabled"] : (function () { throw new TwigErrorRuntime('Variable "sonataadminenabled" does not exist.', 345, $this->getSourceContext()); })()))) {
            // line 346
            echo "            ";
            $context["divclass"] = (((((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 346, $this->getSourceContext()); })()) . " sonata-ba-field-") . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 346, $this->getSourceContext()); })()), "edit", array())) . "-") . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 346, $this->getSourceContext()); })()), "inline", array()));
            // line 347
            echo "        ";
        }
        // line 348
        echo "
        ";
        // line 349
        if ((twiglengthfilter($this->env, (isset($context["errors"]) || arraykeyexists("errors", $context) ? $context["errors"] : (function () { throw new TwigErrorRuntime('Variable "errors" does not exist.', 349, $this->getSourceContext()); })())) > 0)) {
            // line 350
            echo "            ";
            $context["divclass"] = ((isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 350, $this->getSourceContext()); })()) . " sonata-ba-field-error");
            // line 351
            echo "        ";
        }
        // line 352
        echo "
        <div class=\"";
        // line 353
        echo twigescapefilter($this->env, (isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 353, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
            ";
        // line 354
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 354, $this->getSourceContext()); })()), 'widget', array("horizontal" => false, "horizontalinputwrapperclass" => ""));
        echo " ";
        // line 355
        echo "
            ";
        // line 356
        if ((twiglengthfilter($this->env, (isset($context["errors"]) || arraykeyexists("errors", $context) ? $context["errors"] : (function () { throw new TwigErrorRuntime('Variable "errors" does not exist.', 356, $this->getSourceContext()); })())) > 0)) {
            // line 357
            echo "                <div class=\"help-block sonata-ba-field-error-messages\">
                    ";
            // line 358
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 358, $this->getSourceContext()); })()), 'errors');
            echo "
                </div>
            ";
        }
        // line 361
        echo "
            ";
        // line 362
        if (((arraykeyexists("sonataadmin", $context) && (isset($context["sonataadminenabled"]) || arraykeyexists("sonataadminenabled", $context) ? $context["sonataadminenabled"] : (function () { throw new TwigErrorRuntime('Variable "sonataadminenabled" does not exist.', 362, $this->getSourceContext()); })())) && ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "help", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "help", array()), false)) : (false)))) {
            // line 363
            echo "                <span class=\"help-block sonata-ba-field-help\">";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 363, $this->getSourceContext()); })()), "fielddescription", array()), "help", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 363, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 363, $this->getSourceContext()); })()), "fielddescription", array()), "translationDomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 363, $this->getSourceContext()); })()), "translationDomain", array()))));
            echo "</span>
            ";
        }
        // line 365
        echo "        </div>
    </div>
";
        
        $internal5bafdad5708cfc6ca3e91feadd65bd0dd11a4b7b4761fcacc1137856d736933b->leave($internal5bafdad5708cfc6ca3e91feadd65bd0dd11a4b7b4761fcacc1137856d736933bprof);

        
        $internal4057c4861f90e301e4dde1381c7866bff8877d3ebc23d167b1ebd277b7014061->leave($internal4057c4861f90e301e4dde1381c7866bff8877d3ebc23d167b1ebd277b7014061prof);

    }

    // line 369
    public function blockcheckboxrow($context, array $blocks = array())
    {
        $internal735f8a6f1a10a5a882fb1b8508868c95183b78a8f5c1ee8270c649402c61fd3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal735f8a6f1a10a5a882fb1b8508868c95183b78a8f5c1ee8270c649402c61fd3f->enter($internal735f8a6f1a10a5a882fb1b8508868c95183b78a8f5c1ee8270c649402c61fd3fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxrow"));

        $internal21fca8cf7ad979fe41bd45451c37c4fa218297cf7928f0d90fd8439b5d70b444 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal21fca8cf7ad979fe41bd45451c37c4fa218297cf7928f0d90fd8439b5d70b444->enter($internal21fca8cf7ad979fe41bd45451c37c4fa218297cf7928f0d90fd8439b5d70b444prof = new TwigProfilerProfile($this->getTemplateName(), "block", "checkboxrow"));

        // line 370
        $context["showlabel"] = false;
        // line 371
        echo "    ";
        $this->displayBlock("formrow", $context, $blocks);
        
        $internal21fca8cf7ad979fe41bd45451c37c4fa218297cf7928f0d90fd8439b5d70b444->leave($internal21fca8cf7ad979fe41bd45451c37c4fa218297cf7928f0d90fd8439b5d70b444prof);

        
        $internal735f8a6f1a10a5a882fb1b8508868c95183b78a8f5c1ee8270c649402c61fd3f->leave($internal735f8a6f1a10a5a882fb1b8508868c95183b78a8f5c1ee8270c649402c61fd3fprof);

    }

    // line 374
    public function blockradiorow($context, array $blocks = array())
    {
        $internal3ea71750dcc39c9180c4cf3ee4d7af59f606a2272ca526cebd66f2ea8f81e805 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3ea71750dcc39c9180c4cf3ee4d7af59f606a2272ca526cebd66f2ea8f81e805->enter($internal3ea71750dcc39c9180c4cf3ee4d7af59f606a2272ca526cebd66f2ea8f81e805prof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiorow"));

        $internald0591a4827d3894dbf7dba0cf3b5e6ff91a7a8aa8f8c9e49548ddcaa154046de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald0591a4827d3894dbf7dba0cf3b5e6ff91a7a8aa8f8c9e49548ddcaa154046de->enter($internald0591a4827d3894dbf7dba0cf3b5e6ff91a7a8aa8f8c9e49548ddcaa154046deprof = new TwigProfilerProfile($this->getTemplateName(), "block", "radiorow"));

        // line 375
        $context["showlabel"] = false;
        // line 376
        echo "    ";
        $this->displayBlock("formrow", $context, $blocks);
        
        $internald0591a4827d3894dbf7dba0cf3b5e6ff91a7a8aa8f8c9e49548ddcaa154046de->leave($internald0591a4827d3894dbf7dba0cf3b5e6ff91a7a8aa8f8c9e49548ddcaa154046deprof);

        
        $internal3ea71750dcc39c9180c4cf3ee4d7af59f606a2272ca526cebd66f2ea8f81e805->leave($internal3ea71750dcc39c9180c4cf3ee4d7af59f606a2272ca526cebd66f2ea8f81e805prof);

    }

    // line 379
    public function blocksonatatypenativecollectionwidgetrow($context, array $blocks = array())
    {
        $internald8d1b19d240c5f224e303f9c34f275feb80afb7a6f00034778d82098e0fcc943 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald8d1b19d240c5f224e303f9c34f275feb80afb7a6f00034778d82098e0fcc943->enter($internald8d1b19d240c5f224e303f9c34f275feb80afb7a6f00034778d82098e0fcc943prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypenativecollectionwidgetrow"));

        $internal7d316b5c14e2bdaa27e3a842c522096d6e509fa752f7e50ca68e261cc5152f30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7d316b5c14e2bdaa27e3a842c522096d6e509fa752f7e50ca68e261cc5152f30->enter($internal7d316b5c14e2bdaa27e3a842c522096d6e509fa752f7e50ca68e261cc5152f30prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypenativecollectionwidgetrow"));

        // line 380
        obstart();
        // line 381
        echo "    <div class=\"sonata-collection-row\">
        ";
        // line 382
        if ((isset($context["allowdelete"]) || arraykeyexists("allowdelete", $context) ? $context["allowdelete"] : (function () { throw new TwigErrorRuntime('Variable "allowdelete" does not exist.', 382, $this->getSourceContext()); })())) {
            // line 383
            echo "            <div class=\"row\">
                <div class=\"col-xs-1\">
                    <a href=\"#\" class=\"btn btn-link sonata-collection-delete\">
                        <i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>
                    </a>
                </div>
                <div class=\"col-xs-11\">
        ";
        }
        // line 391
        echo "            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 391, $this->getSourceContext()); })()), 'row', array("label" => false));
        echo "
        ";
        // line 392
        if ((isset($context["allowdelete"]) || arraykeyexists("allowdelete", $context) ? $context["allowdelete"] : (function () { throw new TwigErrorRuntime('Variable "allowdelete" does not exist.', 392, $this->getSourceContext()); })())) {
            // line 393
            echo "                </div>
            </div>
        ";
        }
        // line 396
        echo "    </div>
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal7d316b5c14e2bdaa27e3a842c522096d6e509fa752f7e50ca68e261cc5152f30->leave($internal7d316b5c14e2bdaa27e3a842c522096d6e509fa752f7e50ca68e261cc5152f30prof);

        
        $internald8d1b19d240c5f224e303f9c34f275feb80afb7a6f00034778d82098e0fcc943->leave($internald8d1b19d240c5f224e303f9c34f275feb80afb7a6f00034778d82098e0fcc943prof);

    }

    // line 400
    public function blocksonatatypenativecollectionwidget($context, array $blocks = array())
    {
        $internal6e4d4a18f4617a0c8b75440f23be5af0406c4b93ee87722def1fc9429a69fb74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6e4d4a18f4617a0c8b75440f23be5af0406c4b93ee87722def1fc9429a69fb74->enter($internal6e4d4a18f4617a0c8b75440f23be5af0406c4b93ee87722def1fc9429a69fb74prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypenativecollectionwidget"));

        $internal5eb9b1cb178ea862c72c258b70d0e0c3f1d4b8ee02672f2f6af7795f31db6c8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5eb9b1cb178ea862c72c258b70d0e0c3f1d4b8ee02672f2f6af7795f31db6c8e->enter($internal5eb9b1cb178ea862c72c258b70d0e0c3f1d4b8ee02672f2f6af7795f31db6c8eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypenativecollectionwidget"));

        // line 401
        obstart();
        // line 402
        echo "    ";
        if (arraykeyexists("prototype", $context)) {
            // line 403
            echo "        ";
            $context["child"] = (isset($context["prototype"]) || arraykeyexists("prototype", $context) ? $context["prototype"] : (function () { throw new TwigErrorRuntime('Variable "prototype" does not exist.', 403, $this->getSourceContext()); })());
            // line 404
            echo "        ";
            $context["allowdeletebackup"] = (isset($context["allowdelete"]) || arraykeyexists("allowdelete", $context) ? $context["allowdelete"] : (function () { throw new TwigErrorRuntime('Variable "allowdelete" does not exist.', 404, $this->getSourceContext()); })());
            // line 405
            echo "        ";
            $context["allowdelete"] = true;
            // line 406
            echo "        ";
            $context["attr"] = twigarraymerge((isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 406, $this->getSourceContext()); })()), array("data-prototype" =>             $this->renderBlock("sonatatypenativecollectionwidgetrow", $context, $blocks), "data-prototype-name" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["prototype"]) || arraykeyexists("prototype", $context) ? $context["prototype"] : (function () { throw new TwigErrorRuntime('Variable "prototype" does not exist.', 406, $this->getSourceContext()); })()), "vars", array()), "name", array()), "class" => ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attr"] ?? null), "class", array()), "")) : (""))));
            // line 407
            echo "        ";
            $context["allowdelete"] = (isset($context["allowdeletebackup"]) || arraykeyexists("allowdeletebackup", $context) ? $context["allowdeletebackup"] : (function () { throw new TwigErrorRuntime('Variable "allowdeletebackup" does not exist.', 407, $this->getSourceContext()); })());
            // line 408
            echo "    ";
        }
        // line 409
        echo "    <div ";
        $this->displayBlock("widgetcontainerattributes", $context, $blocks);
        echo ">
        ";
        // line 410
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 410, $this->getSourceContext()); })()), 'errors');
        echo "
        ";
        // line 411
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 411, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["child"]) {
            // line 412
            echo "            ";
            $this->displayBlock("sonatatypenativecollectionwidgetrow", $context, $blocks);
            echo "
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 414
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 414, $this->getSourceContext()); })()), 'rest');
        echo "
        ";
        // line 415
        if ((isset($context["allowadd"]) || arraykeyexists("allowadd", $context) ? $context["allowadd"] : (function () { throw new TwigErrorRuntime('Variable "allowadd" does not exist.', 415, $this->getSourceContext()); })())) {
            // line 416
            echo "            <div><a href=\"#\" class=\"btn btn-link sonata-collection-add\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i></a></div>
        ";
        }
        // line 418
        echo "    </div>
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal5eb9b1cb178ea862c72c258b70d0e0c3f1d4b8ee02672f2f6af7795f31db6c8e->leave($internal5eb9b1cb178ea862c72c258b70d0e0c3f1d4b8ee02672f2f6af7795f31db6c8eprof);

        
        $internal6e4d4a18f4617a0c8b75440f23be5af0406c4b93ee87722def1fc9429a69fb74->leave($internal6e4d4a18f4617a0c8b75440f23be5af0406c4b93ee87722def1fc9429a69fb74prof);

    }

    // line 422
    public function blocksonatatypeimmutablearraywidget($context, array $blocks = array())
    {
        $internal3aed4fa546ebf5d0616255305f38233554a7988c3d99572e076f777f6b16a2c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3aed4fa546ebf5d0616255305f38233554a7988c3d99572e076f777f6b16a2c4->enter($internal3aed4fa546ebf5d0616255305f38233554a7988c3d99572e076f777f6b16a2c4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeimmutablearraywidget"));

        $internal6b4b0fdb4662a3af8fb25f7f097952938f2c62b046943c3c526a802a50c2b82d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b4b0fdb4662a3af8fb25f7f097952938f2c62b046943c3c526a802a50c2b82d->enter($internal6b4b0fdb4662a3af8fb25f7f097952938f2c62b046943c3c526a802a50c2b82dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeimmutablearraywidget"));

        // line 423
        echo "    ";
        obstart();
        // line 424
        echo "        <div ";
        $this->displayBlock("widgetcontainerattributes", $context, $blocks);
        echo ">
            ";
        // line 425
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 425, $this->getSourceContext()); })()), 'errors');
        echo "

            ";
        // line 427
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 427, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["child"]) {
            // line 428
            echo "                ";
            $this->displayBlock("sonatatypeimmutablearraywidgetrow", $context, $blocks);
            echo "
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['child'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 430
        echo "
            ";
        // line 431
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 431, $this->getSourceContext()); })()), 'rest');
        echo "
        </div>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal6b4b0fdb4662a3af8fb25f7f097952938f2c62b046943c3c526a802a50c2b82d->leave($internal6b4b0fdb4662a3af8fb25f7f097952938f2c62b046943c3c526a802a50c2b82dprof);

        
        $internal3aed4fa546ebf5d0616255305f38233554a7988c3d99572e076f777f6b16a2c4->leave($internal3aed4fa546ebf5d0616255305f38233554a7988c3d99572e076f777f6b16a2c4prof);

    }

    // line 436
    public function blocksonatatypeimmutablearraywidgetrow($context, array $blocks = array())
    {
        $internalfb75a77185857fcaab2fa7882a3f08716bb70c384e7ab4c2bc0e89c561fed605 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfb75a77185857fcaab2fa7882a3f08716bb70c384e7ab4c2bc0e89c561fed605->enter($internalfb75a77185857fcaab2fa7882a3f08716bb70c384e7ab4c2bc0e89c561fed605prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeimmutablearraywidgetrow"));

        $internalfafa88b1cd17c041f9cb8309681b0a6ff3e77aad4b82fd59a65ec050775b6fa5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfafa88b1cd17c041f9cb8309681b0a6ff3e77aad4b82fd59a65ec050775b6fa5->enter($internalfafa88b1cd17c041f9cb8309681b0a6ff3e77aad4b82fd59a65ec050775b6fa5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeimmutablearraywidgetrow"));

        // line 437
        echo "    ";
        obstart();
        // line 438
        echo "        <div class=\"form-group";
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 438, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo " error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 438, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, (isset($context["key"]) || arraykeyexists("key", $context) ? $context["key"] : (function () { throw new TwigErrorRuntime('Variable "key" does not exist.', 438, $this->getSourceContext()); })()), "html", null, true);
        echo "\">

            ";
        // line 440
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 440, $this->getSourceContext()); })()), 'label');
        echo "

            ";
        // line 442
        $context["divclass"] = "";
        // line 443
        echo "            ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 443, $this->getSourceContext()); })()), "options", array()), "formtype", array(), "array") == "horizontal")) {
            // line 444
            echo "                ";
            $context["divclass"] = "col-sm-9";
            // line 445
            echo "            ";
        }
        // line 446
        echo "
            <div class=\"";
        // line 447
        echo twigescapefilter($this->env, (isset($context["divclass"]) || arraykeyexists("divclass", $context) ? $context["divclass"] : (function () { throw new TwigErrorRuntime('Variable "divclass" does not exist.', 447, $this->getSourceContext()); })()), "html", null, true);
        echo " sonata-ba-field sonata-ba-field-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 447, $this->getSourceContext()); })()), "edit", array()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 447, $this->getSourceContext()); })()), "inline", array()), "html", null, true);
        echo " ";
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 447, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">
                ";
        // line 448
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 448, $this->getSourceContext()); })()), 'widget', array("horizontal" => false, "horizontalinputwrapperclass" => ""));
        echo " ";
        // line 449
        echo "            </div>

            ";
        // line 451
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 451, $this->getSourceContext()); })()), "vars", array()), "errors", array())) > 0)) {
            // line 452
            echo "                <div class=\"help-block sonata-ba-field-error-messages\">
                    ";
            // line 453
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["child"]) || arraykeyexists("child", $context) ? $context["child"] : (function () { throw new TwigErrorRuntime('Variable "child" does not exist.', 453, $this->getSourceContext()); })()), 'errors');
            echo "
                </div>
            ";
        }
        // line 456
        echo "        </div>
    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internalfafa88b1cd17c041f9cb8309681b0a6ff3e77aad4b82fd59a65ec050775b6fa5->leave($internalfafa88b1cd17c041f9cb8309681b0a6ff3e77aad4b82fd59a65ec050775b6fa5prof);

        
        $internalfb75a77185857fcaab2fa7882a3f08716bb70c384e7ab4c2bc0e89c561fed605->leave($internalfb75a77185857fcaab2fa7882a3f08716bb70c384e7ab4c2bc0e89c561fed605prof);

    }

    // line 460
    public function blocksonatatypemodelautocompletewidget($context, array $blocks = array())
    {
        $internale9e83d7db833425133a9f81e0fd1e945029d3200c8a8141f2b059bc4d1829389 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale9e83d7db833425133a9f81e0fd1e945029d3200c8a8141f2b059bc4d1829389->enter($internale9e83d7db833425133a9f81e0fd1e945029d3200c8a8141f2b059bc4d1829389prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletewidget"));

        $internal17a476d61d7c8858d165c85056cf0456585e34bbb3dda33f317cfeb4a48fa901 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal17a476d61d7c8858d165c85056cf0456585e34bbb3dda33f317cfeb4a48fa901->enter($internal17a476d61d7c8858d165c85056cf0456585e34bbb3dda33f317cfeb4a48fa901prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelautocompletewidget"));

        // line 461
        echo "    ";
        $this->loadTemplate((isset($context["template"]) || arraykeyexists("template", $context) ? $context["template"] : (function () { throw new TwigErrorRuntime('Variable "template" does not exist.', 461, $this->getSourceContext()); })()), "SonataAdminBundle:Form:formadminfields.html.twig", 461)->display($context);
        
        $internal17a476d61d7c8858d165c85056cf0456585e34bbb3dda33f317cfeb4a48fa901->leave($internal17a476d61d7c8858d165c85056cf0456585e34bbb3dda33f317cfeb4a48fa901prof);

        
        $internale9e83d7db833425133a9f81e0fd1e945029d3200c8a8141f2b059bc4d1829389->leave($internale9e83d7db833425133a9f81e0fd1e945029d3200c8a8141f2b059bc4d1829389prof);

    }

    // line 464
    public function blocksonatatypechoicefieldmaskwidget($context, array $blocks = array())
    {
        $internal7a7cc10ba254ac8861f6a831e6c13d1e3244b09c8f394eaef6747fc07e5d3ee2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7a7cc10ba254ac8861f6a831e6c13d1e3244b09c8f394eaef6747fc07e5d3ee2->enter($internal7a7cc10ba254ac8861f6a831e6c13d1e3244b09c8f394eaef6747fc07e5d3ee2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypechoicefieldmaskwidget"));

        $internal3afae9e7bf5a973f530b655b2f65db0ce1ae94eba8978b20d4e5542fabcf5378 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3afae9e7bf5a973f530b655b2f65db0ce1ae94eba8978b20d4e5542fabcf5378->enter($internal3afae9e7bf5a973f530b655b2f65db0ce1ae94eba8978b20d4e5542fabcf5378prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypechoicefieldmaskwidget"));

        // line 465
        echo "    ";
        $this->displayBlock("choicewidget", $context, $blocks);
        echo "
    ";
        // line 467
        echo "    ";
        $context["mainformname"] = twigslice($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 467, $this->getSourceContext()); })()), 0, ((twiglengthfilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 467, $this->getSourceContext()); })())) - twiglengthfilter($this->env, (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 467, $this->getSourceContext()); })()))) - 1));
        // line 468
        echo "    <script>
        jQuery(document).ready(function() {
            var allFields = ";
        // line 470
        echo jsonencode((isset($context["allfields"]) || arraykeyexists("allfields", $context) ? $context["allfields"] : (function () { throw new TwigErrorRuntime('Variable "allfields" does not exist.', 470, $this->getSourceContext()); })()));
        echo ";
            var map = ";
        // line 471
        echo jsonencode((isset($context["map"]) || arraykeyexists("map", $context) ? $context["map"] : (function () { throw new TwigErrorRuntime('Variable "map" does not exist.', 471, $this->getSourceContext()); })()));
        echo ";

            var showMaskChoiceEl = jQuery('#";
        // line 473
        echo twigescapefilter($this->env, (isset($context["mainformname"]) || arraykeyexists("mainformname", $context) ? $context["mainformname"] : (function () { throw new TwigErrorRuntime('Variable "mainformname" does not exist.', 473, $this->getSourceContext()); })()), "html", null, true);
        echo "";
        echo twigescapefilter($this->env, (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 473, $this->getSourceContext()); })()), "html", null, true);
        echo "');

            showMaskChoiceEl.on('change', function () {
                choicefieldmaskshow(jQuery(this).val());
            });

            function choicefieldmaskshow(val) {
                var controlGroupIdFunc = function (field) {
                    // Most of fields are named with an underscore
                    var defaultFieldId = '#sonata-ba-field-container-";
        // line 482
        echo twigescapefilter($this->env, (isset($context["mainformname"]) || arraykeyexists("mainformname", $context) ? $context["mainformname"] : (function () { throw new TwigErrorRuntime('Variable "mainformname" does not exist.', 482, $this->getSourceContext()); })()), "html", null, true);
        echo "' + field;

                    // Some fields may be named with a dash (like keys of immutable array form type)
                    if (jQuery(defaultFieldId).length === 0) {
                        return '#sonata-ba-field-container-";
        // line 486
        echo twigescapefilter($this->env, (isset($context["mainformname"]) || arraykeyexists("mainformname", $context) ? $context["mainformname"] : (function () { throw new TwigErrorRuntime('Variable "mainformname" does not exist.', 486, $this->getSourceContext()); })()), "html", null, true);
        echo "-' + field;
                    }

                    return defaultFieldId;
                };

                jQuery.each(allFields, function (i, field) {
                    jQuery(controlGroupIdFunc(field)).hide();
                });

                if (map[val]) {
                    jQuery.each(map[val], function (i, field) {
                        jQuery(controlGroupIdFunc(field)).show();
                    });
                }
            }
            choicefieldmaskshow(showMaskChoiceEl.val());
        });

    </script>
";
        
        $internal3afae9e7bf5a973f530b655b2f65db0ce1ae94eba8978b20d4e5542fabcf5378->leave($internal3afae9e7bf5a973f530b655b2f65db0ce1ae94eba8978b20d4e5542fabcf5378prof);

        
        $internal7a7cc10ba254ac8861f6a831e6c13d1e3244b09c8f394eaef6747fc07e5d3ee2->leave($internal7a7cc10ba254ac8861f6a831e6c13d1e3244b09c8f394eaef6747fc07e5d3ee2prof);

    }

    // line 508
    public function blocksonatatypechoicemultiplesortable($context, array $blocks = array())
    {
        $internaleaf9129df13aa61c9f028d05435a9d7760c3957f3e880e469bfecf20d06cfed0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleaf9129df13aa61c9f028d05435a9d7760c3957f3e880e469bfecf20d06cfed0->enter($internaleaf9129df13aa61c9f028d05435a9d7760c3957f3e880e469bfecf20d06cfed0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypechoicemultiplesortable"));

        $internalb0685722da1eb66f71e96a79ee9b05c637b490781b89abe01afe31e1738d20ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb0685722da1eb66f71e96a79ee9b05c637b490781b89abe01afe31e1738d20ea->enter($internalb0685722da1eb66f71e96a79ee9b05c637b490781b89abe01afe31e1738d20eaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypechoicemultiplesortable"));

        // line 509
        echo "    <input type=\"hidden\" name=\"";
        echo twigescapefilter($this->env, (isset($context["fullname"]) || arraykeyexists("fullname", $context) ? $context["fullname"] : (function () { throw new TwigErrorRuntime('Variable "fullname" does not exist.', 509, $this->getSourceContext()); })()), "html", null, true);
        echo "\" id=\"";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 509, $this->getSourceContext()); })()), "html", null, true);
        echo "\" value=\"";
        echo twigescapefilter($this->env, twigjoinfilter((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 509, $this->getSourceContext()); })()), ","), "html", null, true);
        echo "\" />

    <script>
        jQuery(document).ready(function() {
            Admin.setupsortableselect2(jQuery('#";
        // line 513
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 513, $this->getSourceContext()); })()), "html", null, true);
        echo "'), ";
        echo jsonencode(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 513, $this->getSourceContext()); })()), "vars", array()), "choices", array()));
        echo ");
        });
    </script>
";
        
        $internalb0685722da1eb66f71e96a79ee9b05c637b490781b89abe01afe31e1738d20ea->leave($internalb0685722da1eb66f71e96a79ee9b05c637b490781b89abe01afe31e1738d20eaprof);

        
        $internaleaf9129df13aa61c9f028d05435a9d7760c3957f3e880e469bfecf20d06cfed0->leave($internaleaf9129df13aa61c9f028d05435a9d7760c3957f3e880e469bfecf20d06cfed0prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Form:formadminfields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1660 => 513,  1648 => 509,  1639 => 508,  1608 => 486,  1601 => 482,  1587 => 473,  1582 => 471,  1578 => 470,  1574 => 468,  1571 => 467,  1566 => 465,  1557 => 464,  1546 => 461,  1537 => 460,  1525 => 456,  1519 => 453,  1516 => 452,  1514 => 451,  1510 => 449,  1507 => 448,  1495 => 447,  1492 => 446,  1489 => 445,  1486 => 444,  1483 => 443,  1481 => 442,  1476 => 440,  1464 => 438,  1461 => 437,  1452 => 436,  1438 => 431,  1435 => 430,  1418 => 428,  1401 => 427,  1396 => 425,  1391 => 424,  1388 => 423,  1379 => 422,  1367 => 418,  1363 => 416,  1361 => 415,  1356 => 414,  1339 => 412,  1322 => 411,  1318 => 410,  1313 => 409,  1310 => 408,  1307 => 407,  1304 => 406,  1301 => 405,  1298 => 404,  1295 => 403,  1292 => 402,  1290 => 401,  1281 => 400,  1269 => 396,  1264 => 393,  1262 => 392,  1257 => 391,  1247 => 383,  1245 => 382,  1242 => 381,  1240 => 380,  1231 => 379,  1220 => 376,  1218 => 375,  1209 => 374,  1198 => 371,  1196 => 370,  1187 => 369,  1175 => 365,  1169 => 363,  1167 => 362,  1164 => 361,  1158 => 358,  1155 => 357,  1153 => 356,  1150 => 355,  1147 => 354,  1143 => 353,  1140 => 352,  1137 => 351,  1134 => 350,  1132 => 349,  1129 => 348,  1126 => 347,  1123 => 346,  1121 => 345,  1118 => 344,  1112 => 342,  1110 => 341,  1107 => 340,  1104 => 339,  1101 => 338,  1098 => 337,  1095 => 336,  1092 => 335,  1089 => 334,  1086 => 333,  1083 => 332,  1080 => 331,  1077 => 330,  1075 => 329,  1073 => 328,  1070 => 327,  1067 => 326,  1064 => 325,  1062 => 324,  1059 => 323,  1057 => 322,  1054 => 321,  1051 => 320,  1048 => 319,  1046 => 318,  1037 => 317,  1034 => 316,  1025 => 315,  1012 => 310,  1006 => 308,  1000 => 305,  997 => 304,  995 => 303,  992 => 302,  986 => 300,  980 => 297,  977 => 296,  975 => 295,  970 => 293,  966 => 292,  961 => 291,  958 => 290,  952 => 288,  949 => 287,  947 => 286,  938 => 285,  925 => 280,  919 => 277,  914 => 276,  911 => 275,  905 => 272,  900 => 271,  898 => 270,  893 => 268,  889 => 267,  884 => 266,  881 => 265,  878 => 264,  875 => 263,  872 => 262,  866 => 260,  863 => 259,  861 => 258,  852 => 257,  838 => 251,  836 => 250,  835 => 249,  834 => 248,  833 => 247,  828 => 246,  825 => 245,  822 => 244,  819 => 243,  816 => 242,  810 => 240,  807 => 239,  805 => 238,  796 => 237,  780 => 231,  777 => 230,  774 => 229,  768 => 227,  766 => 226,  761 => 225,  758 => 224,  755 => 223,  751 => 221,  748 => 219,  745 => 217,  743 => 216,  736 => 215,  734 => 214,  731 => 213,  728 => 211,  725 => 209,  723 => 208,  716 => 207,  714 => 206,  706 => 205,  700 => 203,  697 => 202,  695 => 201,  692 => 200,  689 => 199,  686 => 198,  683 => 197,  680 => 196,  677 => 195,  675 => 194,  666 => 193,  654 => 189,  647 => 187,  644 => 186,  641 => 185,  637 => 184,  632 => 183,  629 => 182,  627 => 181,  618 => 180,  606 => 176,  603 => 174,  601 => 173,  599 => 172,  597 => 171,  595 => 170,  580 => 169,  577 => 168,  574 => 167,  571 => 166,  568 => 165,  565 => 164,  562 => 163,  559 => 162,  556 => 161,  553 => 160,  550 => 159,  548 => 158,  545 => 157,  542 => 156,  539 => 155,  530 => 154,  520 => 151,  511 => 150,  501 => 147,  492 => 146,  479 => 141,  473 => 139,  470 => 137,  468 => 136,  466 => 135,  464 => 134,  449 => 133,  446 => 132,  442 => 129,  439 => 126,  438 => 125,  437 => 124,  435 => 123,  433 => 122,  430 => 121,  427 => 120,  424 => 119,  421 => 118,  418 => 117,  415 => 116,  413 => 115,  410 => 114,  407 => 113,  405 => 112,  402 => 111,  400 => 110,  397 => 109,  394 => 108,  391 => 107,  388 => 106,  386 => 105,  377 => 104,  366 => 99,  364 => 98,  362 => 97,  359 => 95,  357 => 94,  355 => 93,  346 => 92,  335 => 88,  333 => 87,  331 => 86,  328 => 84,  326 => 83,  324 => 82,  315 => 81,  300 => 75,  297 => 74,  294 => 73,  291 => 72,  282 => 71,  271 => 68,  267 => 66,  265 => 65,  262 => 64,  259 => 63,  257 => 62,  251 => 60,  249 => 59,  247 => 58,  244 => 57,  241 => 56,  238 => 54,  236 => 53,  227 => 52,  214 => 49,  211 => 48,  202 => 47,  189 => 44,  186 => 43,  183 => 42,  180 => 41,  177 => 40,  168 => 39,  158 => 36,  154 => 35,  145 => 34,  130 => 29,  128 => 28,  126 => 27,  117 => 26,  106 => 23,  102 => 22,  99 => 21,  90 => 19,  86 => 18,  83 => 17,  78 => 16,  76 => 15,  67 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'formdivlayout.html.twig' %}

{% block formerrors -%}
    {% if errors|length > 0 %}
        {% if not form.parent %}<div class=\"alert alert-danger\">{% endif %}
            <ul class=\"list-unstyled\">
                {% for error in errors %}
                    <li><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\"></i> {{ error.message }}</li>
                {% endfor %}
            </ul>
        {% if not form.parent %}</div>{% endif %}
    {% endif %}
{%- endblock formerrors %}

{% block sonatahelp %}
{% spaceless %}
{% if sonatahelp is defined and sonatahelp %}
    <span class=\"help-block sonata-ba-field-widget-help\">{{ sonatahelp|raw }}</span>
{% endif %}
{% endspaceless %}
{% endblock %}

{% block formwidget -%}
    {{ parent() }}
    {{ block('sonatahelp') }}
{%- endblock formwidget %}

{% block formwidgetsimple %}
    {% set type = type|default('text') %}
    {% if type != 'file' %}
        {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {% endif %}
    {{ parent() }}
{% endblock formwidgetsimple %}

{% block textareawidget %}
    {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {{ parent() }}
{% endblock textareawidget %}

{% block moneywidget -%}
    {% if moneypattern == '{{ widget }}' %}
        {{- block('formwidgetsimple') -}}
    {% else %}
        {% set currencySymbol = moneypattern|replace({'{{ widget }}': ''})|trim %}
        {% if moneypattern matches '/^{{ widget }}/' %}
            <div class=\"input-group\">
                {{- block('formwidgetsimple') -}}
                <span class=\"input-group-addon\">{{ currencySymbol }}</span>
            </div>
        {% elseif moneypattern matches '/{{ widget }}\$/' %}
            <div class=\"input-group\">
                <span class=\"input-group-addon\">{{ currencySymbol }}</span>
                {{- block('formwidgetsimple') -}}
            </div>
        {% endif %}
    {% endif %}
{%- endblock moneywidget %}

{% block percentwidget %}
    {% spaceless %}
        {% set type = type|default('text') %}
        <div class=\"input-group\">
            {{ block('formwidgetsimple') }}
            <span class=\"input-group-addon\">%</span>
        </div>
    {% endspaceless %}
{% endblock percentwidget %}

{% block checkboxwidget -%}
    {% set parentlabelclass = parentlabelclass|default('') -%}
    {% if 'checkbox-inline' in parentlabelclass %}
        {{- formlabel(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- formlabel(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock checkboxwidget %}

{% block radiowidget -%}
    {%- set parentlabelclass = parentlabelclass|default('') -%}
    {% if 'radio-inline' in parentlabelclass %}
        {{- formlabel(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"radio\">
            {{- formlabel(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock radiowidget %}

{# Labels #}
{% block formlabel %}
{% spaceless %}
    {% if label is not same as(false) and sonataadmin.options['formtype'] == 'horizontal' %}
        {% set labelclass = 'col-sm-3' %}
    {% endif %}

    {% set labelclass = labelclass|default('') ~ ' control-label' %}

    {% if label is not same as(false) %}
        {% set labelattr = labelattr|merge({'class': labelattr.class|default('') ~ labelclass }) %}

        {% if not compound %}
            {% set labelattr = labelattr|merge({'for': id}) %}
        {% endif %}
        {% if required %}
            {% set labelattr = labelattr|merge({'class': (labelattr.class|default('') ~ ' required')|trim}) %}
        {% endif %}

        {% if label is empty %}
            {%- if labelformat is defined and labelformat is not empty -%}
                {% set label = labelformat|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {% endif %}

        <label{% for attrname, attrvalue in labelattr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {% if translationdomain is same as(false) %}
                {{- label -}}
            {% elseif not sonataadmin.admin %}
                {{- label|trans({}, translationdomain) -}}
            {% else %}
                {{ label|trans({}, sonataadmin.fielddescription.translationDomain ?: admin.translationDomain) }}
            {% endif %}
        </label>
    {% endif %}
{% endspaceless %}
{% endblock formlabel %}

{% block checkboxlabel -%}
    {{- block('checkboxradiolabel') -}}
{%- endblock checkboxlabel %}

{% block radiolabel -%}
    {{- block('checkboxradiolabel') -}}
{%- endblock radiolabel %}

{% block checkboxradiolabel %}
    {% if sonataadmin.admin %}
        {% set translationdomain = sonataadmin.fielddescription.translationDomain %}
    {% endif %}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {% if widget is defined %}
        {% if required %}
            {% set labelattr = labelattr|merge({class: (labelattr.class|default('') ~ ' required')|trim}) %}
        {% endif %}
        {% if parentlabelclass is defined %}
            {% set labelattr = labelattr|merge({class: (labelattr.class|default('') ~ ' ' ~ parentlabelclass)|trim}) %}
        {% endif %}
        {% if label is not same as(false) and label is empty %}
            {% set label = name|humanize %}
        {% endif %}
        <label{% for attrname, attrvalue in labelattr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw -}}
            {%- if label is not same as(false) -%}
                <span class=\"control-labeltext\">
                    {{- label|trans({}, translationdomain) -}}
                </span>
            {%- endif -%}
        </label>
    {% endif %}
{% endblock checkboxradiolabel %}

{% block choicewidgetexpanded %}
{% spaceless %}
    {% set attr = attr|merge({'class': attr.class|default('') ~ ' list-unstyled'}) %}
    <ul {{ block('widgetcontainerattributes') }}>
    {% for child in form %}
        <li>
            {{ formwidget(child, {'horizontal': false, 'horizontalinputwrapperclass': ''}) }} {# {'horizontal': false, 'horizontalinputwrapperclass': ''} needed to avoid MopaBootstrapBundle messing with the DOM #}
        </li>
    {% endfor %}
    </ul>
{% endspaceless %}
{% endblock choicewidgetexpanded %}

{% block choicewidgetcollapsed %}
{% spaceless %}
    {% if required and placeholder is defined and placeholder is none %}
        {% set required = false %}
    {% elseif required and emptyvalue is defined and emptyvalueinchoices is defined and emptyvalue is none and not emptyvalueinchoices and not multiple %}
        {% set required = false %}
    {% endif %}

    {% set attr = attr|merge({'class': attr.class|default('') ~ ' form-control'}) %}
    {% if (sortable is defined) and sortable and multiple %}
        {{ block('sonatatypechoicemultiplesortable') }}
    {% else %}
        <select {{ block('widgetattributes') }}{% if multiple %} multiple=\"multiple\"{% endif %} >
            {% if emptyvalue is defined and emptyvalue is not none %}
                <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>
                    {% if not sonataadmin.admin %}
                        {{- emptyvalue|trans({}, translationdomain) -}}
                    {% else %}
                        {{- emptyvalue|trans({}, sonataadmin.fielddescription.translationDomain) -}}
                    {% endif%}
                </option>
            {% elseif placeholder is defined and placeholder is not none %}
                <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>
                    {% if not sonataadmin.admin %}
                        {{- placeholder|trans({}, translationdomain) -}}
                    {% else %}
                        {{- placeholder|trans({}, sonataadmin.fielddescription.translationDomain) -}}
                    {% endif%}
                </option>
            {% endif %}
            {% if preferredchoices|length > 0 %}
                {% set options = preferredchoices %}
                {{ block('choicewidgetoptions') }}
                {% if choices|length > 0 %}
                    <option disabled=\"disabled\">{{ separator }}</option>
                {% endif %}
            {% endif %}
            {% set options = choices %}
            {{ block('choicewidgetoptions') }}
        </select>
    {% endif %}
{% endspaceless %}
{% endblock choicewidgetcollapsed %}

{% block datewidget %}
{% spaceless %}
    {% if widget == 'singletext' %}
        {{ block('formwidgetsimple') }}
    {% else %}
        {% if row is not defined or row == true %}
            {% set attr = attr|merge({'class': attr.class|default('') ~ ' row' }) %}
        {% endif %}
        {% set inputwrapperclass = inputwrapperclass|default('col-sm-4') %}
        <div {{ block('widgetcontainerattributes') }}>
            {{ datepattern|replace({
                '{{ year }}':  '<div class=\"'~ inputwrapperclass ~ '\">' ~ formwidget(form.year) ~ '</div>',
                '{{ month }}': '<div class=\"'~ inputwrapperclass ~ '\">' ~ formwidget(form.month) ~ '</div>',
                '{{ day }}':   '<div class=\"'~ inputwrapperclass ~ '\">' ~ formwidget(form.day) ~ '</div>',
            })|raw }}
        </div>
    {% endif %}
{% endspaceless %}
{% endblock datewidget %}

{% block timewidget %}
{% spaceless %}
    {% if widget == 'singletext' %}
        {{ block('formwidgetsimple') }}
    {% else %}
        {% if row is not defined or row == true %}
            {% set attr = attr|merge({'class': attr.class|default('') ~ ' row' }) %}
        {% endif %}
        {% set inputwrapperclass = inputwrapperclass|default('col-sm-6') %}
        <div {{ block('widgetcontainerattributes') }}>
            <div class=\"{{ inputwrapperclass }}\">
                {{ formwidget(form.hour) }}
            </div>
            {% if withminutes %}
                <div class=\"{{ inputwrapperclass }}\">
                    {{ formwidget(form.minute) }}
                </div>
            {% endif %}
            {% if withseconds %}
                <div class=\"{{ inputwrapperclass }}\">
                    {{ formwidget(form.second) }}
                </div>
            {% endif %}
        </div>
    {% endif %}
{% endspaceless %}
{% endblock timewidget %}

{% block datetimewidget %}
{% spaceless %}
    {% if widget == 'singletext' %}
        {{ block('formwidgetsimple') }}
    {% else %}
        {% set attr = attr|merge({'class': attr.class|default('') ~ ' row' }) %}
        <div {{ block('widgetcontainerattributes') }}>
            {{ formerrors(form.date) }}
            {{ formerrors(form.time) }}

            {% if form.date.vars.widget == 'singletext' %}
                <div class=\"col-sm-2\">
                    {{ formwidget(form.date) }}
                </div>
            {% else %}
                {{ formwidget(form.date, {'row': false, 'inputwrapperclass': 'col-sm-2'}) }}
            {% endif %}

            {% if form.time.vars.widget == 'singletext' %}
                <div class=\"col-sm-2\">
                    {{ formwidget(form.time) }}
                </div>
            {% else %}
                {{ formwidget(form.time, {'row': false, 'inputwrapperclass': 'col-sm-2'}) }}
            {% endif %}
        </div>
    {% endif %}
{% endspaceless %}
{% endblock datetimewidget %}

{% block formrow %}
    {% set showlabel = showlabel|default(true) %}
    <div class=\"form-group{% if errors|length > 0 %} has-error{% endif %}\" id=\"sonata-ba-field-container-{{ id }}\">
        {% if sonataadmin.fielddescription.options is defined %}
            {% set label = sonataadmin.fielddescription.options.name|default(label)  %}
        {% endif %}

        {% set divclass = 'sonata-ba-field' %}

        {% if label is same as(false) %}
            {% set divclass = divclass ~ ' sonata-collection-row-without-label' %}
        {% endif %}

        {% if sonataadmin is defined and sonataadmin.options['formtype'] == 'horizontal' %}
            {# Add an offset if no label or is a checkbox/radio #}
            {% if label is same as(false) or form.vars.checked is defined %}
                {% if 'collection' in form.parent.vars.blockprefixes %}
                    {% set divclass = divclass ~ ' col-sm-12' %}
                {% else %}
                    {% set divclass = divclass ~ ' col-sm-9 col-sm-offset-3' %}
                {% endif %}
            {% else %}
                {% set divclass = divclass ~ ' col-sm-9' %}
            {% endif %}
        {% endif %}

        {% if showlabel %}
            {{ formlabel(form, label|default(null)) }}
        {% endif %}

        {% if sonataadmin is defined and sonataadminenabled %}
            {% set divclass = divclass ~ ' sonata-ba-field-' ~ sonataadmin.edit ~ '-' ~ sonataadmin.inline %}
        {% endif %}

        {% if errors|length > 0 %}
            {% set divclass = divclass ~ ' sonata-ba-field-error' %}
        {% endif %}

        <div class=\"{{ divclass }}\">
            {{ formwidget(form, {'horizontal': false, 'horizontalinputwrapperclass': ''}) }} {# {'horizontal': false, 'horizontalinputwrapperclass': ''} needed to avoid MopaBootstrapBundle messing with the DOM #}

            {% if errors|length > 0 %}
                <div class=\"help-block sonata-ba-field-error-messages\">
                    {{ formerrors(form) }}
                </div>
            {% endif %}

            {% if sonataadmin is defined and sonataadminenabled and sonataadmin.fielddescription.help|default(false) %}
                <span class=\"help-block sonata-ba-field-help\">{{ sonataadmin.fielddescription.help|trans({}, sonataadmin.fielddescription.translationDomain ?: admin.translationDomain)|raw }}</span>
            {% endif %}
        </div>
    </div>
{% endblock formrow %}

{% block checkboxrow -%}
    {% set showlabel = false %}
    {{ block('formrow') }}
{%- endblock checkboxrow %}

{% block radiorow -%}
    {% set showlabel = false %}
    {{ block('formrow') }}
{%- endblock radiorow %}

{% block sonatatypenativecollectionwidgetrow %}
{% spaceless %}
    <div class=\"sonata-collection-row\">
        {% if allowdelete %}
            <div class=\"row\">
                <div class=\"col-xs-1\">
                    <a href=\"#\" class=\"btn btn-link sonata-collection-delete\">
                        <i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>
                    </a>
                </div>
                <div class=\"col-xs-11\">
        {% endif %}
            {{ formrow(child, { label: false }) }}
        {% if allowdelete %}
                </div>
            </div>
        {% endif %}
    </div>
{% endspaceless %}
{% endblock sonatatypenativecollectionwidgetrow %}

{% block sonatatypenativecollectionwidget %}
{% spaceless %}
    {% if prototype is defined %}
        {% set child = prototype %}
        {% set allowdeletebackup = allowdelete %}
        {% set allowdelete = true %}
        {% set attr = attr|merge({'data-prototype': block('sonatatypenativecollectionwidgetrow'), 'data-prototype-name': prototype.vars.name, 'class': attr.class|default('') }) %}
        {% set allowdelete = allowdeletebackup %}
    {% endif %}
    <div {{ block('widgetcontainerattributes') }}>
        {{ formerrors(form) }}
        {% for child in form %}
            {{ block('sonatatypenativecollectionwidgetrow') }}
        {% endfor %}
        {{ formrest(form) }}
        {% if allowadd %}
            <div><a href=\"#\" class=\"btn btn-link sonata-collection-add\"><i class=\"fa fa-plus-circle\" aria-hidden=\"true\"></i></a></div>
        {% endif %}
    </div>
{% endspaceless %}
{% endblock sonatatypenativecollectionwidget %}

{% block sonatatypeimmutablearraywidget %}
    {% spaceless %}
        <div {{ block('widgetcontainerattributes') }}>
            {{ formerrors(form) }}

            {% for key, child in form %}
                {{ block('sonatatypeimmutablearraywidgetrow') }}
            {% endfor %}

            {{ formrest(form) }}
        </div>
    {% endspaceless %}
{% endblock sonatatypeimmutablearraywidget %}

{% block sonatatypeimmutablearraywidgetrow %}
    {% spaceless %}
        <div class=\"form-group{% if child.vars.errors|length > 0%} error{%endif%}\" id=\"sonata-ba-field-container-{{ id }}-{{ key }}\">

            {{ formlabel(child) }}

            {% set divclass = \"\" %}
            {% if sonataadmin.options['formtype'] == 'horizontal' %}
                {% set divclass = 'col-sm-9' %}
            {% endif%}

            <div class=\"{{ divclass }} sonata-ba-field sonata-ba-field-{{ sonataadmin.edit }}-{{ sonataadmin.inline }} {% if child.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">
                {{ formwidget(child, {'horizontal': false, 'horizontalinputwrapperclass': ''}) }} {# {'horizontal': false, 'horizontalinputwrapperclass': ''} needed to avoid MopaBootstrapBundle messing with the DOM #}
            </div>

            {% if child.vars.errors|length > 0 %}
                <div class=\"help-block sonata-ba-field-error-messages\">
                    {{ formerrors(child) }}
                </div>
            {% endif %}
        </div>
    {% endspaceless %}
{% endblock %}

{% block sonatatypemodelautocompletewidget %}
    {% include template %}
{% endblock sonatatypemodelautocompletewidget %}

{% block sonatatypechoicefieldmaskwidget %}
    {{ block('choicewidget') }}
    {# Taking the form name excluding ending field glue character #}
    {% set mainformname = id|slice(0, (id|length - name|length)-1) %}
    <script>
        jQuery(document).ready(function() {
            var allFields = {{ allfields|jsonencode|raw }};
            var map = {{ map|jsonencode|raw }};

            var showMaskChoiceEl = jQuery('#{{ mainformname }}{{ name }}');

            showMaskChoiceEl.on('change', function () {
                choicefieldmaskshow(jQuery(this).val());
            });

            function choicefieldmaskshow(val) {
                var controlGroupIdFunc = function (field) {
                    // Most of fields are named with an underscore
                    var defaultFieldId = '#sonata-ba-field-container-{{ mainformname }}' + field;

                    // Some fields may be named with a dash (like keys of immutable array form type)
                    if (jQuery(defaultFieldId).length === 0) {
                        return '#sonata-ba-field-container-{{ mainformname }}-' + field;
                    }

                    return defaultFieldId;
                };

                jQuery.each(allFields, function (i, field) {
                    jQuery(controlGroupIdFunc(field)).hide();
                });

                if (map[val]) {
                    jQuery.each(map[val], function (i, field) {
                        jQuery(controlGroupIdFunc(field)).show();
                    });
                }
            }
            choicefieldmaskshow(showMaskChoiceEl.val());
        });

    </script>
{% endblock %}

{%  block sonatatypechoicemultiplesortable %}
    <input type=\"hidden\" name=\"{{ fullname }}\" id=\"{{ id }}\" value=\"{{ value|join(',') }}\" />

    <script>
        jQuery(document).ready(function() {
            Admin.setupsortableselect2(jQuery('#{{ id }}'), {{ form.vars.choices|jsonencode|raw }});
        });
    </script>
{% endblock %}
", "SonataAdminBundle:Form:formadminfields.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Form/formadminfields.html.twig");
    }
}
