<?php

/* @Framework/Form/searchwidget.html.php */
class TwigTemplatea0d5943e89af59669497d15da8763bc9e7326756bf5c133d6c76d82b9f7e86c6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal24ccf3855ddd235a1ebf9fdaeb603867455a966c89d31e2a373e2ac77553d209 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal24ccf3855ddd235a1ebf9fdaeb603867455a966c89d31e2a373e2ac77553d209->enter($internal24ccf3855ddd235a1ebf9fdaeb603867455a966c89d31e2a373e2ac77553d209prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/searchwidget.html.php"));

        $internale56794c6108424ef791f67f80affd848261a42951914934dca09ccfab74d6b0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale56794c6108424ef791f67f80affd848261a42951914934dca09ccfab74d6b0d->enter($internale56794c6108424ef791f67f80affd848261a42951914934dca09ccfab74d6b0dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/searchwidget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $internal24ccf3855ddd235a1ebf9fdaeb603867455a966c89d31e2a373e2ac77553d209->leave($internal24ccf3855ddd235a1ebf9fdaeb603867455a966c89d31e2a373e2ac77553d209prof);

        
        $internale56794c6108424ef791f67f80affd848261a42951914934dca09ccfab74d6b0d->leave($internale56794c6108424ef791f67f80affd848261a42951914934dca09ccfab74d6b0dprof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/searchwidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->block(\$form, 'formwidgetsimple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/searchwidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/searchwidget.html.php");
    }
}
