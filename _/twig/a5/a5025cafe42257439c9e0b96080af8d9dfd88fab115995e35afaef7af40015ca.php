<?php

/* SonataAdminBundle::standardlayout.html.twig */
class TwigTemplate44ebd81767e2841e3777428bf2565dd27ece37e55a5a3f9e074fa62cf77bdb66 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'htmlattributes' => array($this, 'blockhtmlattributes'),
            'metatags' => array($this, 'blockmetatags'),
            'stylesheets' => array($this, 'blockstylesheets'),
            'javascripts' => array($this, 'blockjavascripts'),
            'sonatajavascriptconfig' => array($this, 'blocksonatajavascriptconfig'),
            'sonatajavascriptpool' => array($this, 'blocksonatajavascriptpool'),
            'sonataheadtitle' => array($this, 'blocksonataheadtitle'),
            'bodyattributes' => array($this, 'blockbodyattributes'),
            'sonataheader' => array($this, 'blocksonataheader'),
            'sonataheadernoscriptwarning' => array($this, 'blocksonataheadernoscriptwarning'),
            'logo' => array($this, 'blocklogo'),
            'sonatanav' => array($this, 'blocksonatanav'),
            'sonatabreadcrumb' => array($this, 'blocksonatabreadcrumb'),
            'sonatatopnavmenu' => array($this, 'blocksonatatopnavmenu'),
            'sonatatopnavmenuaddblock' => array($this, 'blocksonatatopnavmenuaddblock'),
            'sonatatopnavmenuuserblock' => array($this, 'blocksonatatopnavmenuuserblock'),
            'sonatawrapper' => array($this, 'blocksonatawrapper'),
            'sonataleftside' => array($this, 'blocksonataleftside'),
            'sonatasidenav' => array($this, 'blocksonatasidenav'),
            'sonatasidebarsearch' => array($this, 'blocksonatasidebarsearch'),
            'sidebarbeforenav' => array($this, 'blocksidebarbeforenav'),
            'sidebarnav' => array($this, 'blocksidebarnav'),
            'sidebarafternav' => array($this, 'blocksidebarafternav'),
            'sidebarafternavcontent' => array($this, 'blocksidebarafternavcontent'),
            'sonatapagecontent' => array($this, 'blocksonatapagecontent'),
            'sonatapagecontentheader' => array($this, 'blocksonatapagecontentheader'),
            'sonatapagecontentnav' => array($this, 'blocksonatapagecontentnav'),
            'tabmenunavbarheader' => array($this, 'blocktabmenunavbarheader'),
            'sonataadmincontentactionswrappers' => array($this, 'blocksonataadmincontentactionswrappers'),
            'sonataadmincontent' => array($this, 'blocksonataadmincontent'),
            'notice' => array($this, 'blocknotice'),
            'bootlint' => array($this, 'blockbootlint'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal700495c7e4ab7d505b51d3438284d2e3a79e1d391afc43bd99f5ee388a710a4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal700495c7e4ab7d505b51d3438284d2e3a79e1d391afc43bd99f5ee388a710a4c->enter($internal700495c7e4ab7d505b51d3438284d2e3a79e1d391afc43bd99f5ee388a710a4cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::standardlayout.html.twig"));

        $internald90762735adbf761dc4d63a931b143ee082ba76120219575f72caa368893854c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald90762735adbf761dc4d63a931b143ee082ba76120219575f72caa368893854c->enter($internald90762735adbf761dc4d63a931b143ee082ba76120219575f72caa368893854cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::standardlayout.html.twig"));

        // line 11
        echo "
";
        // line 12
        $context["preview"] = ((        $this->hasBlock("preview", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("preview", $context, $blocks))) : (null));
        // line 13
        $context["form"] = ((        $this->hasBlock("form", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("form", $context, $blocks))) : (null));
        // line 14
        $context["show"] = ((        $this->hasBlock("show", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("show", $context, $blocks))) : (null));
        // line 15
        $context["listtable"] = ((        $this->hasBlock("listtable", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listtable", $context, $blocks))) : (null));
        // line 16
        $context["listfilters"] = ((        $this->hasBlock("listfilters", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listfilters", $context, $blocks))) : (null));
        // line 17
        $context["tabmenu"] = ((        $this->hasBlock("tabmenu", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("tabmenu", $context, $blocks))) : (null));
        // line 18
        $context["content"] = ((        $this->hasBlock("content", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("content", $context, $blocks))) : (null));
        // line 19
        $context["title"] = ((        $this->hasBlock("title", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("title", $context, $blocks))) : (null));
        // line 20
        $context["breadcrumb"] = ((        $this->hasBlock("breadcrumb", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("breadcrumb", $context, $blocks))) : (null));
        // line 21
        $context["actions"] = ((        $this->hasBlock("actions", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("actions", $context, $blocks))) : (null));
        // line 22
        $context["navbartitle"] = ((        $this->hasBlock("navbartitle", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("navbartitle", $context, $blocks))) : (null));
        // line 23
        $context["listfiltersactions"] = ((        $this->hasBlock("listfiltersactions", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listfiltersactions", $context, $blocks))) : (null));
        // line 24
        echo "
<!DOCTYPE html>
<html ";
        // line 26
        $this->displayBlock('htmlattributes', $context, $blocks);
        echo ">
    <head>
        ";
        // line 28
        $this->displayBlock('metatags', $context, $blocks);
        // line 33
        echo "
        ";
        // line 34
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 39
        echo "
        ";
        // line 40
        $this->displayBlock('javascripts', $context, $blocks);
        // line 88
        echo "
        <title>
        ";
        // line 90
        $this->displayBlock('sonataheadtitle', $context, $blocks);
        // line 116
        echo "        </title>
    </head>
    <body ";
        // line 118
        $this->displayBlock('bodyattributes', $context, $blocks);
        echo ">

    <div class=\"wrapper\">

        ";
        // line 122
        $this->displayBlock('sonataheader', $context, $blocks);
        // line 221
        echo "
        ";
        // line 222
        $this->displayBlock('sonatawrapper', $context, $blocks);
        // line 355
        echo "    </div>

    ";
        // line 357
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 357, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "usebootlint"), "method")) {
            // line 358
            echo "        ";
            $this->displayBlock('bootlint', $context, $blocks);
            // line 364
            echo "    ";
        }
        // line 365
        echo "
    </body>
</html>
";
        
        $internal700495c7e4ab7d505b51d3438284d2e3a79e1d391afc43bd99f5ee388a710a4c->leave($internal700495c7e4ab7d505b51d3438284d2e3a79e1d391afc43bd99f5ee388a710a4cprof);

        
        $internald90762735adbf761dc4d63a931b143ee082ba76120219575f72caa368893854c->leave($internald90762735adbf761dc4d63a931b143ee082ba76120219575f72caa368893854cprof);

    }

    // line 26
    public function blockhtmlattributes($context, array $blocks = array())
    {
        $internal85dfcb1ae756a1ca9e1238467074b14140bc7f686a28ab823fad302f18d6be2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal85dfcb1ae756a1ca9e1238467074b14140bc7f686a28ab823fad302f18d6be2e->enter($internal85dfcb1ae756a1ca9e1238467074b14140bc7f686a28ab823fad302f18d6be2eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "htmlattributes"));

        $internal2cf6b4497c24197fd8300a58dcc1e8b89ff818b0194f8c7b89486dcbafb9ba74 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2cf6b4497c24197fd8300a58dcc1e8b89ff818b0194f8c7b89486dcbafb9ba74->enter($internal2cf6b4497c24197fd8300a58dcc1e8b89ff818b0194f8c7b89486dcbafb9ba74prof = new TwigProfilerProfile($this->getTemplateName(), "block", "htmlattributes"));

        echo "class=\"no-js\"";
        
        $internal2cf6b4497c24197fd8300a58dcc1e8b89ff818b0194f8c7b89486dcbafb9ba74->leave($internal2cf6b4497c24197fd8300a58dcc1e8b89ff818b0194f8c7b89486dcbafb9ba74prof);

        
        $internal85dfcb1ae756a1ca9e1238467074b14140bc7f686a28ab823fad302f18d6be2e->leave($internal85dfcb1ae756a1ca9e1238467074b14140bc7f686a28ab823fad302f18d6be2eprof);

    }

    // line 28
    public function blockmetatags($context, array $blocks = array())
    {
        $internalf758ee16e25e1ac7c80c585e68d1d59cc6047b51b41df6188fa8ca362562c57e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf758ee16e25e1ac7c80c585e68d1d59cc6047b51b41df6188fa8ca362562c57e->enter($internalf758ee16e25e1ac7c80c585e68d1d59cc6047b51b41df6188fa8ca362562c57eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "metatags"));

        $internalbbf89e7e55c074b2ebbb52cde9c91dcd8ef8022d5d449b75b8a847a66ed28fa5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbbf89e7e55c074b2ebbb52cde9c91dcd8ef8022d5d449b75b8a847a66ed28fa5->enter($internalbbf89e7e55c074b2ebbb52cde9c91dcd8ef8022d5d449b75b8a847a66ed28fa5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "metatags"));

        // line 29
        echo "            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        ";
        
        $internalbbf89e7e55c074b2ebbb52cde9c91dcd8ef8022d5d449b75b8a847a66ed28fa5->leave($internalbbf89e7e55c074b2ebbb52cde9c91dcd8ef8022d5d449b75b8a847a66ed28fa5prof);

        
        $internalf758ee16e25e1ac7c80c585e68d1d59cc6047b51b41df6188fa8ca362562c57e->leave($internalf758ee16e25e1ac7c80c585e68d1d59cc6047b51b41df6188fa8ca362562c57eprof);

    }

    // line 34
    public function blockstylesheets($context, array $blocks = array())
    {
        $internal84f50deb44372f03ab3a0cfd3ede72990821f31c78e0e3888eccd53de40acc5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal84f50deb44372f03ab3a0cfd3ede72990821f31c78e0e3888eccd53de40acc5e->enter($internal84f50deb44372f03ab3a0cfd3ede72990821f31c78e0e3888eccd53de40acc5eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        $internal2b399d888ce24eec8f1c1743f9be5c5de85099c3210f04989df799b0e57a8a6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2b399d888ce24eec8f1c1743f9be5c5de85099c3210f04989df799b0e57a8a6b->enter($internal2b399d888ce24eec8f1c1743f9be5c5de85099c3210f04989df799b0e57a8a6bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "stylesheets"));

        // line 35
        echo "            ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 35, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "stylesheets", 1 => array()), "method"));
        foreach ($context['seq'] as $context["key"] => $context["stylesheet"]) {
            // line 36
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["stylesheet"]), "html", null, true);
            echo "\">
            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['stylesheet'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 38
        echo "        ";
        
        $internal2b399d888ce24eec8f1c1743f9be5c5de85099c3210f04989df799b0e57a8a6b->leave($internal2b399d888ce24eec8f1c1743f9be5c5de85099c3210f04989df799b0e57a8a6bprof);

        
        $internal84f50deb44372f03ab3a0cfd3ede72990821f31c78e0e3888eccd53de40acc5e->leave($internal84f50deb44372f03ab3a0cfd3ede72990821f31c78e0e3888eccd53de40acc5eprof);

    }

    // line 40
    public function blockjavascripts($context, array $blocks = array())
    {
        $internalf28f8d95568182a787761c97dae3f2a8a2cfe83bb622726e62d0fe9ece7d4252 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf28f8d95568182a787761c97dae3f2a8a2cfe83bb622726e62d0fe9ece7d4252->enter($internalf28f8d95568182a787761c97dae3f2a8a2cfe83bb622726e62d0fe9ece7d4252prof = new TwigProfilerProfile($this->getTemplateName(), "block", "javascripts"));

        $internald8c9f45dd975f8bebf50b52d4befe2ebe2c84d337b1f55f042473a6f982b59d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald8c9f45dd975f8bebf50b52d4befe2ebe2c84d337b1f55f042473a6f982b59d5->enter($internald8c9f45dd975f8bebf50b52d4befe2ebe2c84d337b1f55f042473a6f982b59d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "javascripts"));

        // line 41
        echo "            ";
        $this->displayBlock('sonatajavascriptconfig', $context, $blocks);
        // line 61
        echo "
            ";
        // line 62
        $this->displayBlock('sonatajavascriptpool', $context, $blocks);
        // line 67
        echo "
            ";
        // line 68
        $context["locale"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 68, $this->getSourceContext()); })()), "request", array()), "locale", array());
        // line 69
        echo "            ";
        // line 70
        echo "            ";
        if ((twigslice($this->env, (isset($context["locale"]) || arraykeyexists("locale", $context) ? $context["locale"] : (function () { throw new TwigErrorRuntime('Variable "locale" does not exist.', 70, $this->getSourceContext()); })()), 0, 2) != "en")) {
            // line 71
            echo "                <script src=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("bundles/sonatacore/vendor/moment/locale/" . twigreplacefilter(twiglowerfilter($this->env,             // line 73
(isset($context["locale"]) || arraykeyexists("locale", $context) ? $context["locale"] : (function () { throw new TwigErrorRuntime('Variable "locale" does not exist.', 73, $this->getSourceContext()); })())), array("" => "-"))) . ".js")), "html", null, true);
            // line 75
            echo "\"></script>
            ";
        }
        // line 77
        echo "
            ";
        // line 79
        echo "            ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 79, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "useselect2"), "method")) {
            // line 80
            echo "                ";
            if (((isset($context["locale"]) || arraykeyexists("locale", $context) ? $context["locale"] : (function () { throw new TwigErrorRuntime('Variable "locale" does not exist.', 80, $this->getSourceContext()); })()) == "pt")) {
                $context["locale"] = "ptPT";
            }
            // line 81
            echo "
                ";
            // line 83
            echo "                ";
            if ((twigslice($this->env, (isset($context["locale"]) || arraykeyexists("locale", $context) ? $context["locale"] : (function () { throw new TwigErrorRuntime('Variable "locale" does not exist.', 83, $this->getSourceContext()); })()), 0, 2) != "en")) {
                // line 84
                echo "                    <script src=\"";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("bundles/sonatacore/vendor/select2/select2locale" . twigreplacefilter((isset($context["locale"]) || arraykeyexists("locale", $context) ? $context["locale"] : (function () { throw new TwigErrorRuntime('Variable "locale" does not exist.', 84, $this->getSourceContext()); })()), array("" => "-"))) . ".js")), "html", null, true);
                echo "\"></script>
                ";
            }
            // line 86
            echo "            ";
        }
        // line 87
        echo "        ";
        
        $internald8c9f45dd975f8bebf50b52d4befe2ebe2c84d337b1f55f042473a6f982b59d5->leave($internald8c9f45dd975f8bebf50b52d4befe2ebe2c84d337b1f55f042473a6f982b59d5prof);

        
        $internalf28f8d95568182a787761c97dae3f2a8a2cfe83bb622726e62d0fe9ece7d4252->leave($internalf28f8d95568182a787761c97dae3f2a8a2cfe83bb622726e62d0fe9ece7d4252prof);

    }

    // line 41
    public function blocksonatajavascriptconfig($context, array $blocks = array())
    {
        $internal3e33efef1530557a6e336b43397b87e461d09ee20d452718424fc91e16072b07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3e33efef1530557a6e336b43397b87e461d09ee20d452718424fc91e16072b07->enter($internal3e33efef1530557a6e336b43397b87e461d09ee20d452718424fc91e16072b07prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatajavascriptconfig"));

        $internal0cd08b1d73bd4766718cbc2f2c636598999be1e5863a94e88c9d80c9b0d496a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0cd08b1d73bd4766718cbc2f2c636598999be1e5863a94e88c9d80c9b0d496a8->enter($internal0cd08b1d73bd4766718cbc2f2c636598999be1e5863a94e88c9d80c9b0d496a8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatajavascriptconfig"));

        // line 42
        echo "                <script>
                    window.SONATACONFIG = {
                        CONFIRMEXIT: ";
        // line 44
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 44, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "confirmexit"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USESELECT2: ";
        // line 45
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 45, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "useselect2"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USEICHECK: ";
        // line 46
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 46, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "useicheck"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USESTICKYFORMS: ";
        // line 47
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 47, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "usestickyforms"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        // line 48
        echo "                    };
                    window.SONATATRANSLATIONS = {
                        CONFIRMEXIT: '";
        // line 50
        echo twigescapefilter($this->env, twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("confirmexit", array(), "SonataAdminBundle"), "js"), "html", null, true);
        echo "'
                    };

                    // http://getbootstrap.com/getting-started/#support-ie10-width
                    if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                        var msViewportStyle = document.createElement('style');
                        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                        document.querySelector('head').appendChild(msViewportStyle);
                    }
                </script>
            ";
        
        $internal0cd08b1d73bd4766718cbc2f2c636598999be1e5863a94e88c9d80c9b0d496a8->leave($internal0cd08b1d73bd4766718cbc2f2c636598999be1e5863a94e88c9d80c9b0d496a8prof);

        
        $internal3e33efef1530557a6e336b43397b87e461d09ee20d452718424fc91e16072b07->leave($internal3e33efef1530557a6e336b43397b87e461d09ee20d452718424fc91e16072b07prof);

    }

    // line 62
    public function blocksonatajavascriptpool($context, array $blocks = array())
    {
        $internal00dbf386252e47db4437c4acd2471eb0ab0de89f3e0310a60ebd5e30ce0da6f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal00dbf386252e47db4437c4acd2471eb0ab0de89f3e0310a60ebd5e30ce0da6f1->enter($internal00dbf386252e47db4437c4acd2471eb0ab0de89f3e0310a60ebd5e30ce0da6f1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatajavascriptpool"));

        $internalf490f7a317da10b95a5a4dbf876ac78a53205bbec5b47cf6839ce0998a17164c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf490f7a317da10b95a5a4dbf876ac78a53205bbec5b47cf6839ce0998a17164c->enter($internalf490f7a317da10b95a5a4dbf876ac78a53205bbec5b47cf6839ce0998a17164cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatajavascriptpool"));

        // line 63
        echo "                ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 63, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "javascripts", 1 => array()), "method"));
        foreach ($context['seq'] as $context["key"] => $context["javascript"]) {
            // line 64
            echo "                    <script src=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["javascript"]), "html", null, true);
            echo "\"></script>
                ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['javascript'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 66
        echo "            ";
        
        $internalf490f7a317da10b95a5a4dbf876ac78a53205bbec5b47cf6839ce0998a17164c->leave($internalf490f7a317da10b95a5a4dbf876ac78a53205bbec5b47cf6839ce0998a17164cprof);

        
        $internal00dbf386252e47db4437c4acd2471eb0ab0de89f3e0310a60ebd5e30ce0da6f1->leave($internal00dbf386252e47db4437c4acd2471eb0ab0de89f3e0310a60ebd5e30ce0da6f1prof);

    }

    // line 90
    public function blocksonataheadtitle($context, array $blocks = array())
    {
        $internal1f43bc3d7cf8ee32d197a120261ea5418c52c5c74f55cb255bf9ee869dadd69d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1f43bc3d7cf8ee32d197a120261ea5418c52c5c74f55cb255bf9ee869dadd69d->enter($internal1f43bc3d7cf8ee32d197a120261ea5418c52c5c74f55cb255bf9ee869dadd69dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheadtitle"));

        $internal2f980cd6ac26980267c13f4f29fce97a3583876d0888717153db72a9204bcea4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2f980cd6ac26980267c13f4f29fce97a3583876d0888717153db72a9204bcea4->enter($internal2f980cd6ac26980267c13f4f29fce97a3583876d0888717153db72a9204bcea4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheadtitle"));

        // line 91
        echo "            ";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Admin", array(), "SonataAdminBundle"), "html", null, true);
        echo "

            ";
        // line 93
        if ( !twigtestempty((isset($context["title"]) || arraykeyexists("title", $context) ? $context["title"] : (function () { throw new TwigErrorRuntime('Variable "title" does not exist.', 93, $this->getSourceContext()); })()))) {
            // line 94
            echo "                ";
            echo striptags((isset($context["title"]) || arraykeyexists("title", $context) ? $context["title"] : (function () { throw new TwigErrorRuntime('Variable "title" does not exist.', 94, $this->getSourceContext()); })()));
            echo "
            ";
        } else {
            // line 96
            echo "                ";
            if (arraykeyexists("action", $context)) {
                // line 97
                echo "                    -
                    ";
                // line 98
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["breadcrumbsbuilder"]) || arraykeyexists("breadcrumbsbuilder", $context) ? $context["breadcrumbsbuilder"] : (function () { throw new TwigErrorRuntime('Variable "breadcrumbsbuilder" does not exist.', 98, $this->getSourceContext()); })()), "breadcrumbs", array(0 => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 98, $this->getSourceContext()); })()), 1 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 98, $this->getSourceContext()); })())), "method"));
                $context['loop'] = array(
                  'parent' => $context['parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                    $length = count($context['seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['seq'] as $context["key"] => $context["menu"]) {
                    // line 99
                    echo "                        ";
                    if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                        // line 100
                        echo "                            ";
                        if ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()) != 2)) {
                            // line 101
                            echo "                                &gt;
                            ";
                        }
                        // line 104
                        $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "extra", array(0 => "translationdomain", 1 => "messages"), "method");
                        // line 105
                        $context["label"] = twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "label", array());
                        // line 106
                        if ( !((isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 106, $this->getSourceContext()); })()) === false)) {
                            // line 107
                            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 107, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "extra", array(0 => "translationparams", 1 => array()), "method"), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 107, $this->getSourceContext()); })()));
                        }
                        // line 110
                        echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 110, $this->getSourceContext()); })()), "html", null, true);
                        echo "
                        ";
                    }
                    // line 112
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['menu'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 113
                echo "                ";
            }
            // line 114
            echo "            ";
        }
        // line 115
        echo "        ";
        
        $internal2f980cd6ac26980267c13f4f29fce97a3583876d0888717153db72a9204bcea4->leave($internal2f980cd6ac26980267c13f4f29fce97a3583876d0888717153db72a9204bcea4prof);

        
        $internal1f43bc3d7cf8ee32d197a120261ea5418c52c5c74f55cb255bf9ee869dadd69d->leave($internal1f43bc3d7cf8ee32d197a120261ea5418c52c5c74f55cb255bf9ee869dadd69dprof);

    }

    // line 118
    public function blockbodyattributes($context, array $blocks = array())
    {
        $internal5e2a8522ffb1c41c30727377099b7c8b8bfc1b541bde4f61a8e035842efdd86b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5e2a8522ffb1c41c30727377099b7c8b8bfc1b541bde4f61a8e035842efdd86b->enter($internal5e2a8522ffb1c41c30727377099b7c8b8bfc1b541bde4f61a8e035842efdd86bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyattributes"));

        $internalbca7a97f17b5bdde2b67808d748cbd48b992b8f794d3ad064522d8be3fbbf2ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbca7a97f17b5bdde2b67808d748cbd48b992b8f794d3ad064522d8be3fbbf2ca->enter($internalbca7a97f17b5bdde2b67808d748cbd48b992b8f794d3ad064522d8be3fbbf2caprof = new TwigProfilerProfile($this->getTemplateName(), "block", "bodyattributes"));

        echo "class=\"sonata-bc skin-black fixed\"";
        
        $internalbca7a97f17b5bdde2b67808d748cbd48b992b8f794d3ad064522d8be3fbbf2ca->leave($internalbca7a97f17b5bdde2b67808d748cbd48b992b8f794d3ad064522d8be3fbbf2caprof);

        
        $internal5e2a8522ffb1c41c30727377099b7c8b8bfc1b541bde4f61a8e035842efdd86b->leave($internal5e2a8522ffb1c41c30727377099b7c8b8bfc1b541bde4f61a8e035842efdd86bprof);

    }

    // line 122
    public function blocksonataheader($context, array $blocks = array())
    {
        $internalaa0ad3827def94de1c1c176c152d76c9da51d7655d0f99b4c8f1ea07d8c9b5a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalaa0ad3827def94de1c1c176c152d76c9da51d7655d0f99b4c8f1ea07d8c9b5a6->enter($internalaa0ad3827def94de1c1c176c152d76c9da51d7655d0f99b4c8f1ea07d8c9b5a6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheader"));

        $internal8c426f88e602999460ecfbe55d62932ff114e13ed0dfc68370b711dcb13f5cca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8c426f88e602999460ecfbe55d62932ff114e13ed0dfc68370b711dcb13f5cca->enter($internal8c426f88e602999460ecfbe55d62932ff114e13ed0dfc68370b711dcb13f5ccaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheader"));

        // line 123
        echo "            <header class=\"main-header\">
                ";
        // line 124
        $this->displayBlock('sonataheadernoscriptwarning', $context, $blocks);
        // line 131
        echo "                ";
        $this->displayBlock('logo', $context, $blocks);
        // line 143
        echo "                ";
        $this->displayBlock('sonatanav', $context, $blocks);
        // line 219
        echo "            </header>
        ";
        
        $internal8c426f88e602999460ecfbe55d62932ff114e13ed0dfc68370b711dcb13f5cca->leave($internal8c426f88e602999460ecfbe55d62932ff114e13ed0dfc68370b711dcb13f5ccaprof);

        
        $internalaa0ad3827def94de1c1c176c152d76c9da51d7655d0f99b4c8f1ea07d8c9b5a6->leave($internalaa0ad3827def94de1c1c176c152d76c9da51d7655d0f99b4c8f1ea07d8c9b5a6prof);

    }

    // line 124
    public function blocksonataheadernoscriptwarning($context, array $blocks = array())
    {
        $internal3d144118855d33f704386c025eb69fb5e2a33ab891cdc87b4c8cfacba24d3c39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3d144118855d33f704386c025eb69fb5e2a33ab891cdc87b4c8cfacba24d3c39->enter($internal3d144118855d33f704386c025eb69fb5e2a33ab891cdc87b4c8cfacba24d3c39prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheadernoscriptwarning"));

        $internalb97ca51bc99ef4fa838b9c76c722d7e121405f9b1b070925cdd94a2e9d28ec45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb97ca51bc99ef4fa838b9c76c722d7e121405f9b1b070925cdd94a2e9d28ec45->enter($internalb97ca51bc99ef4fa838b9c76c722d7e121405f9b1b070925cdd94a2e9d28ec45prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataheadernoscriptwarning"));

        // line 125
        echo "                    <noscript>
                        <div class=\"noscript-warning\">
                            ";
        // line 127
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("noscriptwarning", array(), "SonataAdminBundle"), "html", null, true);
        echo "
                        </div>
                    </noscript>
                ";
        
        $internalb97ca51bc99ef4fa838b9c76c722d7e121405f9b1b070925cdd94a2e9d28ec45->leave($internalb97ca51bc99ef4fa838b9c76c722d7e121405f9b1b070925cdd94a2e9d28ec45prof);

        
        $internal3d144118855d33f704386c025eb69fb5e2a33ab891cdc87b4c8cfacba24d3c39->leave($internal3d144118855d33f704386c025eb69fb5e2a33ab891cdc87b4c8cfacba24d3c39prof);

    }

    // line 131
    public function blocklogo($context, array $blocks = array())
    {
        $internald452731cbf8529107389037e82783673cd22b4024ebddda84be062def5f24ef6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald452731cbf8529107389037e82783673cd22b4024ebddda84be062def5f24ef6->enter($internald452731cbf8529107389037e82783673cd22b4024ebddda84be062def5f24ef6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "logo"));

        $internalbe8e2c9f336bb975e8c6c25ab060701fcc2760bc96f535c756eb078ec52a2c87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbe8e2c9f336bb975e8c6c25ab060701fcc2760bc96f535c756eb078ec52a2c87->enter($internalbe8e2c9f336bb975e8c6c25ab060701fcc2760bc96f535c756eb078ec52a2c87prof = new TwigProfilerProfile($this->getTemplateName(), "block", "logo"));

        // line 132
        echo "                    ";
        obstart();
        // line 133
        echo "                        <a class=\"logo\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadmindashboard");
        echo "\">
                            ";
        // line 134
        if ((("singleimage" == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 134, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "titlemode"), "method")) || ("both" == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 134, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "titlemode"), "method")))) {
            // line 135
            echo "                                <img src=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 135, $this->getSourceContext()); })()), "adminPool", array()), "titlelogo", array())), "html", null, true);
            echo "\" alt=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 135, $this->getSourceContext()); })()), "adminPool", array()), "title", array()), "html", null, true);
            echo "\">
                            ";
        }
        // line 137
        echo "                            ";
        if ((("singletext" == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 137, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "titlemode"), "method")) || ("both" == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 137, $this->getSourceContext()); })()), "adminPool", array()), "getOption", array(0 => "titlemode"), "method")))) {
            // line 138
            echo "                                <span>";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 138, $this->getSourceContext()); })()), "adminPool", array()), "title", array()), "html", null, true);
            echo "</span>
                            ";
        }
        // line 140
        echo "                        </a>
                    ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        // line 142
        echo "                ";
        
        $internalbe8e2c9f336bb975e8c6c25ab060701fcc2760bc96f535c756eb078ec52a2c87->leave($internalbe8e2c9f336bb975e8c6c25ab060701fcc2760bc96f535c756eb078ec52a2c87prof);

        
        $internald452731cbf8529107389037e82783673cd22b4024ebddda84be062def5f24ef6->leave($internald452731cbf8529107389037e82783673cd22b4024ebddda84be062def5f24ef6prof);

    }

    // line 143
    public function blocksonatanav($context, array $blocks = array())
    {
        $internal2ad928c8f2185732e202bf98898f9d39d8463415a2b93c49b587272654d80d13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2ad928c8f2185732e202bf98898f9d39d8463415a2b93c49b587272654d80d13->enter($internal2ad928c8f2185732e202bf98898f9d39d8463415a2b93c49b587272654d80d13prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatanav"));

        $internalc2338b9f84edf8f34529bf7298d7147a0fe7fc2a941e477ee28937749ca6e3fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc2338b9f84edf8f34529bf7298d7147a0fe7fc2a941e477ee28937749ca6e3fd->enter($internalc2338b9f84edf8f34529bf7298d7147a0fe7fc2a941e477ee28937749ca6e3fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatanav"));

        // line 144
        echo "                    <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                            <span class=\"sr-only\">Toggle navigation</span>
                        </a>

                        <div class=\"navbar-left\">
                            ";
        // line 150
        $this->displayBlock('sonatabreadcrumb', $context, $blocks);
        // line 189
        echo "                        </div>

                        ";
        // line 191
        $this->displayBlock('sonatatopnavmenu', $context, $blocks);
        // line 217
        echo "                    </nav>
                ";
        
        $internalc2338b9f84edf8f34529bf7298d7147a0fe7fc2a941e477ee28937749ca6e3fd->leave($internalc2338b9f84edf8f34529bf7298d7147a0fe7fc2a941e477ee28937749ca6e3fdprof);

        
        $internal2ad928c8f2185732e202bf98898f9d39d8463415a2b93c49b587272654d80d13->leave($internal2ad928c8f2185732e202bf98898f9d39d8463415a2b93c49b587272654d80d13prof);

    }

    // line 150
    public function blocksonatabreadcrumb($context, array $blocks = array())
    {
        $internal6b54c3646fafa6ed4a4bc6984bde7e02652d5221f7f0eb31464ade2f830bf5d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6b54c3646fafa6ed4a4bc6984bde7e02652d5221f7f0eb31464ade2f830bf5d2->enter($internal6b54c3646fafa6ed4a4bc6984bde7e02652d5221f7f0eb31464ade2f830bf5d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatabreadcrumb"));

        $internal6dcea3553992be780aa4afd811bfa6406c88d162d6fc17722d78bab34d69b7e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6dcea3553992be780aa4afd811bfa6406c88d162d6fc17722d78bab34d69b7e0->enter($internal6dcea3553992be780aa4afd811bfa6406c88d162d6fc17722d78bab34d69b7e0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatabreadcrumb"));

        // line 151
        echo "                                <div class=\"hidden-xs\">
                                    ";
        // line 152
        if (( !twigtestempty((isset($context["breadcrumb"]) || arraykeyexists("breadcrumb", $context) ? $context["breadcrumb"] : (function () { throw new TwigErrorRuntime('Variable "breadcrumb" does not exist.', 152, $this->getSourceContext()); })())) || arraykeyexists("action", $context))) {
            // line 153
            echo "                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            ";
            // line 154
            if (twigtestempty((isset($context["breadcrumb"]) || arraykeyexists("breadcrumb", $context) ? $context["breadcrumb"] : (function () { throw new TwigErrorRuntime('Variable "breadcrumb" does not exist.', 154, $this->getSourceContext()); })()))) {
                // line 155
                echo "                                                ";
                if (arraykeyexists("action", $context)) {
                    // line 156
                    echo "                                                    ";
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["breadcrumbsbuilder"]) || arraykeyexists("breadcrumbsbuilder", $context) ? $context["breadcrumbsbuilder"] : (function () { throw new TwigErrorRuntime('Variable "breadcrumbsbuilder" does not exist.', 156, $this->getSourceContext()); })()), "breadcrumbs", array(0 => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 156, $this->getSourceContext()); })()), 1 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 156, $this->getSourceContext()); })())), "method"));
                    $context['loop'] = array(
                      'parent' => $context['parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                        $length = count($context['seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['seq'] as $context["key"] => $context["menu"]) {
                        // line 157
                        $context["translationdomain"] = twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "extra", array(0 => "translationdomain", 1 => "messages"), "method");
                        // line 158
                        $context["label"] = twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "label", array());
                        // line 159
                        if ( !((isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 159, $this->getSourceContext()); })()) === false)) {
                            // line 160
                            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 160, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "extra", array(0 => "translationparams", 1 => array()), "method"), (isset($context["translationdomain"]) || arraykeyexists("translationdomain", $context) ? $context["translationdomain"] : (function () { throw new TwigErrorRuntime('Variable "translationdomain" does not exist.', 160, $this->getSourceContext()); })()));
                        }
                        // line 163
                        if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                            // line 164
                            echo "                                                            <li>
                                                                ";
                            // line 165
                            if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "uri", array()))) {
                                // line 166
                                echo "                                                                    <a href=\"";
                                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "uri", array()), "html", null, true);
                                echo "\">
                                                                        ";
                                // line 167
                                if (twiggetattribute($this->env, $this->getSourceContext(), $context["menu"], "extra", array(0 => "safelabel", 1 => true), "method")) {
                                    // line 168
                                    echo (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 168, $this->getSourceContext()); })());
                                } else {
                                    // line 170
                                    echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 170, $this->getSourceContext()); })()), "html", null, true);
                                }
                                // line 172
                                echo "                                                                    </a>
                                                                ";
                            } else {
                                // line 174
                                echo "                                                                    <span>";
                                echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 174, $this->getSourceContext()); })()), "html", null, true);
                                echo "</span>
                                                                ";
                            }
                            // line 176
                            echo "                                                            </li>
                                                        ";
                        } else {
                            // line 178
                            echo "                                                            <li class=\"active\"><span>";
                            echo twigescapefilter($this->env, (isset($context["label"]) || arraykeyexists("label", $context) ? $context["label"] : (function () { throw new TwigErrorRuntime('Variable "label" does not exist.', 178, $this->getSourceContext()); })()), "html", null, true);
                            echo "</span></li>
                                                        ";
                        }
                        // line 180
                        echo "                                                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['menu'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 181
                    echo "                                                ";
                }
                // line 182
                echo "                                            ";
            } else {
                // line 183
                echo "                                                ";
                echo (isset($context["breadcrumb"]) || arraykeyexists("breadcrumb", $context) ? $context["breadcrumb"] : (function () { throw new TwigErrorRuntime('Variable "breadcrumb" does not exist.', 183, $this->getSourceContext()); })());
                echo "
                                            ";
            }
            // line 185
            echo "                                        </ol>
                                    ";
        }
        // line 187
        echo "                                </div>
                            ";
        
        $internal6dcea3553992be780aa4afd811bfa6406c88d162d6fc17722d78bab34d69b7e0->leave($internal6dcea3553992be780aa4afd811bfa6406c88d162d6fc17722d78bab34d69b7e0prof);

        
        $internal6b54c3646fafa6ed4a4bc6984bde7e02652d5221f7f0eb31464ade2f830bf5d2->leave($internal6b54c3646fafa6ed4a4bc6984bde7e02652d5221f7f0eb31464ade2f830bf5d2prof);

    }

    // line 191
    public function blocksonatatopnavmenu($context, array $blocks = array())
    {
        $internal314362a5f4afbdbb224766d7b651439c160a111695c3a27b3f64e7ba0cf70c89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal314362a5f4afbdbb224766d7b651439c160a111695c3a27b3f64e7ba0cf70c89->enter($internal314362a5f4afbdbb224766d7b651439c160a111695c3a27b3f64e7ba0cf70c89prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenu"));

        $internal360444726d7abc74fdaaaa67d32785f5f886ad2385f4c4087f7953b965e4ef70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal360444726d7abc74fdaaaa67d32785f5f886ad2385f4c4087f7953b965e4ef70->enter($internal360444726d7abc74fdaaaa67d32785f5f886ad2385f4c4087f7953b965e4ef70prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenu"));

        // line 192
        echo "                            ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 192, $this->getSourceContext()); })()), "user", array()) && $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLESONATAADMIN"))) {
            // line 193
            echo "                                <div class=\"navbar-custom-menu\">
                                    <ul class=\"nav navbar-nav\">
                                        ";
            // line 195
            $this->displayBlock('sonatatopnavmenuaddblock', $context, $blocks);
            // line 203
            echo "                                        ";
            $this->displayBlock('sonatatopnavmenuuserblock', $context, $blocks);
            // line 213
            echo "                                    </ul>
                                </div>
                            ";
        }
        // line 216
        echo "                        ";
        
        $internal360444726d7abc74fdaaaa67d32785f5f886ad2385f4c4087f7953b965e4ef70->leave($internal360444726d7abc74fdaaaa67d32785f5f886ad2385f4c4087f7953b965e4ef70prof);

        
        $internal314362a5f4afbdbb224766d7b651439c160a111695c3a27b3f64e7ba0cf70c89->leave($internal314362a5f4afbdbb224766d7b651439c160a111695c3a27b3f64e7ba0cf70c89prof);

    }

    // line 195
    public function blocksonatatopnavmenuaddblock($context, array $blocks = array())
    {
        $internal9175f9c969a7ceed0429cfab6828f81de11cbfa69d7becc1bbdc0ef1937d0b90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9175f9c969a7ceed0429cfab6828f81de11cbfa69d7becc1bbdc0ef1937d0b90->enter($internal9175f9c969a7ceed0429cfab6828f81de11cbfa69d7becc1bbdc0ef1937d0b90prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenuaddblock"));

        $internal868a4fc88ff78c291835bba425dd703374db2a1798c2fcf93c57b69381b4a93c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal868a4fc88ff78c291835bba425dd703374db2a1798c2fcf93c57b69381b4a93c->enter($internal868a4fc88ff78c291835bba425dd703374db2a1798c2fcf93c57b69381b4a93cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenuaddblock"));

        // line 196
        echo "                                            <li class=\"dropdown\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-plus-square fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                ";
        // line 200
        $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 200, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "addblock"), "method"), "SonataAdminBundle::standardlayout.html.twig", 200)->display($context);
        // line 201
        echo "                                            </li>
                                        ";
        
        $internal868a4fc88ff78c291835bba425dd703374db2a1798c2fcf93c57b69381b4a93c->leave($internal868a4fc88ff78c291835bba425dd703374db2a1798c2fcf93c57b69381b4a93cprof);

        
        $internal9175f9c969a7ceed0429cfab6828f81de11cbfa69d7becc1bbdc0ef1937d0b90->leave($internal9175f9c969a7ceed0429cfab6828f81de11cbfa69d7becc1bbdc0ef1937d0b90prof);

    }

    // line 203
    public function blocksonatatopnavmenuuserblock($context, array $blocks = array())
    {
        $internal3ad18c47e599208ff143018cfa40322a1d6d20313553eeac908b66ce7eeb7643 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3ad18c47e599208ff143018cfa40322a1d6d20313553eeac908b66ce7eeb7643->enter($internal3ad18c47e599208ff143018cfa40322a1d6d20313553eeac908b66ce7eeb7643prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenuuserblock"));

        $internal3eab923f7816331adc3af687d7f089afea49632aad0bbb0444e877d25c54807d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3eab923f7816331adc3af687d7f089afea49632aad0bbb0444e877d25c54807d->enter($internal3eab923f7816331adc3af687d7f089afea49632aad0bbb0444e877d25c54807dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatopnavmenuuserblock"));

        // line 204
        echo "                                            <li class=\"dropdown user-menu\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-user fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                <ul class=\"dropdown-menu dropdown-user\">
                                                    ";
        // line 209
        $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 209, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "userblock"), "method"), "SonataAdminBundle::standardlayout.html.twig", 209)->display($context);
        // line 210
        echo "                                                </ul>
                                            </li>
                                        ";
        
        $internal3eab923f7816331adc3af687d7f089afea49632aad0bbb0444e877d25c54807d->leave($internal3eab923f7816331adc3af687d7f089afea49632aad0bbb0444e877d25c54807dprof);

        
        $internal3ad18c47e599208ff143018cfa40322a1d6d20313553eeac908b66ce7eeb7643->leave($internal3ad18c47e599208ff143018cfa40322a1d6d20313553eeac908b66ce7eeb7643prof);

    }

    // line 222
    public function blocksonatawrapper($context, array $blocks = array())
    {
        $internal52ce2de60af6b06a7d68b397342790a3df2847cdad595219241a93688a4bee0d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal52ce2de60af6b06a7d68b397342790a3df2847cdad595219241a93688a4bee0d->enter($internal52ce2de60af6b06a7d68b397342790a3df2847cdad595219241a93688a4bee0dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatawrapper"));

        $internal98bd2cabfa14be59bc16a6cf3d5cb6534f53cf80fa53cf0179612f430a209ae8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal98bd2cabfa14be59bc16a6cf3d5cb6534f53cf80fa53cf0179612f430a209ae8->enter($internal98bd2cabfa14be59bc16a6cf3d5cb6534f53cf80fa53cf0179612f430a209ae8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatawrapper"));

        // line 223
        echo "            ";
        $this->displayBlock('sonataleftside', $context, $blocks);
        // line 255
        echo "
            <div class=\"content-wrapper\">
                ";
        // line 257
        $this->displayBlock('sonatapagecontent', $context, $blocks);
        // line 353
        echo "            </div>
        ";
        
        $internal98bd2cabfa14be59bc16a6cf3d5cb6534f53cf80fa53cf0179612f430a209ae8->leave($internal98bd2cabfa14be59bc16a6cf3d5cb6534f53cf80fa53cf0179612f430a209ae8prof);

        
        $internal52ce2de60af6b06a7d68b397342790a3df2847cdad595219241a93688a4bee0d->leave($internal52ce2de60af6b06a7d68b397342790a3df2847cdad595219241a93688a4bee0dprof);

    }

    // line 223
    public function blocksonataleftside($context, array $blocks = array())
    {
        $internal67f8777e2028c5cae227b05f168cdfe945bfd137600f5925dae32a510b331066 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal67f8777e2028c5cae227b05f168cdfe945bfd137600f5925dae32a510b331066->enter($internal67f8777e2028c5cae227b05f168cdfe945bfd137600f5925dae32a510b331066prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataleftside"));

        $internalb6a51718817c4470118ff7c1b260809d4b31b93e30383c6199136b62d227c5dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb6a51718817c4470118ff7c1b260809d4b31b93e30383c6199136b62d227c5dc->enter($internalb6a51718817c4470118ff7c1b260809d4b31b93e30383c6199136b62d227c5dcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataleftside"));

        // line 224
        echo "                <aside class=\"main-sidebar\">
                    <section class=\"sidebar\">
                        ";
        // line 226
        $this->displayBlock('sonatasidenav', $context, $blocks);
        // line 252
        echo "                    </section>
                </aside>
            ";
        
        $internalb6a51718817c4470118ff7c1b260809d4b31b93e30383c6199136b62d227c5dc->leave($internalb6a51718817c4470118ff7c1b260809d4b31b93e30383c6199136b62d227c5dcprof);

        
        $internal67f8777e2028c5cae227b05f168cdfe945bfd137600f5925dae32a510b331066->leave($internal67f8777e2028c5cae227b05f168cdfe945bfd137600f5925dae32a510b331066prof);

    }

    // line 226
    public function blocksonatasidenav($context, array $blocks = array())
    {
        $internale13b8e03ac5eb80146b734b0604180134b65711949ea61ede35d44ec920a68e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale13b8e03ac5eb80146b734b0604180134b65711949ea61ede35d44ec920a68e9->enter($internale13b8e03ac5eb80146b734b0604180134b65711949ea61ede35d44ec920a68e9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatasidenav"));

        $internal704c7f96345d3baa80216d46fcda5fcfd19b00b5edd738b70b2279b5a21d766d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal704c7f96345d3baa80216d46fcda5fcfd19b00b5edd738b70b2279b5a21d766d->enter($internal704c7f96345d3baa80216d46fcda5fcfd19b00b5edd738b70b2279b5a21d766dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatasidenav"));

        // line 227
        echo "                            ";
        $this->displayBlock('sonatasidebarsearch', $context, $blocks);
        // line 239
        echo "
                            ";
        // line 240
        $this->displayBlock('sidebarbeforenav', $context, $blocks);
        // line 241
        echo "                            ";
        $this->displayBlock('sidebarnav', $context, $blocks);
        // line 244
        echo "                            ";
        $this->displayBlock('sidebarafternav', $context, $blocks);
        // line 251
        echo "                        ";
        
        $internal704c7f96345d3baa80216d46fcda5fcfd19b00b5edd738b70b2279b5a21d766d->leave($internal704c7f96345d3baa80216d46fcda5fcfd19b00b5edd738b70b2279b5a21d766dprof);

        
        $internale13b8e03ac5eb80146b734b0604180134b65711949ea61ede35d44ec920a68e9->leave($internale13b8e03ac5eb80146b734b0604180134b65711949ea61ede35d44ec920a68e9prof);

    }

    // line 227
    public function blocksonatasidebarsearch($context, array $blocks = array())
    {
        $internal679c968d388a06b20c12e5d8d32e8c00f980a65b1fbfb77e499ced39e9fea37d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal679c968d388a06b20c12e5d8d32e8c00f980a65b1fbfb77e499ced39e9fea37d->enter($internal679c968d388a06b20c12e5d8d32e8c00f980a65b1fbfb77e499ced39e9fea37dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatasidebarsearch"));

        $internald4e3de2c1139f1a1a51cdcadab4bb516b7e04b775e824c805ce4799fb98655bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald4e3de2c1139f1a1a51cdcadab4bb516b7e04b775e824c805ce4799fb98655bd->enter($internald4e3de2c1139f1a1a51cdcadab4bb516b7e04b775e824c805ce4799fb98655bdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatasidebarsearch"));

        // line 228
        echo "                                <form action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminsearch");
        echo "\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                    <div class=\"input-group custom-search-form\">
                                        <input type=\"text\" name=\"q\" value=\"";
        // line 230
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 230, $this->getSourceContext()); })()), "request", array()), "get", array(0 => "q"), "method"), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("searchplaceholder", array(), "SonataAdminBundle"), "html", null, true);
        echo "\">
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-flat\" type=\"submit\">
                                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            ";
        
        $internald4e3de2c1139f1a1a51cdcadab4bb516b7e04b775e824c805ce4799fb98655bd->leave($internald4e3de2c1139f1a1a51cdcadab4bb516b7e04b775e824c805ce4799fb98655bdprof);

        
        $internal679c968d388a06b20c12e5d8d32e8c00f980a65b1fbfb77e499ced39e9fea37d->leave($internal679c968d388a06b20c12e5d8d32e8c00f980a65b1fbfb77e499ced39e9fea37dprof);

    }

    // line 240
    public function blocksidebarbeforenav($context, array $blocks = array())
    {
        $internal600ad51531eb6b1ef608d161dc205c3e178eaf3a4e807c98a49432f112715d6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal600ad51531eb6b1ef608d161dc205c3e178eaf3a4e807c98a49432f112715d6a->enter($internal600ad51531eb6b1ef608d161dc205c3e178eaf3a4e807c98a49432f112715d6aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarbeforenav"));

        $internal7a4318658466254bf514d1738db04e2e5f60c8a15038f1759835c9548ebebec0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7a4318658466254bf514d1738db04e2e5f60c8a15038f1759835c9548ebebec0->enter($internal7a4318658466254bf514d1738db04e2e5f60c8a15038f1759835c9548ebebec0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarbeforenav"));

        echo " ";
        
        $internal7a4318658466254bf514d1738db04e2e5f60c8a15038f1759835c9548ebebec0->leave($internal7a4318658466254bf514d1738db04e2e5f60c8a15038f1759835c9548ebebec0prof);

        
        $internal600ad51531eb6b1ef608d161dc205c3e178eaf3a4e807c98a49432f112715d6a->leave($internal600ad51531eb6b1ef608d161dc205c3e178eaf3a4e807c98a49432f112715d6aprof);

    }

    // line 241
    public function blocksidebarnav($context, array $blocks = array())
    {
        $internal1d355271d73eab36e016c8ca76920eeacf5e74006218544331e244aa1cdb05c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1d355271d73eab36e016c8ca76920eeacf5e74006218544331e244aa1cdb05c2->enter($internal1d355271d73eab36e016c8ca76920eeacf5e74006218544331e244aa1cdb05c2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarnav"));

        $internalb7c4b71016764a91ba5718825e58b12cff9604aef542f7ba6955343d6b0b86b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb7c4b71016764a91ba5718825e58b12cff9604aef542f7ba6955343d6b0b86b1->enter($internalb7c4b71016764a91ba5718825e58b12cff9604aef542f7ba6955343d6b0b86b1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarnav"));

        // line 242
        echo "                                ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render("sonataadminsidebar", array("template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 242, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "knpmenutemplate"), "method")));
        echo "
                            ";
        
        $internalb7c4b71016764a91ba5718825e58b12cff9604aef542f7ba6955343d6b0b86b1->leave($internalb7c4b71016764a91ba5718825e58b12cff9604aef542f7ba6955343d6b0b86b1prof);

        
        $internal1d355271d73eab36e016c8ca76920eeacf5e74006218544331e244aa1cdb05c2->leave($internal1d355271d73eab36e016c8ca76920eeacf5e74006218544331e244aa1cdb05c2prof);

    }

    // line 244
    public function blocksidebarafternav($context, array $blocks = array())
    {
        $internal558eb7e97a94f61e1450bce7d9be7deb5b463615bedaa2a965f6c58c246231f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal558eb7e97a94f61e1450bce7d9be7deb5b463615bedaa2a965f6c58c246231f7->enter($internal558eb7e97a94f61e1450bce7d9be7deb5b463615bedaa2a965f6c58c246231f7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarafternav"));

        $internal05271927d6aa33a99b02f1cb0614548b6f0c2b594ee82fdbd12d312261566255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal05271927d6aa33a99b02f1cb0614548b6f0c2b594ee82fdbd12d312261566255->enter($internal05271927d6aa33a99b02f1cb0614548b6f0c2b594ee82fdbd12d312261566255prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarafternav"));

        // line 245
        echo "                                <p class=\"text-center small\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                    ";
        // line 246
        $this->displayBlock('sidebarafternavcontent', $context, $blocks);
        // line 249
        echo "                                </p>
                            ";
        
        $internal05271927d6aa33a99b02f1cb0614548b6f0c2b594ee82fdbd12d312261566255->leave($internal05271927d6aa33a99b02f1cb0614548b6f0c2b594ee82fdbd12d312261566255prof);

        
        $internal558eb7e97a94f61e1450bce7d9be7deb5b463615bedaa2a965f6c58c246231f7->leave($internal558eb7e97a94f61e1450bce7d9be7deb5b463615bedaa2a965f6c58c246231f7prof);

    }

    // line 246
    public function blocksidebarafternavcontent($context, array $blocks = array())
    {
        $internalc66138a480ad88bba5fc9a74bfac05a909798edbae6f716bdbd2488244d12f31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc66138a480ad88bba5fc9a74bfac05a909798edbae6f716bdbd2488244d12f31->enter($internalc66138a480ad88bba5fc9a74bfac05a909798edbae6f716bdbd2488244d12f31prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarafternavcontent"));

        $internal8019ba0d40d0f6a3b9a13209af5522cf76a8d53310e4ddc68e2cb45553f758a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8019ba0d40d0f6a3b9a13209af5522cf76a8d53310e4ddc68e2cb45553f758a1->enter($internal8019ba0d40d0f6a3b9a13209af5522cf76a8d53310e4ddc68e2cb45553f758a1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sidebarafternavcontent"));

        // line 247
        echo "                                        <a href=\"https://sonata-project.org\" rel=\"noreferrer\" target=\"blank\">sonata project</a>
                                    ";
        
        $internal8019ba0d40d0f6a3b9a13209af5522cf76a8d53310e4ddc68e2cb45553f758a1->leave($internal8019ba0d40d0f6a3b9a13209af5522cf76a8d53310e4ddc68e2cb45553f758a1prof);

        
        $internalc66138a480ad88bba5fc9a74bfac05a909798edbae6f716bdbd2488244d12f31->leave($internalc66138a480ad88bba5fc9a74bfac05a909798edbae6f716bdbd2488244d12f31prof);

    }

    // line 257
    public function blocksonatapagecontent($context, array $blocks = array())
    {
        $internal291a0b568c54ff8ecad0a7ed1a83049cf8f7fbe7456a4da1c568e0f9b5b16ec1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal291a0b568c54ff8ecad0a7ed1a83049cf8f7fbe7456a4da1c568e0f9b5b16ec1->enter($internal291a0b568c54ff8ecad0a7ed1a83049cf8f7fbe7456a4da1c568e0f9b5b16ec1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontent"));

        $internal54804d21b35c450d535af189b6261fca60b9847b3a8717b3b67bc31b49845d04 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal54804d21b35c450d535af189b6261fca60b9847b3a8717b3b67bc31b49845d04->enter($internal54804d21b35c450d535af189b6261fca60b9847b3a8717b3b67bc31b49845d04prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontent"));

        // line 258
        echo "                    <section class=\"content-header\">

                        ";
        // line 260
        $this->displayBlock('sonatapagecontentheader', $context, $blocks);
        // line 314
        echo "                    </section>

                    <section class=\"content\">
                        ";
        // line 317
        $this->displayBlock('sonataadmincontent', $context, $blocks);
        // line 351
        echo "                    </section>
                ";
        
        $internal54804d21b35c450d535af189b6261fca60b9847b3a8717b3b67bc31b49845d04->leave($internal54804d21b35c450d535af189b6261fca60b9847b3a8717b3b67bc31b49845d04prof);

        
        $internal291a0b568c54ff8ecad0a7ed1a83049cf8f7fbe7456a4da1c568e0f9b5b16ec1->leave($internal291a0b568c54ff8ecad0a7ed1a83049cf8f7fbe7456a4da1c568e0f9b5b16ec1prof);

    }

    // line 260
    public function blocksonatapagecontentheader($context, array $blocks = array())
    {
        $internal685ef0cf4871bbda60fc2ba6f78cfe56eae9ccd30a1f4f90c5c8da4d711933de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal685ef0cf4871bbda60fc2ba6f78cfe56eae9ccd30a1f4f90c5c8da4d711933de->enter($internal685ef0cf4871bbda60fc2ba6f78cfe56eae9ccd30a1f4f90c5c8da4d711933deprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontentheader"));

        $internal1c3d5c464987ff4ba8606c73d51b08c98569eea3db5ef18e7cecd992662a81dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1c3d5c464987ff4ba8606c73d51b08c98569eea3db5ef18e7cecd992662a81dd->enter($internal1c3d5c464987ff4ba8606c73d51b08c98569eea3db5ef18e7cecd992662a81ddprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontentheader"));

        // line 261
        echo "                            ";
        $this->displayBlock('sonatapagecontentnav', $context, $blocks);
        // line 313
        echo "                        ";
        
        $internal1c3d5c464987ff4ba8606c73d51b08c98569eea3db5ef18e7cecd992662a81dd->leave($internal1c3d5c464987ff4ba8606c73d51b08c98569eea3db5ef18e7cecd992662a81ddprof);

        
        $internal685ef0cf4871bbda60fc2ba6f78cfe56eae9ccd30a1f4f90c5c8da4d711933de->leave($internal685ef0cf4871bbda60fc2ba6f78cfe56eae9ccd30a1f4f90c5c8da4d711933deprof);

    }

    // line 261
    public function blocksonatapagecontentnav($context, array $blocks = array())
    {
        $internal61b6e89b07db81225ea3f39eefb5742a708a28d233055dab3f1e6d2cb69f130b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal61b6e89b07db81225ea3f39eefb5742a708a28d233055dab3f1e6d2cb69f130b->enter($internal61b6e89b07db81225ea3f39eefb5742a708a28d233055dab3f1e6d2cb69f130bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontentnav"));

        $internal337773873a0348c0916eca83a6a718c1b244773b1d22d127e238d48b940612a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal337773873a0348c0916eca83a6a718c1b244773b1d22d127e238d48b940612a4->enter($internal337773873a0348c0916eca83a6a718c1b244773b1d22d127e238d48b940612a4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatapagecontentnav"));

        // line 262
        echo "                                ";
        if ((( !twigtestempty((isset($context["tabmenu"]) || arraykeyexists("tabmenu", $context) ? $context["tabmenu"] : (function () { throw new TwigErrorRuntime('Variable "tabmenu" does not exist.', 262, $this->getSourceContext()); })())) ||  !twigtestempty((isset($context["actions"]) || arraykeyexists("actions", $context) ? $context["actions"] : (function () { throw new TwigErrorRuntime('Variable "actions" does not exist.', 262, $this->getSourceContext()); })()))) ||  !twigtestempty((isset($context["listfiltersactions"]) || arraykeyexists("listfiltersactions", $context) ? $context["listfiltersactions"] : (function () { throw new TwigErrorRuntime('Variable "listfiltersactions" does not exist.', 262, $this->getSourceContext()); })())))) {
            // line 263
            echo "                                    <nav class=\"navbar navbar-default\" role=\"navigation\">
                                        <div class=\"container-fluid\">
                                            ";
            // line 265
            $this->displayBlock('tabmenunavbarheader', $context, $blocks);
            // line 272
            echo "
                                            <div class=\"navbar-collapse\">
                                                ";
            // line 274
            if ( !twigtestempty((isset($context["tabmenu"]) || arraykeyexists("tabmenu", $context) ? $context["tabmenu"] : (function () { throw new TwigErrorRuntime('Variable "tabmenu" does not exist.', 274, $this->getSourceContext()); })()))) {
                // line 275
                echo "                                                    <div class=\"navbar-left\">
                                                        ";
                // line 276
                echo (isset($context["tabmenu"]) || arraykeyexists("tabmenu", $context) ? $context["tabmenu"] : (function () { throw new TwigErrorRuntime('Variable "tabmenu" does not exist.', 276, $this->getSourceContext()); })());
                echo "
                                                    </div>
                                                ";
            }
            // line 279
            echo "
                                                ";
            // line 280
            if ((((arraykeyexists("admin", $context) && arraykeyexists("action", $context)) && ((isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 280, $this->getSourceContext()); })()) == "list")) && (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 280, $this->getSourceContext()); })()), "listModes", array())) > 1))) {
                // line 281
                echo "                                                    <div class=\"nav navbar-right btn-group\">
                                                        ";
                // line 282
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 282, $this->getSourceContext()); })()), "listModes", array()));
                foreach ($context['seq'] as $context["mode"] => $context["settings"]) {
                    // line 283
                    echo "                                                            <a href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 283, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 283, $this->getSourceContext()); })()), "request", array()), "query", array()), "all", array()), array("listmode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 283, $this->getSourceContext()); })()), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                                        ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['mode'], $context['settings'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 285
                echo "                                                    </div>
                                                ";
            }
            // line 287
            echo "
                                                ";
            // line 288
            $this->displayBlock('sonataadmincontentactionswrappers', $context, $blocks);
            // line 304
            echo "
                                                ";
            // line 305
            if ( !twigtestempty((isset($context["listfiltersactions"]) || arraykeyexists("listfiltersactions", $context) ? $context["listfiltersactions"] : (function () { throw new TwigErrorRuntime('Variable "listfiltersactions" does not exist.', 305, $this->getSourceContext()); })()))) {
                // line 306
                echo "                                                    ";
                echo (isset($context["listfiltersactions"]) || arraykeyexists("listfiltersactions", $context) ? $context["listfiltersactions"] : (function () { throw new TwigErrorRuntime('Variable "listfiltersactions" does not exist.', 306, $this->getSourceContext()); })());
                echo "
                                                ";
            }
            // line 308
            echo "                                            </div>
                                        </div>
                                    </nav>
                                ";
        }
        // line 312
        echo "                            ";
        
        $internal337773873a0348c0916eca83a6a718c1b244773b1d22d127e238d48b940612a4->leave($internal337773873a0348c0916eca83a6a718c1b244773b1d22d127e238d48b940612a4prof);

        
        $internal61b6e89b07db81225ea3f39eefb5742a708a28d233055dab3f1e6d2cb69f130b->leave($internal61b6e89b07db81225ea3f39eefb5742a708a28d233055dab3f1e6d2cb69f130bprof);

    }

    // line 265
    public function blocktabmenunavbarheader($context, array $blocks = array())
    {
        $internald59b16fa00f0b93de646f85aefac4a78ee86c7bebe7afa26058c90df5d5141b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald59b16fa00f0b93de646f85aefac4a78ee86c7bebe7afa26058c90df5d5141b9->enter($internald59b16fa00f0b93de646f85aefac4a78ee86c7bebe7afa26058c90df5d5141b9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenunavbarheader"));

        $internal8362e27c1ebc214049231ae0669962b16ff67e58a5c09e310110d9360da2f027 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8362e27c1ebc214049231ae0669962b16ff67e58a5c09e310110d9360da2f027->enter($internal8362e27c1ebc214049231ae0669962b16ff67e58a5c09e310110d9360da2f027prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenunavbarheader"));

        // line 266
        echo "                                                ";
        if ( !twigtestempty((isset($context["navbartitle"]) || arraykeyexists("navbartitle", $context) ? $context["navbartitle"] : (function () { throw new TwigErrorRuntime('Variable "navbartitle" does not exist.', 266, $this->getSourceContext()); })()))) {
            // line 267
            echo "                                                    <div class=\"navbar-header\">
                                                        <a class=\"navbar-brand\" href=\"#\">";
            // line 268
            echo (isset($context["navbartitle"]) || arraykeyexists("navbartitle", $context) ? $context["navbartitle"] : (function () { throw new TwigErrorRuntime('Variable "navbartitle" does not exist.', 268, $this->getSourceContext()); })());
            echo "</a>
                                                    </div>
                                                ";
        }
        // line 271
        echo "                                            ";
        
        $internal8362e27c1ebc214049231ae0669962b16ff67e58a5c09e310110d9360da2f027->leave($internal8362e27c1ebc214049231ae0669962b16ff67e58a5c09e310110d9360da2f027prof);

        
        $internald59b16fa00f0b93de646f85aefac4a78ee86c7bebe7afa26058c90df5d5141b9->leave($internald59b16fa00f0b93de646f85aefac4a78ee86c7bebe7afa26058c90df5d5141b9prof);

    }

    // line 288
    public function blocksonataadmincontentactionswrappers($context, array $blocks = array())
    {
        $internal33c64620a1edf3b61d7ec4fff03ab60f0142f94ac9dc34b5e6d84abc48809dc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal33c64620a1edf3b61d7ec4fff03ab60f0142f94ac9dc34b5e6d84abc48809dc5->enter($internal33c64620a1edf3b61d7ec4fff03ab60f0142f94ac9dc34b5e6d84abc48809dc5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadmincontentactionswrappers"));

        $internal3fb852b4e51cf33365fc246343beabe170d72397d332c989989a07eb86c068ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3fb852b4e51cf33365fc246343beabe170d72397d332c989989a07eb86c068ca->enter($internal3fb852b4e51cf33365fc246343beabe170d72397d332c989989a07eb86c068caprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadmincontentactionswrappers"));

        // line 289
        echo "                                                    ";
        if ( !twigtestempty(twigtrimfilter(twigreplacefilter((isset($context["actions"]) || arraykeyexists("actions", $context) ? $context["actions"] : (function () { throw new TwigErrorRuntime('Variable "actions" does not exist.', 289, $this->getSourceContext()); })()), array("<li>" => "", "</li>" => ""))))) {
            // line 290
            echo "                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        ";
            // line 291
            if ((twiglengthfilter($this->env, twigsplitfilter($this->env, (isset($context["actions"]) || arraykeyexists("actions", $context) ? $context["actions"] : (function () { throw new TwigErrorRuntime('Variable "actions" does not exist.', 291, $this->getSourceContext()); })()), "</a>")) > 2)) {
                // line 292
                echo "                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 293
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactions", array(), "SonataAdminBundle"), "html", null, true);
                echo " <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    ";
                // line 295
                echo (isset($context["actions"]) || arraykeyexists("actions", $context) ? $context["actions"] : (function () { throw new TwigErrorRuntime('Variable "actions" does not exist.', 295, $this->getSourceContext()); })());
                echo "
                                                                </ul>
                                                            </li>
                                                        ";
            } else {
                // line 299
                echo "                                                            ";
                echo (isset($context["actions"]) || arraykeyexists("actions", $context) ? $context["actions"] : (function () { throw new TwigErrorRuntime('Variable "actions" does not exist.', 299, $this->getSourceContext()); })());
                echo "
                                                        ";
            }
            // line 301
            echo "                                                        </ul>
                                                    ";
        }
        // line 303
        echo "                                                ";
        
        $internal3fb852b4e51cf33365fc246343beabe170d72397d332c989989a07eb86c068ca->leave($internal3fb852b4e51cf33365fc246343beabe170d72397d332c989989a07eb86c068caprof);

        
        $internal33c64620a1edf3b61d7ec4fff03ab60f0142f94ac9dc34b5e6d84abc48809dc5->leave($internal33c64620a1edf3b61d7ec4fff03ab60f0142f94ac9dc34b5e6d84abc48809dc5prof);

    }

    // line 317
    public function blocksonataadmincontent($context, array $blocks = array())
    {
        $internal3a701c34d8f47f9b14218f39e39f90eb4854c777e7ae29ad69f377d2df612d32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a701c34d8f47f9b14218f39e39f90eb4854c777e7ae29ad69f377d2df612d32->enter($internal3a701c34d8f47f9b14218f39e39f90eb4854c777e7ae29ad69f377d2df612d32prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadmincontent"));

        $internal6feacfdec1e9899b466634955c4c4e9fb089e3890f14b9eb82fe9e76e6c46621 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6feacfdec1e9899b466634955c4c4e9fb089e3890f14b9eb82fe9e76e6c46621->enter($internal6feacfdec1e9899b466634955c4c4e9fb089e3890f14b9eb82fe9e76e6c46621prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadmincontent"));

        // line 318
        echo "
                            ";
        // line 319
        $this->displayBlock('notice', $context, $blocks);
        // line 322
        echo "
                            ";
        // line 323
        if ( !twigtestempty((isset($context["preview"]) || arraykeyexists("preview", $context) ? $context["preview"] : (function () { throw new TwigErrorRuntime('Variable "preview" does not exist.', 323, $this->getSourceContext()); })()))) {
            // line 324
            echo "                                <div class=\"sonata-ba-preview\">";
            echo (isset($context["preview"]) || arraykeyexists("preview", $context) ? $context["preview"] : (function () { throw new TwigErrorRuntime('Variable "preview" does not exist.', 324, $this->getSourceContext()); })());
            echo "</div>
                            ";
        }
        // line 326
        echo "
                            ";
        // line 327
        if ( !twigtestempty((isset($context["content"]) || arraykeyexists("content", $context) ? $context["content"] : (function () { throw new TwigErrorRuntime('Variable "content" does not exist.', 327, $this->getSourceContext()); })()))) {
            // line 328
            echo "                                <div class=\"sonata-ba-content\">";
            echo (isset($context["content"]) || arraykeyexists("content", $context) ? $context["content"] : (function () { throw new TwigErrorRuntime('Variable "content" does not exist.', 328, $this->getSourceContext()); })());
            echo "</div>
                            ";
        }
        // line 330
        echo "
                            ";
        // line 331
        if ( !twigtestempty((isset($context["show"]) || arraykeyexists("show", $context) ? $context["show"] : (function () { throw new TwigErrorRuntime('Variable "show" does not exist.', 331, $this->getSourceContext()); })()))) {
            // line 332
            echo "                                <div class=\"sonata-ba-show\">";
            echo (isset($context["show"]) || arraykeyexists("show", $context) ? $context["show"] : (function () { throw new TwigErrorRuntime('Variable "show" does not exist.', 332, $this->getSourceContext()); })());
            echo "</div>
                            ";
        }
        // line 334
        echo "
                            ";
        // line 335
        if ( !twigtestempty((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 335, $this->getSourceContext()); })()))) {
            // line 336
            echo "                                <div class=\"sonata-ba-form\">";
            echo (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 336, $this->getSourceContext()); })());
            echo "</div>
                            ";
        }
        // line 338
        echo "
                            ";
        // line 339
        if ( !twigtestempty((isset($context["listfilters"]) || arraykeyexists("listfilters", $context) ? $context["listfilters"] : (function () { throw new TwigErrorRuntime('Variable "listfilters" does not exist.', 339, $this->getSourceContext()); })()))) {
            // line 340
            echo "                                <div class=\"row\">
                                    ";
            // line 341
            echo (isset($context["listfilters"]) || arraykeyexists("listfilters", $context) ? $context["listfilters"] : (function () { throw new TwigErrorRuntime('Variable "listfilters" does not exist.', 341, $this->getSourceContext()); })());
            echo "
                                </div>
                            ";
        }
        // line 344
        echo "
                            ";
        // line 345
        if ( !twigtestempty((isset($context["listtable"]) || arraykeyexists("listtable", $context) ? $context["listtable"] : (function () { throw new TwigErrorRuntime('Variable "listtable" does not exist.', 345, $this->getSourceContext()); })()))) {
            // line 346
            echo "                                <div class=\"row\">
                                    ";
            // line 347
            echo (isset($context["listtable"]) || arraykeyexists("listtable", $context) ? $context["listtable"] : (function () { throw new TwigErrorRuntime('Variable "listtable" does not exist.', 347, $this->getSourceContext()); })());
            echo "
                                </div>
                            ";
        }
        // line 350
        echo "                        ";
        
        $internal6feacfdec1e9899b466634955c4c4e9fb089e3890f14b9eb82fe9e76e6c46621->leave($internal6feacfdec1e9899b466634955c4c4e9fb089e3890f14b9eb82fe9e76e6c46621prof);

        
        $internal3a701c34d8f47f9b14218f39e39f90eb4854c777e7ae29ad69f377d2df612d32->leave($internal3a701c34d8f47f9b14218f39e39f90eb4854c777e7ae29ad69f377d2df612d32prof);

    }

    // line 319
    public function blocknotice($context, array $blocks = array())
    {
        $internal707b6900e221de275c62988181f1395ed358acc4fb53429af67b9b44f093e72d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal707b6900e221de275c62988181f1395ed358acc4fb53429af67b9b44f093e72d->enter($internal707b6900e221de275c62988181f1395ed358acc4fb53429af67b9b44f093e72dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "notice"));

        $internal81204fb39b96c95ce66cf44bc8a91899a4dea4f88861d9e791145be23b7d137d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81204fb39b96c95ce66cf44bc8a91899a4dea4f88861d9e791145be23b7d137d->enter($internal81204fb39b96c95ce66cf44bc8a91899a4dea4f88861d9e791145be23b7d137dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "notice"));

        // line 320
        echo "                                ";
        $this->loadTemplate("SonataCoreBundle:FlashMessage:render.html.twig", "SonataAdminBundle::standardlayout.html.twig", 320)->display($context);
        // line 321
        echo "                            ";
        
        $internal81204fb39b96c95ce66cf44bc8a91899a4dea4f88861d9e791145be23b7d137d->leave($internal81204fb39b96c95ce66cf44bc8a91899a4dea4f88861d9e791145be23b7d137dprof);

        
        $internal707b6900e221de275c62988181f1395ed358acc4fb53429af67b9b44f093e72d->leave($internal707b6900e221de275c62988181f1395ed358acc4fb53429af67b9b44f093e72dprof);

    }

    // line 358
    public function blockbootlint($context, array $blocks = array())
    {
        $internala8bdcbc9008743c44031376b04abe30c167775a64d0b8f605860c777d43a3770 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala8bdcbc9008743c44031376b04abe30c167775a64d0b8f605860c777d43a3770->enter($internala8bdcbc9008743c44031376b04abe30c167775a64d0b8f605860c777d43a3770prof = new TwigProfilerProfile($this->getTemplateName(), "block", "bootlint"));

        $internal866741421aeafca21f8fb5e47d5279cda9be214e5ff61ff671ed6199a622ea9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal866741421aeafca21f8fb5e47d5279cda9be214e5ff61ff671ed6199a622ea9a->enter($internal866741421aeafca21f8fb5e47d5279cda9be214e5ff61ff671ed6199a622ea9aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "bootlint"));

        // line 359
        echo "            ";
        // line 360
        echo "            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        ";
        
        $internal866741421aeafca21f8fb5e47d5279cda9be214e5ff61ff671ed6199a622ea9a->leave($internal866741421aeafca21f8fb5e47d5279cda9be214e5ff61ff671ed6199a622ea9aprof);

        
        $internala8bdcbc9008743c44031376b04abe30c167775a64d0b8f605860c777d43a3770->leave($internala8bdcbc9008743c44031376b04abe30c167775a64d0b8f605860c777d43a3770prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::standardlayout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1458 => 360,  1456 => 359,  1447 => 358,  1437 => 321,  1434 => 320,  1425 => 319,  1415 => 350,  1409 => 347,  1406 => 346,  1404 => 345,  1401 => 344,  1395 => 341,  1392 => 340,  1390 => 339,  1387 => 338,  1381 => 336,  1379 => 335,  1376 => 334,  1370 => 332,  1368 => 331,  1365 => 330,  1359 => 328,  1357 => 327,  1354 => 326,  1348 => 324,  1346 => 323,  1343 => 322,  1341 => 319,  1338 => 318,  1329 => 317,  1319 => 303,  1315 => 301,  1309 => 299,  1302 => 295,  1297 => 293,  1294 => 292,  1292 => 291,  1289 => 290,  1286 => 289,  1277 => 288,  1267 => 271,  1261 => 268,  1258 => 267,  1255 => 266,  1246 => 265,  1236 => 312,  1230 => 308,  1224 => 306,  1222 => 305,  1219 => 304,  1217 => 288,  1214 => 287,  1210 => 285,  1195 => 283,  1191 => 282,  1188 => 281,  1186 => 280,  1183 => 279,  1177 => 276,  1174 => 275,  1172 => 274,  1168 => 272,  1166 => 265,  1162 => 263,  1159 => 262,  1150 => 261,  1140 => 313,  1137 => 261,  1128 => 260,  1117 => 351,  1115 => 317,  1110 => 314,  1108 => 260,  1104 => 258,  1095 => 257,  1084 => 247,  1075 => 246,  1064 => 249,  1062 => 246,  1059 => 245,  1050 => 244,  1037 => 242,  1028 => 241,  1010 => 240,  989 => 230,  983 => 228,  974 => 227,  964 => 251,  961 => 244,  958 => 241,  956 => 240,  953 => 239,  950 => 227,  941 => 226,  929 => 252,  927 => 226,  923 => 224,  914 => 223,  903 => 353,  901 => 257,  897 => 255,  894 => 223,  885 => 222,  873 => 210,  871 => 209,  864 => 204,  855 => 203,  844 => 201,  842 => 200,  836 => 196,  827 => 195,  817 => 216,  812 => 213,  809 => 203,  807 => 195,  803 => 193,  800 => 192,  791 => 191,  780 => 187,  776 => 185,  770 => 183,  767 => 182,  764 => 181,  750 => 180,  744 => 178,  740 => 176,  734 => 174,  730 => 172,  727 => 170,  724 => 168,  722 => 167,  717 => 166,  715 => 165,  712 => 164,  710 => 163,  707 => 160,  705 => 159,  703 => 158,  701 => 157,  683 => 156,  680 => 155,  678 => 154,  675 => 153,  673 => 152,  670 => 151,  661 => 150,  650 => 217,  648 => 191,  644 => 189,  642 => 150,  634 => 144,  625 => 143,  615 => 142,  611 => 140,  605 => 138,  602 => 137,  594 => 135,  592 => 134,  587 => 133,  584 => 132,  575 => 131,  561 => 127,  557 => 125,  548 => 124,  537 => 219,  534 => 143,  531 => 131,  529 => 124,  526 => 123,  517 => 122,  499 => 118,  489 => 115,  486 => 114,  483 => 113,  469 => 112,  464 => 110,  461 => 107,  459 => 106,  457 => 105,  455 => 104,  451 => 101,  448 => 100,  445 => 99,  428 => 98,  425 => 97,  422 => 96,  416 => 94,  414 => 93,  408 => 91,  399 => 90,  389 => 66,  380 => 64,  375 => 63,  366 => 62,  345 => 50,  341 => 48,  335 => 47,  327 => 46,  319 => 45,  311 => 44,  307 => 42,  298 => 41,  288 => 87,  285 => 86,  279 => 84,  276 => 83,  273 => 81,  268 => 80,  265 => 79,  262 => 77,  258 => 75,  256 => 73,  254 => 71,  251 => 70,  249 => 69,  247 => 68,  244 => 67,  242 => 62,  239 => 61,  236 => 41,  227 => 40,  217 => 38,  208 => 36,  203 => 35,  194 => 34,  181 => 29,  172 => 28,  154 => 26,  141 => 365,  138 => 364,  135 => 358,  133 => 357,  129 => 355,  127 => 222,  124 => 221,  122 => 122,  115 => 118,  111 => 116,  109 => 90,  105 => 88,  103 => 40,  100 => 39,  98 => 34,  95 => 33,  93 => 28,  88 => 26,  84 => 24,  82 => 23,  80 => 22,  78 => 21,  76 => 20,  74 => 19,  72 => 18,  70 => 17,  68 => 16,  66 => 15,  64 => 14,  62 => 13,  60 => 12,  57 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% set preview = block('preview') is defined ? block('preview')|trim : null %}
{% set form = block('form') is defined ? block('form')|trim : null %}
{% set show = block('show') is defined ? block('show')|trim : null %}
{% set listtable = block('listtable') is defined ? block('listtable')|trim : null %}
{% set listfilters = block('listfilters') is defined ? block('listfilters')|trim : null %}
{% set tabmenu = block('tabmenu') is defined ? block('tabmenu')|trim : null %}
{% set content = block('content') is defined ? block('content')|trim : null %}
{% set title = block('title') is defined ? block('title')|trim : null %}
{% set breadcrumb = block('breadcrumb') is defined ? block('breadcrumb')|trim : null %}
{% set actions = block('actions') is defined ? block('actions')|trim : null %}
{% set navbartitle = block('navbartitle') is defined ? block('navbartitle')|trim : null %}
{% set listfiltersactions = block('listfiltersactions') is defined ? block('listfiltersactions')|trim : null %}

<!DOCTYPE html>
<html {% block htmlattributes %}class=\"no-js\"{% endblock %}>
    <head>
        {% block metatags %}
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {% endblock %}

        {% block stylesheets %}
            {% for stylesheet in sonataadmin.adminPool.getOption('stylesheets', []) %}
                <link rel=\"stylesheet\" href=\"{{ asset(stylesheet) }}\">
            {% endfor %}
        {% endblock %}

        {% block javascripts %}
            {% block sonatajavascriptconfig %}
                <script>
                    window.SONATACONFIG = {
                        CONFIRMEXIT: {% if sonataadmin.adminPool.getOption('confirmexit') %}true{% else %}false{% endif %},
                        USESELECT2: {% if sonataadmin.adminPool.getOption('useselect2') %}true{% else %}false{% endif %},
                        USEICHECK: {% if sonataadmin.adminPool.getOption('useicheck') %}true{% else %}false{% endif %},
                        USESTICKYFORMS: {% if sonataadmin.adminPool.getOption('usestickyforms') %}true{% else %}false{% endif %}
                    };
                    window.SONATATRANSLATIONS = {
                        CONFIRMEXIT: '{{ 'confirmexit'|trans({}, 'SonataAdminBundle')|escape('js') }}'
                    };

                    // http://getbootstrap.com/getting-started/#support-ie10-width
                    if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                        var msViewportStyle = document.createElement('style');
                        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                        document.querySelector('head').appendChild(msViewportStyle);
                    }
                </script>
            {% endblock %}

            {% block sonatajavascriptpool %}
                {% for javascript in sonataadmin.adminPool.getOption('javascripts', []) %}
                    <script src=\"{{ asset(javascript) }}\"></script>
                {% endfor %}
            {% endblock %}

            {% set locale = app.request.locale %}
            {# localize moment #}
            {% if locale[:2] != 'en' %}
                <script src=\"{{ asset(
                    'bundles/sonatacore/vendor/moment/locale/' ~
                    locale|lower|replace({'':'-'}) ~
                    '.js'
                ) }}\"></script>
            {% endif %}

            {# localize select2 #}
            {% if sonataadmin.adminPool.getOption('useselect2') %}
                {% if locale == 'pt' %}{% set locale = 'ptPT' %}{% endif %}

                {# omit default EN locale #}
                {% if locale[:2] != 'en' %}
                    <script src=\"{{ asset('bundles/sonatacore/vendor/select2/select2locale' ~ locale|replace({'':'-'}) ~ '.js') }}\"></script>
                {% endif %}
            {% endif %}
        {% endblock %}

        <title>
        {% block sonataheadtitle %}
            {{ 'Admin'|trans({}, 'SonataAdminBundle') }}

            {% if title is not empty %}
                {{ title|striptags|raw }}
            {% else %}
                {% if action is defined %}
                    -
                    {% for menu in breadcrumbsbuilder.breadcrumbs(admin, action) %}
                        {% if not loop.first %}
                            {% if loop.index != 2 %}
                                &gt;
                            {% endif %}

                            {%- set translationdomain = menu.extra('translationdomain', 'messages') -%}
                            {%- set label = menu.label -%}
                            {%- if translationdomain is not same as(false) -%}
                                {%- set label = label|trans(menu.extra('translationparams', {}), translationdomain) -%}
                            {%- endif -%}

                            {{ label }}
                        {% endif %}
                    {% endfor %}
                {% endif %}
            {% endif %}
        {% endblock %}
        </title>
    </head>
    <body {% block bodyattributes %}class=\"sonata-bc skin-black fixed\"{% endblock %}>

    <div class=\"wrapper\">

        {% block sonataheader %}
            <header class=\"main-header\">
                {% block sonataheadernoscriptwarning %}
                    <noscript>
                        <div class=\"noscript-warning\">
                            {{ 'noscriptwarning'|trans({}, 'SonataAdminBundle') }}
                        </div>
                    </noscript>
                {% endblock %}
                {% block logo %}
                    {% spaceless %}
                        <a class=\"logo\" href=\"{{ path('sonataadmindashboard') }}\">
                            {% if 'singleimage' == sonataadmin.adminPool.getOption('titlemode') or 'both' == sonataadmin.adminPool.getOption('titlemode') %}
                                <img src=\"{{ asset(sonataadmin.adminPool.titlelogo) }}\" alt=\"{{ sonataadmin.adminPool.title }}\">
                            {% endif %}
                            {% if 'singletext' == sonataadmin.adminPool.getOption('titlemode') or 'both' == sonataadmin.adminPool.getOption('titlemode') %}
                                <span>{{ sonataadmin.adminPool.title }}</span>
                            {% endif %}
                        </a>
                    {% endspaceless %}
                {% endblock %}
                {% block sonatanav %}
                    <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                            <span class=\"sr-only\">Toggle navigation</span>
                        </a>

                        <div class=\"navbar-left\">
                            {% block sonatabreadcrumb %}
                                <div class=\"hidden-xs\">
                                    {% if breadcrumb is not empty or action is defined %}
                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            {% if breadcrumb is empty %}
                                                {% if action is defined %}
                                                    {% for menu in breadcrumbsbuilder.breadcrumbs(admin, action) %}
                                                        {%- set translationdomain = menu.extra('translationdomain', 'messages') -%}
                                                        {%- set label = menu.label -%}
                                                        {%- if translationdomain is not same as(false) -%}
                                                            {%- set label = label|trans(menu.extra('translationparams', {}), translationdomain) -%}
                                                        {%- endif -%}

                                                        {% if not loop.last %}
                                                            <li>
                                                                {% if menu.uri is not empty %}
                                                                    <a href=\"{{ menu.uri }}\">
                                                                        {% if menu.extra('safelabel', true) %}
                                                                            {{- label|raw -}}
                                                                        {% else %}
                                                                            {{- label -}}
                                                                        {% endif %}
                                                                    </a>
                                                                {% else %}
                                                                    <span>{{ label }}</span>
                                                                {% endif %}
                                                            </li>
                                                        {% else %}
                                                            <li class=\"active\"><span>{{ label }}</span></li>
                                                        {% endif %}
                                                    {% endfor %}
                                                {% endif %}
                                            {% else %}
                                                {{ breadcrumb|raw }}
                                            {% endif %}
                                        </ol>
                                    {% endif %}
                                </div>
                            {% endblock sonatabreadcrumb %}
                        </div>

                        {% block sonatatopnavmenu %}
                            {% if app.user and isgranted('ROLESONATAADMIN') %}
                                <div class=\"navbar-custom-menu\">
                                    <ul class=\"nav navbar-nav\">
                                        {% block sonatatopnavmenuaddblock %}
                                            <li class=\"dropdown\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-plus-square fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                {% include sonataadmin.adminPool.getTemplate('addblock') %}
                                            </li>
                                        {% endblock %}
                                        {% block sonatatopnavmenuuserblock %}
                                            <li class=\"dropdown user-menu\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-user fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                <ul class=\"dropdown-menu dropdown-user\">
                                                    {% include sonataadmin.adminPool.getTemplate('userblock') %}
                                                </ul>
                                            </li>
                                        {% endblock %}
                                    </ul>
                                </div>
                            {% endif %}
                        {% endblock %}
                    </nav>
                {% endblock sonatanav %}
            </header>
        {% endblock sonataheader %}

        {% block sonatawrapper %}
            {% block sonataleftside %}
                <aside class=\"main-sidebar\">
                    <section class=\"sidebar\">
                        {% block sonatasidenav %}
                            {% block sonatasidebarsearch %}
                                <form action=\"{{ path('sonataadminsearch') }}\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                    <div class=\"input-group custom-search-form\">
                                        <input type=\"text\" name=\"q\" value=\"{{ app.request.get('q') }}\" class=\"form-control\" placeholder=\"{{ 'searchplaceholder'|trans({}, 'SonataAdminBundle') }}\">
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-flat\" type=\"submit\">
                                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            {% endblock sonatasidebarsearch %}

                            {% block sidebarbeforenav %} {% endblock %}
                            {% block sidebarnav %}
                                {{ knpmenurender('sonataadminsidebar', {template: sonataadmin.adminPool.getTemplate('knpmenutemplate')}) }}
                            {% endblock sidebarnav %}
                            {% block sidebarafternav %}
                                <p class=\"text-center small\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                    {% block sidebarafternavcontent %}
                                        <a href=\"https://sonata-project.org\" rel=\"noreferrer\" target=\"blank\">sonata project</a>
                                    {% endblock %}
                                </p>
                            {% endblock %}
                        {% endblock sonatasidenav %}
                    </section>
                </aside>
            {% endblock sonataleftside %}

            <div class=\"content-wrapper\">
                {% block sonatapagecontent %}
                    <section class=\"content-header\">

                        {% block sonatapagecontentheader %}
                            {% block sonatapagecontentnav %}
                                {% if tabmenu is not empty or actions is not empty or listfiltersactions is not empty %}
                                    <nav class=\"navbar navbar-default\" role=\"navigation\">
                                        <div class=\"container-fluid\">
                                            {% block tabmenunavbarheader %}
                                                {% if navbartitle is not empty %}
                                                    <div class=\"navbar-header\">
                                                        <a class=\"navbar-brand\" href=\"#\">{{ navbartitle|raw }}</a>
                                                    </div>
                                                {% endif %}
                                            {% endblock %}

                                            <div class=\"navbar-collapse\">
                                                {% if tabmenu is not empty %}
                                                    <div class=\"navbar-left\">
                                                        {{ tabmenu|raw }}
                                                    </div>
                                                {% endif %}

                                                {% if admin is defined and action is defined and action == 'list' and admin.listModes|length > 1 %}
                                                    <div class=\"nav navbar-right btn-group\">
                                                        {% for mode, settings in admin.listModes %}
                                                            <a href=\"{{ admin.generateUrl('list', app.request.query.all|merge({listmode: mode})) }}\" class=\"btn btn-default navbar-btn btn-sm{% if admin.getListMode() == mode %} active{% endif %}\"><i class=\"{{ settings.class }}\"></i></a>
                                                        {% endfor %}
                                                    </div>
                                                {% endif %}

                                                {% block sonataadmincontentactionswrappers %}
                                                    {% if actions|replace({ '<li>': '', '</li>': '' })|trim is not empty %}
                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        {% if actions|split('</a>')|length > 2 %}
                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{ 'linkactions'|trans({}, 'SonataAdminBundle') }} <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    {{ actions|raw }}
                                                                </ul>
                                                            </li>
                                                        {% else %}
                                                            {{ actions|raw }}
                                                        {% endif %}
                                                        </ul>
                                                    {% endif %}
                                                {% endblock sonataadmincontentactionswrappers %}

                                                {% if listfiltersactions is not empty %}
                                                    {{ listfiltersactions|raw }}
                                                {% endif %}
                                            </div>
                                        </div>
                                    </nav>
                                {% endif %}
                            {% endblock sonatapagecontentnav %}
                        {% endblock sonatapagecontentheader %}
                    </section>

                    <section class=\"content\">
                        {% block sonataadmincontent %}

                            {% block notice %}
                                {% include 'SonataCoreBundle:FlashMessage:render.html.twig' %}
                            {% endblock notice %}

                            {% if preview is not empty %}
                                <div class=\"sonata-ba-preview\">{{ preview|raw }}</div>
                            {% endif %}

                            {% if content is not empty %}
                                <div class=\"sonata-ba-content\">{{ content|raw }}</div>
                            {% endif %}

                            {% if show is not empty %}
                                <div class=\"sonata-ba-show\">{{ show|raw }}</div>
                            {% endif %}

                            {% if form is not empty %}
                                <div class=\"sonata-ba-form\">{{ form|raw }}</div>
                            {% endif %}

                            {% if listfilters is not empty %}
                                <div class=\"row\">
                                    {{ listfilters|raw }}
                                </div>
                            {% endif %}

                            {% if listtable is not empty %}
                                <div class=\"row\">
                                    {{ listtable|raw }}
                                </div>
                            {% endif %}
                        {% endblock sonataadmincontent %}
                    </section>
                {% endblock sonatapagecontent %}
            </div>
        {% endblock sonatawrapper %}
    </div>

    {% if sonataadmin.adminPool.getOption('usebootlint') %}
        {% block bootlint %}
            {# Bootlint - https://github.com/twbs/bootlint#in-the-browser #}
            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        {% endblock %}
    {% endif %}

    </body>
</html>
", "SonataAdminBundle::standardlayout.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/standardlayout.html.twig");
    }
}
