<?php

/* @Framework/Form/choicewidgetoptions.html.php */
class TwigTemplate5be6dd239f50ed7d471cb8afe3b4cbde6580bb9f803a06f1e3429c48397c7dc4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0fb26d6dcb23a1185cf862e43a1b0b276d8f1b32ad02f4ca10650da76a948cae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0fb26d6dcb23a1185cf862e43a1b0b276d8f1b32ad02f4ca10650da76a948cae->enter($internal0fb26d6dcb23a1185cf862e43a1b0b276d8f1b32ad02f4ca10650da76a948caeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetoptions.html.php"));

        $internal5b815f67397c641918fec8ab72c825452d32edba961e2b025f7466ee66a1e154 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5b815f67397c641918fec8ab72c825452d32edba961e2b025f7466ee66a1e154->enter($internal5b815f67397c641918fec8ab72c825452d32edba961e2b025f7466ee66a1e154prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidgetoptions.html.php"));

        // line 1
        echo "<?php use Symfony\\Component\\Form\\ChoiceList\\View\\ChoiceGroupView;

\$translatorHelper = \$view['translator']; // outside of the loop for performance reasons! ?>
<?php \$formHelper = \$view['form']; ?>
<?php foreach (\$choices as \$grouplabel => \$choice): ?>
    <?php if (isarray(\$choice) || \$choice instanceof ChoiceGroupView): ?>
        <optgroup label=\"<?php echo \$view->escape(false !== \$choicetranslationdomain ? \$translatorHelper->trans(\$grouplabel, array(), \$choicetranslationdomain) : \$grouplabel) ?>\">
            <?php echo \$formHelper->block(\$form, 'choicewidgetoptions', array('choices' => \$choice)) ?>
        </optgroup>
    <?php else: ?>
        <option value=\"<?php echo \$view->escape(\$choice->value) ?>\" <?php echo \$formHelper->block(\$form, 'choiceattributes', array('choiceattr' => \$choice->attr)) ?><?php if (\$isselected(\$choice->value, \$value)): ?> selected=\"selected\"<?php endif?>><?php echo \$view->escape(false !== \$choicetranslationdomain ? \$translatorHelper->trans(\$choice->label, array(), \$choicetranslationdomain) : \$choice->label) ?></option>
    <?php endif ?>
<?php endforeach ?>
";
        
        $internal0fb26d6dcb23a1185cf862e43a1b0b276d8f1b32ad02f4ca10650da76a948cae->leave($internal0fb26d6dcb23a1185cf862e43a1b0b276d8f1b32ad02f4ca10650da76a948caeprof);

        
        $internal5b815f67397c641918fec8ab72c825452d32edba961e2b025f7466ee66a1e154->leave($internal5b815f67397c641918fec8ab72c825452d32edba961e2b025f7466ee66a1e154prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choicewidgetoptions.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php use Symfony\\Component\\Form\\ChoiceList\\View\\ChoiceGroupView;

\$translatorHelper = \$view['translator']; // outside of the loop for performance reasons! ?>
<?php \$formHelper = \$view['form']; ?>
<?php foreach (\$choices as \$grouplabel => \$choice): ?>
    <?php if (isarray(\$choice) || \$choice instanceof ChoiceGroupView): ?>
        <optgroup label=\"<?php echo \$view->escape(false !== \$choicetranslationdomain ? \$translatorHelper->trans(\$grouplabel, array(), \$choicetranslationdomain) : \$grouplabel) ?>\">
            <?php echo \$formHelper->block(\$form, 'choicewidgetoptions', array('choices' => \$choice)) ?>
        </optgroup>
    <?php else: ?>
        <option value=\"<?php echo \$view->escape(\$choice->value) ?>\" <?php echo \$formHelper->block(\$form, 'choiceattributes', array('choiceattr' => \$choice->attr)) ?><?php if (\$isselected(\$choice->value, \$value)): ?> selected=\"selected\"<?php endif?>><?php echo \$view->escape(false !== \$choicetranslationdomain ? \$translatorHelper->trans(\$choice->label, array(), \$choicetranslationdomain) : \$choice->label) ?></option>
    <?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choicewidgetoptions.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choicewidgetoptions.html.php");
    }
}
