<?php

/* FOSUserBundle:Profile:show.html.twig */
class TwigTemplateb38b7fe69436a3d4eedd790c8a1478b097064322f1a5f9429f044fe17af7ec1a extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal5820e744e115dde49d6002daba551d9224f3907c3aa4a7aec2f26b6b01e6355e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5820e744e115dde49d6002daba551d9224f3907c3aa4a7aec2f26b6b01e6355e->enter($internal5820e744e115dde49d6002daba551d9224f3907c3aa4a7aec2f26b6b01e6355eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $internal5328aff1c0f2732da9590658c08194e07b543dba44f67ee51d0a7ec005e251aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5328aff1c0f2732da9590658c08194e07b543dba44f67ee51d0a7ec005e251aa->enter($internal5328aff1c0f2732da9590658c08194e07b543dba44f67ee51d0a7ec005e251aaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal5820e744e115dde49d6002daba551d9224f3907c3aa4a7aec2f26b6b01e6355e->leave($internal5820e744e115dde49d6002daba551d9224f3907c3aa4a7aec2f26b6b01e6355eprof);

        
        $internal5328aff1c0f2732da9590658c08194e07b543dba44f67ee51d0a7ec005e251aa->leave($internal5328aff1c0f2732da9590658c08194e07b543dba44f67ee51d0a7ec005e251aaprof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal7f960574e8f4d1b9460a3dec42e4cc2473b5c4e3aae988cea7c9f613244ce2ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7f960574e8f4d1b9460a3dec42e4cc2473b5c4e3aae988cea7c9f613244ce2ca->enter($internal7f960574e8f4d1b9460a3dec42e4cc2473b5c4e3aae988cea7c9f613244ce2caprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internald63f79402c1a7bdf3d3fabde6c9794c8ba52f4e596dbc75d31ef78b1fba69cb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald63f79402c1a7bdf3d3fabde6c9794c8ba52f4e596dbc75d31ef78b1fba69cb8->enter($internald63f79402c1a7bdf3d3fabde6c9794c8ba52f4e596dbc75d31ef78b1fba69cb8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/showcontent.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $internald63f79402c1a7bdf3d3fabde6c9794c8ba52f4e596dbc75d31ef78b1fba69cb8->leave($internald63f79402c1a7bdf3d3fabde6c9794c8ba52f4e596dbc75d31ef78b1fba69cb8prof);

        
        $internal7f960574e8f4d1b9460a3dec42e4cc2473b5c4e3aae988cea7c9f613244ce2ca->leave($internal7f960574e8f4d1b9460a3dec42e4cc2473b5c4e3aae988cea7c9f613244ce2caprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Profile/showcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Profile:show.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}
