<?php

/* @WebProfiler/Icon/time.svg */
class TwigTemplate2c518dbd8b0f2da58796b23cb9d76ecaedc683c148705b3af5f1f3de3fe368b7 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbd1b0da0d08d213539ecd7cfc0d4c32ccf5e45cc125f0b610699a4505baca316 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbd1b0da0d08d213539ecd7cfc0d4c32ccf5e45cc125f0b610699a4505baca316->enter($internalbd1b0da0d08d213539ecd7cfc0d4c32ccf5e45cc125f0b610699a4505baca316prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/time.svg"));

        $internal09408ff09fef74a1edaa1d6c77a3d19489d4cef1e5fe76d3ac5c9ba9997eb614 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal09408ff09fef74a1edaa1d6c77a3d19489d4cef1e5fe76d3ac5c9ba9997eb614->enter($internal09408ff09fef74a1edaa1d6c77a3d19489d4cef1e5fe76d3ac5c9ba9997eb614prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/time.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M15.1,4.3c-2.1-0.5-4.2-0.5-6.2,0C8.6,4.3,8.2,4.1,8.2,3.8V3.4c0-1.2,1-2.3,2.3-2.3h3c1.2,0,2.3,1,2.3,2.3
    v0.3C15.8,4.1,15.4,4.3,15.1,4.3z M20.9,14c0,4.9-4,8.9-8.9,8.9s-8.9-4-8.9-8.9s4-8.9,8.9-8.9S20.9,9.1,20.9,14z M16.7,15
    c0-0.6-0.4-1-1-1H13V8.4c0-0.6-0.4-1-1-1s-1,0.4-1,1v6.2c0,0.6,0.4,1.3,1,1.3h3.7C16.2,16,16.7,15.6,16.7,15z\"/>
</svg>
";
        
        $internalbd1b0da0d08d213539ecd7cfc0d4c32ccf5e45cc125f0b610699a4505baca316->leave($internalbd1b0da0d08d213539ecd7cfc0d4c32ccf5e45cc125f0b610699a4505baca316prof);

        
        $internal09408ff09fef74a1edaa1d6c77a3d19489d4cef1e5fe76d3ac5c9ba9997eb614->leave($internal09408ff09fef74a1edaa1d6c77a3d19489d4cef1e5fe76d3ac5c9ba9997eb614prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/time.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M15.1,4.3c-2.1-0.5-4.2-0.5-6.2,0C8.6,4.3,8.2,4.1,8.2,3.8V3.4c0-1.2,1-2.3,2.3-2.3h3c1.2,0,2.3,1,2.3,2.3
    v0.3C15.8,4.1,15.4,4.3,15.1,4.3z M20.9,14c0,4.9-4,8.9-8.9,8.9s-8.9-4-8.9-8.9s4-8.9,8.9-8.9S20.9,9.1,20.9,14z M16.7,15
    c0-0.6-0.4-1-1-1H13V8.4c0-0.6-0.4-1-1-1s-1,0.4-1,1v6.2c0,0.6,0.4,1.3,1,1.3h3.7C16.2,16,16.7,15.6,16.7,15z\"/>
</svg>
", "@WebProfiler/Icon/time.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/time.svg");
    }
}
