<?php

/* SonataAdminBundle:Core:dashboard.html.twig */
class TwigTemplate6d961819476c80aa11a3c486e142a77992bc58a2bf5ab424fbb9abbd0a2e4711 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'breadcrumb' => array($this, 'blockbreadcrumb'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:Core:dashboard.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala89d189b39597f0b35ca0405e0c33a754839682c401ec1b0bbc6d2fdf4d575e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala89d189b39597f0b35ca0405e0c33a754839682c401ec1b0bbc6d2fdf4d575e3->enter($internala89d189b39597f0b35ca0405e0c33a754839682c401ec1b0bbc6d2fdf4d575e3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:dashboard.html.twig"));

        $internald3ac7c611880f7a9aca3868b11b3c35d415c18725334d40e4f50f0ac2cfffa9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald3ac7c611880f7a9aca3868b11b3c35d415c18725334d40e4f50f0ac2cfffa9f->enter($internald3ac7c611880f7a9aca3868b11b3c35d415c18725334d40e4f50f0ac2cfffa9fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:dashboard.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internala89d189b39597f0b35ca0405e0c33a754839682c401ec1b0bbc6d2fdf4d575e3->leave($internala89d189b39597f0b35ca0405e0c33a754839682c401ec1b0bbc6d2fdf4d575e3prof);

        
        $internald3ac7c611880f7a9aca3868b11b3c35d415c18725334d40e4f50f0ac2cfffa9f->leave($internald3ac7c611880f7a9aca3868b11b3c35d415c18725334d40e4f50f0ac2cfffa9fprof);

    }

    // line 14
    public function blocktitle($context, array $blocks = array())
    {
        $internalbbc07c6d949ce0c2bde31d2c1f8a4517afa317f0b98bf76300f702aa7ce15be1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbbc07c6d949ce0c2bde31d2c1f8a4517afa317f0b98bf76300f702aa7ce15be1->enter($internalbbc07c6d949ce0c2bde31d2c1f8a4517afa317f0b98bf76300f702aa7ce15be1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal25fab0646b0b1d93ebc8852ac8c75b27d103844f85e9df06324a50e177d9bd88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal25fab0646b0b1d93ebc8852ac8c75b27d103844f85e9df06324a50e177d9bd88->enter($internal25fab0646b0b1d93ebc8852ac8c75b27d103844f85e9df06324a50e177d9bd88prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titledashboard", array(), "SonataAdminBundle"), "html", null, true);
        
        $internal25fab0646b0b1d93ebc8852ac8c75b27d103844f85e9df06324a50e177d9bd88->leave($internal25fab0646b0b1d93ebc8852ac8c75b27d103844f85e9df06324a50e177d9bd88prof);

        
        $internalbbc07c6d949ce0c2bde31d2c1f8a4517afa317f0b98bf76300f702aa7ce15be1->leave($internalbbc07c6d949ce0c2bde31d2c1f8a4517afa317f0b98bf76300f702aa7ce15be1prof);

    }

    // line 15
    public function blockbreadcrumb($context, array $blocks = array())
    {
        $internal961a1e1fbcc4ae9588a73e51aaaea4c6f6326a1bde190a2b09140eba42d05bdd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal961a1e1fbcc4ae9588a73e51aaaea4c6f6326a1bde190a2b09140eba42d05bdd->enter($internal961a1e1fbcc4ae9588a73e51aaaea4c6f6326a1bde190a2b09140eba42d05bddprof = new TwigProfilerProfile($this->getTemplateName(), "block", "breadcrumb"));

        $internal198dd45e5000b93c7e97874f45e229b4fa8d75b2d8f3337b60a9df5952f37b72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal198dd45e5000b93c7e97874f45e229b4fa8d75b2d8f3337b60a9df5952f37b72->enter($internal198dd45e5000b93c7e97874f45e229b4fa8d75b2d8f3337b60a9df5952f37b72prof = new TwigProfilerProfile($this->getTemplateName(), "block", "breadcrumb"));

        
        $internal198dd45e5000b93c7e97874f45e229b4fa8d75b2d8f3337b60a9df5952f37b72->leave($internal198dd45e5000b93c7e97874f45e229b4fa8d75b2d8f3337b60a9df5952f37b72prof);

        
        $internal961a1e1fbcc4ae9588a73e51aaaea4c6f6326a1bde190a2b09140eba42d05bdd->leave($internal961a1e1fbcc4ae9588a73e51aaaea4c6f6326a1bde190a2b09140eba42d05bddprof);

    }

    // line 16
    public function blockcontent($context, array $blocks = array())
    {
        $internal81c62ac9de98bcb2e04389a948d3ab41ef58086c3f20a0d711e039c56e47773c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal81c62ac9de98bcb2e04389a948d3ab41ef58086c3f20a0d711e039c56e47773c->enter($internal81c62ac9de98bcb2e04389a948d3ab41ef58086c3f20a0d711e039c56e47773cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internalf8cac28796aaf531eb165adcd508e6b8b8c7c9120971949922125b52561e7d03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf8cac28796aaf531eb165adcd508e6b8b8c7c9120971949922125b52561e7d03->enter($internalf8cac28796aaf531eb165adcd508e6b8b8c7c9120971949922125b52561e7d03prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 17
        echo "
    ";
        // line 18
        $context["hasleft"] = false;
        // line 19
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 19, $this->getSourceContext()); })()), "left", array()));
        foreach ($context['seq'] as $context["key"] => $context["block"]) {
            // line 20
            echo "        ";
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                // line 21
                echo "            ";
                $context["hasleft"] = true;
                // line 22
                echo "        ";
            }
            // line 23
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 24
        echo "
    ";
        // line 25
        $context["hascenter"] = false;
        // line 26
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 26, $this->getSourceContext()); })()), "center", array()));
        foreach ($context['seq'] as $context["key"] => $context["block"]) {
            // line 27
            echo "        ";
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                // line 28
                echo "            ";
                $context["hascenter"] = true;
                // line 29
                echo "        ";
            }
            // line 30
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 31
        echo "
    ";
        // line 32
        $context["hasright"] = false;
        // line 33
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 33, $this->getSourceContext()); })()), "right", array()));
        foreach ($context['seq'] as $context["key"] => $context["block"]) {
            // line 34
            echo "        ";
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                // line 35
                echo "            ";
                $context["hasright"] = true;
                // line 36
                echo "        ";
            }
            // line 37
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 38
        echo "
    ";
        // line 39
        $context["hastop"] = false;
        // line 40
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 40, $this->getSourceContext()); })()), "top", array()));
        foreach ($context['seq'] as $context["key"] => $context["block"]) {
            // line 41
            echo "        ";
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                // line 42
                echo "            ";
                $context["hastop"] = true;
                // line 43
                echo "        ";
            }
            // line 44
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 45
        echo "
    ";
        // line 46
        $context["hasbottom"] = false;
        // line 47
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 47, $this->getSourceContext()); })()), "bottom", array()));
        foreach ($context['seq'] as $context["key"] => $context["block"]) {
            // line 48
            echo "        ";
            if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                // line 49
                echo "            ";
                $context["hasbottom"] = true;
                // line 50
                echo "        ";
            }
            // line 51
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 52
        echo "
    ";
        // line 53
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.dashboard.top", array("adminpool" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 53, $this->getSourceContext()); })()), "adminPool", array()))));
        echo "

    ";
        // line 55
        if ((isset($context["hastop"]) || arraykeyexists("hastop", $context) ? $context["hastop"] : (function () { throw new TwigErrorRuntime('Variable "hastop" does not exist.', 55, $this->getSourceContext()); })())) {
            // line 56
            echo "        <div class=\"row\">
            ";
            // line 57
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 57, $this->getSourceContext()); })()), "top", array()));
            foreach ($context['seq'] as $context["key"] => $context["block"]) {
                // line 58
                echo "                ";
                if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                    // line 59
                    echo "                    <div class=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "class", array()), "html", null, true);
                    echo "\">
                        ";
                    // line 60
                    echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "settings" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "settings", array()))));
                    echo "
                    </div>
                ";
                }
                // line 63
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 64
            echo "        </div>
    ";
        }
        // line 66
        echo "
    <div class=\"row\">
        ";
        // line 68
        $context["widthleft"] = 4;
        // line 69
        echo "        ";
        $context["widthright"] = 4;
        // line 70
        echo "        ";
        $context["widthcenter"] = 4;
        // line 71
        echo "
        ";
        // line 73
        echo "        ";
        if ( !(isset($context["hascenter"]) || arraykeyexists("hascenter", $context) ? $context["hascenter"] : (function () { throw new TwigErrorRuntime('Variable "hascenter" does not exist.', 73, $this->getSourceContext()); })())) {
            // line 74
            echo "            ";
            $context["widthleft"] = 6;
            // line 75
            echo "            ";
            $context["widthright"] = 6;
            // line 76
            echo "        ";
        }
        // line 77
        echo "
        ";
        // line 79
        echo "        ";
        if (( !(isset($context["hasleft"]) || arraykeyexists("hasleft", $context) ? $context["hasleft"] : (function () { throw new TwigErrorRuntime('Variable "hasleft" does not exist.', 79, $this->getSourceContext()); })()) &&  !(isset($context["hasright"]) || arraykeyexists("hasright", $context) ? $context["hasright"] : (function () { throw new TwigErrorRuntime('Variable "hasright" does not exist.', 79, $this->getSourceContext()); })()))) {
            // line 80
            echo "            ";
            $context["widthcenter"] = 12;
            // line 81
            echo "        ";
        }
        // line 82
        echo "
        ";
        // line 84
        echo "        ";
        if (((isset($context["hasleft"]) || arraykeyexists("hasleft", $context) ? $context["hasleft"] : (function () { throw new TwigErrorRuntime('Variable "hasleft" does not exist.', 84, $this->getSourceContext()); })()) || (isset($context["hasright"]) || arraykeyexists("hasright", $context) ? $context["hasright"] : (function () { throw new TwigErrorRuntime('Variable "hasright" does not exist.', 84, $this->getSourceContext()); })()))) {
            // line 85
            echo "        <div class=\"col-md-";
            echo twigescapefilter($this->env, (isset($context["widthleft"]) || arraykeyexists("widthleft", $context) ? $context["widthleft"] : (function () { throw new TwigErrorRuntime('Variable "widthleft" does not exist.', 85, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
            ";
            // line 86
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 86, $this->getSourceContext()); })()), "left", array()));
            foreach ($context['seq'] as $context["key"] => $context["block"]) {
                // line 87
                echo "                ";
                if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                    // line 88
                    echo "                    ";
                    echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "settings" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "settings", array()))));
                    echo "
                ";
                }
                // line 90
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 91
            echo "        </div>
        ";
        }
        // line 93
        echo "
        ";
        // line 94
        if ((isset($context["hascenter"]) || arraykeyexists("hascenter", $context) ? $context["hascenter"] : (function () { throw new TwigErrorRuntime('Variable "hascenter" does not exist.', 94, $this->getSourceContext()); })())) {
            // line 95
            echo "            <div class=\"col-md-";
            echo twigescapefilter($this->env, (isset($context["widthcenter"]) || arraykeyexists("widthcenter", $context) ? $context["widthcenter"] : (function () { throw new TwigErrorRuntime('Variable "widthcenter" does not exist.', 95, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
                ";
            // line 96
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 96, $this->getSourceContext()); })()), "center", array()));
            foreach ($context['seq'] as $context["key"] => $context["block"]) {
                // line 97
                echo "                    ";
                if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                    // line 98
                    echo "                        ";
                    echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "settings" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "settings", array()))));
                    echo "
                    ";
                }
                // line 100
                echo "                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 101
            echo "            </div>
        ";
        }
        // line 103
        echo "
        ";
        // line 105
        echo "        ";
        if (((isset($context["hasleft"]) || arraykeyexists("hasleft", $context) ? $context["hasleft"] : (function () { throw new TwigErrorRuntime('Variable "hasleft" does not exist.', 105, $this->getSourceContext()); })()) || (isset($context["hasright"]) || arraykeyexists("hasright", $context) ? $context["hasright"] : (function () { throw new TwigErrorRuntime('Variable "hasright" does not exist.', 105, $this->getSourceContext()); })()))) {
            // line 106
            echo "         <div class=\"col-md-";
            echo twigescapefilter($this->env, (isset($context["widthright"]) || arraykeyexists("widthright", $context) ? $context["widthright"] : (function () { throw new TwigErrorRuntime('Variable "widthright" does not exist.', 106, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
            ";
            // line 107
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 107, $this->getSourceContext()); })()), "right", array()));
            foreach ($context['seq'] as $context["key"] => $context["block"]) {
                // line 108
                echo "                ";
                if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                    // line 109
                    echo "                    ";
                    echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "settings" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "settings", array()))));
                    echo "
                ";
                }
                // line 111
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 112
            echo "        </div>
        ";
        }
        // line 114
        echo "    </div>

    ";
        // line 116
        if ((isset($context["hasbottom"]) || arraykeyexists("hasbottom", $context) ? $context["hasbottom"] : (function () { throw new TwigErrorRuntime('Variable "hasbottom" does not exist.', 116, $this->getSourceContext()); })())) {
            // line 117
            echo "        <div class=\"row\">
            ";
            // line 118
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["blocks"]) || arraykeyexists("blocks", $context) ? $context["blocks"] : (function () { throw new TwigErrorRuntime('Variable "blocks" does not exist.', 118, $this->getSourceContext()); })()), "bottom", array()));
            foreach ($context['seq'] as $context["key"] => $context["block"]) {
                // line 119
                echo "                ";
                if (((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())) == 0) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted(twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "roles", array())))) {
                    // line 120
                    echo "                    <div class=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "class", array()), "html", null, true);
                    echo "\">
                        ";
                    // line 121
                    echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "type", array()), "settings" => twiggetattribute($this->env, $this->getSourceContext(), $context["block"], "settings", array()))));
                    echo "
                    </div>
                ";
                }
                // line 124
                echo "            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['block'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 125
            echo "        </div>
    ";
        }
        // line 127
        echo "
    ";
        // line 128
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.dashboard.bottom", array("adminpool" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 128, $this->getSourceContext()); })()), "adminPool", array()))));
        echo "

";
        
        $internalf8cac28796aaf531eb165adcd508e6b8b8c7c9120971949922125b52561e7d03->leave($internalf8cac28796aaf531eb165adcd508e6b8b8c7c9120971949922125b52561e7d03prof);

        
        $internal81c62ac9de98bcb2e04389a948d3ab41ef58086c3f20a0d711e039c56e47773c->leave($internal81c62ac9de98bcb2e04389a948d3ab41ef58086c3f20a0d711e039c56e47773cprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  431 => 128,  428 => 127,  424 => 125,  418 => 124,  412 => 121,  407 => 120,  404 => 119,  400 => 118,  397 => 117,  395 => 116,  391 => 114,  387 => 112,  381 => 111,  375 => 109,  372 => 108,  368 => 107,  363 => 106,  360 => 105,  357 => 103,  353 => 101,  347 => 100,  341 => 98,  338 => 97,  334 => 96,  329 => 95,  327 => 94,  324 => 93,  320 => 91,  314 => 90,  308 => 88,  305 => 87,  301 => 86,  296 => 85,  293 => 84,  290 => 82,  287 => 81,  284 => 80,  281 => 79,  278 => 77,  275 => 76,  272 => 75,  269 => 74,  266 => 73,  263 => 71,  260 => 70,  257 => 69,  255 => 68,  251 => 66,  247 => 64,  241 => 63,  235 => 60,  230 => 59,  227 => 58,  223 => 57,  220 => 56,  218 => 55,  213 => 53,  210 => 52,  204 => 51,  201 => 50,  198 => 49,  195 => 48,  190 => 47,  188 => 46,  185 => 45,  179 => 44,  176 => 43,  173 => 42,  170 => 41,  165 => 40,  163 => 39,  160 => 38,  154 => 37,  151 => 36,  148 => 35,  145 => 34,  140 => 33,  138 => 32,  135 => 31,  129 => 30,  126 => 29,  123 => 28,  120 => 27,  115 => 26,  113 => 25,  110 => 24,  104 => 23,  101 => 22,  98 => 21,  95 => 20,  90 => 19,  88 => 18,  85 => 17,  76 => 16,  59 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block title %}{{ 'titledashboard'|trans({}, 'SonataAdminBundle') }}{% endblock%}
{% block breadcrumb %}{% endblock %}
{% block content %}

    {% set hasleft = false %}
    {% for block in blocks.left %}
        {% if block.roles|length == 0 or isgranted(block.roles) %}
            {% set hasleft = true %}
        {% endif %}
    {% endfor %}

    {% set hascenter = false %}
    {% for block in blocks.center %}
        {% if block.roles|length == 0 or isgranted(block.roles) %}
            {% set hascenter = true %}
        {% endif %}
    {% endfor %}

    {% set hasright = false %}
    {% for block in blocks.right %}
        {% if block.roles|length == 0 or isgranted(block.roles) %}
            {% set hasright = true %}
        {% endif %}
    {% endfor %}

    {% set hastop = false %}
    {% for block in blocks.top %}
        {% if block.roles|length == 0 or isgranted(block.roles) %}
            {% set hastop = true %}
        {% endif %}
    {% endfor %}

    {% set hasbottom = false %}
    {% for block in blocks.bottom %}
        {% if block.roles|length == 0 or isgranted(block.roles) %}
            {% set hasbottom = true %}
        {% endif %}
    {% endfor %}

    {{ sonatablockrenderevent('sonata.admin.dashboard.top', { 'adminpool': sonataadmin.adminPool }) }}

    {% if hastop %}
        <div class=\"row\">
            {% for block in blocks.top %}
                {% if block.roles|length == 0 or isgranted(block.roles) %}
                    <div class=\"{{ block.class }}\">
                        {{ sonatablockrender({ 'type': block.type, 'settings': block.settings}) }}
                    </div>
                {% endif %}
            {% endfor %}
        </div>
    {% endif %}

    <div class=\"row\">
        {% set widthleft = 4 %}
        {% set widthright = 4 %}
        {% set widthcenter = 4 %}

        {# if center block is not present we make left and right ones wider #}
        {% if not hascenter %}
            {% set widthleft = 6 %}
            {% set widthright = 6 %}
        {% endif %}

        {# if there is no right and left block present we make center one full width #}
        {% if not hasleft and not hasright %}
            {% set widthcenter = 12 %}
        {% endif %}

        {# don't show left column if only center one is present #}
        {% if hasleft or hasright %}
        <div class=\"col-md-{{ widthleft }}\">
            {% for block in blocks.left %}
                {% if block.roles|length == 0 or isgranted(block.roles) %}
                    {{ sonatablockrender({ 'type': block.type, 'settings': block.settings}) }}
                {% endif %}
            {% endfor %}
        </div>
        {% endif %}

        {% if hascenter %}
            <div class=\"col-md-{{ widthcenter }}\">
                {% for block in blocks.center %}
                    {% if block.roles|length == 0 or isgranted(block.roles) %}
                        {{ sonatablockrender({ 'type': block.type, 'settings': block.settings}) }}
                    {% endif %}
                {% endfor %}
            </div>
        {% endif %}

        {# don't show right column if only center one is present #}
        {% if hasleft or hasright %}
         <div class=\"col-md-{{ widthright }}\">
            {% for block in blocks.right %}
                {% if block.roles|length == 0 or isgranted(block.roles) %}
                    {{ sonatablockrender({ 'type': block.type, 'settings': block.settings}) }}
                {% endif %}
            {% endfor %}
        </div>
        {% endif %}
    </div>

    {% if hasbottom %}
        <div class=\"row\">
            {% for block in blocks.bottom %}
                {% if block.roles|length == 0 or isgranted(block.roles) %}
                    <div class=\"{{ block.class }}\">
                        {{ sonatablockrender({ 'type': block.type, 'settings': block.settings}) }}
                    </div>
                {% endif %}
            {% endfor %}
        </div>
    {% endif %}

    {{ sonatablockrenderevent('sonata.admin.dashboard.bottom', { 'adminpool': sonataadmin.adminPool }) }}

{% endblock %}
", "SonataAdminBundle:Core:dashboard.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/dashboard.html.twig");
    }
}
