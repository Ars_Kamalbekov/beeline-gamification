<?php

/* SonataAdminBundle:CRUD/Association:editmanytoone.html.twig */
class TwigTemplate5c19b2a1e949f48722bb28f1d6a34074f2095b5e84fe785a8a627a7396426104 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3253124f4c5f7a06fbf354a8468f6bf0cd3269a02e1aa4cbe129a71be56af7ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3253124f4c5f7a06fbf354a8468f6bf0cd3269a02e1aa4cbe129a71be56af7ba->enter($internal3253124f4c5f7a06fbf354a8468f6bf0cd3269a02e1aa4cbe129a71be56af7baprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig"));

        $internal6f66753575805fce75b12344b77ececa36e43d01c875f749f3bee78789e5f41d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6f66753575805fce75b12344b77ececa36e43d01c875f749f3bee78789e5f41d->enter($internal6f66753575805fce75b12344b77ececa36e43d01c875f749f3bee78789e5f41dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 12, $this->getSourceContext()); })()), "fielddescription", array()), "hasassociationadmin", array())) {
            // line 13
            echo "    ";
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 13, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 13, $this->getSourceContext()); })()), "fielddescription", array())), "html", null, true);
            echo "
";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(),         // line 14
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 14, $this->getSourceContext()); })()), "edit", array()) == "inline")) {
            // line 15
            echo "    ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 15, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "formfielddescriptions", array()));
            foreach ($context['seq'] as $context["key"] => $context["fielddescription"]) {
                // line 16
                echo "        ";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 16, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "name", array()), array(), "array"), 'row');
                echo "
    ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['fielddescription'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
        } else {
            // line 19
            echo "    <div id=\"fieldcontainer";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
            echo "\" class=\"field-container\">
        ";
            // line 20
            if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 20, $this->getSourceContext()); })()), "edit", array()) == "list")) {
                // line 21
                echo "            <span id=\"fieldwidget";
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
                echo "\" class=\"field-short-description\">
                ";
                // line 22
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 22, $this->getSourceContext()); })()), "admin", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 22, $this->getSourceContext()); })()), "value", array())), "method")) {
                    // line 23
                    echo "                    ";
                    echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminshortobjectinformation", array("code" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 24
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 24, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "code", array()), "objectId" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 25
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 25, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 25, $this->getSourceContext()); })()), "value", array())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 26
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 26, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "uniqid", array()), "linkParameters" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                     // line 27
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 27, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "linkparameters", array()))));
                    // line 28
                    echo "
                ";
                } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),                 // line 29
($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "placeholder", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 29, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "placeholder", array()))) {
                    // line 30
                    echo "                    <span class=\"inner-field-short-description\">
                        ";
                    // line 31
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 31, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "placeholder", array()), array(), "SonataAdminBundle"), "html", null, true);
                    echo "
                    </span>
                ";
                }
                // line 34
                echo "            </span>
            <span style=\"display: none\" >
                ";
                // line 36
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 36, $this->getSourceContext()); })()), 'widget');
                echo "
            </span>
        ";
            } else {
                // line 39
                echo "            <span id=\"fieldwidget";
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
                echo "\" >
                ";
                // line 40
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 40, $this->getSourceContext()); })()), 'widget');
                echo "
            </span>
        ";
            }
            // line 43
            echo "
        <div id=\"fieldactions";
            // line 44
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 44, $this->getSourceContext()); })()), "html", null, true);
            echo "\" class=\"field-actions\">
            ";
            // line 45
            $context["displaybtnlist"] = ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 45, $this->getSourceContext()); })()), "edit", array()) == "list") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 45, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasRoute", array(0 => "list"), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 45, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "LIST"), "method")) && (isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 45, $this->getSourceContext()); })()));
            // line 46
            echo "            ";
            $context["displaybtnadd"] = ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 46, $this->getSourceContext()); })()), "edit", array()) != "admin") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 46, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasRoute", array(0 => "create"), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 46, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "CREATE"), "method")) && (isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 46, $this->getSourceContext()); })()));
            // line 47
            echo "            ";
            if (((isset($context["displaybtnlist"]) || arraykeyexists("displaybtnlist", $context) ? $context["displaybtnlist"] : (function () { throw new TwigErrorRuntime('Variable "displaybtnlist" does not exist.', 47, $this->getSourceContext()); })()) || (isset($context["displaybtnadd"]) || arraykeyexists("displaybtnadd", $context) ? $context["displaybtnadd"] : (function () { throw new TwigErrorRuntime('Variable "displaybtnadd" does not exist.', 47, $this->getSourceContext()); })()))) {
                // line 48
                echo "            <div class=\"btn-group\">
                ";
                // line 49
                if ((isset($context["displaybtnlist"]) || arraykeyexists("displaybtnlist", $context) ? $context["displaybtnlist"] : (function () { throw new TwigErrorRuntime('Variable "displaybtnlist" does not exist.', 49, $this->getSourceContext()); })())) {
                    // line 50
                    echo "                    <a  href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 50, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "list", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 50, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    echo "\"
                        onclick=\"return startfielddialogformlist";
                    // line 51
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 51, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                        class=\"btn btn-info btn-sm sonata-ba-action\"
                        title=\"";
                    // line 53
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 53, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 53, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                        >
                        <i class=\"fa fa-list\"></i>
                        ";
                    // line 56
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 56, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 56, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 59
                echo "
                ";
                // line 60
                if ((isset($context["displaybtnadd"]) || arraykeyexists("displaybtnadd", $context) ? $context["displaybtnadd"] : (function () { throw new TwigErrorRuntime('Variable "displaybtnadd" does not exist.', 60, $this->getSourceContext()); })())) {
                    // line 61
                    echo "                    <a  href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 61, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create", 1 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 61, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")), "method"), "html", null, true);
                    echo "\"
                        onclick=\"return startfielddialogformadd";
                    // line 62
                    echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 62, $this->getSourceContext()); })()), "html", null, true);
                    echo "(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"";
                    // line 64
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 64, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 64, $this->getSourceContext()); })())), "html", null, true);
                    echo "\"
                        >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
                    // line 67
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 67, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 67, $this->getSourceContext()); })())), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 70
                echo "            </div>
            ";
            }
            // line 72
            echo "
            ";
            // line 73
            if (((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 73, $this->getSourceContext()); })()), "edit", array()) == "list") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 73, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasRoute", array(0 => "delete"), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 73, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "DELETE"), "method")) && (isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 73, $this->getSourceContext()); })()))) {
                // line 74
                echo "                <a  href=\"\"
                    onclick=\"return removeselectedelement";
                // line 75
                echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 75, $this->getSourceContext()); })()), "html", null, true);
                echo "(this);\"
                    class=\"btn btn-danger btn-sm sonata-ba-action\"
                    title=\"";
                // line 77
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 77, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 77, $this->getSourceContext()); })())), "html", null, true);
                echo "\"
                    >
                    <i class=\"fa fa-minus-circle\"></i>
                    ";
                // line 80
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 80, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 80, $this->getSourceContext()); })())), "html", null, true);
                echo "
                </a>
            ";
            }
            // line 83
            echo "        </div>

        ";
            // line 85
            $this->loadTemplate("SonataAdminBundle:CRUD/Association:editmodal.html.twig", "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig", 85)->display($context);
            // line 86
            echo "    </div>

    ";
            // line 88
            $this->loadTemplate("SonataAdminBundle:CRUD/Association:editmanyscript.html.twig", "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig", 88)->display($context);
        }
        
        $internal3253124f4c5f7a06fbf354a8468f6bf0cd3269a02e1aa4cbe129a71be56af7ba->leave($internal3253124f4c5f7a06fbf354a8468f6bf0cd3269a02e1aa4cbe129a71be56af7baprof);

        
        $internal6f66753575805fce75b12344b77ececa36e43d01c875f749f3bee78789e5f41d->leave($internal6f66753575805fce75b12344b77ececa36e43d01c875f749f3bee78789e5f41dprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 88,  212 => 86,  210 => 85,  206 => 83,  200 => 80,  194 => 77,  189 => 75,  186 => 74,  184 => 73,  181 => 72,  177 => 70,  171 => 67,  165 => 64,  160 => 62,  155 => 61,  153 => 60,  150 => 59,  144 => 56,  138 => 53,  133 => 51,  128 => 50,  126 => 49,  123 => 48,  120 => 47,  117 => 46,  115 => 45,  111 => 44,  108 => 43,  102 => 40,  97 => 39,  91 => 36,  87 => 34,  81 => 31,  78 => 30,  76 => 29,  73 => 28,  71 => 27,  70 => 26,  69 => 25,  68 => 24,  66 => 23,  64 => 22,  59 => 21,  57 => 20,  52 => 19,  42 => 16,  37 => 15,  35 => 14,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if not sonataadmin.fielddescription.hasassociationadmin%}
    {{ value|renderrelationelement(sonataadmin.fielddescription) }}
{% elseif sonataadmin.edit == 'inline' %}
    {% for fielddescription in sonataadmin.fielddescription.associationadmin.formfielddescriptions %}
        {{ formrow(form[fielddescription.name])}}
    {% endfor %}
{% else %}
    <div id=\"fieldcontainer{{ id }}\" class=\"field-container\">
        {% if sonataadmin.edit == 'list' %}
            <span id=\"fieldwidget{{ id }}\" class=\"field-short-description\">
                {% if sonataadmin.admin.id(sonataadmin.value) %}
                    {{ render(path('sonataadminshortobjectinformation', {
                        'code':     sonataadmin.fielddescription.associationadmin.code,
                        'objectId': sonataadmin.fielddescription.associationadmin.id(sonataadmin.value),
                        'uniqid':   sonataadmin.fielddescription.associationadmin.uniqid,
                        'linkParameters': sonataadmin.fielddescription.options.linkparameters
                    })) }}
                {% elseif sonataadmin.fielddescription.options.placeholder is defined and sonataadmin.fielddescription.options.placeholder %}
                    <span class=\"inner-field-short-description\">
                        {{ sonataadmin.fielddescription.options.placeholder|trans({}, 'SonataAdminBundle') }}
                    </span>
                {% endif %}
            </span>
            <span style=\"display: none\" >
                {{ formwidget(form) }}
            </span>
        {% else %}
            <span id=\"fieldwidget{{ id }}\" >
                {{ formwidget(form) }}
            </span>
        {% endif %}

        <div id=\"fieldactions{{ id }}\" class=\"field-actions\">
            {% set displaybtnlist = sonataadmin.edit == 'list' and sonataadmin.fielddescription.associationadmin.hasRoute('list') and sonataadmin.fielddescription.associationadmin.isGranted('LIST') and btnlist %}
            {% set displaybtnadd = sonataadmin.edit != 'admin' and sonataadmin.fielddescription.associationadmin.hasRoute('create') and sonataadmin.fielddescription.associationadmin.isGranted('CREATE') and btnadd %}
            {% if displaybtnlist or displaybtnadd %}
            <div class=\"btn-group\">
                {% if displaybtnlist %}
                    <a  href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('list', sonataadmin.fielddescription.getOption('linkparameters', {})) }}\"
                        onclick=\"return startfielddialogformlist{{ id }}(this);\"
                        class=\"btn btn-info btn-sm sonata-ba-action\"
                        title=\"{{ btnlist|trans({}, btncatalogue) }}\"
                        >
                        <i class=\"fa fa-list\"></i>
                        {{ btnlist|trans({}, btncatalogue) }}
                    </a>
                {% endif %}

                {% if displaybtnadd %}
                    <a  href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('create', sonataadmin.fielddescription.getOption('linkparameters', {})) }}\"
                        onclick=\"return startfielddialogformadd{{ id }}(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                        >
                        <i class=\"fa fa-plus-circle\"></i>
                        {{ btnadd|trans({}, btncatalogue) }}
                    </a>
                {% endif %}
            </div>
            {% endif %}

            {% if sonataadmin.edit == 'list' and sonataadmin.fielddescription.associationadmin.hasRoute('delete') and sonataadmin.fielddescription.associationadmin.isGranted('DELETE') and btndelete %}
                <a  href=\"\"
                    onclick=\"return removeselectedelement{{ id }}(this);\"
                    class=\"btn btn-danger btn-sm sonata-ba-action\"
                    title=\"{{ btndelete|trans({}, btncatalogue) }}\"
                    >
                    <i class=\"fa fa-minus-circle\"></i>
                    {{ btndelete|trans({}, btncatalogue) }}
                </a>
            {% endif %}
        </div>

        {% include 'SonataAdminBundle:CRUD/Association:editmodal.html.twig' %}
    </div>

    {% include 'SonataAdminBundle:CRUD/Association:editmanyscript.html.twig' %}
{% endif %}
", "SonataAdminBundle:CRUD/Association:editmanytoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/editmanytoone.html.twig");
    }
}
