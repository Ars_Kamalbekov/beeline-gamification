<?php

/* SonataAdminBundle:CRUD:showarray.html.twig */
class TwigTemplate722d19a7ab81837f91e1e32a6682a74fc768da3b3350542737bf54d8ab226fe2 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 13
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showarray.html.twig", 13);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald21dc2606f38ae6c93beaec60bf3527e74846807622f9b3400d7d7d84fc62d40 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald21dc2606f38ae6c93beaec60bf3527e74846807622f9b3400d7d7d84fc62d40->enter($internald21dc2606f38ae6c93beaec60bf3527e74846807622f9b3400d7d7d84fc62d40prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showarray.html.twig"));

        $internalc77746331b970b09a800dee05fd1195a5b0ab666cffb8a9b4c5043ca8a4711bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc77746331b970b09a800dee05fd1195a5b0ab666cffb8a9b4c5043ca8a4711bf->enter($internalc77746331b970b09a800dee05fd1195a5b0ab666cffb8a9b4c5043ca8a4711bfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showarray.html.twig"));

        // line 11
        $context["show"] = $this->loadTemplate("SonataAdminBundle:CRUD:basearraymacro.html.twig", "SonataAdminBundle:CRUD:showarray.html.twig", 11);
        // line 13
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald21dc2606f38ae6c93beaec60bf3527e74846807622f9b3400d7d7d84fc62d40->leave($internald21dc2606f38ae6c93beaec60bf3527e74846807622f9b3400d7d7d84fc62d40prof);

        
        $internalc77746331b970b09a800dee05fd1195a5b0ab666cffb8a9b4c5043ca8a4711bf->leave($internalc77746331b970b09a800dee05fd1195a5b0ab666cffb8a9b4c5043ca8a4711bfprof);

    }

    // line 15
    public function blockfield($context, array $blocks = array())
    {
        $internal23bfe6a49195db81ecf322795829f577542cb647cf6dbf2fad599c9fc0a9134e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal23bfe6a49195db81ecf322795829f577542cb647cf6dbf2fad599c9fc0a9134e->enter($internal23bfe6a49195db81ecf322795829f577542cb647cf6dbf2fad599c9fc0a9134eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal30db46a479e91933ec646925d61cd1cd0db36b03cf076c896389bc7eddd05150 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal30db46a479e91933ec646925d61cd1cd0db36b03cf076c896389bc7eddd05150->enter($internal30db46a479e91933ec646925d61cd1cd0db36b03cf076c896389bc7eddd05150prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["show"]->macrorenderarray((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "inline", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "inline", array()), false)) : (false)));
        echo "
";
        
        $internal30db46a479e91933ec646925d61cd1cd0db36b03cf076c896389bc7eddd05150->leave($internal30db46a479e91933ec646925d61cd1cd0db36b03cf076c896389bc7eddd05150prof);

        
        $internal23bfe6a49195db81ecf322795829f577542cb647cf6dbf2fad599c9fc0a9134e->leave($internal23bfe6a49195db81ecf322795829f577542cb647cf6dbf2fad599c9fc0a9134eprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showarray.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  43 => 15,  33 => 13,  31 => 11,  11 => 13,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:basearraymacro.html.twig' as show %}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field%}
    {{ show.renderarray(value, fielddescription.options.inline|default(false)) }}
{% endblock %}
", "SonataAdminBundle:CRUD:showarray.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showarray.html.twig");
    }
}
