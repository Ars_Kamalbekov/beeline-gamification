<?php

/* SonataAdminBundle:CRUD:basefilterfield.html.twig */
class TwigTemplate3ff271ac0040d9a40fd275c70a19b24c571f0fd0e4a70029986fe8504e2d87c0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'blocklabel'),
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb581e5c530799809dc979b20b8c7a93a5f03e43326391f80f56f69279eb8964f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb581e5c530799809dc979b20b8c7a93a5f03e43326391f80f56f69279eb8964f->enter($internalb581e5c530799809dc979b20b8c7a93a5f03e43326391f80f56f69279eb8964fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basefilterfield.html.twig"));

        $internal8c317bc14726506606bfa56509a23a6aee089a4d4e75e3b9e8feb717cf4df6c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8c317bc14726506606bfa56509a23a6aee089a4d4e75e3b9e8feb717cf4df6c5->enter($internal8c317bc14726506606bfa56509a23a6aee089a4d4e75e3b9e8feb717cf4df6c5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basefilterfield.html.twig"));

        // line 11
        echo "

<div>
    ";
        // line 14
        $this->displayBlock('label', $context, $blocks);
        // line 22
        echo "
    <div class=\"sonata-ba-field";
        // line 23
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["filterform"]) || arraykeyexists("filterform", $context) ? $context["filterform"] : (function () { throw new TwigErrorRuntime('Variable "filterform" does not exist.', 23, $this->getSourceContext()); })()), "vars", array()), "errors", array())) {
            echo " sonata-ba-field-error";
        }
        echo "\">
        ";
        // line 24
        $this->displayBlock('field', $context, $blocks);
        // line 25
        echo "    </div>
</div>
";
        
        $internalb581e5c530799809dc979b20b8c7a93a5f03e43326391f80f56f69279eb8964f->leave($internalb581e5c530799809dc979b20b8c7a93a5f03e43326391f80f56f69279eb8964fprof);

        
        $internal8c317bc14726506606bfa56509a23a6aee089a4d4e75e3b9e8feb717cf4df6c5->leave($internal8c317bc14726506606bfa56509a23a6aee089a4d4e75e3b9e8feb717cf4df6c5prof);

    }

    // line 14
    public function blocklabel($context, array $blocks = array())
    {
        $internala3ad56a48f61a0b75975109076bf968c0506532bbd83d348c819fb31207b5404 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala3ad56a48f61a0b75975109076bf968c0506532bbd83d348c819fb31207b5404->enter($internala3ad56a48f61a0b75975109076bf968c0506532bbd83d348c819fb31207b5404prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal48c8e2b25e5716d9cc0609769687b79e134bca6109424b069ce3ced5442e6fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal48c8e2b25e5716d9cc0609769687b79e134bca6109424b069ce3ced5442e6fd8->enter($internal48c8e2b25e5716d9cc0609769687b79e134bca6109424b069ce3ced5442e6fd8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 15
        echo "        ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["filter"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 16
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filterform"]) || arraykeyexists("filterform", $context) ? $context["filterform"] : (function () { throw new TwigErrorRuntime('Variable "filterform" does not exist.', 16, $this->getSourceContext()); })()), 'label', (twigtestempty($label = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["filter"]) || arraykeyexists("filter", $context) ? $context["filter"] : (function () { throw new TwigErrorRuntime('Variable "filter" does not exist.', 16, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "name", array())) ? array() : array("label" => $label)));
            echo "
        ";
        } else {
            // line 18
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filterform"]) || arraykeyexists("filterform", $context) ? $context["filterform"] : (function () { throw new TwigErrorRuntime('Variable "filterform" does not exist.', 18, $this->getSourceContext()); })()), 'label');
            echo "
        ";
        }
        // line 20
        echo "        <br>
    ";
        
        $internal48c8e2b25e5716d9cc0609769687b79e134bca6109424b069ce3ced5442e6fd8->leave($internal48c8e2b25e5716d9cc0609769687b79e134bca6109424b069ce3ced5442e6fd8prof);

        
        $internala3ad56a48f61a0b75975109076bf968c0506532bbd83d348c819fb31207b5404->leave($internala3ad56a48f61a0b75975109076bf968c0506532bbd83d348c819fb31207b5404prof);

    }

    // line 24
    public function blockfield($context, array $blocks = array())
    {
        $internal22e0582c2dc438413cbf9db48b53b677d653960f1bdf88cbe53b8b4eb1c3a948 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal22e0582c2dc438413cbf9db48b53b677d653960f1bdf88cbe53b8b4eb1c3a948->enter($internal22e0582c2dc438413cbf9db48b53b677d653960f1bdf88cbe53b8b4eb1c3a948prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal3f632991036a25681431a25b76aa0af04d9d224f67ac9c6fee386671765929bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3f632991036a25681431a25b76aa0af04d9d224f67ac9c6fee386671765929bd->enter($internal3f632991036a25681431a25b76aa0af04d9d224f67ac9c6fee386671765929bdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["filterform"]) || arraykeyexists("filterform", $context) ? $context["filterform"] : (function () { throw new TwigErrorRuntime('Variable "filterform" does not exist.', 24, $this->getSourceContext()); })()), 'widget');
        
        $internal3f632991036a25681431a25b76aa0af04d9d224f67ac9c6fee386671765929bd->leave($internal3f632991036a25681431a25b76aa0af04d9d224f67ac9c6fee386671765929bdprof);

        
        $internal22e0582c2dc438413cbf9db48b53b677d653960f1bdf88cbe53b8b4eb1c3a948->leave($internal22e0582c2dc438413cbf9db48b53b677d653960f1bdf88cbe53b8b4eb1c3a948prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:basefilterfield.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 24,  81 => 20,  75 => 18,  69 => 16,  66 => 15,  57 => 14,  45 => 25,  43 => 24,  37 => 23,  34 => 22,  32 => 14,  27 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}


<div>
    {% block label %}
        {% if filter.fielddescription.options.name is defined %}
            {{ formlabel(filterform, filter.fielddescription.options.name) }}
        {% else %}
            {{ formlabel(filterform) }}
        {% endif %}
        <br>
    {% endblock %}

    <div class=\"sonata-ba-field{% if filterform.vars.errors %} sonata-ba-field-error{% endif %}\">
        {% block field %}{{ formwidget(filterform) }}{% endblock %}
    </div>
</div>
", "SonataAdminBundle:CRUD:basefilterfield.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/basefilterfield.html.twig");
    }
}
