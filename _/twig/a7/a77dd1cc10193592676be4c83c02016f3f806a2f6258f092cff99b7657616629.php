<?php

/* SonataAdminBundle:CRUD:baseshowmacro.html.twig */
class TwigTemplate19f49b3b8adffc9af500c761633cba88d359735f2366f15992da7314e84632ee extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fieldrow' => array($this, 'blockfieldrow'),
            'showtitle' => array($this, 'blockshowtitle'),
            'showfield' => array($this, 'blockshowfield'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc264fd10decd5af059b2c7470ad24539cabc92d99819f63bada98330985aa389 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc264fd10decd5af059b2c7470ad24539cabc92d99819f63bada98330985aa389->enter($internalc264fd10decd5af059b2c7470ad24539cabc92d99819f63bada98330985aa389prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowmacro.html.twig"));

        $internale88d56082a86c79a77a82c4ccf1fa2d19c742717fe3e5d6831a2344f6945f9c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale88d56082a86c79a77a82c4ccf1fa2d19c742717fe3e5d6831a2344f6945f9c3->enter($internale88d56082a86c79a77a82c4ccf1fa2d19c742717fe3e5d6831a2344f6945f9c3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baseshowmacro.html.twig"));

        // line 2
        echo "
";
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('fieldrow', $context, $blocks);
        
        $internalc264fd10decd5af059b2c7470ad24539cabc92d99819f63bada98330985aa389->leave($internalc264fd10decd5af059b2c7470ad24539cabc92d99819f63bada98330985aa389prof);

        
        $internale88d56082a86c79a77a82c4ccf1fa2d19c742717fe3e5d6831a2344f6945f9c3->leave($internale88d56082a86c79a77a82c4ccf1fa2d19c742717fe3e5d6831a2344f6945f9c3prof);

    }

    public function blockfieldrow($context, array $blocks = array())
    {
        $internaldb62b015a97d10385f8b8504eb4515973726f1d413e6002c70b3eec45d17acdd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldb62b015a97d10385f8b8504eb4515973726f1d413e6002c70b3eec45d17acdd->enter($internaldb62b015a97d10385f8b8504eb4515973726f1d413e6002c70b3eec45d17acddprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldrow"));

        $internal060c53fc0233f97781d77adcaa93b4ce5943fec934761e8c6ec2a3bad765643b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal060c53fc0233f97781d77adcaa93b4ce5943fec934761e8c6ec2a3bad765643b->enter($internal060c53fc0233f97781d77adcaa93b4ce5943fec934761e8c6ec2a3bad765643bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldrow"));

        // line 10
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 10, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["code"]) {
            // line 11
            echo "        ";
            $context["showgroup"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 11, $this->getSourceContext()); })()), "showgroups", array()), $context["code"], array(), "array");
            // line 12
            echo "
        <div class=\"";
            // line 13
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "class", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["showgroup"] ?? null), "class", array()), "col-md-12")) : ("col-md-12")), "html", null, true);
            echo " ";
            echo (((isset($context["nopadding"]) || arraykeyexists("nopadding", $context) ? $context["nopadding"] : (function () { throw new TwigErrorRuntime('Variable "nopadding" does not exist.', 13, $this->getSourceContext()); })())) ? ("nopadding") : (""));
            echo "\">
            <div class=\"";
            // line 14
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 14, $this->getSourceContext()); })()), "boxclass", array()), "html", null, true);
            echo "\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        ";
            // line 17
            $this->displayBlock('showtitle', $context, $blocks);
            // line 20
            echo "                    </h4>
                </div>
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\">
                        <tbody>
                        ";
            // line 25
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 25, $this->getSourceContext()); })()), "fields", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["fieldname"]) {
                // line 26
                echo "                            ";
                $this->displayBlock('showfield', $context, $blocks);
                // line 33
                echo "                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['fieldname'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 34
            echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['code'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internal060c53fc0233f97781d77adcaa93b4ce5943fec934761e8c6ec2a3bad765643b->leave($internal060c53fc0233f97781d77adcaa93b4ce5943fec934761e8c6ec2a3bad765643bprof);

        
        $internaldb62b015a97d10385f8b8504eb4515973726f1d413e6002c70b3eec45d17acdd->leave($internaldb62b015a97d10385f8b8504eb4515973726f1d413e6002c70b3eec45d17acddprof);

    }

    // line 17
    public function blockshowtitle($context, array $blocks = array())
    {
        $internal85386b1b054c70b4ffb2e834f17c1d5b260c3d91fc462c2655e29e0ee223e8c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal85386b1b054c70b4ffb2e834f17c1d5b260c3d91fc462c2655e29e0ee223e8c9->enter($internal85386b1b054c70b4ffb2e834f17c1d5b260c3d91fc462c2655e29e0ee223e8c9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showtitle"));

        $internal0ba0bdd19b0e9934062ba2ae063f4e955f08dad482442afd22c3f9a4b538ee5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0ba0bdd19b0e9934062ba2ae063f4e955f08dad482442afd22c3f9a4b538ee5b->enter($internal0ba0bdd19b0e9934062ba2ae063f4e955f08dad482442afd22c3f9a4b538ee5bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "showtitle"));

        // line 18
        echo "                            ";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 18, $this->getSourceContext()); })()), "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 18, $this->getSourceContext()); })()), "translationdomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["showgroup"]) || arraykeyexists("showgroup", $context) ? $context["showgroup"] : (function () { throw new TwigErrorRuntime('Variable "showgroup" does not exist.', 18, $this->getSourceContext()); })()), "translationdomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
        echo "
                        ";
        
        $internal0ba0bdd19b0e9934062ba2ae063f4e955f08dad482442afd22c3f9a4b538ee5b->leave($internal0ba0bdd19b0e9934062ba2ae063f4e955f08dad482442afd22c3f9a4b538ee5bprof);

        
        $internal85386b1b054c70b4ffb2e834f17c1d5b260c3d91fc462c2655e29e0ee223e8c9->leave($internal85386b1b054c70b4ffb2e834f17c1d5b260c3d91fc462c2655e29e0ee223e8c9prof);

    }

    // line 26
    public function blockshowfield($context, array $blocks = array())
    {
        $internal2461b096242d0d7d31956e5730c7e766489a0f92e66d90471faae43b5cb9aef4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2461b096242d0d7d31956e5730c7e766489a0f92e66d90471faae43b5cb9aef4->enter($internal2461b096242d0d7d31956e5730c7e766489a0f92e66d90471faae43b5cb9aef4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        $internaldebefd96f024beca0ea9568b376cb01cb2c0f09fdf9a1e1f8ee4218a78a0a993 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldebefd96f024beca0ea9568b376cb01cb2c0f09fdf9a1e1f8ee4218a78a0a993->enter($internaldebefd96f024beca0ea9568b376cb01cb2c0f09fdf9a1e1f8ee4218a78a0a993prof = new TwigProfilerProfile($this->getTemplateName(), "block", "showfield"));

        // line 27
        echo "                                <tr class=\"sonata-ba-view-container\">
                                    ";
        // line 28
        if (twiggetattribute($this->env, $this->getSourceContext(), ($context["elements"] ?? null), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 28, $this->getSourceContext()); })()), array(), "array", true, true)) {
            // line 29
            echo "                                        ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["elements"]) || arraykeyexists("elements", $context) ? $context["elements"] : (function () { throw new TwigErrorRuntime('Variable "elements" does not exist.', 29, $this->getSourceContext()); })()), (isset($context["fieldname"]) || arraykeyexists("fieldname", $context) ? $context["fieldname"] : (function () { throw new TwigErrorRuntime('Variable "fieldname" does not exist.', 29, $this->getSourceContext()); })()), array(), "array"), (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 29, $this->getSourceContext()); })()));
            echo "
                                    ";
        }
        // line 31
        echo "                                </tr>
                            ";
        
        $internaldebefd96f024beca0ea9568b376cb01cb2c0f09fdf9a1e1f8ee4218a78a0a993->leave($internaldebefd96f024beca0ea9568b376cb01cb2c0f09fdf9a1e1f8ee4218a78a0a993prof);

        
        $internal2461b096242d0d7d31956e5730c7e766489a0f92e66d90471faae43b5cb9aef4->leave($internal2461b096242d0d7d31956e5730c7e766489a0f92e66d90471faae43b5cb9aef4prof);

    }

    // line 3
    public function macrorendergroups($admin = null, $object = null, $elements = null, $groups = null, $hastab = null, $nopadding = false, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "admin" => $admin,
            "object" => $object,
            "elements" => $elements,
            "groups" => $groups,
            "hastab" => $hastab,
            "nopadding" => $nopadding,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal717512a73acf1e5ed9c3e06078a526192d69587b13059b23d2b19f0ef9402946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal717512a73acf1e5ed9c3e06078a526192d69587b13059b23d2b19f0ef9402946->enter($internal717512a73acf1e5ed9c3e06078a526192d69587b13059b23d2b19f0ef9402946prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendergroups"));

            $internal1639b6e2584ba9d3180546983ded329d012e803ce3e12b58c720e8f60c314f76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal1639b6e2584ba9d3180546983ded329d012e803ce3e12b58c720e8f60c314f76->enter($internal1639b6e2584ba9d3180546983ded329d012e803ce3e12b58c720e8f60c314f76prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendergroups"));

            // line 4
            echo "    <div class=\"row\">
        ";
            // line 5
            $this->displayBlock("fieldrow", $context, $blocks);
            echo "
    </div>
";
            
            $internal1639b6e2584ba9d3180546983ded329d012e803ce3e12b58c720e8f60c314f76->leave($internal1639b6e2584ba9d3180546983ded329d012e803ce3e12b58c720e8f60c314f76prof);

            
            $internal717512a73acf1e5ed9c3e06078a526192d69587b13059b23d2b19f0ef9402946->leave($internal717512a73acf1e5ed9c3e06078a526192d69587b13059b23d2b19f0ef9402946prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baseshowmacro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 5,  234 => 4,  211 => 3,  200 => 31,  194 => 29,  192 => 28,  189 => 27,  180 => 26,  167 => 18,  158 => 17,  131 => 34,  117 => 33,  114 => 26,  97 => 25,  90 => 20,  88 => 17,  82 => 14,  76 => 13,  73 => 12,  70 => 11,  52 => 10,  34 => 9,  31 => 8,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{# NEXTMAJOR: remove this template #}

{% macro rendergroups(admin, object, elements, groups, hastab, nopadding = false) %}
    <div class=\"row\">
        {{ block('fieldrow') }}
    </div>
{% endmacro %}

{% block fieldrow %}
    {% for code in groups %}
        {% set showgroup = admin.showgroups[code] %}

        <div class=\"{{ showgroup.class|default('col-md-12') }} {{ nopadding ? 'nopadding' }}\">
            <div class=\"{{ showgroup.boxclass }}\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        {% block showtitle %}
                            {{ showgroup.label|trans({}, showgroup.translationdomain ?: admin.translationDomain) }}
                        {% endblock %}
                    </h4>
                </div>
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\">
                        <tbody>
                        {% for fieldname in showgroup.fields %}
                            {% block showfield %}
                                <tr class=\"sonata-ba-view-container\">
                                    {% if elements[fieldname] is defined %}
                                        {{ elements[fieldname]|renderviewelement(object)}}
                                    {% endif %}
                                </tr>
                            {% endblock %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    {% endfor %}
{% endblock %}
", "SonataAdminBundle:CRUD:baseshowmacro.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baseshowmacro.html.twig");
    }
}
