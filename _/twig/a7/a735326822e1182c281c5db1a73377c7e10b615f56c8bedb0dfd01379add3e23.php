<?php

/* SonataAdminBundle:CRUD:showcurrency.html.twig */
class TwigTemplateba395e14034a50da6982c2bc2cfb2a8c49e36968ec73424676557d2540bb3ed3 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showcurrency.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2456ab47c75e52de7ae1abf977929af62dbc143fd19d29b1023e19c63642e5b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2456ab47c75e52de7ae1abf977929af62dbc143fd19d29b1023e19c63642e5b6->enter($internal2456ab47c75e52de7ae1abf977929af62dbc143fd19d29b1023e19c63642e5b6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showcurrency.html.twig"));

        $internalf81b6ba8399626f757d71b4bba41ff770b3c46bed59bc1bc9933d060b43bad7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf81b6ba8399626f757d71b4bba41ff770b3c46bed59bc1bc9933d060b43bad7b->enter($internalf81b6ba8399626f757d71b4bba41ff770b3c46bed59bc1bc9933d060b43bad7bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showcurrency.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal2456ab47c75e52de7ae1abf977929af62dbc143fd19d29b1023e19c63642e5b6->leave($internal2456ab47c75e52de7ae1abf977929af62dbc143fd19d29b1023e19c63642e5b6prof);

        
        $internalf81b6ba8399626f757d71b4bba41ff770b3c46bed59bc1bc9933d060b43bad7b->leave($internalf81b6ba8399626f757d71b4bba41ff770b3c46bed59bc1bc9933d060b43bad7bprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal07d800f1dde9ca1b0b1b4cd3dcf565c824796947a49dba522859ae839084c693 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal07d800f1dde9ca1b0b1b4cd3dcf565c824796947a49dba522859ae839084c693->enter($internal07d800f1dde9ca1b0b1b4cd3dcf565c824796947a49dba522859ae839084c693prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internalb37eee1c4520c4915c5ab88601aa910f5810272104af3e3f871dfdfc00fecdf7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb37eee1c4520c4915c5ab88601aa910f5810272104af3e3f871dfdfc00fecdf7->enter($internalb37eee1c4520c4915c5ab88601aa910f5810272104af3e3f871dfdfc00fecdf7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "        ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $internalb37eee1c4520c4915c5ab88601aa910f5810272104af3e3f871dfdfc00fecdf7->leave($internalb37eee1c4520c4915c5ab88601aa910f5810272104af3e3f871dfdfc00fecdf7prof);

        
        $internal07d800f1dde9ca1b0b1b4cd3dcf565c824796947a49dba522859ae839084c693->leave($internal07d800f1dde9ca1b0b1b4cd3dcf565c824796947a49dba522859ae839084c693prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showcurrency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% if value is not null %}
        {{ fielddescription.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:showcurrency.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showcurrency.html.twig");
    }
}
