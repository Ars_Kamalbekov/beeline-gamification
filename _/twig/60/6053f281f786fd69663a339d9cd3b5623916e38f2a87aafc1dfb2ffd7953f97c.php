<?php

/* WebProfilerBundle:Profiler:open.css.twig */
class TwigTemplate4508741e730d4a580158354a308b57098725a6f52d3e5afb92b4911a5e7a6d48 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale98de189bf16ca5bddbf12475a9477705c927bff92fdc9bfd6f6bbc826f588f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale98de189bf16ca5bddbf12475a9477705c927bff92fdc9bfd6f6bbc826f588f8->enter($internale98de189bf16ca5bddbf12475a9477705c927bff92fdc9bfd6f6bbc826f588f8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.css.twig"));

        $internal6a8a4af791d92324165e50860a78a9133344f459cbd6b533d0729e58bff5cac1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6a8a4af791d92324165e50860a78a9133344f459cbd6b533d0729e58bff5cac1->enter($internal6a8a4af791d92324165e50860a78a9133344f459cbd6b533d0729e58bff5cac1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.css.twig"));

        // line 3
        $context["mixins"] = array("breaklongwords" => "-ms-word-break: break-all; word-break: break-all; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto;", "monospacefont" => "font-family: monospace; font-size: 13px; font-size-adjust: 0.5;", "sansseriffont" => "font-family: Helvetica, Arial, sans-serif;", "subtleborderandshadow" => "background: #FFF; border: 1px solid #E0E0E0; box-shadow: 0px 0px 1px rgba(128, 128, 128, .2);");
        // line 9
        echo "
";
        // line 13
        echo "html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{margin:.67em 0;font-size:2em}mark{color:#000;background:#ff0}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{height:0;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{margin:0;font:inherit;color:inherit}button{overflow:visible}button,select{text-transform:none}button,html input[type=\"button\"],input[type=\"reset\"],input[type=\"submit\"]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{padding:0;border:0}input{line-height:normal}input[type=\"checkbox\"],input[type=\"radio\"]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}input[type=\"number\"]::-webkit-inner-spin-button,input[type=\"number\"]::-webkit-outer-spin-button{height:auto}input[type=\"search\"]{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;-webkit-appearance:textfield}input[type=\"search\"]::-webkit-search-cancel-button,input[type=\"search\"]::-webkit-search-decoration{-webkit-appearance:none}fieldset{padding:.35em .625em .75em;margin:0 2px;border:1px solid silver}legend{padding:0;border:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-spacing:0;border-collapse:collapse}td,th{padding:0}

";
        // line 17
        echo "html, body  {
    height: 100%;
    width: 100%;
}
body {
    background-color: #F9F9F9;
    color: #aaa;
    display: flex;
    flex-direction: column;
    ";
        // line 26
        echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["mixins"]) || arraykeyexists("mixins", $context) ? $context["mixins"] : (function () { throw new TwigErrorRuntime('Variable "mixins" does not exist.', 26, $this->getSourceContext()); })()), "sansseriffont", array());
        echo "
    font-size: 14px;
    line-height: 1.4;
}
.header {
    background-color: #222;
    position: fixed;
    top: 0;
    display: flex;
    width: 100%;
}
.header h1 {
    color: #FFF;
    font-weight: normal;
    font-size: 21px;
    margin: 0;
    padding: 10px 10px 8px;
    word-break: break-all;
}

a.doc {
    color: #FFF;
    text-decoration: none;
    margin: auto;
    margin-right: 10px;
}

a.doc:hover {
    text-decoration: underline;
}

.source {
    margin-top: 41px;
}

.source li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.anchor {
    position: relative;
    display: block;
    top: -7em;
    visibility: hidden;
}
";
        
        $internale98de189bf16ca5bddbf12475a9477705c927bff92fdc9bfd6f6bbc826f588f8->leave($internale98de189bf16ca5bddbf12475a9477705c927bff92fdc9bfd6f6bbc826f588f8prof);

        
        $internal6a8a4af791d92324165e50860a78a9133344f459cbd6b533d0729e58bff5cac1->leave($internal6a8a4af791d92324165e50860a78a9133344f459cbd6b533d0729e58bff5cac1prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 26,  34 => 17,  30 => 13,  27 => 9,  25 => 3,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{# Mixins
   ========================================================================= #}
{% set mixins = {
    'breaklongwords': '-ms-word-break: break-all; word-break: break-all; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto;',
    'monospacefont': 'font-family: monospace; font-size: 13px; font-size-adjust: 0.5;',
    'sansseriffont': 'font-family: Helvetica, Arial, sans-serif;',
    'subtleborderandshadow': 'background: #FFF; border: 1px solid #E0E0E0; box-shadow: 0px 0px 1px rgba(128, 128, 128, .2);'
} %}

{# Normalization
   (normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css)
   ========================================================================= #}
html{font-family:sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{margin:.67em 0;font-size:2em}mark{color:#000;background:#ff0}small{font-size:80%}sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{height:0;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{margin:0;font:inherit;color:inherit}button{overflow:visible}button,select{text-transform:none}button,html input[type=\"button\"],input[type=\"reset\"],input[type=\"submit\"]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{padding:0;border:0}input{line-height:normal}input[type=\"checkbox\"],input[type=\"radio\"]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}input[type=\"number\"]::-webkit-inner-spin-button,input[type=\"number\"]::-webkit-outer-spin-button{height:auto}input[type=\"search\"]{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;-webkit-appearance:textfield}input[type=\"search\"]::-webkit-search-cancel-button,input[type=\"search\"]::-webkit-search-decoration{-webkit-appearance:none}fieldset{padding:.35em .625em .75em;margin:0 2px;border:1px solid silver}legend{padding:0;border:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-spacing:0;border-collapse:collapse}td,th{padding:0}

{# Basic styles
   ========================================================================= #}
html, body  {
    height: 100%;
    width: 100%;
}
body {
    background-color: #F9F9F9;
    color: #aaa;
    display: flex;
    flex-direction: column;
    {{ mixins.sansseriffont|raw }}
    font-size: 14px;
    line-height: 1.4;
}
.header {
    background-color: #222;
    position: fixed;
    top: 0;
    display: flex;
    width: 100%;
}
.header h1 {
    color: #FFF;
    font-weight: normal;
    font-size: 21px;
    margin: 0;
    padding: 10px 10px 8px;
    word-break: break-all;
}

a.doc {
    color: #FFF;
    text-decoration: none;
    margin: auto;
    margin-right: 10px;
}

a.doc:hover {
    text-decoration: underline;
}

.source {
    margin-top: 41px;
}

.source li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.anchor {
    position: relative;
    display: block;
    top: -7em;
    visibility: hidden;
}
", "WebProfilerBundle:Profiler:open.css.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.css.twig");
    }
}
