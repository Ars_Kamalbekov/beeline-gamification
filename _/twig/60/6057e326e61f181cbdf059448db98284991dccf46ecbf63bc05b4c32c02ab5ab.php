<?php

/* SonataAdminBundle:CRUD:baselist.html.twig */
class TwigTemplated40def085e9974813516ddd51111e71a464ed7326eb4b66af9c352fe2f886fd8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'tabmenu' => array($this, 'blocktabmenu'),
            'title' => array($this, 'blocktitle'),
            'navbartitle' => array($this, 'blocknavbartitle'),
            'listtable' => array($this, 'blocklisttable'),
            'listheader' => array($this, 'blocklistheader'),
            'tableheader' => array($this, 'blocktableheader'),
            'tablebody' => array($this, 'blocktablebody'),
            'tablefooter' => array($this, 'blocktablefooter'),
            'noresultcontent' => array($this, 'blocknoresultcontent'),
            'listfooter' => array($this, 'blocklistfooter'),
            'batch' => array($this, 'blockbatch'),
            'batchjavascript' => array($this, 'blockbatchjavascript'),
            'batchactions' => array($this, 'blockbatchactions'),
            'pagerresults' => array($this, 'blockpagerresults'),
            'pagerlinks' => array($this, 'blockpagerlinks'),
            'listfiltersactions' => array($this, 'blocklistfiltersactions'),
            'listfilters' => array($this, 'blocklistfilters'),
            'sonatalistfiltergroupclass' => array($this, 'blocksonatalistfiltergroupclass'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:baselist.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbf8026dab9dce5852fa6646e42e12f09099aa0d3e171b957b6f9f761dd55537a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbf8026dab9dce5852fa6646e42e12f09099aa0d3e171b957b6f9f761dd55537a->enter($internalbf8026dab9dce5852fa6646e42e12f09099aa0d3e171b957b6f9f761dd55537aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselist.html.twig"));

        $internalc4290571b27bb464d0f76b54ec4d3866ef4744cc2ad36f5e9996d4368d39b14c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc4290571b27bb464d0f76b54ec4d3866ef4744cc2ad36f5e9996d4368d39b14c->enter($internalc4290571b27bb464d0f76b54ec4d3866ef4744cc2ad36f5e9996d4368d39b14cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselist.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalbf8026dab9dce5852fa6646e42e12f09099aa0d3e171b957b6f9f761dd55537a->leave($internalbf8026dab9dce5852fa6646e42e12f09099aa0d3e171b957b6f9f761dd55537aprof);

        
        $internalc4290571b27bb464d0f76b54ec4d3866ef4744cc2ad36f5e9996d4368d39b14c->leave($internalc4290571b27bb464d0f76b54ec4d3866ef4744cc2ad36f5e9996d4368d39b14cprof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internal14d10cd628645066c474cce7e060c2bf08419eeed3066457a222aa79889fd4df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal14d10cd628645066c474cce7e060c2bf08419eeed3066457a222aa79889fd4df->enter($internal14d10cd628645066c474cce7e060c2bf08419eeed3066457a222aa79889fd4dfprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal4b84c9f9254913eb04d98927f04821f413afc9b317a3f9e73805503e78d13413 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4b84c9f9254913eb04d98927f04821f413afc9b317a3f9e73805503e78d13413->enter($internal4b84c9f9254913eb04d98927f04821f413afc9b317a3f9e73805503e78d13413prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:baselist.html.twig", 15)->display($context);
        
        $internal4b84c9f9254913eb04d98927f04821f413afc9b317a3f9e73805503e78d13413->leave($internal4b84c9f9254913eb04d98927f04821f413afc9b317a3f9e73805503e78d13413prof);

        
        $internal14d10cd628645066c474cce7e060c2bf08419eeed3066457a222aa79889fd4df->leave($internal14d10cd628645066c474cce7e060c2bf08419eeed3066457a222aa79889fd4dfprof);

    }

    // line 18
    public function blocktabmenu($context, array $blocks = array())
    {
        $internal1eb107f2585ea0cd90af88437bc2a3da4b4f7a5575e08521e6a58d2ec6829cdf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1eb107f2585ea0cd90af88437bc2a3da4b4f7a5575e08521e6a58d2ec6829cdf->enter($internal1eb107f2585ea0cd90af88437bc2a3da4b4f7a5575e08521e6a58d2ec6829cdfprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal815c6b11bbce317318552311aa99f6c0638108e3796d951316d90f837476ffe1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal815c6b11bbce317318552311aa99f6c0638108e3796d951316d90f837476ffe1->enter($internal815c6b11bbce317318552311aa99f6c0638108e3796d951316d90f837476ffe1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 18, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 18, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
        
        $internal815c6b11bbce317318552311aa99f6c0638108e3796d951316d90f837476ffe1->leave($internal815c6b11bbce317318552311aa99f6c0638108e3796d951316d90f837476ffe1prof);

        
        $internal1eb107f2585ea0cd90af88437bc2a3da4b4f7a5575e08521e6a58d2ec6829cdf->leave($internal1eb107f2585ea0cd90af88437bc2a3da4b4f7a5575e08521e6a58d2ec6829cdfprof);

    }

    // line 20
    public function blocktitle($context, array $blocks = array())
    {
        $internale5cdeaab6c139dd0fd72a666fc5cad9fa90d24f5036524dd1851f2ac26869611 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale5cdeaab6c139dd0fd72a666fc5cad9fa90d24f5036524dd1851f2ac26869611->enter($internale5cdeaab6c139dd0fd72a666fc5cad9fa90d24f5036524dd1851f2ac26869611prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internale90e50005eea2e0a9ccde33b616d05163be973a2a1805dcb04af1cc1dc93789f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale90e50005eea2e0a9ccde33b616d05163be973a2a1805dcb04af1cc1dc93789f->enter($internale90e50005eea2e0a9ccde33b616d05163be973a2a1805dcb04af1cc1dc93789fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        // line 21
        echo "    ";
        // line 25
        echo "
    ";
        // line 26
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "isChild", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 26, $this->getSourceContext()); })()), "parent", array()), "subject", array()))) {
            // line 27
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titleedit", array("%name%" => twigtruncatefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 27, $this->getSourceContext()); })()), "parent", array()), "toString", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 27, $this->getSourceContext()); })()), "parent", array()), "subject", array())), "method"), 15)), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        
        $internale90e50005eea2e0a9ccde33b616d05163be973a2a1805dcb04af1cc1dc93789f->leave($internale90e50005eea2e0a9ccde33b616d05163be973a2a1805dcb04af1cc1dc93789fprof);

        
        $internale5cdeaab6c139dd0fd72a666fc5cad9fa90d24f5036524dd1851f2ac26869611->leave($internale5cdeaab6c139dd0fd72a666fc5cad9fa90d24f5036524dd1851f2ac26869611prof);

    }

    // line 31
    public function blocknavbartitle($context, array $blocks = array())
    {
        $internal379b7b5cb92d20f7c5fd43fb3f2a077422ccdadd1f06208c71aad28588d8cc1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal379b7b5cb92d20f7c5fd43fb3f2a077422ccdadd1f06208c71aad28588d8cc1a->enter($internal379b7b5cb92d20f7c5fd43fb3f2a077422ccdadd1f06208c71aad28588d8cc1aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        $internal8467964cdd87bf19c03f555fb2746ff2b058b10634471b317141e440c56162d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8467964cdd87bf19c03f555fb2746ff2b058b10634471b317141e440c56162d3->enter($internal8467964cdd87bf19c03f555fb2746ff2b058b10634471b317141e440c56162d3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "navbartitle"));

        // line 32
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $internal8467964cdd87bf19c03f555fb2746ff2b058b10634471b317141e440c56162d3->leave($internal8467964cdd87bf19c03f555fb2746ff2b058b10634471b317141e440c56162d3prof);

        
        $internal379b7b5cb92d20f7c5fd43fb3f2a077422ccdadd1f06208c71aad28588d8cc1a->leave($internal379b7b5cb92d20f7c5fd43fb3f2a077422ccdadd1f06208c71aad28588d8cc1aprof);

    }

    // line 35
    public function blocklisttable($context, array $blocks = array())
    {
        $internal7ab767c4581677da97830ba361e7607147ace66277ba0cd7008682bb1e644ec5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7ab767c4581677da97830ba361e7607147ace66277ba0cd7008682bb1e644ec5->enter($internal7ab767c4581677da97830ba361e7607147ace66277ba0cd7008682bb1e644ec5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listtable"));

        $internal52ae5f4318f6bd64e2bb93955f9826afd9a1e6e5f6fcf37580e576d6a6c4bb76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal52ae5f4318f6bd64e2bb93955f9826afd9a1e6e5f6fcf37580e576d6a6c4bb76->enter($internal52ae5f4318f6bd64e2bb93955f9826afd9a1e6e5f6fcf37580e576d6a6c4bb76prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listtable"));

        // line 36
        echo "    <div class=\"col-xs-12 col-md-12\">
        ";
        // line 37
        $context["batchactions"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 37, $this->getSourceContext()); })()), "batchactions", array());
        // line 38
        echo "        ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "hasRoute", array(0 => "batch"), "method") && twiglengthfilter($this->env, (isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 38, $this->getSourceContext()); })())))) {
            // line 39
            echo "            <form action=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 39, $this->getSourceContext()); })()), "generateUrl", array(0 => "batch", 1 => array("filter" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 39, $this->getSourceContext()); })()), "filterParameters", array()))), "method"), "html", null, true);
            echo "\" method=\"POST\" >
            <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"";
            // line 40
            echo twigescapefilter($this->env, (isset($context["csrftoken"]) || arraykeyexists("csrftoken", $context) ? $context["csrftoken"] : (function () { throw new TwigErrorRuntime('Variable "csrftoken" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
        ";
        }
        // line 42
        echo "
        ";
        // line 44
        echo "        <div class=\"box box-primary\" ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 44, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "lastPage", array()) == 1)) {
            echo "style=\"margin-bottom: 100px;\"";
        }
        echo ">
            <div class=\"box-body ";
        // line 45
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 45, $this->getSourceContext()); })()), "datagrid", array()), "results", array())) > 0)) {
            echo "table-responsive no-padding";
        }
        echo "\">
                ";
        // line 46
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.list.table.top", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 46, $this->getSourceContext()); })()))));
        echo "

                ";
        // line 48
        $this->displayBlock('listheader', $context, $blocks);
        // line 49
        echo "
                ";
        // line 50
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 50, $this->getSourceContext()); })()), "datagrid", array()), "results", array())) > 0)) {
            // line 51
            echo "                    <table class=\"table table-bordered table-striped sonata-ba-list\">
                        ";
            // line 52
            $this->displayBlock('tableheader', $context, $blocks);
            // line 91
            echo "
                        ";
            // line 92
            $this->displayBlock('tablebody', $context, $blocks);
            // line 97
            echo "
                        ";
            // line 98
            $this->displayBlock('tablefooter', $context, $blocks);
            // line 100
            echo "                    </table>
                ";
        } else {
            // line 102
            echo "                    ";
            $this->displayBlock('noresultcontent', $context, $blocks);
            // line 120
            echo "                ";
        }
        // line 121
        echo "
                ";
        // line 122
        echo calluserfuncarray($this->env->getFunction('sonatablockrenderevent')->getCallable(), array("sonata.admin.list.table.bottom", array("admin" => (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 122, $this->getSourceContext()); })()))));
        echo "
            </div>
            ";
        // line 124
        $this->displayBlock('listfooter', $context, $blocks);
        // line 224
        echo "        </div>
        ";
        // line 225
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 225, $this->getSourceContext()); })()), "hasRoute", array(0 => "batch"), "method") && twiglengthfilter($this->env, (isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 225, $this->getSourceContext()); })())))) {
            // line 226
            echo "            </form>
        ";
        }
        // line 228
        echo "    </div>
";
        
        $internal52ae5f4318f6bd64e2bb93955f9826afd9a1e6e5f6fcf37580e576d6a6c4bb76->leave($internal52ae5f4318f6bd64e2bb93955f9826afd9a1e6e5f6fcf37580e576d6a6c4bb76prof);

        
        $internal7ab767c4581677da97830ba361e7607147ace66277ba0cd7008682bb1e644ec5->leave($internal7ab767c4581677da97830ba361e7607147ace66277ba0cd7008682bb1e644ec5prof);

    }

    // line 48
    public function blocklistheader($context, array $blocks = array())
    {
        $internaldb68d2905ed8141016734f19536db21536ccc8805a0ffaca412b2138e456310b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldb68d2905ed8141016734f19536db21536ccc8805a0ffaca412b2138e456310b->enter($internaldb68d2905ed8141016734f19536db21536ccc8805a0ffaca412b2138e456310bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listheader"));

        $internalbd923041892da033f8677df57f681ec58be913b49ff8005a4c29c9bf9f9c20d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbd923041892da033f8677df57f681ec58be913b49ff8005a4c29c9bf9f9c20d6->enter($internalbd923041892da033f8677df57f681ec58be913b49ff8005a4c29c9bf9f9c20d6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listheader"));

        
        $internalbd923041892da033f8677df57f681ec58be913b49ff8005a4c29c9bf9f9c20d6->leave($internalbd923041892da033f8677df57f681ec58be913b49ff8005a4c29c9bf9f9c20d6prof);

        
        $internaldb68d2905ed8141016734f19536db21536ccc8805a0ffaca412b2138e456310b->leave($internaldb68d2905ed8141016734f19536db21536ccc8805a0ffaca412b2138e456310bprof);

    }

    // line 52
    public function blocktableheader($context, array $blocks = array())
    {
        $internalf22309178f1b62b54d67d46b08d62d0ffb08cc3e92aaf640d5f9717cb28af609 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf22309178f1b62b54d67d46b08d62d0ffb08cc3e92aaf640d5f9717cb28af609->enter($internalf22309178f1b62b54d67d46b08d62d0ffb08cc3e92aaf640d5f9717cb28af609prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tableheader"));

        $internal95119c4bf974e0eaa5e3b696fc0cbec5cf011f0acb90c901701125cab51b0748 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal95119c4bf974e0eaa5e3b696fc0cbec5cf011f0acb90c901701125cab51b0748->enter($internal95119c4bf974e0eaa5e3b696fc0cbec5cf011f0acb90c901701125cab51b0748prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tableheader"));

        // line 53
        echo "                            <thead>
                                <tr class=\"sonata-ba-list-field-header\">
                                    ";
        // line 55
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 55, $this->getSourceContext()); })()), "list", array()), "elements", array()));
        foreach ($context['seq'] as $context["key"] => $context["fielddescription"]) {
            // line 56
            echo "                                        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 56, $this->getSourceContext()); })()), "hasRoute", array(0 => "batch"), "method") && (twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "getOption", array(0 => "code"), "method") == "batch")) && (twiglengthfilter($this->env, (isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 56, $this->getSourceContext()); })())) > 0))) {
                // line 57
                echo "                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-batch\">
                                              <input type=\"checkbox\" id=\"listbatchcheckbox\">
                                            </th>
                                        ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(),             // line 60
$context["fielddescription"], "getOption", array(0 => "code"), "method") == "select")) {
                // line 61
                echo "                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-select\"></th>
                                        ";
            } elseif (((twiggetattribute($this->env, $this->getSourceContext(),             // line 62
$context["fielddescription"], "name", array()) == "action") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 62, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
                // line 63
                echo "                                            ";
                // line 64
                echo "                                        ";
            } elseif (((twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "getOption", array(0 => "ajaxhidden"), "method") == true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 64, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
                // line 65
                echo "                                            ";
                // line 66
                echo "                                        ";
            } else {
                // line 67
                echo "                                            ";
                $context["sortable"] = false;
                // line 68
                echo "                                            ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array(), "any", false, true), "sortable", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array()), "sortable", array()))) {
                    // line 69
                    echo "                                                ";
                    $context["sortable"] = true;
                    // line 70
                    echo "                                                ";
                    $context["sortparameters"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 70, $this->getSourceContext()); })()), "modelmanager", array()), "sortparameters", array(0 => $context["fielddescription"], 1 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 70, $this->getSourceContext()); })()), "datagrid", array())), "method");
                    // line 71
                    echo "                                                ";
                    $context["current"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 71, $this->getSourceContext()); })()), "datagrid", array()), "values", array()), "sortby", array()) == $context["fielddescription"]) || (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 71, $this->getSourceContext()); })()), "datagrid", array()), "values", array()), "sortby", array()), "fieldName", array()) == twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sortparameters"]) || arraykeyexists("sortparameters", $context) ? $context["sortparameters"] : (function () { throw new TwigErrorRuntime('Variable "sortparameters" does not exist.', 71, $this->getSourceContext()); })()), "filter", array()), "sortby", array())));
                    // line 72
                    echo "                                                ";
                    $context["sortactiveclass"] = (((isset($context["current"]) || arraykeyexists("current", $context) ? $context["current"] : (function () { throw new TwigErrorRuntime('Variable "current" does not exist.', 72, $this->getSourceContext()); })())) ? ("sonata-ba-list-field-order-active") : (""));
                    // line 73
                    echo "                                                ";
                    $context["sortby"] = (((isset($context["current"]) || arraykeyexists("current", $context) ? $context["current"] : (function () { throw new TwigErrorRuntime('Variable "current" does not exist.', 73, $this->getSourceContext()); })())) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 73, $this->getSourceContext()); })()), "datagrid", array()), "values", array()), "sortorder", array())) : (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array()), "sortorder", array())));
                    // line 74
                    echo "                                            ";
                }
                // line 75
                echo "
                                            ";
                // line 76
                obstart();
                // line 77
                echo "                                                <th class=\"sonata-ba-list-field-header-";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "type", array()), "html", null, true);
                echo " ";
                if ((isset($context["sortable"]) || arraykeyexists("sortable", $context) ? $context["sortable"] : (function () { throw new TwigErrorRuntime('Variable "sortable" does not exist.', 77, $this->getSourceContext()); })())) {
                    echo " sonata-ba-list-field-header-order-";
                    echo twigescapefilter($this->env, twiglowerfilter($this->env, (isset($context["sortby"]) || arraykeyexists("sortby", $context) ? $context["sortby"] : (function () { throw new TwigErrorRuntime('Variable "sortby" does not exist.', 77, $this->getSourceContext()); })())), "html", null, true);
                    echo " ";
                    echo twigescapefilter($this->env, (isset($context["sortactiveclass"]) || arraykeyexists("sortactiveclass", $context) ? $context["sortactiveclass"] : (function () { throw new TwigErrorRuntime('Variable "sortactiveclass" does not exist.', 77, $this->getSourceContext()); })()), "html", null, true);
                }
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array(), "any", false, true), "headerclass", array(), "any", true, true)) {
                    echo " ";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array()), "headerclass", array()), "html", null, true);
                }
                echo "\"";
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array(), "any", false, true), "headerstyle", array(), "any", true, true)) {
                    echo " style=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "options", array()), "headerstyle", array()), "html", null, true);
                    echo "\"";
                }
                echo ">
                                                    ";
                // line 78
                if ((isset($context["sortable"]) || arraykeyexists("sortable", $context) ? $context["sortable"] : (function () { throw new TwigErrorRuntime('Variable "sortable" does not exist.', 78, $this->getSourceContext()); })())) {
                    echo "<a href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 78, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => (isset($context["sortparameters"]) || arraykeyexists("sortparameters", $context) ? $context["sortparameters"] : (function () { throw new TwigErrorRuntime('Variable "sortparameters" does not exist.', 78, $this->getSourceContext()); })())), "method"), "html", null, true);
                    echo "\">";
                }
                // line 79
                echo "                                                    ";
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "getOption", array(0 => "labelicon"), "method")) {
                    // line 80
                    echo "                                                        <i class=\"sonata-ba-list-field-header-label-icon ";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "getOption", array(0 => "labelicon"), "method"), "html", null, true);
                    echo "\" aria-hidden=\"true\"></i>
                                                    ";
                }
                // line 82
                echo "                                                    ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "label", array()), array(), twiggetattribute($this->env, $this->getSourceContext(), $context["fielddescription"], "translationDomain", array()));
                echo "
                                                    ";
                // line 83
                if ((isset($context["sortable"]) || arraykeyexists("sortable", $context) ? $context["sortable"] : (function () { throw new TwigErrorRuntime('Variable "sortable" does not exist.', 83, $this->getSourceContext()); })())) {
                    echo "</a>";
                }
                // line 84
                echo "                                                </th>
                                            ";
                echo trim(pregreplace('/>\s+</', '><', obgetclean()));
                // line 86
                echo "                                        ";
            }
            // line 87
            echo "                                    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['fielddescription'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 88
        echo "                                </tr>
                            </thead>
                        ";
        
        $internal95119c4bf974e0eaa5e3b696fc0cbec5cf011f0acb90c901701125cab51b0748->leave($internal95119c4bf974e0eaa5e3b696fc0cbec5cf011f0acb90c901701125cab51b0748prof);

        
        $internalf22309178f1b62b54d67d46b08d62d0ffb08cc3e92aaf640d5f9717cb28af609->leave($internalf22309178f1b62b54d67d46b08d62d0ffb08cc3e92aaf640d5f9717cb28af609prof);

    }

    // line 92
    public function blocktablebody($context, array $blocks = array())
    {
        $internala81d64ef52adebff52f9214f765c8714981e3076ad87296cc5c8d54a7d9ba9c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala81d64ef52adebff52f9214f765c8714981e3076ad87296cc5c8d54a7d9ba9c1->enter($internala81d64ef52adebff52f9214f765c8714981e3076ad87296cc5c8d54a7d9ba9c1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablebody"));

        $internal13bc15b7d79312ff52080cd9ef1e67c8cad514f75dec60af1f0b56abf01da41f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal13bc15b7d79312ff52080cd9ef1e67c8cad514f75dec60af1f0b56abf01da41f->enter($internal13bc15b7d79312ff52080cd9ef1e67c8cad514f75dec60af1f0b56abf01da41fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablebody"));

        // line 93
        echo "                            <tbody>
                                ";
        // line 94
        $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 94, $this->getSourceContext()); })()), "getTemplate", array(0 => ("outerlistrows" . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 94, $this->getSourceContext()); })()), "getListMode", array(), "method"))), "method"), "SonataAdminBundle:CRUD:baselist.html.twig", 94)->display($context);
        // line 95
        echo "                            </tbody>
                        ";
        
        $internal13bc15b7d79312ff52080cd9ef1e67c8cad514f75dec60af1f0b56abf01da41f->leave($internal13bc15b7d79312ff52080cd9ef1e67c8cad514f75dec60af1f0b56abf01da41fprof);

        
        $internala81d64ef52adebff52f9214f765c8714981e3076ad87296cc5c8d54a7d9ba9c1->leave($internala81d64ef52adebff52f9214f765c8714981e3076ad87296cc5c8d54a7d9ba9c1prof);

    }

    // line 98
    public function blocktablefooter($context, array $blocks = array())
    {
        $internalc2745c5f7756e32f9b8844db7726eeba0f60e240a28325e946e6412a48ce0ad6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc2745c5f7756e32f9b8844db7726eeba0f60e240a28325e946e6412a48ce0ad6->enter($internalc2745c5f7756e32f9b8844db7726eeba0f60e240a28325e946e6412a48ce0ad6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablefooter"));

        $internalfec889fd36c52a0d232bd380bde4805d81edfe3382073d49239ef781a8977ee5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfec889fd36c52a0d232bd380bde4805d81edfe3382073d49239ef781a8977ee5->enter($internalfec889fd36c52a0d232bd380bde4805d81edfe3382073d49239ef781a8977ee5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "tablefooter"));

        // line 99
        echo "                        ";
        
        $internalfec889fd36c52a0d232bd380bde4805d81edfe3382073d49239ef781a8977ee5->leave($internalfec889fd36c52a0d232bd380bde4805d81edfe3382073d49239ef781a8977ee5prof);

        
        $internalc2745c5f7756e32f9b8844db7726eeba0f60e240a28325e946e6412a48ce0ad6->leave($internalc2745c5f7756e32f9b8844db7726eeba0f60e240a28325e946e6412a48ce0ad6prof);

    }

    // line 102
    public function blocknoresultcontent($context, array $blocks = array())
    {
        $internal9eb5179d4f8b1c9a03536e3bb1f67eeef2a8da7b8fc03a9e72ac2dbdeb5c835a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9eb5179d4f8b1c9a03536e3bb1f67eeef2a8da7b8fc03a9e72ac2dbdeb5c835a->enter($internal9eb5179d4f8b1c9a03536e3bb1f67eeef2a8da7b8fc03a9e72ac2dbdeb5c835aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "noresultcontent"));

        $internald968207a55ca333c8ba3f7223590a3007c8c1295c857f72b37d07e6bd28ad0b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald968207a55ca333c8ba3f7223590a3007c8c1295c857f72b37d07e6bd28ad0b2->enter($internald968207a55ca333c8ba3f7223590a3007c8c1295c857f72b37d07e6bd28ad0b2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "noresultcontent"));

        // line 103
        echo "                        <div class=\"info-box\">
                            <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i></span>
                            <div class=\"info-box-content\">
                                <span class=\"info-box-text\">";
        // line 106
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("noresult", array(), "SonataAdminBundle"), "html", null, true);
        echo "</span>
                                <div class=\"progress\">
                                    <div class=\"progress-bar\" style=\"width: 0%\"></div>
                                </div>
                                <span class=\"progress-description\">
                                    ";
        // line 111
        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 111, $this->getSourceContext()); })()), "request", array()), "xmlHttpRequest", array())) {
            // line 112
            echo "                                    <ul class=\"list-unstyled\">
                                        ";
            // line 113
            $this->loadTemplate("SonataAdminBundle:Button:createbutton.html.twig", "SonataAdminBundle:CRUD:baselist.html.twig", 113)->display($context);
            // line 114
            echo "                                    </ul>
                                    ";
        }
        // line 116
        echo "                                </span>
                            </div><!-- /.info-box-content -->
                        </div>
                    ";
        
        $internald968207a55ca333c8ba3f7223590a3007c8c1295c857f72b37d07e6bd28ad0b2->leave($internald968207a55ca333c8ba3f7223590a3007c8c1295c857f72b37d07e6bd28ad0b2prof);

        
        $internal9eb5179d4f8b1c9a03536e3bb1f67eeef2a8da7b8fc03a9e72ac2dbdeb5c835a->leave($internal9eb5179d4f8b1c9a03536e3bb1f67eeef2a8da7b8fc03a9e72ac2dbdeb5c835aprof);

    }

    // line 124
    public function blocklistfooter($context, array $blocks = array())
    {
        $internal6d3f46bf7ab8df8c0f8c5b143f2fc23e4c4d246bdae39fc00ec8bd8dc32cc5bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6d3f46bf7ab8df8c0f8c5b143f2fc23e4c4d246bdae39fc00ec8bd8dc32cc5bb->enter($internal6d3f46bf7ab8df8c0f8c5b143f2fc23e4c4d246bdae39fc00ec8bd8dc32cc5bbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfooter"));

        $internaldd68fe650d93107d84eba3366b44a1f41d1f7834cdc546ed62ebd4b1beb816ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldd68fe650d93107d84eba3366b44a1f41d1f7834cdc546ed62ebd4b1beb816ed->enter($internaldd68fe650d93107d84eba3366b44a1f41d1f7834cdc546ed62ebd4b1beb816edprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfooter"));

        // line 125
        echo "                ";
        if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 125, $this->getSourceContext()); })()), "datagrid", array()), "results", array())) > 0)) {
            // line 126
            echo "                    <div class=\"box-footer\">
                        <div class=\"form-inline clearfix\">
                            ";
            // line 128
            if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 128, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array())) {
                // line 129
                echo "                                <div class=\"pull-left\">
                                    ";
                // line 130
                if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 130, $this->getSourceContext()); })()), "hasRoute", array(0 => "batch"), "method") && (twiglengthfilter($this->env, (isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 130, $this->getSourceContext()); })())) > 0))) {
                    // line 131
                    echo "                                        ";
                    $this->displayBlock('batch', $context, $blocks);
                    // line 178
                    echo "                                    ";
                }
                // line 179
                echo "                                </div>


                                ";
                // line 183
                echo "                                ";
                $context["exportformats"] = ((arraykeyexists("exportformats", $context)) ? (twigdefaultfilter((isset($context["exportformats"]) || arraykeyexists("exportformats", $context) ? $context["exportformats"] : (function () { throw new TwigErrorRuntime('Variable "exportformats" does not exist.', 183, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 183, $this->getSourceContext()); })()), "exportFormats", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 183, $this->getSourceContext()); })()), "exportFormats", array())));
                // line 184
                echo "
                                <div class=\"pull-right\">
                                    ";
                // line 186
                if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 186, $this->getSourceContext()); })()), "hasRoute", array(0 => "export"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 186, $this->getSourceContext()); })()), "hasAccess", array(0 => "export"), "method")) && twiglengthfilter($this->env, (isset($context["exportformats"]) || arraykeyexists("exportformats", $context) ? $context["exportformats"] : (function () { throw new TwigErrorRuntime('Variable "exportformats" does not exist.', 186, $this->getSourceContext()); })())))) {
                    // line 187
                    echo "                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                                <i class=\"fa fa-share-square-o\" aria-hidden=\"true\"></i>
                                                ";
                    // line 190
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labelexportdownload", array(), "SonataAdminBundle"), "html", null, true);
                    echo "
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\">
                                                ";
                    // line 194
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable((isset($context["exportformats"]) || arraykeyexists("exportformats", $context) ? $context["exportformats"] : (function () { throw new TwigErrorRuntime('Variable "exportformats" does not exist.', 194, $this->getSourceContext()); })()));
                    foreach ($context['seq'] as $context["key"] => $context["format"]) {
                        // line 195
                        echo "                                                <li>
                                                    <a href=\"";
                        // line 196
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 196, $this->getSourceContext()); })()), "generateUrl", array(0 => "export", 1 => (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 196, $this->getSourceContext()); })()), "modelmanager", array()), "paginationparameters", array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 196, $this->getSourceContext()); })()), "datagrid", array()), 1 => 0), "method") + array("format" => $context["format"]))), "method"), "html", null, true);
                        echo "\">
                                                        <i class=\"fa fa-arrow-circle-o-down\" aria-hidden=\"true\"></i>
                                                        ";
                        // line 198
                        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(("exportformat" . $context["format"]), array(), "SonataAdminBundle"), "html", null, true);
                        echo "
                                                    </a>
                                                <li>
                                                ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['format'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 202
                    echo "                                            </ul>
                                        </div>

                                        &nbsp;-&nbsp;
                                    ";
                }
                // line 207
                echo "
                                    ";
                // line 208
                $this->displayBlock('pagerresults', $context, $blocks);
                // line 211
                echo "                                </div>
                            ";
            }
            // line 213
            echo "                        </div>

                        ";
            // line 215
            $this->displayBlock('pagerlinks', $context, $blocks);
            // line 221
            echo "                    </div>
                ";
        }
        // line 223
        echo "            ";
        
        $internaldd68fe650d93107d84eba3366b44a1f41d1f7834cdc546ed62ebd4b1beb816ed->leave($internaldd68fe650d93107d84eba3366b44a1f41d1f7834cdc546ed62ebd4b1beb816edprof);

        
        $internal6d3f46bf7ab8df8c0f8c5b143f2fc23e4c4d246bdae39fc00ec8bd8dc32cc5bb->leave($internal6d3f46bf7ab8df8c0f8c5b143f2fc23e4c4d246bdae39fc00ec8bd8dc32cc5bbprof);

    }

    // line 131
    public function blockbatch($context, array $blocks = array())
    {
        $internal34586246ca437b5acce93ecf0db001d375727e3adcc6aca90989f4b8bc0b6a87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal34586246ca437b5acce93ecf0db001d375727e3adcc6aca90989f4b8bc0b6a87->enter($internal34586246ca437b5acce93ecf0db001d375727e3adcc6aca90989f4b8bc0b6a87prof = new TwigProfilerProfile($this->getTemplateName(), "block", "batch"));

        $internalebe443de21cde80831723005d65e40997a05a7d7d21cf657ed270d2a1caac4cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalebe443de21cde80831723005d65e40997a05a7d7d21cf657ed270d2a1caac4cd->enter($internalebe443de21cde80831723005d65e40997a05a7d7d21cf657ed270d2a1caac4cdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "batch"));

        // line 132
        echo "                                            <script>
                                                ";
        // line 133
        $this->displayBlock('batchjavascript', $context, $blocks);
        // line 160
        echo "                                            </script>

                                        ";
        // line 162
        $this->displayBlock('batchactions', $context, $blocks);
        // line 175
        echo "
                                            <input type=\"submit\" class=\"btn btn-small btn-primary\" value=\"";
        // line 176
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnbatch", array(), "SonataAdminBundle"), "html", null, true);
        echo "\">
                                        ";
        
        $internalebe443de21cde80831723005d65e40997a05a7d7d21cf657ed270d2a1caac4cd->leave($internalebe443de21cde80831723005d65e40997a05a7d7d21cf657ed270d2a1caac4cdprof);

        
        $internal34586246ca437b5acce93ecf0db001d375727e3adcc6aca90989f4b8bc0b6a87->leave($internal34586246ca437b5acce93ecf0db001d375727e3adcc6aca90989f4b8bc0b6a87prof);

    }

    // line 133
    public function blockbatchjavascript($context, array $blocks = array())
    {
        $internal3b6d63b1b9d412a168d65f64d404cfac58293b0bdcbfe3f3846b6dcdc8e96bcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3b6d63b1b9d412a168d65f64d404cfac58293b0bdcbfe3f3846b6dcdc8e96bcd->enter($internal3b6d63b1b9d412a168d65f64d404cfac58293b0bdcbfe3f3846b6dcdc8e96bcdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "batchjavascript"));

        $internal2f705820bd0a2810b26f14967c628bc325248087d0307c378615dd7cd29a9e81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2f705820bd0a2810b26f14967c628bc325248087d0307c378615dd7cd29a9e81->enter($internal2f705820bd0a2810b26f14967c628bc325248087d0307c378615dd7cd29a9e81prof = new TwigProfilerProfile($this->getTemplateName(), "block", "batchjavascript"));

        // line 134
        echo "                                                    jQuery(document).ready(function (\$) {
                                                        // Toggle individual checkboxes when the batch checkbox is changed
                                                        \$('#listbatchcheckbox').on('ifChanged change', function () {
                                                            var checkboxes = \$(this)
                                                                .closest('table')
                                                                .find('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            ;
                                                            if (window.SONATACONFIG.USEICHECK) {
                                                                checkboxes.iCheck(\$(this).is(':checked') ? 'check' : 'uncheck');
                                                            } else {
                                                                checkboxes.prop('checked', this.checked);
                                                            }
                                                        });

                                                        // Add a CSS class to rows when they are selected
                                                        \$('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            .on('ifChanged change', function () {
                                                                \$(this)
                                                                    .closest('tr, div.sonata-ba-list-field-batch')
                                                                    .toggleClass('sonata-ba-list-row-selected', \$(this).is(':checked'))
                                                                ;
                                                            })
                                                            .trigger('ifChanged')
                                                        ;
                                                    });
                                                ";
        
        $internal2f705820bd0a2810b26f14967c628bc325248087d0307c378615dd7cd29a9e81->leave($internal2f705820bd0a2810b26f14967c628bc325248087d0307c378615dd7cd29a9e81prof);

        
        $internal3b6d63b1b9d412a168d65f64d404cfac58293b0bdcbfe3f3846b6dcdc8e96bcd->leave($internal3b6d63b1b9d412a168d65f64d404cfac58293b0bdcbfe3f3846b6dcdc8e96bcdprof);

    }

    // line 162
    public function blockbatchactions($context, array $blocks = array())
    {
        $internal96b12b7505d782eb44f7ab6ebee3049a528df318b4d25b12a9bc4c57eff5da49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal96b12b7505d782eb44f7ab6ebee3049a528df318b4d25b12a9bc4c57eff5da49->enter($internal96b12b7505d782eb44f7ab6ebee3049a528df318b4d25b12a9bc4c57eff5da49prof = new TwigProfilerProfile($this->getTemplateName(), "block", "batchactions"));

        $internal96091652dc7868cf71a435d6d17b95e0c8024df9d67c39dda40a5d90e212ea52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal96091652dc7868cf71a435d6d17b95e0c8024df9d67c39dda40a5d90e212ea52->enter($internal96091652dc7868cf71a435d6d17b95e0c8024df9d67c39dda40a5d90e212ea52prof = new TwigProfilerProfile($this->getTemplateName(), "block", "batchactions"));

        // line 163
        echo "                                            <label class=\"checkbox\" for=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 163, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "allelements\">
                                                <input type=\"checkbox\" name=\"allelements\" id=\"";
        // line 164
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 164, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
        echo "allelements\">
                                                ";
        // line 165
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("allelements", array(), "SonataAdminBundle"), "html", null, true);
        echo "
                                                (";
        // line 166
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 166, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "nbresults", array()), "html", null, true);
        echo ")
                                            </label>

                                            <select name=\"action\" style=\"width: auto; height: auto\" class=\"form-control\">
                                                ";
        // line 170
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["batchactions"]) || arraykeyexists("batchactions", $context) ? $context["batchactions"] : (function () { throw new TwigErrorRuntime('Variable "batchactions" does not exist.', 170, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["action"] => $context["options"]) {
            // line 171
            echo "                                                    <option value=\"";
            echo twigescapefilter($this->env, $context["action"], "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["options"], "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), $context["options"], "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["options"], "translationdomain", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 171, $this->getSourceContext()); })()), "translationDomain", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 171, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
            echo "</option>
                                                ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['action'], $context['options'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 173
        echo "                                            </select>
                                        ";
        
        $internal96091652dc7868cf71a435d6d17b95e0c8024df9d67c39dda40a5d90e212ea52->leave($internal96091652dc7868cf71a435d6d17b95e0c8024df9d67c39dda40a5d90e212ea52prof);

        
        $internal96b12b7505d782eb44f7ab6ebee3049a528df318b4d25b12a9bc4c57eff5da49->leave($internal96b12b7505d782eb44f7ab6ebee3049a528df318b4d25b12a9bc4c57eff5da49prof);

    }

    // line 208
    public function blockpagerresults($context, array $blocks = array())
    {
        $internalbc3e4ed2617af8c433b51791b6a95d4ba5a489b84b3ff338ac017f4284b84095 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbc3e4ed2617af8c433b51791b6a95d4ba5a489b84b3ff338ac017f4284b84095->enter($internalbc3e4ed2617af8c433b51791b6a95d4ba5a489b84b3ff338ac017f4284b84095prof = new TwigProfilerProfile($this->getTemplateName(), "block", "pagerresults"));

        $internal556ad867b6e541df9f97b5e9145b98df8dceece1012cd83913ce91a596ba6a30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal556ad867b6e541df9f97b5e9145b98df8dceece1012cd83913ce91a596ba6a30->enter($internal556ad867b6e541df9f97b5e9145b98df8dceece1012cd83913ce91a596ba6a30prof = new TwigProfilerProfile($this->getTemplateName(), "block", "pagerresults"));

        // line 209
        echo "                                        ";
        $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 209, $this->getSourceContext()); })()), "getTemplate", array(0 => "pagerresults"), "method"), "SonataAdminBundle:CRUD:baselist.html.twig", 209)->display($context);
        // line 210
        echo "                                    ";
        
        $internal556ad867b6e541df9f97b5e9145b98df8dceece1012cd83913ce91a596ba6a30->leave($internal556ad867b6e541df9f97b5e9145b98df8dceece1012cd83913ce91a596ba6a30prof);

        
        $internalbc3e4ed2617af8c433b51791b6a95d4ba5a489b84b3ff338ac017f4284b84095->leave($internalbc3e4ed2617af8c433b51791b6a95d4ba5a489b84b3ff338ac017f4284b84095prof);

    }

    // line 215
    public function blockpagerlinks($context, array $blocks = array())
    {
        $internal023cdfcd5dfea37e1b3c0533a30ba8458b1ad7e642872bd53857c5afc7d5afce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal023cdfcd5dfea37e1b3c0533a30ba8458b1ad7e642872bd53857c5afc7d5afce->enter($internal023cdfcd5dfea37e1b3c0533a30ba8458b1ad7e642872bd53857c5afc7d5afceprof = new TwigProfilerProfile($this->getTemplateName(), "block", "pagerlinks"));

        $internal99e7adced1c4703508a987aac560b5c8b8a2ba3ff2e2d27973e3a67c7fb6825f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal99e7adced1c4703508a987aac560b5c8b8a2ba3ff2e2d27973e3a67c7fb6825f->enter($internal99e7adced1c4703508a987aac560b5c8b8a2ba3ff2e2d27973e3a67c7fb6825fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "pagerlinks"));

        // line 216
        echo "                            ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 216, $this->getSourceContext()); })()), "datagrid", array()), "pager", array()), "haveToPaginate", array(), "method")) {
            // line 217
            echo "                                <hr/>
                                ";
            // line 218
            $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 218, $this->getSourceContext()); })()), "getTemplate", array(0 => "pagerlinks"), "method"), "SonataAdminBundle:CRUD:baselist.html.twig", 218)->display($context);
            // line 219
            echo "                            ";
        }
        // line 220
        echo "                        ";
        
        $internal99e7adced1c4703508a987aac560b5c8b8a2ba3ff2e2d27973e3a67c7fb6825f->leave($internal99e7adced1c4703508a987aac560b5c8b8a2ba3ff2e2d27973e3a67c7fb6825fprof);

        
        $internal023cdfcd5dfea37e1b3c0533a30ba8458b1ad7e642872bd53857c5afc7d5afce->leave($internal023cdfcd5dfea37e1b3c0533a30ba8458b1ad7e642872bd53857c5afc7d5afceprof);

    }

    // line 231
    public function blocklistfiltersactions($context, array $blocks = array())
    {
        $internal3c2adb813d5772057d8a53d87c4582b68e9a8e5670dd0308bba6e705867d556d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3c2adb813d5772057d8a53d87c4582b68e9a8e5670dd0308bba6e705867d556d->enter($internal3c2adb813d5772057d8a53d87c4582b68e9a8e5670dd0308bba6e705867d556dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfiltersactions"));

        $internale2141dde9affa326dd506179d2e732128b423d631b2da9723223288807a300ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale2141dde9affa326dd506179d2e732128b423d631b2da9723223288807a300ab->enter($internale2141dde9affa326dd506179d2e732128b423d631b2da9723223288807a300abprof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfiltersactions"));

        // line 232
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 232, $this->getSourceContext()); })()), "datagrid", array()), "filters", array()))) {
            // line 233
            echo "        <ul class=\"nav navbar-nav navbar-right\">

            <li class=\"dropdown sonata-actions\">
                <a href=\"#\" class=\"dropdown-toggle sonata-ba-action\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-filter\" aria-hidden=\"true\"></i>
                    ";
            // line 238
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkfilters", array(), "SonataAdminBundle"), "html", null, true);
            echo " <b class=\"caret\"></b>
                </a>

                <ul class=\"dropdown-menu\" role=\"menu\">
                    ";
            // line 242
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 242, $this->getSourceContext()); })()), "datagrid", array()), "filters", array()));
            foreach ($context['seq'] as $context["key"] => $context["filter"]) {
                if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array") === true) || (null === twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array")))) {
                    // line 243
                    echo "                        ";
                    $context["filterActive"] = ((twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "isActive", array(), "method") || twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array")) &&  !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 243, $this->getSourceContext()); })()), "isDefaultFilter", array(0 => twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array())), "method"));
                    // line 244
                    echo "                        <li>
                            <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-";
                    // line 245
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 245, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                    echo "-";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "name", array()), "html", null, true);
                    echo "\" filter-container=\"filter-container-";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 245, $this->getSourceContext()); })()), "uniqid", array(), "method"), "html", null, true);
                    echo "\">
                                <i class=\"fa ";
                    // line 246
                    echo (((twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "isActive", array(), "method") || twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array"))) ? ("fa-check-square-o") : ("fa-square-o"));
                    echo "\"></i>";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "translationDomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "translationDomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 246, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                    echo "
                            </a>
                        </li>
                    ";
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['filter'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 250
            echo "                </ul>
            </li>
        </ul>
    ";
        }
        
        $internale2141dde9affa326dd506179d2e732128b423d631b2da9723223288807a300ab->leave($internale2141dde9affa326dd506179d2e732128b423d631b2da9723223288807a300abprof);

        
        $internal3c2adb813d5772057d8a53d87c4582b68e9a8e5670dd0308bba6e705867d556d->leave($internal3c2adb813d5772057d8a53d87c4582b68e9a8e5670dd0308bba6e705867d556dprof);

    }

    // line 256
    public function blocklistfilters($context, array $blocks = array())
    {
        $internalfc5d17cbe496c5ce008815effdfed9c1231861bf320a8d63d7253967abe57a07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfc5d17cbe496c5ce008815effdfed9c1231861bf320a8d63d7253967abe57a07->enter($internalfc5d17cbe496c5ce008815effdfed9c1231861bf320a8d63d7253967abe57a07prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfilters"));

        $internal5bbc1f6aba893e7c380c7dde422717d527a408f0ae86e166988fc997da73acd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5bbc1f6aba893e7c380c7dde422717d527a408f0ae86e166988fc997da73acd6->enter($internal5bbc1f6aba893e7c380c7dde422717d527a408f0ae86e166988fc997da73acd6prof = new TwigProfilerProfile($this->getTemplateName(), "block", "listfilters"));

        // line 257
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 257, $this->getSourceContext()); })()), "datagrid", array()), "filters", array())) {
            // line 258
            echo "        ";
            $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 258, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 258, $this->getSourceContext()); })()), "getTemplate", array(0 => "filter"), "method")));
            // line 259
            echo "
        <div class=\"col-xs-12 col-md-12 sonata-filters-box\" style=\"display: ";
            // line 260
            echo ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 260, $this->getSourceContext()); })()), "datagrid", array()), "hasDisplayableFilters", array())) ? ("block") : ("none"));
            echo "\" id=\"filter-container-";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 260, $this->getSourceContext()); })()), "uniqid", array(), "method"), "html", null, true);
            echo "\">
            <div class=\"box box-primary\" >
                <div class=\"box-body\">
                    <form class=\"sonata-filter-form form-horizontal ";
            // line 263
            echo (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 263, $this->getSourceContext()); })()), "isChild", array()) && (1 == twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 263, $this->getSourceContext()); })()), "datagrid", array()), "filters", array()))))) ? ("hide") : (""));
            echo "\" action=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 263, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
            echo "\" method=\"GET\" role=\"form\">
                        ";
            // line 264
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 264, $this->getSourceContext()); })()), 'errors');
            echo "

                        <div class=\"row\">
                            <div class=\"col-sm-9\">
                                ";
            // line 268
            $context["withAdvancedFilter"] = false;
            // line 269
            echo "                                ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 269, $this->getSourceContext()); })()), "datagrid", array()), "filters", array()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["filter"]) {
                // line 270
                echo "                                    ";
                $context["filterActive"] = (((twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "isActive", array(), "method") && (null === twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array"))) || (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array") === true)) &&  !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 270, $this->getSourceContext()); })()), "isDefaultFilter", array(0 => twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array())), "method"));
                // line 271
                echo "                                    ";
                $context["filterVisible"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array") === true) || (null === twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "showfilter", array(), "array")));
                // line 272
                echo "                                    <div class=\"form-group ";
                $this->displayBlock('sonatalistfiltergroupclass', $context, $blocks);
                echo "\" id=\"filter-";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 272, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "-";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "name", array()), "html", null, true);
                echo "\" sonata-filter=\"";
                echo (((isset($context["filterVisible"]) || arraykeyexists("filterVisible", $context) ? $context["filterVisible"] : (function () { throw new TwigErrorRuntime('Variable "filterVisible" does not exist.', 272, $this->getSourceContext()); })())) ? ("true") : ("false"));
                echo "\" style=\"display: ";
                if ((isset($context["filterActive"]) || arraykeyexists("filterActive", $context) ? $context["filterActive"] : (function () { throw new TwigErrorRuntime('Variable "filterActive" does not exist.', 272, $this->getSourceContext()); })())) {
                    echo "block";
                } else {
                    echo "none";
                }
                echo "\">
                                        ";
                // line 273
                if ( !(twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "label", array()) === false)) {
                    // line 274
                    echo "                                            <label for=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 274, $this->getSourceContext()); })()), "children", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array()), array(), "array"), "children", array()), "value", array(), "array"), "vars", array()), "id", array()), "html", null, true);
                    echo "\" class=\"col-sm-3 control-label\">";
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "label", array()), array(), ((twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "translationDomain", array())) ? (twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "translationDomain", array())) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 274, $this->getSourceContext()); })()), "translationDomain", array())))), "html", null, true);
                    echo "</label>
                                        ";
                }
                // line 276
                echo "                                        ";
                $context["attr"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "children", array(), "any", false, true), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array()), array(), "array", false, true), "children", array(), "any", false, true), "type", array(), "array", false, true), "vars", array(), "any", false, true), "attr", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "children", array(), "any", false, true), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array()), array(), "array", false, true), "children", array(), "any", false, true), "type", array(), "array", false, true), "vars", array(), "any", false, true), "attr", array()), array())) : (array()));
                // line 277
                echo "
                                        <div class=\"col-sm-4 advanced-filter\">
                                            ";
                // line 279
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 279, $this->getSourceContext()); })()), "children", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array()), array(), "array"), "children", array()), "type", array(), "array"), 'widget', array("attr" => (isset($context["attr"]) || arraykeyexists("attr", $context) ? $context["attr"] : (function () { throw new TwigErrorRuntime('Variable "attr" does not exist.', 279, $this->getSourceContext()); })())));
                echo "
                                        </div>

                                        <div class=\"col-sm-4\">
                                            ";
                // line 283
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 283, $this->getSourceContext()); })()), "children", array()), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "formName", array()), array(), "array"), "children", array()), "value", array(), "array"), 'widget');
                echo "
                                        </div>

                                        <div class=\"col-sm-1\">
                                            <label class=\"control-label\">
                                                <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-";
                // line 288
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 288, $this->getSourceContext()); })()), "uniqid", array()), "html", null, true);
                echo "-";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "name", array()), "html", null, true);
                echo "\" filter-container=\"filter-container-";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 288, $this->getSourceContext()); })()), "uniqid", array(), "method"), "html", null, true);
                echo "\">
                                                    <i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>
                                                </a>
                                            </label>
                                        </div>
                                    </div>

                                    ";
                // line 295
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["filter"], "options", array()), "advancedfilter", array(), "array")) {
                    // line 296
                    echo "                                        ";
                    $context["withAdvancedFilter"] = true;
                    // line 297
                    echo "                                    ";
                }
                // line 298
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['filter'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 299
            echo "                            </div>
                            <div class=\"col-sm-3 text-center\">
                                <input type=\"hidden\" name=\"filter[page]\" id=\"filterpage\" value=\"1\">

                                ";
            // line 303
            $context["foo"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 303, $this->getSourceContext()); })()), "children", array()), "page", array(), "array"), "setRendered", array(), "method");
            // line 304
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 304, $this->getSourceContext()); })()), 'rest');
            echo "

                                <div class=\"form-group\">
                                    <button type=\"submit\" class=\"btn btn-primary\">
                                        <i class=\"fa fa-filter\" aria-hidden=\"true\"></i> ";
            // line 308
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnfilter", array(), "SonataAdminBundle"), "html", null, true);
            echo "
                                    </button>

                                    <a class=\"btn btn-default\" href=\"";
            // line 311
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 311, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => array("filters" => "reset")), "method"), "html", null, true);
            echo "\">
                                        ";
            // line 312
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkresetfilter", array(), "SonataAdminBundle"), "html", null, true);
            echo "
                                    </a>
                                </div>

                                ";
            // line 316
            if ((isset($context["withAdvancedFilter"]) || arraykeyexists("withAdvancedFilter", $context) ? $context["withAdvancedFilter"] : (function () { throw new TwigErrorRuntime('Variable "withAdvancedFilter" does not exist.', 316, $this->getSourceContext()); })())) {
                // line 317
                echo "                                    <div class=\"form-group\">
                                        <a href=\"#\" data-toggle=\"advanced-filter\">
                                            <i class=\"fa fa-cogs\" aria-hidden=\"true\"></i>
                                            ";
                // line 320
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnadvancedfilters", array(), "SonataAdminBundle"), "html", null, true);
                echo "
                                        </a>
                                    </div>
                                ";
            }
            // line 324
            echo "                            </div>
                        </div>

                        ";
            // line 327
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 327, $this->getSourceContext()); })()), "persistentParameters", array()));
            foreach ($context['seq'] as $context["paramKey"] => $context["paramValue"]) {
                // line 328
                echo "                            <input type=\"hidden\" name=\"";
                echo twigescapefilter($this->env, $context["paramKey"], "html", null, true);
                echo "\" value=\"";
                echo twigescapefilter($this->env, $context["paramValue"], "html", null, true);
                echo "\">
                        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['paramKey'], $context['paramValue'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 330
            echo "                    </form>
                </div>
            </div>
        </div>
    ";
        }
        
        $internal5bbc1f6aba893e7c380c7dde422717d527a408f0ae86e166988fc997da73acd6->leave($internal5bbc1f6aba893e7c380c7dde422717d527a408f0ae86e166988fc997da73acd6prof);

        
        $internalfc5d17cbe496c5ce008815effdfed9c1231861bf320a8d63d7253967abe57a07->leave($internalfc5d17cbe496c5ce008815effdfed9c1231861bf320a8d63d7253967abe57a07prof);

    }

    // line 272
    public function blocksonatalistfiltergroupclass($context, array $blocks = array())
    {
        $internaldbd6fbf60b37d06abb9e2d7b3b314172c2cadbdbc641275668b059776b6d4ca1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldbd6fbf60b37d06abb9e2d7b3b314172c2cadbdbc641275668b059776b6d4ca1->enter($internaldbd6fbf60b37d06abb9e2d7b3b314172c2cadbdbc641275668b059776b6d4ca1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatalistfiltergroupclass"));

        $internalccbec8ea8b892244227b296217ea701502da515e0e90b370e56985e2a6bf1787 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalccbec8ea8b892244227b296217ea701502da515e0e90b370e56985e2a6bf1787->enter($internalccbec8ea8b892244227b296217ea701502da515e0e90b370e56985e2a6bf1787prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatalistfiltergroupclass"));

        
        $internalccbec8ea8b892244227b296217ea701502da515e0e90b370e56985e2a6bf1787->leave($internalccbec8ea8b892244227b296217ea701502da515e0e90b370e56985e2a6bf1787prof);

        
        $internaldbd6fbf60b37d06abb9e2d7b3b314172c2cadbdbc641275668b059776b6d4ca1->leave($internaldbd6fbf60b37d06abb9e2d7b3b314172c2cadbdbc641275668b059776b6d4ca1prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baselist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1107 => 272,  1092 => 330,  1081 => 328,  1077 => 327,  1072 => 324,  1065 => 320,  1060 => 317,  1058 => 316,  1051 => 312,  1047 => 311,  1041 => 308,  1033 => 304,  1031 => 303,  1025 => 299,  1011 => 298,  1008 => 297,  1005 => 296,  1003 => 295,  989 => 288,  981 => 283,  974 => 279,  970 => 277,  967 => 276,  959 => 274,  957 => 273,  940 => 272,  937 => 271,  934 => 270,  916 => 269,  914 => 268,  907 => 264,  901 => 263,  893 => 260,  890 => 259,  887 => 258,  884 => 257,  875 => 256,  861 => 250,  848 => 246,  840 => 245,  837 => 244,  834 => 243,  829 => 242,  822 => 238,  815 => 233,  813 => 232,  804 => 231,  794 => 220,  791 => 219,  789 => 218,  786 => 217,  783 => 216,  774 => 215,  764 => 210,  761 => 209,  752 => 208,  741 => 173,  730 => 171,  726 => 170,  719 => 166,  715 => 165,  711 => 164,  706 => 163,  697 => 162,  662 => 134,  653 => 133,  641 => 176,  638 => 175,  636 => 162,  632 => 160,  630 => 133,  627 => 132,  618 => 131,  608 => 223,  604 => 221,  602 => 215,  598 => 213,  594 => 211,  592 => 208,  589 => 207,  582 => 202,  572 => 198,  567 => 196,  564 => 195,  560 => 194,  553 => 190,  548 => 187,  546 => 186,  542 => 184,  539 => 183,  534 => 179,  531 => 178,  528 => 131,  526 => 130,  523 => 129,  521 => 128,  517 => 126,  514 => 125,  505 => 124,  492 => 116,  488 => 114,  486 => 113,  483 => 112,  481 => 111,  473 => 106,  468 => 103,  459 => 102,  449 => 99,  440 => 98,  429 => 95,  427 => 94,  424 => 93,  415 => 92,  403 => 88,  397 => 87,  394 => 86,  390 => 84,  386 => 83,  381 => 82,  375 => 80,  372 => 79,  366 => 78,  344 => 77,  342 => 76,  339 => 75,  336 => 74,  333 => 73,  330 => 72,  327 => 71,  324 => 70,  321 => 69,  318 => 68,  315 => 67,  312 => 66,  310 => 65,  307 => 64,  305 => 63,  303 => 62,  300 => 61,  298 => 60,  293 => 57,  290 => 56,  286 => 55,  282 => 53,  273 => 52,  256 => 48,  245 => 228,  241 => 226,  239 => 225,  236 => 224,  234 => 124,  229 => 122,  226 => 121,  223 => 120,  220 => 102,  216 => 100,  214 => 98,  211 => 97,  209 => 92,  206 => 91,  204 => 52,  201 => 51,  199 => 50,  196 => 49,  194 => 48,  189 => 46,  183 => 45,  176 => 44,  173 => 42,  168 => 40,  163 => 39,  160 => 38,  158 => 37,  155 => 36,  146 => 35,  133 => 32,  124 => 31,  110 => 27,  108 => 26,  105 => 25,  103 => 21,  94 => 20,  76 => 18,  66 => 15,  57 => 14,  36 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}{{ knpmenurender(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')}, 'twig') }}{% endblock %}

{% block title %}
    {#
        The list template can be used in nested mode,
        so we define the title corresponding to the parent's admin.
    #}

    {% if admin.isChild and admin.parent.subject %}
        {{ \"titleedit\"|trans({'%name%': admin.parent.toString(admin.parent.subject)|truncate(15) }, 'SonataAdminBundle') }}
    {% endif %}
{% endblock %}

{% block navbartitle %}
    {{ block('title') }}
{% endblock %}

{% block listtable %}
    <div class=\"col-xs-12 col-md-12\">
        {% set batchactions = admin.batchactions %}
        {% if admin.hasRoute('batch') and batchactions|length %}
            <form action=\"{{ admin.generateUrl('batch', {'filter': admin.filterParameters}) }}\" method=\"POST\" >
            <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"{{ csrftoken }}\">
        {% endif %}

        {# Add a margin if no pager to prevent dropdown cropping on window #}
        <div class=\"box box-primary\" {% if admin.datagrid.pager.lastPage == 1 %}style=\"margin-bottom: 100px;\"{% endif %}>
            <div class=\"box-body {% if admin.datagrid.results|length > 0 %}table-responsive no-padding{% endif %}\">
                {{ sonatablockrenderevent('sonata.admin.list.table.top', { 'admin': admin }) }}

                {% block listheader %}{% endblock %}

                {% if admin.datagrid.results|length > 0 %}
                    <table class=\"table table-bordered table-striped sonata-ba-list\">
                        {% block tableheader %}
                            <thead>
                                <tr class=\"sonata-ba-list-field-header\">
                                    {% for fielddescription in admin.list.elements %}
                                        {% if admin.hasRoute('batch') and fielddescription.getOption('code') == 'batch' and batchactions|length > 0 %}
                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-batch\">
                                              <input type=\"checkbox\" id=\"listbatchcheckbox\">
                                            </th>
                                        {% elseif fielddescription.getOption('code') == 'select' %}
                                            <th class=\"sonata-ba-list-field-header sonata-ba-list-field-header-select\"></th>
                                        {% elseif fielddescription.name == 'action' and app.request.isXmlHttpRequest %}
                                            {# Action buttons disabled in ajax view! #}
                                        {% elseif fielddescription.getOption('ajaxhidden') == true and app.request.isXmlHttpRequest %}
                                            {# Disable fields with 'ajaxhidden' option set to true #}
                                        {% else %}
                                            {% set sortable = false %}
                                            {% if fielddescription.options.sortable is defined and fielddescription.options.sortable %}
                                                {% set sortable             = true %}
                                                {% set sortparameters      = admin.modelmanager.sortparameters(fielddescription, admin.datagrid) %}
                                                {% set current              = admin.datagrid.values.sortby == fielddescription or admin.datagrid.values.sortby.fieldName == sortparameters.filter.sortby %}
                                                {% set sortactiveclass    = current ? 'sonata-ba-list-field-order-active' : '' %}
                                                {% set sortby              = current ? admin.datagrid.values.sortorder : fielddescription.options.sortorder %}
                                            {% endif %}

                                            {% spaceless %}
                                                <th class=\"sonata-ba-list-field-header-{{ fielddescription.type}} {% if sortable %} sonata-ba-list-field-header-order-{{ sortby|lower }} {{ sortactiveclass }}{% endif %}{% if fielddescription.options.headerclass is defined %} {{ fielddescription.options.headerclass }}{% endif %}\"{% if fielddescription.options.headerstyle is defined %} style=\"{{ fielddescription.options.headerstyle }}\"{% endif %}>
                                                    {% if sortable %}<a href=\"{{ admin.generateUrl('list', sortparameters) }}\">{% endif %}
                                                    {% if fielddescription.getOption('labelicon') %}
                                                        <i class=\"sonata-ba-list-field-header-label-icon {{ fielddescription.getOption('labelicon') }}\" aria-hidden=\"true\"></i>
                                                    {% endif %}
                                                    {{ fielddescription.label|trans({}, fielddescription.translationDomain)|raw }}
                                                    {% if sortable %}</a>{% endif %}
                                                </th>
                                            {% endspaceless %}
                                        {% endif %}
                                    {% endfor %}
                                </tr>
                            </thead>
                        {% endblock %}

                        {% block tablebody %}
                            <tbody>
                                {% include admin.getTemplate('outerlistrows' ~ admin.getListMode()) %}
                            </tbody>
                        {% endblock %}

                        {% block tablefooter %}
                        {% endblock %}
                    </table>
                {% else %}
                    {% block noresultcontent %}
                        <div class=\"info-box\">
                            <span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i></span>
                            <div class=\"info-box-content\">
                                <span class=\"info-box-text\">{{ 'noresult'|trans({}, 'SonataAdminBundle') }}</span>
                                <div class=\"progress\">
                                    <div class=\"progress-bar\" style=\"width: 0%\"></div>
                                </div>
                                <span class=\"progress-description\">
                                    {% if not app.request.xmlHttpRequest %}
                                    <ul class=\"list-unstyled\">
                                        {% include 'SonataAdminBundle:Button:createbutton.html.twig' %}
                                    </ul>
                                    {% endif %}
                                </span>
                            </div><!-- /.info-box-content -->
                        </div>
                    {% endblock %}
                {% endif %}

                {{ sonatablockrenderevent('sonata.admin.list.table.bottom', { 'admin': admin }) }}
            </div>
            {% block listfooter %}
                {% if admin.datagrid.results|length > 0 %}
                    <div class=\"box-footer\">
                        <div class=\"form-inline clearfix\">
                            {% if not app.request.isXmlHttpRequest %}
                                <div class=\"pull-left\">
                                    {% if admin.hasRoute('batch') and batchactions|length > 0  %}
                                        {% block batch %}
                                            <script>
                                                {% block batchjavascript %}
                                                    jQuery(document).ready(function (\$) {
                                                        // Toggle individual checkboxes when the batch checkbox is changed
                                                        \$('#listbatchcheckbox').on('ifChanged change', function () {
                                                            var checkboxes = \$(this)
                                                                .closest('table')
                                                                .find('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            ;
                                                            if (window.SONATACONFIG.USEICHECK) {
                                                                checkboxes.iCheck(\$(this).is(':checked') ? 'check' : 'uncheck');
                                                            } else {
                                                                checkboxes.prop('checked', this.checked);
                                                            }
                                                        });

                                                        // Add a CSS class to rows when they are selected
                                                        \$('td.sonata-ba-list-field-batch input[type=\"checkbox\"], div.sonata-ba-list-field-batch input[type=\"checkbox\"]')
                                                            .on('ifChanged change', function () {
                                                                \$(this)
                                                                    .closest('tr, div.sonata-ba-list-field-batch')
                                                                    .toggleClass('sonata-ba-list-row-selected', \$(this).is(':checked'))
                                                                ;
                                                            })
                                                            .trigger('ifChanged')
                                                        ;
                                                    });
                                                {% endblock %}
                                            </script>

                                        {% block batchactions %}
                                            <label class=\"checkbox\" for=\"{{ admin.uniqid }}allelements\">
                                                <input type=\"checkbox\" name=\"allelements\" id=\"{{ admin.uniqid }}allelements\">
                                                {{ 'allelements'|trans({}, 'SonataAdminBundle') }}
                                                ({{ admin.datagrid.pager.nbresults }})
                                            </label>

                                            <select name=\"action\" style=\"width: auto; height: auto\" class=\"form-control\">
                                                {% for action, options in batchactions %}
                                                    <option value=\"{{ action }}\">{{ options.label|trans({}, options.translationdomain|default(admin.translationDomain)) }}</option>
                                                {% endfor %}
                                            </select>
                                        {% endblock %}

                                            <input type=\"submit\" class=\"btn btn-small btn-primary\" value=\"{{ 'btnbatch'|trans({}, 'SonataAdminBundle') }}\">
                                        {% endblock %}
                                    {% endif %}
                                </div>


                                {# NEXTMAJOR : remove this assignment #}
                                {% set exportformats = exportformats|default(admin.exportFormats) %}

                                <div class=\"pull-right\">
                                    {% if admin.hasRoute('export') and admin.hasAccess('export') and exportformats|length %}
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                                <i class=\"fa fa-share-square-o\" aria-hidden=\"true\"></i>
                                                {{ \"labelexportdownload\"|trans({}, \"SonataAdminBundle\") }}
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\">
                                                {% for format in exportformats %}
                                                <li>
                                                    <a href=\"{{ admin.generateUrl('export', admin.modelmanager.paginationparameters(admin.datagrid, 0) + {'format' : format}) }}\">
                                                        <i class=\"fa fa-arrow-circle-o-down\" aria-hidden=\"true\"></i>
                                                        {{ (\"exportformat\" ~ format)|trans({}, 'SonataAdminBundle') }}
                                                    </a>
                                                <li>
                                                {% endfor %}
                                            </ul>
                                        </div>

                                        &nbsp;-&nbsp;
                                    {% endif %}

                                    {% block pagerresults %}
                                        {% include admin.getTemplate('pagerresults') %}
                                    {% endblock %}
                                </div>
                            {% endif %}
                        </div>

                        {% block pagerlinks %}
                            {% if admin.datagrid.pager.haveToPaginate() %}
                                <hr/>
                                {% include admin.getTemplate('pagerlinks') %}
                            {% endif %}
                        {% endblock %}
                    </div>
                {% endif %}
            {% endblock %}
        </div>
        {% if admin.hasRoute('batch') and batchactions|length %}
            </form>
        {% endif %}
    </div>
{% endblock %}

{% block listfiltersactions %}
    {%- if admin.datagrid.filters|length %}
        <ul class=\"nav navbar-nav navbar-right\">

            <li class=\"dropdown sonata-actions\">
                <a href=\"#\" class=\"dropdown-toggle sonata-ba-action\" data-toggle=\"dropdown\">
                    <i class=\"fa fa-filter\" aria-hidden=\"true\"></i>
                    {{ 'linkfilters'|trans({}, 'SonataAdminBundle') }} <b class=\"caret\"></b>
                </a>

                <ul class=\"dropdown-menu\" role=\"menu\">
                    {% for filter in admin.datagrid.filters if (filter.options['showfilter'] is same as(true) or filter.options['showfilter'] is null) %}
                        {% set filterActive = ((filter.isActive() or filter.options['showfilter']) and not admin.isDefaultFilter(filter.formName)) %}
                        <li>
                            <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-{{ admin.uniqid }}-{{ filter.name }}\" filter-container=\"filter-container-{{ admin.uniqid() }}\">
                                <i class=\"fa {{ (filter.isActive() or filter.options['showfilter']) ? 'fa-check-square-o' : 'fa-square-o' }}\"></i>{{ filter.label|trans({}, filter.translationDomain ?: admin.translationDomain) }}
                            </a>
                        </li>
                    {% endfor %}
                </ul>
            </li>
        </ul>
    {% endif -%}
{% endblock %}

{% block listfilters %}
    {% if admin.datagrid.filters %}
        {% formtheme form admin.getTemplate('filter') %}

        <div class=\"col-xs-12 col-md-12 sonata-filters-box\" style=\"display: {{ admin.datagrid.hasDisplayableFilters ? 'block' : 'none' }}\" id=\"filter-container-{{ admin.uniqid() }}\">
            <div class=\"box box-primary\" >
                <div class=\"box-body\">
                    <form class=\"sonata-filter-form form-horizontal {{ admin.isChild and 1 == admin.datagrid.filters|length ? 'hide' : '' }}\" action=\"{{ admin.generateUrl('list') }}\" method=\"GET\" role=\"form\">
                        {{ formerrors(form) }}

                        <div class=\"row\">
                            <div class=\"col-sm-9\">
                                {% set withAdvancedFilter = false %}
                                {% for filter in admin.datagrid.filters %}
                                    {% set filterActive = ((filter.isActive() and filter.options['showfilter'] is null) or (filter.options['showfilter'] is same as(true))) and not admin.isDefaultFilter(filter.formName) %}
                                    {% set filterVisible = filter.options['showfilter'] is same as(true) or filter.options['showfilter'] is null %}
                                    <div class=\"form-group {% block sonatalistfiltergroupclass %}{% endblock %}\" id=\"filter-{{ admin.uniqid }}-{{ filter.name }}\" sonata-filter=\"{{ filterVisible ? 'true' : 'false' }}\" style=\"display: {% if filterActive %}block{% else %}none{% endif %}\">
                                        {% if filter.label is not same as(false) %}
                                            <label for=\"{{ form.children[filter.formName].children['value'].vars.id }}\" class=\"col-sm-3 control-label\">{{ filter.label|trans({}, filter.translationDomain ?: admin.translationDomain) }}</label>
                                        {% endif %}
                                        {% set attr = form.children[filter.formName].children['type'].vars.attr|default({}) %}

                                        <div class=\"col-sm-4 advanced-filter\">
                                            {{ formwidget(form.children[filter.formName].children['type'], {'attr':  attr}) }}
                                        </div>

                                        <div class=\"col-sm-4\">
                                            {{ formwidget(form.children[filter.formName].children['value']) }}
                                        </div>

                                        <div class=\"col-sm-1\">
                                            <label class=\"control-label\">
                                                <a href=\"#\" class=\"sonata-toggle-filter sonata-ba-action\" filter-target=\"filter-{{ admin.uniqid }}-{{ filter.name }}\" filter-container=\"filter-container-{{ admin.uniqid() }}\">
                                                    <i class=\"fa fa-minus-circle\" aria-hidden=\"true\"></i>
                                                </a>
                                            </label>
                                        </div>
                                    </div>

                                    {% if filter.options['advancedfilter'] %}
                                        {% set withAdvancedFilter = true %}
                                    {% endif %}
                                {% endfor %}
                            </div>
                            <div class=\"col-sm-3 text-center\">
                                <input type=\"hidden\" name=\"filter[page]\" id=\"filterpage\" value=\"1\">

                                {% set foo = form.children['page'].setRendered() %}
                                {{ formrest(form) }}

                                <div class=\"form-group\">
                                    <button type=\"submit\" class=\"btn btn-primary\">
                                        <i class=\"fa fa-filter\" aria-hidden=\"true\"></i> {{ 'btnfilter'|trans({}, 'SonataAdminBundle') }}
                                    </button>

                                    <a class=\"btn btn-default\" href=\"{{ admin.generateUrl('list', {filters: 'reset'}) }}\">
                                        {{ 'linkresetfilter'|trans({}, 'SonataAdminBundle') }}
                                    </a>
                                </div>

                                {% if withAdvancedFilter %}
                                    <div class=\"form-group\">
                                        <a href=\"#\" data-toggle=\"advanced-filter\">
                                            <i class=\"fa fa-cogs\" aria-hidden=\"true\"></i>
                                            {{ 'btnadvancedfilters'|trans({}, 'SonataAdminBundle') }}
                                        </a>
                                    </div>
                                {% endif %}
                            </div>
                        </div>

                        {% for paramKey, paramValue in admin.persistentParameters %}
                            <input type=\"hidden\" name=\"{{ paramKey }}\" value=\"{{ paramValue }}\">
                        {% endfor %}
                    </form>
                </div>
            </div>
        </div>
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:baselist.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baselist.html.twig");
    }
}
