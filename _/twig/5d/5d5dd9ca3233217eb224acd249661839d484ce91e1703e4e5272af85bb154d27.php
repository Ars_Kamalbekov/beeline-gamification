<?php

/* TwigBundle:Exception:error.js.twig */
class TwigTemplate1aad15ecda650c0d2416b6f19058bb18fd89ffcbe2d81f201838faa390590703 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale5afe25dbcd6157e2b01a45d3ae65324db7fb3e1649ac8b224416aa67c85699b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale5afe25dbcd6157e2b01a45d3ae65324db7fb3e1649ac8b224416aa67c85699b->enter($internale5afe25dbcd6157e2b01a45d3ae65324db7fb3e1649ac8b224416aa67c85699bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $internalaee3e9fc4ffc30cb0fce770743afc11233a2afbb2cceede4ddfed6763477b9c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaee3e9fc4ffc30cb0fce770743afc11233a2afbb2cceede4ddfed6763477b9c2->enter($internalaee3e9fc4ffc30cb0fce770743afc11233a2afbb2cceede4ddfed6763477b9c2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 2, $this->getSourceContext()); })()), "js", null, true);
        echo " ";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 2, $this->getSourceContext()); })()), "js", null, true);
        echo "

*/
";
        
        $internale5afe25dbcd6157e2b01a45d3ae65324db7fb3e1649ac8b224416aa67c85699b->leave($internale5afe25dbcd6157e2b01a45d3ae65324db7fb3e1649ac8b224416aa67c85699bprof);

        
        $internalaee3e9fc4ffc30cb0fce770743afc11233a2afbb2cceede4ddfed6763477b9c2->leave($internalaee3e9fc4ffc30cb0fce770743afc11233a2afbb2cceede4ddfed6763477b9c2prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("/*
{{ statuscode }} {{ statustext }}

*/
", "TwigBundle:Exception:error.js.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
