<?php

/* @WebProfiler/Profiler/layout.html.twig */
class TwigTemplate43a15154bdf601fe68bc009a0df197b58a97848f40fbb8d5322a521ab3e8dc66 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'blockbody'),
            'summary' => array($this, 'blocksummary'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internale94844eb9b1db61b786754fca9660baecb2aa6af14499b8949059f1ed48da807 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale94844eb9b1db61b786754fca9660baecb2aa6af14499b8949059f1ed48da807->enter($internale94844eb9b1db61b786754fca9660baecb2aa6af14499b8949059f1ed48da807prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Profiler/layout.html.twig"));

        $internal0290f45c176d5ffa15c7543e10be94e22a3539f2c3e13552e64167835498a500 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0290f45c176d5ffa15c7543e10be94e22a3539f2c3e13552e64167835498a500->enter($internal0290f45c176d5ffa15c7543e10be94e22a3539f2c3e13552e64167835498a500prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Profiler/layout.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internale94844eb9b1db61b786754fca9660baecb2aa6af14499b8949059f1ed48da807->leave($internale94844eb9b1db61b786754fca9660baecb2aa6af14499b8949059f1ed48da807prof);

        
        $internal0290f45c176d5ffa15c7543e10be94e22a3539f2c3e13552e64167835498a500->leave($internal0290f45c176d5ffa15c7543e10be94e22a3539f2c3e13552e64167835498a500prof);

    }

    // line 3
    public function blockbody($context, array $blocks = array())
    {
        $internal93ee924c456fad929eca8822f868fccc3c547c7d6b4266aad5947d62da61edab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal93ee924c456fad929eca8822f868fccc3c547c7d6b4266aad5947d62da61edab->enter($internal93ee924c456fad929eca8822f868fccc3c547c7d6b4266aad5947d62da61edabprof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internalc18bebb50b2473c50125db51990cb17de99742c11aae5ecba67c92003d8d5b88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc18bebb50b2473c50125db51990cb17de99742c11aae5ecba67c92003d8d5b88->enter($internalc18bebb50b2473c50125db51990cb17de99742c11aae5ecba67c92003d8d5b88prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/header.html.twig", array(), false);
        echo "

    <div id=\"summary\">
        ";
        // line 7
        $this->displayBlock('summary', $context, $blocks);
        // line 85
        echo "    </div>

    <div id=\"content\" class=\"container\">
        <div id=\"main\">
            <div id=\"sidebar\">
                <div id=\"sidebar-shortcuts\">
                    <div class=\"shortcuts\">
                        <a href=\"#\" id=\"sidebarShortcutsMenu\" class=\"visible-small\">
                            <span class=\"icon\">";
        // line 93
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/menu.svg");
        echo "</span>
                        </a>

                        <a class=\"btn btn-sm\" href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearch", array("limit" => 10));
        echo "\">Last 10</a>
                        <a class=\"btn btn-sm\" href=\"";
        // line 97
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", twigarraymerge(array("token" => "latest"), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 97, $this->getSourceContext()); })()), "query", array()), "all", array()))), "html", null, true);
        echo "\">Latest</a>

                        <a class=\"sf-toggle btn btn-sm\" data-toggle-selector=\"#sidebar-search\" ";
        // line 99
        if ((arraykeyexists("tokens", $context) || arraykeyexists("about", $context))) {
            echo "data-toggle-initial=\"display\"";
        }
        echo ">
                            ";
        // line 100
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/search.svg");
        echo " <span class=\"hidden-small\">Search</span>
                        </a>

                        ";
        // line 103
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearchbar", twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 103, $this->getSourceContext()); })()), "query", array()), "all", array())));
        echo "
                    </div>
                </div>

                ";
        // line 107
        if (arraykeyexists("templates", $context)) {
            // line 108
            echo "                    <ul id=\"menu-profiler\">
                        ";
            // line 109
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["templates"]) || arraykeyexists("templates", $context) ? $context["templates"] : (function () { throw new TwigErrorRuntime('Variable "templates" does not exist.', 109, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["name"] => $context["template"]) {
                // line 110
                echo "                            ";
                obstart();
                // line 111
                if (                $this->loadTemplate($context["template"], "@WebProfiler/Profiler/layout.html.twig", 111)->hasBlock("menu", $context)) {
                    // line 112
                    $internalc3e2301c70a48bd6335d37f14ee58e193b6d88064ec9046f8e3b197d9e0f90de = array("collector" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 112, $this->getSourceContext()); })()), "getcollector", array(0 => $context["name"]), "method"), "profilermarkupversion" => (isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 112, $this->getSourceContext()); })()));
                    if (!isarray($internalc3e2301c70a48bd6335d37f14ee58e193b6d88064ec9046f8e3b197d9e0f90de)) {
                        throw new TwigErrorRuntime('Variables passed to the "with" tag must be a hash.');
                    }
                    $context['parent'] = $context;
                    $context = arraymerge($context, $internalc3e2301c70a48bd6335d37f14ee58e193b6d88064ec9046f8e3b197d9e0f90de);
                    // line 113
                    $this->loadTemplate($context["template"], "@WebProfiler/Profiler/layout.html.twig", 113)->displayBlock("menu", $context);
                    $context = $context['parent'];
                }
                $context["menu"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
                // line 117
                echo "                            ";
                if ( !twigtestempty((isset($context["menu"]) || arraykeyexists("menu", $context) ? $context["menu"] : (function () { throw new TwigErrorRuntime('Variable "menu" does not exist.', 117, $this->getSourceContext()); })()))) {
                    // line 118
                    echo "                                <li class=\"";
                    echo twigescapefilter($this->env, $context["name"], "html", null, true);
                    echo " ";
                    echo ((($context["name"] == (isset($context["panel"]) || arraykeyexists("panel", $context) ? $context["panel"] : (function () { throw new TwigErrorRuntime('Variable "panel" does not exist.', 118, $this->getSourceContext()); })()))) ? ("selected") : (""));
                    echo "\">
                                    <a href=\"";
                    // line 119
                    echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 119, $this->getSourceContext()); })()), "panel" => $context["name"])), "html", null, true);
                    echo "\">";
                    echo (isset($context["menu"]) || arraykeyexists("menu", $context) ? $context["menu"] : (function () { throw new TwigErrorRuntime('Variable "menu" does not exist.', 119, $this->getSourceContext()); })());
                    echo "</a>
                                </li>
                            ";
                }
                // line 122
                echo "                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['name'], $context['template'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 123
            echo "                    </ul>
                ";
        }
        // line 125
        echo "            </div>

            <div id=\"collector-wrapper\">
                <div id=\"collector-content\">
                    ";
        // line 129
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/basejs.html.twig");
        echo "
                    ";
        // line 130
        $this->displayBlock('panel', $context, $blocks);
        // line 131
        echo "                </div>
            </div>
        </div>
    </div>
    <script>
        (function () {
            Sfjs.addEventListener(document.getElementById('sidebarShortcutsMenu'), 'click', function (event) {
                event.preventDefault();
                Sfjs.toggleClass(document.getElementById('sidebar'), 'expanded');
            })
        }())
    </script>
";
        
        $internalc18bebb50b2473c50125db51990cb17de99742c11aae5ecba67c92003d8d5b88->leave($internalc18bebb50b2473c50125db51990cb17de99742c11aae5ecba67c92003d8d5b88prof);

        
        $internal93ee924c456fad929eca8822f868fccc3c547c7d6b4266aad5947d62da61edab->leave($internal93ee924c456fad929eca8822f868fccc3c547c7d6b4266aad5947d62da61edabprof);

    }

    // line 7
    public function blocksummary($context, array $blocks = array())
    {
        $internal0d98320e7a923fac4e2c20ff1a6b618fc18f56e1d0a380545cd18d9d0eca1f80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0d98320e7a923fac4e2c20ff1a6b618fc18f56e1d0a380545cd18d9d0eca1f80->enter($internal0d98320e7a923fac4e2c20ff1a6b618fc18f56e1d0a380545cd18d9d0eca1f80prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        $internalf2b76d9a1db5d6e58dd306e86d0690cb915b60c6d8fe85691606785c2a2fa5a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf2b76d9a1db5d6e58dd306e86d0690cb915b60c6d8fe85691606785c2a2fa5a5->enter($internalf2b76d9a1db5d6e58dd306e86d0690cb915b60c6d8fe85691606785c2a2fa5a5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "summary"));

        // line 8
        echo "            ";
        if (arraykeyexists("profile", $context)) {
            // line 9
            echo "                ";
            $context["statuscode"] = ((twiginfilter("request", twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 9, $this->getSourceContext()); })()), "collectors", array())))) ? (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["profile"] ?? null), "getcollector", array(0 => "request"), "method", false, true), "statuscode", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["profile"] ?? null), "getcollector", array(0 => "request"), "method", false, true), "statuscode", array()), 0)) : (0))) : (0));
            // line 10
            echo "                ";
            $context["cssclass"] = ((((isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 10, $this->getSourceContext()); })()) > 399)) ? ("status-error") : (((((isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 10, $this->getSourceContext()); })()) > 299)) ? ("status-warning") : ("status-success"))));
            // line 11
            echo "
                <div class=\"status ";
            // line 12
            echo twigescapefilter($this->env, (isset($context["cssclass"]) || arraykeyexists("cssclass", $context) ? $context["cssclass"] : (function () { throw new TwigErrorRuntime('Variable "cssclass" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
            echo "\">
                    <div class=\"container\">
                        <h2 class=\"break-long-words\">
                            ";
            // line 15
            if (twiginfilter(twigupperfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 15, $this->getSourceContext()); })()), "method", array())), array(0 => "GET", 1 => "HEAD"))) {
                // line 16
                echo "                                <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 16, $this->getSourceContext()); })()), "url", array()), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 16, $this->getSourceContext()); })()), "url", array()), "html", null, true);
                echo "</a>
                            ";
            } else {
                // line 18
                echo "                                ";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 18, $this->getSourceContext()); })()), "url", array()), "html", null, true);
                echo "
                            ";
            }
            // line 20
            echo "                        </h2>

                        ";
            // line 22
            $context["requestcollector"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["profile"] ?? null), "collectors", array(), "any", false, true), "request", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["profile"] ?? null), "collectors", array(), "any", false, true), "request", array()), false)) : (false));
            // line 23
            echo "                        ";
            if ((arraykeyexists("requestcollector", $context) && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["requestcollector"]) || arraykeyexists("requestcollector", $context) ? $context["requestcollector"] : (function () { throw new TwigErrorRuntime('Variable "requestcollector" does not exist.', 23, $this->getSourceContext()); })()), "redirect", array()))) {
                // line 24
                $context["redirect"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["requestcollector"]) || arraykeyexists("requestcollector", $context) ? $context["requestcollector"] : (function () { throw new TwigErrorRuntime('Variable "requestcollector" does not exist.', 24, $this->getSourceContext()); })()), "redirect", array());
                // line 25
                $context["controller"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 25, $this->getSourceContext()); })()), "controller", array());
                // line 26
                $context["redirectroute"] = ("@" . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 26, $this->getSourceContext()); })()), "route", array()));
                // line 27
                echo "                            <dl class=\"metadata\">
                                <dt>
                                    <span class=\"label\">";
                // line 29
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 29, $this->getSourceContext()); })()), "statuscode", array()), "html", null, true);
                echo "</span>
                                    Redirect from
                                </dt>
                                <dd>
                                    ";
                // line 33
                echo twigescapefilter($this->env, ((("GET" != twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 33, $this->getSourceContext()); })()), "method", array()))) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 33, $this->getSourceContext()); })()), "method", array())) : ("")), "html", null, true);
                echo "
                                    ";
                // line 34
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["redirect"] ?? null), "controller", array(), "any", false, true), "class", array(), "any", true, true)) {
                    // line 35
                    $context["link"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->getFileLink(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 35, $this->getSourceContext()); })()), "file", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 35, $this->getSourceContext()); })()), "line", array()));
                    // line 36
                    if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 36, $this->getSourceContext()); })())) {
                        echo "<a href=\"";
                        echo twigescapefilter($this->env, (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 36, $this->getSourceContext()); })()), "html", null, true);
                        echo "\" title=\"";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 36, $this->getSourceContext()); })()), "file", array()), "html", null, true);
                        echo "\">";
                    }
                    // line 37
                    echo twigescapefilter($this->env, (isset($context["redirectroute"]) || arraykeyexists("redirectroute", $context) ? $context["redirectroute"] : (function () { throw new TwigErrorRuntime('Variable "redirectroute" does not exist.', 37, $this->getSourceContext()); })()), "html", null, true);
                    // line 38
                    if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 38, $this->getSourceContext()); })())) {
                        echo "</a>";
                    }
                } else {
                    // line 40
                    echo twigescapefilter($this->env, (isset($context["redirectroute"]) || arraykeyexists("redirectroute", $context) ? $context["redirectroute"] : (function () { throw new TwigErrorRuntime('Variable "redirectroute" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
                }
                // line 42
                echo "                                    (<a href=\"";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 42, $this->getSourceContext()); })()), "token", array()), "panel" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["request"]) || arraykeyexists("request", $context) ? $context["request"] : (function () { throw new TwigErrorRuntime('Variable "request" does not exist.', 42, $this->getSourceContext()); })()), "query", array()), "get", array(0 => "panel", 1 => "request"), "method"))), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["redirect"]) || arraykeyexists("redirect", $context) ? $context["redirect"] : (function () { throw new TwigErrorRuntime('Variable "redirect" does not exist.', 42, $this->getSourceContext()); })()), "token", array()), "html", null, true);
                echo "</a>)
                                </dd>
                            </dl>";
            }
            // line 46
            echo "
                        ";
            // line 47
            if ((((isset($context["requestcollector"]) || arraykeyexists("requestcollector", $context) ? $context["requestcollector"] : (function () { throw new TwigErrorRuntime('Variable "requestcollector" does not exist.', 47, $this->getSourceContext()); })()) && ((twiggetattribute($this->env, $this->getSourceContext(), ($context["requestcollector"] ?? null), "forward", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["requestcollector"] ?? null), "forward", array()), false)) : (false))) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["requestcollector"] ?? null), "forward", array(), "any", false, true), "controller", array(), "any", false, true), "class", array(), "any", true, true))) {
                // line 48
                $context["forward"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["requestcollector"]) || arraykeyexists("requestcollector", $context) ? $context["requestcollector"] : (function () { throw new TwigErrorRuntime('Variable "requestcollector" does not exist.', 48, $this->getSourceContext()); })()), "forward", array());
                // line 49
                $context["controller"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["forward"]) || arraykeyexists("forward", $context) ? $context["forward"] : (function () { throw new TwigErrorRuntime('Variable "forward" does not exist.', 49, $this->getSourceContext()); })()), "controller", array());
                // line 50
                echo "<dl class=\"metadata\">
                                <dt>Forwarded to</dt>
                                <dd>
                                    ";
                // line 53
                $context["link"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->getFileLink(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 53, $this->getSourceContext()); })()), "file", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 53, $this->getSourceContext()); })()), "line", array()));
                // line 54
                if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 54, $this->getSourceContext()); })())) {
                    echo "<a href=\"";
                    echo twigescapefilter($this->env, (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 54, $this->getSourceContext()); })()), "html", null, true);
                    echo "\" title=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 54, $this->getSourceContext()); })()), "file", array()), "html", null, true);
                    echo "\">";
                }
                // line 55
                echo twigescapefilter($this->env, striptags($this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->abbrClass(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 55, $this->getSourceContext()); })()), "class", array()))), "html", null, true);
                // line 56
                echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 56, $this->getSourceContext()); })()), "method", array())) ? ((" :: " . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["controller"]) || arraykeyexists("controller", $context) ? $context["controller"] : (function () { throw new TwigErrorRuntime('Variable "controller" does not exist.', 56, $this->getSourceContext()); })()), "method", array()))) : ("")), "html", null, true);
                // line 57
                if ((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 57, $this->getSourceContext()); })())) {
                    echo "</a>";
                }
                // line 58
                echo "                                    (<a href=\"";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["forward"]) || arraykeyexists("forward", $context) ? $context["forward"] : (function () { throw new TwigErrorRuntime('Variable "forward" does not exist.', 58, $this->getSourceContext()); })()), "token", array()))), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["forward"]) || arraykeyexists("forward", $context) ? $context["forward"] : (function () { throw new TwigErrorRuntime('Variable "forward" does not exist.', 58, $this->getSourceContext()); })()), "token", array()), "html", null, true);
                echo "</a>)
                                </dd>
                            </dl>";
            }
            // line 62
            echo "
                        <dl class=\"metadata\">
                            <dt>Method</dt>
                            <dd>";
            // line 65
            echo twigescapefilter($this->env, twigupperfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 65, $this->getSourceContext()); })()), "method", array())), "html", null, true);
            echo "</dd>

                            <dt>HTTP Status</dt>
                            <dd>";
            // line 68
            echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 68, $this->getSourceContext()); })()), "html", null, true);
            echo "</dd>

                            <dt>IP</dt>
                            <dd>
                                <a href=\"";
            // line 72
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearchresults", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 72, $this->getSourceContext()); })()), "limit" => 10, "ip" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 72, $this->getSourceContext()); })()), "ip", array()))), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 72, $this->getSourceContext()); })()), "ip", array()), "html", null, true);
            echo "</a>
                            </dd>

                            <dt>Profiled on</dt>
                            <dd>";
            // line 76
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 76, $this->getSourceContext()); })()), "time", array()), "r"), "html", null, true);
            echo "</dd>

                            <dt>Token</dt>
                            <dd>";
            // line 79
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["profile"]) || arraykeyexists("profile", $context) ? $context["profile"] : (function () { throw new TwigErrorRuntime('Variable "profile" does not exist.', 79, $this->getSourceContext()); })()), "token", array()), "html", null, true);
            echo "</dd>
                        </dl>
                    </div>
                </div>
            ";
        }
        // line 84
        echo "        ";
        
        $internalf2b76d9a1db5d6e58dd306e86d0690cb915b60c6d8fe85691606785c2a2fa5a5->leave($internalf2b76d9a1db5d6e58dd306e86d0690cb915b60c6d8fe85691606785c2a2fa5a5prof);

        
        $internal0d98320e7a923fac4e2c20ff1a6b618fc18f56e1d0a380545cd18d9d0eca1f80->leave($internal0d98320e7a923fac4e2c20ff1a6b618fc18f56e1d0a380545cd18d9d0eca1f80prof);

    }

    // line 130
    public function blockpanel($context, array $blocks = array())
    {
        $internal4a71c547a087120aa19e806025ed0442cb0694d9ed6038e7b025710f7f25ab5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4a71c547a087120aa19e806025ed0442cb0694d9ed6038e7b025710f7f25ab5d->enter($internal4a71c547a087120aa19e806025ed0442cb0694d9ed6038e7b025710f7f25ab5dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internalbaa6c23945080a12fdff5e2ae95da5dafed07a49d370f283e641316bd289f1a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalbaa6c23945080a12fdff5e2ae95da5dafed07a49d370f283e641316bd289f1a7->enter($internalbaa6c23945080a12fdff5e2ae95da5dafed07a49d370f283e641316bd289f1a7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $internalbaa6c23945080a12fdff5e2ae95da5dafed07a49d370f283e641316bd289f1a7->leave($internalbaa6c23945080a12fdff5e2ae95da5dafed07a49d370f283e641316bd289f1a7prof);

        
        $internal4a71c547a087120aa19e806025ed0442cb0694d9ed6038e7b025710f7f25ab5d->leave($internal4a71c547a087120aa19e806025ed0442cb0694d9ed6038e7b025710f7f25ab5dprof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  409 => 130,  399 => 84,  391 => 79,  385 => 76,  376 => 72,  369 => 68,  363 => 65,  358 => 62,  349 => 58,  345 => 57,  343 => 56,  341 => 55,  333 => 54,  331 => 53,  326 => 50,  324 => 49,  322 => 48,  320 => 47,  317 => 46,  308 => 42,  305 => 40,  300 => 38,  298 => 37,  290 => 36,  288 => 35,  286 => 34,  282 => 33,  275 => 29,  271 => 27,  269 => 26,  267 => 25,  265 => 24,  262 => 23,  260 => 22,  256 => 20,  250 => 18,  242 => 16,  240 => 15,  234 => 12,  231 => 11,  228 => 10,  225 => 9,  222 => 8,  213 => 7,  191 => 131,  189 => 130,  185 => 129,  179 => 125,  175 => 123,  161 => 122,  153 => 119,  146 => 118,  143 => 117,  138 => 113,  131 => 112,  129 => 111,  126 => 110,  109 => 109,  106 => 108,  104 => 107,  97 => 103,  91 => 100,  85 => 99,  80 => 97,  76 => 96,  70 => 93,  60 => 85,  58 => 7,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block body %}
    {{ include('@WebProfiler/Profiler/header.html.twig', withcontext = false) }}

    <div id=\"summary\">
        {% block summary %}
            {% if profile is defined %}
                {% set statuscode = ('request' in profile.collectors|keys) ? profile.getcollector('request').statuscode|default(0) : 0 %}
                {% set cssclass = statuscode > 399 ? 'status-error' : statuscode > 299 ? 'status-warning' : 'status-success' %}

                <div class=\"status {{ cssclass }}\">
                    <div class=\"container\">
                        <h2 class=\"break-long-words\">
                            {% if profile.method|upper in ['GET', 'HEAD'] %}
                                <a href=\"{{ profile.url }}\">{{ profile.url }}</a>
                            {% else %}
                                {{ profile.url }}
                            {% endif %}
                        </h2>

                        {% set requestcollector = profile.collectors.request|default(false) %}
                        {% if requestcollector is defined and requestcollector.redirect -%}
                            {%- set redirect = requestcollector.redirect -%}
                            {%- set controller = redirect.controller -%}
                            {%- set redirectroute = '@' ~ redirect.route %}
                            <dl class=\"metadata\">
                                <dt>
                                    <span class=\"label\">{{ redirect.statuscode }}</span>
                                    Redirect from
                                </dt>
                                <dd>
                                    {{ 'GET' != redirect.method ? redirect.method }}
                                    {% if redirect.controller.class is defined -%}
                                        {%- set link = controller.file|filelink(controller.line) -%}
                                        {% if link %}<a href=\"{{ link }}\" title=\"{{ controller.file }}\">{% endif -%}
                                            {{ redirectroute }}
                                        {%- if link %}</a>{% endif -%}
                                    {%- else -%}
                                            {{ redirectroute }}
                                    {%- endif %}
                                    (<a href=\"{{ path('profiler', { token: redirect.token, panel: request.query.get('panel', 'request') }) }}\">{{ redirect.token }}</a>)
                                </dd>
                            </dl>
                        {%- endif %}

                        {% if requestcollector and requestcollector.forward|default(false) and requestcollector.forward.controller.class is defined -%}
                            {%- set forward = requestcollector.forward -%}
                            {%- set controller = forward.controller -%}
                            <dl class=\"metadata\">
                                <dt>Forwarded to</dt>
                                <dd>
                                    {% set link = controller.file|filelink(controller.line) -%}
                                    {%- if link %}<a href=\"{{ link }}\" title=\"{{ controller.file }}\">{% endif -%}
                                        {{- controller.class|abbrclass|striptags -}}
                                        {{- controller.method ? ' :: ' ~ controller.method }}
                                    {%- if link %}</a>{% endif %}
                                    (<a href=\"{{ path('profiler', { token: forward.token }) }}\">{{ forward.token }}</a>)
                                </dd>
                            </dl>
                        {%- endif %}

                        <dl class=\"metadata\">
                            <dt>Method</dt>
                            <dd>{{ profile.method|upper }}</dd>

                            <dt>HTTP Status</dt>
                            <dd>{{ statuscode }}</dd>

                            <dt>IP</dt>
                            <dd>
                                <a href=\"{{ path('profilersearchresults', { token: token, limit: 10, ip: profile.ip }) }}\">{{ profile.ip }}</a>
                            </dd>

                            <dt>Profiled on</dt>
                            <dd>{{ profile.time|date('r') }}</dd>

                            <dt>Token</dt>
                            <dd>{{ profile.token }}</dd>
                        </dl>
                    </div>
                </div>
            {% endif %}
        {% endblock %}
    </div>

    <div id=\"content\" class=\"container\">
        <div id=\"main\">
            <div id=\"sidebar\">
                <div id=\"sidebar-shortcuts\">
                    <div class=\"shortcuts\">
                        <a href=\"#\" id=\"sidebarShortcutsMenu\" class=\"visible-small\">
                            <span class=\"icon\">{{ include('@WebProfiler/Icon/menu.svg') }}</span>
                        </a>

                        <a class=\"btn btn-sm\" href=\"{{ path('profilersearch', { limit: 10 }) }}\">Last 10</a>
                        <a class=\"btn btn-sm\" href=\"{{ path('profiler', { token: 'latest' }|merge(request.query.all)) }}\">Latest</a>

                        <a class=\"sf-toggle btn btn-sm\" data-toggle-selector=\"#sidebar-search\" {% if tokens is defined or about is defined %}data-toggle-initial=\"display\"{% endif %}>
                            {{ include('@WebProfiler/Icon/search.svg') }} <span class=\"hidden-small\">Search</span>
                        </a>

                        {{ render(path('profilersearchbar', request.query.all)) }}
                    </div>
                </div>

                {% if templates is defined %}
                    <ul id=\"menu-profiler\">
                        {% for name, template in templates %}
                            {% set menu -%}
                                {%- if block('menu', template) is defined -%}
                                    {% with { collector: profile.getcollector(name), profilermarkupversion: profilermarkupversion } %}
                                        {{- block('menu', template) -}}
                                    {% endwith %}
                                {%- endif -%}
                            {%- endset %}
                            {% if menu is not empty %}
                                <li class=\"{{ name }} {{ name == panel ? 'selected' : '' }}\">
                                    <a href=\"{{ path('profiler', { token: token, panel: name }) }}\">{{ menu|raw }}</a>
                                </li>
                            {% endif %}
                        {% endfor %}
                    </ul>
                {% endif %}
            </div>

            <div id=\"collector-wrapper\">
                <div id=\"collector-content\">
                    {{ include('@WebProfiler/Profiler/basejs.html.twig') }}
                    {% block panel '' %}
                </div>
            </div>
        </div>
    </div>
    <script>
        (function () {
            Sfjs.addEventListener(document.getElementById('sidebarShortcutsMenu'), 'click', function (event) {
                event.preventDefault();
                Sfjs.toggleClass(document.getElementById('sidebar'), 'expanded');
            })
        }())
    </script>
{% endblock %}
", "@WebProfiler/Profiler/layout.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/layout.html.twig");
    }
}
