<?php

/* WebProfilerBundle:Collector:memory.html.twig */
class TwigTemplate38e04fd0fde5fcf7c8d8dbe5f8978388610a689b294a219de1de41221522efd4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:memory.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal7fe8a03f5fb20aa3227e063a49e874ee0ad929708c456c8ac6b94b36432a0ef8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7fe8a03f5fb20aa3227e063a49e874ee0ad929708c456c8ac6b94b36432a0ef8->enter($internal7fe8a03f5fb20aa3227e063a49e874ee0ad929708c456c8ac6b94b36432a0ef8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:memory.html.twig"));

        $internal0c2a8dcc8e6366f248ba566c43e8d364f007d8eef8bffa5c7aecbff7ca4c0301 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0c2a8dcc8e6366f248ba566c43e8d364f007d8eef8bffa5c7aecbff7ca4c0301->enter($internal0c2a8dcc8e6366f248ba566c43e8d364f007d8eef8bffa5c7aecbff7ca4c0301prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:memory.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal7fe8a03f5fb20aa3227e063a49e874ee0ad929708c456c8ac6b94b36432a0ef8->leave($internal7fe8a03f5fb20aa3227e063a49e874ee0ad929708c456c8ac6b94b36432a0ef8prof);

        
        $internal0c2a8dcc8e6366f248ba566c43e8d364f007d8eef8bffa5c7aecbff7ca4c0301->leave($internal0c2a8dcc8e6366f248ba566c43e8d364f007d8eef8bffa5c7aecbff7ca4c0301prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal971404742a503cc939f728af96bb8f5c17a8de4972d416469aeb9efb3c9d82bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal971404742a503cc939f728af96bb8f5c17a8de4972d416469aeb9efb3c9d82bf->enter($internal971404742a503cc939f728af96bb8f5c17a8de4972d416469aeb9efb3c9d82bfprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internal7739bad9958860d7252f92a84776e69aff20c9300c27bddd929ad956546fc8dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7739bad9958860d7252f92a84776e69aff20c9300c27bddd929ad956546fc8dc->enter($internal7739bad9958860d7252f92a84776e69aff20c9300c27bddd929ad956546fc8dcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        obstart();
        // line 5
        echo "        ";
        $context["statuscolor"] = (((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 5, $this->getSourceContext()); })()), "memory", array()) / 1024) / 1024) > 50)) ? ("yellow") : (""));
        // line 6
        echo "        ";
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/memory.svg");
        echo "
        <span class=\"sf-toolbar-value\">";
        // line 7
        echo twigescapefilter($this->env, sprintf("%.1f", ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 7, $this->getSourceContext()); })()), "memory", array()) / 1024) / 1024)), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-label\">MB</span>
    ";
        $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 10
        echo "
    ";
        // line 11
        obstart();
        // line 12
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Peak memory usage</b>
            <span>";
        // line 14
        echo twigescapefilter($this->env, sprintf("%.1f", ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 14, $this->getSourceContext()); })()), "memory", array()) / 1024) / 1024)), "html", null, true);
        echo " MB</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>PHP memory limit</b>
            <span>";
        // line 19
        echo twigescapefilter($this->env, (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 19, $this->getSourceContext()); })()), "memoryLimit", array()) ==  -1)) ? ("Unlimited") : (sprintf("%.0f MB", ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 19, $this->getSourceContext()); })()), "memoryLimit", array()) / 1024) / 1024)))), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        // line 22
        echo "
    ";
        // line 23
        echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 23, $this->getSourceContext()); })()), "name" => "time", "status" => (isset($context["statuscolor"]) || arraykeyexists("statuscolor", $context) ? $context["statuscolor"] : (function () { throw new TwigErrorRuntime('Variable "statuscolor" does not exist.', 23, $this->getSourceContext()); })())));
        echo "
";
        
        $internal7739bad9958860d7252f92a84776e69aff20c9300c27bddd929ad956546fc8dc->leave($internal7739bad9958860d7252f92a84776e69aff20c9300c27bddd929ad956546fc8dcprof);

        
        $internal971404742a503cc939f728af96bb8f5c17a8de4972d416469aeb9efb3c9d82bf->leave($internal971404742a503cc939f728af96bb8f5c17a8de4972d416469aeb9efb3c9d82bfprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:memory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 23,  89 => 22,  83 => 19,  75 => 14,  71 => 12,  69 => 11,  66 => 10,  60 => 7,  55 => 6,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {% set statuscolor = (collector.memory / 1024 / 1024) > 50 ? 'yellow' : '' %}
        {{ include('@WebProfiler/Icon/memory.svg') }}
        <span class=\"sf-toolbar-value\">{{ '%.1f'|format(collector.memory / 1024 / 1024) }}</span>
        <span class=\"sf-toolbar-label\">MB</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b>Peak memory usage</b>
            <span>{{ '%.1f'|format(collector.memory / 1024 / 1024) }} MB</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>PHP memory limit</b>
            <span>{{ collector.memoryLimit == -1 ? 'Unlimited' : '%.0f MB'|format(collector.memoryLimit / 1024 / 1024) }}</span>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { link: profilerurl, name: 'time', status: statuscolor }) }}
{% endblock %}
", "WebProfilerBundle:Collector:memory.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/memory.html.twig");
    }
}
