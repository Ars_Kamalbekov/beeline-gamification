<?php

/* SonataAdminBundle:CRUD:listtrans.html.twig */
class TwigTemplateac3c725896af35c70e2b8c3836392e573e4cbe429fe96da24d8ae55f36dac5e5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listtrans.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal98f4857573db5a2028f0a692503c1d722d65ed7bf0752d4c944318f0e57f0382 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal98f4857573db5a2028f0a692503c1d722d65ed7bf0752d4c944318f0e57f0382->enter($internal98f4857573db5a2028f0a692503c1d722d65ed7bf0752d4c944318f0e57f0382prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listtrans.html.twig"));

        $internalae08f09088f803e3dd7f97b4ccc93a58185f6ac6abe969812be50c1a9a17d10e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalae08f09088f803e3dd7f97b4ccc93a58185f6ac6abe969812be50c1a9a17d10e->enter($internalae08f09088f803e3dd7f97b4ccc93a58185f6ac6abe969812be50c1a9a17d10eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listtrans.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal98f4857573db5a2028f0a692503c1d722d65ed7bf0752d4c944318f0e57f0382->leave($internal98f4857573db5a2028f0a692503c1d722d65ed7bf0752d4c944318f0e57f0382prof);

        
        $internalae08f09088f803e3dd7f97b4ccc93a58185f6ac6abe969812be50c1a9a17d10e->leave($internalae08f09088f803e3dd7f97b4ccc93a58185f6ac6abe969812be50c1a9a17d10eprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal0606518a2c24b9296d825a60617ff69d04c95bf482ddfbb0d7c54d87e6e871aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0606518a2c24b9296d825a60617ff69d04c95bf482ddfbb0d7c54d87e6e871aa->enter($internal0606518a2c24b9296d825a60617ff69d04c95bf482ddfbb0d7c54d87e6e871aaprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal01ab21fd83b82b9ea55e42b7fc234f57438a4952c4bb88f84e917661cf174a35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal01ab21fd83b82b9ea55e42b7fc234f57438a4952c4bb88f84e917661cf174a35->enter($internal01ab21fd83b82b9ea55e42b7fc234f57438a4952c4bb88f84e917661cf174a35prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["translationDomain"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array()), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "translationDomain", array()))) : (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "translationDomain", array())));
        // line 16
        echo "    ";
        $context["valueFormat"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array()), "%s")) : ("%s"));
        // line 17
        echo "
    ";
        // line 18
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(sprintf((isset($context["valueFormat"]) || arraykeyexists("valueFormat", $context) ? $context["valueFormat"] : (function () { throw new TwigErrorRuntime('Variable "valueFormat" does not exist.', 18, $this->getSourceContext()); })()), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })())), array(), (isset($context["translationDomain"]) || arraykeyexists("translationDomain", $context) ? $context["translationDomain"] : (function () { throw new TwigErrorRuntime('Variable "translationDomain" does not exist.', 18, $this->getSourceContext()); })())), "html", null, true);
        echo "
";
        
        $internal01ab21fd83b82b9ea55e42b7fc234f57438a4952c4bb88f84e917661cf174a35->leave($internal01ab21fd83b82b9ea55e42b7fc234f57438a4952c4bb88f84e917661cf174a35prof);

        
        $internal0606518a2c24b9296d825a60617ff69d04c95bf482ddfbb0d7c54d87e6e871aa->leave($internal0606518a2c24b9296d825a60617ff69d04c95bf482ddfbb0d7c54d87e6e871aaprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listtrans.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 18,  54 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field%}
    {% set translationDomain = fielddescription.options.catalogue|default(admin.translationDomain) %}
    {% set valueFormat = fielddescription.options.format|default('%s') %}

    {{valueFormat|format(value)|trans({}, translationDomain)}}
{% endblock %}
", "SonataAdminBundle:CRUD:listtrans.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listtrans.html.twig");
    }
}
