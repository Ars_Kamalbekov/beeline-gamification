<?php

/* @Framework/Form/moneywidget.html.php */
class TwigTemplatee6dd6e524107f7ff2baddec710dd2ceef33a465c0ac8e9ce51f0f26d7a4b01e8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3e113935a13a6daa45f4622276587e1a45b7016d998146adeceff98397dbfa06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3e113935a13a6daa45f4622276587e1a45b7016d998146adeceff98397dbfa06->enter($internal3e113935a13a6daa45f4622276587e1a45b7016d998146adeceff98397dbfa06prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/moneywidget.html.php"));

        $internal2f9bad1ba73f9829755dc22bb63a15f165e71e264cf9c2d12a566dae12a52240 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2f9bad1ba73f9829755dc22bb63a15f165e71e264cf9c2d12a566dae12a52240->enter($internal2f9bad1ba73f9829755dc22bb63a15f165e71e264cf9c2d12a566dae12a52240prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/moneywidget.html.php"));

        // line 1
        echo "<?php echo strreplace('";
        echo twigescapefilter($this->env, (isset($context["widget"]) || arraykeyexists("widget", $context) ? $context["widget"] : (function () { throw new TwigErrorRuntime('Variable "widget" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "', \$view['form']->block(\$form, 'formwidgetsimple'), \$moneypattern) ?>
";
        
        $internal3e113935a13a6daa45f4622276587e1a45b7016d998146adeceff98397dbfa06->leave($internal3e113935a13a6daa45f4622276587e1a45b7016d998146adeceff98397dbfa06prof);

        
        $internal2f9bad1ba73f9829755dc22bb63a15f165e71e264cf9c2d12a566dae12a52240->leave($internal2f9bad1ba73f9829755dc22bb63a15f165e71e264cf9c2d12a566dae12a52240prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/moneywidget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo strreplace('{{ widget }}', \$view['form']->block(\$form, 'formwidgetsimple'), \$moneypattern) ?>
", "@Framework/Form/moneywidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/moneywidget.html.php");
    }
}
