<?php

/* @Framework/Form/timewidget.html.php */
class TwigTemplate57b4abb320137f4211188fcae63063f029ff21f533592df3c200b964f61d52c9 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal291ffae64b079e62200772e381e81864809a2aca997cd7368d01abcf918a4b5f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal291ffae64b079e62200772e381e81864809a2aca997cd7368d01abcf918a4b5f->enter($internal291ffae64b079e62200772e381e81864809a2aca997cd7368d01abcf918a4b5fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/timewidget.html.php"));

        $internal97c756c95edacaa052f49bdd2cc2c0d047e41e88eceacf5829885aa37919f847 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal97c756c95edacaa052f49bdd2cc2c0d047e41e88eceacf5829885aa37919f847->enter($internal97c756c95edacaa052f49bdd2cc2c0d047e41e88eceacf5829885aa37919f847prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/timewidget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <?php \$vars = \$widget == 'text' ? array('attr' => array('size' => 1)) : array() ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php
            // There should be no spaces between the colons and the widgets, that's why
            // this block is written in a single PHP tag
            echo \$view['form']->widget(\$form['hour'], \$vars);

            if (\$withminutes) {
                echo ':';
                echo \$view['form']->widget(\$form['minute'], \$vars);
            }

            if (\$withseconds) {
                echo ':';
                echo \$view['form']->widget(\$form['second'], \$vars);
            }
        ?>
    </div>
<?php endif ?>
";
        
        $internal291ffae64b079e62200772e381e81864809a2aca997cd7368d01abcf918a4b5f->leave($internal291ffae64b079e62200772e381e81864809a2aca997cd7368d01abcf918a4b5fprof);

        
        $internal97c756c95edacaa052f49bdd2cc2c0d047e41e88eceacf5829885aa37919f847->leave($internal97c756c95edacaa052f49bdd2cc2c0d047e41e88eceacf5829885aa37919f847prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/timewidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$widget == 'singletext'): ?>
    <?php echo \$view['form']->block(\$form, 'formwidgetsimple'); ?>
<?php else: ?>
    <?php \$vars = \$widget == 'text' ? array('attr' => array('size' => 1)) : array() ?>
    <div <?php echo \$view['form']->block(\$form, 'widgetcontainerattributes') ?>>
        <?php
            // There should be no spaces between the colons and the widgets, that's why
            // this block is written in a single PHP tag
            echo \$view['form']->widget(\$form['hour'], \$vars);

            if (\$withminutes) {
                echo ':';
                echo \$view['form']->widget(\$form['minute'], \$vars);
            }

            if (\$withseconds) {
                echo ':';
                echo \$view['form']->widget(\$form['second'], \$vars);
            }
        ?>
    </div>
<?php endif ?>
", "@Framework/Form/timewidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/timewidget.html.php");
    }
}
