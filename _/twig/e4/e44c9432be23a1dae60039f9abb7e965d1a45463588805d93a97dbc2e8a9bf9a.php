<?php

/* @Framework/Form/form.html.php */
class TwigTemplate7427b108560f72aa8b31c309058380b12663792f3ef4f89f41920ff1d40752b8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1c03fdd9833ab2fc607f79a13a4cc41aa0a10135e67e92bece6499604b92df27 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1c03fdd9833ab2fc607f79a13a4cc41aa0a10135e67e92bece6499604b92df27->enter($internal1c03fdd9833ab2fc607f79a13a4cc41aa0a10135e67e92bece6499604b92df27prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $internalfe42218209dba4316fa1ccf13f89e7a528a6f92c3fbc3ed2c20e63a480f00041 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalfe42218209dba4316fa1ccf13f89e7a528a6f92c3fbc3ed2c20e63a480f00041->enter($internalfe42218209dba4316fa1ccf13f89e7a528a6f92c3fbc3ed2c20e63a480f00041prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $internal1c03fdd9833ab2fc607f79a13a4cc41aa0a10135e67e92bece6499604b92df27->leave($internal1c03fdd9833ab2fc607f79a13a4cc41aa0a10135e67e92bece6499604b92df27prof);

        
        $internalfe42218209dba4316fa1ccf13f89e7a528a6f92c3fbc3ed2c20e63a480f00041->leave($internalfe42218209dba4316fa1ccf13f89e7a528a6f92c3fbc3ed2c20e63a480f00041prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
