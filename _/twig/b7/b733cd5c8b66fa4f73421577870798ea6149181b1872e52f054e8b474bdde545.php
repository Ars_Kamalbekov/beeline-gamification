<?php

/* SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig */
class TwigTemplateaa8ec1e348f38044a0c2cf1eeb4c9a8f213aeb3167870100fdcd986cad22d7f4 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal166149002341e91beef980e4f3dff39b09e6751a6fc8b8fe13af68f803ad84d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal166149002341e91beef980e4f3dff39b09e6751a6fc8b8fe13af68f803ad84d1->enter($internal166149002341e91beef980e4f3dff39b09e6751a6fc8b8fe13af68f803ad84d1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig"));

        $internalabc8b658dace66fe6cb98d1d912b784a1f97f466945bf8a838ec59266acc1e97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalabc8b658dace66fe6cb98d1d912b784a1f97f466945bf8a838ec59266acc1e97->enter($internalabc8b658dace66fe6cb98d1d912b784a1f97f466945bf8a838ec59266acc1e97prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal166149002341e91beef980e4f3dff39b09e6751a6fc8b8fe13af68f803ad84d1->leave($internal166149002341e91beef980e4f3dff39b09e6751a6fc8b8fe13af68f803ad84d1prof);

        
        $internalabc8b658dace66fe6cb98d1d912b784a1f97f466945bf8a838ec59266acc1e97->leave($internalabc8b658dace66fe6cb98d1d912b784a1f97f466945bf8a838ec59266acc1e97prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal306f80e57965fd37888b448666e594c67bd24c4cef0c3c350df8ef7dc6a96abd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal306f80e57965fd37888b448666e594c67bd24c4cef0c3c350df8ef7dc6a96abd->enter($internal306f80e57965fd37888b448666e594c67bd24c4cef0c3c350df8ef7dc6a96abdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal5f2d3ab105775a037252177f61b3bdb4af3a040be2e2c669f17c3105f7bc1e54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5f2d3ab105775a037252177f61b3bdb4af3a040be2e2c669f17c3105f7bc1e54->enter($internal5f2d3ab105775a037252177f61b3bdb4af3a040be2e2c669f17c3105f7bc1e54prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasAssociationAdmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "associationadmin", array()), "id", array(0 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 18
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())), "method"))) {
            // line 20
            echo "        <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
            echo "\">
            ";
            // line 21
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })())), "html", null, true);
            echo "
        </a>
    ";
        } else {
            // line 24
            echo "        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 24, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 24, $this->getSourceContext()); })())), "html", null, true);
            echo "
    ";
        }
        
        $internal5f2d3ab105775a037252177f61b3bdb4af3a040be2e2c669f17c3105f7bc1e54->leave($internal5f2d3ab105775a037252177f61b3bdb4af3a040be2e2c669f17c3105f7bc1e54prof);

        
        $internal306f80e57965fd37888b448666e594c67bd24c4cef0c3c350df8ef7dc6a96abd->leave($internal306f80e57965fd37888b448666e594c67bd24c4cef0c3c350df8ef7dc6a96abdprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 24,  63 => 21,  58 => 20,  56 => 19,  55 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasAssociationAdmin
    and fielddescription.associationadmin.id(value)
    and fielddescription.associationadmin.hasRoute(routename)
    and fielddescription.associationadmin.hasAccess(routename, value) %}
        <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, value, fielddescription.options.route.parameters) }}\">
            {{ value|renderrelationelement(fielddescription) }}
        </a>
    {% else %}
        {{ value|renderrelationelement(fielddescription) }}
    {% endif %}
{% endblock %}
", "SonataDoctrineORMAdminBundle:CRUD:showormonetoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/showormonetoone.html.twig");
    }
}
