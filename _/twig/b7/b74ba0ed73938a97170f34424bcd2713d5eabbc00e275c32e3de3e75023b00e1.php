<?php

/* TwigBundle:Exception:tracestext.html.twig */
class TwigTemplateed769ed653f5546e11211192a18c7bade779f22b668a4c7125d5c2118cd2d048 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3a89032ca1a86210917bde2a0517417b2392596fd69f5f08d52d03cdf48e9d55 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3a89032ca1a86210917bde2a0517417b2392596fd69f5f08d52d03cdf48e9d55->enter($internal3a89032ca1a86210917bde2a0517417b2392596fd69f5f08d52d03cdf48e9d55prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:tracestext.html.twig"));

        $internal0c4e9642a4baa6b49914567998c2cc66fdb272181c84f78a910fa43eed8e0019 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0c4e9642a4baa6b49914567998c2cc66fdb272181c84f78a910fa43eed8e0019->enter($internal0c4e9642a4baa6b49914567998c2cc66fdb272181c84f78a910fa43eed8e0019prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:tracestext.html.twig"));

        // line 1
        echo "<table class=\"trace trace-as-text\">
    <thead class=\"trace-head\">
        <tr>
            <th class=\"sf-toggle\" data-toggle-selector=\"#trace-text-";
        // line 4
        echo twigescapefilter($this->env, (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 4, $this->getSourceContext()); })()), "html", null, true);
        echo "\" data-toggle-initial=\"";
        echo (((1 == (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 4, $this->getSourceContext()); })()))) ? ("display") : (""));
        echo "\">
                <h3 class=\"trace-class\">
                    ";
        // line 6
        if (((isset($context["numexceptions"]) || arraykeyexists("numexceptions", $context) ? $context["numexceptions"] : (function () { throw new TwigErrorRuntime('Variable "numexceptions" does not exist.', 6, $this->getSourceContext()); })()) > 1)) {
            // line 7
            echo "                        <span class=\"text-muted\">[";
            echo twigescapefilter($this->env, (((isset($context["numexceptions"]) || arraykeyexists("numexceptions", $context) ? $context["numexceptions"] : (function () { throw new TwigErrorRuntime('Variable "numexceptions" does not exist.', 7, $this->getSourceContext()); })()) - (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 7, $this->getSourceContext()); })())) + 1), "html", null, true);
            echo "/";
            echo twigescapefilter($this->env, (isset($context["numexceptions"]) || arraykeyexists("numexceptions", $context) ? $context["numexceptions"] : (function () { throw new TwigErrorRuntime('Variable "numexceptions" does not exist.', 7, $this->getSourceContext()); })()), "html", null, true);
            echo "]</span>
                    ";
        }
        // line 9
        echo "                    ";
        echo twigescapefilter($this->env, twiglast($this->env, twigsplitfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 9, $this->getSourceContext()); })()), "class", array()), "\\")), "html", null, true);
        echo "
                    <span class=\"icon icon-close\">";
        // line 10
        echo twiginclude($this->env, $context, "@Twig/images/icon-minus-square-o.svg");
        echo "</span>
                    <span class=\"icon icon-open\">";
        // line 11
        echo twiginclude($this->env, $context, "@Twig/images/icon-plus-square-o.svg");
        echo "</span>
                </h3>
            </th>
        </tr>
    </thead>

    <tbody id=\"trace-text-";
        // line 17
        echo twigescapefilter($this->env, (isset($context["index"]) || arraykeyexists("index", $context) ? $context["index"] : (function () { throw new TwigErrorRuntime('Variable "index" does not exist.', 17, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        <tr>
            <td>
                ";
        // line 20
        echo twiginclude($this->env, $context, "@Twig/Exception/traces.txt.twig", array("exception" => (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 20, $this->getSourceContext()); })())), false);
        echo "
            </td>
        </tr>
    </tbody>
</table>
";
        
        $internal3a89032ca1a86210917bde2a0517417b2392596fd69f5f08d52d03cdf48e9d55->leave($internal3a89032ca1a86210917bde2a0517417b2392596fd69f5f08d52d03cdf48e9d55prof);

        
        $internal0c4e9642a4baa6b49914567998c2cc66fdb272181c84f78a910fa43eed8e0019->leave($internal0c4e9642a4baa6b49914567998c2cc66fdb272181c84f78a910fa43eed8e0019prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:tracestext.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 20,  65 => 17,  56 => 11,  52 => 10,  47 => 9,  39 => 7,  37 => 6,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<table class=\"trace trace-as-text\">
    <thead class=\"trace-head\">
        <tr>
            <th class=\"sf-toggle\" data-toggle-selector=\"#trace-text-{{ index }}\" data-toggle-initial=\"{{ 1 == index ? 'display' }}\">
                <h3 class=\"trace-class\">
                    {% if numexceptions > 1 %}
                        <span class=\"text-muted\">[{{ numexceptions - index + 1 }}/{{ numexceptions }}]</span>
                    {% endif %}
                    {{ exception.class|split('\\\\')|last }}
                    <span class=\"icon icon-close\">{{ include('@Twig/images/icon-minus-square-o.svg') }}</span>
                    <span class=\"icon icon-open\">{{ include('@Twig/images/icon-plus-square-o.svg') }}</span>
                </h3>
            </th>
        </tr>
    </thead>

    <tbody id=\"trace-text-{{ index }}\">
        <tr>
            <td>
                {{ include('@Twig/Exception/traces.txt.twig', { exception: exception }, withcontext = false) }}
            </td>
        </tr>
    </tbody>
</table>
", "TwigBundle:Exception:tracestext.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/tracestext.html.twig");
    }
}
