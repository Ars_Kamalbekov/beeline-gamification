<?php

/* :basic:index.html.twig */
class TwigTemplate9afa96e820320b1a7462bda453e5090419dba2d7744f34ca0e2718e25603029e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":basic:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'blockbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbcf47e62a289bd0a92751593e247d6b9fd3ef9a15bf08e6efd0e88d0578d92df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbcf47e62a289bd0a92751593e247d6b9fd3ef9a15bf08e6efd0e88d0578d92df->enter($internalbcf47e62a289bd0a92751593e247d6b9fd3ef9a15bf08e6efd0e88d0578d92dfprof = new TwigProfilerProfile($this->getTemplateName(), "template", ":basic:index.html.twig"));

        $internalf6b3fa24e1fade45a59c31081a5fb81b4af2cc9f988a63704778952e03895f7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf6b3fa24e1fade45a59c31081a5fb81b4af2cc9f988a63704778952e03895f7d->enter($internalf6b3fa24e1fade45a59c31081a5fb81b4af2cc9f988a63704778952e03895f7dprof = new TwigProfilerProfile($this->getTemplateName(), "template", ":basic:index.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalbcf47e62a289bd0a92751593e247d6b9fd3ef9a15bf08e6efd0e88d0578d92df->leave($internalbcf47e62a289bd0a92751593e247d6b9fd3ef9a15bf08e6efd0e88d0578d92dfprof);

        
        $internalf6b3fa24e1fade45a59c31081a5fb81b4af2cc9f988a63704778952e03895f7d->leave($internalf6b3fa24e1fade45a59c31081a5fb81b4af2cc9f988a63704778952e03895f7dprof);

    }

    // line 2
    public function blockbody($context, array $blocks = array())
    {
        $internalc6d224a4230490cfd4d1957a6fa546c1fc5fdc270279b9f7a0f594983b98e185 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc6d224a4230490cfd4d1957a6fa546c1fc5fdc270279b9f7a0f594983b98e185->enter($internalc6d224a4230490cfd4d1957a6fa546c1fc5fdc270279b9f7a0f594983b98e185prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internal29e008c93188e195853a65edbdcebf0bc2d186a5dd61a9e27210fe01d47fa4f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal29e008c93188e195853a65edbdcebf0bc2d186a5dd61a9e27210fe01d47fa4f8->enter($internal29e008c93188e195853a65edbdcebf0bc2d186a5dd61a9e27210fe01d47fa4f8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "    <div class=\"row\">
        <ul class=\"nav nav-tabs\">
            <li><a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserregistrationregister");
        echo "\">Регистрация</a></li>
            <li><a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusersecuritylogin");
        echo "\">Авторизация</a></li>
        </ul>
    </div>
    <div class=\"jumbotron\">
        <h1>Hello World</h1>
        <p>This is main page for beeline-gamification project</p>
    </div>
";
        
        $internal29e008c93188e195853a65edbdcebf0bc2d186a5dd61a9e27210fe01d47fa4f8->leave($internal29e008c93188e195853a65edbdcebf0bc2d186a5dd61a9e27210fe01d47fa4f8prof);

        
        $internalc6d224a4230490cfd4d1957a6fa546c1fc5fdc270279b9f7a0f594983b98e185->leave($internalc6d224a4230490cfd4d1957a6fa546c1fc5fdc270279b9f7a0f594983b98e185prof);

    }

    public function getTemplateName()
    {
        return ":basic:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 6,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"::base.html.twig\" %}
{% block body %}
    <div class=\"row\">
        <ul class=\"nav nav-tabs\">
            <li><a href=\"{{ path('fosuserregistrationregister') }}\">Регистрация</a></li>
            <li><a href=\"{{ path('fosusersecuritylogin') }}\">Авторизация</a></li>
        </ul>
    </div>
    <div class=\"jumbotron\">
        <h1>Hello World</h1>
        <p>This is main page for beeline-gamification project</p>
    </div>
{% endblock %}", ":basic:index.html.twig", "/var/www/html/beeline-gamification/app/Resources/views/basic/index.html.twig");
    }
}
