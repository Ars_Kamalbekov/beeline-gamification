<?php

/* TwigBundle:Exception:traces.xml.twig */
class TwigTemplatebf2f45fd71eb82b47299f77cbf41368a5bffbbbec8565654c1342c8734d698b3 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalafa782b0cc8b435acdd2203ff3037400cfae76826c7e4c71e6fb5b484289722f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalafa782b0cc8b435acdd2203ff3037400cfae76826c7e4c71e6fb5b484289722f->enter($internalafa782b0cc8b435acdd2203ff3037400cfae76826c7e4c71e6fb5b484289722fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $internal9ce4ef8fed6734fe106ce6f90f0dc1fdac4b9dbc3dad6f9a1ec097ae6c194e1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9ce4ef8fed6734fe106ce6f90f0dc1fdac4b9dbc3dad6f9a1ec097ae6c194e1b->enter($internal9ce4ef8fed6734fe106ce6f90f0dc1fdac4b9dbc3dad6f9a1ec097ae6c194e1bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()), "trace", array()));
        foreach ($context['seq'] as $context["key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            echo twiginclude($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
            echo "

            </trace>
";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['trace'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 8
        echo "        </traces>
";
        
        $internalafa782b0cc8b435acdd2203ff3037400cfae76826c7e4c71e6fb5b484289722f->leave($internalafa782b0cc8b435acdd2203ff3037400cfae76826c7e4c71e6fb5b484289722fprof);

        
        $internal9ce4ef8fed6734fe106ce6f90f0dc1fdac4b9dbc3dad6f9a1ec097ae6c194e1b->leave($internal9ce4ef8fed6734fe106ce6f90f0dc1fdac4b9dbc3dad6f9a1ec097ae6c194e1bprof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("        <traces>
{% for trace in exception.trace %}
            <trace>
{{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, withcontext = false) }}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}
