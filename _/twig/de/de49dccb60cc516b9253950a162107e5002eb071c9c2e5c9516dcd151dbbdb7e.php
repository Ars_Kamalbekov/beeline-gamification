<?php

/* TwigBundle:Exception:error.css.twig */
class TwigTemplate8bfded8c4e88ce1b5324d3c94cdb66cc0fcfb85b039cec6b61991d78e8e9e289 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf5c606ec1b7005d817582bdf7f71f38e1bbe2009e696e3b5b5e26b4f9ac6828e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf5c606ec1b7005d817582bdf7f71f38e1bbe2009e696e3b5b5e26b4f9ac6828e->enter($internalf5c606ec1b7005d817582bdf7f71f38e1bbe2009e696e3b5b5e26b4f9ac6828eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $internal4c38be41a5c8087f7c222e4d998cbe195137777619cf9afd1881b86db6bb8c87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4c38be41a5c8087f7c222e4d998cbe195137777619cf9afd1881b86db6bb8c87->enter($internal4c38be41a5c8087f7c222e4d998cbe195137777619cf9afd1881b86db6bb8c87prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo " ";
        echo twigescapefilter($this->env, (isset($context["statustext"]) || arraykeyexists("statustext", $context) ? $context["statustext"] : (function () { throw new TwigErrorRuntime('Variable "statustext" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo "

*/
";
        
        $internalf5c606ec1b7005d817582bdf7f71f38e1bbe2009e696e3b5b5e26b4f9ac6828e->leave($internalf5c606ec1b7005d817582bdf7f71f38e1bbe2009e696e3b5b5e26b4f9ac6828eprof);

        
        $internal4c38be41a5c8087f7c222e4d998cbe195137777619cf9afd1881b86db6bb8c87->leave($internal4c38be41a5c8087f7c222e4d998cbe195137777619cf9afd1881b86db6bb8c87prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("/*
{{ statuscode }} {{ statustext }}

*/
", "TwigBundle:Exception:error.css.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
