<?php

/* SonataAdminBundle:CRUD:listchoice.html.twig */
class TwigTemplatef4e8f2c2a4653839c8d5ad1d830b84e46fb1622cc5ae61cf8ce6ab8f5e3b8f4d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'fieldspanattributes' => array($this, 'blockfieldspanattributes'),
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listchoice.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal099d3e8949d27a071541153b82d48f05cd4d13802d46cb55fcf3662798c500a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal099d3e8949d27a071541153b82d48f05cd4d13802d46cb55fcf3662798c500a1->enter($internal099d3e8949d27a071541153b82d48f05cd4d13802d46cb55fcf3662798c500a1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listchoice.html.twig"));

        $internal850fcd7d743711f8535138a4b3f01403271d4ef40bb6ce51ffa50db8218c8157 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal850fcd7d743711f8535138a4b3f01403271d4ef40bb6ce51ffa50db8218c8157->enter($internal850fcd7d743711f8535138a4b3f01403271d4ef40bb6ce51ffa50db8218c8157prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listchoice.html.twig"));

        // line 14
        $context["iseditable"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 15
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "editable", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 16
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "editable", array())) && twiggetattribute($this->env, $this->getSourceContext(),         // line 17
(isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 17, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 17, $this->getSourceContext()); })())), "method"));
        // line 19
        $context["xeditabletype"] = $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getXEditableType(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "type", array()));
        // line 21
        if (((isset($context["iseditable"]) || arraykeyexists("iseditable", $context) ? $context["iseditable"] : (function () { throw new TwigErrorRuntime('Variable "iseditable" does not exist.', 21, $this->getSourceContext()); })()) && (isset($context["xeditabletype"]) || arraykeyexists("xeditabletype", $context) ? $context["xeditabletype"] : (function () { throw new TwigErrorRuntime('Variable "xeditabletype" does not exist.', 21, $this->getSourceContext()); })()))) {
        }
        // line 12
        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal099d3e8949d27a071541153b82d48f05cd4d13802d46cb55fcf3662798c500a1->leave($internal099d3e8949d27a071541153b82d48f05cd4d13802d46cb55fcf3662798c500a1prof);

        
        $internal850fcd7d743711f8535138a4b3f01403271d4ef40bb6ce51ffa50db8218c8157->leave($internal850fcd7d743711f8535138a4b3f01403271d4ef40bb6ce51ffa50db8218c8157prof);

    }

    // line 22
    public function blockfieldspanattributes($context, array $blocks = array())
    {
        $internal84d23de3ac2d90c18d33b00f3f9ee034a0cb96ccd94c79877859f62955eb962d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal84d23de3ac2d90c18d33b00f3f9ee034a0cb96ccd94c79877859f62955eb962d->enter($internal84d23de3ac2d90c18d33b00f3f9ee034a0cb96ccd94c79877859f62955eb962dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        $internal7e22b0d689bc80d2947d4118d73a3cdc2ce10a14388809ab32481f5b1265ccc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7e22b0d689bc80d2947d4118d73a3cdc2ce10a14388809ab32481f5b1265ccc1->enter($internal7e22b0d689bc80d2947d4118d73a3cdc2ce10a14388809ab32481f5b1265ccc1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fieldspanattributes"));

        // line 23
        echo "        ";
        obstart();
        // line 24
        echo "            ";
        $this->displayParentBlock("fieldspanattributes", $context, $blocks);
        echo "
            data-source=\"";
        // line 25
        echo twigescapefilter($this->env, jsonencode($this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getXEditableChoices((isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 25, $this->getSourceContext()); })()))), "html", null, true);
        echo "\"
        ";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        // line 27
        echo "    ";
        
        $internal7e22b0d689bc80d2947d4118d73a3cdc2ce10a14388809ab32481f5b1265ccc1->leave($internal7e22b0d689bc80d2947d4118d73a3cdc2ce10a14388809ab32481f5b1265ccc1prof);

        
        $internal84d23de3ac2d90c18d33b00f3f9ee034a0cb96ccd94c79877859f62955eb962d->leave($internal84d23de3ac2d90c18d33b00f3f9ee034a0cb96ccd94c79877859f62955eb962dprof);

    }

    // line 30
    public function blockfield($context, array $blocks = array())
    {
        $internal9dbf0b098065766f48286461514fd49b84977984f10052e3bc66393bfb41694b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9dbf0b098065766f48286461514fd49b84977984f10052e3bc66393bfb41694b->enter($internal9dbf0b098065766f48286461514fd49b84977984f10052e3bc66393bfb41694bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal36bc1d3981ebe645e2304980dadd3e0baefff8ba4999fdd2133cfa1ac90c10a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal36bc1d3981ebe645e2304980dadd3e0baefff8ba4999fdd2133cfa1ac90c10a9->enter($internal36bc1d3981ebe645e2304980dadd3e0baefff8ba4999fdd2133cfa1ac90c10a9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 31
        obstart();
        // line 32
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "choices", array(), "any", true, true)) {
            // line 33
            echo "        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "multiple", array(), "any", true, true) && (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 33, $this->getSourceContext()); })()), "options", array()), "multiple", array()) == true)) && twigtestiterable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 33, $this->getSourceContext()); })())))) {
                // line 34
                echo "
            ";
                // line 35
                $context["result"] = "";
                // line 36
                echo "            ";
                $context["delimiter"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "delimiter", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "delimiter", array()), ", ")) : (", "));
                // line 37
                echo "
            ";
                // line 38
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 38, $this->getSourceContext()); })()));
                foreach ($context['seq'] as $context["key"] => $context["val"]) {
                    // line 39
                    echo "                ";
                    if ( !twigtestempty((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 39, $this->getSourceContext()); })()))) {
                        // line 40
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 40, $this->getSourceContext()); })()) . (isset($context["delimiter"]) || arraykeyexists("delimiter", $context) ? $context["delimiter"] : (function () { throw new TwigErrorRuntime('Variable "delimiter" does not exist.', 40, $this->getSourceContext()); })()));
                        // line 41
                        echo "                ";
                    }
                    // line 42
                    echo "
                ";
                    // line 43
                    if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "choices", array(), "any", false, true), $context["val"], array(), "array", true, true)) {
                        // line 44
                        echo "                    ";
                        if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
                            // line 45
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 45, $this->getSourceContext()); })()) . twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 45, $this->getSourceContext()); })()), "options", array()), "choices", array()), $context["val"], array(), "array"));
                            // line 46
                            echo "                    ";
                        } else {
                            // line 47
                            echo "                        ";
                            $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 47, $this->getSourceContext()); })()) . $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 47, $this->getSourceContext()); })()), "options", array()), "choices", array()), $context["val"], array(), "array"), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 47, $this->getSourceContext()); })()), "options", array()), "catalogue", array())));
                            // line 48
                            echo "                    ";
                        }
                        // line 49
                        echo "                ";
                    } else {
                        // line 50
                        echo "                    ";
                        $context["result"] = ((isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 50, $this->getSourceContext()); })()) . $context["val"]);
                        // line 51
                        echo "                ";
                    }
                    // line 52
                    echo "            ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['val'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 53
                echo "
            ";
                // line 54
                $context["value"] = (isset($context["result"]) || arraykeyexists("result", $context) ? $context["result"] : (function () { throw new TwigErrorRuntime('Variable "result" does not exist.', 54, $this->getSourceContext()); })());
                // line 55
                echo "
        ";
            } elseif (twiginfilter(            // line 56
(isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 56, $this->getSourceContext()); })()), twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 56, $this->getSourceContext()); })()), "options", array()), "choices", array())))) {
                // line 57
                echo "            ";
                if ( !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "catalogue", array(), "any", true, true)) {
                    // line 58
                    echo "                ";
                    $context["value"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 58, $this->getSourceContext()); })()), "options", array()), "choices", array()), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 58, $this->getSourceContext()); })()), array(), "array");
                    // line 59
                    echo "            ";
                } else {
                    // line 60
                    echo "                ";
                    $context["value"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 60, $this->getSourceContext()); })()), "options", array()), "choices", array()), (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 60, $this->getSourceContext()); })()), array(), "array"), array(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 60, $this->getSourceContext()); })()), "options", array()), "catalogue", array()));
                    // line 61
                    echo "            ";
                }
                // line 62
                echo "        ";
            }
            // line 63
            echo "    ";
        }
        // line 64
        echo "
    ";
        // line 65
        echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 65, $this->getSourceContext()); })()), "html", null, true);
        echo "
";
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal36bc1d3981ebe645e2304980dadd3e0baefff8ba4999fdd2133cfa1ac90c10a9->leave($internal36bc1d3981ebe645e2304980dadd3e0baefff8ba4999fdd2133cfa1ac90c10a9prof);

        
        $internal9dbf0b098065766f48286461514fd49b84977984f10052e3bc66393bfb41694b->leave($internal9dbf0b098065766f48286461514fd49b84977984f10052e3bc66393bfb41694bprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listchoice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 65,  190 => 64,  187 => 63,  184 => 62,  181 => 61,  178 => 60,  175 => 59,  172 => 58,  169 => 57,  167 => 56,  164 => 55,  162 => 54,  159 => 53,  153 => 52,  150 => 51,  147 => 50,  144 => 49,  141 => 48,  138 => 47,  135 => 46,  132 => 45,  129 => 44,  127 => 43,  124 => 42,  121 => 41,  118 => 40,  115 => 39,  111 => 38,  108 => 37,  105 => 36,  103 => 35,  100 => 34,  97 => 33,  94 => 32,  92 => 31,  83 => 30,  73 => 27,  68 => 25,  63 => 24,  60 => 23,  51 => 22,  41 => 12,  38 => 21,  36 => 19,  34 => 17,  33 => 16,  32 => 15,  31 => 14,  19 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% set iseditable =
    fielddescription.options.editable is defined and
    fielddescription.options.editable and
    admin.hasAccess('edit', object)
%}
{% set xeditabletype = fielddescription.type|sonataxeditabletype %}

{% if iseditable and xeditabletype %}
    {% block fieldspanattributes %}
        {% spaceless %}
            {{ parent() }}
            data-source=\"{{ fielddescription|sonataxeditablechoices|jsonencode }}\"
        {% endspaceless %}
    {% endblock %}
{% endif %}

{% block field %}
{% spaceless %}
    {% if fielddescription.options.choices is defined %}
        {% if fielddescription.options.multiple is defined and fielddescription.options.multiple==true and value is iterable %}

            {% set result = '' %}
            {% set delimiter = fielddescription.options.delimiter|default(', ') %}

            {% for val in value %}
                {% if result is not empty %}
                    {% set result = result ~ delimiter %}
                {% endif %}

                {% if fielddescription.options.choices[val] is defined %}
                    {% if fielddescription.options.catalogue is not defined %}
                        {% set result = result ~ fielddescription.options.choices[val] %}
                    {% else %}
                        {% set result = result ~ fielddescription.options.choices[val]|trans({}, fielddescription.options.catalogue) %}
                    {% endif %}
                {% else %}
                    {% set result = result ~ val %}
                {% endif %}
            {% endfor %}

            {% set value = result %}

        {% elseif value in fielddescription.options.choices|keys %}
            {% if fielddescription.options.catalogue is not defined %}
                {% set value = fielddescription.options.choices[value] %}
            {% else %}
                {% set value = fielddescription.options.choices[value]|trans({}, fielddescription.options.catalogue) %}
            {% endif %}
        {% endif %}
    {% endif %}

    {{ value }}
{% endspaceless %}
{% endblock %}
", "SonataAdminBundle:CRUD:listchoice.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listchoice.html.twig");
    }
}
