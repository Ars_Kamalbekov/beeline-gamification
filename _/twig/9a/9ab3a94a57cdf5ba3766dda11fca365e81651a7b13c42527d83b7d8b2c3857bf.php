<?php

/* SonataAdminBundle:CRUD:show.html.twig */
class TwigTemplate33c2735b5dfbcd9fabd07fc6f2f78ee328b63e269754b03c1a7100e78c9cda1e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshow.html.twig", "SonataAdminBundle:CRUD:show.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshow.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald68df2c230f3c6e316cd2f65a1bc39175eb56cac045a88594214220dd68b2bb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald68df2c230f3c6e316cd2f65a1bc39175eb56cac045a88594214220dd68b2bb4->enter($internald68df2c230f3c6e316cd2f65a1bc39175eb56cac045a88594214220dd68b2bb4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $internal3a4dee9a409bf15cec3d7afacb31e6125fcac8a5fb3a870737f9856b2265b307 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3a4dee9a409bf15cec3d7afacb31e6125fcac8a5fb3a870737f9856b2265b307->enter($internal3a4dee9a409bf15cec3d7afacb31e6125fcac8a5fb3a870737f9856b2265b307prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internald68df2c230f3c6e316cd2f65a1bc39175eb56cac045a88594214220dd68b2bb4->leave($internald68df2c230f3c6e316cd2f65a1bc39175eb56cac045a88594214220dd68b2bb4prof);

        
        $internal3a4dee9a409bf15cec3d7afacb31e6125fcac8a5fb3a870737f9856b2265b307->leave($internal3a4dee9a409bf15cec3d7afacb31e6125fcac8a5fb3a870737f9856b2265b307prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshow.html.twig' %}
", "SonataAdminBundle:CRUD:show.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/show.html.twig");
    }
}
