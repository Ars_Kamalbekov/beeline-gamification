<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class TwigTemplatef0abdd1a85462b09b91cee530d949b16bc5644939ded2e1be32569709785a0b8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbdde0447beaa3a9be5747d387899ddc5355e5bf829460df44f988e8475abf3a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbdde0447beaa3a9be5747d387899ddc5355e5bf829460df44f988e8475abf3a7->enter($internalbdde0447beaa3a9be5747d387899ddc5355e5bf829460df44f988e8475abf3a7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $internal97473ba55503369e858f6a7a90227b5ac7caed592dd8ea97362b0ed7815e1e6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal97473ba55503369e858f6a7a90227b5ac7caed592dd8ea97362b0ed7815e1e6c->enter($internal97473ba55503369e858f6a7a90227b5ac7caed592dd8ea97362b0ed7815e1e6cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo twiginclude($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $internalbdde0447beaa3a9be5747d387899ddc5355e5bf829460df44f988e8475abf3a7->leave($internalbdde0447beaa3a9be5747d387899ddc5355e5bf829460df44f988e8475abf3a7prof);

        
        $internal97473ba55503369e858f6a7a90227b5ac7caed592dd8ea97362b0ed7815e1e6c->leave($internal97473ba55503369e858f6a7a90227b5ac7caed592dd8ea97362b0ed7815e1e6cprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}
