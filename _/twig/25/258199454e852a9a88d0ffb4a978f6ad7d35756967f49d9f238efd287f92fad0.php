<?php

/* SonataAdminBundle:CRUD/Association:showonetomany.html.twig */
class TwigTemplatec15767c32712fa465f2fef1dc0ac73c7f278ba3ec1612b17894059753f0be418 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD/Association:showonetomany.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6afb37cc45c8f72c0a8e5334e9af7a1ab181785331aed20af0e389aeade5b05d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6afb37cc45c8f72c0a8e5334e9af7a1ab181785331aed20af0e389aeade5b05d->enter($internal6afb37cc45c8f72c0a8e5334e9af7a1ab181785331aed20af0e389aeade5b05dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showonetomany.html.twig"));

        $internalb6df9151fa35f1c10b924bcd56c61aeb05a17b8976013bfde156741d16dd1b7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb6df9151fa35f1c10b924bcd56c61aeb05a17b8976013bfde156741d16dd1b7d->enter($internalb6df9151fa35f1c10b924bcd56c61aeb05a17b8976013bfde156741d16dd1b7dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:showonetomany.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal6afb37cc45c8f72c0a8e5334e9af7a1ab181785331aed20af0e389aeade5b05d->leave($internal6afb37cc45c8f72c0a8e5334e9af7a1ab181785331aed20af0e389aeade5b05dprof);

        
        $internalb6df9151fa35f1c10b924bcd56c61aeb05a17b8976013bfde156741d16dd1b7d->leave($internalb6df9151fa35f1c10b924bcd56c61aeb05a17b8976013bfde156741d16dd1b7dprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal29009a5132ae6dcbaed7d1bdb65d4d03a0bedcd5f514c2d012802d998ccbfb01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29009a5132ae6dcbaed7d1bdb65d4d03a0bedcd5f514c2d012802d998ccbfb01->enter($internal29009a5132ae6dcbaed7d1bdb65d4d03a0bedcd5f514c2d012802d998ccbfb01prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal3ca89d357461db8c7ebac2191d9479393a23fe5a8bbf1c555b4345ada076fa52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3ca89d357461db8c7ebac2191d9479393a23fe5a8bbf1c555b4345ada076fa52->enter($internal3ca89d357461db8c7ebac2191d9479393a23fe5a8bbf1c555b4345ada076fa52prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <ul class=\"sonata-ba-show-one-to-many\">
    ";
        // line 16
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 17
        echo "    ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["element"]) {
            // line 18
            echo "        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 20
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => $context["element"]), "method"))) {
                // line 21
                echo "            <li>
                <a href=\"";
                // line 22
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 22, $this->getSourceContext()); })()), 1 => $context["element"], 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                    ";
                // line 23
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
                echo "
                </a>
            </li>
        ";
            } else {
                // line 27
                echo "            <li>";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 27, $this->getSourceContext()); })())), "html", null, true);
                echo "</li>
        ";
            }
            // line 29
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 30
        echo "    </ul>
";
        
        $internal3ca89d357461db8c7ebac2191d9479393a23fe5a8bbf1c555b4345ada076fa52->leave($internal3ca89d357461db8c7ebac2191d9479393a23fe5a8bbf1c555b4345ada076fa52prof);

        
        $internal29009a5132ae6dcbaed7d1bdb65d4d03a0bedcd5f514c2d012802d998ccbfb01->leave($internal29009a5132ae6dcbaed7d1bdb65d4d03a0bedcd5f514c2d012802d998ccbfb01prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:showonetomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 30,  84 => 29,  78 => 27,  71 => 23,  67 => 22,  64 => 21,  62 => 20,  61 => 19,  59 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    <ul class=\"sonata-ba-show-one-to-many\">
    {% set routename = fielddescription.options.route.name %}
    {% for element in value%}
        {% if fielddescription.hasassociationadmin
        and fielddescription.associationadmin.hasRoute(routename)
        and fielddescription.associationadmin.hasAccess(routename, element) %}
            <li>
                <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
                    {{ element|renderrelationelement(fielddescription) }}
                </a>
            </li>
        {% else %}
            <li>{{ element|renderrelationelement(fielddescription) }}</li>
        {% endif %}
    {% endfor %}
    </ul>
{% endblock %}
", "SonataAdminBundle:CRUD/Association:showonetomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/showonetomany.html.twig");
    }
}
