<?php

/* WebProfilerBundle:Profiler:search.html.twig */
class TwigTemplate9d4c95e2f98f6f2bde41d5d87256326cc9037e3a65dd7edcf30bb598d7c7a239 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internald676a77e54e0bb2548981e719cd8b67963563f346b9eaa8b24929a68d15e7988 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald676a77e54e0bb2548981e719cd8b67963563f346b9eaa8b24929a68d15e7988->enter($internald676a77e54e0bb2548981e719cd8b67963563f346b9eaa8b24929a68d15e7988prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:search.html.twig"));

        $internal283f8b0a0e858800a469949d9e96bc1a5a16ed60758488de36a89fbb980668e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal283f8b0a0e858800a469949d9e96bc1a5a16ed60758488de36a89fbb980668e6->enter($internal283f8b0a0e858800a469949d9e96bc1a5a16ed60758488de36a89fbb980668e6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:search.html.twig"));

        // line 1
        echo "<div id=\"sidebar-search\" class=\"hidden\">
    <form action=\"";
        // line 2
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilersearch");
        echo "\" method=\"get\">
        <div class=\"form-group\">
            <label for=\"ip\">IP</label>
            <input type=\"text\" name=\"ip\" id=\"ip\" value=\"";
        // line 5
        echo twigescapefilter($this->env, (isset($context["ip"]) || arraykeyexists("ip", $context) ? $context["ip"] : (function () { throw new TwigErrorRuntime('Variable "ip" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"method\">Method</label>
            <select name=\"method\" id=\"method\">
                <option value=\"\">Any</option>
                ";
        // line 12
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(array(0 => "DELETE", 1 => "GET", 2 => "HEAD", 3 => "PATCH", 4 => "POST", 5 => "PUT"));
        foreach ($context['seq'] as $context["key"] => $context["m"]) {
            // line 13
            echo "                    <option ";
            echo ((($context["m"] == (isset($context["method"]) || arraykeyexists("method", $context) ? $context["method"] : (function () { throw new TwigErrorRuntime('Variable "method" does not exist.', 13, $this->getSourceContext()); })()))) ? ("selected=\"selected\"") : (""));
            echo ">";
            echo twigescapefilter($this->env, $context["m"], "html", null, true);
            echo "</option>
                ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['m'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 15
        echo "            </select>
        </div>

        <div class=\"form-group\">
            <label for=\"statuscode\">Status</label>
            <input type=\"number\" name=\"statuscode\" id=\"statuscode\" value=\"";
        // line 20
        echo twigescapefilter($this->env, (isset($context["statuscode"]) || arraykeyexists("statuscode", $context) ? $context["statuscode"] : (function () { throw new TwigErrorRuntime('Variable "statuscode" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"url\">URL</label>
            <input type=\"text\" name=\"url\" id=\"url\" value=\"";
        // line 25
        echo twigescapefilter($this->env, (isset($context["url"]) || arraykeyexists("url", $context) ? $context["url"] : (function () { throw new TwigErrorRuntime('Variable "url" does not exist.', 25, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"token\">Token</label>
            <input type=\"text\" name=\"token\" id=\"token\" value=\"";
        // line 30
        echo twigescapefilter($this->env, (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 30, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"start\">From</label>
            <input type=\"date\" name=\"start\" id=\"start\" value=\"";
        // line 35
        echo twigescapefilter($this->env, (isset($context["start"]) || arraykeyexists("start", $context) ? $context["start"] : (function () { throw new TwigErrorRuntime('Variable "start" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"end\">Until</label>
            <input type=\"date\" name=\"end\" id=\"end\" value=\"";
        // line 40
        echo twigescapefilter($this->env, (isset($context["end"]) || arraykeyexists("end", $context) ? $context["end"] : (function () { throw new TwigErrorRuntime('Variable "end" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
        echo "\">
        </div>

        <div class=\"form-group\">
            <label for=\"limit\">Results</label>
            <select name=\"limit\" id=\"limit\">
                ";
        // line 46
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(array(0 => 10, 1 => 50, 2 => 100));
        foreach ($context['seq'] as $context["key"] => $context["l"]) {
            // line 47
            echo "                    <option ";
            echo ((($context["l"] == (isset($context["limit"]) || arraykeyexists("limit", $context) ? $context["limit"] : (function () { throw new TwigErrorRuntime('Variable "limit" does not exist.', 47, $this->getSourceContext()); })()))) ? ("selected=\"selected\"") : (""));
            echo ">";
            echo twigescapefilter($this->env, $context["l"], "html", null, true);
            echo "</option>
                ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['l'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 49
        echo "            </select>
        </div>

        <div class=\"form-group\">
            <button type=\"submit\" class=\"btn btn-sm\">Search</button>
        </div>
    </form>
</div>
";
        
        $internald676a77e54e0bb2548981e719cd8b67963563f346b9eaa8b24929a68d15e7988->leave($internald676a77e54e0bb2548981e719cd8b67963563f346b9eaa8b24929a68d15e7988prof);

        
        $internal283f8b0a0e858800a469949d9e96bc1a5a16ed60758488de36a89fbb980668e6->leave($internal283f8b0a0e858800a469949d9e96bc1a5a16ed60758488de36a89fbb980668e6prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 49,  111 => 47,  107 => 46,  98 => 40,  90 => 35,  82 => 30,  74 => 25,  66 => 20,  59 => 15,  48 => 13,  44 => 12,  34 => 5,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div id=\"sidebar-search\" class=\"hidden\">
    <form action=\"{{ path('profilersearch') }}\" method=\"get\">
        <div class=\"form-group\">
            <label for=\"ip\">IP</label>
            <input type=\"text\" name=\"ip\" id=\"ip\" value=\"{{ ip }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"method\">Method</label>
            <select name=\"method\" id=\"method\">
                <option value=\"\">Any</option>
                {% for m in ['DELETE', 'GET', 'HEAD', 'PATCH', 'POST', 'PUT'] %}
                    <option {{ m == method ? 'selected=\"selected\"' }}>{{ m }}</option>
                {% endfor %}
            </select>
        </div>

        <div class=\"form-group\">
            <label for=\"statuscode\">Status</label>
            <input type=\"number\" name=\"statuscode\" id=\"statuscode\" value=\"{{ statuscode }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"url\">URL</label>
            <input type=\"text\" name=\"url\" id=\"url\" value=\"{{ url }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"token\">Token</label>
            <input type=\"text\" name=\"token\" id=\"token\" value=\"{{ token }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"start\">From</label>
            <input type=\"date\" name=\"start\" id=\"start\" value=\"{{ start }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"end\">Until</label>
            <input type=\"date\" name=\"end\" id=\"end\" value=\"{{ end }}\">
        </div>

        <div class=\"form-group\">
            <label for=\"limit\">Results</label>
            <select name=\"limit\" id=\"limit\">
                {% for l in [10, 50, 100] %}
                    <option {{ l == limit ? 'selected=\"selected\"' }}>{{ l }}</option>
                {% endfor %}
            </select>
        </div>

        <div class=\"form-group\">
            <button type=\"submit\" class=\"btn btn-sm\">Search</button>
        </div>
    </form>
</div>
", "WebProfilerBundle:Profiler:search.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/search.html.twig");
    }
}
