<?php

/* SonataAdminBundle::ajaxlayout.html.twig */
class TwigTemplatebcdf283ce5bd2933320eff9b5a0f53c78fd1e05c989c725208e6e8a1eaeb559e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'blockcontent'),
            'preview' => array($this, 'blockpreview'),
            'form' => array($this, 'blockform'),
            'list' => array($this, 'blocklist'),
            'show' => array($this, 'blockshow'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal53a2b8b31e7c851186a3f313c87c8bf8613f4dcd86dd3d37703d29f8093e86f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal53a2b8b31e7c851186a3f313c87c8bf8613f4dcd86dd3d37703d29f8093e86f8->enter($internal53a2b8b31e7c851186a3f313c87c8bf8613f4dcd86dd3d37703d29f8093e86f8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::ajaxlayout.html.twig"));

        $internal7916ff3e10ac6d4c32d3474e6bb244162ad69515238b51d0047b4c8e450bef67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7916ff3e10ac6d4c32d3474e6bb244162ad69515238b51d0047b4c8e450bef67->enter($internal7916ff3e10ac6d4c32d3474e6bb244162ad69515238b51d0047b4c8e450bef67prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle::ajaxlayout.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('content', $context, $blocks);
        
        $internal53a2b8b31e7c851186a3f313c87c8bf8613f4dcd86dd3d37703d29f8093e86f8->leave($internal53a2b8b31e7c851186a3f313c87c8bf8613f4dcd86dd3d37703d29f8093e86f8prof);

        
        $internal7916ff3e10ac6d4c32d3474e6bb244162ad69515238b51d0047b4c8e450bef67->leave($internal7916ff3e10ac6d4c32d3474e6bb244162ad69515238b51d0047b4c8e450bef67prof);

    }

    public function blockcontent($context, array $blocks = array())
    {
        $internal77e1af475e4a51887490b8b86d686faf875e3b5e5ea8763056a517cb7ef4d5cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal77e1af475e4a51887490b8b86d686faf875e3b5e5ea8763056a517cb7ef4d5cb->enter($internal77e1af475e4a51887490b8b86d686faf875e3b5e5ea8763056a517cb7ef4d5cbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internal01b950d80a3d149d581ebc36b33e42eda9b059629976cd5a31a20185f9221cb0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal01b950d80a3d149d581ebc36b33e42eda9b059629976cd5a31a20185f9221cb0->enter($internal01b950d80a3d149d581ebc36b33e42eda9b059629976cd5a31a20185f9221cb0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 13
        echo "    ";
        $context["listtable"] = ((        $this->hasBlock("listtable", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listtable", $context, $blocks))) : (null));
        // line 14
        echo "    ";
        $context["listfilters"] = ((        $this->hasBlock("listfilters", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listfilters", $context, $blocks))) : (null));
        // line 15
        echo "    ";
        $context["listfiltersactions"] = ((        $this->hasBlock("listfiltersactions", $context, $blocks)) ? (twigtrimfilter(        $this->renderBlock("listfiltersactions", $context, $blocks))) : (null));
        // line 16
        echo "
    ";
        // line 17
        $this->displayBlock('preview', $context, $blocks);
        // line 18
        echo "    ";
        $this->displayBlock('form', $context, $blocks);
        // line 19
        echo "    ";
        $this->displayBlock('list', $context, $blocks);
        // line 20
        echo "    ";
        $this->displayBlock('show', $context, $blocks);
        // line 21
        echo "
    ";
        // line 22
        if (( !twigtestempty((isset($context["listtable"]) || arraykeyexists("listtable", $context) ? $context["listtable"] : (function () { throw new TwigErrorRuntime('Variable "listtable" does not exist.', 22, $this->getSourceContext()); })())) ||  !twigtestempty((isset($context["listfilters"]) || arraykeyexists("listfilters", $context) ? $context["listfilters"] : (function () { throw new TwigErrorRuntime('Variable "listfilters" does not exist.', 22, $this->getSourceContext()); })())))) {
            // line 23
            echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        ";
            // line 28
            if ((((arraykeyexists("admin", $context) && arraykeyexists("action", $context)) && ((isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 28, $this->getSourceContext()); })()) == "list")) && (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "listModes", array())) > 1))) {
                // line 29
                echo "                            <div class=\"nav navbar-right btn-group\">
                                ";
                // line 30
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 30, $this->getSourceContext()); })()), "listModes", array()));
                foreach ($context['seq'] as $context["mode"] => $context["settings"]) {
                    // line 31
                    echo "                                    <a href=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "generateUrl", array(0 => "list", 1 => twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 31, $this->getSourceContext()); })()), "request", array()), "query", array()), "all", array()), array("listmode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 31, $this->getSourceContext()); })()), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['mode'], $context['settings'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 33
                echo "                            </div>
                        ";
            }
            // line 35
            echo "
                        ";
            // line 36
            if ( !twigtestempty((isset($context["listfiltersactions"]) || arraykeyexists("listfiltersactions", $context) ? $context["listfiltersactions"] : (function () { throw new TwigErrorRuntime('Variable "listfiltersactions" does not exist.', 36, $this->getSourceContext()); })()))) {
                // line 37
                echo "                            ";
                echo (isset($context["listfiltersactions"]) || arraykeyexists("listfiltersactions", $context) ? $context["listfiltersactions"] : (function () { throw new TwigErrorRuntime('Variable "listfiltersactions" does not exist.', 37, $this->getSourceContext()); })());
                echo "
                        ";
            }
            // line 39
            echo "                    </div>
                </div>
            </div>
        </div>

        ";
            // line 44
            if (twigtrimfilter((isset($context["listfilters"]) || arraykeyexists("listfilters", $context) ? $context["listfilters"] : (function () { throw new TwigErrorRuntime('Variable "listfilters" does not exist.', 44, $this->getSourceContext()); })()))) {
                // line 45
                echo "            <div class=\"row\">
                ";
                // line 46
                echo (isset($context["listfilters"]) || arraykeyexists("listfilters", $context) ? $context["listfilters"] : (function () { throw new TwigErrorRuntime('Variable "listfilters" does not exist.', 46, $this->getSourceContext()); })());
                echo "
            </div>
        ";
            }
            // line 49
            echo "
        <div class=\"row\">
            ";
            // line 51
            echo (isset($context["listtable"]) || arraykeyexists("listtable", $context) ? $context["listtable"] : (function () { throw new TwigErrorRuntime('Variable "listtable" does not exist.', 51, $this->getSourceContext()); })());
            echo "
        </div>
    </div>
    ";
        }
        
        $internal01b950d80a3d149d581ebc36b33e42eda9b059629976cd5a31a20185f9221cb0->leave($internal01b950d80a3d149d581ebc36b33e42eda9b059629976cd5a31a20185f9221cb0prof);

        
        $internal77e1af475e4a51887490b8b86d686faf875e3b5e5ea8763056a517cb7ef4d5cb->leave($internal77e1af475e4a51887490b8b86d686faf875e3b5e5ea8763056a517cb7ef4d5cbprof);

    }

    // line 17
    public function blockpreview($context, array $blocks = array())
    {
        $internal61d031b96a9c6804a0a850cb500e4b9b25e048e75c4ac358985dd861d17774d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal61d031b96a9c6804a0a850cb500e4b9b25e048e75c4ac358985dd861d17774d1->enter($internal61d031b96a9c6804a0a850cb500e4b9b25e048e75c4ac358985dd861d17774d1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "preview"));

        $internalc1797d31f43c08a33e316152d3c7e07bec0b204caef172a7fe36db54161f91e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc1797d31f43c08a33e316152d3c7e07bec0b204caef172a7fe36db54161f91e7->enter($internalc1797d31f43c08a33e316152d3c7e07bec0b204caef172a7fe36db54161f91e7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "preview"));

        
        $internalc1797d31f43c08a33e316152d3c7e07bec0b204caef172a7fe36db54161f91e7->leave($internalc1797d31f43c08a33e316152d3c7e07bec0b204caef172a7fe36db54161f91e7prof);

        
        $internal61d031b96a9c6804a0a850cb500e4b9b25e048e75c4ac358985dd861d17774d1->leave($internal61d031b96a9c6804a0a850cb500e4b9b25e048e75c4ac358985dd861d17774d1prof);

    }

    // line 18
    public function blockform($context, array $blocks = array())
    {
        $internal60b014dc74cc1e446974edd71d9b6779846d598c365ef4a9308d038f35b3f826 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal60b014dc74cc1e446974edd71d9b6779846d598c365ef4a9308d038f35b3f826->enter($internal60b014dc74cc1e446974edd71d9b6779846d598c365ef4a9308d038f35b3f826prof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        $internalead553c1af5411e37ad85fb3b4cfd4acaae00b917649d4088146f016930c679b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalead553c1af5411e37ad85fb3b4cfd4acaae00b917649d4088146f016930c679b->enter($internalead553c1af5411e37ad85fb3b4cfd4acaae00b917649d4088146f016930c679bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "form"));

        
        $internalead553c1af5411e37ad85fb3b4cfd4acaae00b917649d4088146f016930c679b->leave($internalead553c1af5411e37ad85fb3b4cfd4acaae00b917649d4088146f016930c679bprof);

        
        $internal60b014dc74cc1e446974edd71d9b6779846d598c365ef4a9308d038f35b3f826->leave($internal60b014dc74cc1e446974edd71d9b6779846d598c365ef4a9308d038f35b3f826prof);

    }

    // line 19
    public function blocklist($context, array $blocks = array())
    {
        $internal168a056345424f4dc23533b06627d2f9b0332d49c9a0698f8b2df34bfca74b37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal168a056345424f4dc23533b06627d2f9b0332d49c9a0698f8b2df34bfca74b37->enter($internal168a056345424f4dc23533b06627d2f9b0332d49c9a0698f8b2df34bfca74b37prof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        $internal0e3d2da687b13a73e4859ea1a08fef30b54d5975739c037c110048ad14bdf6f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0e3d2da687b13a73e4859ea1a08fef30b54d5975739c037c110048ad14bdf6f0->enter($internal0e3d2da687b13a73e4859ea1a08fef30b54d5975739c037c110048ad14bdf6f0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "list"));

        
        $internal0e3d2da687b13a73e4859ea1a08fef30b54d5975739c037c110048ad14bdf6f0->leave($internal0e3d2da687b13a73e4859ea1a08fef30b54d5975739c037c110048ad14bdf6f0prof);

        
        $internal168a056345424f4dc23533b06627d2f9b0332d49c9a0698f8b2df34bfca74b37->leave($internal168a056345424f4dc23533b06627d2f9b0332d49c9a0698f8b2df34bfca74b37prof);

    }

    // line 20
    public function blockshow($context, array $blocks = array())
    {
        $internal6259a1aa6a3f3597c64cb7dd7c2e7a21f90567137e8b07eca9f930d777264e62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6259a1aa6a3f3597c64cb7dd7c2e7a21f90567137e8b07eca9f930d777264e62->enter($internal6259a1aa6a3f3597c64cb7dd7c2e7a21f90567137e8b07eca9f930d777264e62prof = new TwigProfilerProfile($this->getTemplateName(), "block", "show"));

        $internal18dad758a5e219bbfaec35a79a4adef69f5de2f7f642d6dbef1b94b458fda70d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal18dad758a5e219bbfaec35a79a4adef69f5de2f7f642d6dbef1b94b458fda70d->enter($internal18dad758a5e219bbfaec35a79a4adef69f5de2f7f642d6dbef1b94b458fda70dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "show"));

        
        $internal18dad758a5e219bbfaec35a79a4adef69f5de2f7f642d6dbef1b94b458fda70d->leave($internal18dad758a5e219bbfaec35a79a4adef69f5de2f7f642d6dbef1b94b458fda70dprof);

        
        $internal6259a1aa6a3f3597c64cb7dd7c2e7a21f90567137e8b07eca9f930d777264e62->leave($internal6259a1aa6a3f3597c64cb7dd7c2e7a21f90567137e8b07eca9f930d777264e62prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::ajaxlayout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  213 => 20,  196 => 19,  179 => 18,  162 => 17,  147 => 51,  143 => 49,  137 => 46,  134 => 45,  132 => 44,  125 => 39,  119 => 37,  117 => 36,  114 => 35,  110 => 33,  95 => 31,  91 => 30,  88 => 29,  86 => 28,  79 => 23,  77 => 22,  74 => 21,  71 => 20,  68 => 19,  65 => 18,  63 => 17,  60 => 16,  57 => 15,  54 => 14,  51 => 13,  33 => 12,  30 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block content %}
    {% set listtable = block('listtable') is defined ? block('listtable')|trim : null %}
    {% set listfilters = block('listfilters') is defined ? block('listfilters')|trim : null %}
    {% set listfiltersactions = block('listfiltersactions') is defined ? block('listfiltersactions')|trim : null %}

    {% block preview %}{% endblock %}
    {% block form %}{% endblock %}
    {% block list %}{% endblock %}
    {% block show %}{% endblock %}

    {% if listtable is not empty or listfilters is not empty %}
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"navbar navbar-default sonata-list-table\">
                <div class=\"container-fluid\">
                    <div class=\"navbar-collapse\">
                        {% if admin is defined and action is defined and action == 'list' and admin.listModes|length > 1 %}
                            <div class=\"nav navbar-right btn-group\">
                                {% for mode, settings in admin.listModes %}
                                    <a href=\"{{ admin.generateUrl('list', app.request.query.all|merge({listmode: mode})) }}\" class=\"btn btn-default navbar-btn btn-sm{% if admin.getListMode() == mode %} active{% endif %}\"><i class=\"{{ settings.class }}\"></i></a>
                                {% endfor %}
                            </div>
                        {% endif %}

                        {% if listfiltersactions is not empty %}
                            {{ listfiltersactions|raw }}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>

        {% if listfilters|trim %}
            <div class=\"row\">
                {{ listfilters|raw }}
            </div>
        {% endif %}

        <div class=\"row\">
            {{ listtable|raw }}
        </div>
    </div>
    {% endif %}
{% endblock %}
", "SonataAdminBundle::ajaxlayout.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/ajaxlayout.html.twig");
    }
}
