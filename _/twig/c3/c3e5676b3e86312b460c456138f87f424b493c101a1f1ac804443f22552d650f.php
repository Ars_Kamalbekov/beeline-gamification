<?php

/* SonataAdminBundle:CRUD:listselect.html.twig */
class TwigTemplate3b97b632045a945bc8979b1e698044ab1525453bed32d88da76bd42f27c6b4b8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listselect.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6ac52a8589f14d0224bc8c0889a45d3e177ca12851c0b0cfe3c23906e5b2fad1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6ac52a8589f14d0224bc8c0889a45d3e177ca12851c0b0cfe3c23906e5b2fad1->enter($internal6ac52a8589f14d0224bc8c0889a45d3e177ca12851c0b0cfe3c23906e5b2fad1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listselect.html.twig"));

        $internalc276f02e7217bf3c0e53abe43cefe2de3277b11a2db074f4f5b2d843e9de347f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc276f02e7217bf3c0e53abe43cefe2de3277b11a2db074f4f5b2d843e9de347f->enter($internalc276f02e7217bf3c0e53abe43cefe2de3277b11a2db074f4f5b2d843e9de347fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listselect.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal6ac52a8589f14d0224bc8c0889a45d3e177ca12851c0b0cfe3c23906e5b2fad1->leave($internal6ac52a8589f14d0224bc8c0889a45d3e177ca12851c0b0cfe3c23906e5b2fad1prof);

        
        $internalc276f02e7217bf3c0e53abe43cefe2de3277b11a2db074f4f5b2d843e9de347f->leave($internalc276f02e7217bf3c0e53abe43cefe2de3277b11a2db074f4f5b2d843e9de347fprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalc64ab5d70d6c1ec7e7165815f3f3e5e56859d2d56177112d280f59d52aee2065 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc64ab5d70d6c1ec7e7165815f3f3e5e56859d2d56177112d280f59d52aee2065->enter($internalc64ab5d70d6c1ec7e7165815f3f3e5e56859d2d56177112d280f59d52aee2065prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal92cbb0f9431b39518ee97cf49b502fa8dc835024f25c0af801e07e7211cb88ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal92cbb0f9431b39518ee97cf49b502fa8dc835024f25c0af801e07e7211cb88ac->enter($internal92cbb0f9431b39518ee97cf49b502fa8dc835024f25c0af801e07e7211cb88acprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <a class=\"btn btn-primary\" href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 15, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
        echo "\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 17
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("listselect", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </a>
";
        
        $internal92cbb0f9431b39518ee97cf49b502fa8dc835024f25c0af801e07e7211cb88ac->leave($internal92cbb0f9431b39518ee97cf49b502fa8dc835024f25c0af801e07e7211cb88acprof);

        
        $internalc64ab5d70d6c1ec7e7165815f3f3e5e56859d2d56177112d280f59d52aee2065->leave($internalc64ab5d70d6c1ec7e7165815f3f3e5e56859d2d56177112d280f59d52aee2065prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listselect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 17,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    <a class=\"btn btn-primary\" href=\"{{ admin.generateUrl('list') }}\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'listselect'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endblock %}
", "SonataAdminBundle:CRUD:listselect.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listselect.html.twig");
    }
}
