<?php

/* SonataAdminBundle:Core:createbutton.html.twig */
class TwigTemplate5868aecaccd09f0186fde1c01f4052eedcbbae404d0ebd3c8770c31b1f32b7a6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 15
        $this->parent = $this->loadTemplate("SonataAdminBundle:Button:createbutton.html.twig", "SonataAdminBundle:Core:createbutton.html.twig", 15);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Button:createbutton.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal38f5ad6c5d99980bf2e69cd7a2ae553f221686694c328d22e993625c0c8a3eb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal38f5ad6c5d99980bf2e69cd7a2ae553f221686694c328d22e993625c0c8a3eb2->enter($internal38f5ad6c5d99980bf2e69cd7a2ae553f221686694c328d22e993625c0c8a3eb2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:createbutton.html.twig"));

        $internal2ab7df9eb11e20c664a0e383b15adcfd3f671e029f976a168a8fc0ebab90688f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2ab7df9eb11e20c664a0e383b15adcfd3f671e029f976a168a8fc0ebab90688f->enter($internal2ab7df9eb11e20c664a0e383b15adcfd3f671e029f976a168a8fc0ebab90688fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:createbutton.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal38f5ad6c5d99980bf2e69cd7a2ae553f221686694c328d22e993625c0c8a3eb2->leave($internal38f5ad6c5d99980bf2e69cd7a2ae553f221686694c328d22e993625c0c8a3eb2prof);

        
        $internal2ab7df9eb11e20c664a0e383b15adcfd3f671e029f976a168a8fc0ebab90688f->leave($internal2ab7df9eb11e20c664a0e383b15adcfd3f671e029f976a168a8fc0ebab90688fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:createbutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 15,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{# DEPRECATED #}
{# This file is kept here for backward compatibility - Rather use SonataAdminBundle:Button:createbutton.html.twig #}

{% extends 'SonataAdminBundle:Button:createbutton.html.twig' %}
", "SonataAdminBundle:Core:createbutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/createbutton.html.twig");
    }
}
