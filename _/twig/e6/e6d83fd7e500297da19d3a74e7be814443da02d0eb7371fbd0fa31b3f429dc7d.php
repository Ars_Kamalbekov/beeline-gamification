<?php

/* SwiftmailerBundle:Collector:swiftmailer.html.twig */
class TwigTemplate886efce0b9a4b639667711f9f96a8da318e5a643887c1d7bffda858ef213e294 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "SwiftmailerBundle:Collector:swiftmailer.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal231976969b55f8c06fecf7d6c63bdb2c29c090888d9f698c098a68272f65b3c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal231976969b55f8c06fecf7d6c63bdb2c29c090888d9f698c098a68272f65b3c7->enter($internal231976969b55f8c06fecf7d6c63bdb2c29c090888d9f698c098a68272f65b3c7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SwiftmailerBundle:Collector:swiftmailer.html.twig"));

        $internal031349d2d5ef77df65c0382189d16fa43f7d317bc2666686028a70bdc2c82008 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal031349d2d5ef77df65c0382189d16fa43f7d317bc2666686028a70bdc2c82008->enter($internal031349d2d5ef77df65c0382189d16fa43f7d317bc2666686028a70bdc2c82008prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SwiftmailerBundle:Collector:swiftmailer.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal231976969b55f8c06fecf7d6c63bdb2c29c090888d9f698c098a68272f65b3c7->leave($internal231976969b55f8c06fecf7d6c63bdb2c29c090888d9f698c098a68272f65b3c7prof);

        
        $internal031349d2d5ef77df65c0382189d16fa43f7d317bc2666686028a70bdc2c82008->leave($internal031349d2d5ef77df65c0382189d16fa43f7d317bc2666686028a70bdc2c82008prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal6d41131edf3492aeafdecbbcbcd859f80807a137f4870f3b6481f5af5497489c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6d41131edf3492aeafdecbbcbcd859f80807a137f4870f3b6481f5af5497489c->enter($internal6d41131edf3492aeafdecbbcbcd859f80807a137f4870f3b6481f5af5497489cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internalf24dfa6c9ee82067a1021c78f695275a99719e8e8a42696caf75358d497718d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf24dfa6c9ee82067a1021c78f695275a99719e8e8a42696caf75358d497718d4->enter($internalf24dfa6c9ee82067a1021c78f695275a99719e8e8a42696caf75358d497718d4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        $context["profilermarkupversion"] = ((arraykeyexists("profilermarkupversion", $context)) ? (twigdefaultfilter((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 4, $this->getSourceContext()); })()), 1)) : (1));
        // line 5
        echo "
    ";
        // line 6
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 6, $this->getSourceContext()); })()), "messageCount", array())) {
            // line 7
            echo "        ";
            obstart();
            // line 8
            echo "            ";
            if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 8, $this->getSourceContext()); })()) == 1)) {
                // line 9
                echo "                <img width=\"23\" height=\"28\" alt=\"Swiftmailer\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAcCAYAAACK7SRjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0NEOTU1MjM0OThFMTFFMDg3NzJBNTE2ODgwQzMxMzQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0NEOTU1MjQ0OThFMTFFMDg3NzJBNTE2ODgwQzMxMzQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxMEQ5OTQ5QzQ5OEMxMUUwODc3MkE1MTY4ODBDMzEzNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3Q0Q5NTUyMjQ5OEUxMUUwODc3MkE1MTY4ODBDMzEzNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpkRnSAAAAJ0SURBVHjaYvz//z8DrQATAw3BqOFYAaO9vT1FBhw4cGCAXA5MipxBQUHT3r17l0AVAxkZ/wkLC89as2ZNIcjlYkALXKnlWqBZTH/+/PEDmQsynLW/v3+NoaHhN2oYDjJn8uTJK4BMNpDhPwsLCwOKiop2+fn5vafEYC8vrw8gc/Lz8wOB3B8gw/nev38vn5WV5eTg4LA/Ly/vESsrK2npmYmJITU19SnQ8L0gc4DxpwgyF2S4EEjB58+f+crLy31YWFjOt7S0XBYUFPxHjMEcHBz/6+rqboqJiZ0qKSnxBpkDlRICGc4MU/j792+2CRMm+L18+fLSxIkTDykoKPzBZ7CoqOi/np6eE8rKylvb29v9fvz4wYEkzYKRzjk5OX/LyMjcnDRpEkjjdisrK6wRraOj8wvokAMLFy788ejRoxcaGhrPCWai4ODgB8DUE3/mzBknYMToASNoMzAfvEVW4+Tk9LmhoWFbTU2NwunTpx2BjiiMjo6+hm4WCzJHUlLyz+vXrxkfP36sDOI/ffpUPikpibe7u3sLsJjQW7VqlSrQxe+Avjmanp7u9PbtWzGQOmCCkARmmu/m5uYfT548yY/V5UpKSl+2b9+uiiz26dMnIWBSDTp27NgdYGrYCIzwE7m5uR4wg2Hg/PnzSsDI/QlKOcjZ3wGUBLm5uf+DwLdv38gub4AG/xcSEvr35s0bZmCB5sgCE/z69SsjyDJKMtG/f/8YQQYD8wkoGf8H51AbG5sH1CpbQBnQ09PzBiiHgoIFFHlBQGwLxLxUMP8dqJgH4k3gIhfIkAKVYkDMTmmhCHIxEL8A4peMo02L4WU4QIABANxZAoDIQDv3AAAAAElFTkSuQmCC\" />
                <span class=\"sf-toolbar-status\">";
                // line 10
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 10, $this->getSourceContext()); })()), "messageCount", array()), "html", null, true);
                echo "</span>
            ";
            } else {
                // line 12
                echo "                ";
                echo twiginclude($this->env, $context, "@Swiftmailer/Collector/icon.svg");
                echo "
                <span class=\"sf-toolbar-value\">";
                // line 13
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "messageCount", array()), "html", null, true);
                echo "</span>
            ";
            }
            // line 15
            echo "        ";
            $context["icon"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 16
            echo "
        ";
            // line 17
            obstart();
            // line 18
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Sent messages</b>
                <span class=\"sf-toolbar-status\">";
            // line 20
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 20, $this->getSourceContext()); })()), "messageCount", array()), "html", null, true);
            echo "</span>
            </div>

            ";
            // line 23
            if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 23, $this->getSourceContext()); })()) == 1)) {
                // line 24
                echo "                ";
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 24, $this->getSourceContext()); })()), "mailers", array()));
                $context['loop'] = array(
                  'parent' => $context['parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                    $length = count($context['seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['seq'] as $context["key"] => $context["name"]) {
                    // line 25
                    echo "                    <div class=\"sf-toolbar-info-piece\">
                        <b>";
                    // line 26
                    echo twigescapefilter($this->env, $context["name"], "html", null, true);
                    echo "</b>
                        <span>";
                    // line 27
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "messageCount", array(0 => $context["name"]), "method"), "html", null, true);
                    echo "</span>
                    </div>
                    <div class=\"sf-toolbar-info-piece\">
                        <b>Is spooled?</b>
                        <span>";
                    // line 31
                    echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "isSpool", array(0 => $context["name"]), "method")) ? ("yes") : ("no"));
                    echo "</span>
                    </div>

                    ";
                    // line 34
                    if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                        // line 35
                        echo "                        <hr>
                    ";
                    }
                    // line 37
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['name'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 38
                echo "            ";
            } else {
                // line 39
                echo "                ";
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 39, $this->getSourceContext()); })()), "mailers", array()));
                foreach ($context['seq'] as $context["key"] => $context["name"]) {
                    // line 40
                    echo "                    <div class=\"sf-toolbar-info-piece\">
                        <b>";
                    // line 41
                    echo twigescapefilter($this->env, $context["name"], "html", null, true);
                    echo " mailer</b>
                        <span class=\"sf-toolbar-status\">";
                    // line 42
                    echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "messageCount", array(0 => $context["name"]), "method", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["collector"] ?? null), "messageCount", array(0 => $context["name"]), "method"), 0)) : (0)), "html", null, true);
                    echo "</span>
                        &nbsp; (<small>";
                    // line 43
                    echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 43, $this->getSourceContext()); })()), "isSpool", array(0 => $context["name"]), "method")) ? ("spooled") : ("sent"));
                    echo "</small>)
                    </div>
                ";
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['name'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 46
                echo "            ";
            }
            // line 47
            echo "        ";
            $context["text"] = ('' === $tmp = obgetclean()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
            // line 48
            echo "
        ";
            // line 49
            echo twiginclude($this->env, $context, "@WebProfiler/Profiler/toolbaritem.html.twig", array("link" => (isset($context["profilerurl"]) || arraykeyexists("profilerurl", $context) ? $context["profilerurl"] : (function () { throw new TwigErrorRuntime('Variable "profilerurl" does not exist.', 49, $this->getSourceContext()); })())));
            echo "
    ";
        }
        
        $internalf24dfa6c9ee82067a1021c78f695275a99719e8e8a42696caf75358d497718d4->leave($internalf24dfa6c9ee82067a1021c78f695275a99719e8e8a42696caf75358d497718d4prof);

        
        $internal6d41131edf3492aeafdecbbcbcd859f80807a137f4870f3b6481f5af5497489c->leave($internal6d41131edf3492aeafdecbbcbcd859f80807a137f4870f3b6481f5af5497489cprof);

    }

    // line 53
    public function blockmenu($context, array $blocks = array())
    {
        $internal6d315d225c934c2c500f2e63ad1b703867d877cf114eefdc15aa193b2e5fe72b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6d315d225c934c2c500f2e63ad1b703867d877cf114eefdc15aa193b2e5fe72b->enter($internal6d315d225c934c2c500f2e63ad1b703867d877cf114eefdc15aa193b2e5fe72bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internalf299b6c7f8cc72c133f8990ccbe1075767f2af8c66014deafcecb0970f40c5cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf299b6c7f8cc72c133f8990ccbe1075767f2af8c66014deafcecb0970f40c5cd->enter($internalf299b6c7f8cc72c133f8990ccbe1075767f2af8c66014deafcecb0970f40c5cdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 54
        echo "    ";
        $context["profilermarkupversion"] = ((arraykeyexists("profilermarkupversion", $context)) ? (twigdefaultfilter((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 54, $this->getSourceContext()); })()), 1)) : (1));
        // line 55
        echo "
    <span class=\"label ";
        // line 56
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 56, $this->getSourceContext()); })()), "messageCount", array())) ? ("") : ("disabled"));
        echo "\">
        ";
        // line 57
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 57, $this->getSourceContext()); })()) == 1)) {
            // line 58
            echo "            <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAeCAYAAABaKIzgAAAEDElEQVR42u2XWUhUURjHy6isILAebO+lonpJ8qkHM3NFVFxAUVFM0RSXUKnwwdAQn3wQE0MyA1MwBEUQcxvHZTTTHNcZl9DUGqd0JBOzcsZ7+n8XJ0Z0GueOVwg68GfmnLn3O7/7Lee7s4cxZpHq6uos0bb3+Q+6DrG3u7v7RHt7u3tbW9uD5ubm8qamJnlDQ4NKIpG8HhkZOU3X7BYoD9Tb22sjk8mcWltb70ul0pcAegugBfzOjKmzs/MJ7j0kCujw8PBhADkAKAVAz+EZGYA+08bmCN79qdFo7sGmjaWg+wgIIUtoaWl5CqDmxsbGj7SJpYK3uYWFBRnsDmEfWyGg+zs6OkIBNEoGxVB9fT2bnZ1VIHff03xmZuY29rUyF9QWT6sWC5I0OTk5rVAo3unnSqXyEfa1Nhf0Kp5UKRYk8lsDD0oM1/r6+l5h32Pmgl5UqVTP5ubmlEgBHRlCobC8vDzm5eXFHB0dRZWzs/OXsLCwu5SCpkBPo4DaMVRI9rbp6Wk1vnP+/v5kaFfk4eGhAcdJU6Dn+/v7q9aTnpPL5YqVlRV5eXm5Fk+7Gx7lCgsL68Fx2RToWST7C8McQgr8yMrKWkLu/hQz/LDNIZojycnJb8BxwRTocYT8oSEoQs8bSkpK0k5MTGgiIiK4nYYMDg7mcBLIo6OjP9Ec44opUGvIF+eoTg/a1dX1x2BISMgyKncpLS1tbacgU1NT2djY2HxoaOi8fg3jhilQK+gmQvBVD4qQbzDs4+ND6bBWUFCgtRQyJyeHdwSKdcODY9zaTgu9jheMcWOgJFdXV1ZZWclqamp0bm5uQoqGVVRUrFHBuru782tCQC+hW0j/BkpCPlGXIY9wfn5+5hQN5T3dS+cz5btg0DNTU1NFpkBra2tZaWkpy8jIYOPj4ywmJmY7RcMGBwdZZmYmKykpoa7ELPGozeLiYrIetKenZ5OhuLg4ft3T05OfJyQk8LDp6el/LRq6JiUlheb8vXgzY7m5uYJBD0LeeDnRApQ8sKloqK3GxsZuWEPrYzhiWHFx8ZZFMzo6yiIjIw1DTTZ+qNXqMRcXF0GgVpADKltDoCisDcbj4+NZfn7+ll5D9fKeprYbFRXFwsPDWVVV1SodPwEBAVueEtnZ2QNIhTkhoKRrAxhb5WhRURFzcnIyGmIcX9rq6uoPq6urAzqdrresrGwIHtMZux62OOT6AD4FgZ5bXl5+DMhv6P16j/KhCwoK2vHO5O3t/SsxMfG7ENAjkAOUBUkMvMVDiiFKDSGge6Gj0CUoGmffpvwSEfg7dUch/0LtkWcdvr6+a2JDBgYG6tDt6DXPTgjoKegOVAo1QVKR1AgVQ8GQrRDQA+uw9ushuSWSyLYdQRr7K/JP6DcTwr8i7Fj8pwAAAABJRU5ErkJggg==\" alt=\"Swiftmailer\" /></span>
        ";
        } else {
            // line 60
            echo "            <span class=\"icon\">";
            echo twiginclude($this->env, $context, "@Swiftmailer/Collector/icon.svg");
            echo "</span>
        ";
        }
        // line 62
        echo "
        <strong>E-Mails</strong>
        ";
        // line 64
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 64, $this->getSourceContext()); })()), "messageCount", array()) > 0)) {
            // line 65
            echo "            <span class=\"count\">
                <span>";
            // line 66
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 66, $this->getSourceContext()); })()), "messageCount", array()), "html", null, true);
            echo "</span>
            </span>
        ";
        }
        // line 69
        echo "    </span>
";
        
        $internalf299b6c7f8cc72c133f8990ccbe1075767f2af8c66014deafcecb0970f40c5cd->leave($internalf299b6c7f8cc72c133f8990ccbe1075767f2af8c66014deafcecb0970f40c5cdprof);

        
        $internal6d315d225c934c2c500f2e63ad1b703867d877cf114eefdc15aa193b2e5fe72b->leave($internal6d315d225c934c2c500f2e63ad1b703867d877cf114eefdc15aa193b2e5fe72bprof);

    }

    // line 72
    public function blockpanel($context, array $blocks = array())
    {
        $internal5d9a1f0dfc462aaed6e628f4e88ef3ddda3aa08156423aa68bb169e8598b05ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5d9a1f0dfc462aaed6e628f4e88ef3ddda3aa08156423aa68bb169e8598b05ca->enter($internal5d9a1f0dfc462aaed6e628f4e88ef3ddda3aa08156423aa68bb169e8598b05caprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal455f074a8374f87a9e32f0703248977fce43f98c796cc83c49d97354ade7957a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal455f074a8374f87a9e32f0703248977fce43f98c796cc83c49d97354ade7957a->enter($internal455f074a8374f87a9e32f0703248977fce43f98c796cc83c49d97354ade7957aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 73
        echo "    ";
        $context["profilermarkupversion"] = ((arraykeyexists("profilermarkupversion", $context)) ? (twigdefaultfilter((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 73, $this->getSourceContext()); })()), 1)) : (1));
        // line 74
        echo "
    ";
        // line 75
        if (((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 75, $this->getSourceContext()); })()) == 1)) {
            // line 76
            echo "        <style>
            h3 { margin-top: 2em; }
            h3 span { color: #999; font-weight: normal; }
            h3 small { color: #999; }
            h4 { font-size: 14px; font-weight: bold; }
            .card { background: #F5F5F5; margin: .5em 0 1em; padding: .5em; }
            .card .label { display: block; font-size: 13px; font-weight: bold; margin-bottom: .5em; }
            .card .card-block { margin-bottom: 1em; }
        </style>
    ";
        }
        // line 86
        echo "
    <h2>E-mails</h2>

    ";
        // line 89
        if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 89, $this->getSourceContext()); })()), "mailers", array())) {
            // line 90
            echo "        <div class=\"empty\">
            <p>No e-mail messages were sent.</p>
        </div>
    ";
        }
        // line 94
        echo "
    ";
        // line 95
        if ((((isset($context["profilermarkupversion"]) || arraykeyexists("profilermarkupversion", $context) ? $context["profilermarkupversion"] : (function () { throw new TwigErrorRuntime('Variable "profilermarkupversion" does not exist.', 95, $this->getSourceContext()); })()) == 1) || (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 95, $this->getSourceContext()); })()), "mailers", array())) > 1))) {
            // line 96
            echo "        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Mailer Name</th>
                    <th scope=\"col\">Num. of messages</th>
                    <th scope=\"col\">Messages status</th>
                    <th scope=\"col\">Notes</th>
                </tr>
            </thead>
            <tbody>
                ";
            // line 106
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 106, $this->getSourceContext()); })()), "mailers", array()));
            foreach ($context['seq'] as $context["key"] => $context["name"]) {
                // line 107
                echo "                    <tr>
                        <th class=\"font-normal\">";
                // line 108
                echo twigescapefilter($this->env, $context["name"], "html", null, true);
                echo "</th>
                        <td class=\"font-normal\">";
                // line 109
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 109, $this->getSourceContext()); })()), "messageCount", array(0 => $context["name"]), "method"), "html", null, true);
                echo "</td>
                        <td class=\"font-normal\">";
                // line 110
                echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 110, $this->getSourceContext()); })()), "isSpool", array(0 => $context["name"]), "method")) ? ("spooled") : ("sent"));
                echo "</td>
                        <td class=\"font-normal\">";
                // line 111
                echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 111, $this->getSourceContext()); })()), "isDefaultMailer", array(0 => $context["name"]), "method")) ? ("This is the default mailer") : (""));
                echo "</td>
                    </tr>
                ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['name'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 114
            echo "            </tbody>
        </table>
    ";
        } else {
            // line 117
            echo "        <div class=\"metrics\">
            ";
            // line 118
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 118, $this->getSourceContext()); })()), "mailers", array()));
            foreach ($context['seq'] as $context["key"] => $context["name"]) {
                // line 119
                echo "                <div class=\"metric\">
                    <span class=\"value\">";
                // line 120
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 120, $this->getSourceContext()); })()), "messageCount", array(0 => $context["name"]), "method"), "html", null, true);
                echo "</span>
                    <span class=\"label\">";
                // line 121
                echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 121, $this->getSourceContext()); })()), "isSpool", array(0 => $context["name"]), "method")) ? ("spooled") : ("sent"));
                echo " ";
                echo (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 121, $this->getSourceContext()); })()), "messageCount", array(0 => $context["name"]), "method") == 1)) ? ("message") : ("messages"));
                echo "</span>
                </div>
            ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['name'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 124
            echo "        </div>
    ";
        }
        // line 126
        echo "
    ";
        // line 127
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 127, $this->getSourceContext()); })()), "mailers", array()));
        foreach ($context['seq'] as $context["key"] => $context["name"]) {
            // line 128
            echo "        ";
            if ((twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 128, $this->getSourceContext()); })()), "mailers", array())) > 1)) {
                // line 129
                echo "            <h3>
                ";
                // line 130
                echo twigescapefilter($this->env, $context["name"], "html", null, true);
                echo " <span>mailer</span>
                <small>";
                // line 131
                echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 131, $this->getSourceContext()); })()), "isDefaultMailer", array(0 => $context["name"]), "method")) ? ("(default app mailer)") : (""));
                echo "</small>
            </h3>
        ";
            }
            // line 134
            echo "
        ";
            // line 135
            if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 135, $this->getSourceContext()); })()), "messages", array(0 => $context["name"]), "method")) {
                // line 136
                echo "            <div class=\"empty\">
                <p>No e-mail messages were sent.</p>
            </div>
        ";
            } else {
                // line 140
                echo "            ";
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 140, $this->getSourceContext()); })()), "messages", array(0 => $context["name"]), "method"));
                $context['loop'] = array(
                  'parent' => $context['parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                    $length = count($context['seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['seq'] as $context["key"] => $context["message"]) {
                    // line 141
                    echo "                ";
                    if ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "length", array()) > 1)) {
                        // line 142
                        echo "                    <h4>E-mail #";
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
                        echo " details</h4>
                ";
                    } else {
                        // line 144
                        echo "                    <h4>E-mail details</h4>
                ";
                    }
                    // line 146
                    echo "
                <div class=\"card\">
                    <div class=\"card-block\">
                        <span class=\"label\">Message headers</span>
                        <pre>";
                    // line 150
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "headers", array()), "all", array()));
                    foreach ($context['seq'] as $context["key"] => $context["header"]) {
                        // line 151
                        echo twigescapefilter($this->env, $context["header"], "html", null, true);
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['header'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 152
                    echo "</pre>
                    </div>

                    <div class=\"card-block\">
                        <span class=\"label\">Message body</span>
                        <pre>";
                    // line 158
                    if ((twiggetattribute($this->env, $this->getSourceContext(), ($context["messagePart"] ?? null), "charset", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "charset", array()))) {
                        // line 159
                        echo twigescapefilter($this->env, twigconvertencoding(twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "body", array()), "UTF-8", twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "charset", array())), "html", null, true);
                    } else {
                        // line 161
                        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "body", array()), "html", null, true);
                    }
                    // line 163
                    echo "</pre>
                    </div>

                    ";
                    // line 166
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["message"], "children", array()));
                    foreach ($context['seq'] as $context["key"] => $context["messagePart"]) {
                        if (twiginfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "contentType", array()), array(0 => "text/plain", 1 => "text/html"))) {
                            // line 167
                            echo "                        <div class=\"card-block\">
                            <span class=\"label\">Alternative part (";
                            // line 168
                            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "contentType", array()), "html", null, true);
                            echo ")</span>
                            <pre>";
                            // line 170
                            if ((twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "charset", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "charset", array()))) {
                                // line 171
                                echo twigescapefilter($this->env, twigconvertencoding(twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "body", array()), "UTF-8", twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "charset", array())), "html", null, true);
                            } else {
                                // line 173
                                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["messagePart"], "body", array()), "html", null, true);
                            }
                            // line 175
                            echo "</pre>
                        </div>
                    ";
                        }
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['messagePart'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 178
                    echo "
                    ";
                    // line 179
                    $context["attachments"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 179, $this->getSourceContext()); })()), "extractAttachments", array(0 => $context["message"]), "method");
                    // line 180
                    echo "                    ";
                    if ((isset($context["attachments"]) || arraykeyexists("attachments", $context) ? $context["attachments"] : (function () { throw new TwigErrorRuntime('Variable "attachments" does not exist.', 180, $this->getSourceContext()); })())) {
                        // line 181
                        echo "                        <div class=\"card-block\">
                            <span class=\"label\">
                                ";
                        // line 183
                        if ((twiglengthfilter($this->env, (isset($context["attachments"]) || arraykeyexists("attachments", $context) ? $context["attachments"] : (function () { throw new TwigErrorRuntime('Variable "attachments" does not exist.', 183, $this->getSourceContext()); })())) > 1)) {
                            // line 184
                            echo "                                    ";
                            echo twigescapefilter($this->env, twiglengthfilter($this->env, (isset($context["attachments"]) || arraykeyexists("attachments", $context) ? $context["attachments"] : (function () { throw new TwigErrorRuntime('Variable "attachments" does not exist.', 184, $this->getSourceContext()); })())), "html", null, true);
                            echo " Attachments
                                ";
                        } else {
                            // line 186
                            echo "                                    1 Attachment
                                ";
                        }
                        // line 188
                        echo "                            </span>

                            <ol>
                                ";
                        // line 191
                        $context['parent'] = $context;
                        $context['seq'] = twigensuretraversable((isset($context["attachments"]) || arraykeyexists("attachments", $context) ? $context["attachments"] : (function () { throw new TwigErrorRuntime('Variable "attachments" does not exist.', 191, $this->getSourceContext()); })()));
                        foreach ($context['seq'] as $context["key"] => $context["attachment"]) {
                            // line 192
                            echo "                                    <li>
                                        Filename:
                                        ";
                            // line 194
                            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["attachment"], "filename", array()), "html", null, true);
                            echo "
                                    </li>
                                ";
                        }
                        $parent = $context['parent'];
                        unset($context['seq'], $context['iterated'], $context['key'], $context['attachment'], $context['parent'], $context['loop']);
                        $context = arrayintersectkey($context, $parent) + $parent;
                        // line 197
                        echo "                            </ol>
                        </div>
                    ";
                    }
                    // line 200
                    echo "                </div>
            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['message'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 202
                echo "        ";
            }
            // line 203
            echo "    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['name'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        
        $internal455f074a8374f87a9e32f0703248977fce43f98c796cc83c49d97354ade7957a->leave($internal455f074a8374f87a9e32f0703248977fce43f98c796cc83c49d97354ade7957aprof);

        
        $internal5d9a1f0dfc462aaed6e628f4e88ef3ddda3aa08156423aa68bb169e8598b05ca->leave($internal5d9a1f0dfc462aaed6e628f4e88ef3ddda3aa08156423aa68bb169e8598b05caprof);

    }

    public function getTemplateName()
    {
        return "SwiftmailerBundle:Collector:swiftmailer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  588 => 203,  585 => 202,  570 => 200,  565 => 197,  556 => 194,  552 => 192,  548 => 191,  543 => 188,  539 => 186,  533 => 184,  531 => 183,  527 => 181,  524 => 180,  522 => 179,  519 => 178,  510 => 175,  507 => 173,  504 => 171,  502 => 170,  498 => 168,  495 => 167,  490 => 166,  485 => 163,  482 => 161,  479 => 159,  477 => 158,  470 => 152,  464 => 151,  460 => 150,  454 => 146,  450 => 144,  444 => 142,  441 => 141,  423 => 140,  417 => 136,  415 => 135,  412 => 134,  406 => 131,  402 => 130,  399 => 129,  396 => 128,  392 => 127,  389 => 126,  385 => 124,  374 => 121,  370 => 120,  367 => 119,  363 => 118,  360 => 117,  355 => 114,  346 => 111,  342 => 110,  338 => 109,  334 => 108,  331 => 107,  327 => 106,  315 => 96,  313 => 95,  310 => 94,  304 => 90,  302 => 89,  297 => 86,  285 => 76,  283 => 75,  280 => 74,  277 => 73,  268 => 72,  257 => 69,  251 => 66,  248 => 65,  246 => 64,  242 => 62,  236 => 60,  232 => 58,  230 => 57,  226 => 56,  223 => 55,  220 => 54,  211 => 53,  198 => 49,  195 => 48,  192 => 47,  189 => 46,  180 => 43,  176 => 42,  172 => 41,  169 => 40,  164 => 39,  161 => 38,  147 => 37,  143 => 35,  141 => 34,  135 => 31,  128 => 27,  124 => 26,  121 => 25,  103 => 24,  101 => 23,  95 => 20,  91 => 18,  89 => 17,  86 => 16,  83 => 15,  78 => 13,  73 => 12,  68 => 10,  65 => 9,  62 => 8,  59 => 7,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set profilermarkupversion = profilermarkupversion|default(1) %}

    {% if collector.messageCount %}
        {% set icon %}
            {% if profilermarkupversion == 1 %}
                <img width=\"23\" height=\"28\" alt=\"Swiftmailer\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAcCAYAAACK7SRjAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0NEOTU1MjM0OThFMTFFMDg3NzJBNTE2ODgwQzMxMzQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0NEOTU1MjQ0OThFMTFFMDg3NzJBNTE2ODgwQzMxMzQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxMEQ5OTQ5QzQ5OEMxMUUwODc3MkE1MTY4ODBDMzEzNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo3Q0Q5NTUyMjQ5OEUxMUUwODc3MkE1MTY4ODBDMzEzNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpkRnSAAAAJ0SURBVHjaYvz//z8DrQATAw3BqOFYAaO9vT1FBhw4cGCAXA5MipxBQUHT3r17l0AVAxkZ/wkLC89as2ZNIcjlYkALXKnlWqBZTH/+/PEDmQsynLW/v3+NoaHhN2oYDjJn8uTJK4BMNpDhPwsLCwOKiop2+fn5vafEYC8vrw8gc/Lz8wOB3B8gw/nev38vn5WV5eTg4LA/Ly/vESsrK2npmYmJITU19SnQ8L0gc4DxpwgyF2S4EEjB58+f+crLy31YWFjOt7S0XBYUFPxHjMEcHBz/6+rqboqJiZ0qKSnxBpkDlRICGc4MU/j792+2CRMm+L18+fLSxIkTDykoKPzBZ7CoqOi/np6eE8rKylvb29v9fvz4wYEkzYKRzjk5OX/LyMjcnDRpEkjjdisrK6wRraOj8wvokAMLFy788ejRoxcaGhrPCWai4ODgB8DUE3/mzBknYMToASNoMzAfvEVW4+Tk9LmhoWFbTU2NwunTpx2BjiiMjo6+hm4WCzJHUlLyz+vXrxkfP36sDOI/ffpUPikpibe7u3sLsJjQW7VqlSrQxe+Avjmanp7u9PbtWzGQOmCCkARmmu/m5uYfT548yY/V5UpKSl+2b9+uiiz26dMnIWBSDTp27NgdYGrYCIzwE7m5uR4wg2Hg/PnzSsDI/QlKOcjZ3wGUBLm5uf+DwLdv38gub4AG/xcSEvr35s0bZmCB5sgCE/z69SsjyDJKMtG/f/8YQQYD8wkoGf8H51AbG5sH1CpbQBnQ09PzBiiHgoIFFHlBQGwLxLxUMP8dqJgH4k3gIhfIkAKVYkDMTmmhCHIxEL8A4peMo02L4WU4QIABANxZAoDIQDv3AAAAAElFTkSuQmCC\" />
                <span class=\"sf-toolbar-status\">{{ collector.messageCount }}</span>
            {% else %}
                {{ include('@Swiftmailer/Collector/icon.svg') }}
                <span class=\"sf-toolbar-value\">{{ collector.messageCount }}</span>
            {% endif %}
        {% endset %}

        {% set text %}
            <div class=\"sf-toolbar-info-piece\">
                <b>Sent messages</b>
                <span class=\"sf-toolbar-status\">{{ collector.messageCount }}</span>
            </div>

            {% if profilermarkupversion == 1 %}
                {% for name in collector.mailers %}
                    <div class=\"sf-toolbar-info-piece\">
                        <b>{{ name }}</b>
                        <span>{{ collector.messageCount(name) }}</span>
                    </div>
                    <div class=\"sf-toolbar-info-piece\">
                        <b>Is spooled?</b>
                        <span>{{ collector.isSpool(name) ? 'yes' : 'no' }}</span>
                    </div>

                    {% if not loop.first %}
                        <hr>
                    {% endif %}
                {% endfor %}
            {% else %}
                {% for name in collector.mailers %}
                    <div class=\"sf-toolbar-info-piece\">
                        <b>{{ name }} mailer</b>
                        <span class=\"sf-toolbar-status\">{{ collector.messageCount(name)|default(0) }}</span>
                        &nbsp; (<small>{{ collector.isSpool(name) ? 'spooled' : 'sent' }}</small>)
                    </div>
                {% endfor %}
            {% endif %}
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbaritem.html.twig', { 'link': profilerurl }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    {% set profilermarkupversion = profilermarkupversion|default(1) %}

    <span class=\"label {{ collector.messageCount ? '' : 'disabled' }}\">
        {% if profilermarkupversion == 1 %}
            <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAeCAYAAABaKIzgAAAEDElEQVR42u2XWUhUURjHy6isILAebO+lonpJ8qkHM3NFVFxAUVFM0RSXUKnwwdAQn3wQE0MyA1MwBEUQcxvHZTTTHNcZl9DUGqd0JBOzcsZ7+n8XJ0Z0GueOVwg68GfmnLn3O7/7Lee7s4cxZpHq6uos0bb3+Q+6DrG3u7v7RHt7u3tbW9uD5ubm8qamJnlDQ4NKIpG8HhkZOU3X7BYoD9Tb22sjk8mcWltb70ul0pcAegugBfzOjKmzs/MJ7j0kCujw8PBhADkAKAVAz+EZGYA+08bmCN79qdFo7sGmjaWg+wgIIUtoaWl5CqDmxsbGj7SJpYK3uYWFBRnsDmEfWyGg+zs6OkIBNEoGxVB9fT2bnZ1VIHff03xmZuY29rUyF9QWT6sWC5I0OTk5rVAo3unnSqXyEfa1Nhf0Kp5UKRYk8lsDD0oM1/r6+l5h32Pmgl5UqVTP5ubmlEgBHRlCobC8vDzm5eXFHB0dRZWzs/OXsLCwu5SCpkBPo4DaMVRI9rbp6Wk1vnP+/v5kaFfk4eGhAcdJU6Dn+/v7q9aTnpPL5YqVlRV5eXm5Fk+7Gx7lCgsL68Fx2RToWST7C8McQgr8yMrKWkLu/hQz/LDNIZojycnJb8BxwRTocYT8oSEoQs8bSkpK0k5MTGgiIiK4nYYMDg7mcBLIo6OjP9Ec44opUGvIF+eoTg/a1dX1x2BISMgyKncpLS1tbacgU1NT2djY2HxoaOi8fg3jhilQK+gmQvBVD4qQbzDs4+ND6bBWUFCgtRQyJyeHdwSKdcODY9zaTgu9jheMcWOgJFdXV1ZZWclqamp0bm5uQoqGVVRUrFHBuru782tCQC+hW0j/BkpCPlGXIY9wfn5+5hQN5T3dS+cz5btg0DNTU1NFpkBra2tZaWkpy8jIYOPj4ywmJmY7RcMGBwdZZmYmKykpoa7ELPGozeLiYrIetKenZ5OhuLg4ft3T05OfJyQk8LDp6el/LRq6JiUlheb8vXgzY7m5uYJBD0LeeDnRApQ8sKloqK3GxsZuWEPrYzhiWHFx8ZZFMzo6yiIjIw1DTTZ+qNXqMRcXF0GgVpADKltDoCisDcbj4+NZfn7+ll5D9fKeprYbFRXFwsPDWVVV1SodPwEBAVueEtnZ2QNIhTkhoKRrAxhb5WhRURFzcnIyGmIcX9rq6uoPq6urAzqdrresrGwIHtMZux62OOT6AD4FgZ5bXl5+DMhv6P16j/KhCwoK2vHO5O3t/SsxMfG7ENAjkAOUBUkMvMVDiiFKDSGge6Gj0CUoGmffpvwSEfg7dUch/0LtkWcdvr6+a2JDBgYG6tDt6DXPTgjoKegOVAo1QVKR1AgVQ8GQrRDQA+uw9ushuSWSyLYdQRr7K/JP6DcTwr8i7Fj8pwAAAABJRU5ErkJggg==\" alt=\"Swiftmailer\" /></span>
        {% else %}
            <span class=\"icon\">{{ include('@Swiftmailer/Collector/icon.svg') }}</span>
        {% endif %}

        <strong>E-Mails</strong>
        {% if collector.messageCount > 0 %}
            <span class=\"count\">
                <span>{{ collector.messageCount }}</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    {% set profilermarkupversion = profilermarkupversion|default(1) %}

    {% if profilermarkupversion == 1 %}
        <style>
            h3 { margin-top: 2em; }
            h3 span { color: #999; font-weight: normal; }
            h3 small { color: #999; }
            h4 { font-size: 14px; font-weight: bold; }
            .card { background: #F5F5F5; margin: .5em 0 1em; padding: .5em; }
            .card .label { display: block; font-size: 13px; font-weight: bold; margin-bottom: .5em; }
            .card .card-block { margin-bottom: 1em; }
        </style>
    {% endif %}

    <h2>E-mails</h2>

    {% if not collector.mailers %}
        <div class=\"empty\">
            <p>No e-mail messages were sent.</p>
        </div>
    {% endif %}

    {% if profilermarkupversion == 1 or collector.mailers|length > 1 %}
        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Mailer Name</th>
                    <th scope=\"col\">Num. of messages</th>
                    <th scope=\"col\">Messages status</th>
                    <th scope=\"col\">Notes</th>
                </tr>
            </thead>
            <tbody>
                {% for name in collector.mailers %}
                    <tr>
                        <th class=\"font-normal\">{{ name }}</th>
                        <td class=\"font-normal\">{{ collector.messageCount(name) }}</td>
                        <td class=\"font-normal\">{{ collector.isSpool(name) ? 'spooled' : 'sent' }}</td>
                        <td class=\"font-normal\">{{ collector.isDefaultMailer(name) ? 'This is the default mailer' }}</td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>
    {% else %}
        <div class=\"metrics\">
            {% for name in collector.mailers %}
                <div class=\"metric\">
                    <span class=\"value\">{{ collector.messageCount(name) }}</span>
                    <span class=\"label\">{{ collector.isSpool(name) ? 'spooled' : 'sent' }} {{ collector.messageCount(name) == 1 ? 'message' : 'messages' }}</span>
                </div>
            {% endfor %}
        </div>
    {% endif %}

    {% for name in collector.mailers %}
        {% if collector.mailers|length > 1 %}
            <h3>
                {{ name }} <span>mailer</span>
                <small>{{ collector.isDefaultMailer(name) ? '(default app mailer)' }}</small>
            </h3>
        {% endif %}

        {% if not collector.messages(name) %}
            <div class=\"empty\">
                <p>No e-mail messages were sent.</p>
            </div>
        {% else %}
            {% for message in collector.messages(name) %}
                {% if loop.length > 1 %}
                    <h4>E-mail #{{ loop.index }} details</h4>
                {% else %}
                    <h4>E-mail details</h4>
                {% endif %}

                <div class=\"card\">
                    <div class=\"card-block\">
                        <span class=\"label\">Message headers</span>
                        <pre>{% for header in message.headers.all %}
                            {{- header -}}
                        {% endfor %}</pre>
                    </div>

                    <div class=\"card-block\">
                        <span class=\"label\">Message body</span>
                        <pre>
                            {%- if messagePart.charset is defined and message.charset %}
                                {{- message.body|convertencoding('UTF-8', message.charset) }}
                            {%- else %}
                                {{- message.body }}
                            {%- endif -%}
                        </pre>
                    </div>

                    {% for messagePart in message.children if messagePart.contentType in ['text/plain', 'text/html'] %}
                        <div class=\"card-block\">
                            <span class=\"label\">Alternative part ({{ messagePart.contentType }})</span>
                            <pre>
                                {%- if messagePart.charset is defined and messagePart.charset %}
                                    {{- messagePart.body|convertencoding('UTF-8', messagePart.charset) }}
                                {%- else %}
                                    {{- messagePart.body }}
                                {%- endif -%}
                            </pre>
                        </div>
                    {% endfor %}

                    {% set attachments = collector.extractAttachments(message) %}
                    {% if attachments %}
                        <div class=\"card-block\">
                            <span class=\"label\">
                                {% if attachments|length > 1 %}
                                    {{ attachments|length }} Attachments
                                {% else %}
                                    1 Attachment
                                {% endif %}
                            </span>

                            <ol>
                                {% for attachment in attachments %}
                                    <li>
                                        Filename:
                                        {{ attachment.filename }}
                                    </li>
                                {% endfor %}
                            </ol>
                        </div>
                    {% endif %}
                </div>
            {% endfor %}
        {% endif %}
    {% endfor %}
{% endblock %}
", "SwiftmailerBundle:Collector:swiftmailer.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/swiftmailer-bundle/Resources/views/Collector/swiftmailer.html.twig");
    }
}
