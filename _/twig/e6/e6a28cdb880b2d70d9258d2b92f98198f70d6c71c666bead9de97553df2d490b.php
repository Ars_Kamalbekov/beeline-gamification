<?php

/* FOSUserBundle:Resetting:resetcontent.html.twig */
class TwigTemplate6033689a91fd8fe73869ca8bdbe14eb07870e8d6d6fbf49019b4719d3158b332 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3e0997921c93b86317dfa0a849c4e5b6d467eb83ac6862a1e4f5ba380a9a797b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3e0997921c93b86317dfa0a849c4e5b6d467eb83ac6862a1e4f5ba380a9a797b->enter($internal3e0997921c93b86317dfa0a849c4e5b6d467eb83ac6862a1e4f5ba380a9a797bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:resetcontent.html.twig"));

        $internal3ff10079aa650e77952ea34c939538f3dc82c918adee97fb91de39e5d718815d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3ff10079aa650e77952ea34c939538f3dc82c918adee97fb91de39e5d718815d->enter($internal3ff10079aa650e77952ea34c939538f3dc82c918adee97fb91de39e5d718815dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:resetcontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserresettingreset", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 3, $this->getSourceContext()); })()))), "attr" => array("class" => "fosuserresettingreset")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.reset.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internal3e0997921c93b86317dfa0a849c4e5b6d467eb83ac6862a1e4f5ba380a9a797b->leave($internal3e0997921c93b86317dfa0a849c4e5b6d467eb83ac6862a1e4f5ba380a9a797bprof);

        
        $internal3ff10079aa650e77952ea34c939538f3dc82c918adee97fb91de39e5d718815d->leave($internal3ff10079aa650e77952ea34c939538f3dc82c918adee97fb91de39e5d718815dprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:resetcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, { 'action': path('fosuserresettingreset', {'token': token}), 'attr': { 'class': 'fosuserresettingreset' } }) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'resetting.reset.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:Resetting:resetcontent.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/resetcontent.html.twig");
    }
}
