<?php

/* SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig */
class TwigTemplate6146a022f353f5ddbc200dd7963060d4395fcaa4f29672504baf82d3e0dd2b94 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb553e4ef853e3ebab4f7140b6c2fe6ebb4df1bb55689b551bb506a0c41f28a86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb553e4ef853e3ebab4f7140b6c2fe6ebb4df1bb55689b551bb506a0c41f28a86->enter($internalb553e4ef853e3ebab4f7140b6c2fe6ebb4df1bb55689b551bb506a0c41f28a86prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig"));

        $internal275147d55c4bcc73785a0e3383e4fef89c65229d1face6f978b3de242c65ebc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal275147d55c4bcc73785a0e3383e4fef89c65229d1face6f978b3de242c65ebc1->enter($internal275147d55c4bcc73785a0e3383e4fef89c65229d1face6f978b3de242c65ebc1prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig"));

        // line 11
        echo "<script type=\"text/javascript\">
    jQuery('div#fieldcontainer";
        // line 12
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 12, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs').sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> div',
        stop: applypositionvalue";
        // line 16
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo "
    });

    function applypositionvalue";
        // line 19
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
        echo "() {
        // update the input value position
        jQuery('div#fieldcontainer";
        // line 21
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs .sonata-ba-field-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 21, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 21, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo "').each(function(index, element) {
            // remove the sortable handler and put it back
            var parent = jQuery(element).closest('.nav-tabs-custom');
            var tabs = parent.find('> .nav-tabs');
            jQuery('.sonata-ba-sortable-handler', tabs).remove();
            \$('<li class=\"sonata-ba-sortable-handler pull-right\"></li>')
                    .prepend('<span class=\"ui-icon ui-icon-grip-solid-horizontal\"></span>')
                    .appendTo(tabs);
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer";
        // line 32
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs .sonata-ba-field-";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 32, $this->getSourceContext()); })()), "html", null, true);
        echo "-";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 32, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "sortable", array()), "html", null, true);
        echo " input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-";
        // line 38
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 38, $this->getSourceContext()); })()), "html", null, true);
        echo "').bind('sonata.addelement', function() {
        applypositionvalue";
        // line 39
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 39, $this->getSourceContext()); })()), "html", null, true);
        echo "();
        jQuery('div#fieldcontainer";
        // line 40
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
        echo " .sonata-ba-tabs').sortable('refresh');
    });

    applypositionvalue";
        // line 43
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 43, $this->getSourceContext()); })()), "html", null, true);
        echo "();
</script>
";
        
        $internalb553e4ef853e3ebab4f7140b6c2fe6ebb4df1bb55689b551bb506a0c41f28a86->leave($internalb553e4ef853e3ebab4f7140b6c2fe6ebb4df1bb55689b551bb506a0c41f28a86prof);

        
        $internal275147d55c4bcc73785a0e3383e4fef89c65229d1face6f978b3de242c65ebc1->leave($internal275147d55c4bcc73785a0e3383e4fef89c65229d1face6f978b3de242c65ebc1prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 43,  85 => 40,  81 => 39,  77 => 38,  64 => 32,  46 => 21,  41 => 19,  35 => 16,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<script type=\"text/javascript\">
    jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs').sortable({
        axis: 'y',
        opacity: 0.6,
        items: '> div',
        stop: applypositionvalue{{ id }}
    });

    function applypositionvalue{{ id }}() {
        // update the input value position
        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs .sonata-ba-field-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }}').each(function(index, element) {
            // remove the sortable handler and put it back
            var parent = jQuery(element).closest('.nav-tabs-custom');
            var tabs = parent.find('> .nav-tabs');
            jQuery('.sonata-ba-sortable-handler', tabs).remove();
            \$('<li class=\"sonata-ba-sortable-handler pull-right\"></li>')
                    .prepend('<span class=\"ui-icon ui-icon-grip-solid-horizontal\"></span>')
                    .appendTo(tabs);
            jQuery('input', element).hide();
        });

        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs .sonata-ba-field-{{ id }}-{{ sonataadmin.fielddescription.options.sortable }} input').each(function(index, value) {
            jQuery(value).val(index + 1);
        });
    }

    // refresh the sortable option when a new element is added
    jQuery('#sonata-ba-field-container-{{ id }}').bind('sonata.addelement', function() {
        applypositionvalue{{ id }}();
        jQuery('div#fieldcontainer{{ id }} .sonata-ba-tabs').sortable('refresh');
    });

    applypositionvalue{{ id }}();
</script>
", "SonataDoctrineORMAdminBundle:CRUD:editormonetomanysortablescripttabs.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editormonetomanysortablescripttabs.html.twig");
    }
}
