<?php

/* TwigBundle:Exception:traces.txt.twig */
class TwigTemplate3257d4a6f2ca5f5cfdc2cb3c27878800d57ca934f27ccd40d1ec3217961bcb0f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6f7f9b9b7bfeffad05de9551928aa9586df072e1a0914f6143b28c9a41cb4290 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6f7f9b9b7bfeffad05de9551928aa9586df072e1a0914f6143b28c9a41cb4290->enter($internal6f7f9b9b7bfeffad05de9551928aa9586df072e1a0914f6143b28c9a41cb4290prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        $internala5cc57844b8f0b154028c57d4fa9e1d0fb692761d584e73c8183c8c8bf1202e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala5cc57844b8f0b154028c57d4fa9e1d0fb692761d584e73c8183c8c8bf1202e3->enter($internala5cc57844b8f0b154028c57d4fa9e1d0fb692761d584e73c8183c8c8bf1202e3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.txt.twig"));

        // line 1
        if (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "trace", array()))) {
            // line 2
            echo "<pre class=\"stacktrace\">
";
            // line 3
            echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 3, $this->getSourceContext()); })()), "class", array());
            echo ":
";
            // line 4
            if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 4, $this->getSourceContext()); })()), "message", array()))) {
                // line 5
                echo twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 5, $this->getSourceContext()); })()), "message", array());
                echo "
";
            }
            // line 7
            echo "
";
            // line 8
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 8, $this->getSourceContext()); })()), "trace", array()));
            foreach ($context['seq'] as $context["key"] => $context["trace"]) {
                // line 9
                echo "  ";
                echo twiginclude($this->env, $context, "@Twig/Exception/trace.txt.twig", array("trace" => $context["trace"]), false);
                echo "
";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['trace'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 11
            echo "</pre>
";
        }
        
        $internal6f7f9b9b7bfeffad05de9551928aa9586df072e1a0914f6143b28c9a41cb4290->leave($internal6f7f9b9b7bfeffad05de9551928aa9586df072e1a0914f6143b28c9a41cb4290prof);

        
        $internala5cc57844b8f0b154028c57d4fa9e1d0fb692761d584e73c8183c8c8bf1202e3->leave($internala5cc57844b8f0b154028c57d4fa9e1d0fb692761d584e73c8183c8c8bf1202e3prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 11,  48 => 9,  44 => 8,  41 => 7,  36 => 5,  34 => 4,  30 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% if exception.trace|length %}
<pre class=\"stacktrace\">
{{ exception.class }}:
{% if exception.message is not empty %}
    {{- exception.message }}
{% endif %}

{% for trace in exception.trace %}
  {{ include('@Twig/Exception/trace.txt.twig', { trace: trace }, withcontext = false) }}
{% endfor %}
</pre>
{% endif %}
", "TwigBundle:Exception:traces.txt.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
