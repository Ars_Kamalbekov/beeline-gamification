<?php

/* SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig */
class TwigTemplatec6b49c7416bd890811e4d69237cf338f7e2e710bcb073a2fed5e876f7cff2050 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal13e4843fa034568c4f3274a36cd57f8238692bade7f7068e0c274843c0245754 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal13e4843fa034568c4f3274a36cd57f8238692bade7f7068e0c274843c0245754->enter($internal13e4843fa034568c4f3274a36cd57f8238692bade7f7068e0c274843c0245754prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig"));

        $internalcf0a8fa9710f332cc05d1b32ff87b954c6baad375fb35ba8208d44e9fdae032f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalcf0a8fa9710f332cc05d1b32ff87b954c6baad375fb35ba8208d44e9fdae032f->enter($internalcf0a8fa9710f332cc05d1b32ff87b954c6baad375fb35ba8208d44e9fdae032fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal13e4843fa034568c4f3274a36cd57f8238692bade7f7068e0c274843c0245754->leave($internal13e4843fa034568c4f3274a36cd57f8238692bade7f7068e0c274843c0245754prof);

        
        $internalcf0a8fa9710f332cc05d1b32ff87b954c6baad375fb35ba8208d44e9fdae032f->leave($internalcf0a8fa9710f332cc05d1b32ff87b954c6baad375fb35ba8208d44e9fdae032fprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal54cfe716abb09c2ca16172034c8b64dd690e0066b591d09d99d4a328a4ba0790 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal54cfe716abb09c2ca16172034c8b64dd690e0066b591d09d99d4a328a4ba0790->enter($internal54cfe716abb09c2ca16172034c8b64dd690e0066b591d09d99d4a328a4ba0790prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal81759d16e8056c0b0180f03e4f6ed396e1f256b93fbd18a0158edfb2b9ae5b07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal81759d16e8056c0b0180f03e4f6ed396e1f256b93fbd18a0158edfb2b9ae5b07->enter($internal81759d16e8056c0b0180f03e4f6ed396e1f256b93fbd18a0158edfb2b9ae5b07prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <ul class=\"sonata-ba-show-many-to-many\">
    ";
        // line 16
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 17
        echo "        ";
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["element"]) {
            // line 18
            echo "            <li>
                ";
            // line 19
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 20
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 21
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 21, $this->getSourceContext()); })()), 1 => $context["element"]), "method"))) {
                // line 22
                echo "                    <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 22, $this->getSourceContext()); })()), 1 => $context["element"], 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                        ";
                // line 23
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 23, $this->getSourceContext()); })())), "html", null, true);
                echo "
                    </a>
                ";
            } else {
                // line 26
                echo "                    ";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement($context["element"], (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 26, $this->getSourceContext()); })())), "html", null, true);
                echo "
                ";
            }
            // line 28
            echo "            </li>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 30
        echo "    </ul>
";
        
        $internal81759d16e8056c0b0180f03e4f6ed396e1f256b93fbd18a0158edfb2b9ae5b07->leave($internal81759d16e8056c0b0180f03e4f6ed396e1f256b93fbd18a0158edfb2b9ae5b07prof);

        
        $internal54cfe716abb09c2ca16172034c8b64dd690e0066b591d09d99d4a328a4ba0790->leave($internal54cfe716abb09c2ca16172034c8b64dd690e0066b591d09d99d4a328a4ba0790prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 30,  83 => 28,  77 => 26,  71 => 23,  66 => 22,  64 => 21,  63 => 20,  62 => 19,  59 => 18,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    <ul class=\"sonata-ba-show-many-to-many\">
    {% set routename = fielddescription.options.route.name %}
        {% for element in value %}
            <li>
                {% if fielddescription.hasassociationadmin
                and fielddescription.associationadmin.hasRoute(routename)
                and fielddescription.associationadmin.hasAccess(routename, element) %}
                    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
                        {{ element|renderrelationelement(fielddescription) }}
                    </a>
                {% else %}
                    {{ element|renderrelationelement(fielddescription) }}
                {% endif %}
            </li>
        {% endfor %}
    </ul>
{% endblock %}
", "SonataDoctrineORMAdminBundle:CRUD:showormmanytomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/showormmanytomany.html.twig");
    }
}
