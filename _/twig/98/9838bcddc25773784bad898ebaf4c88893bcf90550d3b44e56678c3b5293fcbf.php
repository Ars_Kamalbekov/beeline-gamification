<?php

/* SonataAdminBundle:CRUD:showurl.html.twig */
class TwigTemplate115cc6e614ba167abff3936b03c5d41a362fbbba8a8c6b2df93239fa76e6ff58 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showurl.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal21e338f3d2c550028d568edf3c8aa8213c2a28d9e6af83d2db48ece25bf76a34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal21e338f3d2c550028d568edf3c8aa8213c2a28d9e6af83d2db48ece25bf76a34->enter($internal21e338f3d2c550028d568edf3c8aa8213c2a28d9e6af83d2db48ece25bf76a34prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showurl.html.twig"));

        $internald0fb09c786cacfc4696233c06741042926fbe74797e4dc23fe0135622f805471 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald0fb09c786cacfc4696233c06741042926fbe74797e4dc23fe0135622f805471->enter($internald0fb09c786cacfc4696233c06741042926fbe74797e4dc23fe0135622f805471prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showurl.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal21e338f3d2c550028d568edf3c8aa8213c2a28d9e6af83d2db48ece25bf76a34->leave($internal21e338f3d2c550028d568edf3c8aa8213c2a28d9e6af83d2db48ece25bf76a34prof);

        
        $internald0fb09c786cacfc4696233c06741042926fbe74797e4dc23fe0135622f805471->leave($internald0fb09c786cacfc4696233c06741042926fbe74797e4dc23fe0135622f805471prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal459d8933659733d1b9f698f180e29da94074dcca8c982f86262616401225747f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal459d8933659733d1b9f698f180e29da94074dcca8c982f86262616401225747f->enter($internal459d8933659733d1b9f698f180e29da94074dcca8c982f86262616401225747fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal60d0d72943811ffdeaea3f54e78ab0cc4a9ae65fc517f921e02a03acd57d0b56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal60d0d72943811ffdeaea3f54e78ab0cc4a9ae65fc517f921e02a03acd57d0b56->enter($internal60d0d72943811ffdeaea3f54e78ab0cc4a9ae65fc517f921e02a03acd57d0b56prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        obstart();
        // line 16
        echo "    ";
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 16, $this->getSourceContext()); })()))) {
            // line 17
            echo "        &nbsp;
    ";
        } else {
            // line 19
            echo "        ";
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "url", array(), "any", true, true)) {
                // line 20
                echo "            ";
                // line 21
                echo "            ";
                $context["urladdress"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })()), "options", array()), "url", array());
                // line 22
                echo "        ";
            } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", true, true) && !twiginfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 22, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), array(0 => "edit", 1 => "show")))) {
                // line 23
                echo "            ";
                // line 24
                echo "            ";
                $context["parameters"] = ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "parameters", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "parameters", array()), array())) : (array()));
                // line 25
                echo "
            ";
                // line 27
                echo "            ";
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "identifierparametername", array(), "any", true, true)) {
                    // line 28
                    echo "                ";
                    $context["parameters"] = twigarraymerge((isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 28, $this->getSourceContext()); })()), array(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 28, $this->getSourceContext()); })()), "options", array()), "route", array()), "identifierparametername", array()) => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 28, $this->getSourceContext()); })()), "normalizedidentifier", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 28, $this->getSourceContext()); })())), "method")));
                    // line 29
                    echo "            ";
                }
                // line 30
                echo "
            ";
                // line 31
                if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "absolute", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "route", array(), "any", false, true), "absolute", array()), false)) : (false))) {
                    // line 32
                    echo "                ";
                    $context["urladdress"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getUrl(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 32, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), (isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 32, $this->getSourceContext()); })()));
                    // line 33
                    echo "            ";
                } else {
                    // line 34
                    echo "                ";
                    $context["urladdress"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array()), (isset($context["parameters"]) || arraykeyexists("parameters", $context) ? $context["parameters"] : (function () { throw new TwigErrorRuntime('Variable "parameters" does not exist.', 34, $this->getSourceContext()); })()));
                    // line 35
                    echo "            ";
                }
                // line 36
                echo "        ";
            } else {
                // line 37
                echo "            ";
                // line 38
                echo "            ";
                $context["urladdress"] = (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 38, $this->getSourceContext()); })());
                // line 39
                echo "        ";
            }
            // line 40
            echo "
        ";
            // line 41
            if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "hideprotocol", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "hideprotocol", array()), false)) : (false))) {
                // line 42
                echo "            ";
                $context["value"] = twigreplacefilter((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 42, $this->getSourceContext()); })()), array("http://" => "", "https://" => ""));
                // line 43
                echo "        ";
            }
            // line 44
            echo "
        <a
            href=\"";
            // line 46
            echo twigescapefilter($this->env, (isset($context["urladdress"]) || arraykeyexists("urladdress", $context) ? $context["urladdress"] : (function () { throw new TwigErrorRuntime('Variable "urladdress" does not exist.', 46, $this->getSourceContext()); })()), "html", null, true);
            echo "\"";
            // line 47
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable(((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "attributes", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), ($context["fielddescription"] ?? null), "options", array(), "any", false, true), "attributes", array()), array())) : (array())));
            foreach ($context['seq'] as $context["attribute"] => $context["value"]) {
                // line 48
                echo "                ";
                echo twigescapefilter($this->env, $context["attribute"], "html", null, true);
                echo "=\"";
                echo twigescapefilter($this->env, $context["value"], "htmlattr");
                echo "\"";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['attribute'], $context['value'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 50
            echo ">";
            // line 51
            if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 51, $this->getSourceContext()); })()), "options", array()), "safe", array())) {
                // line 52
                echo (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 52, $this->getSourceContext()); })());
            } else {
                // line 54
                echo twigescapefilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 54, $this->getSourceContext()); })()), "html", null, true);
            }
            // line 56
            echo "</a>
    ";
        }
        echo trim(pregreplace('/>\s+</', '><', obgetclean()));
        
        $internal60d0d72943811ffdeaea3f54e78ab0cc4a9ae65fc517f921e02a03acd57d0b56->leave($internal60d0d72943811ffdeaea3f54e78ab0cc4a9ae65fc517f921e02a03acd57d0b56prof);

        
        $internal459d8933659733d1b9f698f180e29da94074dcca8c982f86262616401225747f->leave($internal459d8933659733d1b9f698f180e29da94074dcca8c982f86262616401225747fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showurl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 56,  153 => 54,  150 => 52,  148 => 51,  146 => 50,  136 => 48,  132 => 47,  129 => 46,  125 => 44,  122 => 43,  119 => 42,  117 => 41,  114 => 40,  111 => 39,  108 => 38,  106 => 37,  103 => 36,  100 => 35,  97 => 34,  94 => 33,  91 => 32,  89 => 31,  86 => 30,  83 => 29,  80 => 28,  77 => 27,  74 => 25,  71 => 24,  69 => 23,  66 => 22,  63 => 21,  61 => 20,  58 => 19,  54 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
{% spaceless %}
    {% if value is empty %}
        &nbsp;
    {% else %}
        {% if fielddescription.options.url is defined %}
            {# target url is string #}
            {% set urladdress = fielddescription.options.url %}
        {% elseif fielddescription.options.route is defined and fielddescription.options.route.name not in ['edit', 'show'] %}
            {# target url is Symfony route #}
            {% set parameters = fielddescription.options.route.parameters|default([]) %}

            {# route with paramter related to object ID #}
            {% if fielddescription.options.route.identifierparametername is defined %}
                {% set parameters = parameters|merge({(fielddescription.options.route.identifierparametername):(admin.normalizedidentifier(object))}) %}
            {% endif %}

            {% if fielddescription.options.route.absolute|default(false) %}
                {% set urladdress = url(fielddescription.options.route.name, parameters) %}
            {% else %}
                {% set urladdress = path(fielddescription.options.route.name, parameters) %}
            {% endif %}
        {% else %}
            {# value is url #}
            {% set urladdress = value %}
        {% endif %}

        {% if fielddescription.options.hideprotocol|default(false) %}
            {% set value = value|replace({'http://': '', 'https://': ''}) %}
        {% endif %}

        <a
            href=\"{{ urladdress }}\"
            {%- for attribute, value in fielddescription.options.attributes|default([]) %}
                {{ attribute }}=\"{{ value|escape('htmlattr') }}\"
            {%- endfor -%}
        >
            {%- if fielddescription.options.safe -%}
                {{- value|raw -}}
            {%- else -%}
                {{- value -}}
            {%- endif -%}
        </a>
    {% endif %}
{% endspaceless %}
{% endblock %}
", "SonataAdminBundle:CRUD:showurl.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showurl.html.twig");
    }
}
