<?php

/* SonataAdminBundle:Core:tabmenutemplate.html.twig */
class TwigTemplatedb654a2fde7c8bbeefc3a19c0a1976a69aacc58a3f43edcc32a51794ff429846 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Core:tabmenutemplate.html.twig", 1);
        $this->blocks = array(
            'item' => array($this, 'blockitem'),
            'dividerElement' => array($this, 'blockdividerElement'),
            'linkElement' => array($this, 'blocklinkElement'),
            'spanElement' => array($this, 'blockspanElement'),
            'dropdownElement' => array($this, 'blockdropdownElement'),
            'label' => array($this, 'blocklabel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knpmenu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal13eb502919a801b075ba3f6a27a532fd4ac7516c3a01616136048d895d85d9ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal13eb502919a801b075ba3f6a27a532fd4ac7516c3a01616136048d895d85d9ef->enter($internal13eb502919a801b075ba3f6a27a532fd4ac7516c3a01616136048d895d85d9efprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:tabmenutemplate.html.twig"));

        $internalc662bfd76e7c99c7834a9f97e8e2ea35d355b43df0ed9df4f59d70a08ffd6174 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc662bfd76e7c99c7834a9f97e8e2ea35d355b43df0ed9df4f59d70a08ffd6174->enter($internalc662bfd76e7c99c7834a9f97e8e2ea35d355b43df0ed9df4f59d70a08ffd6174prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:tabmenutemplate.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal13eb502919a801b075ba3f6a27a532fd4ac7516c3a01616136048d895d85d9ef->leave($internal13eb502919a801b075ba3f6a27a532fd4ac7516c3a01616136048d895d85d9efprof);

        
        $internalc662bfd76e7c99c7834a9f97e8e2ea35d355b43df0ed9df4f59d70a08ffd6174->leave($internalc662bfd76e7c99c7834a9f97e8e2ea35d355b43df0ed9df4f59d70a08ffd6174prof);

    }

    // line 3
    public function blockitem($context, array $blocks = array())
    {
        $internal4425fd32aadbab48bafffd973fd8b8a3c31454a97ea3de48d9b44c55657cc7e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4425fd32aadbab48bafffd973fd8b8a3c31454a97ea3de48d9b44c55657cc7e0->enter($internal4425fd32aadbab48bafffd973fd8b8a3c31454a97ea3de48d9b44c55657cc7e0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        $internal5f8a4d3d0dcba61d311251ca8c592b564f20867d4e22f82de46af737f8c39984 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5f8a4d3d0dcba61d311251ca8c592b564f20867d4e22f82de46af737f8c39984->enter($internal5f8a4d3d0dcba61d311251ca8c592b564f20867d4e22f82de46af737f8c39984prof = new TwigProfilerProfile($this->getTemplateName(), "block", "item"));

        // line 4
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Core:tabmenutemplate.html.twig", 4);
        // line 5
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 5, $this->getSourceContext()); })()), "displayed", array())) {
            // line 6
            $context["attributes"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 6, $this->getSourceContext()); })()), "attributes", array());
            // line 7
            $context["isdropdown"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dropdown", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dropdown", array()), false)) : (false));
            // line 8
            $context["dividerprepend"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dividerprepend", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dividerprepend", array()), false)) : (false));
            // line 9
            $context["dividerappend"] = ((twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dividerappend", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["attributes"] ?? null), "dividerappend", array()), false)) : (false));
            // line 10
            echo "
";
            // line 12
            $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 12, $this->getSourceContext()); })()), array("dropdown" => null, "dividerprepend" => null, "dividerappend" => null));
            // line 14
            if ((isset($context["dividerprepend"]) || arraykeyexists("dividerprepend", $context) ? $context["dividerprepend"] : (function () { throw new TwigErrorRuntime('Variable "dividerprepend" does not exist.', 14, $this->getSourceContext()); })())) {
                // line 15
                echo "        ";
                $this->displayBlock("dividerElement", $context, $blocks);
            }
            // line 17
            echo "
";
            // line 19
            $context["classes"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 19, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 19, $this->getSourceContext()); })()), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 21
            if (arraykeyexists("matcher", $context)) {
                echo " ";
                // line 22
                if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 22, $this->getSourceContext()); })()), "isCurrent", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 22, $this->getSourceContext()); })())), "method")) {
                    // line 23
                    $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 23, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 23, $this->getSourceContext()); })()), "currentClass", array())));
                } elseif (twiggetattribute($this->env, $this->getSourceContext(),                 // line 24
(isset($context["matcher"]) || arraykeyexists("matcher", $context) ? $context["matcher"] : (function () { throw new TwigErrorRuntime('Variable "matcher" does not exist.', 24, $this->getSourceContext()); })()), "isAncestor", array(0 => (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 24, $this->getSourceContext()); })()), 1 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 24, $this->getSourceContext()); })()), "depth", array())), "method")) {
                    // line 25
                    $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 25, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 25, $this->getSourceContext()); })()), "ancestorClass", array())));
                }
            } else {
                // line 27
                echo " ";
                // line 28
                if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 28, $this->getSourceContext()); })()), "current", array())) {
                    // line 29
                    $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 29, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 29, $this->getSourceContext()); })()), "currentClass", array())));
                } elseif (twiggetattribute($this->env, $this->getSourceContext(),                 // line 30
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 30, $this->getSourceContext()); })()), "currentAncestor", array())) {
                    // line 31
                    $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 31, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 31, $this->getSourceContext()); })()), "ancestorClass", array())));
                }
            }
            // line 35
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 35, $this->getSourceContext()); })()), "actsLikeFirst", array())) {
                // line 36
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 36, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 36, $this->getSourceContext()); })()), "firstClass", array())));
            }
            // line 38
            if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 38, $this->getSourceContext()); })()), "actsLikeLast", array())) {
                // line 39
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 39, $this->getSourceContext()); })()), array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 39, $this->getSourceContext()); })()), "lastClass", array())));
            }
            // line 41
            echo "
";
            // line 43
            $context["childrenClasses"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 43, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 43, $this->getSourceContext()); })()), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 44
            $context["childrenClasses"] = twigarraymerge((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 44, $this->getSourceContext()); })()), array(0 => ("menulevel" . twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 44, $this->getSourceContext()); })()), "level", array()))));
            // line 45
            echo "
";
            // line 47
            if ((isset($context["isdropdown"]) || arraykeyexists("isdropdown", $context) ? $context["isdropdown"] : (function () { throw new TwigErrorRuntime('Variable "isdropdown" does not exist.', 47, $this->getSourceContext()); })())) {
                // line 48
                $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 48, $this->getSourceContext()); })()), array(0 => "dropdown"));
                // line 49
                $context["childrenClasses"] = twigarraymerge((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 49, $this->getSourceContext()); })()), array(0 => "dropdown-menu"));
            }
            // line 51
            echo "
";
            // line 53
            if ( !twigtestempty((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 53, $this->getSourceContext()); })()))) {
                // line 54
                $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 54, $this->getSourceContext()); })()), array("class" => twigjoinfilter((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 54, $this->getSourceContext()); })()), " ")));
            }
            // line 56
            $context["listAttributes"] = twigarraymerge(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 56, $this->getSourceContext()); })()), "childrenAttributes", array()), array("class" => twigjoinfilter((isset($context["childrenClasses"]) || arraykeyexists("childrenClasses", $context) ? $context["childrenClasses"] : (function () { throw new TwigErrorRuntime('Variable "childrenClasses" does not exist.', 56, $this->getSourceContext()); })()), " ")));
            // line 57
            echo "
";
            // line 59
            echo "    <li";
            echo $context["macros"]->macroattributes((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 59, $this->getSourceContext()); })()));
            echo ">";
            // line 60
            if ((isset($context["isdropdown"]) || arraykeyexists("isdropdown", $context) ? $context["isdropdown"] : (function () { throw new TwigErrorRuntime('Variable "isdropdown" does not exist.', 60, $this->getSourceContext()); })())) {
                // line 61
                echo "            ";
                $this->displayBlock("dropdownElement", $context, $blocks);
            } elseif (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(),             // line 62
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 62, $this->getSourceContext()); })()), "uri", array())) && ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 62, $this->getSourceContext()); })()), "current", array()) || twiggetattribute($this->env, $this->getSourceContext(), (isset($context["options"]) || arraykeyexists("options", $context) ? $context["options"] : (function () { throw new TwigErrorRuntime('Variable "options" does not exist.', 62, $this->getSourceContext()); })()), "currentAsLink", array())))) {
                // line 63
                echo "            ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 65
                echo "            ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 68
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    </li>";
            // line 71
            if ((isset($context["dividerappend"]) || arraykeyexists("dividerappend", $context) ? $context["dividerappend"] : (function () { throw new TwigErrorRuntime('Variable "dividerappend" does not exist.', 71, $this->getSourceContext()); })())) {
                // line 72
                echo "        ";
                $this->displayBlock("dividerElement", $context, $blocks);
            }
        }
        
        $internal5f8a4d3d0dcba61d311251ca8c592b564f20867d4e22f82de46af737f8c39984->leave($internal5f8a4d3d0dcba61d311251ca8c592b564f20867d4e22f82de46af737f8c39984prof);

        
        $internal4425fd32aadbab48bafffd973fd8b8a3c31454a97ea3de48d9b44c55657cc7e0->leave($internal4425fd32aadbab48bafffd973fd8b8a3c31454a97ea3de48d9b44c55657cc7e0prof);

    }

    // line 77
    public function blockdividerElement($context, array $blocks = array())
    {
        $internala1419d94286e5623379c91c8a668e736378e05491fbdf88d80660cd7784129b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala1419d94286e5623379c91c8a668e736378e05491fbdf88d80660cd7784129b3->enter($internala1419d94286e5623379c91c8a668e736378e05491fbdf88d80660cd7784129b3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "dividerElement"));

        $internale8b75afb0590a53c14775fe8fdc6630ace1003de1dbfea384f344a2352eea88d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale8b75afb0590a53c14775fe8fdc6630ace1003de1dbfea384f344a2352eea88d->enter($internale8b75afb0590a53c14775fe8fdc6630ace1003de1dbfea384f344a2352eea88dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "dividerElement"));

        // line 78
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 78, $this->getSourceContext()); })()), "level", array()) == 1)) {
            // line 79
            echo "    <li class=\"divider-vertical\"></li>
";
        } else {
            // line 81
            echo "    <li class=\"divider\"></li>
";
        }
        
        $internale8b75afb0590a53c14775fe8fdc6630ace1003de1dbfea384f344a2352eea88d->leave($internale8b75afb0590a53c14775fe8fdc6630ace1003de1dbfea384f344a2352eea88dprof);

        
        $internala1419d94286e5623379c91c8a668e736378e05491fbdf88d80660cd7784129b3->leave($internala1419d94286e5623379c91c8a668e736378e05491fbdf88d80660cd7784129b3prof);

    }

    // line 85
    public function blocklinkElement($context, array $blocks = array())
    {
        $internal0b7c3e1aee073cd449bc6552a57b605db2827325af38135c81ebdd9cc421fc31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0b7c3e1aee073cd449bc6552a57b605db2827325af38135c81ebdd9cc421fc31->enter($internal0b7c3e1aee073cd449bc6552a57b605db2827325af38135c81ebdd9cc421fc31prof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        $internal4cc417143a603a65ac71e91d584b9c43fccbb8a9e22a2237413ad813628197d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4cc417143a603a65ac71e91d584b9c43fccbb8a9e22a2237413ad813628197d2->enter($internal4cc417143a603a65ac71e91d584b9c43fccbb8a9e22a2237413ad813628197d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "linkElement"));

        // line 86
        echo "    ";
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Core:tabmenutemplate.html.twig", 86);
        // line 87
        echo "    <a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 87, $this->getSourceContext()); })()), "uri", array()), "html", null, true);
        echo "\"";
        echo $context["macros"]->macroattributes(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 87, $this->getSourceContext()); })()), "linkAttributes", array()));
        echo ">
        ";
        // line 88
        if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 88, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"))) {
            // line 89
            echo "            <i class=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 89, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"), "html", null, true);
            echo "\"></i>
        ";
        }
        // line 91
        echo "        ";
        $this->displayBlock("label", $context, $blocks);
        echo "
    </a>
";
        
        $internal4cc417143a603a65ac71e91d584b9c43fccbb8a9e22a2237413ad813628197d2->leave($internal4cc417143a603a65ac71e91d584b9c43fccbb8a9e22a2237413ad813628197d2prof);

        
        $internal0b7c3e1aee073cd449bc6552a57b605db2827325af38135c81ebdd9cc421fc31->leave($internal0b7c3e1aee073cd449bc6552a57b605db2827325af38135c81ebdd9cc421fc31prof);

    }

    // line 95
    public function blockspanElement($context, array $blocks = array())
    {
        $internal5a52b86017af5b61e555fbc38f8e054ccc691fef35f61c82911a9f9d1af8a08d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5a52b86017af5b61e555fbc38f8e054ccc691fef35f61c82911a9f9d1af8a08d->enter($internal5a52b86017af5b61e555fbc38f8e054ccc691fef35f61c82911a9f9d1af8a08dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        $internal86c4a38cb9a458b5c2712718d4c249725bfae49b263f2e54d3348922a7d57771 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal86c4a38cb9a458b5c2712718d4c249725bfae49b263f2e54d3348922a7d57771->enter($internal86c4a38cb9a458b5c2712718d4c249725bfae49b263f2e54d3348922a7d57771prof = new TwigProfilerProfile($this->getTemplateName(), "block", "spanElement"));

        // line 96
        echo "    ";
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Core:tabmenutemplate.html.twig", 96);
        // line 97
        echo "    <span ";
        echo $context["macros"]->macroattributes(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 97, $this->getSourceContext()); })()), "labelAttributes", array()));
        echo ">
        ";
        // line 98
        if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 98, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"))) {
            // line 99
            echo "            <i class=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 99, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"), "html", null, true);
            echo "\"></i>
        ";
        }
        // line 101
        echo "        ";
        $this->displayBlock("label", $context, $blocks);
        echo "
    </span>
";
        
        $internal86c4a38cb9a458b5c2712718d4c249725bfae49b263f2e54d3348922a7d57771->leave($internal86c4a38cb9a458b5c2712718d4c249725bfae49b263f2e54d3348922a7d57771prof);

        
        $internal5a52b86017af5b61e555fbc38f8e054ccc691fef35f61c82911a9f9d1af8a08d->leave($internal5a52b86017af5b61e555fbc38f8e054ccc691fef35f61c82911a9f9d1af8a08dprof);

    }

    // line 105
    public function blockdropdownElement($context, array $blocks = array())
    {
        $internalb92c0859f63b86bcb9a85292c3aefac4e67247cea494f36d3e4ffa87d714794d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb92c0859f63b86bcb9a85292c3aefac4e67247cea494f36d3e4ffa87d714794d->enter($internalb92c0859f63b86bcb9a85292c3aefac4e67247cea494f36d3e4ffa87d714794dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "dropdownElement"));

        $internala32443fe32fde9242fc999a63d21c70da01c003b609214bbf1e9b6cd5398390c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala32443fe32fde9242fc999a63d21c70da01c003b609214bbf1e9b6cd5398390c->enter($internala32443fe32fde9242fc999a63d21c70da01c003b609214bbf1e9b6cd5398390cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "dropdownElement"));

        // line 106
        echo "    ";
        $context["macros"] = $this->loadTemplate("knpmenu.html.twig", "SonataAdminBundle:Core:tabmenutemplate.html.twig", 106);
        // line 107
        $context["classes"] = (( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 107, $this->getSourceContext()); })()), "linkAttribute", array(0 => "class"), "method"))) ? (array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 107, $this->getSourceContext()); })()), "linkAttribute", array(0 => "class"), "method"))) : (array()));
        // line 108
        $context["classes"] = twigarraymerge((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 108, $this->getSourceContext()); })()), array(0 => "dropdown-toggle"));
        // line 109
        $context["attributes"] = twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 109, $this->getSourceContext()); })()), "linkAttributes", array());
        // line 110
        $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 110, $this->getSourceContext()); })()), array("class" => twigjoinfilter((isset($context["classes"]) || arraykeyexists("classes", $context) ? $context["classes"] : (function () { throw new TwigErrorRuntime('Variable "classes" does not exist.', 110, $this->getSourceContext()); })()), " ")));
        // line 111
        $context["attributes"] = twigarraymerge((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 111, $this->getSourceContext()); })()), array("data-toggle" => "dropdown"));
        // line 112
        echo "    <a href=\"#\"";
        echo $context["macros"]->macroattributes((isset($context["attributes"]) || arraykeyexists("attributes", $context) ? $context["attributes"] : (function () { throw new TwigErrorRuntime('Variable "attributes" does not exist.', 112, $this->getSourceContext()); })()));
        echo ">
        ";
        // line 113
        if ( !twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 113, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"))) {
            // line 114
            echo "            <i class=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 114, $this->getSourceContext()); })()), "attribute", array(0 => "icon"), "method"), "html", null, true);
            echo "\"></i>
        ";
        }
        // line 116
        echo "        ";
        $this->displayBlock("label", $context, $blocks);
        echo "
        <b class=\"caret\"></b>
    </a>
";
        
        $internala32443fe32fde9242fc999a63d21c70da01c003b609214bbf1e9b6cd5398390c->leave($internala32443fe32fde9242fc999a63d21c70da01c003b609214bbf1e9b6cd5398390cprof);

        
        $internalb92c0859f63b86bcb9a85292c3aefac4e67247cea494f36d3e4ffa87d714794d->leave($internalb92c0859f63b86bcb9a85292c3aefac4e67247cea494f36d3e4ffa87d714794dprof);

    }

    // line 121
    public function blocklabel($context, array $blocks = array())
    {
        $internala87a33f3027473afe6e0ea995005d9b591d31c1ca7312dad4874cd158edee94f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala87a33f3027473afe6e0ea995005d9b591d31c1ca7312dad4874cd158edee94f->enter($internala87a33f3027473afe6e0ea995005d9b591d31c1ca7312dad4874cd158edee94fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        $internal6fca3cec41269783cae7df70fe9e9d2eacdbbea3da7ae9775ed771ca2d654a6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6fca3cec41269783cae7df70fe9e9d2eacdbbea3da7ae9775ed771ca2d654a6c->enter($internal6fca3cec41269783cae7df70fe9e9d2eacdbbea3da7ae9775ed771ca2d654a6cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "label"));

        // line 122
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(),         // line 123
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 123, $this->getSourceContext()); })()), "label", array()), twiggetattribute($this->env, $this->getSourceContext(),         // line 124
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 124, $this->getSourceContext()); })()), "getExtra", array(0 => "translationparams", 1 => array()), "method"), twiggetattribute($this->env, $this->getSourceContext(),         // line 125
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 125, $this->getSourceContext()); })()), "getExtra", array(0 => "translationdomain", 1 => ((twiggetattribute($this->env, $this->getSourceContext(),         // line 127
(isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 127, $this->getSourceContext()); })()), "getParent", array(), "method")) ? (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["item"]) || arraykeyexists("item", $context) ? $context["item"] : (function () { throw new TwigErrorRuntime('Variable "item" does not exist.', 127, $this->getSourceContext()); })()), "getParent", array(), "method"), "getExtra", array(0 => "translationdomain"), "method")) : (null))), "method")), "html", null, true);
        
        $internal6fca3cec41269783cae7df70fe9e9d2eacdbbea3da7ae9775ed771ca2d654a6c->leave($internal6fca3cec41269783cae7df70fe9e9d2eacdbbea3da7ae9775ed771ca2d654a6cprof);

        
        $internala87a33f3027473afe6e0ea995005d9b591d31c1ca7312dad4874cd158edee94f->leave($internala87a33f3027473afe6e0ea995005d9b591d31c1ca7312dad4874cd158edee94fprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:tabmenutemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 127,  355 => 125,  354 => 124,  353 => 123,  352 => 122,  343 => 121,  328 => 116,  322 => 114,  320 => 113,  315 => 112,  313 => 111,  311 => 110,  309 => 109,  307 => 108,  305 => 107,  302 => 106,  293 => 105,  279 => 101,  273 => 99,  271 => 98,  266 => 97,  263 => 96,  254 => 95,  240 => 91,  234 => 89,  232 => 88,  225 => 87,  222 => 86,  213 => 85,  201 => 81,  197 => 79,  195 => 78,  186 => 77,  173 => 72,  171 => 71,  166 => 68,  162 => 65,  158 => 63,  156 => 62,  153 => 61,  151 => 60,  147 => 59,  144 => 57,  142 => 56,  139 => 54,  137 => 53,  134 => 51,  131 => 49,  129 => 48,  127 => 47,  124 => 45,  122 => 44,  120 => 43,  117 => 41,  114 => 39,  112 => 38,  109 => 36,  107 => 35,  103 => 31,  101 => 30,  99 => 29,  97 => 28,  95 => 27,  91 => 25,  89 => 24,  87 => 23,  85 => 22,  82 => 21,  80 => 19,  77 => 17,  73 => 15,  71 => 14,  69 => 12,  66 => 10,  64 => 9,  62 => 8,  60 => 7,  58 => 6,  56 => 5,  54 => 4,  45 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends 'knpmenu.html.twig' %}

{% block item %}
{% import \"knpmenu.html.twig\" as macros %}
{% if item.displayed %}
    {%- set attributes = item.attributes %}
    {%- set isdropdown = attributes.dropdown|default(false) %}
    {%- set dividerprepend = attributes.dividerprepend|default(false) %}
    {%- set dividerappend = attributes.dividerappend|default(false) %}

{# unset bootstrap specific attributes #}
    {%- set attributes = attributes|merge({'dropdown': null, 'dividerprepend': null, 'dividerappend': null }) %}

    {%- if dividerprepend %}
        {{ block('dividerElement') }}
    {%- endif %}

{# building the class of the item #}
    {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}

    {%- if matcher is defined %} {# KnpMenu 2.0#}
        {%- if matcher.isCurrent(item) %}
            {%- set classes = classes|merge([options.currentClass]) %}
        {%- elseif matcher.isAncestor(item, options.depth) %}
            {%- set classes = classes|merge([options.ancestorClass]) %}
        {%- endif %}
    {%- else %} {# KnpMenu 1.X #}
        {%- if item.current %}
        {%- set classes = classes|merge([options.currentClass]) %}
        {%- elseif item.currentAncestor %}
        {%- set classes = classes|merge([options.ancestorClass]) %}
        {%- endif %}
    {%- endif %}

    {%- if item.actsLikeFirst %}
        {%- set classes = classes|merge([options.firstClass]) %}
    {%- endif %}
    {%- if item.actsLikeLast %}
        {%- set classes = classes|merge([options.lastClass]) %}
    {%- endif %}

{# building the class of the children #}
    {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}
    {%- set childrenClasses = childrenClasses|merge(['menulevel' ~ item.level]) %}

{# adding classes for dropdown #}
    {%- if isdropdown %}
        {%- set classes = classes|merge(['dropdown']) %}
        {%- set childrenClasses = childrenClasses|merge(['dropdown-menu']) %}
    {%- endif %}

{# putting classes together #}
    {%- if classes is not empty %}
        {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
    {%- endif %}
    {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}

{# displaying the item #}
    <li{{ macros.attributes(attributes) }}>
        {%- if isdropdown %}
            {{ block('dropdownElement') }}
        {%- elseif item.uri is not empty and (not item.current or options.currentAsLink) %}
            {{ block('linkElement') }}
        {%- else %}
            {{ block('spanElement') }}
        {%- endif %}
{# render the list of children#}
        {{ block('list') }}
    </li>

    {%- if dividerappend %}
        {{ block('dividerElement') }}
    {%- endif %}
{% endif %}
{% endblock %}

{% block dividerElement %}
{% if item.level == 1 %}
    <li class=\"divider-vertical\"></li>
{% else %}
    <li class=\"divider\"></li>
{% endif %}
{% endblock %}

{% block linkElement %}
    {% import \"knpmenu.html.twig\" as macros %}
    <a href=\"{{ item.uri }}\"{{ macros.attributes(item.linkAttributes) }}>
        {% if item.attribute('icon') is not empty  %}
            <i class=\"{{ item.attribute('icon') }}\"></i>
        {% endif %}
        {{ block('label') }}
    </a>
{% endblock %}

{% block spanElement %}
    {% import \"knpmenu.html.twig\" as macros %}
    <span {{ macros.attributes(item.labelAttributes) }}>
        {% if item.attribute('icon') is not empty  %}
            <i class=\"{{ item.attribute('icon') }}\"></i>
        {% endif %}
        {{ block('label') }}
    </span>
{% endblock %}

{% block dropdownElement %}
    {% import \"knpmenu.html.twig\" as macros %}
    {%- set classes = item.linkAttribute('class') is not empty ? [item.linkAttribute('class')] : [] %}
    {%- set classes = classes|merge(['dropdown-toggle']) %}
    {%- set attributes = item.linkAttributes %}
    {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
    {%- set attributes = attributes|merge({'data-toggle': 'dropdown'}) %}
    <a href=\"#\"{{ macros.attributes(attributes) }}>
        {% if item.attribute('icon') is not empty  %}
            <i class=\"{{ item.attribute('icon') }}\"></i>
        {% endif %}
        {{ block('label') }}
        <b class=\"caret\"></b>
    </a>
{% endblock %}

{% block label %}
{{-
    item.label|trans(
        item.getExtra('translationparams', {}),
        item.getExtra(
            'translationdomain',
            item.getParent() ? item.getParent().getExtra('translationdomain') : null
        )
    )
-}}
{% endblock %}
", "SonataAdminBundle:Core:tabmenutemplate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/tabmenutemplate.html.twig");
    }
}
