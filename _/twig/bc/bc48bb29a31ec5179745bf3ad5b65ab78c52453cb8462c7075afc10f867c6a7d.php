<?php

/* @WebProfiler/Icon/logger.svg */
class TwigTemplated2fb40deea2cc9b09432ddf0a44a5325259335970e64e6030d518b6b47b71f6d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0062c60d2c1758915a43d9d47602bbab490cc97678f36c53097e6311488fc2a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0062c60d2c1758915a43d9d47602bbab490cc97678f36c53097e6311488fc2a5->enter($internal0062c60d2c1758915a43d9d47602bbab490cc97678f36c53097e6311488fc2a5prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/logger.svg"));

        $internal322a84e1ffef6a257eb0738ae634e75a48e3ae14946f3dd6d510c73e31416cdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal322a84e1ffef6a257eb0738ae634e75a48e3ae14946f3dd6d510c73e31416cdd->enter($internal322a84e1ffef6a257eb0738ae634e75a48e3ae14946f3dd6d510c73e31416cddprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@WebProfiler/Icon/logger.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21,4v13.8c0,2.7-2.5,5.2-5.2,5.2H6c-0.6,0-1-0.4-1-1s0.4-1,1-1h9.8c1.6,0,3.2-1.7,3.2-3.2V4
    c0-0.6,0.4-1,1-1S21,3.4,21,4z M5.5,20C4.1,20,3,18.9,3,17.5V3.5C3,2.1,4.1,1,5.5,1h10.1C16.9,1,18,2.1,18,3.5v14.1
    c0,1.4-1.1,2.5-2.5,2.5H5.5z M9,11.4C9,11.7,9.3,12,9.6,12h1.8c0.3,0,0.6-0.3,0.6-0.6V4.6C12,4.3,11.7,4,11.4,4H9.6
    C9.3,4,9,4.3,9,4.6V11.4z M9,16.4C9,16.7,9.3,17,9.6,17h1.8c0.3,0,0.6-0.3,0.6-0.6v-1.8c0-0.3-0.3-0.6-0.6-0.6H9.6
    C9.3,14,9,14.3,9,14.6V16.4z\"/>
</svg>
";
        
        $internal0062c60d2c1758915a43d9d47602bbab490cc97678f36c53097e6311488fc2a5->leave($internal0062c60d2c1758915a43d9d47602bbab490cc97678f36c53097e6311488fc2a5prof);

        
        $internal322a84e1ffef6a257eb0738ae634e75a48e3ae14946f3dd6d510c73e31416cdd->leave($internal322a84e1ffef6a257eb0738ae634e75a48e3ae14946f3dd6d510c73e31416cddprof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/logger.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M21,4v13.8c0,2.7-2.5,5.2-5.2,5.2H6c-0.6,0-1-0.4-1-1s0.4-1,1-1h9.8c1.6,0,3.2-1.7,3.2-3.2V4
    c0-0.6,0.4-1,1-1S21,3.4,21,4z M5.5,20C4.1,20,3,18.9,3,17.5V3.5C3,2.1,4.1,1,5.5,1h10.1C16.9,1,18,2.1,18,3.5v14.1
    c0,1.4-1.1,2.5-2.5,2.5H5.5z M9,11.4C9,11.7,9.3,12,9.6,12h1.8c0.3,0,0.6-0.3,0.6-0.6V4.6C12,4.3,11.7,4,11.4,4H9.6
    C9.3,4,9,4.3,9,4.6V11.4z M9,16.4C9,16.7,9.3,17,9.6,17h1.8c0.3,0,0.6-0.3,0.6-0.6v-1.8c0-0.3-0.3-0.6-0.6-0.6H9.6
    C9.3,14,9,14.3,9,14.6V16.4z\"/>
</svg>
", "@WebProfiler/Icon/logger.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/logger.svg");
    }
}
