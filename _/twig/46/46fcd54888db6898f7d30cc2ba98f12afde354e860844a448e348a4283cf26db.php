<?php

/* @Framework/Form/choicewidget.html.php */
class TwigTemplate0c4772bca31b91a5490e5486285c90e5ae8d2cbe6e833bd1d1ce7c8060b7c7c0 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalade132efc87a46b533dbfa18152aace82ffafcae7d47205d8972c93c359b21c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalade132efc87a46b533dbfa18152aace82ffafcae7d47205d8972c93c359b21c3->enter($internalade132efc87a46b533dbfa18152aace82ffafcae7d47205d8972c93c359b21c3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidget.html.php"));

        $internal9bdc67f9e82d3edef2e9edbb43406e82a99768419057b8faae2fad266fbdf236 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9bdc67f9e82d3edef2e9edbb43406e82a99768419057b8faae2fad266fbdf236->enter($internal9bdc67f9e82d3edef2e9edbb43406e82a99768419057b8faae2fad266fbdf236prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/choicewidget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choicewidgetexpanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choicewidgetcollapsed') ?>
<?php endif ?>
";
        
        $internalade132efc87a46b533dbfa18152aace82ffafcae7d47205d8972c93c359b21c3->leave($internalade132efc87a46b533dbfa18152aace82ffafcae7d47205d8972c93c359b21c3prof);

        
        $internal9bdc67f9e82d3edef2e9edbb43406e82a99768419057b8faae2fad266fbdf236->leave($internal9bdc67f9e82d3edef2e9edbb43406e82a99768419057b8faae2fad266fbdf236prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choicewidget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choicewidgetexpanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choicewidgetcollapsed') ?>
<?php endif ?>
", "@Framework/Form/choicewidget.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choicewidget.html.php");
    }
}
