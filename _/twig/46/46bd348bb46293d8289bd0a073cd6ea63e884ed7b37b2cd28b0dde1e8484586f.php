<?php

/* FOSUserBundle:Group:newcontent.html.twig */
class TwigTemplate222876e832a2d7fe526d26786ee2c4030955e8d9e5c3472c821c6a06b590a499 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal768ef3526dd539d76076138eb6575aaecc04e50281079bc3e9f82af204d175a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal768ef3526dd539d76076138eb6575aaecc04e50281079bc3e9f82af204d175a4->enter($internal768ef3526dd539d76076138eb6575aaecc04e50281079bc3e9f82af204d175a4prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:newcontent.html.twig"));

        $internal6e0144a9e99f834c7a4427a2b5f86c543e60ff369a1e795e367197928586abdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6e0144a9e99f834c7a4427a2b5f86c543e60ff369a1e795e367197928586abdf->enter($internal6e0144a9e99f834c7a4427a2b5f86c543e60ff369a1e795e367197928586abdfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:newcontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusergroupnew"), "attr" => array("class" => "fosusergroupnew")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internal768ef3526dd539d76076138eb6575aaecc04e50281079bc3e9f82af204d175a4->leave($internal768ef3526dd539d76076138eb6575aaecc04e50281079bc3e9f82af204d175a4prof);

        
        $internal6e0144a9e99f834c7a4427a2b5f86c543e60ff369a1e795e367197928586abdf->leave($internal6e0144a9e99f834c7a4427a2b5f86c543e60ff369a1e795e367197928586abdfprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:newcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, { 'action': path('fosusergroupnew'), 'attr': { 'class': 'fosusergroupnew' } }) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:Group:newcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/newcontent.html.twig");
    }
}
