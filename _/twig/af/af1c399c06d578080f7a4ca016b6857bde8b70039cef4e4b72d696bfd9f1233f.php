<?php

/* SonataAdminBundle:CRUD/Association:listonetomany.html.twig */
class TwigTemplatecccf62f4be238500be574d2bac96ea587dbcf6285f93e48672b60292c0c973e9 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
            'relationlink' => array($this, 'blockrelationlink'),
            'relationvalue' => array($this, 'blockrelationvalue'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD/Association:listonetomany.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6ed5889812508927c7298c5bd88eac2ee250a40173b72f0387f87d64f3c210bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6ed5889812508927c7298c5bd88eac2ee250a40173b72f0387f87d64f3c210bf->enter($internal6ed5889812508927c7298c5bd88eac2ee250a40173b72f0387f87d64f3c210bfprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listonetomany.html.twig"));

        $internal26d50712d76e4c8d78c168eebc1c84178e7f841ece07c82730198256679eb604 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal26d50712d76e4c8d78c168eebc1c84178e7f841ece07c82730198256679eb604->enter($internal26d50712d76e4c8d78c168eebc1c84178e7f841ece07c82730198256679eb604prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:listonetomany.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal6ed5889812508927c7298c5bd88eac2ee250a40173b72f0387f87d64f3c210bf->leave($internal6ed5889812508927c7298c5bd88eac2ee250a40173b72f0387f87d64f3c210bfprof);

        
        $internal26d50712d76e4c8d78c168eebc1c84178e7f841ece07c82730198256679eb604->leave($internal26d50712d76e4c8d78c168eebc1c84178e7f841ece07c82730198256679eb604prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal0317e6e7a140a11c6fa999a7a2767dd5d4eae41ac768a388fdf45f98910c2dd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0317e6e7a140a11c6fa999a7a2767dd5d4eae41ac768a388fdf45f98910c2dd7->enter($internal0317e6e7a140a11c6fa999a7a2767dd5d4eae41ac768a388fdf45f98910c2dd7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal8a65d5ec3bf3e6b3af51f61bf651ebd5841fd73e366ad79e7212cf3996115b09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8a65d5ec3bf3e6b3af51f61bf651ebd5841fd73e366ad79e7212cf3996115b09->enter($internal8a65d5ec3bf3e6b3af51f61bf651ebd5841fd73e366ad79e7212cf3996115b09prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 15, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "hasassociationadmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 16, $this->getSourceContext()); })())), "method"))) {
            // line 17
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 17, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 18
                if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })()), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relationlink", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relationvalue", $context, $blocks);
                }
                // line 23
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 26, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relationvalue", $context, $blocks);
                if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 28
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['element'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 29
            echo "    ";
        }
        
        $internal8a65d5ec3bf3e6b3af51f61bf651ebd5841fd73e366ad79e7212cf3996115b09->leave($internal8a65d5ec3bf3e6b3af51f61bf651ebd5841fd73e366ad79e7212cf3996115b09prof);

        
        $internal0317e6e7a140a11c6fa999a7a2767dd5d4eae41ac768a388fdf45f98910c2dd7->leave($internal0317e6e7a140a11c6fa999a7a2767dd5d4eae41ac768a388fdf45f98910c2dd7prof);

    }

    // line 32
    public function blockrelationlink($context, array $blocks = array())
    {
        $internal4e0466b5ce6bcb2eaed60f1dde87945207eb75140371e32ccdfe172a7a5994b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4e0466b5ce6bcb2eaed60f1dde87945207eb75140371e32ccdfe172a7a5994b7->enter($internal4e0466b5ce6bcb2eaed60f1dde87945207eb75140371e32ccdfe172a7a5994b7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        $internal45fe4fd2c6e9e5f9739459afbd76e7900bdaff543bfe2eec72affc36dcc13f02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal45fe4fd2c6e9e5f9739459afbd76e7900bdaff543bfe2eec72affc36dcc13f02->enter($internal45fe4fd2c6e9e5f9739459afbd76e7900bdaff543bfe2eec72affc36dcc13f02prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationlink"));

        // line 33
        echo "<a href=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 33, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 33, $this->getSourceContext()); })()), 1 => (isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 33, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 33, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 34
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 34, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 34, $this->getSourceContext()); })())), "html", null, true);
        // line 35
        echo "</a>";
        
        $internal45fe4fd2c6e9e5f9739459afbd76e7900bdaff543bfe2eec72affc36dcc13f02->leave($internal45fe4fd2c6e9e5f9739459afbd76e7900bdaff543bfe2eec72affc36dcc13f02prof);

        
        $internal4e0466b5ce6bcb2eaed60f1dde87945207eb75140371e32ccdfe172a7a5994b7->leave($internal4e0466b5ce6bcb2eaed60f1dde87945207eb75140371e32ccdfe172a7a5994b7prof);

    }

    // line 38
    public function blockrelationvalue($context, array $blocks = array())
    {
        $internal9742dd3ea857ee33eb8361cc82907aa76af7d6235ab9eeda23937597ba1ecb81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9742dd3ea857ee33eb8361cc82907aa76af7d6235ab9eeda23937597ba1ecb81->enter($internal9742dd3ea857ee33eb8361cc82907aa76af7d6235ab9eeda23937597ba1ecb81prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        $internal6df75853c0900cf09e186d92ff0c29e1a01f06f1356e9945e6c1957c67c52ed9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6df75853c0900cf09e186d92ff0c29e1a01f06f1356e9945e6c1957c67c52ed9->enter($internal6df75853c0900cf09e186d92ff0c29e1a01f06f1356e9945e6c1957c67c52ed9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "relationvalue"));

        // line 39
        echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) || arraykeyexists("element", $context) ? $context["element"] : (function () { throw new TwigErrorRuntime('Variable "element" does not exist.', 39, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 39, $this->getSourceContext()); })())), "html", null, true);
        
        $internal6df75853c0900cf09e186d92ff0c29e1a01f06f1356e9945e6c1957c67c52ed9->leave($internal6df75853c0900cf09e186d92ff0c29e1a01f06f1356e9945e6c1957c67c52ed9prof);

        
        $internal9742dd3ea857ee33eb8361cc82907aa76af7d6235ab9eeda23937597ba1ecb81->leave($internal9742dd3ea857ee33eb8361cc82907aa76af7d6235ab9eeda23937597ba1ecb81prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:listonetomany.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 39,  177 => 38,  167 => 35,  165 => 34,  161 => 33,  152 => 32,  141 => 29,  127 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field %}
    {% set routename = fielddescription.options.route.name %}
    {% if fielddescription.hasassociationadmin and fielddescription.associationadmin.hasRoute(routename) %}
        {% for element in value %}
            {%- if fielddescription.associationadmin.hasAccess(routename, element) -%}
                {{ block('relationlink') }}
            {%- else -%}
                {{ block('relationvalue') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value %}
            {{ block('relationvalue') }}{% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relationlink -%}
    <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, element, fielddescription.options.route.parameters) }}\">
        {{- element|renderrelationelement(fielddescription) -}}
    </a>
{%- endblock -%}

{%- block relationvalue -%}
    {{- element|renderrelationelement(fielddescription) -}}
{%- endblock -%}
", "SonataAdminBundle:CRUD/Association:listonetomany.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/Association/listonetomany.html.twig");
    }
}
