<?php

/* FOSUserBundle:Profile:edit.html.twig */
class TwigTemplateaf7c61c0478b5df8d320eeb94ce6b963d1a818998d129833287caf5ff2cfe4bd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal90224e174c010f1271b610e6553e703bebc96bfb8ecb369433c74e65f2daa17d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal90224e174c010f1271b610e6553e703bebc96bfb8ecb369433c74e65f2daa17d->enter($internal90224e174c010f1271b610e6553e703bebc96bfb8ecb369433c74e65f2daa17dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $internal885f72902309742233e99f7fc722b705bc34d7c5683791632004b440a3e0c5bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal885f72902309742233e99f7fc722b705bc34d7c5683791632004b440a3e0c5bd->enter($internal885f72902309742233e99f7fc722b705bc34d7c5683791632004b440a3e0c5bdprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal90224e174c010f1271b610e6553e703bebc96bfb8ecb369433c74e65f2daa17d->leave($internal90224e174c010f1271b610e6553e703bebc96bfb8ecb369433c74e65f2daa17dprof);

        
        $internal885f72902309742233e99f7fc722b705bc34d7c5683791632004b440a3e0c5bd->leave($internal885f72902309742233e99f7fc722b705bc34d7c5683791632004b440a3e0c5bdprof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal98ed97ff612916bbb82c101606abe4d607f35f90eaff9f2aa432315c660f19d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal98ed97ff612916bbb82c101606abe4d607f35f90eaff9f2aa432315c660f19d3->enter($internal98ed97ff612916bbb82c101606abe4d607f35f90eaff9f2aa432315c660f19d3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal3266867601b9e312b9dc59261b9732a59893b926cb7cd32369c62bd9eeabdf3e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3266867601b9e312b9dc59261b9732a59893b926cb7cd32369c62bd9eeabdf3e->enter($internal3266867601b9e312b9dc59261b9732a59893b926cb7cd32369c62bd9eeabdf3eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/editcontent.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $internal3266867601b9e312b9dc59261b9732a59893b926cb7cd32369c62bd9eeabdf3e->leave($internal3266867601b9e312b9dc59261b9732a59893b926cb7cd32369c62bd9eeabdf3eprof);

        
        $internal98ed97ff612916bbb82c101606abe4d607f35f90eaff9f2aa432315c660f19d3->leave($internal98ed97ff612916bbb82c101606abe4d607f35f90eaff9f2aa432315c660f19d3prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Profile/editcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Profile:edit.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
