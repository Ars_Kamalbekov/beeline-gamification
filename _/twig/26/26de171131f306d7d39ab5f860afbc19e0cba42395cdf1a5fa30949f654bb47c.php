<?php

/* SonataAdminBundle:CRUD:basearraymacro.html.twig */
class TwigTemplate1f400be4062bb22cb019d03c23cefa99a9039447ea2f8578dab3328d158c8c75 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal8b8c0230475122c5d629d915a905e5e8ce04a714ba30072a5365d7723805d606 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal8b8c0230475122c5d629d915a905e5e8ce04a714ba30072a5365d7723805d606->enter($internal8b8c0230475122c5d629d915a905e5e8ce04a714ba30072a5365d7723805d606prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basearraymacro.html.twig"));

        $internald6958c5dc99656f1d6cc175990dca80415ec00ec6e3d7bbec0f7e1968ca36153 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald6958c5dc99656f1d6cc175990dca80415ec00ec6e3d7bbec0f7e1968ca36153->enter($internald6958c5dc99656f1d6cc175990dca80415ec00ec6e3d7bbec0f7e1968ca36153prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basearraymacro.html.twig"));

        
        $internal8b8c0230475122c5d629d915a905e5e8ce04a714ba30072a5365d7723805d606->leave($internal8b8c0230475122c5d629d915a905e5e8ce04a714ba30072a5365d7723805d606prof);

        
        $internald6958c5dc99656f1d6cc175990dca80415ec00ec6e3d7bbec0f7e1968ca36153->leave($internald6958c5dc99656f1d6cc175990dca80415ec00ec6e3d7bbec0f7e1968ca36153prof);

    }

    // line 11
    public function macrorenderarray($value = null, $inline = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "value" => $value,
            "inline" => $inline,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internalc5034cb93fbef2cdb318ff3e5515638d529f8975df2bf52b775469428028c282 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internalc5034cb93fbef2cdb318ff3e5515638d529f8975df2bf52b775469428028c282->enter($internalc5034cb93fbef2cdb318ff3e5515638d529f8975df2bf52b775469428028c282prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderarray"));

            $internal54d594ac70c471db6294e9e01d374ee38a3b5fe5cf61c0b79ae6b82fb1e7c70a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internal54d594ac70c471db6294e9e01d374ee38a3b5fe5cf61c0b79ae6b82fb1e7c70a->enter($internal54d594ac70c471db6294e9e01d374ee38a3b5fe5cf61c0b79ae6b82fb1e7c70aprof = new TwigProfilerProfile($this->getTemplateName(), "macro", "renderarray"));

            // line 12
            echo "    ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 12, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["val"]) {
                // line 13
                echo "        ";
                if (twigtestiterable($context["val"])) {
                    // line 14
                    echo "            [";
                    echo twigescapefilter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $this->getTemplateName(), "renderarray", array(0 => $context["val"], 1 => (isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 14, $this->getSourceContext()); })())), "method"), "html", null, true);
                    echo "]
        ";
                } else {
                    // line 16
                    echo "            [";
                    echo twigescapefilter($this->env, $context["key"], "html", null, true);
                    echo " => ";
                    echo twigescapefilter($this->env, $context["val"], "html", null, true);
                    echo "]
        ";
                }
                // line 18
                echo "
        ";
                // line 19
                if (( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array()) &&  !(isset($context["inline"]) || arraykeyexists("inline", $context) ? $context["inline"] : (function () { throw new TwigErrorRuntime('Variable "inline" does not exist.', 19, $this->getSourceContext()); })()))) {
                    // line 20
                    echo "            <br>
        ";
                }
                // line 22
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['val'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            
            $internal54d594ac70c471db6294e9e01d374ee38a3b5fe5cf61c0b79ae6b82fb1e7c70a->leave($internal54d594ac70c471db6294e9e01d374ee38a3b5fe5cf61c0b79ae6b82fb1e7c70aprof);

            
            $internalc5034cb93fbef2cdb318ff3e5515638d529f8975df2bf52b775469428028c282->leave($internalc5034cb93fbef2cdb318ff3e5515638d529f8975df2bf52b775469428028c282prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:basearraymacro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 22,  94 => 20,  92 => 19,  89 => 18,  81 => 16,  73 => 14,  70 => 13,  52 => 12,  33 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% macro renderarray(value, inline) %}
    {% for key, val in value %}
        {% if val is iterable %}
            [{{ key }} => {{ self.renderarray(val, inline) }}]
        {%  else %}
            [{{ key }} => {{ val }}]
        {%  endif %}

        {% if not loop.last and not inline %}
            <br>
        {% endif %}
    {% endfor %}
{% endmacro %}
", "SonataAdminBundle:CRUD:basearraymacro.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/basearraymacro.html.twig");
    }
}
