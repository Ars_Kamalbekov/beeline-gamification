<?php

/* SonataAdminBundle:CRUD:batchconfirmation.html.twig */
class TwigTemplated2b5eb0e2ac764ce1da02794893c8061f1d8c2b3f13d3a12e09d35d60d9970ce extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'tabmenu' => array($this, 'blocktabmenu'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:batchconfirmation.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2267a5816b65125c09d11a8717dc41f64e7fa7a6ddcb2806c333aff17a998d56 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2267a5816b65125c09d11a8717dc41f64e7fa7a6ddcb2806c333aff17a998d56->enter($internal2267a5816b65125c09d11a8717dc41f64e7fa7a6ddcb2806c333aff17a998d56prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:batchconfirmation.html.twig"));

        $internal5259fad7042d26ccee0fb3c1489bf71c9ad366b6db8607766f4b358ddea1919f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5259fad7042d26ccee0fb3c1489bf71c9ad366b6db8607766f4b358ddea1919f->enter($internal5259fad7042d26ccee0fb3c1489bf71c9ad366b6db8607766f4b358ddea1919fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:batchconfirmation.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal2267a5816b65125c09d11a8717dc41f64e7fa7a6ddcb2806c333aff17a998d56->leave($internal2267a5816b65125c09d11a8717dc41f64e7fa7a6ddcb2806c333aff17a998d56prof);

        
        $internal5259fad7042d26ccee0fb3c1489bf71c9ad366b6db8607766f4b358ddea1919f->leave($internal5259fad7042d26ccee0fb3c1489bf71c9ad366b6db8607766f4b358ddea1919fprof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internalba6a345f53f538b5fb2f080e1853b16128ff5deb117a2594e358de98ce06f220 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalba6a345f53f538b5fb2f080e1853b16128ff5deb117a2594e358de98ce06f220->enter($internalba6a345f53f538b5fb2f080e1853b16128ff5deb117a2594e358de98ce06f220prof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal304668dd068031298a7378199920aa19cc7b9a199e7f7518da97ccf088aa49fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal304668dd068031298a7378199920aa19cc7b9a199e7f7518da97ccf088aa49fd->enter($internal304668dd068031298a7378199920aa19cc7b9a199e7f7518da97ccf088aa49fdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:batchconfirmation.html.twig", 15)->display($context);
        
        $internal304668dd068031298a7378199920aa19cc7b9a199e7f7518da97ccf088aa49fd->leave($internal304668dd068031298a7378199920aa19cc7b9a199e7f7518da97ccf088aa49fdprof);

        
        $internalba6a345f53f538b5fb2f080e1853b16128ff5deb117a2594e358de98ce06f220->leave($internalba6a345f53f538b5fb2f080e1853b16128ff5deb117a2594e358de98ce06f220prof);

    }

    // line 18
    public function blocktabmenu($context, array $blocks = array())
    {
        $internalddaf389401682e997ef98c0f30646e6b16128c505a1400694838f140a0b4619e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalddaf389401682e997ef98c0f30646e6b16128c505a1400694838f140a0b4619e->enter($internalddaf389401682e997ef98c0f30646e6b16128c505a1400694838f140a0b4619eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        $internal3759b30a8b8ed0db96cf875d99ec8df1b4d3cb29e65d7d497cb11f5ca82b14af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3759b30a8b8ed0db96cf875d99ec8df1b4d3cb29e65d7d497cb11f5ca82b14af->enter($internal3759b30a8b8ed0db96cf875d99ec8df1b4d3cb29e65d7d497cb11f5ca82b14afprof = new TwigProfilerProfile($this->getTemplateName(), "block", "tabmenu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "sidemenu", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 18, $this->getSourceContext()); })())), "method"), array("currentClass" => "active", "template" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 18, $this->getSourceContext()); })()), "adminPool", array()), "getTemplate", array(0 => "tabmenutemplate"), "method")), "twig");
        
        $internal3759b30a8b8ed0db96cf875d99ec8df1b4d3cb29e65d7d497cb11f5ca82b14af->leave($internal3759b30a8b8ed0db96cf875d99ec8df1b4d3cb29e65d7d497cb11f5ca82b14afprof);

        
        $internalddaf389401682e997ef98c0f30646e6b16128c505a1400694838f140a0b4619e->leave($internalddaf389401682e997ef98c0f30646e6b16128c505a1400694838f140a0b4619eprof);

    }

    // line 20
    public function blockcontent($context, array $blocks = array())
    {
        $internal11644fade2c3ed405c901a7b552d334de72ec4e2f1acedf4b75e663a0cb99ca1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal11644fade2c3ed405c901a7b552d334de72ec4e2f1acedf4b75e663a0cb99ca1->enter($internal11644fade2c3ed405c901a7b552d334de72ec4e2f1acedf4b75e663a0cb99ca1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internal5ca42307d83c5ab01ef0d2c68a0dd2afd01f7e473a4350b6fa7c561a777df997 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5ca42307d83c5ab01ef0d2c68a0dd2afd01f7e473a4350b6fa7c561a777df997->enter($internal5ca42307d83c5ab01ef0d2c68a0dd2afd01f7e473a4350b6fa7c561a777df997prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <div class=\"sonata-ba-delete\">
        <div class=\"box box-danger\">
            <div class=\"box-header\">
                ";
        // line 24
        if ( !((isset($context["batchtranslationdomain"]) || arraykeyexists("batchtranslationdomain", $context) ? $context["batchtranslationdomain"] : (function () { throw new TwigErrorRuntime('Variable "batchtranslationdomain" does not exist.', 24, $this->getSourceContext()); })()) === false)) {
            // line 25
            echo "                    ";
            $context["actionlabel"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["actionlabel"]) || arraykeyexists("actionlabel", $context) ? $context["actionlabel"] : (function () { throw new TwigErrorRuntime('Variable "actionlabel" does not exist.', 25, $this->getSourceContext()); })()), array(), (isset($context["batchtranslationdomain"]) || arraykeyexists("batchtranslationdomain", $context) ? $context["batchtranslationdomain"] : (function () { throw new TwigErrorRuntime('Variable "batchtranslationdomain" does not exist.', 25, $this->getSourceContext()); })()));
            // line 26
            echo "                ";
        }
        // line 27
        echo "                <h4 class=\"box-title\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("titlebatchconfirmation", array("%action%" => (isset($context["actionlabel"]) || arraykeyexists("actionlabel", $context) ? $context["actionlabel"] : (function () { throw new TwigErrorRuntime('Variable "actionlabel" does not exist.', 27, $this->getSourceContext()); })())), "SonataAdminBundle");
        echo "</h4>
            </div>
            <div class=\"box-body\">
                ";
        // line 30
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 30, $this->getSourceContext()); })()), "allelements", array())) {
            // line 31
            echo "                    ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("messagebatchallconfirmation", array(), "SonataAdminBundle"), "html", null, true);
            echo "
                ";
        } else {
            // line 33
            echo "                    ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("messagebatchconfirmation", twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 33, $this->getSourceContext()); })()), "idx", array())), array("%count%" => twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 33, $this->getSourceContext()); })()), "idx", array()))), "SonataAdminBundle");
            // line 34
            echo "                ";
        }
        // line 35
        echo "            </div>
            <div class=\"box-footer clearfix\">
                <form action=\"";
        // line 37
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 37, $this->getSourceContext()); })()), "generateUrl", array(0 => "batch", 1 => array("filter" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 37, $this->getSourceContext()); })()), "filterParameters", array()))), "method"), "html", null, true);
        echo "\" method=\"POST\">
                    <input type=\"hidden\" name=\"confirmation\" value=\"ok\">
                    <input type=\"hidden\" name=\"data\" value=\"";
        // line 39
        echo twigescapefilter($this->env, jsonencode((isset($context["data"]) || arraykeyexists("data", $context) ? $context["data"] : (function () { throw new TwigErrorRuntime('Variable "data" does not exist.', 39, $this->getSourceContext()); })())), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"";
        // line 40
        echo twigescapefilter($this->env, (isset($context["csrftoken"]) || arraykeyexists("csrftoken", $context) ? $context["csrftoken"] : (function () { throw new TwigErrorRuntime('Variable "csrftoken" does not exist.', 40, $this->getSourceContext()); })()), "html", null, true);
        echo "\">

                    <div style=\"display: none\">
                        ";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 43, $this->getSourceContext()); })()), 'rest');
        echo "
                    </div>

                    <button type=\"submit\" class=\"btn btn-danger\">";
        // line 46
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btnexecutebatchaction", array(), "SonataAdminBundle"), "html", null, true);
        echo "</button>

                    ";
        // line 48
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 48, $this->getSourceContext()); })()), "hasRoute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 48, $this->getSourceContext()); })()), "hasAccess", array(0 => "list"), "method"))) {
            // line 49
            echo "                        ";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("deleteor", array(), "SonataAdminBundle"), "html", null, true);
            echo "

                        <a class=\"btn btn-success\" href=\"";
            // line 51
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 51, $this->getSourceContext()); })()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-th-list\" aria-hidden=\"true\"></i> ";
            // line 52
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionlist", array(), "SonataAdminBundle"), "html", null, true);
            echo "
                        </a>
                    ";
        }
        // line 55
        echo "                </form>
            </div>
        </div>
    </div>
";
        
        $internal5ca42307d83c5ab01ef0d2c68a0dd2afd01f7e473a4350b6fa7c561a777df997->leave($internal5ca42307d83c5ab01ef0d2c68a0dd2afd01f7e473a4350b6fa7c561a777df997prof);

        
        $internal11644fade2c3ed405c901a7b552d334de72ec4e2f1acedf4b75e663a0cb99ca1->leave($internal11644fade2c3ed405c901a7b552d334de72ec4e2f1acedf4b75e663a0cb99ca1prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:batchconfirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 55,  163 => 52,  159 => 51,  153 => 49,  151 => 48,  146 => 46,  140 => 43,  134 => 40,  130 => 39,  125 => 37,  121 => 35,  118 => 34,  115 => 33,  109 => 31,  107 => 30,  100 => 27,  97 => 26,  94 => 25,  92 => 24,  87 => 21,  78 => 20,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block tabmenu %}{{ knpmenurender(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonataadmin.adminPool.getTemplate('tabmenutemplate')}, 'twig') }}{% endblock %}

{% block content %}
    <div class=\"sonata-ba-delete\">
        <div class=\"box box-danger\">
            <div class=\"box-header\">
                {% if batchtranslationdomain is not same as(false) %}
                    {% set actionlabel = actionlabel|trans({}, batchtranslationdomain) %}
                {% endif %}
                <h4 class=\"box-title\">{% trans with {'%action%': actionlabel} from 'SonataAdminBundle' %}titlebatchconfirmation{% endtrans %}</h4>
            </div>
            <div class=\"box-body\">
                {% if data.allelements %}
                    {{ 'messagebatchallconfirmation'|trans({}, 'SonataAdminBundle') }}
                {% else %}
                    {% transchoice data.idx|length with {'%count%': data.idx|length} from 'SonataAdminBundle' %}messagebatchconfirmation{% endtranschoice %}
                {% endif %}
            </div>
            <div class=\"box-footer clearfix\">
                <form action=\"{{ admin.generateUrl('batch', {'filter': admin.filterParameters}) }}\" method=\"POST\">
                    <input type=\"hidden\" name=\"confirmation\" value=\"ok\">
                    <input type=\"hidden\" name=\"data\" value=\"{{ data|jsonencode }}\">
                    <input type=\"hidden\" name=\"sonatacsrftoken\" value=\"{{ csrftoken }}\">

                    <div style=\"display: none\">
                        {{ formrest(form) }}
                    </div>

                    <button type=\"submit\" class=\"btn btn-danger\">{{ 'btnexecutebatchaction'|trans({}, 'SonataAdminBundle') }}</button>

                    {% if admin.hasRoute('list') and admin.hasAccess('list') %}
                        {{ 'deleteor'|trans({}, 'SonataAdminBundle') }}

                        <a class=\"btn btn-success\" href=\"{{ admin.generateUrl('list') }}\">
                            <i class=\"fa fa-th-list\" aria-hidden=\"true\"></i> {{ 'linkactionlist'|trans({}, 'SonataAdminBundle') }}
                        </a>
                    {% endif %}
                </form>
            </div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:batchconfirmation.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/batchconfirmation.html.twig");
    }
}
