<?php

/* WebProfilerBundle:Profiler:bag.html.twig */
class TwigTemplate22e25e079879d6c96054fd934c2bd5fb84473a3a2dd086301e532b81f36c115d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6e949aa5367b74b06dd8a107051bb7a3ec3f0408851696e5dc9620c756f279f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6e949aa5367b74b06dd8a107051bb7a3ec3f0408851696e5dc9620c756f279f2->enter($internal6e949aa5367b74b06dd8a107051bb7a3ec3f0408851696e5dc9620c756f279f2prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:bag.html.twig"));

        $internal4602b0b3ebc74ccddd3eabf85d6077c2b4f619cbe28f42b7921e312a1d23532a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4602b0b3ebc74ccddd3eabf85d6077c2b4f619cbe28f42b7921e312a1d23532a->enter($internal4602b0b3ebc74ccddd3eabf85d6077c2b4f619cbe28f42b7921e312a1d23532aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:bag.html.twig"));

        // line 1
        echo "<table class=\"";
        echo twigescapefilter($this->env, ((arraykeyexists("class", $context)) ? (twigdefaultfilter((isset($context["class"]) || arraykeyexists("class", $context) ? $context["class"] : (function () { throw new TwigErrorRuntime('Variable "class" does not exist.', 1, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
        echo "\">
    <thead>
        <tr>
            <th scope=\"col\" class=\"key\">";
        // line 4
        echo twigescapefilter($this->env, ((arraykeyexists("labels", $context)) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["labels"]) || arraykeyexists("labels", $context) ? $context["labels"] : (function () { throw new TwigErrorRuntime('Variable "labels" does not exist.', 4, $this->getSourceContext()); })()), 0, array(), "array")) : ("Key")), "html", null, true);
        echo "</th>
            <th scope=\"col\">";
        // line 5
        echo twigescapefilter($this->env, ((arraykeyexists("labels", $context)) ? (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["labels"]) || arraykeyexists("labels", $context) ? $context["labels"] : (function () { throw new TwigErrorRuntime('Variable "labels" does not exist.', 5, $this->getSourceContext()); })()), 1, array(), "array")) : ("Value")), "html", null, true);
        echo "</th>
        </tr>
    </thead>
    <tbody>
        ";
        // line 9
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twigsortfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["bag"]) || arraykeyexists("bag", $context) ? $context["bag"] : (function () { throw new TwigErrorRuntime('Variable "bag" does not exist.', 9, $this->getSourceContext()); })()), "keys", array())));
        $context['iterated'] = false;
        foreach ($context['seq'] as $context["key"] => $context["key"]) {
            // line 10
            echo "            <tr>
                <th>";
            // line 11
            echo twigescapefilter($this->env, $context["key"], "html", null, true);
            echo "</th>
                <td>";
            // line 12
            echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["bag"]) || arraykeyexists("bag", $context) ? $context["bag"] : (function () { throw new TwigErrorRuntime('Variable "bag" does not exist.', 12, $this->getSourceContext()); })()), "get", array(0 => $context["key"]), "method"), ((arraykeyexists("maxDepth", $context)) ? (twigdefaultfilter((isset($context["maxDepth"]) || arraykeyexists("maxDepth", $context) ? $context["maxDepth"] : (function () { throw new TwigErrorRuntime('Variable "maxDepth" does not exist.', 12, $this->getSourceContext()); })()), 0)) : (0))));
            echo "</td>
            </tr>
        ";
            $context['iterated'] = true;
        }
        if (!$context['iterated']) {
            // line 15
            echo "            <tr>
                <td colspan=\"2\">(no data)</td>
            </tr>
        ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['key'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 19
        echo "    </tbody>
</table>
";
        
        $internal6e949aa5367b74b06dd8a107051bb7a3ec3f0408851696e5dc9620c756f279f2->leave($internal6e949aa5367b74b06dd8a107051bb7a3ec3f0408851696e5dc9620c756f279f2prof);

        
        $internal4602b0b3ebc74ccddd3eabf85d6077c2b4f619cbe28f42b7921e312a1d23532a->leave($internal4602b0b3ebc74ccddd3eabf85d6077c2b4f619cbe28f42b7921e312a1d23532aprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:bag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 19,  63 => 15,  55 => 12,  51 => 11,  48 => 10,  43 => 9,  36 => 5,  32 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<table class=\"{{ class|default('') }}\">
    <thead>
        <tr>
            <th scope=\"col\" class=\"key\">{{ labels is defined ? labels[0] : 'Key' }}</th>
            <th scope=\"col\">{{ labels is defined ? labels[1] : 'Value' }}</th>
        </tr>
    </thead>
    <tbody>
        {% for key in bag.keys|sort %}
            <tr>
                <th>{{ key }}</th>
                <td>{{ profilerdump(bag.get(key), maxDepth=maxDepth|default(0)) }}</td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"2\">(no data)</td>
            </tr>
        {% endfor %}
    </tbody>
</table>
", "WebProfilerBundle:Profiler:bag.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/bag.html.twig");
    }
}
