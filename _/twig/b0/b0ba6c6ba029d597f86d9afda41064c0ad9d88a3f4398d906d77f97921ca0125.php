<?php

/* FOSUserBundle:Group:listcontent.html.twig */
class TwigTemplate94ff0fe056c36d6716feb686043d7fa9c56b2e09e8499dbf993323921042ff41 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal48350160841ad6a55a2853377210364ceb78a2219e865e05034b739ee4e673f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal48350160841ad6a55a2853377210364ceb78a2219e865e05034b739ee4e673f6->enter($internal48350160841ad6a55a2853377210364ceb78a2219e865e05034b739ee4e673f6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:listcontent.html.twig"));

        $internal8d6b2277495f559fbf9a6f0c2f80d6d88af9582dd82f8e21dee0a019cef44f06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8d6b2277495f559fbf9a6f0c2f80d6d88af9582dd82f8e21dee0a019cef44f06->enter($internal8d6b2277495f559fbf9a6f0c2f80d6d88af9582dd82f8e21dee0a019cef44f06prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:listcontent.html.twig"));

        // line 1
        echo "<div class=\"fosusergrouplist\">
    <ul>
    ";
        // line 3
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 3, $this->getSourceContext()); })()));
        foreach ($context['seq'] as $context["key"] => $context["group"]) {
            // line 4
            echo "        <li><a href=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosusergroupshow", array("groupName" => twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "getName", array(), "method"))), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "getName", array(), "method"), "html", null, true);
            echo "</a></li>
    ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['group'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 6
        echo "    </ul>
</div>
";
        
        $internal48350160841ad6a55a2853377210364ceb78a2219e865e05034b739ee4e673f6->leave($internal48350160841ad6a55a2853377210364ceb78a2219e865e05034b739ee4e673f6prof);

        
        $internal8d6b2277495f559fbf9a6f0c2f80d6d88af9582dd82f8e21dee0a019cef44f06->leave($internal8d6b2277495f559fbf9a6f0c2f80d6d88af9582dd82f8e21dee0a019cef44f06prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:listcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 6,  33 => 4,  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div class=\"fosusergrouplist\">
    <ul>
    {% for group in groups %}
        <li><a href=\"{{ path('fosusergroupshow', {'groupName': group.getName()} ) }}\">{{ group.getName() }}</a></li>
    {% endfor %}
    </ul>
</div>
", "FOSUserBundle:Group:listcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/listcontent.html.twig");
    }
}
