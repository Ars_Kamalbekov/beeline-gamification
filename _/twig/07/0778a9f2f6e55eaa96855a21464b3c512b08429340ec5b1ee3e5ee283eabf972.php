<?php

/* FOSUserBundle:Profile:editcontent.html.twig */
class TwigTemplate99ea890f68ed14c2f95b3f1b6d14b1a86cdbc1c07533c03781ff84e9c427953f extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalbee0813714d34a00bd1431c13609627bc3ff9d95e0b7d9d6887835616afe2ad0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalbee0813714d34a00bd1431c13609627bc3ff9d95e0b7d9d6887835616afe2ad0->enter($internalbee0813714d34a00bd1431c13609627bc3ff9d95e0b7d9d6887835616afe2ad0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:editcontent.html.twig"));

        $internal4de909200b12eaae5c7bdddede3f1c1188e55787858e0f2c516b0d2c9a08b8c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal4de909200b12eaae5c7bdddede3f1c1188e55787858e0f2c516b0d2c9a08b8c7->enter($internal4de909200b12eaae5c7bdddede3f1c1188e55787858e0f2c516b0d2c9a08b8c7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Profile:editcontent.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'formstart', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fosuserprofileedit"), "attr" => array("class" => "fosuserprofileedit")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.edit.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'formend');
        echo "
";
        
        $internalbee0813714d34a00bd1431c13609627bc3ff9d95e0b7d9d6887835616afe2ad0->leave($internalbee0813714d34a00bd1431c13609627bc3ff9d95e0b7d9d6887835616afe2ad0prof);

        
        $internal4de909200b12eaae5c7bdddede3f1c1188e55787858e0f2c516b0d2c9a08b8c7->leave($internal4de909200b12eaae5c7bdddede3f1c1188e55787858e0f2c516b0d2c9a08b8c7prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:editcontent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% transdefaultdomain 'FOSUserBundle' %}

{{ formstart(form, { 'action': path('fosuserprofileedit'), 'attr': { 'class': 'fosuserprofileedit' } }) }}
    {{ formwidget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'profile.edit.submit'|trans }}\" />
    </div>
{{ formend(form) }}
", "FOSUserBundle:Profile:editcontent.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/editcontent.html.twig");
    }
}
