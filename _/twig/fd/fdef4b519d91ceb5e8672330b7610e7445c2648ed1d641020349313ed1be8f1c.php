<?php

/* SonataAdminBundle:CRUD:listactionedit.html.twig */
class TwigTemplate594486e2845aea356ebf329ea5f8c9cf129950188663c05a25224738f0777ca6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal228e7ad6264edd013519c93e674b2a9d0b69fa46fd5ee4e07a4be5be2c3c7c40 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal228e7ad6264edd013519c93e674b2a9d0b69fa46fd5ee4e07a4be5be2c3c7c40->enter($internal228e7ad6264edd013519c93e674b2a9d0b69fa46fd5ee4e07a4be5be2c3c7c40prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactionedit.html.twig"));

        $internal61face62f304bbc64e663c073bdc445038c72ff844e299d72b531ee6846ea4c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal61face62f304bbc64e663c073bdc445038c72ff844e299d72b531ee6846ea4c9->enter($internal61face62f304bbc64e663c073bdc445038c72ff844e299d72b531ee6846ea4c9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listactionedit.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method"))) {
            // line 13
            echo "    <a href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 13, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 13, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default editlink\" title=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actionedit", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">
        <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
        ";
            // line 15
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("actionedit", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    </a>
";
        }
        
        $internal228e7ad6264edd013519c93e674b2a9d0b69fa46fd5ee4e07a4be5be2c3c7c40->leave($internal228e7ad6264edd013519c93e674b2a9d0b69fa46fd5ee4e07a4be5be2c3c7c40prof);

        
        $internal61face62f304bbc64e663c073bdc445038c72ff844e299d72b531ee6846ea4c9->leave($internal61face62f304bbc64e663c073bdc445038c72ff844e299d72b531ee6846ea4c9prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listactionedit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 15,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.hasAccess('edit', object) and admin.hasRoute('edit') %}
    <a href=\"{{ admin.generateObjectUrl('edit', object) }}\" class=\"btn btn-sm btn-default editlink\" title=\"{{ 'actionedit'|trans({}, 'SonataAdminBundle') }}\">
        <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
        {{ 'actionedit'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endif %}
", "SonataAdminBundle:CRUD:listactionedit.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listactionedit.html.twig");
    }
}
