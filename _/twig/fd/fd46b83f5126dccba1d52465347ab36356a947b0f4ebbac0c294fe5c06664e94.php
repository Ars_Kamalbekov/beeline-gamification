<?php

/* @Twig/layout.html.twig */
class TwigTemplate8c79b959145dca7682ea740640a741386190655ab39031816861a58a34ba7b92 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'head' => array($this, 'blockhead'),
            'body' => array($this, 'blockbody'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaleae8be754a0f8e6b1f7201c6b82d5b83919b17a23b552630a1e986372534dc02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaleae8be754a0f8e6b1f7201c6b82d5b83919b17a23b552630a1e986372534dc02->enter($internaleae8be754a0f8e6b1f7201c6b82d5b83919b17a23b552630a1e986372534dc02prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $internal83034de3f78b15bf55e16be206bccab04d65d2064a8c9b57e31268d7dca9e47a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal83034de3f78b15bf55e16be206bccab04d65d2064a8c9b57e31268d7dca9e47a->enter($internal83034de3f78b15bf55e16be206bccab04d65d2064a8c9b57e31268d7dca9e47aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twigescapefilter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twiginclude($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twiginclude($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twiginclude($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twiginclude($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twiginclude($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twiginclude($this->env, $context, "@Twig/basejs.html.twig");
        echo "
    </body>
</html>
";
        
        $internaleae8be754a0f8e6b1f7201c6b82d5b83919b17a23b552630a1e986372534dc02->leave($internaleae8be754a0f8e6b1f7201c6b82d5b83919b17a23b552630a1e986372534dc02prof);

        
        $internal83034de3f78b15bf55e16be206bccab04d65d2064a8c9b57e31268d7dca9e47a->leave($internal83034de3f78b15bf55e16be206bccab04d65d2064a8c9b57e31268d7dca9e47aprof);

    }

    // line 7
    public function blocktitle($context, array $blocks = array())
    {
        $internal3930c336a27bb89ff5bdd5c2580991b5d7606ed779cd3743fac2bc03fcec14d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3930c336a27bb89ff5bdd5c2580991b5d7606ed779cd3743fac2bc03fcec14d3->enter($internal3930c336a27bb89ff5bdd5c2580991b5d7606ed779cd3743fac2bc03fcec14d3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal3e96862a00d6c40aaf3af7d48a8a46a6075d2de07c032790460f5625f6652b51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3e96862a00d6c40aaf3af7d48a8a46a6075d2de07c032790460f5625f6652b51->enter($internal3e96862a00d6c40aaf3af7d48a8a46a6075d2de07c032790460f5625f6652b51prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        
        $internal3e96862a00d6c40aaf3af7d48a8a46a6075d2de07c032790460f5625f6652b51->leave($internal3e96862a00d6c40aaf3af7d48a8a46a6075d2de07c032790460f5625f6652b51prof);

        
        $internal3930c336a27bb89ff5bdd5c2580991b5d7606ed779cd3743fac2bc03fcec14d3->leave($internal3930c336a27bb89ff5bdd5c2580991b5d7606ed779cd3743fac2bc03fcec14d3prof);

    }

    // line 10
    public function blockhead($context, array $blocks = array())
    {
        $internal7e579b71a1c3a8c27f8242208afdd2d8bd48c4def87762b2ca9c59185c145240 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7e579b71a1c3a8c27f8242208afdd2d8bd48c4def87762b2ca9c59185c145240->enter($internal7e579b71a1c3a8c27f8242208afdd2d8bd48c4def87762b2ca9c59185c145240prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        $internal40e6bf796905cfb42a19bdbb1defe35b114b7d8f4af64b2a209a9a822ad489c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal40e6bf796905cfb42a19bdbb1defe35b114b7d8f4af64b2a209a9a822ad489c9->enter($internal40e6bf796905cfb42a19bdbb1defe35b114b7d8f4af64b2a209a9a822ad489c9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        
        $internal40e6bf796905cfb42a19bdbb1defe35b114b7d8f4af64b2a209a9a822ad489c9->leave($internal40e6bf796905cfb42a19bdbb1defe35b114b7d8f4af64b2a209a9a822ad489c9prof);

        
        $internal7e579b71a1c3a8c27f8242208afdd2d8bd48c4def87762b2ca9c59185c145240->leave($internal7e579b71a1c3a8c27f8242208afdd2d8bd48c4def87762b2ca9c59185c145240prof);

    }

    // line 33
    public function blockbody($context, array $blocks = array())
    {
        $internalc0d5383a5ab9530ffe5dfb2e6d5f1cd70f187c6cd771c9b2dc92e421e9022f47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc0d5383a5ab9530ffe5dfb2e6d5f1cd70f187c6cd771c9b2dc92e421e9022f47->enter($internalc0d5383a5ab9530ffe5dfb2e6d5f1cd70f187c6cd771c9b2dc92e421e9022f47prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internalb562364db5ad7fbb2c33727b6fe5abe845d7a0540547695f91487eaff5e62f51 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalb562364db5ad7fbb2c33727b6fe5abe845d7a0540547695f91487eaff5e62f51->enter($internalb562364db5ad7fbb2c33727b6fe5abe845d7a0540547695f91487eaff5e62f51prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        
        $internalb562364db5ad7fbb2c33727b6fe5abe845d7a0540547695f91487eaff5e62f51->leave($internalb562364db5ad7fbb2c33727b6fe5abe845d7a0540547695f91487eaff5e62f51prof);

        
        $internalc0d5383a5ab9530ffe5dfb2e6d5f1cd70f187c6cd771c9b2dc92e421e9022f47->leave($internalc0d5383a5ab9530ffe5dfb2e6d5f1cd70f187c6cd771c9b2dc92e421e9022f47prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/basejs.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
