<?php

/* FOSUserBundle:Resetting:checkemail.html.twig */
class TwigTemplate787a29b19333f6cde56e8798a3f7e91b3d11aa11f3b351dcf922f24b48404e1d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:checkemail.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal59a635c92b2f23cb1b9a4891a4e5077b0311d7124f844f4c4c9403b066f5d49e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal59a635c92b2f23cb1b9a4891a4e5077b0311d7124f844f4c4c9403b066f5d49e->enter($internal59a635c92b2f23cb1b9a4891a4e5077b0311d7124f844f4c4c9403b066f5d49eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkemail.html.twig"));

        $internal3679762315316897be4e12ff7d87e600acf9064c9db88ec17e33e23e70767ac8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3679762315316897be4e12ff7d87e600acf9064c9db88ec17e33e23e70767ac8->enter($internal3679762315316897be4e12ff7d87e600acf9064c9db88ec17e33e23e70767ac8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:checkemail.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal59a635c92b2f23cb1b9a4891a4e5077b0311d7124f844f4c4c9403b066f5d49e->leave($internal59a635c92b2f23cb1b9a4891a4e5077b0311d7124f844f4c4c9403b066f5d49eprof);

        
        $internal3679762315316897be4e12ff7d87e600acf9064c9db88ec17e33e23e70767ac8->leave($internal3679762315316897be4e12ff7d87e600acf9064c9db88ec17e33e23e70767ac8prof);

    }

    // line 5
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internalf1c7e1a70ca99a2367b7cc073cb430edc0b87757149592e2e34cb6a81360d4e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf1c7e1a70ca99a2367b7cc073cb430edc0b87757149592e2e34cb6a81360d4e3->enter($internalf1c7e1a70ca99a2367b7cc073cb430edc0b87757149592e2e34cb6a81360d4e3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internaled72150f123559f24912224f07511473ef9d21afa00e6ffaa3398f4f228043b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaled72150f123559f24912224f07511473ef9d21afa00e6ffaa3398f4f228043b4->enter($internaled72150f123559f24912224f07511473ef9d21afa00e6ffaa3398f4f228043b4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.checkemail", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) || arraykeyexists("tokenLifetime", $context) ? $context["tokenLifetime"] : (function () { throw new TwigErrorRuntime('Variable "tokenLifetime" does not exist.', 7, $this->getSourceContext()); })())), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $internaled72150f123559f24912224f07511473ef9d21afa00e6ffaa3398f4f228043b4->leave($internaled72150f123559f24912224f07511473ef9d21afa00e6ffaa3398f4f228043b4prof);

        
        $internalf1c7e1a70ca99a2367b7cc073cb430edc0b87757149592e2e34cb6a81360d4e3->leave($internalf1c7e1a70ca99a2367b7cc073cb430edc0b87757149592e2e34cb6a81360d4e3prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkemail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% transdefaultdomain 'FOSUserBundle' %}

{% block fosusercontent %}
<p>
{{ 'resetting.checkemail'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:checkemail.html.twig", "/var/www/html/beeline-gamification/app/Resources/FOSUserBundle/views/Resetting/checkemail.html.twig");
    }
}
