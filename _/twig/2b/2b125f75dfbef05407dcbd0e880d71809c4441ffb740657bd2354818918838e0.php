<?php

/* SonataAdminBundle:CRUD:listdate.html.twig */
class TwigTemplateb33648fe5cbd6804372f013944ee04ed047afc7ecf750a208910fab537a4a9ab extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "getTemplate", array(0 => "baselistfield"), "method"), "SonataAdminBundle:CRUD:listdate.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internala6c01f8bca9291fcc8c9104cb0f89f2a87383e569c67e49a98d54969b4d5d640 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala6c01f8bca9291fcc8c9104cb0f89f2a87383e569c67e49a98d54969b4d5d640->enter($internala6c01f8bca9291fcc8c9104cb0f89f2a87383e569c67e49a98d54969b4d5d640prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listdate.html.twig"));

        $internald6bbcd83a74e0880bbbda27051f4b6a8352ae1e49cc380fc30e716554beadee8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald6bbcd83a74e0880bbbda27051f4b6a8352ae1e49cc380fc30e716554beadee8->enter($internald6bbcd83a74e0880bbbda27051f4b6a8352ae1e49cc380fc30e716554beadee8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:listdate.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internala6c01f8bca9291fcc8c9104cb0f89f2a87383e569c67e49a98d54969b4d5d640->leave($internala6c01f8bca9291fcc8c9104cb0f89f2a87383e569c67e49a98d54969b4d5d640prof);

        
        $internald6bbcd83a74e0880bbbda27051f4b6a8352ae1e49cc380fc30e716554beadee8->leave($internald6bbcd83a74e0880bbbda27051f4b6a8352ae1e49cc380fc30e716554beadee8prof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internal40bc369b745d9d09a4207ba1a4188b47fb0cebe080c2aaa3f829442d5c0e5d12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal40bc369b745d9d09a4207ba1a4188b47fb0cebe080c2aaa3f829442d5c0e5d12->enter($internal40bc369b745d9d09a4207ba1a4188b47fb0cebe080c2aaa3f829442d5c0e5d12prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal2a33abb27e68a3e08623f8e9172c756f2203e8e2e98a0d23b94f852c250194db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2a33abb27e68a3e08623f8e9172c756f2203e8e2e98a0d23b94f852c250194db->enter($internal2a33abb27e68a3e08623f8e9172c756f2203e8e2e98a0d23b94f852c250194dbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } elseif (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 17
($context["fielddescription"] ?? null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), "F j, Y"), "html", null, true);
        }
        
        $internal2a33abb27e68a3e08623f8e9172c756f2203e8e2e98a0d23b94f852c250194db->leave($internal2a33abb27e68a3e08623f8e9172c756f2203e8e2e98a0d23b94f852c250194dbprof);

        
        $internal40bc369b745d9d09a4207ba1a4188b47fb0cebe080c2aaa3f829442d5c0e5d12->leave($internal40bc369b745d9d09a4207ba1a4188b47fb0cebe080c2aaa3f829442d5c0e5d12prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:listdate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 20,  54 => 18,  52 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('baselistfield') %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif fielddescription.options.format is defined -%}
        {{ value|date(fielddescription.options.format) }}
    {%- else -%}
        {{ value|date('F j, Y') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:listdate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/listdate.html.twig");
    }
}
