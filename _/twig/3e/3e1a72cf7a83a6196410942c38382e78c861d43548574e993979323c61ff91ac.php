<?php

/* SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig */
class TwigTemplatee61f03210457b98f5baa8ecaf83afc39024e59c306483e0e2bcdd5a121b7c5aa extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal6f81707ad1f094493df9c6abef414190cc8d75ad061d1da884f589b67418d7fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6f81707ad1f094493df9c6abef414190cc8d75ad061d1da884f589b67418d7fa->enter($internal6f81707ad1f094493df9c6abef414190cc8d75ad061d1da884f589b67418d7faprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig"));

        $internal8a0f175a0b4e4c2882409c1cddcb7a53c278c78c42163c413b8713f771d3620a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal8a0f175a0b4e4c2882409c1cddcb7a53c278c78c42163c413b8713f771d3620a->enter($internal8a0f175a0b4e4c2882409c1cddcb7a53c278c78c42163c413b8713f771d3620aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig"));

        // line 11
        echo "

";
        // line 18
        echo "
";
        // line 20
        echo "
<!-- edit one association -->

<script type=\"text/javascript\">

    // handle the add link
    var fieldadd";
        // line 26
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 26, $this->getSourceContext()); })());
        echo " = function(event) {

        event.preventDefault();
        event.stopPropagation();

        var form = jQuery(this).closest('form');

        // the ajax post
        jQuery(form).ajaxSubmit({
            url: '";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminappendformelement", (array("code" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 36
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 36, $this->getSourceContext()); })()), "admin", array()), "root", array()), "code", array()), "elementId" =>         // line 37
(isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 37, $this->getSourceContext()); })()), "objectId" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 38
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 38, $this->getSourceContext()); })()), "admin", array()), "root", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 38, $this->getSourceContext()); })()), "admin", array()), "root", array()), "subject", array())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 39
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 39, $this->getSourceContext()); })()), "admin", array()), "root", array()), "uniqid", array()), "subclass" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 40
(isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 40, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "subclass"), "method")) + twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 41
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 41, $this->getSourceContext()); })()), "fielddescription", array()), "getOption", array(0 => "linkparameters", 1 => array()), "method")));
        echo "',
            type: \"POST\",
            dataType: 'html',
            data: { xmlhttprequest: true },
            success: function(html) {
                if (!html.length) {
                    return;
                }

                jQuery('#fieldcontainer";
        // line 50
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 50, $this->getSourceContext()); })());
        echo "').replaceWith(html); // replace the html

                Admin.sharedsetup(jQuery('#fieldcontainer";
        // line 52
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 52, $this->getSourceContext()); })());
        echo "'));

                if(jQuery('input[type=\"file\"]', form).length > 0) {
                    jQuery(form).attr('enctype', 'multipart/form-data');
                    jQuery(form).attr('encoding', 'multipart/form-data');
                }
                jQuery('#sonata-ba-field-container-";
        // line 58
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 58, $this->getSourceContext()); })());
        echo "').trigger('sonata.addelement');
                jQuery('#fieldcontainer";
        // line 59
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 59, $this->getSourceContext()); })());
        echo "').trigger('sonata.addelement');
            }
        });

        return false;
    };

    var fieldwidget";
        // line 66
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 66, $this->getSourceContext()); })());
        echo " = false;

    // this function initialize the popup
    // this can be only done this way has popup can be cascaded
    function startfieldretrieve";
        // line 70
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 70, $this->getSourceContext()); })());
        echo "(link) {

        link.onclick = null;

        // initialize component
        fieldwidget";
        // line 75
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 75, $this->getSourceContext()); })());
        echo " = jQuery(\"#fieldwidget";
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 75, $this->getSourceContext()); })());
        echo "\");

        // add the jQuery event to the a element
        jQuery(link)
            .click(fieldadd";
        // line 79
        echo (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 79, $this->getSourceContext()); })());
        echo ")
            .trigger('click')
        ;

        return false;
    }
</script>

<!-- / edit one association -->

";
        
        $internal6f81707ad1f094493df9c6abef414190cc8d75ad061d1da884f589b67418d7fa->leave($internal6f81707ad1f094493df9c6abef414190cc8d75ad061d1da884f589b67418d7faprof);

        
        $internal8a0f175a0b4e4c2882409c1cddcb7a53c278c78c42163c413b8713f771d3620a->leave($internal8a0f175a0b4e4c2882409c1cddcb7a53c278c78c42163c413b8713f771d3620aprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 79,  113 => 75,  105 => 70,  98 => 66,  88 => 59,  84 => 58,  75 => 52,  70 => 50,  58 => 41,  57 => 40,  56 => 39,  55 => 38,  54 => 37,  53 => 36,  52 => 35,  40 => 26,  32 => 20,  29 => 18,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}


{#

This code manage the one-to-many association field popup

#}

{% autoescape false %}

<!-- edit one association -->

<script type=\"text/javascript\">

    // handle the add link
    var fieldadd{{ id }} = function(event) {

        event.preventDefault();
        event.stopPropagation();

        var form = jQuery(this).closest('form');

        // the ajax post
        jQuery(form).ajaxSubmit({
            url: '{{ path('sonataadminappendformelement', {
                'code':      sonataadmin.admin.root.code,
                'elementId': id,
                'objectId':  sonataadmin.admin.root.id(sonataadmin.admin.root.subject),
                'uniqid':    sonataadmin.admin.root.uniqid,
                'subclass': app.request.query.get('subclass'),
            } + sonataadmin.fielddescription.getOption('linkparameters', {})) }}',
            type: \"POST\",
            dataType: 'html',
            data: { xmlhttprequest: true },
            success: function(html) {
                if (!html.length) {
                    return;
                }

                jQuery('#fieldcontainer{{ id }}').replaceWith(html); // replace the html

                Admin.sharedsetup(jQuery('#fieldcontainer{{ id }}'));

                if(jQuery('input[type=\"file\"]', form).length > 0) {
                    jQuery(form).attr('enctype', 'multipart/form-data');
                    jQuery(form).attr('encoding', 'multipart/form-data');
                }
                jQuery('#sonata-ba-field-container-{{ id }}').trigger('sonata.addelement');
                jQuery('#fieldcontainer{{ id }}').trigger('sonata.addelement');
            }
        });

        return false;
    };

    var fieldwidget{{ id }} = false;

    // this function initialize the popup
    // this can be only done this way has popup can be cascaded
    function startfieldretrieve{{ id }}(link) {

        link.onclick = null;

        // initialize component
        fieldwidget{{ id }} = jQuery(\"#fieldwidget{{ id }}\");

        // add the jQuery event to the a element
        jQuery(link)
            .click(fieldadd{{ id }})
            .trigger('click')
        ;

        return false;
    }
</script>

<!-- / edit one association -->

{% endautoescape %}
", "SonataDoctrineORMAdminBundle:CRUD:editormoneassociationscript.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/editormoneassociationscript.html.twig");
    }
}
