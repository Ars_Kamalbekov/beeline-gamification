<?php

/* WebProfilerBundle:Collector:router.html.twig */
class TwigTemplatefc9c41d16b8df2792c3f1e57fefdef54fe027b7d0a13fb1936056df6837bb5ce extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'blocktoolbar'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfe22528704bb20434b762c11414b3d71f616154b4271ef0f663872e624515bb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfe22528704bb20434b762c11414b3d71f616154b4271ef0f663872e624515bb3->enter($internalfe22528704bb20434b762c11414b3d71f616154b4271ef0f663872e624515bb3prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $internal1ff207c3e3256d60fa96ab13b287d22003d2a0c3b520f32a473a1fc3878c72e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1ff207c3e3256d60fa96ab13b287d22003d2a0c3b520f32a473a1fc3878c72e7->enter($internal1ff207c3e3256d60fa96ab13b287d22003d2a0c3b520f32a473a1fc3878c72e7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalfe22528704bb20434b762c11414b3d71f616154b4271ef0f663872e624515bb3->leave($internalfe22528704bb20434b762c11414b3d71f616154b4271ef0f663872e624515bb3prof);

        
        $internal1ff207c3e3256d60fa96ab13b287d22003d2a0c3b520f32a473a1fc3878c72e7->leave($internal1ff207c3e3256d60fa96ab13b287d22003d2a0c3b520f32a473a1fc3878c72e7prof);

    }

    // line 3
    public function blocktoolbar($context, array $blocks = array())
    {
        $internal47f1ece0ffa49af8853067e6770881904d055706d898df06b53bfdb06ed0c9bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal47f1ece0ffa49af8853067e6770881904d055706d898df06b53bfdb06ed0c9bd->enter($internal47f1ece0ffa49af8853067e6770881904d055706d898df06b53bfdb06ed0c9bdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        $internal3ad19d24994cc6d13bf8198a4341bdc542e1c5a9d00f1fadc0c573d38272b2e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3ad19d24994cc6d13bf8198a4341bdc542e1c5a9d00f1fadc0c573d38272b2e2->enter($internal3ad19d24994cc6d13bf8198a4341bdc542e1c5a9d00f1fadc0c573d38272b2e2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "toolbar"));

        
        $internal3ad19d24994cc6d13bf8198a4341bdc542e1c5a9d00f1fadc0c573d38272b2e2->leave($internal3ad19d24994cc6d13bf8198a4341bdc542e1c5a9d00f1fadc0c573d38272b2e2prof);

        
        $internal47f1ece0ffa49af8853067e6770881904d055706d898df06b53bfdb06ed0c9bd->leave($internal47f1ece0ffa49af8853067e6770881904d055706d898df06b53bfdb06ed0c9bdprof);

    }

    // line 5
    public function blockmenu($context, array $blocks = array())
    {
        $internale62294d524068984ae4a4513edae06528c7a56565edef8b0a00636cf6ed59a28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internale62294d524068984ae4a4513edae06528c7a56565edef8b0a00636cf6ed59a28->enter($internale62294d524068984ae4a4513edae06528c7a56565edef8b0a00636cf6ed59a28prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internaldbee3534c0a700695c0afb8b48ba122147b8f3659166358ae451cc3c80e4d67a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldbee3534c0a700695c0afb8b48ba122147b8f3659166358ae451cc3c80e4d67a->enter($internaldbee3534c0a700695c0afb8b48ba122147b8f3659166358ae451cc3c80e4d67aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $internaldbee3534c0a700695c0afb8b48ba122147b8f3659166358ae451cc3c80e4d67a->leave($internaldbee3534c0a700695c0afb8b48ba122147b8f3659166358ae451cc3c80e4d67aprof);

        
        $internale62294d524068984ae4a4513edae06528c7a56565edef8b0a00636cf6ed59a28->leave($internale62294d524068984ae4a4513edae06528c7a56565edef8b0a00636cf6ed59a28prof);

    }

    // line 12
    public function blockpanel($context, array $blocks = array())
    {
        $internal524a47cff7ed9d601e830fb5da38ac13e4f4cf763ee965e296e39093d4e0a1e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal524a47cff7ed9d601e830fb5da38ac13e4f4cf763ee965e296e39093d4e0a1e5->enter($internal524a47cff7ed9d601e830fb5da38ac13e4f4cf763ee965e296e39093d4e0a1e5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal944fd846c7c708db5ab1f98aa334f188c6970347d875e3e75cae652f8e0c79d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal944fd846c7c708db5ab1f98aa334f188c6970347d875e3e75cae652f8e0c79d2->enter($internal944fd846c7c708db5ab1f98aa334f188c6970347d875e3e75cae652f8e0c79d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilerrouter", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $internal944fd846c7c708db5ab1f98aa334f188c6970347d875e3e75cae652f8e0c79d2->leave($internal944fd846c7c708db5ab1f98aa334f188c6970347d875e3e75cae652f8e0c79d2prof);

        
        $internal524a47cff7ed9d601e830fb5da38ac13e4f4cf763ee965e296e39093d4e0a1e5->leave($internal524a47cff7ed9d601e830fb5da38ac13e4f4cf763ee965e296e39093d4e0a1e5prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('profilerrouter', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
