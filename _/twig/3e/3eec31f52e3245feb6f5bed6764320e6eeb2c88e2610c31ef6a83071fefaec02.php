<?php

/* SonataAdminBundle:CRUD:edit.html.twig */
class TwigTemplatee48c5fe22156d1d70fc1cc0e92d16a0bddbf4f08a37e93bc272777eb8bf30559 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseedit.html.twig", "SonataAdminBundle:CRUD:edit.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseedit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal0bcb9811f6cca4b1dedd766884df6c7e8722255a86bf2d7354d7eff574ce0715 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal0bcb9811f6cca4b1dedd766884df6c7e8722255a86bf2d7354d7eff574ce0715->enter($internal0bcb9811f6cca4b1dedd766884df6c7e8722255a86bf2d7354d7eff574ce0715prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $internala693c4e89174bb4100231faad1a57cdce2fde9fe632c9bde6e70d5bd1a1f9cc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala693c4e89174bb4100231faad1a57cdce2fde9fe632c9bde6e70d5bd1a1f9cc0->enter($internala693c4e89174bb4100231faad1a57cdce2fde9fe632c9bde6e70d5bd1a1f9cc0prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal0bcb9811f6cca4b1dedd766884df6c7e8722255a86bf2d7354d7eff574ce0715->leave($internal0bcb9811f6cca4b1dedd766884df6c7e8722255a86bf2d7354d7eff574ce0715prof);

        
        $internala693c4e89174bb4100231faad1a57cdce2fde9fe632c9bde6e70d5bd1a1f9cc0->leave($internala693c4e89174bb4100231faad1a57cdce2fde9fe632c9bde6e70d5bd1a1f9cc0prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseedit.html.twig' %}
", "SonataAdminBundle:CRUD:edit.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/edit.html.twig");
    }
}
