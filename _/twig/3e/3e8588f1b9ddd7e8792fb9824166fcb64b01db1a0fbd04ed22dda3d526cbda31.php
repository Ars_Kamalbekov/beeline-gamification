<?php

/* @Security/Collector/icon.svg */
class TwigTemplate0c55c523c0d2087a462fcf94685173e8a4452edeae2b0993bc7ea1ab8fdd0d77 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb91fe87ec8153006778e31fb69cd7496d339d777c30aee8658dbae4324c4256c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb91fe87ec8153006778e31fb69cd7496d339d777c30aee8658dbae4324c4256c->enter($internalb91fe87ec8153006778e31fb69cd7496d339d777c30aee8658dbae4324c4256cprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $internal5c73e439f953f9fecd4e424f9a4ec8b453150102fe5600efe5e04e235dbb4a12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5c73e439f953f9fecd4e424f9a4ec8b453150102fe5600efe5e04e235dbb4a12->enter($internal5c73e439f953f9fecd4e424f9a4ec8b453150102fe5600efe5e04e235dbb4a12prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $internalb91fe87ec8153006778e31fb69cd7496d339d777c30aee8658dbae4324c4256c->leave($internalb91fe87ec8153006778e31fb69cd7496d339d777c30aee8658dbae4324c4256cprof);

        
        $internal5c73e439f953f9fecd4e424f9a4ec8b453150102fe5600efe5e04e235dbb4a12->leave($internal5c73e439f953f9fecd4e424f9a4ec8b453150102fe5600efe5e04e235dbb4a12prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}
