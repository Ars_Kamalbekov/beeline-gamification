<?php

/* WebProfilerBundle:Profiler:toolbarredirect.html.twig */
class TwigTemplatef7e92f177088ecc2508b5a91801de9836c54bbaa74ab89abdedef6745515ac3d extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbarredirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'body' => array($this, 'blockbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf419204e52a58b00f0e05dcaee55bdba2ec98a187d1934e0fa66af472be34f49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf419204e52a58b00f0e05dcaee55bdba2ec98a187d1934e0fa66af472be34f49->enter($internalf419204e52a58b00f0e05dcaee55bdba2ec98a187d1934e0fa66af472be34f49prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbarredirect.html.twig"));

        $internal0c10fb2018bfcdfa990d73b61a7d208993bff0be4a6b8631e995a41e1f3162ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0c10fb2018bfcdfa990d73b61a7d208993bff0be4a6b8631e995a41e1f3162ee->enter($internal0c10fb2018bfcdfa990d73b61a7d208993bff0be4a6b8631e995a41e1f3162eeprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbarredirect.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalf419204e52a58b00f0e05dcaee55bdba2ec98a187d1934e0fa66af472be34f49->leave($internalf419204e52a58b00f0e05dcaee55bdba2ec98a187d1934e0fa66af472be34f49prof);

        
        $internal0c10fb2018bfcdfa990d73b61a7d208993bff0be4a6b8631e995a41e1f3162ee->leave($internal0c10fb2018bfcdfa990d73b61a7d208993bff0be4a6b8631e995a41e1f3162eeprof);

    }

    // line 3
    public function blocktitle($context, array $blocks = array())
    {
        $internal2523fbbafcf98bafb80d500727434f36efdb46a766a9f763222c69527a06f4bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2523fbbafcf98bafb80d500727434f36efdb46a766a9f763222c69527a06f4bd->enter($internal2523fbbafcf98bafb80d500727434f36efdb46a766a9f763222c69527a06f4bdprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal24c5d3ab617b28b4960019b47ba012278ed2616d9b6ed0a9454afdf832a7a941 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal24c5d3ab617b28b4960019b47ba012278ed2616d9b6ed0a9454afdf832a7a941->enter($internal24c5d3ab617b28b4960019b47ba012278ed2616d9b6ed0a9454afdf832a7a941prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $internal24c5d3ab617b28b4960019b47ba012278ed2616d9b6ed0a9454afdf832a7a941->leave($internal24c5d3ab617b28b4960019b47ba012278ed2616d9b6ed0a9454afdf832a7a941prof);

        
        $internal2523fbbafcf98bafb80d500727434f36efdb46a766a9f763222c69527a06f4bd->leave($internal2523fbbafcf98bafb80d500727434f36efdb46a766a9f763222c69527a06f4bdprof);

    }

    // line 5
    public function blockbody($context, array $blocks = array())
    {
        $internal4b072ea5b326cfce124bd77f2f5fa1184ae59e481aa8a22b2cb377ef05a62111 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal4b072ea5b326cfce124bd77f2f5fa1184ae59e481aa8a22b2cb377ef05a62111->enter($internal4b072ea5b326cfce124bd77f2f5fa1184ae59e481aa8a22b2cb377ef05a62111prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        $internal020eab6640a5d4d371a051cd72f6fd8960e7710669c2a3c94395b8a1dd0e4700 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal020eab6640a5d4d371a051cd72f6fd8960e7710669c2a3c94395b8a1dd0e4700->enter($internal020eab6640a5d4d371a051cd72f6fd8960e7710669c2a3c94395b8a1dd0e4700prof = new TwigProfilerProfile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twigescapefilter($this->env, (isset($context["location"]) || arraykeyexists("location", $context) ? $context["location"] : (function () { throw new TwigErrorRuntime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twigescapefilter($this->env, (isset($context["location"]) || arraykeyexists("location", $context) ? $context["location"] : (function () { throw new TwigErrorRuntime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $internal020eab6640a5d4d371a051cd72f6fd8960e7710669c2a3c94395b8a1dd0e4700->leave($internal020eab6640a5d4d371a051cd72f6fd8960e7710669c2a3c94395b8a1dd0e4700prof);

        
        $internal4b072ea5b326cfce124bd77f2f5fa1184ae59e481aa8a22b2cb377ef05a62111->leave($internal4b072ea5b326cfce124bd77f2f5fa1184ae59e481aa8a22b2cb377ef05a62111prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbarredirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbarredirect.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbarredirect.html.twig");
    }
}
