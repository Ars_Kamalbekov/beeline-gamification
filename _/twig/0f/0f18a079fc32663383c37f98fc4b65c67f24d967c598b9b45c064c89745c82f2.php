<?php

/* WebProfilerBundle:Collector:events.html.twig */
class TwigTemplated1180e5d8ceea3eec618e70bc49637f73149cdc14e5d3f074cb77af51a817dfb extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:events.html.twig", 1);
        $this->blocks = array(
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalde3c7e5cf84413f0660769a2e6c646ee845ffab02e01618fe35c82301d97cc97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalde3c7e5cf84413f0660769a2e6c646ee845ffab02e01618fe35c82301d97cc97->enter($internalde3c7e5cf84413f0660769a2e6c646ee845ffab02e01618fe35c82301d97cc97prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:events.html.twig"));

        $internal2aba40736547a143751a769ac1ad47286ffefe62f7ad9f204f4db8387dfa1aab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2aba40736547a143751a769ac1ad47286ffefe62f7ad9f204f4db8387dfa1aab->enter($internal2aba40736547a143751a769ac1ad47286ffefe62f7ad9f204f4db8387dfa1aabprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:events.html.twig"));

        // line 3
        $context["helper"] = $this;
        // line 1
        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internalde3c7e5cf84413f0660769a2e6c646ee845ffab02e01618fe35c82301d97cc97->leave($internalde3c7e5cf84413f0660769a2e6c646ee845ffab02e01618fe35c82301d97cc97prof);

        
        $internal2aba40736547a143751a769ac1ad47286ffefe62f7ad9f204f4db8387dfa1aab->leave($internal2aba40736547a143751a769ac1ad47286ffefe62f7ad9f204f4db8387dfa1aabprof);

    }

    // line 5
    public function blockmenu($context, array $blocks = array())
    {
        $internal5c778c4e5063fbb1d72184f8076998ac14641fc26c0f43acd781d9ae79630166 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5c778c4e5063fbb1d72184f8076998ac14641fc26c0f43acd781d9ae79630166->enter($internal5c778c4e5063fbb1d72184f8076998ac14641fc26c0f43acd781d9ae79630166prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internala4eb8099bac9ccb17f4b3999cde5a5541c368d159fa0314d36ed16529186ed45 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala4eb8099bac9ccb17f4b3999cde5a5541c368d159fa0314d36ed16529186ed45->enter($internala4eb8099bac9ccb17f4b3999cde5a5541c368d159fa0314d36ed16529186ed45prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/event.svg");
        echo "</span>
    <strong>Events</strong>
</span>
";
        
        $internala4eb8099bac9ccb17f4b3999cde5a5541c368d159fa0314d36ed16529186ed45->leave($internala4eb8099bac9ccb17f4b3999cde5a5541c368d159fa0314d36ed16529186ed45prof);

        
        $internal5c778c4e5063fbb1d72184f8076998ac14641fc26c0f43acd781d9ae79630166->leave($internal5c778c4e5063fbb1d72184f8076998ac14641fc26c0f43acd781d9ae79630166prof);

    }

    // line 12
    public function blockpanel($context, array $blocks = array())
    {
        $internal6b118a30d5a027d44f256522c651eb00ad3461b5ce9c13c105bcc12e2ac96c81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal6b118a30d5a027d44f256522c651eb00ad3461b5ce9c13c105bcc12e2ac96c81->enter($internal6b118a30d5a027d44f256522c651eb00ad3461b5ce9c13c105bcc12e2ac96c81prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal29d625b9f1730fa08353f064945e081d0b628e6c9753b86e008e4aff3790d97f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal29d625b9f1730fa08353f064945e081d0b628e6c9753b86e008e4aff3790d97f->enter($internal29d625b9f1730fa08353f064945e081d0b628e6c9753b86e008e4aff3790d97fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    <h2>Event Dispatcher</h2>

    ";
        // line 15
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 15, $this->getSourceContext()); })()), "calledlisteners", array()))) {
            // line 16
            echo "        <div class=\"empty\">
            <p>No events have been recorded. Check that debugging is enabled in the kernel.</p>
        </div>
    ";
        } else {
            // line 20
            echo "        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Called Listeners <span class=\"badge\">";
            // line 22
            echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 22, $this->getSourceContext()); })()), "calledlisteners", array())), "html", null, true);
            echo "</span></h3>

                <div class=\"tab-content\">
                    ";
            // line 25
            echo $context["helper"]->macrorendertable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 25, $this->getSourceContext()); })()), "calledlisteners", array()));
            echo "
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Not Called Listeners <span class=\"badge\">";
            // line 30
            echo twigescapefilter($this->env, twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 30, $this->getSourceContext()); })()), "notcalledlisteners", array())), "html", null, true);
            echo "</span></h3>
                <div class=\"tab-content\">
                    ";
            // line 32
            if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 32, $this->getSourceContext()); })()), "notcalledlisteners", array()))) {
                // line 33
                echo "                        <div class=\"empty\">
                            <p>
                                <strong>There are no uncalled listeners</strong>.
                            </p>
                            <p>
                                All listeners were called for this request or an error occurred
                                when trying to collect uncalled listeners (in which case check the
                                logs to get more information).
                            </p>
                        </div>
                    ";
            } else {
                // line 44
                echo "                        ";
                echo $context["helper"]->macrorendertable(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 44, $this->getSourceContext()); })()), "notcalledlisteners", array()));
                echo "
                    ";
            }
            // line 46
            echo "                </div>
            </div>
        </div>
    ";
        }
        
        $internal29d625b9f1730fa08353f064945e081d0b628e6c9753b86e008e4aff3790d97f->leave($internal29d625b9f1730fa08353f064945e081d0b628e6c9753b86e008e4aff3790d97fprof);

        
        $internal6b118a30d5a027d44f256522c651eb00ad3461b5ce9c13c105bcc12e2ac96c81->leave($internal6b118a30d5a027d44f256522c651eb00ad3461b5ce9c13c105bcc12e2ac96c81prof);

    }

    // line 52
    public function macrorendertable($listeners = null, ...$varargs)
    {
        $context = $this->env->mergeGlobals(array(
            "listeners" => $listeners,
            "varargs" => $varargs,
        ));

        $blocks = array();

        obstart();
        try {
            $internal588a77a744f7ec926c81ea4a29cb4d608323bb8aa36df0970b8480c25d614e92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $internal588a77a744f7ec926c81ea4a29cb4d608323bb8aa36df0970b8480c25d614e92->enter($internal588a77a744f7ec926c81ea4a29cb4d608323bb8aa36df0970b8480c25d614e92prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            $internalec162c7bd936944c39b2086a4e95ade130a07b201edc6878d0b292c490053568 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $internalec162c7bd936944c39b2086a4e95ade130a07b201edc6878d0b292c490053568->enter($internalec162c7bd936944c39b2086a4e95ade130a07b201edc6878d0b292c490053568prof = new TwigProfilerProfile($this->getTemplateName(), "macro", "rendertable"));

            // line 53
            echo "    <table>
        <thead>
            <tr>
                <th class=\"text-right\">Priority</th>
                <th>Listener</th>
            </tr>
        </thead>

        ";
            // line 61
            $context["previousevent"] = twiggetattribute($this->env, $this->getSourceContext(), twigfirst($this->env, (isset($context["listeners"]) || arraykeyexists("listeners", $context) ? $context["listeners"] : (function () { throw new TwigErrorRuntime('Variable "listeners" does not exist.', 61, $this->getSourceContext()); })())), "event", array());
            // line 62
            echo "        ";
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["listeners"]) || arraykeyexists("listeners", $context) ? $context["listeners"] : (function () { throw new TwigErrorRuntime('Variable "listeners" does not exist.', 62, $this->getSourceContext()); })()));
            $context['loop'] = array(
              'parent' => $context['parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
                $length = count($context['seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['seq'] as $context["key"] => $context["listener"]) {
                // line 63
                echo "            ";
                if ((twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array()) || (twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "event", array()) != (isset($context["previousevent"]) || arraykeyexists("previousevent", $context) ? $context["previousevent"] : (function () { throw new TwigErrorRuntime('Variable "previousevent" does not exist.', 63, $this->getSourceContext()); })())))) {
                    // line 64
                    echo "                ";
                    if ( !twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "first", array())) {
                        // line 65
                        echo "                    </tbody>
                ";
                    }
                    // line 67
                    echo "
                <tbody>
                    <tr>
                        <th colspan=\"2\" class=\"colored font-normal\">";
                    // line 70
                    echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "event", array()), "html", null, true);
                    echo "</th>
                    </tr>

                ";
                    // line 73
                    $context["previousevent"] = twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "event", array());
                    // line 74
                    echo "            ";
                }
                // line 75
                echo "
            <tr>
                <td class=\"text-right\">";
                // line 77
                echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "priority", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "priority", array()), "-")) : ("-")), "html", null, true);
                echo "</td>
                <td class=\"font-normal\">";
                // line 78
                echo calluserfuncarray($this->env->getFunction('profilerdump')->getCallable(), array($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["listener"], "stub", array())));
                echo "</td>
            </tr>

            ";
                // line 81
                if (twiggetattribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    // line 82
                    echo "                </tbody>
            ";
                }
                // line 84
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['listener'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 85
            echo "    </table>
";
            
            $internalec162c7bd936944c39b2086a4e95ade130a07b201edc6878d0b292c490053568->leave($internalec162c7bd936944c39b2086a4e95ade130a07b201edc6878d0b292c490053568prof);

            
            $internal588a77a744f7ec926c81ea4a29cb4d608323bb8aa36df0970b8480c25d614e92->leave($internal588a77a744f7ec926c81ea4a29cb4d608323bb8aa36df0970b8480c25d614e92prof);


            return ('' === $tmp = obgetcontents()) ? '' : new TwigMarkup($tmp, $this->env->getCharset());
        } finally {
            obendclean();
        }
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:events.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 85,  243 => 84,  239 => 82,  237 => 81,  231 => 78,  227 => 77,  223 => 75,  220 => 74,  218 => 73,  212 => 70,  207 => 67,  203 => 65,  200 => 64,  197 => 63,  179 => 62,  177 => 61,  167 => 53,  149 => 52,  135 => 46,  129 => 44,  116 => 33,  114 => 32,  109 => 30,  101 => 25,  95 => 22,  91 => 20,  85 => 16,  83 => 15,  79 => 13,  70 => 12,  56 => 7,  53 => 6,  44 => 5,  34 => 1,  32 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% import self as helper %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/event.svg') }}</span>
    <strong>Events</strong>
</span>
{% endblock %}

{% block panel %}
    <h2>Event Dispatcher</h2>

    {% if collector.calledlisteners is empty %}
        <div class=\"empty\">
            <p>No events have been recorded. Check that debugging is enabled in the kernel.</p>
        </div>
    {% else %}
        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Called Listeners <span class=\"badge\">{{ collector.calledlisteners|length }}</span></h3>

                <div class=\"tab-content\">
                    {{ helper.rendertable(collector.calledlisteners) }}
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Not Called Listeners <span class=\"badge\">{{ collector.notcalledlisteners|length }}</span></h3>
                <div class=\"tab-content\">
                    {% if collector.notcalledlisteners is empty %}
                        <div class=\"empty\">
                            <p>
                                <strong>There are no uncalled listeners</strong>.
                            </p>
                            <p>
                                All listeners were called for this request or an error occurred
                                when trying to collect uncalled listeners (in which case check the
                                logs to get more information).
                            </p>
                        </div>
                    {% else %}
                        {{ helper.rendertable(collector.notcalledlisteners) }}
                    {% endif %}
                </div>
            </div>
        </div>
    {% endif %}
{% endblock %}

{% macro rendertable(listeners) %}
    <table>
        <thead>
            <tr>
                <th class=\"text-right\">Priority</th>
                <th>Listener</th>
            </tr>
        </thead>

        {% set previousevent = (listeners|first).event %}
        {% for listener in listeners %}
            {% if loop.first or listener.event != previousevent %}
                {% if not loop.first %}
                    </tbody>
                {% endif %}

                <tbody>
                    <tr>
                        <th colspan=\"2\" class=\"colored font-normal\">{{ listener.event }}</th>
                    </tr>

                {% set previousevent = listener.event %}
            {% endif %}

            <tr>
                <td class=\"text-right\">{{ listener.priority|default('-') }}</td>
                <td class=\"font-normal\">{{ profilerdump(listener.stub) }}</td>
            </tr>

            {% if loop.last %}
                </tbody>
            {% endif %}
        {% endfor %}
    </table>
{% endmacro %}
", "WebProfilerBundle:Collector:events.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/events.html.twig");
    }
}
