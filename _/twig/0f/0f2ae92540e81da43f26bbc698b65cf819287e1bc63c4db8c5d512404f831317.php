<?php

/* FOSUserBundle:Group:edit.html.twig */
class TwigTemplatedd9a441438f48acacab283ae24f63f5e6fb39ac965148ac867ce191964fce90e extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9f17d504ca555f220a261c16ead50ea89651e8e5726221169ea49b31949654ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9f17d504ca555f220a261c16ead50ea89651e8e5726221169ea49b31949654ba->enter($internal9f17d504ca555f220a261c16ead50ea89651e8e5726221169ea49b31949654baprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $internalc90cde83f90e863a75b1261734509eb38ff1b8061144999688ab45952ff86a48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc90cde83f90e863a75b1261734509eb38ff1b8061144999688ab45952ff86a48->enter($internalc90cde83f90e863a75b1261734509eb38ff1b8061144999688ab45952ff86a48prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal9f17d504ca555f220a261c16ead50ea89651e8e5726221169ea49b31949654ba->leave($internal9f17d504ca555f220a261c16ead50ea89651e8e5726221169ea49b31949654baprof);

        
        $internalc90cde83f90e863a75b1261734509eb38ff1b8061144999688ab45952ff86a48->leave($internalc90cde83f90e863a75b1261734509eb38ff1b8061144999688ab45952ff86a48prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internal29f04b50d35fdf745e37d230bf47bc60356d21a645c8dc9f3e9f04a8db23253b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal29f04b50d35fdf745e37d230bf47bc60356d21a645c8dc9f3e9f04a8db23253b->enter($internal29f04b50d35fdf745e37d230bf47bc60356d21a645c8dc9f3e9f04a8db23253bprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal593ae921d356a2d290916510d783f921b55e0631528a42deb5aec0334b35ede5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal593ae921d356a2d290916510d783f921b55e0631528a42deb5aec0334b35ede5->enter($internal593ae921d356a2d290916510d783f921b55e0631528a42deb5aec0334b35ede5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/editcontent.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $internal593ae921d356a2d290916510d783f921b55e0631528a42deb5aec0334b35ede5->leave($internal593ae921d356a2d290916510d783f921b55e0631528a42deb5aec0334b35ede5prof);

        
        $internal29f04b50d35fdf745e37d230bf47bc60356d21a645c8dc9f3e9f04a8db23253b->leave($internal29f04b50d35fdf745e37d230bf47bc60356d21a645c8dc9f3e9f04a8db23253bprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/Group/editcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:Group:edit.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}
