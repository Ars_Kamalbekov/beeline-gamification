<?php

/* SonataAdminBundle:Button:editbutton.html.twig */
class TwigTemplate86461e4e3e779507a381b9479b55bc8f917045b751c7258b04ab66515002aa11 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal33f95028defd697f01ad596ef30883d9f5b3d7d743fcbaae2fbc39e686adfdfb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal33f95028defd697f01ad596ef30883d9f5b3d7d743fcbaae2fbc39e686adfdfb->enter($internal33f95028defd697f01ad596ef30883d9f5b3d7d743fcbaae2fbc39e686adfdfbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:editbutton.html.twig"));

        $internal47facda4c5e5d62d83c4ceef4b24a7954c2061b83900ddddc482136e6462e745 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal47facda4c5e5d62d83c4ceef4b24a7954c2061b83900ddddc482136e6462e745->enter($internal47facda4c5e5d62d83c4ceef4b24a7954c2061b83900ddddc482136e6462e745prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Button:editbutton.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "canAccessObject", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 12, $this->getSourceContext()); })())), "method") && twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "hasRoute", array(0 => "edit"), "method"))) {
            // line 13
            echo "    <li>
        <a class=\"sonata-action-element\" href=\"";
            // line 14
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 14, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
            <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>
            ";
            // line 16
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("linkactionedit", array(), "SonataAdminBundle"), "html", null, true);
            echo "
        </a>
    </li>
";
        }
        
        $internal33f95028defd697f01ad596ef30883d9f5b3d7d743fcbaae2fbc39e686adfdfb->leave($internal33f95028defd697f01ad596ef30883d9f5b3d7d743fcbaae2fbc39e686adfdfbprof);

        
        $internal47facda4c5e5d62d83c4ceef4b24a7954c2061b83900ddddc482136e6462e745->leave($internal47facda4c5e5d62d83c4ceef4b24a7954c2061b83900ddddc482136e6462e745prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Button:editbutton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 16,  33 => 14,  30 => 13,  28 => 12,  25 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.canAccessObject('edit', object) and admin.hasRoute('edit') %}
    <li>
        <a class=\"sonata-action-element\" href=\"{{ admin.generateObjectUrl('edit', object) }}\">
            <i class=\"fa fa-edit\" aria-hidden=\"true\"></i>
            {{ 'linkactionedit'|trans({}, 'SonataAdminBundle') }}
        </a>
    </li>
{% endif %}
", "SonataAdminBundle:Button:editbutton.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Button/editbutton.html.twig");
    }
}
