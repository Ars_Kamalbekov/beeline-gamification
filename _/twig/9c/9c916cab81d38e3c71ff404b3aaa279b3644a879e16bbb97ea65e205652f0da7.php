<?php

/* WebProfilerBundle:Profiler:ajaxlayout.html.twig */
class TwigTemplate42d7c3598267bddb92da5faa0bfd660794b0213be3d08c246fe139ec874c4452 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1c6523d4a139a7b72e911ed2742310526399dcc9380ad403d2d3deb6af463baa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1c6523d4a139a7b72e911ed2742310526399dcc9380ad403d2d3deb6af463baa->enter($internal1c6523d4a139a7b72e911ed2742310526399dcc9380ad403d2d3deb6af463baaprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajaxlayout.html.twig"));

        $internaleb26cf392e8dc7018d93952d54579f5d38111eedf405ace34d7a253a5209269a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaleb26cf392e8dc7018d93952d54579f5d38111eedf405ace34d7a253a5209269a->enter($internaleb26cf392e8dc7018d93952d54579f5d38111eedf405ace34d7a253a5209269aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajaxlayout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $internal1c6523d4a139a7b72e911ed2742310526399dcc9380ad403d2d3deb6af463baa->leave($internal1c6523d4a139a7b72e911ed2742310526399dcc9380ad403d2d3deb6af463baaprof);

        
        $internaleb26cf392e8dc7018d93952d54579f5d38111eedf405ace34d7a253a5209269a->leave($internaleb26cf392e8dc7018d93952d54579f5d38111eedf405ace34d7a253a5209269aprof);

    }

    public function blockpanel($context, array $blocks = array())
    {
        $internal52710045132dd182f4336b2df9c8d9b84e559f670faacf5d00a9b81159cb47d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal52710045132dd182f4336b2df9c8d9b84e559f670faacf5d00a9b81159cb47d5->enter($internal52710045132dd182f4336b2df9c8d9b84e559f670faacf5d00a9b81159cb47d5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internal35b33ffd45d85cdd88d4292692c2fb785119aecdb4c968740d454b13fa851935 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal35b33ffd45d85cdd88d4292692c2fb785119aecdb4c968740d454b13fa851935->enter($internal35b33ffd45d85cdd88d4292692c2fb785119aecdb4c968740d454b13fa851935prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $internal35b33ffd45d85cdd88d4292692c2fb785119aecdb4c968740d454b13fa851935->leave($internal35b33ffd45d85cdd88d4292692c2fb785119aecdb4c968740d454b13fa851935prof);

        
        $internal52710045132dd182f4336b2df9c8d9b84e559f670faacf5d00a9b81159cb47d5->leave($internal52710045132dd182f4336b2df9c8d9b84e559f670faacf5d00a9b81159cb47d5prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajaxlayout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajaxlayout.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajaxlayout.html.twig");
    }
}
