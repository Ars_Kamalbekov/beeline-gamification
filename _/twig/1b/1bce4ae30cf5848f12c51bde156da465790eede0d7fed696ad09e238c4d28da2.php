<?php

/* SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig */
class TwigTemplatec280757fcf9c5e6b711eb20b85578e9e0e32cb9d87f58e45b1a8248afbfcac50 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Form:formadminfields.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 12);
        $this->blocks = array(
            'sonataadminormonetoonewidget' => array($this, 'blocksonataadminormonetoonewidget'),
            'sonataadminormmanytomanywidget' => array($this, 'blocksonataadminormmanytomanywidget'),
            'sonataadminormmanytoonewidget' => array($this, 'blocksonataadminormmanytoonewidget'),
            'sonataadminormonetomanywidget' => array($this, 'blocksonataadminormonetomanywidget'),
            'sonatatypemodelwidget' => array($this, 'blocksonatatypemodelwidget'),
            'sonatatypemodellistwidget' => array($this, 'blocksonatatypemodellistwidget'),
            'sonatatypeadminwidget' => array($this, 'blocksonatatypeadminwidget'),
            'sonatatypecollectionwidget' => array($this, 'blocksonatatypecollectionwidget'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Form:formadminfields.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2b4795dacaf424a4e1b732624d6c49baa736778fa951a31c8c61e1f1b6b50356 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2b4795dacaf424a4e1b732624d6c49baa736778fa951a31c8c61e1f1b6b50356->enter($internal2b4795dacaf424a4e1b732624d6c49baa736778fa951a31c8c61e1f1b6b50356prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig"));

        $internalaf560ee03689150bfeb01273e655b97453c1302cc1daac2f1367035c383a6d6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaf560ee03689150bfeb01273e655b97453c1302cc1daac2f1367035c383a6d6d->enter($internalaf560ee03689150bfeb01273e655b97453c1302cc1daac2f1367035c383a6d6dprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal2b4795dacaf424a4e1b732624d6c49baa736778fa951a31c8c61e1f1b6b50356->leave($internal2b4795dacaf424a4e1b732624d6c49baa736778fa951a31c8c61e1f1b6b50356prof);

        
        $internalaf560ee03689150bfeb01273e655b97453c1302cc1daac2f1367035c383a6d6d->leave($internalaf560ee03689150bfeb01273e655b97453c1302cc1daac2f1367035c383a6d6dprof);

    }

    // line 16
    public function blocksonataadminormonetoonewidget($context, array $blocks = array())
    {
        $internal49aac269ef1a869e6e989a3ed27c1503d34a498338d7032dcee92235593a6f25 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal49aac269ef1a869e6e989a3ed27c1503d34a498338d7032dcee92235593a6f25->enter($internal49aac269ef1a869e6e989a3ed27c1503d34a498338d7032dcee92235593a6f25prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormonetoonewidget"));

        $internal58811c520a829af912b8b81a42a76a5e8af0128de51c1719b633b0d3d03500fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal58811c520a829af912b8b81a42a76a5e8af0128de51c1719b633b0d3d03500fe->enter($internal58811c520a829af912b8b81a42a76a5e8af0128de51c1719b633b0d3d03500feprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormonetoonewidget"));

        // line 17
        echo "    ";
        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetoone.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 17)->display($context);
        
        $internal58811c520a829af912b8b81a42a76a5e8af0128de51c1719b633b0d3d03500fe->leave($internal58811c520a829af912b8b81a42a76a5e8af0128de51c1719b633b0d3d03500feprof);

        
        $internal49aac269ef1a869e6e989a3ed27c1503d34a498338d7032dcee92235593a6f25->leave($internal49aac269ef1a869e6e989a3ed27c1503d34a498338d7032dcee92235593a6f25prof);

    }

    // line 20
    public function blocksonataadminormmanytomanywidget($context, array $blocks = array())
    {
        $internal2ff54762e0add81cf01ab6d3ace38988ad9fc981d7ead130443be209e2d9d8bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2ff54762e0add81cf01ab6d3ace38988ad9fc981d7ead130443be209e2d9d8bc->enter($internal2ff54762e0add81cf01ab6d3ace38988ad9fc981d7ead130443be209e2d9d8bcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormmanytomanywidget"));

        $internal0afcb247c5ad1ab71686e2a71cc8ad714b692325c61e9ec941a720f907d53cd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0afcb247c5ad1ab71686e2a71cc8ad714b692325c61e9ec941a720f907d53cd0->enter($internal0afcb247c5ad1ab71686e2a71cc8ad714b692325c61e9ec941a720f907d53cd0prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormmanytomanywidget"));

        // line 21
        echo "    ";
        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 21)->display($context);
        
        $internal0afcb247c5ad1ab71686e2a71cc8ad714b692325c61e9ec941a720f907d53cd0->leave($internal0afcb247c5ad1ab71686e2a71cc8ad714b692325c61e9ec941a720f907d53cd0prof);

        
        $internal2ff54762e0add81cf01ab6d3ace38988ad9fc981d7ead130443be209e2d9d8bc->leave($internal2ff54762e0add81cf01ab6d3ace38988ad9fc981d7ead130443be209e2d9d8bcprof);

    }

    // line 24
    public function blocksonataadminormmanytoonewidget($context, array $blocks = array())
    {
        $internalb2bdc3afaf46d82c0513ecf8c459119887c11df2a4c4b6792277d4cd99a8102e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb2bdc3afaf46d82c0513ecf8c459119887c11df2a4c4b6792277d4cd99a8102e->enter($internalb2bdc3afaf46d82c0513ecf8c459119887c11df2a4c4b6792277d4cd99a8102eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormmanytoonewidget"));

        $internalaafe12977648865811abb59a0e72e2de808d0accb5a66f9d9e14d779d82839a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalaafe12977648865811abb59a0e72e2de808d0accb5a66f9d9e14d779d82839a7->enter($internalaafe12977648865811abb59a0e72e2de808d0accb5a66f9d9e14d779d82839a7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormmanytoonewidget"));

        // line 25
        echo "    ";
        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormmanytoone.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 25)->display($context);
        
        $internalaafe12977648865811abb59a0e72e2de808d0accb5a66f9d9e14d779d82839a7->leave($internalaafe12977648865811abb59a0e72e2de808d0accb5a66f9d9e14d779d82839a7prof);

        
        $internalb2bdc3afaf46d82c0513ecf8c459119887c11df2a4c4b6792277d4cd99a8102e->leave($internalb2bdc3afaf46d82c0513ecf8c459119887c11df2a4c4b6792277d4cd99a8102eprof);

    }

    // line 28
    public function blocksonataadminormonetomanywidget($context, array $blocks = array())
    {
        $internal41a3f6237c0136073efe7d630467575d147b254cc59b5f9ab56ecf7812070dc4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal41a3f6237c0136073efe7d630467575d147b254cc59b5f9ab56ecf7812070dc4->enter($internal41a3f6237c0136073efe7d630467575d147b254cc59b5f9ab56ecf7812070dc4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormonetomanywidget"));

        $internalff6813f578bf42af38ecea6dafc34368c14a0788b9e83ca136159f9ab782b965 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalff6813f578bf42af38ecea6dafc34368c14a0788b9e83ca136159f9ab782b965->enter($internalff6813f578bf42af38ecea6dafc34368c14a0788b9e83ca136159f9ab782b965prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonataadminormonetomanywidget"));

        // line 29
        echo "    ";
        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 29)->display($context);
        
        $internalff6813f578bf42af38ecea6dafc34368c14a0788b9e83ca136159f9ab782b965->leave($internalff6813f578bf42af38ecea6dafc34368c14a0788b9e83ca136159f9ab782b965prof);

        
        $internal41a3f6237c0136073efe7d630467575d147b254cc59b5f9ab56ecf7812070dc4->leave($internal41a3f6237c0136073efe7d630467575d147b254cc59b5f9ab56ecf7812070dc4prof);

    }

    // line 32
    public function blocksonatatypemodelwidget($context, array $blocks = array())
    {
        $internal5dba3167612af832810fda20bf395aab64dadead5cbe69b51368a7bbcd1e96e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5dba3167612af832810fda20bf395aab64dadead5cbe69b51368a7bbcd1e96e4->enter($internal5dba3167612af832810fda20bf395aab64dadead5cbe69b51368a7bbcd1e96e4prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelwidget"));

        $internalf811b7021e5bbea55b98dca1731a457d70ea2a4677b2eb9845b9519f4c190093 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf811b7021e5bbea55b98dca1731a457d70ea2a4677b2eb9845b9519f4c190093->enter($internalf811b7021e5bbea55b98dca1731a457d70ea2a4677b2eb9845b9519f4c190093prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodelwidget"));

        // line 33
        echo "    ";
        // line 37
        echo "
    ";
        // line 39
        echo "
    ";
        // line 40
        if (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 40, $this->getSourceContext()); })()), "fielddescription", array()))) {
            // line 41
            echo "        ";
            $this->displayBlock("choicewidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 42
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 42, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::ONETOONE"))) {
            // line 43
            echo "        ";
            $this->displayBlock("sonataadminormonetoonewidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 44
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 44, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::MANYTOONE"))) {
            // line 45
            echo "        ";
            $this->displayBlock("sonataadminormmanytoonewidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 46
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 46, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::MANYTOMANY"))) {
            // line 47
            echo "        ";
            $this->displayBlock("sonataadminormmanytomanywidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 48
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 48, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::ONETOMANY"))) {
            // line 49
            echo "        ";
            $this->displayBlock("sonataadminormonetomanywidget", $context, $blocks);
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            // line 52
            echo "        ";
            $this->displayBlock("choicewidget", $context, $blocks);
            echo "
    ";
        }
        
        $internalf811b7021e5bbea55b98dca1731a457d70ea2a4677b2eb9845b9519f4c190093->leave($internalf811b7021e5bbea55b98dca1731a457d70ea2a4677b2eb9845b9519f4c190093prof);

        
        $internal5dba3167612af832810fda20bf395aab64dadead5cbe69b51368a7bbcd1e96e4->leave($internal5dba3167612af832810fda20bf395aab64dadead5cbe69b51368a7bbcd1e96e4prof);

    }

    // line 56
    public function blocksonatatypemodellistwidget($context, array $blocks = array())
    {
        $internal3c5d4f287f700a782192def92e8f90fa2da58dda5c418901b9e697f7c1456ca7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3c5d4f287f700a782192def92e8f90fa2da58dda5c418901b9e697f7c1456ca7->enter($internal3c5d4f287f700a782192def92e8f90fa2da58dda5c418901b9e697f7c1456ca7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodellistwidget"));

        $internalf440f4792825810cc1f3e4f18608830e2511afab8dbaccea4587f4579c61f19f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf440f4792825810cc1f3e4f18608830e2511afab8dbaccea4587f4579c61f19f->enter($internalf440f4792825810cc1f3e4f18608830e2511afab8dbaccea4587f4579c61f19fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypemodellistwidget"));

        // line 57
        echo "    <div id=\"fieldcontainer";
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 57, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"field-container\">
        <span id=\"fieldwidget";
        // line 58
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 58, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"field-short-description\">
            ";
        // line 59
        if (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 59, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 59, $this->getSourceContext()); })()), "value", array())), "method")) {
            // line 60
            echo "                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonataadminshortobjectinformation", array("code" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 61
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 61, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "code", array()), "objectId" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 62
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 62, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "id", array(0 => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 62, $this->getSourceContext()); })()), "value", array())), "method"), "uniqid" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 63
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 63, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "uniqid", array()), "linkParameters" => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 64
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 64, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "linkparameters", array()))));
            // line 65
            echo "
            ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 66
($context["sonataadmin"] ?? null), "fielddescription", array(), "any", false, true), "options", array(), "any", false, true), "placeholder", array(), "any", true, true) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 66, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "placeholder", array()))) {
            // line 67
            echo "                <span class=\"inner-field-short-description\">
                    ";
            // line 68
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 68, $this->getSourceContext()); })()), "fielddescription", array()), "options", array()), "placeholder", array()), array(), "SonataAdminBundle"), "html", null, true);
            echo "
                </span>
            ";
        }
        // line 71
        echo "        </span>
        <span id=\"fieldactions";
        // line 72
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 72, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"field-actions\">
            <span class=\"btn-group\">
                ";
        // line 74
        if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 74, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasroute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 74, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "LIST"), "method")) && (isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 74, $this->getSourceContext()); })()))) {
            // line 75
            echo "                    <a  href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 75, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "list"), "method"), "html", null, true);
            echo "\"
                        onclick=\"return startfielddialogformlist";
            // line 76
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 76, $this->getSourceContext()); })()), "html", null, true);
            echo "(this);\"
                        class=\"btn btn-info btn-sm sonata-ba-action\"
                        title=\"";
            // line 78
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 78, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 78, $this->getSourceContext()); })())), "html", null, true);
            echo "\"
                            >
                        <i class=\"fa fa-list\"></i>
                        ";
            // line 81
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnlist"]) || arraykeyexists("btnlist", $context) ? $context["btnlist"] : (function () { throw new TwigErrorRuntime('Variable "btnlist" does not exist.', 81, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 81, $this->getSourceContext()); })())), "html", null, true);
            echo "
                    </a>
                ";
        }
        // line 84
        echo "
                ";
        // line 85
        if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 85, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasroute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 85, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "CREATE"), "method")) && (isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 85, $this->getSourceContext()); })()))) {
            // line 86
            echo "                    <a  href=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 86, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "generateUrl", array(0 => "create"), "method"), "html", null, true);
            echo "\"
                        onclick=\"return startfielddialogformadd";
            // line 87
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 87, $this->getSourceContext()); })()), "html", null, true);
            echo "(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"";
            // line 89
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 89, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 89, $this->getSourceContext()); })())), "html", null, true);
            echo "\"
                            >
                        <i class=\"fa fa-plus-circle\"></i>
                        ";
            // line 92
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btnadd"]) || arraykeyexists("btnadd", $context) ? $context["btnadd"] : (function () { throw new TwigErrorRuntime('Variable "btnadd" does not exist.', 92, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 92, $this->getSourceContext()); })())), "html", null, true);
            echo "
                    </a>
                ";
        }
        // line 95
        echo "            </span>

            <span class=\"btn-group\">
                ";
        // line 98
        if (((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 98, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "hasRoute", array(0 => "delete"), "method") && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 98, $this->getSourceContext()); })()), "fielddescription", array()), "associationadmin", array()), "isGranted", array(0 => "DELETE"), "method")) && (isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 98, $this->getSourceContext()); })()))) {
            // line 99
            echo "                    <a  href=\"\"
                        onclick=\"return removeselectedelement";
            // line 100
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 100, $this->getSourceContext()); })()), "html", null, true);
            echo "(this);\"
                        class=\"btn btn-danger btn-sm sonata-ba-action\"
                        title=\"";
            // line 102
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 102, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 102, $this->getSourceContext()); })())), "html", null, true);
            echo "\"
                            >
                        <i class=\"fa fa-minus-circle\"></i>
                        ";
            // line 105
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["btndelete"]) || arraykeyexists("btndelete", $context) ? $context["btndelete"] : (function () { throw new TwigErrorRuntime('Variable "btndelete" does not exist.', 105, $this->getSourceContext()); })()), array(), (isset($context["btncatalogue"]) || arraykeyexists("btncatalogue", $context) ? $context["btncatalogue"] : (function () { throw new TwigErrorRuntime('Variable "btncatalogue" does not exist.', 105, $this->getSourceContext()); })())), "html", null, true);
            echo "
                    </a>
                ";
        }
        // line 108
        echo "            </span>
        </span>

        <span style=\"display: none\" >
            ";
        // line 113
        echo "            ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || arraykeyexists("form", $context) ? $context["form"] : (function () { throw new TwigErrorRuntime('Variable "form" does not exist.', 113, $this->getSourceContext()); })()), 'widget', array("required" => false));
        echo "
        </span>

        ";
        // line 116
        $this->displayBlock("sonatahelp", $context, $blocks);
        echo "

        <div class=\"modal fade\" id=\"fielddialog";
        // line 118
        echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 118, $this->getSourceContext()); })()), "html", null, true);
        echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-lg\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                        <h4 class=\"modal-title\"></h4>
                    </div>
                    <div class=\"modal-body\">
                    </div>
                </div>
            </div>
        </div>
    </div>

    ";
        // line 132
        $this->loadTemplate("SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", 132)->display($context);
        
        $internalf440f4792825810cc1f3e4f18608830e2511afab8dbaccea4587f4579c61f19f->leave($internalf440f4792825810cc1f3e4f18608830e2511afab8dbaccea4587f4579c61f19fprof);

        
        $internal3c5d4f287f700a782192def92e8f90fa2da58dda5c418901b9e697f7c1456ca7->leave($internal3c5d4f287f700a782192def92e8f90fa2da58dda5c418901b9e697f7c1456ca7prof);

    }

    // line 135
    public function blocksonatatypeadminwidget($context, array $blocks = array())
    {
        $internala21cda01e48dfb83a3a28f46eeef07498dc3b5604d0e71edb60958d67a4ded93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala21cda01e48dfb83a3a28f46eeef07498dc3b5604d0e71edb60958d67a4ded93->enter($internala21cda01e48dfb83a3a28f46eeef07498dc3b5604d0e71edb60958d67a4ded93prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeadminwidget"));

        $internal081214d6249494d9dd6b53fba4b4fd53a8545eda1d509a9fd73bc67ca3dd783a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal081214d6249494d9dd6b53fba4b4fd53a8545eda1d509a9fd73bc67ca3dd783a->enter($internal081214d6249494d9dd6b53fba4b4fd53a8545eda1d509a9fd73bc67ca3dd783aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypeadminwidget"));

        // line 136
        echo "    ";
        // line 137
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 137, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::ONETOONE"))) {
            // line 138
            echo "        ";
            $this->displayBlock("sonataadminormonetoonewidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 139
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 139, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::MANYTOONE"))) {
            // line 140
            echo "        ";
            $this->displayBlock("sonataadminormmanytoonewidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 141
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 141, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::MANYTOMANY"))) {
            // line 142
            echo "        ";
            $this->displayBlock("sonataadminormmanytomanywidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 143
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 143, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::ONETOMANY"))) {
            // line 144
            echo "        ";
            $this->displayBlock("sonataadminormonetomanywidget", $context, $blocks);
            echo "
    ";
        } else {
            // line 146
            echo "        INVALID MODE : ";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 146, $this->getSourceContext()); })()), "html", null, true);
            echo "
    ";
        }
        
        $internal081214d6249494d9dd6b53fba4b4fd53a8545eda1d509a9fd73bc67ca3dd783a->leave($internal081214d6249494d9dd6b53fba4b4fd53a8545eda1d509a9fd73bc67ca3dd783aprof);

        
        $internala21cda01e48dfb83a3a28f46eeef07498dc3b5604d0e71edb60958d67a4ded93->leave($internala21cda01e48dfb83a3a28f46eeef07498dc3b5604d0e71edb60958d67a4ded93prof);

    }

    // line 150
    public function blocksonatatypecollectionwidget($context, array $blocks = array())
    {
        $internald7c128632496816001f4ce7ca2b176bccaa760bb39cf2592a4d4a8b5f0d2cc09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald7c128632496816001f4ce7ca2b176bccaa760bb39cf2592a4d4a8b5f0d2cc09->enter($internald7c128632496816001f4ce7ca2b176bccaa760bb39cf2592a4d4a8b5f0d2cc09prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypecollectionwidget"));

        $internal32e11696a50403d809ce1fea07e3a3804894fd17ed39f632bf659c93d2757797 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal32e11696a50403d809ce1fea07e3a3804894fd17ed39f632bf659c93d2757797->enter($internal32e11696a50403d809ce1fea07e3a3804894fd17ed39f632bf659c93d2757797prof = new TwigProfilerProfile($this->getTemplateName(), "block", "sonatatypecollectionwidget"));

        // line 151
        echo "    ";
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 151, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::ONETOMANY"))) {
            // line 152
            echo "        ";
            $this->displayBlock("sonataadminormonetomanywidget", $context, $blocks);
            echo "
    ";
        } elseif ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),         // line 153
(isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 153, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()) == twigconstant("Doctrine\\ORM\\Mapping\\ClassMetadataInfo::MANYTOMANY"))) {
            // line 154
            echo "        ";
            $this->displayBlock("sonataadminormmanytomanywidget", $context, $blocks);
            echo "
    ";
        } else {
            // line 156
            echo "        INVALID MODE : ";
            echo twigescapefilter($this->env, (isset($context["id"]) || arraykeyexists("id", $context) ? $context["id"] : (function () { throw new TwigErrorRuntime('Variable "id" does not exist.', 156, $this->getSourceContext()); })()), "html", null, true);
            echo " - type : sonatatypecollection - mapping : ";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonataadmin"]) || arraykeyexists("sonataadmin", $context) ? $context["sonataadmin"] : (function () { throw new TwigErrorRuntime('Variable "sonataadmin" does not exist.', 156, $this->getSourceContext()); })()), "fielddescription", array()), "mappingtype", array()), "html", null, true);
            echo "
    ";
        }
        
        $internal32e11696a50403d809ce1fea07e3a3804894fd17ed39f632bf659c93d2757797->leave($internal32e11696a50403d809ce1fea07e3a3804894fd17ed39f632bf659c93d2757797prof);

        
        $internald7c128632496816001f4ce7ca2b176bccaa760bb39cf2592a4d4a8b5f0d2cc09->leave($internald7c128632496816001f4ce7ca2b176bccaa760bb39cf2592a4d4a8b5f0d2cc09prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  448 => 156,  442 => 154,  440 => 153,  435 => 152,  432 => 151,  423 => 150,  409 => 146,  403 => 144,  401 => 143,  396 => 142,  394 => 141,  389 => 140,  387 => 139,  382 => 138,  379 => 137,  377 => 136,  368 => 135,  358 => 132,  341 => 118,  336 => 116,  329 => 113,  323 => 108,  317 => 105,  311 => 102,  306 => 100,  303 => 99,  301 => 98,  296 => 95,  290 => 92,  284 => 89,  279 => 87,  274 => 86,  272 => 85,  269 => 84,  263 => 81,  257 => 78,  252 => 76,  247 => 75,  245 => 74,  240 => 72,  237 => 71,  231 => 68,  228 => 67,  226 => 66,  223 => 65,  221 => 64,  220 => 63,  219 => 62,  218 => 61,  216 => 60,  214 => 59,  210 => 58,  205 => 57,  196 => 56,  182 => 52,  180 => 51,  174 => 49,  172 => 48,  167 => 47,  165 => 46,  160 => 45,  158 => 44,  153 => 43,  151 => 42,  146 => 41,  144 => 40,  141 => 39,  138 => 37,  136 => 33,  127 => 32,  116 => 29,  107 => 28,  96 => 25,  87 => 24,  76 => 21,  67 => 20,  56 => 17,  47 => 16,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Form:formadminfields.html.twig' %}


{# Custom Sonata Admin Extension #}
{% block sonataadminormonetoonewidget %}
    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetoone.html.twig' %}
{% endblock %}

{% block sonataadminormmanytomanywidget %}
    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormmanytomany.html.twig' %}
{% endblock %}

{% block sonataadminormmanytoonewidget %}
    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormmanytoone.html.twig' %}
{% endblock %}

{% block sonataadminormonetomanywidget %}
    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormonetomany.html.twig' %}
{% endblock %}

{% block sonatatypemodelwidget %}
    {#
        This is not the best way to do if
        TODO : improve this part
    #}

    {#model {{ sonataadmin.fielddescription.mappingtype }}#}

    {% if sonataadmin.fielddescription is empty %}
        {{ block('choicewidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::ONETOONE') %}
        {{ block('sonataadminormonetoonewidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::MANYTOONE') %}
        {{ block('sonataadminormmanytoonewidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::MANYTOMANY') %}
        {{ block('sonataadminormmanytomanywidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::ONETOMANY') %}
        {{ block('sonataadminormonetomanywidget') }}
    {% else %}
        {#INVALID MODE : {{ id }}#}
        {{ block('choicewidget') }}
    {% endif %}
{% endblock %}

{% block sonatatypemodellistwidget %}
    <div id=\"fieldcontainer{{ id }}\" class=\"field-container\">
        <span id=\"fieldwidget{{ id }}\" class=\"field-short-description\">
            {% if sonataadmin.fielddescription.associationadmin.id(sonataadmin.value) %}
                {{ render(path('sonataadminshortobjectinformation', {
                    'code':     sonataadmin.fielddescription.associationadmin.code,
                    'objectId': sonataadmin.fielddescription.associationadmin.id(sonataadmin.value),
                    'uniqid':   sonataadmin.fielddescription.associationadmin.uniqid,
                    'linkParameters': sonataadmin.fielddescription.options.linkparameters
                })) }}
            {% elseif sonataadmin.fielddescription.options.placeholder is defined and sonataadmin.fielddescription.options.placeholder %}
                <span class=\"inner-field-short-description\">
                    {{ sonataadmin.fielddescription.options.placeholder|trans({}, 'SonataAdminBundle') }}
                </span>
            {% endif %}
        </span>
        <span id=\"fieldactions{{ id }}\" class=\"field-actions\">
            <span class=\"btn-group\">
                {% if sonataadmin.fielddescription.associationadmin.hasroute('list') and sonataadmin.fielddescription.associationadmin.isGranted('LIST') and btnlist %}
                    <a  href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('list') }}\"
                        onclick=\"return startfielddialogformlist{{ id }}(this);\"
                        class=\"btn btn-info btn-sm sonata-ba-action\"
                        title=\"{{ btnlist|trans({}, btncatalogue) }}\"
                            >
                        <i class=\"fa fa-list\"></i>
                        {{ btnlist|trans({}, btncatalogue) }}
                    </a>
                {% endif %}

                {% if sonataadmin.fielddescription.associationadmin.hasroute('create') and sonataadmin.fielddescription.associationadmin.isGranted('CREATE') and btnadd %}
                    <a  href=\"{{ sonataadmin.fielddescription.associationadmin.generateUrl('create') }}\"
                        onclick=\"return startfielddialogformadd{{ id }}(this);\"
                        class=\"btn btn-success btn-sm sonata-ba-action\"
                        title=\"{{ btnadd|trans({}, btncatalogue) }}\"
                            >
                        <i class=\"fa fa-plus-circle\"></i>
                        {{ btnadd|trans({}, btncatalogue) }}
                    </a>
                {% endif %}
            </span>

            <span class=\"btn-group\">
                {% if sonataadmin.fielddescription.associationadmin.hasRoute('delete') and sonataadmin.fielddescription.associationadmin.isGranted('DELETE') and btndelete %}
                    <a  href=\"\"
                        onclick=\"return removeselectedelement{{ id }}(this);\"
                        class=\"btn btn-danger btn-sm sonata-ba-action\"
                        title=\"{{ btndelete|trans({}, btncatalogue) }}\"
                            >
                        <i class=\"fa fa-minus-circle\"></i>
                        {{ btndelete|trans({}, btncatalogue) }}
                    </a>
                {% endif %}
            </span>
        </span>

        <span style=\"display: none\" >
            {# Hidden text input cannot be required, because browser will throw error \"An invalid form control with name='' is not focusable\"  #}
            {{ formwidget(form, {'required':false}) }}
        </span>

        {{ block('sonatahelp') }}

        <div class=\"modal fade\" id=\"fielddialog{{ id }}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-lg\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                        <h4 class=\"modal-title\"></h4>
                    </div>
                    <div class=\"modal-body\">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {% include 'SonataDoctrineORMAdminBundle:CRUD:editormmanyassociationscript.html.twig' %}
{% endblock %}

{% block sonatatypeadminwidget %}
    {#admin {{ sonataadmin.fielddescription.mappingtype }}#}
    {% if sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::ONETOONE') %}
        {{ block('sonataadminormonetoonewidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::MANYTOONE') %}
        {{ block('sonataadminormmanytoonewidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::MANYTOMANY') %}
        {{ block('sonataadminormmanytomanywidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::ONETOMANY') %}
        {{ block('sonataadminormonetomanywidget') }}
    {% else %}
        INVALID MODE : {{ id }}
    {% endif %}
{% endblock %}

{% block sonatatypecollectionwidget %}
    {% if sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::ONETOMANY') %}
        {{ block('sonataadminormonetomanywidget') }}
    {% elseif sonataadmin.fielddescription.mappingtype == constant('Doctrine\\\\ORM\\\\Mapping\\\\ClassMetadataInfo::MANYTOMANY') %}
        {{ block('sonataadminormmanytomanywidget') }}
    {% else %}
        INVALID MODE : {{ id }} - type : sonatatypecollection - mapping : {{ sonataadmin.fielddescription.mappingtype }}
    {% endif %}
{% endblock %}
", "SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/Form/formadminfields.html.twig");
    }
}
