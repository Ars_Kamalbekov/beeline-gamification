<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class TwigTemplate7e53595704ac37a88e80a6b8183901f5fe6bb0cb1e6de2ea7c0dcd0678de6ef8 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'blockhead'),
            'menu' => array($this, 'blockmenu'),
            'panel' => array($this, 'blockpanel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal117fcfe73a4fd7046dd58bae4a24fe2b0041ef3f720e52a50a5ecaff9ddf318f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal117fcfe73a4fd7046dd58bae4a24fe2b0041ef3f720e52a50a5ecaff9ddf318f->enter($internal117fcfe73a4fd7046dd58bae4a24fe2b0041ef3f720e52a50a5ecaff9ddf318fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $internal26736ebda135912e2118e4517e92476cda455076321ccab100acfa63384a0bd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal26736ebda135912e2118e4517e92476cda455076321ccab100acfa63384a0bd6->enter($internal26736ebda135912e2118e4517e92476cda455076321ccab100acfa63384a0bd6prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal117fcfe73a4fd7046dd58bae4a24fe2b0041ef3f720e52a50a5ecaff9ddf318f->leave($internal117fcfe73a4fd7046dd58bae4a24fe2b0041ef3f720e52a50a5ecaff9ddf318fprof);

        
        $internal26736ebda135912e2118e4517e92476cda455076321ccab100acfa63384a0bd6->leave($internal26736ebda135912e2118e4517e92476cda455076321ccab100acfa63384a0bd6prof);

    }

    // line 3
    public function blockhead($context, array $blocks = array())
    {
        $internal5c00ed72c560a12850a2e747481eeb589ef18347ec71452e487351fc32355358 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal5c00ed72c560a12850a2e747481eeb589ef18347ec71452e487351fc32355358->enter($internal5c00ed72c560a12850a2e747481eeb589ef18347ec71452e487351fc32355358prof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        $internal959785bcae2466d37457fe4089e4f4438e6fb112758c93f246a6c7606d40fbce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal959785bcae2466d37457fe4089e4f4438e6fb112758c93f246a6c7606d40fbce->enter($internal959785bcae2466d37457fe4089e4f4438e6fb112758c93f246a6c7606d40fbceprof = new TwigProfilerProfile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilerexceptioncss", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $internal959785bcae2466d37457fe4089e4f4438e6fb112758c93f246a6c7606d40fbce->leave($internal959785bcae2466d37457fe4089e4f4438e6fb112758c93f246a6c7606d40fbceprof);

        
        $internal5c00ed72c560a12850a2e747481eeb589ef18347ec71452e487351fc32355358->leave($internal5c00ed72c560a12850a2e747481eeb589ef18347ec71452e487351fc32355358prof);

    }

    // line 12
    public function blockmenu($context, array $blocks = array())
    {
        $internala716a9ee58cd9d1bdf3962a9bc065f24f9d74957ba1d47046e06b778ea7c6ef8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internala716a9ee58cd9d1bdf3962a9bc065f24f9d74957ba1d47046e06b778ea7c6ef8->enter($internala716a9ee58cd9d1bdf3962a9bc065f24f9d74957ba1d47046e06b778ea7c6ef8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        $internald2294df6ba42c30b05365ba6197bb58227eed1aa89f54a03484f2905a41165e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald2294df6ba42c30b05365ba6197bb58227eed1aa89f54a03484f2905a41165e7->enter($internald2294df6ba42c30b05365ba6197bb58227eed1aa89f54a03484f2905a41165e7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twiginclude($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $internald2294df6ba42c30b05365ba6197bb58227eed1aa89f54a03484f2905a41165e7->leave($internald2294df6ba42c30b05365ba6197bb58227eed1aa89f54a03484f2905a41165e7prof);

        
        $internala716a9ee58cd9d1bdf3962a9bc065f24f9d74957ba1d47046e06b778ea7c6ef8->leave($internala716a9ee58cd9d1bdf3962a9bc065f24f9d74957ba1d47046e06b778ea7c6ef8prof);

    }

    // line 24
    public function blockpanel($context, array $blocks = array())
    {
        $internal62f605c23f81d5743cfb1875b4360403df038fbb7cbb681baedee8e2effec5f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal62f605c23f81d5743cfb1875b4360403df038fbb7cbb681baedee8e2effec5f8->enter($internal62f605c23f81d5743cfb1875b4360403df038fbb7cbb681baedee8e2effec5f8prof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        $internalc430c646db70be7225472bb86a853a43cc16d2036cb9f34d430dad096650f60a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc430c646db70be7225472bb86a853a43cc16d2036cb9f34d430dad096650f60a->enter($internalc430c646db70be7225472bb86a853a43cc16d2036cb9f34d430dad096650f60aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twiggetattribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || arraykeyexists("collector", $context) ? $context["collector"] : (function () { throw new TwigErrorRuntime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profilerexception", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $internalc430c646db70be7225472bb86a853a43cc16d2036cb9f34d430dad096650f60a->leave($internalc430c646db70be7225472bb86a853a43cc16d2036cb9f34d430dad096650f60aprof);

        
        $internal62f605c23f81d5743cfb1875b4360403df038fbb7cbb681baedee8e2effec5f8->leave($internal62f605c23f81d5743cfb1875b4360403df038fbb7cbb681baedee8e2effec5f8prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('profilerexceptioncss', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('profilerexception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "WebProfilerBundle:Collector:exception.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
