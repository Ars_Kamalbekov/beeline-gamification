<?php

/* WebProfilerBundle:Profiler:toolbaritem.html.twig */
class TwigTemplate461904d51d629d9904db585887d72fff7aab494f5d64fee13b41276b3655a7d6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaldd7281836bb36b900d1ea0827c0f8584a839aa30f3caefe16b4d05d638816572 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaldd7281836bb36b900d1ea0827c0f8584a839aa30f3caefe16b4d05d638816572->enter($internaldd7281836bb36b900d1ea0827c0f8584a839aa30f3caefe16b4d05d638816572prof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbaritem.html.twig"));

        $internal0597cb2e60af58cbe3f016c9cf1968d492052a0c45610f6b2328938a1ba49bcb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal0597cb2e60af58cbe3f016c9cf1968d492052a0c45610f6b2328938a1ba49bcb->enter($internal0597cb2e60af58cbe3f016c9cf1968d492052a0c45610f6b2328938a1ba49bcbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbaritem.html.twig"));

        // line 1
        echo "<div class=\"sf-toolbar-block sf-toolbar-block-";
        echo twigescapefilter($this->env, (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo " sf-toolbar-status-";
        echo twigescapefilter($this->env, ((arraykeyexists("status", $context)) ? (twigdefaultfilter((isset($context["status"]) || arraykeyexists("status", $context) ? $context["status"] : (function () { throw new TwigErrorRuntime('Variable "status" does not exist.', 1, $this->getSourceContext()); })()), "normal")) : ("normal")), "html", null, true);
        echo " ";
        echo twigescapefilter($this->env, ((arraykeyexists("additionalclasses", $context)) ? (twigdefaultfilter((isset($context["additionalclasses"]) || arraykeyexists("additionalclasses", $context) ? $context["additionalclasses"] : (function () { throw new TwigErrorRuntime('Variable "additionalclasses" does not exist.', 1, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
        echo "\" ";
        echo ((arraykeyexists("blockattrs", $context)) ? (twigdefaultfilter((isset($context["blockattrs"]) || arraykeyexists("blockattrs", $context) ? $context["blockattrs"] : (function () { throw new TwigErrorRuntime('Variable "blockattrs" does not exist.', 1, $this->getSourceContext()); })()), "")) : (""));
        echo ">
    ";
        // line 2
        if (( !arraykeyexists("link", $context) || (isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 2, $this->getSourceContext()); })()))) {
            echo "<a href=\"";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("profiler", array("token" => (isset($context["token"]) || arraykeyexists("token", $context) ? $context["token"] : (function () { throw new TwigErrorRuntime('Variable "token" does not exist.', 2, $this->getSourceContext()); })()), "panel" => (isset($context["name"]) || arraykeyexists("name", $context) ? $context["name"] : (function () { throw new TwigErrorRuntime('Variable "name" does not exist.', 2, $this->getSourceContext()); })()))), "html", null, true);
            echo "\">";
        }
        // line 3
        echo "        <div class=\"sf-toolbar-icon\">";
        echo twigescapefilter($this->env, ((arraykeyexists("icon", $context)) ? (twigdefaultfilter((isset($context["icon"]) || arraykeyexists("icon", $context) ? $context["icon"] : (function () { throw new TwigErrorRuntime('Variable "icon" does not exist.', 3, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
        echo "</div>
    ";
        // line 4
        if (((arraykeyexists("link", $context)) ? (twigdefaultfilter((isset($context["link"]) || arraykeyexists("link", $context) ? $context["link"] : (function () { throw new TwigErrorRuntime('Variable "link" does not exist.', 4, $this->getSourceContext()); })()), false)) : (false))) {
            echo "</a>";
        }
        // line 5
        echo "        <div class=\"sf-toolbar-info\">";
        echo twigescapefilter($this->env, ((arraykeyexists("text", $context)) ? (twigdefaultfilter((isset($context["text"]) || arraykeyexists("text", $context) ? $context["text"] : (function () { throw new TwigErrorRuntime('Variable "text" does not exist.', 5, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
        echo "</div>
</div>
";
        
        $internaldd7281836bb36b900d1ea0827c0f8584a839aa30f3caefe16b4d05d638816572->leave($internaldd7281836bb36b900d1ea0827c0f8584a839aa30f3caefe16b4d05d638816572prof);

        
        $internal0597cb2e60af58cbe3f016c9cf1968d492052a0c45610f6b2328938a1ba49bcb->leave($internal0597cb2e60af58cbe3f016c9cf1968d492052a0c45610f6b2328938a1ba49bcbprof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbaritem.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 5,  47 => 4,  42 => 3,  36 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<div class=\"sf-toolbar-block sf-toolbar-block-{{ name }} sf-toolbar-status-{{ status|default('normal') }} {{ additionalclasses|default('') }}\" {{ blockattrs|default('')|raw }}>
    {% if link is not defined or link %}<a href=\"{{ path('profiler', { token: token, panel: name }) }}\">{% endif %}
        <div class=\"sf-toolbar-icon\">{{ icon|default('') }}</div>
    {% if link|default(false) %}</a>{% endif %}
        <div class=\"sf-toolbar-info\">{{ text|default('') }}</div>
</div>
", "WebProfilerBundle:Profiler:toolbaritem.html.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbaritem.html.twig");
    }
}
