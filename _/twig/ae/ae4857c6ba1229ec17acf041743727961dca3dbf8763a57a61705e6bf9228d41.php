<?php

/* @Twig/images/icon-plus-square-o.svg */
class TwigTemplate5c33986bd31fba695ad20603e25b19914905d2ca91536a7520d692fd9a6efaca extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal3e14c7dc7a01e8a9ebb21e66cf1aa593745f6adc57a5cdbe76c28d127569d492 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal3e14c7dc7a01e8a9ebb21e66cf1aa593745f6adc57a5cdbe76c28d127569d492->enter($internal3e14c7dc7a01e8a9ebb21e66cf1aa593745f6adc57a5cdbe76c28d127569d492prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        $internalac17d90abb3ca63eccb93ac0ae99e4396e05a3a9fc8dbcc8ef7cf0b3b82d963b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalac17d90abb3ca63eccb93ac0ae99e4396e05a3a9fc8dbcc8ef7cf0b3b82d963b->enter($internalac17d90abb3ca63eccb93ac0ae99e4396e05a3a9fc8dbcc8ef7cf0b3b82d963bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-plus-square-o.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
";
        
        $internal3e14c7dc7a01e8a9ebb21e66cf1aa593745f6adc57a5cdbe76c28d127569d492->leave($internal3e14c7dc7a01e8a9ebb21e66cf1aa593745f6adc57a5cdbe76c28d127569d492prof);

        
        $internalac17d90abb3ca63eccb93ac0ae99e4396e05a3a9fc8dbcc8ef7cf0b3b82d963b->leave($internalac17d90abb3ca63eccb93ac0ae99e4396e05a3a9fc8dbcc8ef7cf0b3b82d963bprof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-plus-square-o.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1344 800v64q0 14-9 23t-23 9H960v352q0 14-9 23t-23 9h-64q-14 0-23-9t-9-23V896H480q-14 0-23-9t-9-23v-64q0-14 9-23t23-9h352V416q0-14 9-23t23-9h64q14 0 23 9t9 23v352h352q14 0 23 9t9 23zm128 448V416q0-66-47-113t-113-47H480q-66 0-113 47t-47 113v832q0 66 47 113t113 47h832q66 0 113-47t47-113zm128-832v832q0 119-84.5 203.5T1312 1536H480q-119 0-203.5-84.5T192 1248V416q0-119 84.5-203.5T480 128h832q119 0 203.5 84.5T1600 416z\"/></svg>
", "@Twig/images/icon-plus-square-o.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-plus-square-o.svg");
    }
}
