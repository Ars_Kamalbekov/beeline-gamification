<?php

/* SonataBlockBundle:Block:blocktemplate.html.twig */
class TwigTemplatee7d353289b37214420b18d87d11d74ccdce53a0638cf1981864186d3a42038fd extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blocktemplate.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal9feccbb4111e27d75bcb2744b05a82dcc5037eae7c3e13b64ce366612599349e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9feccbb4111e27d75bcb2744b05a82dcc5037eae7c3e13b64ce366612599349e->enter($internal9feccbb4111e27d75bcb2744b05a82dcc5037eae7c3e13b64ce366612599349eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blocktemplate.html.twig"));

        $internalc5ef009cfdc8be7677639785ccb0b771ccab3c32b423166cdcf0b66c8ea6a3db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalc5ef009cfdc8be7677639785ccb0b771ccab3c32b423166cdcf0b66c8ea6a3db->enter($internalc5ef009cfdc8be7677639785ccb0b771ccab3c32b423166cdcf0b66c8ea6a3dbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blocktemplate.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal9feccbb4111e27d75bcb2744b05a82dcc5037eae7c3e13b64ce366612599349e->leave($internal9feccbb4111e27d75bcb2744b05a82dcc5037eae7c3e13b64ce366612599349eprof);

        
        $internalc5ef009cfdc8be7677639785ccb0b771ccab3c32b423166cdcf0b66c8ea6a3db->leave($internalc5ef009cfdc8be7677639785ccb0b771ccab3c32b423166cdcf0b66c8ea6a3dbprof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internal88915ee164c318c5c1c733a46e816db1febb2eca2ccc736a95c46cbd7aee8dc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal88915ee164c318c5c1c733a46e816db1febb2eca2ccc736a95c46cbd7aee8dc2->enter($internal88915ee164c318c5c1c733a46e816db1febb2eca2ccc736a95c46cbd7aee8dc2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal9751dac08afb70fb46ae0132ffb82a8072d5fb5b3e89548e84c00e82ceb5e4d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9751dac08afb70fb46ae0132ffb82a8072d5fb5b3e89548e84c00e82ceb5e4d2->enter($internal9751dac08afb70fb46ae0132ffb82a8072d5fb5b3e89548e84c00e82ceb5e4d2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>";
        // line 33
        echo "{# file: 'MyBundle:Block:myblockfeature1.html.twig' #}
{% extends sonatablock.templates.blockbase %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}";
        echo "</pre>

    And then call it from a template with the <code>sonatablockrender</code> helper:

    <pre>";
        // line 43
        echo "{{ sonatablockrender({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:myblockfeature1.html.twig',
}) }}";
        echo "</pre>
";
        
        $internal9751dac08afb70fb46ae0132ffb82a8072d5fb5b3e89548e84c00e82ceb5e4d2->leave($internal9751dac08afb70fb46ae0132ffb82a8072d5fb5b3e89548e84c00e82ceb5e4d2prof);

        
        $internal88915ee164c318c5c1c733a46e816db1febb2eca2ccc736a95c46cbd7aee8dc2->leave($internal88915ee164c318c5c1c733a46e816db1febb2eca2ccc736a95c46cbd7aee8dc2prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blocktemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 43,  53 => 33,  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>
        {%- verbatim -%}
{# file: 'MyBundle:Block:myblockfeature1.html.twig' #}
{% extends sonatablock.templates.blockbase %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}
        {%- endverbatim -%}
    </pre>

    And then call it from a template with the <code>sonatablockrender</code> helper:

    <pre>
{%- verbatim -%}
{{ sonatablockrender({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:myblockfeature1.html.twig',
}) }}
{%- endverbatim -%}
    </pre>
{% endblock %}
", "SonataBlockBundle:Block:blocktemplate.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blocktemplate.html.twig");
    }
}
