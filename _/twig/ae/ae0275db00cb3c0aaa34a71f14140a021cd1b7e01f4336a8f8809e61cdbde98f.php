<?php

/* SonataAdminBundle:CRUD:basehistory.html.twig */
class TwigTemplate48b86b5b613527d28039faa9592db90c3f7bcab89f6bd7c17ca067449e45b4d5 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'actions' => array($this, 'blockactions'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:basehistory.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal25c78c4db840fdbfe2a1c5a6238c632b87e27ef4e4148323a0bfcca73407288b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal25c78c4db840fdbfe2a1c5a6238c632b87e27ef4e4148323a0bfcca73407288b->enter($internal25c78c4db840fdbfe2a1c5a6238c632b87e27ef4e4148323a0bfcca73407288bprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basehistory.html.twig"));

        $internal3c4f67f139fda8eacfe41b46e28830dde5fb83d6e54dd1830a49e2eb1463bc63 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal3c4f67f139fda8eacfe41b46e28830dde5fb83d6e54dd1830a49e2eb1463bc63->enter($internal3c4f67f139fda8eacfe41b46e28830dde5fb83d6e54dd1830a49e2eb1463bc63prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:basehistory.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal25c78c4db840fdbfe2a1c5a6238c632b87e27ef4e4148323a0bfcca73407288b->leave($internal25c78c4db840fdbfe2a1c5a6238c632b87e27ef4e4148323a0bfcca73407288bprof);

        
        $internal3c4f67f139fda8eacfe41b46e28830dde5fb83d6e54dd1830a49e2eb1463bc63->leave($internal3c4f67f139fda8eacfe41b46e28830dde5fb83d6e54dd1830a49e2eb1463bc63prof);

    }

    // line 14
    public function blockactions($context, array $blocks = array())
    {
        $internalf3b84c6d72323066766c76490d83aab07d1deecd19ec9c2182532d4a7bdc8fce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf3b84c6d72323066766c76490d83aab07d1deecd19ec9c2182532d4a7bdc8fce->enter($internalf3b84c6d72323066766c76490d83aab07d1deecd19ec9c2182532d4a7bdc8fceprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        $internal6b8e8e261ffdab5bfe8e52133141c8b64245b25d534de0c7f4ca8dfd2b695f8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6b8e8e261ffdab5bfe8e52133141c8b64245b25d534de0c7f4ca8dfd2b695f8a->enter($internal6b8e8e261ffdab5bfe8e52133141c8b64245b25d534de0c7f4ca8dfd2b695f8aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:actionbuttons.html.twig", "SonataAdminBundle:CRUD:basehistory.html.twig", 15)->display($context);
        
        $internal6b8e8e261ffdab5bfe8e52133141c8b64245b25d534de0c7f4ca8dfd2b695f8a->leave($internal6b8e8e261ffdab5bfe8e52133141c8b64245b25d534de0c7f4ca8dfd2b695f8aprof);

        
        $internalf3b84c6d72323066766c76490d83aab07d1deecd19ec9c2182532d4a7bdc8fce->leave($internalf3b84c6d72323066766c76490d83aab07d1deecd19ec9c2182532d4a7bdc8fceprof);

    }

    // line 18
    public function blockcontent($context, array $blocks = array())
    {
        $internal804fdbe264be343e5f3c4cd1ee74d8139148efcfd3fb6e12fbe3563e0e727aa3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal804fdbe264be343e5f3c4cd1ee74d8139148efcfd3fb6e12fbe3563e0e727aa3->enter($internal804fdbe264be343e5f3c4cd1ee74d8139148efcfd3fb6e12fbe3563e0e727aa3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internalf533dd9d2ade547fed720090afce82c6a076e71a75bf1e3e28b64c97178da1d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf533dd9d2ade547fed720090afce82c6a076e71a75bf1e3e28b64c97178da1d1->enter($internalf533dd9d2ade547fed720090afce82c6a076e71a75bf1e3e28b64c97178da1d1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 19
        echo "
    <div class=\"row\">
        <div class=\"col-md-5\">
            <div class=\"box box-primary\">
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\" id=\"revisions\">
                        <thead>
                        <tr>
                            <th>";
        // line 27
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tdrevision", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                            <th>";
        // line 28
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tdtimestamp", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                            <th>";
        // line 29
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tdusername", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                            <th>";
        // line 30
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tdaction", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                            <th>";
        // line 31
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("tdcompare", array(), "SonataAdminBundle"), "html", null, true);
        echo "</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
        // line 35
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable((isset($context["revisions"]) || arraykeyexists("revisions", $context) ? $context["revisions"] : (function () { throw new TwigErrorRuntime('Variable "revisions" does not exist.', 35, $this->getSourceContext()); })()));
        $context['loop'] = array(
          'parent' => $context['parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (isarray($context['seq']) || (isobject($context['seq']) && $context['seq'] instanceof Countable)) {
            $length = count($context['seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['seq'] as $context["key"] => $context["revision"]) {
            // line 36
            echo "                            <tr class=\"";
            if ((((isset($context["currentRevision"]) || arraykeyexists("currentRevision", $context) ? $context["currentRevision"] : (function () { throw new TwigErrorRuntime('Variable "currentRevision" does not exist.', 36, $this->getSourceContext()); })()) != false) && (twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()) == twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentRevision"]) || arraykeyexists("currentRevision", $context) ? $context["currentRevision"] : (function () { throw new TwigErrorRuntime('Variable "currentRevision" does not exist.', 36, $this->getSourceContext()); })()), "rev", array())))) {
                echo "current-revision";
            }
            echo "\">
                                <td>";
            // line 37
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()), "html", null, true);
            echo "</td>
                                <td>";
            // line 38
            $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 38, $this->getSourceContext()); })()), "getTemplate", array(0 => "historyrevisiontimestamp"), "method"), "SonataAdminBundle:CRUD:basehistory.html.twig", 38)->display($context);
            echo "</td>
                                <td>";
            // line 39
            echo twigescapefilter($this->env, ((twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "username", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labelunknownuser", array(), "SonataAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labelunknownuser", array(), "SonataAdminBundle"))), "html", null, true);
            echo "</td>
                                <td><a href=\"";
            // line 40
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 40, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "historyviewrevision", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 40, $this->getSourceContext()); })()), 2 => array("revision" => twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()))), "method"), "html", null, true);
            echo "\" class=\"revision-link\" rel=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()), "html", null, true);
            echo "\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labelviewrevision", array(), "SonataAdminBundle"), "html", null, true);
            echo "</a></td>
                                <td>
                                    ";
            // line 42
            if ((((isset($context["currentRevision"]) || arraykeyexists("currentRevision", $context) ? $context["currentRevision"] : (function () { throw new TwigErrorRuntime('Variable "currentRevision" does not exist.', 42, $this->getSourceContext()); })()) == false) || (twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()) == twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentRevision"]) || arraykeyexists("currentRevision", $context) ? $context["currentRevision"] : (function () { throw new TwigErrorRuntime('Variable "currentRevision" does not exist.', 42, $this->getSourceContext()); })()), "rev", array())))) {
                // line 43
                echo "                                        /
                                    ";
            } else {
                // line 45
                echo "                                        <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 45, $this->getSourceContext()); })()), "generateObjectUrl", array(0 => "historycomparerevisions", 1 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 45, $this->getSourceContext()); })()), 2 => array("baserevision" => twiggetattribute($this->env, $this->getSourceContext(), (isset($context["currentRevision"]) || arraykeyexists("currentRevision", $context) ? $context["currentRevision"] : (function () { throw new TwigErrorRuntime('Variable "currentRevision" does not exist.', 45, $this->getSourceContext()); })()), "rev", array()), "comparerevision" => twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()))), "method"), "html", null, true);
                echo "\" class=\"revision-compare-link\" rel=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), $context["revision"], "rev", array()), "html", null, true);
                echo "\">";
                echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("labelcomparerevision", array(), "SonataAdminBundle"), "html", null, true);
                echo "</a>
                                    ";
            }
            // line 47
            echo "                                </td>
                            </tr>
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['revision'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 50
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id=\"revision-detail\" class=\"col-md-7 revision-detail\">

        </div>
    </div>

    <script>
        jQuery(document).ready(function() {

            jQuery('a.revision-link, a.revision-compare-link').bind('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                action = jQuery(this).hasClass('revision-link')
                    ? 'show'
                    : 'compare';

                jQuery('#revision-detail').html('');

                if(action == 'show'){
                    jQuery('table#revisions tbody tr').removeClass('current');
                    jQuery(this).parent('').removeClass('current');
                }

                jQuery.ajax({
                    url: jQuery(this).attr('href'),
                    dataType: 'html',
                    success: function(data) {
                        jQuery('#revision-detail').html(data);
                    }
                });

                return false;
            });

        });
    </script>
";
        
        $internalf533dd9d2ade547fed720090afce82c6a076e71a75bf1e3e28b64c97178da1d1->leave($internalf533dd9d2ade547fed720090afce82c6a076e71a75bf1e3e28b64c97178da1d1prof);

        
        $internal804fdbe264be343e5f3c4cd1ee74d8139148efcfd3fb6e12fbe3563e0e727aa3->leave($internal804fdbe264be343e5f3c4cd1ee74d8139148efcfd3fb6e12fbe3563e0e727aa3prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:basehistory.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 50,  162 => 47,  152 => 45,  148 => 43,  146 => 42,  137 => 40,  133 => 39,  129 => 38,  125 => 37,  118 => 36,  101 => 35,  94 => 31,  90 => 30,  86 => 29,  82 => 28,  78 => 27,  68 => 19,  59 => 18,  49 => 15,  40 => 14,  19 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:actionbuttons.html.twig' %}
{%- endblock -%}

{% block content %}

    <div class=\"row\">
        <div class=\"col-md-5\">
            <div class=\"box box-primary\">
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\" id=\"revisions\">
                        <thead>
                        <tr>
                            <th>{{ 'tdrevision'|trans({}, 'SonataAdminBundle') }}</th>
                            <th>{{ 'tdtimestamp'|trans({}, 'SonataAdminBundle') }}</th>
                            <th>{{ 'tdusername'|trans({}, 'SonataAdminBundle') }}</th>
                            <th>{{ 'tdaction'|trans({}, 'SonataAdminBundle') }}</th>
                            <th>{{ 'tdcompare'|trans({}, 'SonataAdminBundle') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for revision in revisions %}
                            <tr class=\"{% if (currentRevision != false and revision.rev == currentRevision.rev) %}current-revision{% endif %}\">
                                <td>{{ revision.rev }}</td>
                                <td>{% include admin.getTemplate('historyrevisiontimestamp') %}</td>
                                <td>{{ revision.username|default('labelunknownuser'|trans({}, 'SonataAdminBundle')) }}</td>
                                <td><a href=\"{{ admin.generateObjectUrl('historyviewrevision', object, {'revision': revision.rev }) }}\" class=\"revision-link\" rel=\"{{ revision.rev }}\">{{ \"labelviewrevision\"|trans({}, 'SonataAdminBundle') }}</a></td>
                                <td>
                                    {% if (currentRevision == false or revision.rev == currentRevision.rev) %}
                                        /
                                    {% else %}
                                        <a href=\"{{ admin.generateObjectUrl('historycomparerevisions', object, {'baserevision': currentRevision.rev, 'comparerevision': revision.rev }) }}\" class=\"revision-compare-link\" rel=\"{{ revision.rev }}\">{{ 'labelcomparerevision'|trans({}, 'SonataAdminBundle') }}</a>
                                    {% endif %}
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id=\"revision-detail\" class=\"col-md-7 revision-detail\">

        </div>
    </div>

    <script>
        jQuery(document).ready(function() {

            jQuery('a.revision-link, a.revision-compare-link').bind('click', function(event) {
                event.stopPropagation();
                event.preventDefault();

                action = jQuery(this).hasClass('revision-link')
                    ? 'show'
                    : 'compare';

                jQuery('#revision-detail').html('');

                if(action == 'show'){
                    jQuery('table#revisions tbody tr').removeClass('current');
                    jQuery(this).parent('').removeClass('current');
                }

                jQuery.ajax({
                    url: jQuery(this).attr('href'),
                    dataType: 'html',
                    success: function(data) {
                        jQuery('#revision-detail').html(data);
                    }
                });

                return false;
            });

        });
    </script>
{% endblock %}
", "SonataAdminBundle:CRUD:basehistory.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/basehistory.html.twig");
    }
}
