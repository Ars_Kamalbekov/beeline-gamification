<?php

/* SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig */
class TwigTemplate61b3a4de98e1b09a4a282de4819a2e94ab9fd0d34f61448569f3d0e54ec97442 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal061717e7f1bc8ba0c9b3c7c45b4aff132450db92bf281a6409900d5d53e7a218 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal061717e7f1bc8ba0c9b3c7c45b4aff132450db92bf281a6409900d5d53e7a218->enter($internal061717e7f1bc8ba0c9b3c7c45b4aff132450db92bf281a6409900d5d53e7a218prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig"));

        $internal5f4bca8c8cd03cb1725e218bbbefa0960f9db32605d950e7ab226b45673a613a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5f4bca8c8cd03cb1725e218bbbefa0960f9db32605d950e7ab226b45673a613a->enter($internal5f4bca8c8cd03cb1725e218bbbefa0960f9db32605d950e7ab226b45673a613aprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal061717e7f1bc8ba0c9b3c7c45b4aff132450db92bf281a6409900d5d53e7a218->leave($internal061717e7f1bc8ba0c9b3c7c45b4aff132450db92bf281a6409900d5d53e7a218prof);

        
        $internal5f4bca8c8cd03cb1725e218bbbefa0960f9db32605d950e7ab226b45673a613a->leave($internal5f4bca8c8cd03cb1725e218bbbefa0960f9db32605d950e7ab226b45673a613aprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalcabb5e2a25187a34b378b9738e00b1f6e999269138c60955772290bd507783eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcabb5e2a25187a34b378b9738e00b1f6e999269138c60955772290bd507783eb->enter($internalcabb5e2a25187a34b378b9738e00b1f6e999269138c60955772290bd507783ebprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal9a40f82d2a5341782b99a90e8acf0d88c1b0b7c156458531b7aa4b696a8f8ebb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal9a40f82d2a5341782b99a90e8acf0d88c1b0b7c156458531b7aa4b696a8f8ebb->enter($internal9a40f82d2a5341782b99a90e8acf0d88c1b0b7c156458531b7aa4b696a8f8ebbprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })())) {
            // line 16
            echo "        ";
            $context["routename"] = twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 16, $this->getSourceContext()); })()), "options", array()), "route", array()), "name", array());
            // line 17
            echo "        ";
            if (((twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 17, $this->getSourceContext()); })()), "hasAssociationAdmin", array()) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 18
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 18, $this->getSourceContext()); })()), "associationadmin", array()), "hasRoute", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 18, $this->getSourceContext()); })())), "method")) && twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(),             // line 19
(isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 19, $this->getSourceContext()); })()), "associationadmin", array()), "hasAccess", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 19, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 19, $this->getSourceContext()); })())), "method"))) {
                // line 20
                echo "            <a href=\"";
                echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["routename"]) || arraykeyexists("routename", $context) ? $context["routename"] : (function () { throw new TwigErrorRuntime('Variable "routename" does not exist.', 20, $this->getSourceContext()); })()), 1 => (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 20, $this->getSourceContext()); })()), 2 => twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 20, $this->getSourceContext()); })()), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
                echo "\">
                ";
                // line 21
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 21, $this->getSourceContext()); })())), "html", null, true);
                echo "
            </a>
        ";
            } else {
                // line 24
                echo "            ";
                echo twigescapefilter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 24, $this->getSourceContext()); })()), (isset($context["fielddescription"]) || arraykeyexists("fielddescription", $context) ? $context["fielddescription"] : (function () { throw new TwigErrorRuntime('Variable "fielddescription" does not exist.', 24, $this->getSourceContext()); })())), "html", null, true);
                echo "
        ";
            }
            // line 26
            echo "    ";
        }
        
        $internal9a40f82d2a5341782b99a90e8acf0d88c1b0b7c156458531b7aa4b696a8f8ebb->leave($internal9a40f82d2a5341782b99a90e8acf0d88c1b0b7c156458531b7aa4b696a8f8ebbprof);

        
        $internalcabb5e2a25187a34b378b9738e00b1f6e999269138c60955772290bd507783eb->leave($internalcabb5e2a25187a34b378b9738e00b1f6e999269138c60955772290bd507783ebprof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 26,  71 => 24,  65 => 21,  60 => 20,  58 => 19,  57 => 18,  55 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {% if value %}
        {% set routename = fielddescription.options.route.name %}
        {% if fielddescription.hasAssociationAdmin
        and fielddescription.associationadmin.hasRoute(routename)
        and fielddescription.associationadmin.hasAccess(routename, value) %}
            <a href=\"{{ fielddescription.associationadmin.generateObjectUrl(routename, value, fielddescription.options.route.parameters) }}\">
                {{ value|renderrelationelement(fielddescription) }}
            </a>
        {% else %}
            {{ value|renderrelationelement(fielddescription) }}
        {% endif %}
    {% endif %}
{% endblock %}
", "SonataDoctrineORMAdminBundle:CRUD:showormmanytoone.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views/CRUD/showormmanytoone.html.twig");
    }
}
