<?php

/* @Framework/Form/formrows.html.php */
class TwigTemplate7d99e9b098d78202d9208f772449220de6e4386e9c76a0b7832b7de3425406ca extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalf2e05f2b750737273805a28e00ece9945d11535feb8413fc21a43d01c289c65f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalf2e05f2b750737273805a28e00ece9945d11535feb8413fc21a43d01c289c65f->enter($internalf2e05f2b750737273805a28e00ece9945d11535feb8413fc21a43d01c289c65fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrows.html.php"));

        $internaldc6e79c8cb564ee3549d8894ae62a51e2b3ed26a6a6c0ca7fbe3356c4a02ae47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internaldc6e79c8cb564ee3549d8894ae62a51e2b3ed26a6a6c0ca7fbe3356c4a02ae47->enter($internaldc6e79c8cb564ee3549d8894ae62a51e2b3ed26a6a6c0ca7fbe3356c4a02ae47prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Framework/Form/formrows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $internalf2e05f2b750737273805a28e00ece9945d11535feb8413fc21a43d01c289c65f->leave($internalf2e05f2b750737273805a28e00ece9945d11535feb8413fc21a43d01c289c65fprof);

        
        $internaldc6e79c8cb564ee3549d8894ae62a51e2b3ed26a6a6c0ca7fbe3356c4a02ae47->leave($internaldc6e79c8cb564ee3549d8894ae62a51e2b3ed26a6a6c0ca7fbe3356c4a02ae47prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/formrows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/formrows.html.php", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/formrows.html.php");
    }
}
