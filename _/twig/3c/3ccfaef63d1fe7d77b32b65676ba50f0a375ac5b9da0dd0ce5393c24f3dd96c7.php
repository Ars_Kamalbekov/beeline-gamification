<?php

/* SonataAdminBundle:Core:search.html.twig */
class TwigTemplate0d09c059cec63cb2533753923e245a8fd44695c702d5e36c0def7797c6a9f819 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'breadcrumb' => array($this, 'blockbreadcrumb'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 12, $this->getSourceContext()); })()), "SonataAdminBundle:Core:search.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalb1affa0568e533c31f53516623dfa5773e2c28aec8b43e312027236b720a9e16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb1affa0568e533c31f53516623dfa5773e2c28aec8b43e312027236b720a9e16->enter($internalb1affa0568e533c31f53516623dfa5773e2c28aec8b43e312027236b720a9e16prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:search.html.twig"));

        $internalf44ca2827dee6e6728241d5c41ccfa7f021a81183a45e5322b1c3b00233c2431 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalf44ca2827dee6e6728241d5c41ccfa7f021a81183a45e5322b1c3b00233c2431->enter($internalf44ca2827dee6e6728241d5c41ccfa7f021a81183a45e5322b1c3b00233c2431prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:Core:search.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalb1affa0568e533c31f53516623dfa5773e2c28aec8b43e312027236b720a9e16->leave($internalb1affa0568e533c31f53516623dfa5773e2c28aec8b43e312027236b720a9e16prof);

        
        $internalf44ca2827dee6e6728241d5c41ccfa7f021a81183a45e5322b1c3b00233c2431->leave($internalf44ca2827dee6e6728241d5c41ccfa7f021a81183a45e5322b1c3b00233c2431prof);

    }

    // line 14
    public function blocktitle($context, array $blocks = array())
    {
        $internal7f3c01f51bf4e30722e204c17d41da5b02508bfbdd8957b9ee6c4228b0a6c848 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal7f3c01f51bf4e30722e204c17d41da5b02508bfbdd8957b9ee6c4228b0a6c848->enter($internal7f3c01f51bf4e30722e204c17d41da5b02508bfbdd8957b9ee6c4228b0a6c848prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal25d6f8a350a8d64194304e4db478abdb88725c80477084e1d85552eea40b47a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal25d6f8a350a8d64194304e4db478abdb88725c80477084e1d85552eea40b47a5->enter($internal25d6f8a350a8d64194304e4db478abdb88725c80477084e1d85552eea40b47a5prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titlesearchresults", array("%query%" => (isset($context["query"]) || arraykeyexists("query", $context) ? $context["query"] : (function () { throw new TwigErrorRuntime('Variable "query" does not exist.', 14, $this->getSourceContext()); })())), "SonataAdminBundle"), "html", null, true);
        
        $internal25d6f8a350a8d64194304e4db478abdb88725c80477084e1d85552eea40b47a5->leave($internal25d6f8a350a8d64194304e4db478abdb88725c80477084e1d85552eea40b47a5prof);

        
        $internal7f3c01f51bf4e30722e204c17d41da5b02508bfbdd8957b9ee6c4228b0a6c848->leave($internal7f3c01f51bf4e30722e204c17d41da5b02508bfbdd8957b9ee6c4228b0a6c848prof);

    }

    // line 15
    public function blockbreadcrumb($context, array $blocks = array())
    {
        $internald8e7f2912491d58410ae5885e705e6a2fa26a6f6fc01dfa9b5c71efa9b4c65f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald8e7f2912491d58410ae5885e705e6a2fa26a6f6fc01dfa9b5c71efa9b4c65f7->enter($internald8e7f2912491d58410ae5885e705e6a2fa26a6f6fc01dfa9b5c71efa9b4c65f7prof = new TwigProfilerProfile($this->getTemplateName(), "block", "breadcrumb"));

        $internala87d5d7e29ceabccc3fabb397884814efcec309ffdba2ad6dbe0becb4d3fe0e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internala87d5d7e29ceabccc3fabb397884814efcec309ffdba2ad6dbe0becb4d3fe0e2->enter($internala87d5d7e29ceabccc3fabb397884814efcec309ffdba2ad6dbe0becb4d3fe0e2prof = new TwigProfilerProfile($this->getTemplateName(), "block", "breadcrumb"));

        
        $internala87d5d7e29ceabccc3fabb397884814efcec309ffdba2ad6dbe0becb4d3fe0e2->leave($internala87d5d7e29ceabccc3fabb397884814efcec309ffdba2ad6dbe0becb4d3fe0e2prof);

        
        $internald8e7f2912491d58410ae5885e705e6a2fa26a6f6fc01dfa9b5c71efa9b4c65f7->leave($internald8e7f2912491d58410ae5885e705e6a2fa26a6f6fc01dfa9b5c71efa9b4c65f7prof);

    }

    // line 16
    public function blockcontent($context, array $blocks = array())
    {
        $internal9d978ed86ea0b2b636aaddf97272e8607d4b2978a08ee51c63bf2e68ad98eeff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal9d978ed86ea0b2b636aaddf97272e8607d4b2978a08ee51c63bf2e68ad98eeff->enter($internal9d978ed86ea0b2b636aaddf97272e8607d4b2978a08ee51c63bf2e68ad98eeffprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internal7ec28971087647ca45a7abeda3d7fd7cd813976270743fc0cee4c7c232f8e130 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal7ec28971087647ca45a7abeda3d7fd7cd813976270743fc0cee4c7c232f8e130->enter($internal7ec28971087647ca45a7abeda3d7fd7cd813976270743fc0cee4c7c232f8e130prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 17
        echo "    <h2 class=\"page-header\">";
        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titlesearchresults", array("%query%" => (isset($context["query"]) || arraykeyexists("query", $context) ? $context["query"] : (function () { throw new TwigErrorRuntime('Variable "query" does not exist.', 17, $this->getSourceContext()); })())), "SonataAdminBundle"), "html", null, true);
        echo "</h2>

    ";
        // line 19
        if ((arraykeyexists("query", $context) &&  !((isset($context["query"]) || arraykeyexists("query", $context) ? $context["query"] : (function () { throw new TwigErrorRuntime('Variable "query" does not exist.', 19, $this->getSourceContext()); })()) === false))) {
            // line 20
            echo "        ";
            $context["count"] = 0;
            // line 21
            echo "        <div class=\"row\" data-masonry='{ \"itemSelector\": \".search-box-item\" }'>
        ";
            // line 22
            $context['parent'] = $context;
            $context['seq'] = twigensuretraversable((isset($context["groups"]) || arraykeyexists("groups", $context) ? $context["groups"] : (function () { throw new TwigErrorRuntime('Variable "groups" does not exist.', 22, $this->getSourceContext()); })()));
            foreach ($context['seq'] as $context["key"] => $context["group"]) {
                // line 23
                echo "            ";
                $context["display"] = (twigtestempty(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array())) || $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLESUPERADMIN"));
                // line 24
                echo "            ";
                $context['parent'] = $context;
                $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "roles", array()));
                foreach ($context['seq'] as $context["key"] => $context["role"]) {
                    if ( !(isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 24, $this->getSourceContext()); })())) {
                        // line 25
                        echo "                ";
                        $context["display"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted($context["role"]);
                        // line 26
                        echo "            ";
                    }
                }
                $parent = $context['parent'];
                unset($context['seq'], $context['iterated'], $context['key'], $context['role'], $context['parent'], $context['loop']);
                $context = arrayintersectkey($context, $parent) + $parent;
                // line 27
                echo "
            ";
                // line 28
                if ((isset($context["display"]) || arraykeyexists("display", $context) ? $context["display"] : (function () { throw new TwigErrorRuntime('Variable "display" does not exist.', 28, $this->getSourceContext()); })())) {
                    // line 29
                    echo "                ";
                    $context['parent'] = $context;
                    $context['seq'] = twigensuretraversable(twiggetattribute($this->env, $this->getSourceContext(), $context["group"], "items", array()));
                    foreach ($context['seq'] as $context["key"] => $context["admin"]) {
                        // line 30
                        echo "                    ";
                        $context["count"] = ((isset($context["count"]) || arraykeyexists("count", $context) ? $context["count"] : (function () { throw new TwigErrorRuntime('Variable "count" does not exist.', 30, $this->getSourceContext()); })()) + 1);
                        // line 31
                        echo "                    ";
                        if (((twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "create"), "method") && twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "create"), "method")) || (twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasRoute", array(0 => "list"), "method") && twiggetattribute($this->env, $this->getSourceContext(), $context["admin"], "hasAccess", array(0 => "list"), "method")))) {
                            // line 32
                            echo "                        ";
                            echo calluserfuncarray($this->env->getFunction('sonatablockrender')->getCallable(), array(array("type" => "sonata.admin.block.searchresult"), array("query" =>                             // line 35
(isset($context["query"]) || arraykeyexists("query", $context) ? $context["query"] : (function () { throw new TwigErrorRuntime('Variable "query" does not exist.', 35, $this->getSourceContext()); })()), "admincode" => twiggetattribute($this->env, $this->getSourceContext(),                             // line 36
$context["admin"], "code", array()), "page" => 0, "perpage" => 10, "icon" => twiggetattribute($this->env, $this->getSourceContext(),                             // line 39
$context["group"], "icon", array()))));
                            // line 40
                            echo "
                    ";
                        }
                        // line 42
                        echo "                ";
                    }
                    $parent = $context['parent'];
                    unset($context['seq'], $context['iterated'], $context['key'], $context['admin'], $context['parent'], $context['loop']);
                    $context = arrayintersectkey($context, $parent) + $parent;
                    // line 43
                    echo "            ";
                }
                // line 44
                echo "        ";
            }
            $parent = $context['parent'];
            unset($context['seq'], $context['iterated'], $context['key'], $context['group'], $context['parent'], $context['loop']);
            $context = arrayintersectkey($context, $parent) + $parent;
            // line 45
            echo "        </div>
    ";
        }
        // line 47
        echo "
";
        
        $internal7ec28971087647ca45a7abeda3d7fd7cd813976270743fc0cee4c7c232f8e130->leave($internal7ec28971087647ca45a7abeda3d7fd7cd813976270743fc0cee4c7c232f8e130prof);

        
        $internal9d978ed86ea0b2b636aaddf97272e8607d4b2978a08ee51c63bf2e68ad98eeff->leave($internal9d978ed86ea0b2b636aaddf97272e8607d4b2978a08ee51c63bf2e68ad98eeffprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 47,  163 => 45,  157 => 44,  154 => 43,  148 => 42,  144 => 40,  142 => 39,  141 => 36,  140 => 35,  138 => 32,  135 => 31,  132 => 30,  127 => 29,  125 => 28,  122 => 27,  115 => 26,  112 => 25,  106 => 24,  103 => 23,  99 => 22,  96 => 21,  93 => 20,  91 => 19,  85 => 17,  76 => 16,  59 => 15,  41 => 14,  20 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends basetemplate %}

{% block title %}{{ 'titlesearchresults'|trans({'%query%': query}, 'SonataAdminBundle') }}{% endblock %}
{% block breadcrumb %}{% endblock %}
{% block content %}
    <h2 class=\"page-header\">{{ 'titlesearchresults'|trans({'%query%': query}, 'SonataAdminBundle') }}</h2>

    {% if query is defined and query is not same as(false) %}
        {% set count = 0 %}
        <div class=\"row\" data-masonry='{ \"itemSelector\": \".search-box-item\" }'>
        {% for group in groups %}
            {% set display = (group.roles is empty or isgranted('ROLESUPERADMIN') ) %}
            {% for role in group.roles if not display %}
                {% set display = isgranted(role) %}
            {% endfor %}

            {% if display %}
                {% for admin in group.items %}
                    {% set count = count + 1 %}
                    {% if admin.hasRoute('create') and admin.hasAccess('create') or admin.hasRoute('list') and admin.hasAccess('list') %}
                        {{ sonatablockrender({
                            'type': 'sonata.admin.block.searchresult'
                        }, {
                            'query': query,
                            'admincode': admin.code,
                            'page': 0,
                            'perpage': 10,
                            'icon': group.icon
                        }) }}
                    {% endif %}
                {% endfor %}
            {% endif %}
        {% endfor %}
        </div>
    {% endif %}

{% endblock %}
", "SonataAdminBundle:Core:search.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/Core/search.html.twig");
    }
}
