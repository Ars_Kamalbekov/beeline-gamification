<?php

/* FOSUserBundle:ChangePassword:changepassword.html.twig */
class TwigTemplate79c6e3fc454dc8b4d99b1d5a77ade72880bb46d4e48d3995ff257f10fc6c1f47 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:changepassword.html.twig", 1);
        $this->blocks = array(
            'fosusercontent' => array($this, 'blockfosusercontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internaladd7ed663f76577f888d2ebd1ff6dcef13584ccc3a62e6f39407cef87fa185eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internaladd7ed663f76577f888d2ebd1ff6dcef13584ccc3a62e6f39407cef87fa185eb->enter($internaladd7ed663f76577f888d2ebd1ff6dcef13584ccc3a62e6f39407cef87fa185ebprof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changepassword.html.twig"));

        $internal120d47c16075a49269938024d639a85f8a7aa83830a1a7717794f99a54a515a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal120d47c16075a49269938024d639a85f8a7aa83830a1a7717794f99a54a515a7->enter($internal120d47c16075a49269938024d639a85f8a7aa83830a1a7717794f99a54a515a7prof = new TwigProfilerProfile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:changepassword.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internaladd7ed663f76577f888d2ebd1ff6dcef13584ccc3a62e6f39407cef87fa185eb->leave($internaladd7ed663f76577f888d2ebd1ff6dcef13584ccc3a62e6f39407cef87fa185ebprof);

        
        $internal120d47c16075a49269938024d639a85f8a7aa83830a1a7717794f99a54a515a7->leave($internal120d47c16075a49269938024d639a85f8a7aa83830a1a7717794f99a54a515a7prof);

    }

    // line 3
    public function blockfosusercontent($context, array $blocks = array())
    {
        $internald1d7da39cffe9c0f7fc44883a090373e520aedaf5b62bd598147bf5b466a224a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internald1d7da39cffe9c0f7fc44883a090373e520aedaf5b62bd598147bf5b466a224a->enter($internald1d7da39cffe9c0f7fc44883a090373e520aedaf5b62bd598147bf5b466a224aprof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        $internal745237d56b8b73b7b83d3ced51df7270a07eea9ca0b4f3f07092f69794e16665 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal745237d56b8b73b7b83d3ced51df7270a07eea9ca0b4f3f07092f69794e16665->enter($internal745237d56b8b73b7b83d3ced51df7270a07eea9ca0b4f3f07092f69794e16665prof = new TwigProfilerProfile($this->getTemplateName(), "block", "fosusercontent"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/changepasswordcontent.html.twig", "FOSUserBundle:ChangePassword:changepassword.html.twig", 4)->display($context);
        
        $internal745237d56b8b73b7b83d3ced51df7270a07eea9ca0b4f3f07092f69794e16665->leave($internal745237d56b8b73b7b83d3ced51df7270a07eea9ca0b4f3f07092f69794e16665prof);

        
        $internald1d7da39cffe9c0f7fc44883a090373e520aedaf5b62bd598147bf5b466a224a->leave($internald1d7da39cffe9c0f7fc44883a090373e520aedaf5b62bd598147bf5b466a224aprof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:changepassword.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fosusercontent %}
{% include \"@FOSUser/ChangePassword/changepasswordcontent.html.twig\" %}
{% endblock fosusercontent %}
", "FOSUserBundle:ChangePassword:changepassword.html.twig", "/var/www/html/beeline-gamification/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/changepassword.html.twig");
    }
}
