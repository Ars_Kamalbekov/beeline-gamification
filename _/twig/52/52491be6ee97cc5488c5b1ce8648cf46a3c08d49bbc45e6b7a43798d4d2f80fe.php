<?php

/* SonataAdminBundle:CRUD:selectsubclass.html.twig */
class TwigTemplate96fbe7b6440857093544a85ecd425c4294cb72cb68713ce0c5d0cc422c83e6ed extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'title' => array($this, 'blocktitle'),
            'content' => array($this, 'blockcontent'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate((isset($context["basetemplate"]) || arraykeyexists("basetemplate", $context) ? $context["basetemplate"] : (function () { throw new TwigErrorRuntime('Variable "basetemplate" does not exist.', 11, $this->getSourceContext()); })()), "SonataAdminBundle:CRUD:selectsubclass.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalee613b6a3d325071163e803104c18970c017f5cf0197153915ad8fb2ab549759 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalee613b6a3d325071163e803104c18970c017f5cf0197153915ad8fb2ab549759->enter($internalee613b6a3d325071163e803104c18970c017f5cf0197153915ad8fb2ab549759prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:selectsubclass.html.twig"));

        $internal61c4ca7dfdd620bfcc3b12950297f1dfce2f6f58e877c0d1e91c28fccfefc2af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal61c4ca7dfdd620bfcc3b12950297f1dfce2f6f58e877c0d1e91c28fccfefc2af->enter($internal61c4ca7dfdd620bfcc3b12950297f1dfce2f6f58e877c0d1e91c28fccfefc2afprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:selectsubclass.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internalee613b6a3d325071163e803104c18970c017f5cf0197153915ad8fb2ab549759->leave($internalee613b6a3d325071163e803104c18970c017f5cf0197153915ad8fb2ab549759prof);

        
        $internal61c4ca7dfdd620bfcc3b12950297f1dfce2f6f58e877c0d1e91c28fccfefc2af->leave($internal61c4ca7dfdd620bfcc3b12950297f1dfce2f6f58e877c0d1e91c28fccfefc2afprof);

    }

    // line 13
    public function blocktitle($context, array $blocks = array())
    {
        $internalb22ee7e26ba310cfe809bef440fc6135f897b314a9b1b87db66a7f5bf616a6e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb22ee7e26ba310cfe809bef440fc6135f897b314a9b1b87db66a7f5bf616a6e3->enter($internalb22ee7e26ba310cfe809bef440fc6135f897b314a9b1b87db66a7f5bf616a6e3prof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        $internal5a05eeaefa5d3cfa4b4b0f94c169a55a8b6be72935f4e5322082301d952c5d3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal5a05eeaefa5d3cfa4b4b0f94c169a55a8b6be72935f4e5322082301d952c5d3d->enter($internal5a05eeaefa5d3cfa4b4b0f94c169a55a8b6be72935f4e5322082301d952c5d3dprof = new TwigProfilerProfile($this->getTemplateName(), "block", "title"));

        echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("titleselectsubclass", array(), "SonataAdminBundle"), "html", null, true);
        
        $internal5a05eeaefa5d3cfa4b4b0f94c169a55a8b6be72935f4e5322082301d952c5d3d->leave($internal5a05eeaefa5d3cfa4b4b0f94c169a55a8b6be72935f4e5322082301d952c5d3dprof);

        
        $internalb22ee7e26ba310cfe809bef440fc6135f897b314a9b1b87db66a7f5bf616a6e3->leave($internalb22ee7e26ba310cfe809bef440fc6135f897b314a9b1b87db66a7f5bf616a6e3prof);

    }

    // line 15
    public function blockcontent($context, array $blocks = array())
    {
        $internal33c67aaf09c6a4fd9637d3f72e9c401e7ee5e25f255ff6056a7e32b20d7ab855 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal33c67aaf09c6a4fd9637d3f72e9c401e7ee5e25f255ff6056a7e32b20d7ab855->enter($internal33c67aaf09c6a4fd9637d3f72e9c401e7ee5e25f255ff6056a7e32b20d7ab855prof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        $internald52190fcd3e7bb90313b9bcb7cc8899b1d05b8287709b7ff12bf26d75a05c08c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internald52190fcd3e7bb90313b9bcb7cc8899b1d05b8287709b7ff12bf26d75a05c08c->enter($internald52190fcd3e7bb90313b9bcb7cc8899b1d05b8287709b7ff12bf26d75a05c08cprof = new TwigProfilerProfile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                ";
        // line 19
        $this->displayBlock("title", $context, $blocks);
        echo "
            </h3>
        </div>
        <div class=\"box-body\">
            ";
        // line 23
        $context['parent'] = $context;
        $context['seq'] = twigensuretraversable(twiggetarraykeysfilter(twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "subclasses", array())));
        $context['iterated'] = false;
        foreach ($context['seq'] as $context["key"] => $context["subclass"]) {
            // line 24
            echo "                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"";
            // line 25
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 25, $this->getSourceContext()); })()), "generateUrl", array(0 => (isset($context["action"]) || arraykeyexists("action", $context) ? $context["action"] : (function () { throw new TwigErrorRuntime('Variable "action" does not exist.', 25, $this->getSourceContext()); })()), 1 => array("subclass" => $context["subclass"])), "method"), "html", null, true);
            echo "\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        ";
            // line 29
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["subclass"], array(), ((twiggetattribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "translationdomain", array(), "any", true, true)) ? (twigdefaultfilter(twiggetattribute($this->env, $this->getSourceContext(), ($context["admin"] ?? null), "translationdomain", array()), "SonataAdminBundle")) : ("SonataAdminBundle"))), "html", null, true);
            echo "
                    </a>
                </div>
            ";
            $context['iterated'] = true;
        }
        if (!$context['iterated']) {
            // line 33
            echo "                <span class=\"alert alert-info\">";
            echo twigescapefilter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("nosubclassavailable", array(), "SonataAdminBundle"), "html", null, true);
            echo "</span>
            ";
        }
        $parent = $context['parent'];
        unset($context['seq'], $context['iterated'], $context['key'], $context['subclass'], $context['parent'], $context['loop']);
        $context = arrayintersectkey($context, $parent) + $parent;
        // line 35
        echo "            <div class=\"clearfix\"></div>
        </div>
    </div>
";
        
        $internald52190fcd3e7bb90313b9bcb7cc8899b1d05b8287709b7ff12bf26d75a05c08c->leave($internald52190fcd3e7bb90313b9bcb7cc8899b1d05b8287709b7ff12bf26d75a05c08cprof);

        
        $internal33c67aaf09c6a4fd9637d3f72e9c401e7ee5e25f255ff6056a7e32b20d7ab855->leave($internal33c67aaf09c6a4fd9637d3f72e9c401e7ee5e25f255ff6056a7e32b20d7ab855prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:selectsubclass.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 35,  103 => 33,  94 => 29,  87 => 25,  84 => 24,  79 => 23,  72 => 19,  67 => 16,  58 => 15,  40 => 13,  19 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends basetemplate %}

{% block title %}{{ 'titleselectsubclass'|trans({}, 'SonataAdminBundle') }}{% endblock %}

{% block content %}
    <div class=\"box box-success\">
        <div class=\"box-header\">
            <h3 class=\"box-title\">
                {{ block('title') }}
            </h3>
        </div>
        <div class=\"box-body\">
            {% for subclass in admin.subclasses|keys %}
                <div class=\"col-lg-2 col-md-3 col-sm-4 col-xs-6\">
                    <a href=\"{{ admin.generateUrl(action, {'subclass': subclass }) }}\"
                       class=\"btn btn-app btn-block\"
                            >
                        <i class=\"fa fa-plus-square\" aria-hidden=\"true\"></i>
                        {{ subclass|trans({}, admin.translationdomain|default('SonataAdminBundle')) }}
                    </a>
                </div>
            {% else %}
                <span class=\"alert alert-info\">{{ 'nosubclassavailable'|trans({}, 'SonataAdminBundle') }}</span>
            {% endfor %}
            <div class=\"clearfix\"></div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:selectsubclass.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/selectsubclass.html.twig");
    }
}
