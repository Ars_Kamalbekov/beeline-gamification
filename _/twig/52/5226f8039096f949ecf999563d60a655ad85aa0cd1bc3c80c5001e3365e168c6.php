<?php

/* SonataAdminBundle:CRUD:showtime.html.twig */
class TwigTemplate892a02363d1e705d84a7ecd0b35c839910e6fee92a4e80eb4d0279f5d08c380c extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:baseshowfield.html.twig", "SonataAdminBundle:CRUD:showtime.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'blockfield'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:baseshowfield.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal1ad83d3c26a5ff3881ac2365337a7026aa6fbaf05bdaeca95659c9f650af773f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal1ad83d3c26a5ff3881ac2365337a7026aa6fbaf05bdaeca95659c9f650af773f->enter($internal1ad83d3c26a5ff3881ac2365337a7026aa6fbaf05bdaeca95659c9f650af773fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showtime.html.twig"));

        $internal2aeefa1bbf4ab1a4744429341a618e1bc635ea194d65f8379ff81296a871e8ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal2aeefa1bbf4ab1a4744429341a618e1bc635ea194d65f8379ff81296a871e8ce->enter($internal2aeefa1bbf4ab1a4744429341a618e1bc635ea194d65f8379ff81296a871e8ceprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:showtime.html.twig"));

        $this->parent->display($context, arraymerge($this->blocks, $blocks));
        
        $internal1ad83d3c26a5ff3881ac2365337a7026aa6fbaf05bdaeca95659c9f650af773f->leave($internal1ad83d3c26a5ff3881ac2365337a7026aa6fbaf05bdaeca95659c9f650af773fprof);

        
        $internal2aeefa1bbf4ab1a4744429341a618e1bc635ea194d65f8379ff81296a871e8ce->leave($internal2aeefa1bbf4ab1a4744429341a618e1bc635ea194d65f8379ff81296a871e8ceprof);

    }

    // line 14
    public function blockfield($context, array $blocks = array())
    {
        $internalb315de9b24a4795a4e17e526eb992d653ff931bfb39a1da38a9e852a8f4210bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb315de9b24a4795a4e17e526eb992d653ff931bfb39a1da38a9e852a8f4210bc->enter($internalb315de9b24a4795a4e17e526eb992d653ff931bfb39a1da38a9e852a8f4210bcprof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        $internal53a7015faa7cfdf75554fbafd04cfa57ff520cde906b487dff9d1106e7ee7af1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal53a7015faa7cfdf75554fbafd04cfa57ff520cde906b487dff9d1106e7ee7af1->enter($internal53a7015faa7cfdf75554fbafd04cfa57ff520cde906b487dff9d1106e7ee7af1prof = new TwigProfilerProfile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twigtestempty((isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 15, $this->getSourceContext()); })()))) {
            // line 16
            echo "&nbsp;";
        } else {
            // line 18
            echo twigescapefilter($this->env, twigdateformatfilter($this->env, (isset($context["value"]) || arraykeyexists("value", $context) ? $context["value"] : (function () { throw new TwigErrorRuntime('Variable "value" does not exist.', 18, $this->getSourceContext()); })()), "H:i:s"), "html", null, true);
        }
        
        $internal53a7015faa7cfdf75554fbafd04cfa57ff520cde906b487dff9d1106e7ee7af1->leave($internal53a7015faa7cfdf75554fbafd04cfa57ff520cde906b487dff9d1106e7ee7af1prof);

        
        $internalb315de9b24a4795a4e17e526eb992d653ff931bfb39a1da38a9e852a8f4210bc->leave($internalb315de9b24a4795a4e17e526eb992d653ff931bfb39a1da38a9e852a8f4210bcprof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:showtime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 18,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:baseshowfield.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- else -%}
        {{ value|date('H:i:s') }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:showtime.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/showtime.html.twig");
    }
}
