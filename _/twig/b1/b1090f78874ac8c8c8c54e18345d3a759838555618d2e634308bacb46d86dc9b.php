<?php

/* @Twig/images/icon-support.svg */
class TwigTemplateb050487bb3589b5fe6fd6ec78458392367c70fc94f67c0f44d0163d8cbddf10b extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalc466f4792d5fa61878ecae124c514c8f2f2ff25d0d810c692a5f215dc71e2d18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalc466f4792d5fa61878ecae124c514c8f2f2ff25d0d810c692a5f215dc71e2d18->enter($internalc466f4792d5fa61878ecae124c514c8f2f2ff25d0d810c692a5f215dc71e2d18prof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        $internale102d56f938439ddfdc411d4c8b614af9ea38e8a1b8ca2bad0ff74c93189a38e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internale102d56f938439ddfdc411d4c8b614af9ea38e8a1b8ca2bad0ff74c93189a38e->enter($internale102d56f938439ddfdc411d4c8b614af9ea38e8a1b8ca2bad0ff74c93189a38eprof = new TwigProfilerProfile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
";
        
        $internalc466f4792d5fa61878ecae124c514c8f2f2ff25d0d810c692a5f215dc71e2d18->leave($internalc466f4792d5fa61878ecae124c514c8f2f2ff25d0d810c692a5f215dc71e2d18prof);

        
        $internale102d56f938439ddfdc411d4c8b614af9ea38e8a1b8ca2bad0ff74c93189a38e->leave($internale102d56f938439ddfdc411d4c8b614af9ea38e8a1b8ca2bad0ff74c93189a38eprof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-support.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
", "@Twig/images/icon-support.svg", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-support.svg");
    }
}
