<?php

/* TwigBundle:Exception:exception.atom.twig */
class TwigTemplate864c822154c1ce7b256f2f1636bace484329500baee06eb4f1a5700483052626 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalcca65f6e8bf6d30a0f8c4c26018a29a4fb3f0c84204f144ea91f82238ef66ba9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalcca65f6e8bf6d30a0f8c4c26018a29a4fb3f0c84204f144ea91f82238ef66ba9->enter($internalcca65f6e8bf6d30a0f8c4c26018a29a4fb3f0c84204f144ea91f82238ef66ba9prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $internal1583d73dfa97b6bb0b6fbaedcca16e4147bec93face4e79f5d2a731e1d7e6a22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1583d73dfa97b6bb0b6fbaedcca16e4147bec93face4e79f5d2a731e1d7e6a22->enter($internal1583d73dfa97b6bb0b6fbaedcca16e4147bec93face4e79f5d2a731e1d7e6a22prof = new TwigProfilerProfile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twiginclude($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || arraykeyexists("exception", $context) ? $context["exception"] : (function () { throw new TwigErrorRuntime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $internalcca65f6e8bf6d30a0f8c4c26018a29a4fb3f0c84204f144ea91f82238ef66ba9->leave($internalcca65f6e8bf6d30a0f8c4c26018a29a4fb3f0c84204f144ea91f82238ef66ba9prof);

        
        $internal1583d73dfa97b6bb0b6fbaedcca16e4147bec93face4e79f5d2a731e1d7e6a22->leave($internal1583d73dfa97b6bb0b6fbaedcca16e4147bec93face4e79f5d2a731e1d7e6a22prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/var/www/html/beeline-gamification/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
