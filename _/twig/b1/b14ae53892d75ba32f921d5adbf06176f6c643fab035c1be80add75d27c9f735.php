<?php

/* SonataAdminBundle:CRUD:baselistflatinnerrow.html.twig */
class TwigTemplate6e0d26c3374fbfa9ab8f51533009808b669d03b676773fca7254a3de84e146e6 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->parent = false;

        $this->blocks = array(
            'row' => array($this, 'blockrow'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internalfcb69dc824196099f45744ff81de5a25276c2b26910c51356b21976b34c71bdb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalfcb69dc824196099f45744ff81de5a25276c2b26910c51356b21976b34c71bdb->enter($internalfcb69dc824196099f45744ff81de5a25276c2b26910c51356b21976b34c71bdbprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistflatinnerrow.html.twig"));

        $internalca8120049bae5bc81de404ddbdb9c6b5f842ab0fe6782116a6b400eaee97e799 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internalca8120049bae5bc81de404ddbdb9c6b5f842ab0fe6782116a6b400eaee97e799->enter($internalca8120049bae5bc81de404ddbdb9c6b5f842ab0fe6782116a6b400eaee97e799prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:baselistflatinnerrow.html.twig"));

        // line 11
        echo "
";
        // line 12
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 12, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "batch"), "method") &&  !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 12, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
            // line 13
            echo "    <td class=\"sonata-ba-list-field sonata-ba-list-field-batch\">
        ";
            // line 14
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderListElement($this->env, (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 14, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 14, $this->getSourceContext()); })()), "list", array()), "batch", array(), "array"));
            echo "
    </td>
";
        }
        // line 17
        echo "
<td class=\"sonata-ba-list-field sonata-ba-list-field-inline-fields\" colspan=\"";
        // line 18
        echo twigescapefilter($this->env, (twiglengthfilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "list", array()), "elements", array())) - (twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "action"), "method") + twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "batch"), "method"))), "html", null, true);
        echo "\" objectId=\"";
        echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 18, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 18, $this->getSourceContext()); })())), "method"), "html", null, true);
        echo "\">
    ";
        // line 19
        $this->displayBlock('row', $context, $blocks);
        // line 20
        echo "</td>

";
        // line 22
        if ((twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 22, $this->getSourceContext()); })()), "list", array()), "has", array(0 => "action"), "method") &&  !twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["app"]) || arraykeyexists("app", $context) ? $context["app"] : (function () { throw new TwigErrorRuntime('Variable "app" does not exist.', 22, $this->getSourceContext()); })()), "request", array()), "isXmlHttpRequest", array()))) {
            // line 23
            echo "    <td class=\"sonata-ba-list-field sonata-ba-list-field-action\" objectId=\"";
            echo twigescapefilter($this->env, twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 23, $this->getSourceContext()); })()), "id", array(0 => (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 23, $this->getSourceContext()); })())), "method"), "html", null, true);
            echo "\">
        ";
            // line 24
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderListElement($this->env, (isset($context["object"]) || arraykeyexists("object", $context) ? $context["object"] : (function () { throw new TwigErrorRuntime('Variable "object" does not exist.', 24, $this->getSourceContext()); })()), twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["admin"]) || arraykeyexists("admin", $context) ? $context["admin"] : (function () { throw new TwigErrorRuntime('Variable "admin" does not exist.', 24, $this->getSourceContext()); })()), "list", array()), "action", array(), "array"));
            echo "
    </td>
";
        }
        
        $internalfcb69dc824196099f45744ff81de5a25276c2b26910c51356b21976b34c71bdb->leave($internalfcb69dc824196099f45744ff81de5a25276c2b26910c51356b21976b34c71bdbprof);

        
        $internalca8120049bae5bc81de404ddbdb9c6b5f842ab0fe6782116a6b400eaee97e799->leave($internalca8120049bae5bc81de404ddbdb9c6b5f842ab0fe6782116a6b400eaee97e799prof);

    }

    // line 19
    public function blockrow($context, array $blocks = array())
    {
        $internal793e58767cf886917b5673cf00a0682910cf36afae133957717430a08bcb5966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal793e58767cf886917b5673cf00a0682910cf36afae133957717430a08bcb5966->enter($internal793e58767cf886917b5673cf00a0682910cf36afae133957717430a08bcb5966prof = new TwigProfilerProfile($this->getTemplateName(), "block", "row"));

        $internal53d317a7fef66036c0174ba9b22d3234eca485315113737094c454bdc2e1e60e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal53d317a7fef66036c0174ba9b22d3234eca485315113737094c454bdc2e1e60e->enter($internal53d317a7fef66036c0174ba9b22d3234eca485315113737094c454bdc2e1e60eprof = new TwigProfilerProfile($this->getTemplateName(), "block", "row"));

        
        $internal53d317a7fef66036c0174ba9b22d3234eca485315113737094c454bdc2e1e60e->leave($internal53d317a7fef66036c0174ba9b22d3234eca485315113737094c454bdc2e1e60eprof);

        
        $internal793e58767cf886917b5673cf00a0682910cf36afae133957717430a08bcb5966->leave($internal793e58767cf886917b5673cf00a0682910cf36afae133957717430a08bcb5966prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:baselistflatinnerrow.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 19,  62 => 24,  57 => 23,  55 => 22,  51 => 20,  49 => 19,  43 => 18,  40 => 17,  34 => 14,  31 => 13,  29 => 12,  26 => 11,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% if admin.list.has('batch') and not app.request.isXmlHttpRequest %}
    <td class=\"sonata-ba-list-field sonata-ba-list-field-batch\">
        {{ object|renderlistelement(admin.list['batch']) }}
    </td>
{% endif %}

<td class=\"sonata-ba-list-field sonata-ba-list-field-inline-fields\" colspan=\"{{ admin.list.elements|length - (admin.list.has('action') + admin.list.has('batch')) }}\" objectId=\"{{ admin.id(object) }}\">
    {% block row %}{% endblock %}
</td>

{% if admin.list.has('action') and not app.request.isXmlHttpRequest %}
    <td class=\"sonata-ba-list-field sonata-ba-list-field-action\" objectId=\"{{ admin.id(object) }}\">
        {{ object|renderlistelement(admin.list['action']) }}
    </td>
{% endif %}
", "SonataAdminBundle:CRUD:baselistflatinnerrow.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/admin-bundle/Resources/views/CRUD/baselistflatinnerrow.html.twig");
    }
}
