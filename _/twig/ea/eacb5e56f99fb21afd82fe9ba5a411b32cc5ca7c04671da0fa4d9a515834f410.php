<?php

/* SonataBlockBundle:Block:blockcoreaction.html.twig */
class TwigTemplate8a43f9e06577b484c174e9f46d174a37e4ef03b30ec1052f8695c60e39b35005 extends TwigTemplate
{
    public function construct(TwigEnvironment $env)
    {
        parent::construct($env);

        $this->blocks = array(
            'block' => array($this, 'blockblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate(twiggetattribute($this->env, $this->getSourceContext(), twiggetattribute($this->env, $this->getSourceContext(), (isset($context["sonatablock"]) || arraykeyexists("sonatablock", $context) ? $context["sonatablock"] : (function () { throw new TwigErrorRuntime('Variable "sonatablock" does not exist.', 12, $this->getSourceContext()); })()), "templates", array()), "blockbase", array()), "SonataBlockBundle:Block:blockcoreaction.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $internal2acdb5c0d60ad362130b5d7a2ba886f2679e8bcee0406882e359e950f07c13d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internal2acdb5c0d60ad362130b5d7a2ba886f2679e8bcee0406882e359e950f07c13d8->enter($internal2acdb5c0d60ad362130b5d7a2ba886f2679e8bcee0406882e359e950f07c13d8prof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoreaction.html.twig"));

        $internal1a5ed706fea518936ac7bc289679356f7e659df1891039b8d69af82ebea76d8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal1a5ed706fea518936ac7bc289679356f7e659df1891039b8d69af82ebea76d8f->enter($internal1a5ed706fea518936ac7bc289679356f7e659df1891039b8d69af82ebea76d8fprof = new TwigProfilerProfile($this->getTemplateName(), "template", "SonataBlockBundle:Block:blockcoreaction.html.twig"));

        $this->getParent($context)->display($context, arraymerge($this->blocks, $blocks));
        
        $internal2acdb5c0d60ad362130b5d7a2ba886f2679e8bcee0406882e359e950f07c13d8->leave($internal2acdb5c0d60ad362130b5d7a2ba886f2679e8bcee0406882e359e950f07c13d8prof);

        
        $internal1a5ed706fea518936ac7bc289679356f7e659df1891039b8d69af82ebea76d8f->leave($internal1a5ed706fea518936ac7bc289679356f7e659df1891039b8d69af82ebea76d8fprof);

    }

    // line 14
    public function blockblock($context, array $blocks = array())
    {
        $internalb752ba05a36c773eb4c7d5f54c837af2e20458831d8ac224e12dd78014f83ad9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $internalb752ba05a36c773eb4c7d5f54c837af2e20458831d8ac224e12dd78014f83ad9->enter($internalb752ba05a36c773eb4c7d5f54c837af2e20458831d8ac224e12dd78014f83ad9prof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        $internal6a2a33c6ab1550b65bec6e3052c08f7b418362a26f9e9c3831de1bd39830af2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $internal6a2a33c6ab1550b65bec6e3052c08f7b418362a26f9e9c3831de1bd39830af2f->enter($internal6a2a33c6ab1550b65bec6e3052c08f7b418362a26f9e9c3831de1bd39830af2fprof = new TwigProfilerProfile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo (isset($context["content"]) || arraykeyexists("content", $context) ? $context["content"] : (function () { throw new TwigErrorRuntime('Variable "content" does not exist.', 15, $this->getSourceContext()); })());
        echo "
";
        
        $internal6a2a33c6ab1550b65bec6e3052c08f7b418362a26f9e9c3831de1bd39830af2f->leave($internal6a2a33c6ab1550b65bec6e3052c08f7b418362a26f9e9c3831de1bd39830af2fprof);

        
        $internalb752ba05a36c773eb4c7d5f54c837af2e20458831d8ac224e12dd78014f83ad9->leave($internalb752ba05a36c773eb4c7d5f54c837af2e20458831d8ac224e12dd78014f83ad9prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:blockcoreaction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    public function getSourceContext()
    {
        return new TwigSource("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonatablock.templates.blockbase %}

{% block block %}
    {{ content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:blockcoreaction.html.twig", "/var/www/html/beeline-gamification/vendor/sonata-project/block-bundle/Resources/views/Block/blockcoreaction.html.twig");
    }
}
