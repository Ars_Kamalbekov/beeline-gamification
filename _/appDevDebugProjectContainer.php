<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;

/**
 * appDevDebugProjectContaine.
 *
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final since Symfony 3.3
 */
class appDevDebugProjectContaine extends Container
{
    private $parameters;
    private $targetDirs = array();

    /**
     * Constructor.
     */
    public function construct()
    {
        $dir = DIR;
        for ($i = 1; $i <= 3; ++$i) {
            $this->targetDirs[$i] = $dir = dirname($dir);
        }
        $this->parameters = $this->getDefaultParameters();

        $this->services = array();
        $this->methodMap = array(
            '10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' => 'get10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service',
            '20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' => 'get20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service',
            'annotationreader' => 'getAnnotationReaderService',
            'annotations.reader' => 'getAnnotationsReaderService',
            'app.admin.office' => 'getAppAdminOfficeService',
            'app.admin.partner' => 'getAppAdminPartnerService',
            'app.admin.region' => 'getAppAdminRegionService',
            'app.admin.user' => 'getAppAdminUserService',
            'app.form.registration' => 'getAppFormRegistrationService',
            'argumentresolver.default' => 'getArgumentResolverDefaultService',
            'argumentresolver.request' => 'getArgumentResolverRequestService',
            'argumentresolver.requestattribute' => 'getArgumentResolverRequestAttributeService',
            'argumentresolver.service' => 'getArgumentResolverServiceService',
            'argumentresolver.session' => 'getArgumentResolverSessionService',
            'argumentresolver.variadic' => 'getArgumentResolverVariadicService',
            'assets.context' => 'getAssetsContextService',
            'assets.packages' => 'getAssetsPackagesService',
            'cache.annotations' => 'getCacheAnnotationsService',
            'cache.annotations.recorderinner' => 'getCacheAnnotationsRecorderInnerService',
            'cache.app' => 'getCacheAppService',
            'cache.app.recorderinner' => 'getCacheAppRecorderInnerService',
            'cache.defaultclearer' => 'getCacheDefaultClearerService',
            'cache.globalclearer' => 'getCacheGlobalClearerService',
            'cache.serializer.recorderinner' => 'getCacheSerializerRecorderInnerService',
            'cache.system' => 'getCacheSystemService',
            'cache.system.recorderinner' => 'getCacheSystemRecorderInnerService',
            'cache.validator' => 'getCacheValidatorService',
            'cache.validator.recorderinner' => 'getCacheValidatorRecorderInnerService',
            'cacheclearer' => 'getCacheClearerService',
            'cachewarmer' => 'getCacheWarmerService',
            'configcachefactory' => 'getConfigCacheFactoryService',
            'console.command.symfonybundlesecuritybundlecommanduserpasswordencodercommand' => 'getConsoleCommandSymfonyBundleSecuritybundleCommandUserpasswordencodercommandService',
            'console.command.symfonybundlewebserverbundlecommandserverruncommand' => 'getConsoleCommandSymfonyBundleWebserverbundleCommandServerruncommandService',
            'console.command.symfonybundlewebserverbundlecommandserverstartcommand' => 'getConsoleCommandSymfonyBundleWebserverbundleCommandServerstartcommandService',
            'console.command.symfonybundlewebserverbundlecommandserverstatuscommand' => 'getConsoleCommandSymfonyBundleWebserverbundleCommandServerstatuscommandService',
            'console.command.symfonybundlewebserverbundlecommandserverstopcommand' => 'getConsoleCommandSymfonyBundleWebserverbundleCommandServerstopcommandService',
            'console.errorlistener' => 'getConsoleErrorListenerService',
            'controllernameconverter' => 'getControllerNameConverterService',
            'datacollector.dump' => 'getDataCollectorDumpService',
            'datacollector.form' => 'getDataCollectorFormService',
            'datacollector.form.extractor' => 'getDataCollectorFormExtractorService',
            'datacollector.request' => 'getDataCollectorRequestService',
            'datacollector.router' => 'getDataCollectorRouterService',
            'datacollector.translation' => 'getDataCollectorTranslationService',
            'debug.argumentresolver' => 'getDebugArgumentResolverService',
            'debug.controllerresolver' => 'getDebugControllerResolverService',
            'debug.debughandlerslistener' => 'getDebugDebugHandlersListenerService',
            'debug.dumplistener' => 'getDebugDumpListenerService',
            'debug.eventdispatcher' => 'getDebugEventDispatcherService',
            'debug.filelinkformatter' => 'getDebugFileLinkFormatterService',
            'debug.logprocessor' => 'getDebugLogProcessorService',
            'debug.security.access.decisionmanager' => 'getDebugSecurityAccessDecisionManagerService',
            'debug.stopwatch' => 'getDebugStopwatchService',
            'deprecated.form.registry' => 'getDeprecatedFormRegistryService',
            'deprecated.form.registry.csrf' => 'getDeprecatedFormRegistryCsrfService',
            'doctrine' => 'getDoctrineService',
            'doctrine.dbal.connectionfactory' => 'getDoctrineDbalConnectionFactoryService',
            'doctrine.dbal.defaultconnection' => 'getDoctrineDbalDefaultConnectionService',
            'doctrine.dbal.logger.profiling.default' => 'getDoctrineDbalLoggerProfilingDefaultService',
            'doctrine.orm.defaultentitylistenerresolver' => 'getDoctrineOrmDefaultEntityListenerResolverService',
            'doctrine.orm.defaultentitymanager' => 'getDoctrineOrmDefaultEntityManagerService',
            'doctrine.orm.defaultentitymanager.propertyinfoextractor' => 'getDoctrineOrmDefaultEntityManagerPropertyInfoExtractorService',
            'doctrine.orm.defaultlisteners.attachentitylisteners' => 'getDoctrineOrmDefaultListenersAttachEntityListenersService',
            'doctrine.orm.defaultmanagerconfigurator' => 'getDoctrineOrmDefaultManagerConfiguratorService',
            'doctrine.orm.validator.unique' => 'getDoctrineOrmValidatorUniqueService',
            'doctrine.orm.validatorinitializer' => 'getDoctrineOrmValidatorInitializerService',
            'doctrinecache.providers.doctrine.orm.defaultmetadatacache' => 'getDoctrineCacheProvidersDoctrineOrmDefaultMetadataCacheService',
            'doctrinecache.providers.doctrine.orm.defaultquerycache' => 'getDoctrineCacheProvidersDoctrineOrmDefaultQueryCacheService',
            'doctrinecache.providers.doctrine.orm.defaultresultcache' => 'getDoctrineCacheProvidersDoctrineOrmDefaultResultCacheService',
            'filelocator' => 'getFileLocatorService',
            'filesystem' => 'getFilesystemService',
            'form.extension' => 'getFormExtensionService',
            'form.factory' => 'getFormFactoryService',
            'form.registry' => 'getFormRegistryService',
            'form.resolvedtypefactory' => 'getFormResolvedTypeFactoryService',
            'form.serverparams' => 'getFormServerParamsService',
            'form.type.birthday' => 'getFormTypeBirthdayService',
            'form.type.button' => 'getFormTypeButtonService',
            'form.type.checkbox' => 'getFormTypeCheckboxService',
            'form.type.choice' => 'getFormTypeChoiceService',
            'form.type.collection' => 'getFormTypeCollectionService',
            'form.type.country' => 'getFormTypeCountryService',
            'form.type.currency' => 'getFormTypeCurrencyService',
            'form.type.date' => 'getFormTypeDateService',
            'form.type.datetime' => 'getFormTypeDatetimeService',
            'form.type.email' => 'getFormTypeEmailService',
            'form.type.entity' => 'getFormTypeEntityService',
            'form.type.file' => 'getFormTypeFileService',
            'form.type.form' => 'getFormTypeFormService',
            'form.type.hidden' => 'getFormTypeHiddenService',
            'form.type.integer' => 'getFormTypeIntegerService',
            'form.type.language' => 'getFormTypeLanguageService',
            'form.type.locale' => 'getFormTypeLocaleService',
            'form.type.money' => 'getFormTypeMoneyService',
            'form.type.number' => 'getFormTypeNumberService',
            'form.type.password' => 'getFormTypePasswordService',
            'form.type.percent' => 'getFormTypePercentService',
            'form.type.radio' => 'getFormTypeRadioService',
            'form.type.range' => 'getFormTypeRangeService',
            'form.type.repeated' => 'getFormTypeRepeatedService',
            'form.type.reset' => 'getFormTypeResetService',
            'form.type.search' => 'getFormTypeSearchService',
            'form.type.submit' => 'getFormTypeSubmitService',
            'form.type.text' => 'getFormTypeTextService',
            'form.type.textarea' => 'getFormTypeTextareaService',
            'form.type.time' => 'getFormTypeTimeService',
            'form.type.timezone' => 'getFormTypeTimezoneService',
            'form.type.url' => 'getFormTypeUrlService',
            'form.typeextension.csrf' => 'getFormTypeExtensionCsrfService',
            'form.typeextension.form.datacollector' => 'getFormTypeExtensionFormDataCollectorService',
            'form.typeextension.form.httpfoundation' => 'getFormTypeExtensionFormHttpFoundationService',
            'form.typeextension.form.validator' => 'getFormTypeExtensionFormValidatorService',
            'form.typeextension.repeated.validator' => 'getFormTypeExtensionRepeatedValidatorService',
            'form.typeextension.submit.validator' => 'getFormTypeExtensionSubmitValidatorService',
            'form.typeextension.upload.validator' => 'getFormTypeExtensionUploadValidatorService',
            'form.typeguesser.doctrine' => 'getFormTypeGuesserDoctrineService',
            'form.typeguesser.validator' => 'getFormTypeGuesserValidatorService',
            'fosuser.changepassword.form.factory' => 'getFosUserChangePasswordFormFactoryService',
            'fosuser.changepassword.form.type' => 'getFosUserChangePasswordFormTypeService',
            'fosuser.listener.authentication' => 'getFosUserListenerAuthenticationService',
            'fosuser.listener.emailconfirmation' => 'getFosUserListenerEmailConfirmationService',
            'fosuser.listener.flash' => 'getFosUserListenerFlashService',
            'fosuser.listener.resetting' => 'getFosUserListenerResettingService',
            'fosuser.mailer' => 'getFosUserMailerService',
            'fosuser.profile.form.factory' => 'getFosUserProfileFormFactoryService',
            'fosuser.profile.form.type' => 'getFosUserProfileFormTypeService',
            'fosuser.registration.form.factory' => 'getFosUserRegistrationFormFactoryService',
            'fosuser.registration.form.type' => 'getFosUserRegistrationFormTypeService',
            'fosuser.resetting.form.factory' => 'getFosUserResettingFormFactoryService',
            'fosuser.resetting.form.type' => 'getFosUserResettingFormTypeService',
            'fosuser.security.interactiveloginlistener' => 'getFosUserSecurityInteractiveLoginListenerService',
            'fosuser.security.loginmanager' => 'getFosUserSecurityLoginManagerService',
            'fosuser.usermanager' => 'getFosUserUserManagerService',
            'fosuser.userprovider.usernameemail' => 'getFosUserUserProviderUsernameEmailService',
            'fosuser.usernameformtype' => 'getFosUserUsernameFormTypeService',
            'fosuser.util.canonicalfieldsupdater' => 'getFosUserUtilCanonicalFieldsUpdaterService',
            'fosuser.util.emailcanonicalizer' => 'getFosUserUtilEmailCanonicalizerService',
            'fosuser.util.passwordupdater' => 'getFosUserUtilPasswordUpdaterService',
            'fosuser.util.tokengenerator' => 'getFosUserUtilTokenGeneratorService',
            'fosuser.util.usermanipulator' => 'getFosUserUtilUserManipulatorService',
            'fragment.handler' => 'getFragmentHandlerService',
            'fragment.listener' => 'getFragmentListenerService',
            'fragment.renderer.esi' => 'getFragmentRendererEsiService',
            'fragment.renderer.hinclude' => 'getFragmentRendererHincludeService',
            'fragment.renderer.inline' => 'getFragmentRendererInlineService',
            'fragment.renderer.ssi' => 'getFragmentRendererSsiService',
            'httpkernel' => 'getHttpKernelService',
            'kernel.classcache.cachewarmer' => 'getKernelClassCacheCacheWarmerService',
            'knpmenu.factory' => 'getKnpMenuFactoryService',
            'knpmenu.listener.voters' => 'getKnpMenuListenerVotersService',
            'knpmenu.matcher' => 'getKnpMenuMatcherService',
            'knpmenu.menuprovider' => 'getKnpMenuMenuProviderService',
            'knpmenu.renderer.list' => 'getKnpMenuRendererListService',
            'knpmenu.renderer.twig' => 'getKnpMenuRendererTwigService',
            'knpmenu.rendererprovider' => 'getKnpMenuRendererProviderService',
            'knpmenu.voter.router' => 'getKnpMenuVoterRouterService',
            'localelistener' => 'getLocaleListenerService',
            'logger' => 'getLoggerService',
            'monolog.activationstrategy.notfound' => 'getMonologActivationStrategyNotFoundService',
            'monolog.handler.console' => 'getMonologHandlerConsoleService',
            'monolog.handler.fingerscrossed.errorlevelactivationstrategy' => 'getMonologHandlerFingersCrossedErrorLevelActivationStrategyService',
            'monolog.handler.main' => 'getMonologHandlerMainService',
            'monolog.handler.nullinternal' => 'getMonologHandlerNullInternalService',
            'monolog.handler.serverlog' => 'getMonologHandlerServerLogService',
            'monolog.logger.cache' => 'getMonologLoggerCacheService',
            'monolog.logger.console' => 'getMonologLoggerConsoleService',
            'monolog.logger.doctrine' => 'getMonologLoggerDoctrineService',
            'monolog.logger.event' => 'getMonologLoggerEventService',
            'monolog.logger.php' => 'getMonologLoggerPhpService',
            'monolog.logger.profiler' => 'getMonologLoggerProfilerService',
            'monolog.logger.request' => 'getMonologLoggerRequestService',
            'monolog.logger.router' => 'getMonologLoggerRouterService',
            'monolog.logger.security' => 'getMonologLoggerSecurityService',
            'monolog.logger.templating' => 'getMonologLoggerTemplatingService',
            'monolog.logger.translation' => 'getMonologLoggerTranslationService',
            'profiler' => 'getProfilerService',
            'profilerlistener' => 'getProfilerListenerService',
            'propertyaccessor' => 'getPropertyAccessorService',
            'requeststack' => 'getRequestStackService',
            'resolvecontrollernamesubscriber' => 'getResolveControllerNameSubscriberService',
            'responselistener' => 'getResponseListenerService',
            'router' => 'getRouterService',
            'router.requestcontext' => 'getRouterRequestContextService',
            'routerlistener' => 'getRouterListenerService',
            'routing.loader' => 'getRoutingLoaderService',
            'security.access.authenticatedvoter' => 'getSecurityAccessAuthenticatedVoterService',
            'security.access.expressionvoter' => 'getSecurityAccessExpressionVoterService',
            'security.access.rolehierarchyvoter' => 'getSecurityAccessRoleHierarchyVoterService',
            'security.authentication.guardhandler' => 'getSecurityAuthenticationGuardHandlerService',
            'security.authentication.manager' => 'getSecurityAuthenticationManagerService',
            'security.authentication.provider.anonymous.main' => 'getSecurityAuthenticationProviderAnonymousMainService',
            'security.authentication.provider.dao.main' => 'getSecurityAuthenticationProviderDaoMainService',
            'security.authentication.sessionstrategy' => 'getSecurityAuthenticationSessionStrategyService',
            'security.authentication.trustresolver' => 'getSecurityAuthenticationTrustResolverService',
            'security.authenticationutils' => 'getSecurityAuthenticationUtilsService',
            'security.authorizationchecker' => 'getSecurityAuthorizationCheckerService',
            'security.csrf.tokenmanager' => 'getSecurityCsrfTokenManagerService',
            'security.encoderfactory' => 'getSecurityEncoderFactoryService',
            'security.firewall' => 'getSecurityFirewallService',
            'security.firewall.map' => 'getSecurityFirewallMapService',
            'security.firewall.map.context.main' => 'getSecurityFirewallMapContextMainService',
            'security.logouturlgenerator' => 'getSecurityLogoutUrlGeneratorService',
            'security.passwordencoder' => 'getSecurityPasswordEncoderService',
            'security.rememberme.responselistener' => 'getSecurityRemembermeResponseListenerService',
            'security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db' => 'getSecurityRequestMatcherA64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60dbService',
            'security.rolehierarchy' => 'getSecurityRoleHierarchyService',
            'security.tokenstorage' => 'getSecurityTokenStorageService',
            'security.userchecker' => 'getSecurityUserCheckerService',
            'security.uservalueresolver' => 'getSecurityUserValueResolverService',
            'security.validator.userpassword' => 'getSecurityValidatorUserPasswordService',
            'sensiodistribution.securitychecker' => 'getSensioDistributionSecurityCheckerService',
            'sensiodistribution.securitychecker.command' => 'getSensioDistributionSecurityCheckerCommandService',
            'sensioframeworkextra.cache.listener' => 'getSensioFrameworkExtraCacheListenerService',
            'sensioframeworkextra.controller.listener' => 'getSensioFrameworkExtraControllerListenerService',
            'sensioframeworkextra.converter.datetime' => 'getSensioFrameworkExtraConverterDatetimeService',
            'sensioframeworkextra.converter.doctrine.orm' => 'getSensioFrameworkExtraConverterDoctrineOrmService',
            'sensioframeworkextra.converter.listener' => 'getSensioFrameworkExtraConverterListenerService',
            'sensioframeworkextra.converter.manager' => 'getSensioFrameworkExtraConverterManagerService',
            'sensioframeworkextra.security.listener' => 'getSensioFrameworkExtraSecurityListenerService',
            'sensioframeworkextra.view.guesser' => 'getSensioFrameworkExtraViewGuesserService',
            'sensioframeworkextra.view.listener' => 'getSensioFrameworkExtraViewListenerService',
            'servicelocator.e64d23c3bf770e2cf44b71643280668d' => 'getServiceLocatorE64d23c3bf770e2cf44b71643280668dService',
            'session' => 'getSessionService',
            'session.handler' => 'getSessionHandlerService',
            'session.savelistener' => 'getSessionSaveListenerService',
            'session.storage.filesystem' => 'getSessionStorageFilesystemService',
            'session.storage.metadatabag' => 'getSessionStorageMetadataBagService',
            'session.storage.native' => 'getSessionStorageNativeService',
            'session.storage.phpbridge' => 'getSessionStoragePhpBridgeService',
            'sessionlistener' => 'getSessionListenerService',
            'sonata.admin.audit.manager' => 'getSonataAdminAuditManagerService',
            'sonata.admin.block.adminlist' => 'getSonataAdminBlockAdminListService',
            'sonata.admin.block.searchresult' => 'getSonataAdminBlockSearchResultService',
            'sonata.admin.block.stats' => 'getSonataAdminBlockStatsService',
            'sonata.admin.breadcrumbsbuilder' => 'getSonataAdminBreadcrumbsBuilderService',
            'sonata.admin.builder.filter.factory' => 'getSonataAdminBuilderFilterFactoryService',
            'sonata.admin.builder.ormdatagrid' => 'getSonataAdminBuilderOrmDatagridService',
            'sonata.admin.builder.ormform' => 'getSonataAdminBuilderOrmFormService',
            'sonata.admin.builder.ormlist' => 'getSonataAdminBuilderOrmListService',
            'sonata.admin.builder.ormshow' => 'getSonataAdminBuilderOrmShowService',
            'sonata.admin.controller.admin' => 'getSonataAdminControllerAdminService',
            'sonata.admin.doctrineorm.form.type.choicefieldmask' => 'getSonataAdminDoctrineOrmFormTypeChoiceFieldMaskService',
            'sonata.admin.event.extension' => 'getSonataAdminEventExtensionService',
            'sonata.admin.exporter' => 'getSonataAdminExporterService',
            'sonata.admin.form.extension.choice' => 'getSonataAdminFormExtensionChoiceService',
            'sonata.admin.form.extension.field' => 'getSonataAdminFormExtensionFieldService',
            'sonata.admin.form.extension.field.mopa' => 'getSonataAdminFormExtensionFieldMopaService',
            'sonata.admin.form.filter.type.choice' => 'getSonataAdminFormFilterTypeChoiceService',
            'sonata.admin.form.filter.type.date' => 'getSonataAdminFormFilterTypeDateService',
            'sonata.admin.form.filter.type.daterange' => 'getSonataAdminFormFilterTypeDaterangeService',
            'sonata.admin.form.filter.type.datetime' => 'getSonataAdminFormFilterTypeDatetimeService',
            'sonata.admin.form.filter.type.datetimerange' => 'getSonataAdminFormFilterTypeDatetimeRangeService',
            'sonata.admin.form.filter.type.default' => 'getSonataAdminFormFilterTypeDefaultService',
            'sonata.admin.form.filter.type.number' => 'getSonataAdminFormFilterTypeNumberService',
            'sonata.admin.form.type.admin' => 'getSonataAdminFormTypeAdminService',
            'sonata.admin.form.type.collection' => 'getSonataAdminFormTypeCollectionService',
            'sonata.admin.form.type.modelautocomplete' => 'getSonataAdminFormTypeModelAutocompleteService',
            'sonata.admin.form.type.modelchoice' => 'getSonataAdminFormTypeModelChoiceService',
            'sonata.admin.form.type.modelhidden' => 'getSonataAdminFormTypeModelHiddenService',
            'sonata.admin.form.type.modellist' => 'getSonataAdminFormTypeModelListService',
            'sonata.admin.form.type.modelreference' => 'getSonataAdminFormTypeModelReferenceService',
            'sonata.admin.guesser.ormdatagrid' => 'getSonataAdminGuesserOrmDatagridService',
            'sonata.admin.guesser.ormdatagridchain' => 'getSonataAdminGuesserOrmDatagridChainService',
            'sonata.admin.guesser.ormlist' => 'getSonataAdminGuesserOrmListService',
            'sonata.admin.guesser.ormlistchain' => 'getSonataAdminGuesserOrmListChainService',
            'sonata.admin.guesser.ormshow' => 'getSonataAdminGuesserOrmShowService',
            'sonata.admin.guesser.ormshowchain' => 'getSonataAdminGuesserOrmShowChainService',
            'sonata.admin.helper' => 'getSonataAdminHelperService',
            'sonata.admin.label.strategy.bc' => 'getSonataAdminLabelStrategyBcService',
            'sonata.admin.label.strategy.formcomponent' => 'getSonataAdminLabelStrategyFormComponentService',
            'sonata.admin.label.strategy.native' => 'getSonataAdminLabelStrategyNativeService',
            'sonata.admin.label.strategy.noop' => 'getSonataAdminLabelStrategyNoopService',
            'sonata.admin.label.strategy.underscore' => 'getSonataAdminLabelStrategyUnderscoreService',
            'sonata.admin.manager.orm' => 'getSonataAdminManagerOrmService',
            'sonata.admin.manipulator.acl.admin' => 'getSonataAdminManipulatorAclAdminService',
            'sonata.admin.manipulator.acl.object.orm' => 'getSonataAdminManipulatorAclObjectOrmService',
            'sonata.admin.menu.matcher.voter.active' => 'getSonataAdminMenuMatcherVoterActiveService',
            'sonata.admin.menu.matcher.voter.admin' => 'getSonataAdminMenuMatcherVoterAdminService',
            'sonata.admin.menu.matcher.voter.children' => 'getSonataAdminMenuMatcherVoterChildrenService',
            'sonata.admin.menubuilder' => 'getSonataAdminMenuBuilderService',
            'sonata.admin.object.manipulator.acl.admin' => 'getSonataAdminObjectManipulatorAclAdminService',
            'sonata.admin.orm.filter.type.boolean' => 'getSonataAdminOrmFilterTypeBooleanService',
            'sonata.admin.orm.filter.type.callback' => 'getSonataAdminOrmFilterTypeCallbackService',
            'sonata.admin.orm.filter.type.choice' => 'getSonataAdminOrmFilterTypeChoiceService',
            'sonata.admin.orm.filter.type.class' => 'getSonataAdminOrmFilterTypeClassService',
            'sonata.admin.orm.filter.type.date' => 'getSonataAdminOrmFilterTypeDateService',
            'sonata.admin.orm.filter.type.daterange' => 'getSonataAdminOrmFilterTypeDateRangeService',
            'sonata.admin.orm.filter.type.datetime' => 'getSonataAdminOrmFilterTypeDatetimeService',
            'sonata.admin.orm.filter.type.datetimerange' => 'getSonataAdminOrmFilterTypeDatetimeRangeService',
            'sonata.admin.orm.filter.type.model' => 'getSonataAdminOrmFilterTypeModelService',
            'sonata.admin.orm.filter.type.modelautocomplete' => 'getSonataAdminOrmFilterTypeModelAutocompleteService',
            'sonata.admin.orm.filter.type.number' => 'getSonataAdminOrmFilterTypeNumberService',
            'sonata.admin.orm.filter.type.string' => 'getSonataAdminOrmFilterTypeStringService',
            'sonata.admin.orm.filter.type.time' => 'getSonataAdminOrmFilterTypeTimeService',
            'sonata.admin.pool' => 'getSonataAdminPoolService',
            'sonata.admin.route.cache' => 'getSonataAdminRouteCacheService',
            'sonata.admin.route.cachewarmup' => 'getSonataAdminRouteCacheWarmupService',
            'sonata.admin.route.defaultgenerator' => 'getSonataAdminRouteDefaultGeneratorService',
            'sonata.admin.route.pathinfo' => 'getSonataAdminRoutePathInfoService',
            'sonata.admin.route.querystring' => 'getSonataAdminRouteQueryStringService',
            'sonata.admin.routeloader' => 'getSonataAdminRouteLoaderService',
            'sonata.admin.search.handler' => 'getSonataAdminSearchHandlerService',
            'sonata.admin.security.handler' => 'getSonataAdminSecurityHandlerService',
            'sonata.admin.sidebarmenu' => 'getSonataAdminSidebarMenuService',
            'sonata.admin.twig.extension' => 'getSonataAdminTwigExtensionService',
            'sonata.admin.twig.global' => 'getSonataAdminTwigGlobalService',
            'sonata.admin.validator.inline' => 'getSonataAdminValidatorInlineService',
            'sonata.block.cache.handler.default' => 'getSonataBlockCacheHandlerDefaultService',
            'sonata.block.cache.handler.noop' => 'getSonataBlockCacheHandlerNoopService',
            'sonata.block.contextmanager.default' => 'getSonataBlockContextManagerDefaultService',
            'sonata.block.exception.filter.debugonly' => 'getSonataBlockExceptionFilterDebugOnlyService',
            'sonata.block.exception.filter.ignoreblockexception' => 'getSonataBlockExceptionFilterIgnoreBlockExceptionService',
            'sonata.block.exception.filter.keepall' => 'getSonataBlockExceptionFilterKeepAllService',
            'sonata.block.exception.filter.keepnone' => 'getSonataBlockExceptionFilterKeepNoneService',
            'sonata.block.exception.renderer.inline' => 'getSonataBlockExceptionRendererInlineService',
            'sonata.block.exception.renderer.inlinedebug' => 'getSonataBlockExceptionRendererInlineDebugService',
            'sonata.block.exception.renderer.throw' => 'getSonataBlockExceptionRendererThrowService',
            'sonata.block.exception.strategy.manager' => 'getSonataBlockExceptionStrategyManagerService',
            'sonata.block.form.type.block' => 'getSonataBlockFormTypeBlockService',
            'sonata.block.form.type.containertemplate' => 'getSonataBlockFormTypeContainerTemplateService',
            'sonata.block.loader.chain' => 'getSonataBlockLoaderChainService',
            'sonata.block.loader.service' => 'getSonataBlockLoaderServiceService',
            'sonata.block.manager' => 'getSonataBlockManagerService',
            'sonata.block.menu.registry' => 'getSonataBlockMenuRegistryService',
            'sonata.block.renderer.default' => 'getSonataBlockRendererDefaultService',
            'sonata.block.service.container' => 'getSonataBlockServiceContainerService',
            'sonata.block.service.empty' => 'getSonataBlockServiceEmptyService',
            'sonata.block.service.menu' => 'getSonataBlockServiceMenuService',
            'sonata.block.service.rss' => 'getSonataBlockServiceRssService',
            'sonata.block.service.template' => 'getSonataBlockServiceTemplateService',
            'sonata.block.service.text' => 'getSonataBlockServiceTextService',
            'sonata.block.templating.helper' => 'getSonataBlockTemplatingHelperService',
            'sonata.block.twig.global' => 'getSonataBlockTwigGlobalService',
            'sonata.core.date.momentformatconverter' => 'getSonataCoreDateMomentFormatConverterService',
            'sonata.core.flashmessage.manager' => 'getSonataCoreFlashmessageManagerService',
            'sonata.core.flashmessage.twig.extension' => 'getSonataCoreFlashmessageTwigExtensionService',
            'sonata.core.form.type.array' => 'getSonataCoreFormTypeArrayService',
            'sonata.core.form.type.boolean' => 'getSonataCoreFormTypeBooleanService',
            'sonata.core.form.type.collection' => 'getSonataCoreFormTypeCollectionService',
            'sonata.core.form.type.colorselector' => 'getSonataCoreFormTypeColorSelectorService',
            'sonata.core.form.type.datepicker' => 'getSonataCoreFormTypeDatePickerService',
            'sonata.core.form.type.daterange' => 'getSonataCoreFormTypeDateRangeService',
            'sonata.core.form.type.daterangepicker' => 'getSonataCoreFormTypeDateRangePickerService',
            'sonata.core.form.type.datetimepicker' => 'getSonataCoreFormTypeDatetimePickerService',
            'sonata.core.form.type.datetimerange' => 'getSonataCoreFormTypeDatetimeRangeService',
            'sonata.core.form.type.datetimerangepicker' => 'getSonataCoreFormTypeDatetimeRangePickerService',
            'sonata.core.form.type.equal' => 'getSonataCoreFormTypeEqualService',
            'sonata.core.form.type.translatablechoice' => 'getSonataCoreFormTypeTranslatableChoiceService',
            'sonata.core.model.adapter.chain' => 'getSonataCoreModelAdapterChainService',
            'sonata.core.slugify.cocur' => 'getSonataCoreSlugifyCocurService',
            'sonata.core.slugify.native' => 'getSonataCoreSlugifyNativeService',
            'sonata.core.twig.extension.text' => 'getSonataCoreTwigExtensionTextService',
            'sonata.core.twig.extension.wrapping' => 'getSonataCoreTwigExtensionWrappingService',
            'sonata.core.twig.statusextension' => 'getSonataCoreTwigStatusExtensionService',
            'sonata.core.twig.templateextension' => 'getSonataCoreTwigTemplateExtensionService',
            'sonata.core.validator.inline' => 'getSonataCoreValidatorInlineService',
            'streamedresponselistener' => 'getStreamedResponseListenerService',
            'swiftmailer.emailsender.listener' => 'getSwiftmailerEmailSenderListenerService',
            'swiftmailer.mailer.default' => 'getSwiftmailerMailerDefaultService',
            'swiftmailer.mailer.default.plugin.messagelogger' => 'getSwiftmailerMailerDefaultPluginMessageloggerService',
            'swiftmailer.mailer.default.spool' => 'getSwiftmailerMailerDefaultSpoolService',
            'swiftmailer.mailer.default.transport' => 'getSwiftmailerMailerDefaultTransportService',
            'swiftmailer.mailer.default.transport.eventdispatcher' => 'getSwiftmailerMailerDefaultTransportEventdispatcherService',
            'swiftmailer.mailer.default.transport.real' => 'getSwiftmailerMailerDefaultTransportRealService',
            'templating' => 'getTemplatingService',
            'templating.filenameparser' => 'getTemplatingFilenameParserService',
            'templating.helper.logouturl' => 'getTemplatingHelperLogoutUrlService',
            'templating.helper.security' => 'getTemplatingHelperSecurityService',
            'templating.loader' => 'getTemplatingLoaderService',
            'templating.locator' => 'getTemplatingLocatorService',
            'templating.nameparser' => 'getTemplatingNameParserService',
            'translation.dumper.csv' => 'getTranslationDumperCsvService',
            'translation.dumper.ini' => 'getTranslationDumperIniService',
            'translation.dumper.json' => 'getTranslationDumperJsonService',
            'translation.dumper.mo' => 'getTranslationDumperMoService',
            'translation.dumper.php' => 'getTranslationDumperPhpService',
            'translation.dumper.po' => 'getTranslationDumperPoService',
            'translation.dumper.qt' => 'getTranslationDumperQtService',
            'translation.dumper.res' => 'getTranslationDumperResService',
            'translation.dumper.xliff' => 'getTranslationDumperXliffService',
            'translation.dumper.yml' => 'getTranslationDumperYmlService',
            'translation.extractor' => 'getTranslationExtractorService',
            'translation.extractor.php' => 'getTranslationExtractorPhpService',
            'translation.loader' => 'getTranslationLoaderService',
            'translation.loader.csv' => 'getTranslationLoaderCsvService',
            'translation.loader.dat' => 'getTranslationLoaderDatService',
            'translation.loader.ini' => 'getTranslationLoaderIniService',
            'translation.loader.json' => 'getTranslationLoaderJsonService',
            'translation.loader.mo' => 'getTranslationLoaderMoService',
            'translation.loader.php' => 'getTranslationLoaderPhpService',
            'translation.loader.po' => 'getTranslationLoaderPoService',
            'translation.loader.qt' => 'getTranslationLoaderQtService',
            'translation.loader.res' => 'getTranslationLoaderResService',
            'translation.loader.xliff' => 'getTranslationLoaderXliffService',
            'translation.loader.yml' => 'getTranslationLoaderYmlService',
            'translation.writer' => 'getTranslationWriterService',
            'translator' => 'getTranslatorService',
            'translator.default' => 'getTranslatorDefaultService',
            'translatorlistener' => 'getTranslatorListenerService',
            'twig' => 'getTwigService',
            'twig.controller.exception' => 'getTwigControllerExceptionService',
            'twig.controller.previewerror' => 'getTwigControllerPreviewErrorService',
            'twig.exceptionlistener' => 'getTwigExceptionListenerService',
            'twig.form.renderer' => 'getTwigFormRendererService',
            'twig.loader' => 'getTwigLoaderService',
            'twig.profile' => 'getTwigProfileService',
            'twig.runtime.httpkernel' => 'getTwigRuntimeHttpkernelService',
            'twig.translation.extractor' => 'getTwigTranslationExtractorService',
            'urisigner' => 'getUriSignerService',
            'validaterequestlistener' => 'getValidateRequestListenerService',
            'validator' => 'getValidatorService',
            'validator.builder' => 'getValidatorBuilderService',
            'validator.email' => 'getValidatorEmailService',
            'validator.expression' => 'getValidatorExpressionService',
            'validator.validatorfactory' => 'getValidatorValidatorFactoryService',
            'vardumper.clidumper' => 'getVarDumperCliDumperService',
            'vardumper.cloner' => 'getVarDumperClonerService',
            'webprofiler.controller.exception' => 'getWebProfilerControllerExceptionService',
            'webprofiler.controller.profiler' => 'getWebProfilerControllerProfilerService',
            'webprofiler.controller.router' => 'getWebProfilerControllerRouterService',
            'webprofiler.csp.handler' => 'getWebProfilerCspHandlerService',
            'webprofiler.debugtoolbar' => 'getWebProfilerDebugToolbarService',
        );
        $this->privates = array(
            '10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' => true,
            '20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' => true,
            'annotations.reader' => true,
            'argumentresolver.default' => true,
            'argumentresolver.request' => true,
            'argumentresolver.requestattribute' => true,
            'argumentresolver.service' => true,
            'argumentresolver.session' => true,
            'argumentresolver.variadic' => true,
            'cache.annotations' => true,
            'cache.annotations.recorderinner' => true,
            'cache.app.recorderinner' => true,
            'cache.serializer.recorderinner' => true,
            'cache.system.recorderinner' => true,
            'cache.validator' => true,
            'cache.validator.recorderinner' => true,
            'console.errorlistener' => true,
            'controllernameconverter' => true,
            'debug.filelinkformatter' => true,
            'debug.logprocessor' => true,
            'debug.security.access.decisionmanager' => true,
            'doctrine.dbal.logger.profiling.default' => true,
            'form.serverparams' => true,
            'fosuser.userprovider.usernameemail' => true,
            'fosuser.util.canonicalfieldsupdater' => true,
            'fosuser.util.passwordupdater' => true,
            'resolvecontrollernamesubscriber' => true,
            'router.requestcontext' => true,
            'security.access.authenticatedvoter' => true,
            'security.access.expressionvoter' => true,
            'security.access.rolehierarchyvoter' => true,
            'security.authentication.manager' => true,
            'security.authentication.provider.anonymous.main' => true,
            'security.authentication.provider.dao.main' => true,
            'security.authentication.sessionstrategy' => true,
            'security.authentication.trustresolver' => true,
            'security.firewall.map' => true,
            'security.logouturlgenerator' => true,
            'security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db' => true,
            'security.rolehierarchy' => true,
            'security.userchecker' => true,
            'security.uservalueresolver' => true,
            'servicelocator.e64d23c3bf770e2cf44b71643280668d' => true,
            'session.storage.metadatabag' => true,
            'sonata.block.manager' => true,
            'swiftmailer.mailer.default.transport.eventdispatcher' => true,
            'templating.locator' => true,
            'validator.validatorfactory' => true,
            'webprofiler.csp.handler' => true,
        );
        $this->aliases = array(
            'cache.appclearer' => 'cache.defaultclearer',
            'databaseconnection' => 'doctrine.dbal.defaultconnection',
            'doctrine.orm.defaultmetadatacache' => 'doctrinecache.providers.doctrine.orm.defaultmetadatacache',
            'doctrine.orm.defaultquerycache' => 'doctrinecache.providers.doctrine.orm.defaultquerycache',
            'doctrine.orm.defaultresultcache' => 'doctrinecache.providers.doctrine.orm.defaultresultcache',
            'doctrine.orm.entitymanager' => 'doctrine.orm.defaultentitymanager',
            'eventdispatcher' => 'debug.eventdispatcher',
            'fosuser.util.usernamecanonicalizer' => 'fosuser.util.emailcanonicalizer',
            'mailer' => 'swiftmailer.mailer.default',
            'session.storage' => 'session.storage.native',
            'sonata.block.cache.handler' => 'sonata.block.cache.handler.default',
            'sonata.block.contextmanager' => 'sonata.block.contextmanager.default',
            'sonata.block.renderer' => 'sonata.block.renderer.default',
            'swiftmailer.mailer' => 'swiftmailer.mailer.default',
            'swiftmailer.plugin.messagelogger' => 'swiftmailer.mailer.default.plugin.messagelogger',
            'swiftmailer.spool' => 'swiftmailer.mailer.default.spool',
            'swiftmailer.transport' => 'swiftmailer.mailer.default.transport',
            'swiftmailer.transport.real' => 'swiftmailer.mailer.default.transport.real',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function compile()
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    /**
     * {@inheritdoc}
     */
    public function isCompiled()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isFrozen()
    {
        @triggererror(sprintf('The %s() method is deprecated since version 3.3 and will be removed in 4.0. Use the isCompiled() method instead.', METHOD), EUSERDEPRECATED);

        return true;
    }

    /**
     * Gets the public 'annotationreader' shared service.
     *
     * @return \Doctrine\Common\Annotations\CachedReader
     */
    protected function getAnnotationReaderService()
    {
        return $this->services['annotationreader'] = new \Doctrine\Common\Annotations\CachedReader(${($ = isset($this->services['annotations.reader']) ? $this->services['annotations.reader'] : $this->getAnnotationsReaderService()) && false ?: ''}, new \Symfony\Component\Cache\DoctrineProvider(\Symfony\Component\Cache\Adapter\PhpArrayAdapter::create((DIR.'/annotations.php'), ${($ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCacheAnnotationsService()) && false ?: ''})), true);
    }

    /**
     * Gets the public 'app.admin.office' service.
     *
     * @return \AppBundle\Admin\OfficeAdmin
     */
    protected function getAppAdminOfficeService()
    {
        $instance = new \AppBundle\Admin\OfficeAdmin('app.admin.office', 'AppBundle\\Entity\\Office', 'SonataAdminBundle:CRUD');

        $instance->setFormTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig'));
        $instance->setFilterTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:filteradminfields.html.twig'));
        $instance->setManagerType('orm');
        $instance->setModelManager(${($ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->get('sonata.admin.manager.orm')) && false ?: ''});
        $instance->setFormContractor(${($ = isset($this->services['sonata.admin.builder.ormform']) ? $this->services['sonata.admin.builder.ormform'] : $this->get('sonata.admin.builder.ormform')) && false ?: ''});
        $instance->setShowBuilder(${($ = isset($this->services['sonata.admin.builder.ormshow']) ? $this->services['sonata.admin.builder.ormshow'] : $this->get('sonata.admin.builder.ormshow')) && false ?: ''});
        $instance->setListBuilder(${($ = isset($this->services['sonata.admin.builder.ormlist']) ? $this->services['sonata.admin.builder.ormlist'] : $this->get('sonata.admin.builder.ormlist')) && false ?: ''});
        $instance->setDatagridBuilder(${($ = isset($this->services['sonata.admin.builder.ormdatagrid']) ? $this->services['sonata.admin.builder.ormdatagrid'] : $this->get('sonata.admin.builder.ormdatagrid')) && false ?: ''});
        $instance->setTranslator(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, false);
        $instance->setConfigurationPool(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
        $instance->setRouteGenerator(${($ = isset($this->services['sonata.admin.route.defaultgenerator']) ? $this->services['sonata.admin.route.defaultgenerator'] : $this->get('sonata.admin.route.defaultgenerator')) && false ?: ''});
        $instance->setValidator(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
        $instance->setSecurityHandler(${($ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : $this->get('sonata.admin.security.handler')) && false ?: ''});
        $instance->setMenuFactory(${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''});
        $instance->setRouteBuilder(${($ = isset($this->services['sonata.admin.route.pathinfo']) ? $this->services['sonata.admin.route.pathinfo'] : $this->get('sonata.admin.route.pathinfo')) && false ?: ''});
        $instance->setLabelTranslatorStrategy(${($ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : $this->get('sonata.admin.label.strategy.native')) && false ?: ''});
        $instance->setPagerType('default');
        $instance->setLabel('Office');
        $instance->setPersistFilters(false);
        $instance->showMosaicButton(true);
        $instance->setTemplates(array('userblock' => 'SonataAdminBundle:Core:userblock.html.twig', 'addblock' => 'SonataAdminBundle:Core:addblock.html.twig', 'layout' => 'SonataAdminBundle::standardlayout.html.twig', 'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig', 'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig', 'list' => 'SonataAdminBundle:CRUD:list.html.twig', 'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig', 'show' => 'SonataAdminBundle:CRUD:show.html.twig', 'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig', 'edit' => 'SonataAdminBundle:CRUD:edit.html.twig', 'history' => 'SonataAdminBundle:CRUD:history.html.twig', 'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig', 'acl' => 'SonataAdminBundle:CRUD:acl.html.twig', 'action' => 'SonataAdminBundle:CRUD:action.html.twig', 'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig', 'preview' => 'SonataAdminBundle:CRUD:preview.html.twig', 'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig', 'delete' => 'SonataAdminBundle:CRUD:delete.html.twig', 'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig', 'select' => 'SonataAdminBundle:CRUD:listselect.html.twig', 'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig', 'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig', 'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig', 'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig', 'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig', 'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig', 'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig', 'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig', 'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig', 'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig', 'search' => 'SonataAdminBundle:Core:search.html.twig', 'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig', 'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig', 'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig', 'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig', 'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig', 'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig', 'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig', 'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig'));
        $instance->setSecurityInformation(array());
        $instance->initialize();
        $instance->addExtension(${($ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->get('sonata.admin.event.extension')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'app.admin.partner' service.
     *
     * @return \AppBundle\Admin\PartnerAdmin
     */
    protected function getAppAdminPartnerService()
    {
        $instance = new \AppBundle\Admin\PartnerAdmin('app.admin.partner', 'AppBundle\\Entity\\Partner', 'SonataAdminBundle:CRUD');

        $instance->setFormTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig'));
        $instance->setFilterTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:filteradminfields.html.twig'));
        $instance->setManagerType('orm');
        $instance->setModelManager(${($ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->get('sonata.admin.manager.orm')) && false ?: ''});
        $instance->setFormContractor(${($ = isset($this->services['sonata.admin.builder.ormform']) ? $this->services['sonata.admin.builder.ormform'] : $this->get('sonata.admin.builder.ormform')) && false ?: ''});
        $instance->setShowBuilder(${($ = isset($this->services['sonata.admin.builder.ormshow']) ? $this->services['sonata.admin.builder.ormshow'] : $this->get('sonata.admin.builder.ormshow')) && false ?: ''});
        $instance->setListBuilder(${($ = isset($this->services['sonata.admin.builder.ormlist']) ? $this->services['sonata.admin.builder.ormlist'] : $this->get('sonata.admin.builder.ormlist')) && false ?: ''});
        $instance->setDatagridBuilder(${($ = isset($this->services['sonata.admin.builder.ormdatagrid']) ? $this->services['sonata.admin.builder.ormdatagrid'] : $this->get('sonata.admin.builder.ormdatagrid')) && false ?: ''});
        $instance->setTranslator(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, false);
        $instance->setConfigurationPool(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
        $instance->setRouteGenerator(${($ = isset($this->services['sonata.admin.route.defaultgenerator']) ? $this->services['sonata.admin.route.defaultgenerator'] : $this->get('sonata.admin.route.defaultgenerator')) && false ?: ''});
        $instance->setValidator(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
        $instance->setSecurityHandler(${($ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : $this->get('sonata.admin.security.handler')) && false ?: ''});
        $instance->setMenuFactory(${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''});
        $instance->setRouteBuilder(${($ = isset($this->services['sonata.admin.route.pathinfo']) ? $this->services['sonata.admin.route.pathinfo'] : $this->get('sonata.admin.route.pathinfo')) && false ?: ''});
        $instance->setLabelTranslatorStrategy(${($ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : $this->get('sonata.admin.label.strategy.native')) && false ?: ''});
        $instance->setPagerType('default');
        $instance->setLabel('Partner');
        $instance->setPersistFilters(false);
        $instance->showMosaicButton(true);
        $instance->setTemplates(array('userblock' => 'SonataAdminBundle:Core:userblock.html.twig', 'addblock' => 'SonataAdminBundle:Core:addblock.html.twig', 'layout' => 'SonataAdminBundle::standardlayout.html.twig', 'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig', 'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig', 'list' => 'SonataAdminBundle:CRUD:list.html.twig', 'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig', 'show' => 'SonataAdminBundle:CRUD:show.html.twig', 'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig', 'edit' => 'SonataAdminBundle:CRUD:edit.html.twig', 'history' => 'SonataAdminBundle:CRUD:history.html.twig', 'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig', 'acl' => 'SonataAdminBundle:CRUD:acl.html.twig', 'action' => 'SonataAdminBundle:CRUD:action.html.twig', 'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig', 'preview' => 'SonataAdminBundle:CRUD:preview.html.twig', 'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig', 'delete' => 'SonataAdminBundle:CRUD:delete.html.twig', 'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig', 'select' => 'SonataAdminBundle:CRUD:listselect.html.twig', 'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig', 'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig', 'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig', 'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig', 'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig', 'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig', 'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig', 'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig', 'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig', 'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig', 'search' => 'SonataAdminBundle:Core:search.html.twig', 'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig', 'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig', 'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig', 'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig', 'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig', 'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig', 'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig', 'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig'));
        $instance->setSecurityInformation(array());
        $instance->initialize();
        $instance->addExtension(${($ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->get('sonata.admin.event.extension')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'app.admin.region' service.
     *
     * @return \AppBundle\Admin\RegionAdmin
     */
    protected function getAppAdminRegionService()
    {
        $instance = new \AppBundle\Admin\RegionAdmin('app.admin.region', 'AppBundle\\Entity\\Region', 'SonataAdminBundle:CRUD');

        $instance->setFormTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig'));
        $instance->setFilterTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:filteradminfields.html.twig'));
        $instance->setManagerType('orm');
        $instance->setModelManager(${($ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->get('sonata.admin.manager.orm')) && false ?: ''});
        $instance->setFormContractor(${($ = isset($this->services['sonata.admin.builder.ormform']) ? $this->services['sonata.admin.builder.ormform'] : $this->get('sonata.admin.builder.ormform')) && false ?: ''});
        $instance->setShowBuilder(${($ = isset($this->services['sonata.admin.builder.ormshow']) ? $this->services['sonata.admin.builder.ormshow'] : $this->get('sonata.admin.builder.ormshow')) && false ?: ''});
        $instance->setListBuilder(${($ = isset($this->services['sonata.admin.builder.ormlist']) ? $this->services['sonata.admin.builder.ormlist'] : $this->get('sonata.admin.builder.ormlist')) && false ?: ''});
        $instance->setDatagridBuilder(${($ = isset($this->services['sonata.admin.builder.ormdatagrid']) ? $this->services['sonata.admin.builder.ormdatagrid'] : $this->get('sonata.admin.builder.ormdatagrid')) && false ?: ''});
        $instance->setTranslator(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, false);
        $instance->setConfigurationPool(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
        $instance->setRouteGenerator(${($ = isset($this->services['sonata.admin.route.defaultgenerator']) ? $this->services['sonata.admin.route.defaultgenerator'] : $this->get('sonata.admin.route.defaultgenerator')) && false ?: ''});
        $instance->setValidator(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
        $instance->setSecurityHandler(${($ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : $this->get('sonata.admin.security.handler')) && false ?: ''});
        $instance->setMenuFactory(${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''});
        $instance->setRouteBuilder(${($ = isset($this->services['sonata.admin.route.pathinfo']) ? $this->services['sonata.admin.route.pathinfo'] : $this->get('sonata.admin.route.pathinfo')) && false ?: ''});
        $instance->setLabelTranslatorStrategy(${($ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : $this->get('sonata.admin.label.strategy.native')) && false ?: ''});
        $instance->setPagerType('default');
        $instance->setLabel('Region');
        $instance->setPersistFilters(false);
        $instance->showMosaicButton(true);
        $instance->setTemplates(array('userblock' => 'SonataAdminBundle:Core:userblock.html.twig', 'addblock' => 'SonataAdminBundle:Core:addblock.html.twig', 'layout' => 'SonataAdminBundle::standardlayout.html.twig', 'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig', 'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig', 'list' => 'SonataAdminBundle:CRUD:list.html.twig', 'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig', 'show' => 'SonataAdminBundle:CRUD:show.html.twig', 'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig', 'edit' => 'SonataAdminBundle:CRUD:edit.html.twig', 'history' => 'SonataAdminBundle:CRUD:history.html.twig', 'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig', 'acl' => 'SonataAdminBundle:CRUD:acl.html.twig', 'action' => 'SonataAdminBundle:CRUD:action.html.twig', 'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig', 'preview' => 'SonataAdminBundle:CRUD:preview.html.twig', 'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig', 'delete' => 'SonataAdminBundle:CRUD:delete.html.twig', 'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig', 'select' => 'SonataAdminBundle:CRUD:listselect.html.twig', 'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig', 'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig', 'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig', 'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig', 'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig', 'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig', 'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig', 'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig', 'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig', 'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig', 'search' => 'SonataAdminBundle:Core:search.html.twig', 'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig', 'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig', 'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig', 'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig', 'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig', 'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig', 'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig', 'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig'));
        $instance->setSecurityInformation(array());
        $instance->initialize();
        $instance->addExtension(${($ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->get('sonata.admin.event.extension')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'app.admin.user' service.
     *
     * @return \AppBundle\Admin\UserAdmin
     */
    protected function getAppAdminUserService()
    {
        $instance = new \AppBundle\Admin\UserAdmin('app.admin.user', 'AppBundle\\Entity\\User', 'SonataAdminBundle:CRUD');

        $instance->setFormTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig'));
        $instance->setFilterTheme(array(0 => 'SonataDoctrineORMAdminBundle:Form:filteradminfields.html.twig'));
        $instance->setManagerType('orm');
        $instance->setModelManager(${($ = isset($this->services['sonata.admin.manager.orm']) ? $this->services['sonata.admin.manager.orm'] : $this->get('sonata.admin.manager.orm')) && false ?: ''});
        $instance->setFormContractor(${($ = isset($this->services['sonata.admin.builder.ormform']) ? $this->services['sonata.admin.builder.ormform'] : $this->get('sonata.admin.builder.ormform')) && false ?: ''});
        $instance->setShowBuilder(${($ = isset($this->services['sonata.admin.builder.ormshow']) ? $this->services['sonata.admin.builder.ormshow'] : $this->get('sonata.admin.builder.ormshow')) && false ?: ''});
        $instance->setListBuilder(${($ = isset($this->services['sonata.admin.builder.ormlist']) ? $this->services['sonata.admin.builder.ormlist'] : $this->get('sonata.admin.builder.ormlist')) && false ?: ''});
        $instance->setDatagridBuilder(${($ = isset($this->services['sonata.admin.builder.ormdatagrid']) ? $this->services['sonata.admin.builder.ormdatagrid'] : $this->get('sonata.admin.builder.ormdatagrid')) && false ?: ''});
        $instance->setTranslator(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, false);
        $instance->setConfigurationPool(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
        $instance->setRouteGenerator(${($ = isset($this->services['sonata.admin.route.defaultgenerator']) ? $this->services['sonata.admin.route.defaultgenerator'] : $this->get('sonata.admin.route.defaultgenerator')) && false ?: ''});
        $instance->setValidator(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
        $instance->setSecurityHandler(${($ = isset($this->services['sonata.admin.security.handler']) ? $this->services['sonata.admin.security.handler'] : $this->get('sonata.admin.security.handler')) && false ?: ''});
        $instance->setMenuFactory(${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''});
        $instance->setRouteBuilder(${($ = isset($this->services['sonata.admin.route.pathinfo']) ? $this->services['sonata.admin.route.pathinfo'] : $this->get('sonata.admin.route.pathinfo')) && false ?: ''});
        $instance->setLabelTranslatorStrategy(${($ = isset($this->services['sonata.admin.label.strategy.native']) ? $this->services['sonata.admin.label.strategy.native'] : $this->get('sonata.admin.label.strategy.native')) && false ?: ''});
        $instance->setPagerType('default');
        $instance->setLabel('User');
        $instance->setPersistFilters(false);
        $instance->showMosaicButton(true);
        $instance->setTemplates(array('userblock' => 'SonataAdminBundle:Core:userblock.html.twig', 'addblock' => 'SonataAdminBundle:Core:addblock.html.twig', 'layout' => 'SonataAdminBundle::standardlayout.html.twig', 'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig', 'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig', 'list' => 'SonataAdminBundle:CRUD:list.html.twig', 'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig', 'show' => 'SonataAdminBundle:CRUD:show.html.twig', 'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig', 'edit' => 'SonataAdminBundle:CRUD:edit.html.twig', 'history' => 'SonataAdminBundle:CRUD:history.html.twig', 'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig', 'acl' => 'SonataAdminBundle:CRUD:acl.html.twig', 'action' => 'SonataAdminBundle:CRUD:action.html.twig', 'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig', 'preview' => 'SonataAdminBundle:CRUD:preview.html.twig', 'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig', 'delete' => 'SonataAdminBundle:CRUD:delete.html.twig', 'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig', 'select' => 'SonataAdminBundle:CRUD:listselect.html.twig', 'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig', 'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig', 'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig', 'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig', 'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig', 'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig', 'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig', 'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig', 'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig', 'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig', 'search' => 'SonataAdminBundle:Core:search.html.twig', 'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig', 'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig', 'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig', 'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig', 'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig', 'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig', 'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig', 'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig'));
        $instance->setSecurityInformation(array());
        $instance->initialize();
        $instance->addExtension(${($ = isset($this->services['sonata.admin.event.extension']) ? $this->services['sonata.admin.event.extension'] : $this->get('sonata.admin.event.extension')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'app.form.registration' shared service.
     *
     * @return \AppBundle\Form\RegistrationType
     */
    protected function getAppFormRegistrationService()
    {
        return $this->services['app.form.registration'] = new \AppBundle\Form\RegistrationType();
    }

    /**
     * Gets the public 'assets.context' shared service.
     *
     * @return \Symfony\Component\Asset\Context\RequestStackContext
     */
    protected function getAssetsContextService()
    {
        return $this->services['assets.context'] = new \Symfony\Component\Asset\Context\RequestStackContext(${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''});
    }

    /**
     * Gets the public 'assets.packages' shared service.
     *
     * @return \Symfony\Component\Asset\Packages
     */
    protected function getAssetsPackagesService()
    {
        return $this->services['assets.packages'] = new \Symfony\Component\Asset\Packages(new \Symfony\Component\Asset\PathPackage('', new \Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy(), ${($ = isset($this->services['assets.context']) ? $this->services['assets.context'] : $this->get('assets.context')) && false ?: ''}), array());
    }

    /**
     * Gets the public 'cache.app' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\TraceableAdapter
     */
    protected function getCacheAppService()
    {
        return $this->services['cache.app'] = new \Symfony\Component\Cache\Adapter\TraceableAdapter(${($ = isset($this->services['cache.app.recorderinner']) ? $this->services['cache.app.recorderinner'] : $this->getCacheAppRecorderInnerService()) && false ?: ''});
    }

    /**
     * Gets the public 'cache.defaultclearer' shared service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer
     */
    protected function getCacheDefaultClearerService()
    {
        return $this->services['cache.defaultclearer'] = new \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer(array('cache.app' => ${($ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: ''}, 'cache.system' => ${($ = isset($this->services['cache.system']) ? $this->services['cache.system'] : $this->get('cache.system')) && false ?: ''}, 'cache.validator' => ${($ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCacheValidatorService()) && false ?: ''}, 'cache.annotations' => ${($ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCacheAnnotationsService()) && false ?: ''}));
    }

    /**
     * Gets the public 'cache.globalclearer' shared service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer
     */
    protected function getCacheGlobalClearerService()
    {
        return $this->services['cache.globalclearer'] = new \Symfony\Component\HttpKernel\CacheClearer\Psr6CacheClearer(array('cache.app' => ${($ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: ''}, 'cache.system' => ${($ = isset($this->services['cache.system']) ? $this->services['cache.system'] : $this->get('cache.system')) && false ?: ''}, 'cache.validator' => ${($ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCacheValidatorService()) && false ?: ''}, 'cache.annotations' => ${($ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCacheAnnotationsService()) && false ?: ''}));
    }

    /**
     * Gets the public 'cache.system' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\TraceableAdapter
     */
    protected function getCacheSystemService()
    {
        return $this->services['cache.system'] = new \Symfony\Component\Cache\Adapter\TraceableAdapter(${($ = isset($this->services['cache.system.recorderinner']) ? $this->services['cache.system.recorderinner'] : $this->getCacheSystemRecorderInnerService()) && false ?: ''});
    }

    /**
     * Gets the public 'cacheclearer' shared service.
     *
     * @return \Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer
     */
    protected function getCacheClearerService()
    {
        return $this->services['cacheclearer'] = new \Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer(array(0 => ${($ = isset($this->services['cache.defaultclearer']) ? $this->services['cache.defaultclearer'] : $this->get('cache.defaultclearer')) && false ?: ''}));
    }

    /**
     * Gets the public 'cachewarmer' shared service.
     *
     * @return \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate
     */
    protected function getCacheWarmerService()
    {
        $a = ${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''};
        $b = ${($ = isset($this->services['templating.filenameparser']) ? $this->services['templating.filenameparser'] : $this->get('templating.filenameparser')) && false ?: ''};

        $c = new \Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplateFinder($a, $b, ($this->targetDirs[1].'/app/Resources'));

        return $this->services['cachewarmer'] = new \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate(array(0 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplatePathsCacheWarmer($c, ${($ = isset($this->services['templating.locator']) ? $this->services['templating.locator'] : $this->getTemplatingLocatorService()) && false ?: ''}), 1 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\ValidatorCacheWarmer(${($ = isset($this->services['validator.builder']) ? $this->services['validator.builder'] : $this->get('validator.builder')) && false ?: ''}, (DIR.'/validation.php'), ${($ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCacheValidatorService()) && false ?: ''}), 2 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\TranslationsCacheWarmer(${($ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->get('translator.default')) && false ?: ''}), 3 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\RouterCacheWarmer(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}), 4 => new \Symfony\Bundle\FrameworkBundle\CacheWarmer\AnnotationsCacheWarmer(${($ = isset($this->services['annotations.reader']) ? $this->services['annotations.reader'] : $this->getAnnotationsReaderService()) && false ?: ''}, (DIR.'/annotations.php'), ${($ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCacheAnnotationsService()) && false ?: ''}), 5 => new \Symfony\Bundle\TwigBundle\CacheWarmer\TemplateCacheCacheWarmer(new \Symfony\Component\DependencyInjection\ServiceLocator(array('twig' => function () {
            $f = function (\Twig\Environment $v) { return $v; }; return $f(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''});
        })), $c, array()), 6 => new \Symfony\Bundle\TwigBundle\CacheWarmer\TemplateCacheWarmer($this, new \Symfony\Bundle\TwigBundle\TemplateIterator($a, ($this->targetDirs[1].'/app'), array())), 7 => new \Symfony\Bridge\Doctrine\CacheWarmer\ProxyCacheWarmer(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''}), 8 => ${($ = isset($this->services['sonata.admin.route.cachewarmup']) ? $this->services['sonata.admin.route.cachewarmup'] : $this->get('sonata.admin.route.cachewarmup')) && false ?: ''}));
    }

    /**
     * Gets the public 'configcachefactory' shared service.
     *
     * @return \Symfony\Component\Config\ResourceCheckerConfigCacheFactory
     */
    protected function getConfigCacheFactoryService()
    {
        return $this->services['configcachefactory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory(new RewindableGenerator(function () {
            yield 0 => ${($ = isset($this->services['10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7']) ? $this->services['10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7'] : $this->get10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service()) && false ?: ''};
            yield 1 => ${($ = isset($this->services['20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7']) ? $this->services['20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7'] : $this->get20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service()) && false ?: ''};
        }, 2));
    }

    /**
     * Gets the public 'console.command.symfonybundlesecuritybundlecommanduserpasswordencodercommand' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand
     */
    protected function getConsoleCommandSymfonyBundleSecuritybundleCommandUserpasswordencodercommandService()
    {
        return $this->services['console.command.symfonybundlesecuritybundlecommanduserpasswordencodercommand'] = new \Symfony\Bundle\SecurityBundle\Command\UserPasswordEncoderCommand(${($ = isset($this->services['security.encoderfactory']) ? $this->services['security.encoderfactory'] : $this->get('security.encoderfactory')) && false ?: ''}, array(0 => 'FOS\\UserBundle\\Model\\UserInterface'));
    }

    /**
     * Gets the public 'console.command.symfonybundlewebserverbundlecommandserverruncommand' shared service.
     *
     * @return \Symfony\Bundle\WebServerBundle\Command\ServerRunCommand
     */
    protected function getConsoleCommandSymfonyBundleWebserverbundleCommandServerruncommandService()
    {
        return $this->services['console.command.symfonybundlewebserverbundlecommandserverruncommand'] = new \Symfony\Bundle\WebServerBundle\Command\ServerRunCommand(($this->targetDirs[1].'/public'), 'dev');
    }

    /**
     * Gets the public 'console.command.symfonybundlewebserverbundlecommandserverstartcommand' shared service.
     *
     * @return \Symfony\Bundle\WebServerBundle\Command\ServerStartCommand
     */
    protected function getConsoleCommandSymfonyBundleWebserverbundleCommandServerstartcommandService()
    {
        return $this->services['console.command.symfonybundlewebserverbundlecommandserverstartcommand'] = new \Symfony\Bundle\WebServerBundle\Command\ServerStartCommand(($this->targetDirs[1].'/public'), 'dev');
    }

    /**
     * Gets the public 'console.command.symfonybundlewebserverbundlecommandserverstatuscommand' shared service.
     *
     * @return \Symfony\Bundle\WebServerBundle\Command\ServerStatusCommand
     */
    protected function getConsoleCommandSymfonyBundleWebserverbundleCommandServerstatuscommandService()
    {
        return $this->services['console.command.symfonybundlewebserverbundlecommandserverstatuscommand'] = new \Symfony\Bundle\WebServerBundle\Command\ServerStatusCommand();
    }

    /**
     * Gets the public 'console.command.symfonybundlewebserverbundlecommandserverstopcommand' shared service.
     *
     * @return \Symfony\Bundle\WebServerBundle\Command\ServerStopCommand
     */
    protected function getConsoleCommandSymfonyBundleWebserverbundleCommandServerstopcommandService()
    {
        return $this->services['console.command.symfonybundlewebserverbundlecommandserverstopcommand'] = new \Symfony\Bundle\WebServerBundle\Command\ServerStopCommand();
    }

    /**
     * Gets the public 'datacollector.dump' shared service.
     *
     * @return \Symfony\Component\HttpKernel\DataCollector\DumpDataCollector
     */
    protected function getDataCollectorDumpService()
    {
        return $this->services['datacollector.dump'] = new \Symfony\Component\HttpKernel\DataCollector\DumpDataCollector(${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['debug.filelinkformatter']) ? $this->services['debug.filelinkformatter'] : $this->getDebugFileLinkFormatterService()) && false ?: ''}, 'UTF-8', ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, NULL);
    }

    /**
     * Gets the public 'datacollector.form' shared service.
     *
     * @return \Symfony\Component\Form\Extension\DataCollector\FormDataCollector
     */
    protected function getDataCollectorFormService()
    {
        return $this->services['datacollector.form'] = new \Symfony\Component\Form\Extension\DataCollector\FormDataCollector(${($ = isset($this->services['datacollector.form.extractor']) ? $this->services['datacollector.form.extractor'] : $this->get('datacollector.form.extractor')) && false ?: ''});
    }

    /**
     * Gets the public 'datacollector.form.extractor' shared service.
     *
     * @return \Symfony\Component\Form\Extension\DataCollector\FormDataExtractor
     */
    protected function getDataCollectorFormExtractorService()
    {
        return $this->services['datacollector.form.extractor'] = new \Symfony\Component\Form\Extension\DataCollector\FormDataExtractor();
    }

    /**
     * Gets the public 'datacollector.request' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\DataCollector\RequestDataCollector
     */
    protected function getDataCollectorRequestService()
    {
        return $this->services['datacollector.request'] = new \Symfony\Bundle\FrameworkBundle\DataCollector\RequestDataCollector();
    }

    /**
     * Gets the public 'datacollector.router' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\DataCollector\RouterDataCollector
     */
    protected function getDataCollectorRouterService()
    {
        return $this->services['datacollector.router'] = new \Symfony\Bundle\FrameworkBundle\DataCollector\RouterDataCollector();
    }

    /**
     * Gets the public 'datacollector.translation' shared service.
     *
     * @return \Symfony\Component\Translation\DataCollector\TranslationDataCollector
     */
    protected function getDataCollectorTranslationService()
    {
        return $this->services['datacollector.translation'] = new \Symfony\Component\Translation\DataCollector\TranslationDataCollector(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'debug.argumentresolver' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver
     */
    protected function getDebugArgumentResolverService()
    {
        return $this->services['debug.argumentresolver'] = new \Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver(new \Symfony\Component\HttpKernel\Controller\ArgumentResolver(new \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory(), new RewindableGenerator(function () {
            yield 0 => ${($ = isset($this->services['argumentresolver.requestattribute']) ? $this->services['argumentresolver.requestattribute'] : $this->getArgumentResolverRequestAttributeService()) && false ?: ''};
            yield 1 => ${($ = isset($this->services['argumentresolver.request']) ? $this->services['argumentresolver.request'] : $this->getArgumentResolverRequestService()) && false ?: ''};
            yield 2 => ${($ = isset($this->services['argumentresolver.session']) ? $this->services['argumentresolver.session'] : $this->getArgumentResolverSessionService()) && false ?: ''};
            yield 3 => ${($ = isset($this->services['security.uservalueresolver']) ? $this->services['security.uservalueresolver'] : $this->getSecurityUserValueResolverService()) && false ?: ''};
            yield 4 => ${($ = isset($this->services['argumentresolver.service']) ? $this->services['argumentresolver.service'] : $this->getArgumentResolverServiceService()) && false ?: ''};
            yield 5 => ${($ = isset($this->services['argumentresolver.default']) ? $this->services['argumentresolver.default'] : $this->getArgumentResolverDefaultService()) && false ?: ''};
            yield 6 => ${($ = isset($this->services['argumentresolver.variadic']) ? $this->services['argumentresolver.variadic'] : $this->getArgumentResolverVariadicService()) && false ?: ''};
        }, 7)), ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: ''});
    }

    /**
     * Gets the public 'debug.controllerresolver' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\TraceableControllerResolver
     */
    protected function getDebugControllerResolverService()
    {
        return $this->services['debug.controllerresolver'] = new \Symfony\Component\HttpKernel\Controller\TraceableControllerResolver(new \Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver($this, ${($ = isset($this->services['controllernameconverter']) ? $this->services['controllernameconverter'] : $this->getControllerNameConverterService()) && false ?: ''}, ${($ = isset($this->services['monolog.logger.request']) ? $this->services['monolog.logger.request'] : $this->get('monolog.logger.request', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}), ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: ''}, ${($ = isset($this->services['debug.argumentresolver']) ? $this->services['debug.argumentresolver'] : $this->get('debug.argumentresolver')) && false ?: ''});
    }

    /**
     * Gets the public 'debug.debughandlerslistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener
     */
    protected function getDebugDebugHandlersListenerService()
    {
        return $this->services['debug.debughandlerslistener'] = new \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener(NULL, ${($ = isset($this->services['monolog.logger.php']) ? $this->services['monolog.logger.php'] : $this->get('monolog.logger.php', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, -1, -1, true, ${($ = isset($this->services['debug.filelinkformatter']) ? $this->services['debug.filelinkformatter'] : $this->getDebugFileLinkFormatterService()) && false ?: ''}, true);
    }

    /**
     * Gets the public 'debug.dumplistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\DumpListener
     */
    protected function getDebugDumpListenerService()
    {
        return $this->services['debug.dumplistener'] = new \Symfony\Component\HttpKernel\EventListener\DumpListener(${($ = isset($this->services['vardumper.cloner']) ? $this->services['vardumper.cloner'] : $this->get('vardumper.cloner')) && false ?: ''}, ${($ = isset($this->services['vardumper.clidumper']) ? $this->services['vardumper.clidumper'] : $this->get('vardumper.clidumper')) && false ?: ''});
    }

    /**
     * Gets the public 'debug.eventdispatcher' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
     */
    protected function getDebugEventDispatcherService()
    {
        $this->services['debug.eventdispatcher'] = $instance = new \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher(new \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher($this), ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch')) && false ?: ''}, ${($ = isset($this->services['monolog.logger.event']) ? $this->services['monolog.logger.event'] : $this->get('monolog.logger.event', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});

        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['datacollector.router']) ? $this->services['datacollector.router'] : $this->get('datacollector.router')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['sonata.block.cache.handler.default']) ? $this->services['sonata.block.cache.handler.default'] : $this->get('sonata.block.cache.handler.default')) && false ?: ''};
        }, 1 => 'onKernelResponse'), 0);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['knpmenu.listener.voters']) ? $this->services['knpmenu.listener.voters'] : $this->get('knpmenu.listener.voters')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['responselistener']) ? $this->services['responselistener'] : $this->get('responselistener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['streamedresponselistener']) ? $this->services['streamedresponselistener'] : $this->get('streamedresponselistener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), -1024);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['localelistener']) ? $this->services['localelistener'] : $this->get('localelistener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 16);
        $instance->addListener('kernel.finishrequest', array(0 => function () {
            return ${($ = isset($this->services['localelistener']) ? $this->services['localelistener'] : $this->get('localelistener')) && false ?: ''};
        }, 1 => 'onKernelFinishRequest'), 0);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['validaterequestlistener']) ? $this->services['validaterequestlistener'] : $this->get('validaterequestlistener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 256);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['resolvecontrollernamesubscriber']) ? $this->services['resolvecontrollernamesubscriber'] : $this->getResolveControllerNameSubscriberService()) && false ?: ''};
        }, 1 => 'onKernelRequest'), 24);
        $instance->addListener('console.error', array(0 => function () {
            return ${($ = isset($this->services['console.errorlistener']) ? $this->services['console.errorlistener'] : $this->getConsoleErrorListenerService()) && false ?: ''};
        }, 1 => 'onConsoleError'), -128);
        $instance->addListener('console.terminate', array(0 => function () {
            return ${($ = isset($this->services['console.errorlistener']) ? $this->services['console.errorlistener'] : $this->getConsoleErrorListenerService()) && false ?: ''};
        }, 1 => 'onConsoleTerminate'), -128);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['sessionlistener']) ? $this->services['sessionlistener'] : $this->get('sessionlistener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 128);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['session.savelistener']) ? $this->services['session.savelistener'] : $this->get('session.savelistener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), -1000);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['fragment.listener']) ? $this->services['fragment.listener'] : $this->get('fragment.listener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 48);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['translatorlistener']) ? $this->services['translatorlistener'] : $this->get('translatorlistener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 10);
        $instance->addListener('kernel.finishrequest', array(0 => function () {
            return ${($ = isset($this->services['translatorlistener']) ? $this->services['translatorlistener'] : $this->get('translatorlistener')) && false ?: ''};
        }, 1 => 'onKernelFinishRequest'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['profilerlistener']) ? $this->services['profilerlistener'] : $this->get('profilerlistener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), -100);
        $instance->addListener('kernel.exception', array(0 => function () {
            return ${($ = isset($this->services['profilerlistener']) ? $this->services['profilerlistener'] : $this->get('profilerlistener')) && false ?: ''};
        }, 1 => 'onKernelException'), 0);
        $instance->addListener('kernel.terminate', array(0 => function () {
            return ${($ = isset($this->services['profilerlistener']) ? $this->services['profilerlistener'] : $this->get('profilerlistener')) && false ?: ''};
        }, 1 => 'onKernelTerminate'), -1024);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['datacollector.request']) ? $this->services['datacollector.request'] : $this->get('datacollector.request')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['datacollector.request']) ? $this->services['datacollector.request'] : $this->get('datacollector.request')) && false ?: ''};
        }, 1 => 'onKernelResponse'), 0);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['debug.debughandlerslistener']) ? $this->services['debug.debughandlerslistener'] : $this->get('debug.debughandlerslistener')) && false ?: ''};
        }, 1 => 'configure'), 2048);
        $instance->addListener('console.command', array(0 => function () {
            return ${($ = isset($this->services['debug.debughandlerslistener']) ? $this->services['debug.debughandlerslistener'] : $this->get('debug.debughandlerslistener')) && false ?: ''};
        }, 1 => 'configure'), 2048);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['routerlistener']) ? $this->services['routerlistener'] : $this->get('routerlistener')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 32);
        $instance->addListener('kernel.finishrequest', array(0 => function () {
            return ${($ = isset($this->services['routerlistener']) ? $this->services['routerlistener'] : $this->get('routerlistener')) && false ?: ''};
        }, 1 => 'onKernelFinishRequest'), 0);
        $instance->addListener('kernel.request', array(0 => function () {
            return ${($ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->get('security.firewall')) && false ?: ''};
        }, 1 => 'onKernelRequest'), 8);
        $instance->addListener('kernel.finishrequest', array(0 => function () {
            return ${($ = isset($this->services['security.firewall']) ? $this->services['security.firewall'] : $this->get('security.firewall')) && false ?: ''};
        }, 1 => 'onKernelFinishRequest'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['security.rememberme.responselistener']) ? $this->services['security.rememberme.responselistener'] : $this->get('security.rememberme.responselistener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), 0);
        $instance->addListener('kernel.exception', array(0 => function () {
            return ${($ = isset($this->services['twig.exceptionlistener']) ? $this->services['twig.exceptionlistener'] : $this->get('twig.exceptionlistener')) && false ?: ''};
        }, 1 => 'onKernelException'), -128);
        $instance->addListener('console.command', array(0 => function () {
            return ${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''};
        }, 1 => 'onCommand'), 255);
        $instance->addListener('console.terminate', array(0 => function () {
            return ${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''};
        }, 1 => 'onTerminate'), -255);
        $instance->addListener('kernel.exception', array(0 => function () {
            return ${($ = isset($this->services['swiftmailer.emailsender.listener']) ? $this->services['swiftmailer.emailsender.listener'] : $this->get('swiftmailer.emailsender.listener')) && false ?: ''};
        }, 1 => 'onException'), 0);
        $instance->addListener('kernel.terminate', array(0 => function () {
            return ${($ = isset($this->services['swiftmailer.emailsender.listener']) ? $this->services['swiftmailer.emailsender.listener'] : $this->get('swiftmailer.emailsender.listener')) && false ?: ''};
        }, 1 => 'onTerminate'), 0);
        $instance->addListener('console.error', array(0 => function () {
            return ${($ = isset($this->services['swiftmailer.emailsender.listener']) ? $this->services['swiftmailer.emailsender.listener'] : $this->get('swiftmailer.emailsender.listener')) && false ?: ''};
        }, 1 => 'onException'), 0);
        $instance->addListener('console.terminate', array(0 => function () {
            return ${($ = isset($this->services['swiftmailer.emailsender.listener']) ? $this->services['swiftmailer.emailsender.listener'] : $this->get('swiftmailer.emailsender.listener')) && false ?: ''};
        }, 1 => 'onTerminate'), 0);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.controller.listener']) ? $this->services['sensioframeworkextra.controller.listener'] : $this->get('sensioframeworkextra.controller.listener')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.converter.listener']) ? $this->services['sensioframeworkextra.converter.listener'] : $this->get('sensioframeworkextra.converter.listener')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.view.listener']) ? $this->services['sensioframeworkextra.view.listener'] : $this->get('sensioframeworkextra.view.listener')) && false ?: ''};
        }, 1 => 'onKernelController'), -128);
        $instance->addListener('kernel.view', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.view.listener']) ? $this->services['sensioframeworkextra.view.listener'] : $this->get('sensioframeworkextra.view.listener')) && false ?: ''};
        }, 1 => 'onKernelView'), 0);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.cache.listener']) ? $this->services['sensioframeworkextra.cache.listener'] : $this->get('sensioframeworkextra.cache.listener')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.cache.listener']) ? $this->services['sensioframeworkextra.cache.listener'] : $this->get('sensioframeworkextra.cache.listener')) && false ?: ''};
        }, 1 => 'onKernelResponse'), 0);
        $instance->addListener('kernel.controller', array(0 => function () {
            return ${($ = isset($this->services['sensioframeworkextra.security.listener']) ? $this->services['sensioframeworkextra.security.listener'] : $this->get('sensioframeworkextra.security.listener')) && false ?: ''};
        }, 1 => 'onKernelController'), 0);
        $instance->addListener('fosuser.security.implicitlogin', array(0 => function () {
            return ${($ = isset($this->services['fosuser.security.interactiveloginlistener']) ? $this->services['fosuser.security.interactiveloginlistener'] : $this->get('fosuser.security.interactiveloginlistener')) && false ?: ''};
        }, 1 => 'onImplicitLogin'), 0);
        $instance->addListener('security.interactivelogin', array(0 => function () {
            return ${($ = isset($this->services['fosuser.security.interactiveloginlistener']) ? $this->services['fosuser.security.interactiveloginlistener'] : $this->get('fosuser.security.interactiveloginlistener')) && false ?: ''};
        }, 1 => 'onSecurityInteractiveLogin'), 0);
        $instance->addListener('fosuser.registration.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.authentication']) ? $this->services['fosuser.listener.authentication'] : $this->get('fosuser.listener.authentication')) && false ?: ''};
        }, 1 => 'authenticate'), 0);
        $instance->addListener('fosuser.registration.confirmed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.authentication']) ? $this->services['fosuser.listener.authentication'] : $this->get('fosuser.listener.authentication')) && false ?: ''};
        }, 1 => 'authenticate'), 0);
        $instance->addListener('fosuser.resetting.reset.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.authentication']) ? $this->services['fosuser.listener.authentication'] : $this->get('fosuser.listener.authentication')) && false ?: ''};
        }, 1 => 'authenticate'), 0);
        $instance->addListener('fosuser.changepassword.edit.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.group.create.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.group.delete.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.group.edit.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.profile.edit.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.registration.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.resetting.reset.completed', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.flash']) ? $this->services['fosuser.listener.flash'] : $this->get('fosuser.listener.flash')) && false ?: ''};
        }, 1 => 'addSuccessFlash'), 0);
        $instance->addListener('fosuser.registration.success', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.emailconfirmation']) ? $this->services['fosuser.listener.emailconfirmation'] : $this->get('fosuser.listener.emailconfirmation')) && false ?: ''};
        }, 1 => 'onRegistrationSuccess'), 0);
        $instance->addListener('fosuser.resetting.reset.initialize', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.resetting']) ? $this->services['fosuser.listener.resetting'] : $this->get('fosuser.listener.resetting')) && false ?: ''};
        }, 1 => 'onResettingResetInitialize'), 0);
        $instance->addListener('fosuser.resetting.reset.success', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.resetting']) ? $this->services['fosuser.listener.resetting'] : $this->get('fosuser.listener.resetting')) && false ?: ''};
        }, 1 => 'onResettingResetSuccess'), 0);
        $instance->addListener('fosuser.resetting.reset.request', array(0 => function () {
            return ${($ = isset($this->services['fosuser.listener.resetting']) ? $this->services['fosuser.listener.resetting'] : $this->get('fosuser.listener.resetting')) && false ?: ''};
        }, 1 => 'onResettingResetRequest'), 0);
        $instance->addListener('console.command', array(0 => function () {
            return ${($ = isset($this->services['debug.dumplistener']) ? $this->services['debug.dumplistener'] : $this->get('debug.dumplistener')) && false ?: ''};
        }, 1 => 'configure'), 1024);
        $instance->addListener('kernel.response', array(0 => function () {
            return ${($ = isset($this->services['webprofiler.debugtoolbar']) ? $this->services['webprofiler.debugtoolbar'] : $this->get('webprofiler.debugtoolbar')) && false ?: ''};
        }, 1 => 'onKernelResponse'), -128);

        return $instance;
    }

    /**
     * Gets the public 'debug.stopwatch' shared service.
     *
     * @return \Symfony\Component\Stopwatch\Stopwatch
     */
    protected function getDebugStopwatchService()
    {
        return $this->services['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch();
    }

    /**
     * Gets the public 'deprecated.form.registry' shared service.
     *
     * @return \stdClass
     *
     * @deprecated The service "deprecated.form.registry" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0
     */
    protected function getDeprecatedFormRegistryService()
    {
        @triggererror('The service "deprecated.form.registry" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0', EUSERDEPRECATED);

        $this->services['deprecated.form.registry'] = $instance = new \stdClass();

        $instance->registry = array(0 => ${($ = isset($this->services['form.typeguesser.validator']) ? $this->services['form.typeguesser.validator'] : $this->get('form.typeguesser.validator')) && false ?: ''}, 1 => ${($ = isset($this->services['form.type.choice']) ? $this->services['form.type.choice'] : $this->get('form.type.choice')) && false ?: ''}, 2 => ${($ = isset($this->services['form.type.form']) ? $this->services['form.type.form'] : $this->get('form.type.form')) && false ?: ''}, 3 => ${($ = isset($this->services['form.typeextension.form.httpfoundation']) ? $this->services['form.typeextension.form.httpfoundation'] : $this->get('form.typeextension.form.httpfoundation')) && false ?: ''}, 4 => ${($ = isset($this->services['form.typeextension.form.validator']) ? $this->services['form.typeextension.form.validator'] : $this->get('form.typeextension.form.validator')) && false ?: ''}, 5 => ${($ = isset($this->services['form.typeextension.repeated.validator']) ? $this->services['form.typeextension.repeated.validator'] : $this->get('form.typeextension.repeated.validator')) && false ?: ''}, 6 => ${($ = isset($this->services['form.typeextension.submit.validator']) ? $this->services['form.typeextension.submit.validator'] : $this->get('form.typeextension.submit.validator')) && false ?: ''}, 7 => ${($ = isset($this->services['form.typeextension.upload.validator']) ? $this->services['form.typeextension.upload.validator'] : $this->get('form.typeextension.upload.validator')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'deprecated.form.registry.csrf' shared service.
     *
     * @return \stdClass
     *
     * @deprecated The service "deprecated.form.registry.csrf" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0
     */
    protected function getDeprecatedFormRegistryCsrfService()
    {
        @triggererror('The service "deprecated.form.registry.csrf" is internal and deprecated since Symfony 3.3 and will be removed in Symfony 4.0', EUSERDEPRECATED);

        $this->services['deprecated.form.registry.csrf'] = $instance = new \stdClass();

        $instance->registry = array(0 => ${($ = isset($this->services['form.typeextension.csrf']) ? $this->services['form.typeextension.csrf'] : $this->get('form.typeextension.csrf')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'doctrine' shared service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Registry
     */
    protected function getDoctrineService()
    {
        return $this->services['doctrine'] = new \Doctrine\Bundle\DoctrineBundle\Registry($this, array('default' => 'doctrine.dbal.defaultconnection'), array('default' => 'doctrine.orm.defaultentitymanager'), 'default', 'default');
    }

    /**
     * Gets the public 'doctrine.dbal.connectionfactory' shared service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ConnectionFactory
     */
    protected function getDoctrineDbalConnectionFactoryService()
    {
        return $this->services['doctrine.dbal.connectionfactory'] = new \Doctrine\Bundle\DoctrineBundle\ConnectionFactory(array());
    }

    /**
     * Gets the public 'doctrine.dbal.defaultconnection' shared service.
     *
     * @return \Doctrine\DBAL\Connection
     */
    protected function getDoctrineDbalDefaultConnectionService()
    {
        $a = new \Doctrine\DBAL\Logging\LoggerChain();
        $a->addLogger(new \Symfony\Bridge\Doctrine\Logger\DbalLogger(${($ = isset($this->services['monolog.logger.doctrine']) ? $this->services['monolog.logger.doctrine'] : $this->get('monolog.logger.doctrine', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}));
        $a->addLogger(${($ = isset($this->services['doctrine.dbal.logger.profiling.default']) ? $this->services['doctrine.dbal.logger.profiling.default'] : $this->getDoctrineDbalLoggerProfilingDefaultService()) && false ?: ''});

        $b = new \Doctrine\DBAL\Configuration();
        $b->setSQLLogger($a);

        $c = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager($this);
        $c->addEventSubscriber(new \FOS\UserBundle\Doctrine\UserListener(${($ = isset($this->services['fosuser.util.passwordupdater']) ? $this->services['fosuser.util.passwordupdater'] : $this->getFosUserUtilPasswordUpdaterService()) && false ?: ''}, ${($ = isset($this->services['fosuser.util.canonicalfieldsupdater']) ? $this->services['fosuser.util.canonicalfieldsupdater'] : $this->getFosUserUtilCanonicalFieldsUpdaterService()) && false ?: ''}));
        $c->addEventListener(array(0 => 'loadClassMetadata'), ${($ = isset($this->services['doctrine.orm.defaultlisteners.attachentitylisteners']) ? $this->services['doctrine.orm.defaultlisteners.attachentitylisteners'] : $this->get('doctrine.orm.defaultlisteners.attachentitylisteners')) && false ?: ''});

        return $this->services['doctrine.dbal.defaultconnection'] = ${($ = isset($this->services['doctrine.dbal.connectionfactory']) ? $this->services['doctrine.dbal.connectionfactory'] : $this->get('doctrine.dbal.connectionfactory')) && false ?: ''}->createConnection(array('driver' => 'pdomysql', 'host' => '127.0.0.1', 'port' => NULL, 'dbname' => 'gamification', 'user' => 'root', 'password' => 'root', 'charset' => 'UTF8', 'driverOptions' => array(), 'defaultTableOptions' => array()), $b, $c, array());
    }

    /**
     * Gets the public 'doctrine.orm.defaultentitylistenerresolver' shared service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver
     */
    protected function getDoctrineOrmDefaultEntityListenerResolverService()
    {
        return $this->services['doctrine.orm.defaultentitylistenerresolver'] = new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerAwareEntityListenerResolver($this);
    }

    /**
     * Gets the public 'doctrine.orm.defaultentitymanager' shared service.
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getDoctrineOrmDefaultEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['doctrine.orm.defaultentitymanager'] = DoctrineORMEntityManager000000005e7601c1000000001c1e21934905e7acb9fc1a61530eec0a2e144cc0::staticProxyConstructor(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrineOrmDefaultEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $a = new \Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain();
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(${($ = isset($this->services['annotationreader']) ? $this->services['annotationreader'] : $this->get('annotationreader')) && false ?: ''}, array(0 => ($this->targetDirs[1].'/src/AppBundle/Entity'))), 'AppBundle\\Entity');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator(array(($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/config/doctrine-mapping') => 'FOS\\UserBundle\\Model'), '.orm.xml')), 'FOS\\UserBundle\\Model');

        $b = new \Doctrine\ORM\Configuration();
        $b->setEntityNamespaces(array('AppBundle' => 'AppBundle\\Entity'));
        $b->setMetadataCacheImpl(${($ = isset($this->services['doctrinecache.providers.doctrine.orm.defaultmetadatacache']) ? $this->services['doctrinecache.providers.doctrine.orm.defaultmetadatacache'] : $this->get('doctrinecache.providers.doctrine.orm.defaultmetadatacache')) && false ?: ''});
        $b->setQueryCacheImpl(${($ = isset($this->services['doctrinecache.providers.doctrine.orm.defaultquerycache']) ? $this->services['doctrinecache.providers.doctrine.orm.defaultquerycache'] : $this->get('doctrinecache.providers.doctrine.orm.defaultquerycache')) && false ?: ''});
        $b->setResultCacheImpl(${($ = isset($this->services['doctrinecache.providers.doctrine.orm.defaultresultcache']) ? $this->services['doctrinecache.providers.doctrine.orm.defaultresultcache'] : $this->get('doctrinecache.providers.doctrine.orm.defaultresultcache')) && false ?: ''});
        $b->setMetadataDriverImpl($a);
        $b->setProxyDir((DIR.'/doctrine/orm/Proxies'));
        $b->setProxyNamespace('Proxies');
        $b->setAutoGenerateProxyClasses(true);
        $b->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $b->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $b->setNamingStrategy(new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy());
        $b->setQuoteStrategy(new \Doctrine\ORM\Mapping\DefaultQuoteStrategy());
        $b->setEntityListenerResolver(${($ = isset($this->services['doctrine.orm.defaultentitylistenerresolver']) ? $this->services['doctrine.orm.defaultentitylistenerresolver'] : $this->get('doctrine.orm.defaultentitylistenerresolver')) && false ?: ''});

        $instance = \Doctrine\ORM\EntityManager::create(${($ = isset($this->services['doctrine.dbal.defaultconnection']) ? $this->services['doctrine.dbal.defaultconnection'] : $this->get('doctrine.dbal.defaultconnection')) && false ?: ''}, $b);

        ${($ = isset($this->services['doctrine.orm.defaultmanagerconfigurator']) ? $this->services['doctrine.orm.defaultmanagerconfigurator'] : $this->get('doctrine.orm.defaultmanagerconfigurator')) && false ?: ''}->configure($instance);

        return $instance;
    }

    /**
     * Gets the public 'doctrine.orm.defaultentitymanager.propertyinfoextractor' shared service.
     *
     * @return \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor
     */
    protected function getDoctrineOrmDefaultEntityManagerPropertyInfoExtractorService()
    {
        return $this->services['doctrine.orm.defaultentitymanager.propertyinfoextractor'] = new \Symfony\Bridge\Doctrine\PropertyInfo\DoctrineExtractor(${($ = isset($this->services['doctrine.orm.defaultentitymanager']) ? $this->services['doctrine.orm.defaultentitymanager'] : $this->get('doctrine.orm.defaultentitymanager')) && false ?: ''}->getMetadataFactory());
    }

    /**
     * Gets the public 'doctrine.orm.defaultlisteners.attachentitylisteners' shared service.
     *
     * @return \Doctrine\ORM\Tools\AttachEntityListenersListener
     */
    protected function getDoctrineOrmDefaultListenersAttachEntityListenersService()
    {
        return $this->services['doctrine.orm.defaultlisteners.attachentitylisteners'] = new \Doctrine\ORM\Tools\AttachEntityListenersListener();
    }

    /**
     * Gets the public 'doctrine.orm.defaultmanagerconfigurator' shared service.
     *
     * @return \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator
     */
    protected function getDoctrineOrmDefaultManagerConfiguratorService()
    {
        return $this->services['doctrine.orm.defaultmanagerconfigurator'] = new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator(array(), array());
    }

    /**
     * Gets the public 'doctrine.orm.validator.unique' shared service.
     *
     * @return \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator
     */
    protected function getDoctrineOrmValidatorUniqueService()
    {
        return $this->services['doctrine.orm.validator.unique'] = new \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
    }

    /**
     * Gets the public 'doctrine.orm.validatorinitializer' shared service.
     *
     * @return \Symfony\Bridge\Doctrine\Validator\DoctrineInitializer
     */
    protected function getDoctrineOrmValidatorInitializerService()
    {
        return $this->services['doctrine.orm.validatorinitializer'] = new \Symfony\Bridge\Doctrine\Validator\DoctrineInitializer(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
    }

    /**
     * Gets the public 'doctrinecache.providers.doctrine.orm.defaultmetadatacache' shared service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache
     */
    protected function getDoctrineCacheProvidersDoctrineOrmDefaultMetadataCacheService()
    {
        $this->services['doctrinecache.providers.doctrine.orm.defaultmetadatacache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2ormdefaultbf59bac6678fe9124ceaf6484d2843cf3b7dc46d076cbd0f732210ee2ca9682b');

        return $instance;
    }

    /**
     * Gets the public 'doctrinecache.providers.doctrine.orm.defaultquerycache' shared service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache
     */
    protected function getDoctrineCacheProvidersDoctrineOrmDefaultQueryCacheService()
    {
        $this->services['doctrinecache.providers.doctrine.orm.defaultquerycache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2ormdefaultbf59bac6678fe9124ceaf6484d2843cf3b7dc46d076cbd0f732210ee2ca9682b');

        return $instance;
    }

    /**
     * Gets the public 'doctrinecache.providers.doctrine.orm.defaultresultcache' shared service.
     *
     * @return \Doctrine\Common\Cache\ArrayCache
     */
    protected function getDoctrineCacheProvidersDoctrineOrmDefaultResultCacheService()
    {
        $this->services['doctrinecache.providers.doctrine.orm.defaultresultcache'] = $instance = new \Doctrine\Common\Cache\ArrayCache();

        $instance->setNamespace('sf2ormdefaultbf59bac6678fe9124ceaf6484d2843cf3b7dc46d076cbd0f732210ee2ca9682b');

        return $instance;
    }

    /**
     * Gets the public 'filelocator' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Config\FileLocator
     */
    protected function getFileLocatorService()
    {
        return $this->services['filelocator'] = new \Symfony\Component\HttpKernel\Config\FileLocator(${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''}, ($this->targetDirs[1].'/app/Resources'), array(0 => ($this->targetDirs[1].'/app')));
    }

    /**
     * Gets the public 'filesystem' shared service.
     *
     * @return \Symfony\Component\Filesystem\Filesystem
     */
    protected function getFilesystemService()
    {
        return $this->services['filesystem'] = new \Symfony\Component\Filesystem\Filesystem();
    }

    /**
     * Gets the public 'form.extension' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Extension\DependencyInjectionExtension
     */
    protected function getFormExtensionService()
    {
        return $this->services['form.extension'] = new \Sonata\CoreBundle\Form\Extension\DependencyInjectionExtension($this, array('AppBundle\\Form\\RegistrationType' => 'app.form.registration', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => 'form.type.form', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType' => 'form.type.choice', 'Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType' => 'form.type.entity', 'FOS\\UserBundle\\Form\\Type\\UsernameFormType' => 'fosuser.usernameformtype', 'FOS\\UserBundle\\Form\\Type\\ProfileFormType' => 'fosuser.profile.form.type', 'FOS\\UserBundle\\Form\\Type\\RegistrationFormType' => 'fosuser.registration.form.type', 'FOS\\UserBundle\\Form\\Type\\ChangePasswordFormType' => 'fosuser.changepassword.form.type', 'FOS\\UserBundle\\Form\\Type\\ResettingFormType' => 'fosuser.resetting.form.type', 'Sonata\\CoreBundle\\Form\\Type\\ImmutableArrayType' => 'sonata.core.form.type.array', 'Sonata\\CoreBundle\\Form\\Type\\BooleanType' => 'sonata.core.form.type.boolean', 'Sonata\\CoreBundle\\Form\\Type\\CollectionType' => 'sonata.core.form.type.collection', 'Sonata\\CoreBundle\\Form\\Type\\TranslatableChoiceType' => 'sonata.core.form.type.translatablechoice', 'Sonata\\CoreBundle\\Form\\Type\\DateRangeType' => 'sonata.core.form.type.daterange', 'Sonata\\CoreBundle\\Form\\Type\\DateTimeRangeType' => 'sonata.core.form.type.datetimerange', 'Sonata\\CoreBundle\\Form\\Type\\DatePickerType' => 'sonata.core.form.type.datepicker', 'Sonata\\CoreBundle\\Form\\Type\\DateTimePickerType' => 'sonata.core.form.type.datetimepicker', 'Sonata\\CoreBundle\\Form\\Type\\DateRangePickerType' => 'sonata.core.form.type.daterangepicker', 'Sonata\\CoreBundle\\Form\\Type\\DateTimeRangePickerType' => 'sonata.core.form.type.datetimerangepicker', 'Sonata\\CoreBundle\\Form\\Type\\EqualType' => 'sonata.core.form.type.equal', 'Sonata\\CoreBundle\\Form\\Type\\ColorSelectorType' => 'sonata.core.form.type.colorselector', 'Sonata\\BlockBundle\\Form\\Type\\ServiceListType' => 'sonata.block.form.type.block', 'Sonata\\BlockBundle\\Form\\Type\\ContainerTemplateType' => 'sonata.block.form.type.containertemplate', 'Sonata\\AdminBundle\\Form\\Type\\AdminType' => 'sonata.admin.form.type.admin', 'Sonata\\AdminBundle\\Form\\Type\\ModelType' => 'sonata.admin.form.type.modelchoice', 'Sonata\\AdminBundle\\Form\\Type\\ModelListType' => 'sonata.admin.form.type.modellist', 'Sonata\\AdminBundle\\Form\\Type\\ModelReferenceType' => 'sonata.admin.form.type.modelreference', 'Sonata\\AdminBundle\\Form\\Type\\ModelHiddenType' => 'sonata.admin.form.type.modelhidden', 'Sonata\\AdminBundle\\Form\\Type\\ModelAutocompleteType' => 'sonata.admin.form.type.modelautocomplete', 'Sonata\\AdminBundle\\Form\\Type\\CollectionType' => 'sonata.admin.form.type.collection', 'Sonata\\AdminBundle\\Form\\Type\\ChoiceFieldMaskType' => 'sonata.admin.doctrineorm.form.type.choicefieldmask', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\NumberType' => 'sonata.admin.form.filter.type.number', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\ChoiceType' => 'sonata.admin.form.filter.type.choice', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DefaultType' => 'sonata.admin.form.filter.type.default', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateType' => 'sonata.admin.form.filter.type.date', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateRangeType' => 'sonata.admin.form.filter.type.daterange', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateTimeType' => 'sonata.admin.form.filter.type.datetime', 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateTimeRangeType' => 'sonata.admin.form.filter.type.datetimerange'), array('Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType' => array(0 => 'form.typeextension.form.httpfoundation', 1 => 'form.typeextension.form.validator', 2 => 'form.typeextension.upload.validator', 3 => 'form.typeextension.csrf', 4 => 'form.typeextension.form.datacollector', 5 => 'sonata.admin.form.extension.field', 6 => 'sonata.admin.form.extension.field.mopa'), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RepeatedType' => array(0 => 'form.typeextension.repeated.validator'), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\SubmitType' => array(0 => 'form.typeextension.submit.validator'), 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType' => array(0 => 'sonata.admin.form.extension.choice')), array(0 => 'form.typeguesser.validator', 1 => 'form.typeguesser.doctrine'), array('form' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FormType', 'birthday' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\BirthdayType', 'checkbox' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\CheckboxType', 'choice' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType', 'collection' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\CollectionType', 'country' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\CountryType', 'date' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\DateType', 'datetime' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\DateTimeType', 'email' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\EmailType', 'file' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\FileType', 'hidden' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\HiddenType', 'integer' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\IntegerType', 'language' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\LanguageType', 'locale' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\LocaleType', 'money' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\MoneyType', 'number' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\NumberType', 'password' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\PasswordType', 'percent' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\PercentType', 'radio' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RadioType', 'repeated' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\RepeatedType', 'search' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\SearchType', 'textarea' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextareaType', 'text' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType', 'time' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TimeType', 'timezone' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TimezoneType', 'url' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\UrlType', 'button' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ButtonType', 'submit' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\SubmitType', 'reset' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ResetType', 'currency' => 'Symfony\\Component\\Form\\Extension\\Core\\Type\\CurrencyType', 'entity' => 'Symfony\\Bridge\\Doctrine\\Form\\Type\\EntityType', 'sonatatypeimmutablearray' => 'Sonata\\CoreBundle\\Form\\Type\\ImmutableArrayType', 'sonatatypeboolean' => 'Sonata\\CoreBundle\\Form\\Type\\BooleanType', 'sonatatypecollection' => 'Sonata\\CoreBundle\\Form\\Type\\CollectionType', 'sonatatypetranslatablechoice' => 'Sonata\\CoreBundle\\Form\\Type\\TranslatableChoiceType', 'sonatatypedaterange' => 'Sonata\\CoreBundle\\Form\\Type\\DateRangeType', 'sonatatypedatetimerange' => 'Sonata\\CoreBundle\\Form\\Type\\DateTimeRangeType', 'sonatatypedatepicker' => 'Sonata\\CoreBundle\\Form\\Type\\DatePickerType', 'sonatatypedatetimepicker' => 'Sonata\\CoreBundle\\Form\\Type\\DateTimePickerType', 'sonatatypedaterangepicker' => 'Sonata\\CoreBundle\\Form\\Type\\DateRangePickerType', 'sonatatypedatetimerangepicker' => 'Sonata\\CoreBundle\\Form\\Type\\DateTimeRangePickerType', 'sonatatypeequal' => 'Sonata\\CoreBundle\\Form\\Type\\EqualType', 'sonatatypecolorselector' => 'Sonata\\CoreBundle\\Form\\Type\\ColorSelectorType', 'sonatablockservicechoice' => 'Sonata\\BlockBundle\\Form\\Type\\ServiceListType', 'sonatatypecontainertemplatechoice' => 'Sonata\\BlockBundle\\Form\\Type\\ContainerTemplateType', 'sonatatypeadmin' => 'Sonata\\AdminBundle\\Form\\Type\\AdminType', 'sonatatypemodel' => 'Sonata\\AdminBundle\\Form\\Type\\ModelType', 'sonatatypemodellist' => 'Sonata\\AdminBundle\\Form\\Type\\ModelListType', 'sonatatypemodelreference' => 'Sonata\\AdminBundle\\Form\\Type\\ModelReferenceType', 'sonatatypemodelhidden' => 'Sonata\\AdminBundle\\Form\\Type\\ModelHiddenType', 'sonatatypemodelautocomplete' => 'Sonata\\AdminBundle\\Form\\Type\\ModelAutocompleteType', 'sonatatypenativecollection' => 'Sonata\\AdminBundle\\Form\\Type\\CollectionType', 'sonatatypechoicefieldmask' => 'Sonata\\AdminBundle\\Form\\Type\\ChoiceFieldMaskType', 'sonatatypefilternumber' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\NumberType', 'sonatatypefilterchoice' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\ChoiceType', 'sonatatypefilterdefault' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DefaultType', 'sonatatypefilterdate' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateType', 'sonatatypefilterdaterange' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateRangeType', 'sonatatypefilterdatetime' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateTimeType', 'sonatatypefilterdatetimerange' => 'Sonata\\AdminBundle\\Form\\Type\\Filter\\DateTimeRangeType', 'tab' => 'Mopa\\Bundle\\BootstrapBundle\\Form\\Type\\TabType'), array('form' => array(0 => 'form.typeextension.form.httpfoundation', 1 => 'form.typeextension.form.validator', 2 => 'form.typeextension.csrf', 3 => 'form.typeextension.form.datacollector', 4 => 'sonata.admin.form.extension.field', 5 => 'mopabootstrap.form.typeextension.help', 6 => 'mopabootstrap.form.typeextension.legend', 7 => 'mopabootstrap.form.typeextension.error', 8 => 'mopabootstrap.form.typeextension.widget', 9 => 'mopabootstrap.form.typeextension.horizontal', 10 => 'mopabootstrap.form.typeextension.widgetcollection', 11 => 'mopabootstrap.form.typeextension.tabbed', 12 => 'form.typeextension.form.httpfoundation', 13 => 'form.typeextension.form.validator', 14 => 'form.typeextension.csrf', 15 => 'form.typeextension.form.datacollector', 16 => 'sonata.admin.form.extension.field', 17 => 'mopabootstrap.form.typeextension.help', 18 => 'mopabootstrap.form.typeextension.legend', 19 => 'mopabootstrap.form.typeextension.error', 20 => 'mopabootstrap.form.typeextension.widget', 21 => 'mopabootstrap.form.typeextension.horizontal', 22 => 'mopabootstrap.form.typeextension.widgetcollection', 23 => 'mopabootstrap.form.typeextension.tabbed', 24 => 'form.typeextension.form.httpfoundation', 25 => 'form.typeextension.form.validator', 26 => 'form.typeextension.csrf', 27 => 'form.typeextension.form.datacollector', 28 => 'sonata.admin.form.extension.field', 29 => 'mopabootstrap.form.typeextension.help', 30 => 'mopabootstrap.form.typeextension.legend', 31 => 'mopabootstrap.form.typeextension.error', 32 => 'mopabootstrap.form.typeextension.widget', 33 => 'mopabootstrap.form.typeextension.horizontal', 34 => 'mopabootstrap.form.typeextension.widgetcollection', 35 => 'mopabootstrap.form.typeextension.tabbed'), 'repeated' => array(0 => 'form.typeextension.repeated.validator', 1 => 'form.typeextension.repeated.validator', 2 => 'form.typeextension.repeated.validator'), 'submit' => array(0 => 'form.typeextension.submit.validator', 1 => 'form.typeextension.submit.validator', 2 => 'form.typeextension.submit.validator'), 'choice' => array(0 => 'sonata.admin.form.extension.choice', 1 => 'sonata.admin.form.extension.choice', 2 => 'sonata.admin.form.extension.choice'), 'button' => array(0 => 'mopabootstrap.form.typeextension.button', 1 => 'mopabootstrap.form.typeextension.button', 2 => 'mopabootstrap.form.typeextension.button'), 'date' => array(0 => 'mopabootstrap.form.typeextension.date', 1 => 'mopabootstrap.form.typeextension.date', 2 => 'mopabootstrap.form.typeextension.date')), array());
    }

    /**
     * Gets the public 'form.factory' shared service.
     *
     * @return \Symfony\Component\Form\FormFactory
     */
    protected function getFormFactoryService()
    {
        return $this->services['form.factory'] = new \Symfony\Component\Form\FormFactory(${($ = isset($this->services['form.registry']) ? $this->services['form.registry'] : $this->get('form.registry')) && false ?: ''}, ${($ = isset($this->services['form.resolvedtypefactory']) ? $this->services['form.resolvedtypefactory'] : $this->get('form.resolvedtypefactory')) && false ?: ''});
    }

    /**
     * Gets the public 'form.registry' shared service.
     *
     * @return \Symfony\Component\Form\FormRegistry
     */
    protected function getFormRegistryService()
    {
        return $this->services['form.registry'] = new \Symfony\Component\Form\FormRegistry(array(0 => ${($ = isset($this->services['form.extension']) ? $this->services['form.extension'] : $this->get('form.extension')) && false ?: ''}), ${($ = isset($this->services['form.resolvedtypefactory']) ? $this->services['form.resolvedtypefactory'] : $this->get('form.resolvedtypefactory')) && false ?: ''});
    }

    /**
     * Gets the public 'form.resolvedtypefactory' shared service.
     *
     * @return \Symfony\Component\Form\Extension\DataCollector\Proxy\ResolvedTypeFactoryDataCollectorProxy
     */
    protected function getFormResolvedTypeFactoryService()
    {
        return $this->services['form.resolvedtypefactory'] = new \Symfony\Component\Form\Extension\DataCollector\Proxy\ResolvedTypeFactoryDataCollectorProxy(new \Symfony\Component\Form\ResolvedFormTypeFactory(), ${($ = isset($this->services['datacollector.form']) ? $this->services['datacollector.form'] : $this->get('datacollector.form')) && false ?: ''});
    }

    /**
     * Gets the public 'form.type.birthday' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\BirthdayType
     *
     * @deprecated The "form.type.birthday" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeBirthdayService()
    {
        @triggererror('The "form.type.birthday" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.birthday'] = new \Symfony\Component\Form\Extension\Core\Type\BirthdayType();
    }

    /**
     * Gets the public 'form.type.button' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ButtonType
     *
     * @deprecated The "form.type.button" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeButtonService()
    {
        @triggererror('The "form.type.button" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.button'] = new \Symfony\Component\Form\Extension\Core\Type\ButtonType();
    }

    /**
     * Gets the public 'form.type.checkbox' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CheckboxType
     *
     * @deprecated The "form.type.checkbox" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeCheckboxService()
    {
        @triggererror('The "form.type.checkbox" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.checkbox'] = new \Symfony\Component\Form\Extension\Core\Type\CheckboxType();
    }

    /**
     * Gets the public 'form.type.choice' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ChoiceType
     */
    protected function getFormTypeChoiceService()
    {
        return $this->services['form.type.choice'] = new \Symfony\Component\Form\Extension\Core\Type\ChoiceType(new \Symfony\Component\Form\ChoiceList\Factory\CachingFactoryDecorator(new \Symfony\Component\Form\ChoiceList\Factory\PropertyAccessDecorator(new \Symfony\Component\Form\ChoiceList\Factory\DefaultChoiceListFactory(), ${($ = isset($this->services['propertyaccessor']) ? $this->services['propertyaccessor'] : $this->get('propertyaccessor')) && false ?: ''})));
    }

    /**
     * Gets the public 'form.type.collection' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CollectionType
     *
     * @deprecated The "form.type.collection" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeCollectionService()
    {
        @triggererror('The "form.type.collection" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.collection'] = new \Symfony\Component\Form\Extension\Core\Type\CollectionType();
    }

    /**
     * Gets the public 'form.type.country' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CountryType
     *
     * @deprecated The "form.type.country" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeCountryService()
    {
        @triggererror('The "form.type.country" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.country'] = new \Symfony\Component\Form\Extension\Core\Type\CountryType();
    }

    /**
     * Gets the public 'form.type.currency' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\CurrencyType
     *
     * @deprecated The "form.type.currency" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeCurrencyService()
    {
        @triggererror('The "form.type.currency" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.currency'] = new \Symfony\Component\Form\Extension\Core\Type\CurrencyType();
    }

    /**
     * Gets the public 'form.type.date' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\DateType
     *
     * @deprecated The "form.type.date" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeDateService()
    {
        @triggererror('The "form.type.date" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.date'] = new \Symfony\Component\Form\Extension\Core\Type\DateType();
    }

    /**
     * Gets the public 'form.type.datetime' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\DateTimeType
     *
     * @deprecated The "form.type.datetime" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeDatetimeService()
    {
        @triggererror('The "form.type.datetime" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.datetime'] = new \Symfony\Component\Form\Extension\Core\Type\DateTimeType();
    }

    /**
     * Gets the public 'form.type.email' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\EmailType
     *
     * @deprecated The "form.type.email" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeEmailService()
    {
        @triggererror('The "form.type.email" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.email'] = new \Symfony\Component\Form\Extension\Core\Type\EmailType();
    }

    /**
     * Gets the public 'form.type.entity' shared service.
     *
     * @return \Symfony\Bridge\Doctrine\Form\Type\EntityType
     */
    protected function getFormTypeEntityService()
    {
        return $this->services['form.type.entity'] = new \Symfony\Bridge\Doctrine\Form\Type\EntityType(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
    }

    /**
     * Gets the public 'form.type.file' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\FileType
     *
     * @deprecated The "form.type.file" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeFileService()
    {
        @triggererror('The "form.type.file" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.file'] = new \Symfony\Component\Form\Extension\Core\Type\FileType();
    }

    /**
     * Gets the public 'form.type.form' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\FormType
     */
    protected function getFormTypeFormService()
    {
        return $this->services['form.type.form'] = new \Symfony\Component\Form\Extension\Core\Type\FormType(${($ = isset($this->services['propertyaccessor']) ? $this->services['propertyaccessor'] : $this->get('propertyaccessor')) && false ?: ''});
    }

    /**
     * Gets the public 'form.type.hidden' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\HiddenType
     *
     * @deprecated The "form.type.hidden" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeHiddenService()
    {
        @triggererror('The "form.type.hidden" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.hidden'] = new \Symfony\Component\Form\Extension\Core\Type\HiddenType();
    }

    /**
     * Gets the public 'form.type.integer' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\IntegerType
     *
     * @deprecated The "form.type.integer" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeIntegerService()
    {
        @triggererror('The "form.type.integer" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.integer'] = new \Symfony\Component\Form\Extension\Core\Type\IntegerType();
    }

    /**
     * Gets the public 'form.type.language' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\LanguageType
     *
     * @deprecated The "form.type.language" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeLanguageService()
    {
        @triggererror('The "form.type.language" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.language'] = new \Symfony\Component\Form\Extension\Core\Type\LanguageType();
    }

    /**
     * Gets the public 'form.type.locale' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\LocaleType
     *
     * @deprecated The "form.type.locale" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeLocaleService()
    {
        @triggererror('The "form.type.locale" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.locale'] = new \Symfony\Component\Form\Extension\Core\Type\LocaleType();
    }

    /**
     * Gets the public 'form.type.money' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\MoneyType
     *
     * @deprecated The "form.type.money" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeMoneyService()
    {
        @triggererror('The "form.type.money" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.money'] = new \Symfony\Component\Form\Extension\Core\Type\MoneyType();
    }

    /**
     * Gets the public 'form.type.number' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\NumberType
     *
     * @deprecated The "form.type.number" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeNumberService()
    {
        @triggererror('The "form.type.number" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.number'] = new \Symfony\Component\Form\Extension\Core\Type\NumberType();
    }

    /**
     * Gets the public 'form.type.password' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\PasswordType
     *
     * @deprecated The "form.type.password" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypePasswordService()
    {
        @triggererror('The "form.type.password" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.password'] = new \Symfony\Component\Form\Extension\Core\Type\PasswordType();
    }

    /**
     * Gets the public 'form.type.percent' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\PercentType
     *
     * @deprecated The "form.type.percent" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypePercentService()
    {
        @triggererror('The "form.type.percent" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.percent'] = new \Symfony\Component\Form\Extension\Core\Type\PercentType();
    }

    /**
     * Gets the public 'form.type.radio' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RadioType
     *
     * @deprecated The "form.type.radio" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeRadioService()
    {
        @triggererror('The "form.type.radio" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.radio'] = new \Symfony\Component\Form\Extension\Core\Type\RadioType();
    }

    /**
     * Gets the public 'form.type.range' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RangeType
     *
     * @deprecated The "form.type.range" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeRangeService()
    {
        @triggererror('The "form.type.range" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.range'] = new \Symfony\Component\Form\Extension\Core\Type\RangeType();
    }

    /**
     * Gets the public 'form.type.repeated' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\RepeatedType
     *
     * @deprecated The "form.type.repeated" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeRepeatedService()
    {
        @triggererror('The "form.type.repeated" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.repeated'] = new \Symfony\Component\Form\Extension\Core\Type\RepeatedType();
    }

    /**
     * Gets the public 'form.type.reset' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\ResetType
     *
     * @deprecated The "form.type.reset" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeResetService()
    {
        @triggererror('The "form.type.reset" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.reset'] = new \Symfony\Component\Form\Extension\Core\Type\ResetType();
    }

    /**
     * Gets the public 'form.type.search' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\SearchType
     *
     * @deprecated The "form.type.search" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeSearchService()
    {
        @triggererror('The "form.type.search" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.search'] = new \Symfony\Component\Form\Extension\Core\Type\SearchType();
    }

    /**
     * Gets the public 'form.type.submit' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\SubmitType
     *
     * @deprecated The "form.type.submit" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeSubmitService()
    {
        @triggererror('The "form.type.submit" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.submit'] = new \Symfony\Component\Form\Extension\Core\Type\SubmitType();
    }

    /**
     * Gets the public 'form.type.text' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TextType
     *
     * @deprecated The "form.type.text" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeTextService()
    {
        @triggererror('The "form.type.text" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.text'] = new \Symfony\Component\Form\Extension\Core\Type\TextType();
    }

    /**
     * Gets the public 'form.type.textarea' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TextareaType
     *
     * @deprecated The "form.type.textarea" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeTextareaService()
    {
        @triggererror('The "form.type.textarea" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.textarea'] = new \Symfony\Component\Form\Extension\Core\Type\TextareaType();
    }

    /**
     * Gets the public 'form.type.time' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TimeType
     *
     * @deprecated The "form.type.time" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeTimeService()
    {
        @triggererror('The "form.type.time" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.time'] = new \Symfony\Component\Form\Extension\Core\Type\TimeType();
    }

    /**
     * Gets the public 'form.type.timezone' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\TimezoneType
     *
     * @deprecated The "form.type.timezone" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeTimezoneService()
    {
        @triggererror('The "form.type.timezone" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.timezone'] = new \Symfony\Component\Form\Extension\Core\Type\TimezoneType();
    }

    /**
     * Gets the public 'form.type.url' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Core\Type\UrlType
     *
     * @deprecated The "form.type.url" service is deprecated since Symfony 3.1 and will be removed in 4.0.
     */
    protected function getFormTypeUrlService()
    {
        @triggererror('The "form.type.url" service is deprecated since Symfony 3.1 and will be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['form.type.url'] = new \Symfony\Component\Form\Extension\Core\Type\UrlType();
    }

    /**
     * Gets the public 'form.typeextension.csrf' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension
     */
    protected function getFormTypeExtensionCsrfService()
    {
        return $this->services['form.typeextension.csrf'] = new \Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension(${($ = isset($this->services['security.csrf.tokenmanager']) ? $this->services['security.csrf.tokenmanager'] : $this->get('security.csrf.tokenmanager')) && false ?: ''}, true, 'token', ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, 'validators', ${($ = isset($this->services['form.serverparams']) ? $this->services['form.serverparams'] : $this->getFormServerParamsService()) && false ?: ''});
    }

    /**
     * Gets the public 'form.typeextension.form.datacollector' shared service.
     *
     * @return \Symfony\Component\Form\Extension\DataCollector\Type\DataCollectorTypeExtension
     */
    protected function getFormTypeExtensionFormDataCollectorService()
    {
        return $this->services['form.typeextension.form.datacollector'] = new \Symfony\Component\Form\Extension\DataCollector\Type\DataCollectorTypeExtension(${($ = isset($this->services['datacollector.form']) ? $this->services['datacollector.form'] : $this->get('datacollector.form')) && false ?: ''});
    }

    /**
     * Gets the public 'form.typeextension.form.httpfoundation' shared service.
     *
     * @return \Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension
     */
    protected function getFormTypeExtensionFormHttpFoundationService()
    {
        return $this->services['form.typeextension.form.httpfoundation'] = new \Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension(new \Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationRequestHandler(${($ = isset($this->services['form.serverparams']) ? $this->services['form.serverparams'] : $this->getFormServerParamsService()) && false ?: ''}));
    }

    /**
     * Gets the public 'form.typeextension.form.validator' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension
     */
    protected function getFormTypeExtensionFormValidatorService()
    {
        return $this->services['form.typeextension.form.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
    }

    /**
     * Gets the public 'form.typeextension.repeated.validator' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension
     */
    protected function getFormTypeExtensionRepeatedValidatorService()
    {
        return $this->services['form.typeextension.repeated.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension();
    }

    /**
     * Gets the public 'form.typeextension.submit.validator' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension
     */
    protected function getFormTypeExtensionSubmitValidatorService()
    {
        return $this->services['form.typeextension.submit.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension();
    }

    /**
     * Gets the public 'form.typeextension.upload.validator' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension
     */
    protected function getFormTypeExtensionUploadValidatorService()
    {
        return $this->services['form.typeextension.upload.validator'] = new \Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, 'validators');
    }

    /**
     * Gets the public 'form.typeguesser.doctrine' shared service.
     *
     * @return \Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser
     */
    protected function getFormTypeGuesserDoctrineService()
    {
        return $this->services['form.typeguesser.doctrine'] = new \Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
    }

    /**
     * Gets the public 'form.typeguesser.validator' shared service.
     *
     * @return \Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser
     */
    protected function getFormTypeGuesserValidatorService()
    {
        return $this->services['form.typeguesser.validator'] = new \Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser(${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
    }

    /**
     * Gets the public 'fosuser.changepassword.form.factory' shared service.
     *
     * @return \FOS\UserBundle\Form\Factory\FormFactory
     */
    protected function getFosUserChangePasswordFormFactoryService()
    {
        return $this->services['fosuser.changepassword.form.factory'] = new \FOS\UserBundle\Form\Factory\FormFactory(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, 'fosuserchangepasswordform', 'FOS\\UserBundle\\Form\\Type\\ChangePasswordFormType', array(0 => 'ChangePassword', 1 => 'Default'));
    }

    /**
     * Gets the public 'fosuser.changepassword.form.type' shared service.
     *
     * @return \FOS\UserBundle\Form\Type\ChangePasswordFormType
     */
    protected function getFosUserChangePasswordFormTypeService()
    {
        return $this->services['fosuser.changepassword.form.type'] = new \FOS\UserBundle\Form\Type\ChangePasswordFormType('AppBundle\\Entity\\User');
    }

    /**
     * Gets the public 'fosuser.listener.authentication' shared service.
     *
     * @return \FOS\UserBundle\EventListener\AuthenticationListener
     */
    protected function getFosUserListenerAuthenticationService()
    {
        return $this->services['fosuser.listener.authentication'] = new \FOS\UserBundle\EventListener\AuthenticationListener(${($ = isset($this->services['fosuser.security.loginmanager']) ? $this->services['fosuser.security.loginmanager'] : $this->get('fosuser.security.loginmanager')) && false ?: ''}, 'main');
    }

    /**
     * Gets the public 'fosuser.listener.emailconfirmation' shared service.
     *
     * @return \FOS\UserBundle\EventListener\EmailConfirmationListener
     */
    protected function getFosUserListenerEmailConfirmationService()
    {
        return $this->services['fosuser.listener.emailconfirmation'] = new \FOS\UserBundle\EventListener\EmailConfirmationListener(${($ = isset($this->services['fosuser.mailer']) ? $this->services['fosuser.mailer'] : $this->get('fosuser.mailer')) && false ?: ''}, ${($ = isset($this->services['fosuser.util.tokengenerator']) ? $this->services['fosuser.util.tokengenerator'] : $this->get('fosuser.util.tokengenerator')) && false ?: ''}, ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}, ${($ = isset($this->services['session']) ? $this->services['session'] : $this->get('session')) && false ?: ''});
    }

    /**
     * Gets the public 'fosuser.listener.flash' shared service.
     *
     * @return \FOS\UserBundle\EventListener\FlashListener
     */
    protected function getFosUserListenerFlashService()
    {
        return $this->services['fosuser.listener.flash'] = new \FOS\UserBundle\EventListener\FlashListener(${($ = isset($this->services['session']) ? $this->services['session'] : $this->get('session')) && false ?: ''}, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'fosuser.listener.resetting' shared service.
     *
     * @return \FOS\UserBundle\EventListener\ResettingListener
     */
    protected function getFosUserListenerResettingService()
    {
        return $this->services['fosuser.listener.resetting'] = new \FOS\UserBundle\EventListener\ResettingListener(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}, 86400);
    }

    /**
     * Gets the public 'fosuser.mailer' shared service.
     *
     * @return \FOS\UserBundle\Mailer\TwigSwiftMailer
     */
    protected function getFosUserMailerService()
    {
        return $this->services['fosuser.mailer'] = new \FOS\UserBundle\Mailer\TwigSwiftMailer(${($ = isset($this->services['swiftmailer.mailer.default']) ? $this->services['swiftmailer.mailer.default'] : $this->get('swiftmailer.mailer.default')) && false ?: ''}, ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}, ${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, array('template' => array('confirmation' => '@FOSUser/Registration/email.txt.twig', 'resetting' => 'email/passwordresetting.email.twig'), 'fromemail' => array('confirmation' => array('itap.test.for.studens@gmail.com' => 'itap.test.for.studens@gmail.com'), 'resetting' => array('itap.test.for.studens@gmail.com' => 'itap.test.for.studens@gmail.com'))));
    }

    /**
     * Gets the public 'fosuser.profile.form.factory' shared service.
     *
     * @return \FOS\UserBundle\Form\Factory\FormFactory
     */
    protected function getFosUserProfileFormFactoryService()
    {
        return $this->services['fosuser.profile.form.factory'] = new \FOS\UserBundle\Form\Factory\FormFactory(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, 'fosuserprofileform', 'FOS\\UserBundle\\Form\\Type\\ProfileFormType', array(0 => 'Profile', 1 => 'Default'));
    }

    /**
     * Gets the public 'fosuser.profile.form.type' shared service.
     *
     * @return \FOS\UserBundle\Form\Type\ProfileFormType
     */
    protected function getFosUserProfileFormTypeService()
    {
        return $this->services['fosuser.profile.form.type'] = new \FOS\UserBundle\Form\Type\ProfileFormType('AppBundle\\Entity\\User');
    }

    /**
     * Gets the public 'fosuser.registration.form.factory' shared service.
     *
     * @return \FOS\UserBundle\Form\Factory\FormFactory
     */
    protected function getFosUserRegistrationFormFactoryService()
    {
        return $this->services['fosuser.registration.form.factory'] = new \FOS\UserBundle\Form\Factory\FormFactory(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, 'fosuserregistrationform', 'AppBundle\\Form\\RegistrationType', array(0 => 'Registration', 1 => 'Default'));
    }

    /**
     * Gets the public 'fosuser.registration.form.type' shared service.
     *
     * @return \FOS\UserBundle\Form\Type\RegistrationFormType
     */
    protected function getFosUserRegistrationFormTypeService()
    {
        return $this->services['fosuser.registration.form.type'] = new \FOS\UserBundle\Form\Type\RegistrationFormType('AppBundle\\Entity\\User');
    }

    /**
     * Gets the public 'fosuser.resetting.form.factory' shared service.
     *
     * @return \FOS\UserBundle\Form\Factory\FormFactory
     */
    protected function getFosUserResettingFormFactoryService()
    {
        return $this->services['fosuser.resetting.form.factory'] = new \FOS\UserBundle\Form\Factory\FormFactory(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, 'fosuserresettingform', 'FOS\\UserBundle\\Form\\Type\\ResettingFormType', array(0 => 'ResetPassword', 1 => 'Default'));
    }

    /**
     * Gets the public 'fosuser.resetting.form.type' shared service.
     *
     * @return \FOS\UserBundle\Form\Type\ResettingFormType
     */
    protected function getFosUserResettingFormTypeService()
    {
        return $this->services['fosuser.resetting.form.type'] = new \FOS\UserBundle\Form\Type\ResettingFormType('AppBundle\\Entity\\User');
    }

    /**
     * Gets the public 'fosuser.security.interactiveloginlistener' shared service.
     *
     * @return \FOS\UserBundle\EventListener\LastLoginListener
     */
    protected function getFosUserSecurityInteractiveLoginListenerService()
    {
        return $this->services['fosuser.security.interactiveloginlistener'] = new \FOS\UserBundle\EventListener\LastLoginListener(${($ = isset($this->services['fosuser.usermanager']) ? $this->services['fosuser.usermanager'] : $this->get('fosuser.usermanager')) && false ?: ''});
    }

    /**
     * Gets the public 'fosuser.security.loginmanager' shared service.
     *
     * @return \FOS\UserBundle\Security\LoginManager
     */
    protected function getFosUserSecurityLoginManagerService()
    {
        return $this->services['fosuser.security.loginmanager'] = new \FOS\UserBundle\Security\LoginManager(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''}, ${($ = isset($this->services['security.userchecker']) ? $this->services['security.userchecker'] : $this->getSecurityUserCheckerService()) && false ?: ''}, ${($ = isset($this->services['security.authentication.sessionstrategy']) ? $this->services['security.authentication.sessionstrategy'] : $this->getSecurityAuthenticationSessionStrategyService()) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, NULL);
    }

    /**
     * Gets the public 'fosuser.usermanager' shared service.
     *
     * @return \FOS\UserBundle\Doctrine\UserManager
     */
    protected function getFosUserUserManagerService()
    {
        return $this->services['fosuser.usermanager'] = new \FOS\UserBundle\Doctrine\UserManager(${($ = isset($this->services['fosuser.util.passwordupdater']) ? $this->services['fosuser.util.passwordupdater'] : $this->getFosUserUtilPasswordUpdaterService()) && false ?: ''}, ${($ = isset($this->services['fosuser.util.canonicalfieldsupdater']) ? $this->services['fosuser.util.canonicalfieldsupdater'] : $this->getFosUserUtilCanonicalFieldsUpdaterService()) && false ?: ''}, ${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''}->getManager(NULL), 'AppBundle\\Entity\\User');
    }

    /**
     * Gets the public 'fosuser.usernameformtype' shared service.
     *
     * @return \FOS\UserBundle\Form\Type\UsernameFormType
     */
    protected function getFosUserUsernameFormTypeService()
    {
        return $this->services['fosuser.usernameformtype'] = new \FOS\UserBundle\Form\Type\UsernameFormType(new \FOS\UserBundle\Form\DataTransformer\UserToUsernameTransformer(${($ = isset($this->services['fosuser.usermanager']) ? $this->services['fosuser.usermanager'] : $this->get('fosuser.usermanager')) && false ?: ''}));
    }

    /**
     * Gets the public 'fosuser.util.emailcanonicalizer' shared service.
     *
     * @return \FOS\UserBundle\Util\Canonicalizer
     */
    protected function getFosUserUtilEmailCanonicalizerService()
    {
        return $this->services['fosuser.util.emailcanonicalizer'] = new \FOS\UserBundle\Util\Canonicalizer();
    }

    /**
     * Gets the public 'fosuser.util.tokengenerator' shared service.
     *
     * @return \FOS\UserBundle\Util\TokenGenerator
     */
    protected function getFosUserUtilTokenGeneratorService()
    {
        return $this->services['fosuser.util.tokengenerator'] = new \FOS\UserBundle\Util\TokenGenerator();
    }

    /**
     * Gets the public 'fosuser.util.usermanipulator' shared service.
     *
     * @return \FOS\UserBundle\Util\UserManipulator
     */
    protected function getFosUserUtilUserManipulatorService()
    {
        return $this->services['fosuser.util.usermanipulator'] = new \FOS\UserBundle\Util\UserManipulator(${($ = isset($this->services['fosuser.usermanager']) ? $this->services['fosuser.usermanager'] : $this->get('fosuser.usermanager')) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''});
    }

    /**
     * Gets the public 'fragment.handler' shared service.
     *
     * @return \Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler
     */
    protected function getFragmentHandlerService()
    {
        return $this->services['fragment.handler'] = new \Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler(${($ = isset($this->services['servicelocator.e64d23c3bf770e2cf44b71643280668d']) ? $this->services['servicelocator.e64d23c3bf770e2cf44b71643280668d'] : $this->getServiceLocatorE64d23c3bf770e2cf44b71643280668dService()) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, true);
    }

    /**
     * Gets the public 'fragment.listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\FragmentListener
     */
    protected function getFragmentListenerService()
    {
        return $this->services['fragment.listener'] = new \Symfony\Component\HttpKernel\EventListener\FragmentListener(${($ = isset($this->services['urisigner']) ? $this->services['urisigner'] : $this->get('urisigner')) && false ?: ''}, '/fragment');
    }

    /**
     * Gets the public 'fragment.renderer.esi' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer
     */
    protected function getFragmentRendererEsiService()
    {
        $this->services['fragment.renderer.esi'] = $instance = new \Symfony\Component\HttpKernel\Fragment\EsiFragmentRenderer(NULL, ${($ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: ''}, ${($ = isset($this->services['urisigner']) ? $this->services['urisigner'] : $this->get('urisigner')) && false ?: ''});

        $instance->setFragmentPath('/fragment');

        return $instance;
    }

    /**
     * Gets the public 'fragment.renderer.hinclude' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer
     */
    protected function getFragmentRendererHincludeService()
    {
        $this->services['fragment.renderer.hinclude'] = $instance = new \Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, ${($ = isset($this->services['urisigner']) ? $this->services['urisigner'] : $this->get('urisigner')) && false ?: ''}, NULL);

        $instance->setFragmentPath('/fragment');

        return $instance;
    }

    /**
     * Gets the public 'fragment.renderer.inline' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer
     */
    protected function getFragmentRendererInlineService()
    {
        $this->services['fragment.renderer.inline'] = $instance = new \Symfony\Component\HttpKernel\Fragment\InlineFragmentRenderer(${($ = isset($this->services['httpkernel']) ? $this->services['httpkernel'] : $this->get('httpkernel')) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''});

        $instance->setFragmentPath('/fragment');

        return $instance;
    }

    /**
     * Gets the public 'fragment.renderer.ssi' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer
     */
    protected function getFragmentRendererSsiService()
    {
        $this->services['fragment.renderer.ssi'] = $instance = new \Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer(NULL, ${($ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: ''}, ${($ = isset($this->services['urisigner']) ? $this->services['urisigner'] : $this->get('urisigner')) && false ?: ''});

        $instance->setFragmentPath('/fragment');

        return $instance;
    }

    /**
     * Gets the public 'httpkernel' shared service.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernel
     */
    protected function getHttpKernelService()
    {
        return $this->services['httpkernel'] = new \Symfony\Component\HttpKernel\HttpKernel(${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''}, ${($ = isset($this->services['debug.controllerresolver']) ? $this->services['debug.controllerresolver'] : $this->get('debug.controllerresolver')) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, ${($ = isset($this->services['debug.argumentresolver']) ? $this->services['debug.argumentresolver'] : $this->get('debug.argumentresolver')) && false ?: ''});
    }

    /**
     * Gets the public 'kernel.classcache.cachewarmer' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\CacheWarmer\ClassCacheCacheWarmer
     *
     * @deprecated The "kernel.classcache.cachewarmer" option is deprecated since version 3.3, to be removed in 4.0.
     */
    protected function getKernelClassCacheCacheWarmerService()
    {
        @triggererror('The "kernel.classcache.cachewarmer" option is deprecated since version 3.3, to be removed in 4.0.', EUSERDEPRECATED);

        return $this->services['kernel.classcache.cachewarmer'] = new \Symfony\Bundle\FrameworkBundle\CacheWarmer\ClassCacheCacheWarmer(array(0 => 'Symfony\\Component\\HttpFoundation\\ParameterBag', 1 => 'Symfony\\Component\\HttpFoundation\\HeaderBag', 2 => 'Symfony\\Component\\HttpFoundation\\FileBag', 3 => 'Symfony\\Component\\HttpFoundation\\ServerBag', 4 => 'Symfony\\Component\\HttpFoundation\\Request', 5 => 'Symfony\\Component\\HttpKernel\\Kernel'));
    }

    /**
     * Gets the public 'knpmenu.factory' shared service.
     *
     * @return \Knp\Menu\MenuFactory
     */
    protected function getKnpMenuFactoryService()
    {
        $this->services['knpmenu.factory'] = $instance = new \Knp\Menu\MenuFactory();

        $instance->addExtension(new \Knp\Menu\Integration\Symfony\RoutingExtension(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}), 0);

        return $instance;
    }

    /**
     * Gets the public 'knpmenu.listener.voters' shared service.
     *
     * @return \Knp\Bundle\MenuBundle\EventListener\VoterInitializerListener
     */
    protected function getKnpMenuListenerVotersService()
    {
        $this->services['knpmenu.listener.voters'] = $instance = new \Knp\Bundle\MenuBundle\EventListener\VoterInitializerListener();

        $instance->addVoter(${($ = isset($this->services['knpmenu.voter.router']) ? $this->services['knpmenu.voter.router'] : $this->get('knpmenu.voter.router')) && false ?: ''});
        $instance->addVoter(${($ = isset($this->services['sonata.admin.menu.matcher.voter.admin']) ? $this->services['sonata.admin.menu.matcher.voter.admin'] : $this->get('sonata.admin.menu.matcher.voter.admin')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'knpmenu.matcher' shared service.
     *
     * @return \Knp\Menu\Matcher\Matcher
     */
    protected function getKnpMenuMatcherService()
    {
        $this->services['knpmenu.matcher'] = $instance = new \Knp\Menu\Matcher\Matcher();

        $instance->addVoter(${($ = isset($this->services['knpmenu.voter.router']) ? $this->services['knpmenu.voter.router'] : $this->get('knpmenu.voter.router')) && false ?: ''});
        $instance->addVoter(${($ = isset($this->services['sonata.admin.menu.matcher.voter.admin']) ? $this->services['sonata.admin.menu.matcher.voter.admin'] : $this->get('sonata.admin.menu.matcher.voter.admin')) && false ?: ''});
        $instance->addVoter(${($ = isset($this->services['sonata.admin.menu.matcher.voter.children']) ? $this->services['sonata.admin.menu.matcher.voter.children'] : $this->get('sonata.admin.menu.matcher.voter.children')) && false ?: ''});
        $instance->addVoter(${($ = isset($this->services['sonata.admin.menu.matcher.voter.active']) ? $this->services['sonata.admin.menu.matcher.voter.active'] : $this->get('sonata.admin.menu.matcher.voter.active')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'knpmenu.menuprovider' shared service.
     *
     * @return \Knp\Menu\Provider\ChainProvider
     */
    protected function getKnpMenuMenuProviderService()
    {
        $a = ${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''};

        return $this->services['knpmenu.menuprovider'] = new \Knp\Menu\Provider\ChainProvider(array(0 => new \Knp\Bundle\MenuBundle\Provider\ContainerAwareProvider($this, array('sonataadminsidebar' => 'sonata.admin.sidebarmenu')), 1 => new \Knp\Bundle\MenuBundle\Provider\BuilderServiceProvider($this, array()), 2 => new \Knp\Bundle\MenuBundle\Provider\BuilderAliasProvider(${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''}, $this, $a), 3 => new \Sonata\AdminBundle\Menu\Provider\GroupMenuProvider($a, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, ${($ = isset($this->services['security.authorizationchecker']) ? $this->services['security.authorizationchecker'] : $this->get('security.authorizationchecker')) && false ?: ''})));
    }

    /**
     * Gets the public 'knpmenu.renderer.list' shared service.
     *
     * @return \Knp\Menu\Renderer\ListRenderer
     */
    protected function getKnpMenuRendererListService()
    {
        return $this->services['knpmenu.renderer.list'] = new \Knp\Menu\Renderer\ListRenderer(${($ = isset($this->services['knpmenu.matcher']) ? $this->services['knpmenu.matcher'] : $this->get('knpmenu.matcher')) && false ?: ''}, array(), 'UTF-8');
    }

    /**
     * Gets the public 'knpmenu.renderer.twig' shared service.
     *
     * @return \Knp\Menu\Renderer\TwigRenderer
     */
    protected function getKnpMenuRendererTwigService()
    {
        return $this->services['knpmenu.renderer.twig'] = new \Knp\Menu\Renderer\TwigRenderer(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, 'KnpMenuBundle::menu.html.twig', ${($ = isset($this->services['knpmenu.matcher']) ? $this->services['knpmenu.matcher'] : $this->get('knpmenu.matcher')) && false ?: ''}, array());
    }

    /**
     * Gets the public 'knpmenu.rendererprovider' shared service.
     *
     * @return \Knp\Bundle\MenuBundle\Renderer\ContainerAwareProvider
     */
    protected function getKnpMenuRendererProviderService()
    {
        return $this->services['knpmenu.rendererprovider'] = new \Knp\Bundle\MenuBundle\Renderer\ContainerAwareProvider($this, 'twig', array('list' => 'knpmenu.renderer.list', 'twig' => 'knpmenu.renderer.twig'));
    }

    /**
     * Gets the public 'knpmenu.voter.router' shared service.
     *
     * @return \Knp\Menu\Matcher\Voter\RouteVoter
     */
    protected function getKnpMenuVoterRouterService()
    {
        return $this->services['knpmenu.voter.router'] = new \Knp\Menu\Matcher\Voter\RouteVoter();
    }

    /**
     * Gets the public 'localelistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\LocaleListener
     */
    protected function getLocaleListenerService()
    {
        return $this->services['localelistener'] = new \Symfony\Component\HttpKernel\EventListener\LocaleListener(${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, 'en', ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'logger' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getLoggerService()
    {
        $this->services['logger'] = $instance = new \Symfony\Bridge\Monolog\Logger('app');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->useMicrosecondTimestamps(true);
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.activationstrategy.notfound' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy
     */
    protected function getMonologActivationStrategyNotFoundService()
    {
        return $this->services['monolog.activationstrategy.notfound'] = new \Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy();
    }

    /**
     * Gets the public 'monolog.handler.console' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Handler\ConsoleHandler
     */
    protected function getMonologHandlerConsoleService()
    {
        return $this->services['monolog.handler.console'] = new \Symfony\Bridge\Monolog\Handler\ConsoleHandler(NULL, true, array());
    }

    /**
     * Gets the public 'monolog.handler.fingerscrossed.errorlevelactivationstrategy' shared service.
     *
     * @return \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy
     */
    protected function getMonologHandlerFingersCrossedErrorLevelActivationStrategyService()
    {
        return $this->services['monolog.handler.fingerscrossed.errorlevelactivationstrategy'] = new \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy();
    }

    /**
     * Gets the public 'monolog.handler.main' shared service.
     *
     * @return \Monolog\Handler\StreamHandler
     */
    protected function getMonologHandlerMainService()
    {
        $this->services['monolog.handler.main'] = $instance = new \Monolog\Handler\StreamHandler(($this->targetDirs[1].'/var/logs/dev.log'), 100, true, NULL);

        $instance->pushProcessor(new \Monolog\Processor\PsrLogMessageProcessor());

        return $instance;
    }

    /**
     * Gets the public 'monolog.handler.nullinternal' shared service.
     *
     * @return \Monolog\Handler\NullHandler
     */
    protected function getMonologHandlerNullInternalService()
    {
        return $this->services['monolog.handler.nullinternal'] = new \Monolog\Handler\NullHandler();
    }

    /**
     * Gets the public 'monolog.handler.serverlog' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Handler\ServerLogHandler
     */
    protected function getMonologHandlerServerLogService()
    {
        return $this->services['monolog.handler.serverlog'] = new \Symfony\Bridge\Monolog\Handler\ServerLogHandler('127.0.0.1:9911', 100, true);
    }

    /**
     * Gets the public 'monolog.logger.cache' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerCacheService()
    {
        $this->services['monolog.logger.cache'] = $instance = new \Symfony\Bridge\Monolog\Logger('cache');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.console' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerConsoleService()
    {
        $this->services['monolog.logger.console'] = $instance = new \Symfony\Bridge\Monolog\Logger('console');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.doctrine' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerDoctrineService()
    {
        $this->services['monolog.logger.doctrine'] = $instance = new \Symfony\Bridge\Monolog\Logger('doctrine');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.event' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerEventService()
    {
        $this->services['monolog.logger.event'] = $instance = new \Symfony\Bridge\Monolog\Logger('event');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.php' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerPhpService()
    {
        $this->services['monolog.logger.php'] = $instance = new \Symfony\Bridge\Monolog\Logger('php');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.profiler' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerProfilerService()
    {
        $this->services['monolog.logger.profiler'] = $instance = new \Symfony\Bridge\Monolog\Logger('profiler');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.request' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerRequestService()
    {
        $this->services['monolog.logger.request'] = $instance = new \Symfony\Bridge\Monolog\Logger('request');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.router' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerRouterService()
    {
        $this->services['monolog.logger.router'] = $instance = new \Symfony\Bridge\Monolog\Logger('router');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.security' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerSecurityService()
    {
        $this->services['monolog.logger.security'] = $instance = new \Symfony\Bridge\Monolog\Logger('security');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.templating' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerTemplatingService()
    {
        $this->services['monolog.logger.templating'] = $instance = new \Symfony\Bridge\Monolog\Logger('templating');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'monolog.logger.translation' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonologLoggerTranslationService()
    {
        $this->services['monolog.logger.translation'] = $instance = new \Symfony\Bridge\Monolog\Logger('translation');

        $instance->pushProcessor(${($ = isset($this->services['debug.logprocessor']) ? $this->services['debug.logprocessor'] : $this->getDebugLogProcessorService()) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.serverlog']) ? $this->services['monolog.handler.serverlog'] : $this->get('monolog.handler.serverlog')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.console']) ? $this->services['monolog.handler.console'] : $this->get('monolog.handler.console')) && false ?: ''});
        $instance->pushHandler(${($ = isset($this->services['monolog.handler.main']) ? $this->services['monolog.handler.main'] : $this->get('monolog.handler.main')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'profiler' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Profiler\Profiler
     */
    protected function getProfilerService()
    {
        $a = ${($ = isset($this->services['monolog.logger.profiler']) ? $this->services['monolog.logger.profiler'] : $this->get('monolog.logger.profiler', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        $b = ${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};

        $c = new \Symfony\Component\Cache\DataCollector\CacheDataCollector();
        $c->addInstance('cache.app', ${($ = isset($this->services['cache.app']) ? $this->services['cache.app'] : $this->get('cache.app')) && false ?: ''});
        $c->addInstance('cache.system', ${($ = isset($this->services['cache.system']) ? $this->services['cache.system'] : $this->get('cache.system')) && false ?: ''});
        $c->addInstance('cache.validator', ${($ = isset($this->services['cache.validator']) ? $this->services['cache.validator'] : $this->getCacheValidatorService()) && false ?: ''});
        $c->addInstance('cache.serializer', new \Symfony\Component\Cache\Adapter\TraceableAdapter(${($ = isset($this->services['cache.serializer.recorderinner']) ? $this->services['cache.serializer.recorderinner'] : $this->getCacheSerializerRecorderInnerService()) && false ?: ''}));
        $c->addInstance('cache.annotations', ${($ = isset($this->services['cache.annotations']) ? $this->services['cache.annotations'] : $this->getCacheAnnotationsService()) && false ?: ''});

        $d = new \Doctrine\Bundle\DoctrineBundle\DataCollector\DoctrineDataCollector(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
        $d->addLogger('default', ${($ = isset($this->services['doctrine.dbal.logger.profiling.default']) ? $this->services['doctrine.dbal.logger.profiling.default'] : $this->getDoctrineDbalLoggerProfilingDefaultService()) && false ?: ''});

        $e = new \Symfony\Component\HttpKernel\DataCollector\ConfigDataCollector();
        if ($this->has('kernel')) {
            $e->setKernel($b);
        }

        $this->services['profiler'] = $instance = new \Symfony\Component\HttpKernel\Profiler\Profiler(new \Symfony\Component\HttpKernel\Profiler\FileProfilerStorage(('file:'.DIR.'/profiler')), $a);

        $instance->add(${($ = isset($this->services['datacollector.request']) ? $this->services['datacollector.request'] : $this->get('datacollector.request')) && false ?: ''});
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\TimeDataCollector($b, ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}));
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\MemoryDataCollector());
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\AjaxDataCollector());
        $instance->add(${($ = isset($this->services['datacollector.form']) ? $this->services['datacollector.form'] : $this->get('datacollector.form')) && false ?: ''});
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\ExceptionDataCollector());
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\LoggerDataCollector($a, (DIR.'/appDevDebugProjectContaine')));
        $instance->add(new \Symfony\Component\HttpKernel\DataCollector\EventDataCollector(${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}));
        $instance->add(${($ = isset($this->services['datacollector.router']) ? $this->services['datacollector.router'] : $this->get('datacollector.router')) && false ?: ''});
        $instance->add($c);
        $instance->add(${($ = isset($this->services['datacollector.translation']) ? $this->services['datacollector.translation'] : $this->get('datacollector.translation')) && false ?: ''});
        $instance->add(new \Symfony\Bundle\SecurityBundle\DataCollector\SecurityDataCollector(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['security.rolehierarchy']) ? $this->services['security.rolehierarchy'] : $this->getSecurityRoleHierarchyService()) && false ?: ''}, ${($ = isset($this->services['security.logouturlgenerator']) ? $this->services['security.logouturlgenerator'] : $this->getSecurityLogoutUrlGeneratorService()) && false ?: ''}, ${($ = isset($this->services['debug.security.access.decisionmanager']) ? $this->services['debug.security.access.decisionmanager'] : $this->getDebugSecurityAccessDecisionManagerService()) && false ?: ''}, ${($ = isset($this->services['security.firewall.map']) ? $this->services['security.firewall.map'] : $this->getSecurityFirewallMapService()) && false ?: ''}));
        $instance->add(new \Symfony\Bridge\Twig\DataCollector\TwigDataCollector(${($ = isset($this->services['twig.profile']) ? $this->services['twig.profile'] : $this->get('twig.profile')) && false ?: ''}));
        $instance->add($d);
        $instance->add(new \Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector($this));
        $instance->add(${($ = isset($this->services['datacollector.dump']) ? $this->services['datacollector.dump'] : $this->get('datacollector.dump')) && false ?: ''});
        $instance->add(new \Sonata\BlockBundle\Profiler\DataCollector\BlockDataCollector(${($ = isset($this->services['sonata.block.templating.helper']) ? $this->services['sonata.block.templating.helper'] : $this->get('sonata.block.templating.helper')) && false ?: ''}, array(0 => 'sonata.block.service.container', 1 => 'sonata.page.block.container', 2 => 'sonata.dashboard.block.container', 3 => 'cmf.block.container', 4 => 'cmf.block.slideshow')));
        $instance->add($e);

        return $instance;
    }

    /**
     * Gets the public 'profilerlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ProfilerListener
     */
    protected function getProfilerListenerService()
    {
        return $this->services['profilerlistener'] = new \Symfony\Component\HttpKernel\EventListener\ProfilerListener(${($ = isset($this->services['profiler']) ? $this->services['profiler'] : $this->get('profiler')) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, NULL, false, false);
    }

    /**
     * Gets the public 'propertyaccessor' shared service.
     *
     * @return \Symfony\Component\PropertyAccess\PropertyAccessor
     */
    protected function getPropertyAccessorService()
    {
        return $this->services['propertyaccessor'] = new \Symfony\Component\PropertyAccess\PropertyAccessor(false, false, new \Symfony\Component\Cache\Adapter\ArrayAdapter(0, false));
    }

    /**
     * Gets the public 'requeststack' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\RequestStack
     */
    protected function getRequestStackService()
    {
        return $this->services['requeststack'] = new \Symfony\Component\HttpFoundation\RequestStack();
    }

    /**
     * Gets the public 'responselistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ResponseListener
     */
    protected function getResponseListenerService()
    {
        return $this->services['responselistener'] = new \Symfony\Component\HttpKernel\EventListener\ResponseListener('UTF-8');
    }

    /**
     * Gets the public 'router' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected function getRouterService()
    {
        $this->services['router'] = $instance = new \Symfony\Bundle\FrameworkBundle\Routing\Router($this, ($this->targetDirs[1].'/app/config/routingdev.yml'), array('cachedir' => DIR, 'debug' => true, 'generatorclass' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generatorbaseclass' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator', 'generatordumperclass' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper', 'generatorcacheclass' => 'appDevDebugProjectContaineUrlGenerator', 'matcherclass' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcherbaseclass' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher', 'matcherdumperclass' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper', 'matchercacheclass' => 'appDevDebugProjectContaineUrlMatcher', 'strictrequirements' => true), ${($ = isset($this->services['router.requestcontext']) ? $this->services['router.requestcontext'] : $this->getRouterRequestContextService()) && false ?: ''}, ${($ = isset($this->services['monolog.logger.router']) ? $this->services['monolog.logger.router'] : $this->get('monolog.logger.router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});

        $instance->setConfigCacheFactory(${($ = isset($this->services['configcachefactory']) ? $this->services['configcachefactory'] : $this->get('configcachefactory')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'routerlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\RouterListener
     */
    protected function getRouterListenerService()
    {
        return $this->services['routerlistener'] = new \Symfony\Component\HttpKernel\EventListener\RouterListener(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''}, ${($ = isset($this->services['router.requestcontext']) ? $this->services['router.requestcontext'] : $this->getRouterRequestContextService()) && false ?: ''}, ${($ = isset($this->services['monolog.logger.request']) ? $this->services['monolog.logger.request'] : $this->get('monolog.logger.request', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'routing.loader' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader
     */
    protected function getRoutingLoaderService()
    {
        $a = ${($ = isset($this->services['filelocator']) ? $this->services['filelocator'] : $this->get('filelocator')) && false ?: ''};
        $b = ${($ = isset($this->services['annotationreader']) ? $this->services['annotationreader'] : $this->get('annotationreader')) && false ?: ''};

        $c = new \Sensio\Bundle\FrameworkExtraBundle\Routing\AnnotatedRouteControllerLoader($b);

        $d = new \Symfony\Component\Config\Loader\LoaderResolver();
        $d->addLoader(new \Symfony\Component\Routing\Loader\XmlFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\YamlFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\PhpFileLoader($a));
        $d->addLoader(new \Symfony\Component\Config\Loader\GlobFileLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\DirectoryLoader($a));
        $d->addLoader(new \Symfony\Component\Routing\Loader\DependencyInjection\ServiceRouterLoader($this));
        $d->addLoader(new \Symfony\Component\Routing\Loader\AnnotationDirectoryLoader($a, $c));
        $d->addLoader(new \Symfony\Component\Routing\Loader\AnnotationFileLoader($a, $c));
        $d->addLoader($c);
        $d->addLoader(${($ = isset($this->services['sonata.admin.routeloader']) ? $this->services['sonata.admin.routeloader'] : $this->get('sonata.admin.routeloader')) && false ?: ''});

        return $this->services['routing.loader'] = new \Symfony\Bundle\FrameworkBundle\Routing\DelegatingLoader(${($ = isset($this->services['controllernameconverter']) ? $this->services['controllernameconverter'] : $this->getControllerNameConverterService()) && false ?: ''}, $d);
    }

    /**
     * Gets the public 'security.authentication.guardhandler' shared service.
     *
     * @return \Symfony\Component\Security\Guard\GuardAuthenticatorHandler
     */
    protected function getSecurityAuthenticationGuardHandlerService()
    {
        return $this->services['security.authentication.guardhandler'] = new \Symfony\Component\Security\Guard\GuardAuthenticatorHandler(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'security.authenticationutils' shared service.
     *
     * @return \Symfony\Component\Security\Http\Authentication\AuthenticationUtils
     */
    protected function getSecurityAuthenticationUtilsService()
    {
        return $this->services['security.authenticationutils'] = new \Symfony\Component\Security\Http\Authentication\AuthenticationUtils(${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''});
    }

    /**
     * Gets the public 'security.authorizationchecker' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    protected function getSecurityAuthorizationCheckerService()
    {
        return $this->services['security.authorizationchecker'] = new \Symfony\Component\Security\Core\Authorization\AuthorizationChecker(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''}, ${($ = isset($this->services['security.authentication.manager']) ? $this->services['security.authentication.manager'] : $this->getSecurityAuthenticationManagerService()) && false ?: ''}, ${($ = isset($this->services['debug.security.access.decisionmanager']) ? $this->services['debug.security.access.decisionmanager'] : $this->getDebugSecurityAccessDecisionManagerService()) && false ?: ''}, false);
    }

    /**
     * Gets the public 'security.csrf.tokenmanager' shared service.
     *
     * @return \Symfony\Component\Security\Csrf\CsrfTokenManager
     */
    protected function getSecurityCsrfTokenManagerService()
    {
        return $this->services['security.csrf.tokenmanager'] = new \Symfony\Component\Security\Csrf\CsrfTokenManager(new \Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator(), new \Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage(${($ = isset($this->services['session']) ? $this->services['session'] : $this->get('session')) && false ?: ''}));
    }

    /**
     * Gets the public 'security.encoderfactory' shared service.
     *
     * @return \Symfony\Component\Security\Core\Encoder\EncoderFactory
     */
    protected function getSecurityEncoderFactoryService()
    {
        return $this->services['security.encoderfactory'] = new \Symfony\Component\Security\Core\Encoder\EncoderFactory(array('FOS\\UserBundle\\Model\\UserInterface' => array('class' => 'Symfony\\Component\\Security\\Core\\Encoder\\BCryptPasswordEncoder', 'arguments' => array(0 => 13))));
    }

    /**
     * Gets the public 'security.firewall' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener
     */
    protected function getSecurityFirewallService()
    {
        return $this->services['security.firewall'] = new \Symfony\Bundle\SecurityBundle\EventListener\FirewallListener(${($ = isset($this->services['security.firewall.map']) ? $this->services['security.firewall.map'] : $this->getSecurityFirewallMapService()) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''}, ${($ = isset($this->services['security.logouturlgenerator']) ? $this->services['security.logouturlgenerator'] : $this->getSecurityLogoutUrlGeneratorService()) && false ?: ''});
    }

    /**
     * Gets the public 'security.firewall.map.context.main' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallContext
     */
    protected function getSecurityFirewallMapContextMainService()
    {
        $a = ${($ = isset($this->services['monolog.logger.security']) ? $this->services['monolog.logger.security'] : $this->get('monolog.logger.security', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        $b = ${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''};
        $c = ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        $d = ${($ = isset($this->services['security.authentication.trustresolver']) ? $this->services['security.authentication.trustresolver'] : $this->getSecurityAuthenticationTrustResolverService()) && false ?: ''};
        $e = ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        $f = ${($ = isset($this->services['httpkernel']) ? $this->services['httpkernel'] : $this->get('httpkernel')) && false ?: ''};
        $g = ${($ = isset($this->services['security.authentication.manager']) ? $this->services['security.authentication.manager'] : $this->getSecurityAuthenticationManagerService()) && false ?: ''};

        $h = new \Symfony\Component\HttpFoundation\RequestMatcher('^login$');

        $i = new \Symfony\Component\HttpFoundation\RequestMatcher('^/register');

        $j = new \Symfony\Component\HttpFoundation\RequestMatcher('^/resetting');

        $k = new \Symfony\Component\HttpFoundation\RequestMatcher('^/admin/');

        $l = new \Symfony\Component\HttpFoundation\RequestMatcher('/generate-article');

        $m = new \Symfony\Component\Security\Http\AccessMap();
        $m->add($h, array(0 => 'ISAUTHENTICATEDANONYMOUSLY'), NULL);
        $m->add($i, array(0 => 'ISAUTHENTICATEDANONYMOUSLY'), NULL);
        $m->add($j, array(0 => 'ISAUTHENTICATEDANONYMOUSLY'), NULL);
        $m->add($k, array(0 => 'ROLEADMIN'), NULL);
        $m->add($l, array(0 => 'ROLEUSER'), NULL);

        $n = new \Symfony\Component\Security\Http\HttpUtils($e, $e);

        $o = new \Symfony\Component\Security\Http\Firewall\LogoutListener($b, $n, new \Symfony\Component\Security\Http\Logout\DefaultLogoutSuccessHandler($n, '/login'), array('csrfparameter' => 'csrftoken', 'csrftokenid' => 'logout', 'logoutpath' => '/logout'));
        $o->addHandler(new \Symfony\Component\Security\Http\Logout\SessionLogoutHandler());

        $p = new \Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler($n, array());
        $p->setOptions(array('defaulttargetpath' => '/', 'alwaysusedefaulttargetpath' => false, 'loginpath' => '/login', 'targetpathparameter' => 'targetpath', 'usereferer' => false));
        $p->setProviderKey('main');

        $q = new \Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler($f, $n, array(), $a);
        $q->setOptions(array('loginpath' => '/login', 'failurepath' => NULL, 'failureforward' => false, 'failurepathparameter' => 'failurepath'));

        return $this->services['security.firewall.map.context.main'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallContext(array(0 => new \Symfony\Component\Security\Http\Firewall\ChannelListener($m, new \Symfony\Component\Security\Http\EntryPoint\RetryAuthenticationEntryPoint(80, 443), $a), 1 => new \Symfony\Component\Security\Http\Firewall\ContextListener($b, array(0 => ${($ = isset($this->services['fosuser.userprovider.usernameemail']) ? $this->services['fosuser.userprovider.usernameemail'] : $this->getFosUserUserProviderUsernameEmailService()) && false ?: ''}), 'main', $a, $c, $d), 2 => $o, 3 => new \Symfony\Component\Security\Http\Firewall\UsernamePasswordFormAuthenticationListener($b, $g, ${($ = isset($this->services['security.authentication.sessionstrategy']) ? $this->services['security.authentication.sessionstrategy'] : $this->getSecurityAuthenticationSessionStrategyService()) && false ?: ''}, $n, 'main', $p, $q, array('checkpath' => '/logincheck', 'useforward' => false, 'requireprevioussession' => true, 'usernameparameter' => 'username', 'passwordparameter' => 'password', 'csrfparameter' => 'csrftoken', 'csrftokenid' => 'authenticate', 'postonly' => true), $a, $c, ${($ = isset($this->services['security.csrf.tokenmanager']) ? $this->services['security.csrf.tokenmanager'] : $this->get('security.csrf.tokenmanager')) && false ?: ''}), 4 => new \Symfony\Component\Security\Http\Firewall\AnonymousAuthenticationListener($b, '599edd120b95d0.75981985', $a, $g), 5 => new \Symfony\Component\Security\Http\Firewall\AccessListener($b, ${($ = isset($this->services['debug.security.access.decisionmanager']) ? $this->services['debug.security.access.decisionmanager'] : $this->getDebugSecurityAccessDecisionManagerService()) && false ?: ''}, $m, $g)), new \Symfony\Component\Security\Http\Firewall\ExceptionListener($b, $d, $n, 'main', new \Symfony\Component\Security\Http\EntryPoint\FormAuthenticationEntryPoint($f, $n, '/login', false), NULL, NULL, $a, false), new \Symfony\Bundle\SecurityBundle\Security\FirewallConfig('main', 'security.userchecker', 'security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db', true, false, 'fosuser.userprovider.usernameemail', 'main', 'security.authentication.formentrypoint.main', NULL, NULL, array(0 => 'logout', 1 => 'formlogin', 2 => 'anonymous')));
    }

    /**
     * Gets the public 'security.passwordencoder' shared service.
     *
     * @return \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder
     */
    protected function getSecurityPasswordEncoderService()
    {
        return $this->services['security.passwordencoder'] = new \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder(${($ = isset($this->services['security.encoderfactory']) ? $this->services['security.encoderfactory'] : $this->get('security.encoderfactory')) && false ?: ''});
    }

    /**
     * Gets the public 'security.rememberme.responselistener' shared service.
     *
     * @return \Symfony\Component\Security\Http\RememberMe\ResponseListener
     */
    protected function getSecurityRemembermeResponseListenerService()
    {
        return $this->services['security.rememberme.responselistener'] = new \Symfony\Component\Security\Http\RememberMe\ResponseListener();
    }

    /**
     * Gets the public 'security.tokenstorage' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    protected function getSecurityTokenStorageService()
    {
        return $this->services['security.tokenstorage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage();
    }

    /**
     * Gets the public 'security.validator.userpassword' shared service.
     *
     * @return \Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator
     */
    protected function getSecurityValidatorUserPasswordService()
    {
        return $this->services['security.validator.userpassword'] = new \Symfony\Component\Security\Core\Validator\Constraints\UserPasswordValidator(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''}, ${($ = isset($this->services['security.encoderfactory']) ? $this->services['security.encoderfactory'] : $this->get('security.encoderfactory')) && false ?: ''});
    }

    /**
     * Gets the public 'sensiodistribution.securitychecker' shared service.
     *
     * @return \SensioLabs\Security\SecurityChecker
     */
    protected function getSensioDistributionSecurityCheckerService()
    {
        return $this->services['sensiodistribution.securitychecker'] = new \SensioLabs\Security\SecurityChecker();
    }

    /**
     * Gets the public 'sensiodistribution.securitychecker.command' shared service.
     *
     * @return \SensioLabs\Security\Command\SecurityCheckerCommand
     */
    protected function getSensioDistributionSecurityCheckerCommandService()
    {
        return $this->services['sensiodistribution.securitychecker.command'] = new \SensioLabs\Security\Command\SecurityCheckerCommand(${($ = isset($this->services['sensiodistribution.securitychecker']) ? $this->services['sensiodistribution.securitychecker'] : $this->get('sensiodistribution.securitychecker')) && false ?: ''});
    }

    /**
     * Gets the public 'sensioframeworkextra.cache.listener' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener
     */
    protected function getSensioFrameworkExtraCacheListenerService()
    {
        return $this->services['sensioframeworkextra.cache.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\HttpCacheListener();
    }

    /**
     * Gets the public 'sensioframeworkextra.controller.listener' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener
     */
    protected function getSensioFrameworkExtraControllerListenerService()
    {
        return $this->services['sensioframeworkextra.controller.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\ControllerListener(${($ = isset($this->services['annotationreader']) ? $this->services['annotationreader'] : $this->get('annotationreader')) && false ?: ''});
    }

    /**
     * Gets the public 'sensioframeworkextra.converter.datetime' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DateTimeParamConverter
     */
    protected function getSensioFrameworkExtraConverterDatetimeService()
    {
        return $this->services['sensioframeworkextra.converter.datetime'] = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DateTimeParamConverter();
    }

    /**
     * Gets the public 'sensioframeworkextra.converter.doctrine.orm' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter
     */
    protected function getSensioFrameworkExtraConverterDoctrineOrmService()
    {
        return $this->services['sensioframeworkextra.converter.doctrine.orm'] = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\DoctrineParamConverter(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'sensioframeworkextra.converter.listener' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener
     */
    protected function getSensioFrameworkExtraConverterListenerService()
    {
        return $this->services['sensioframeworkextra.converter.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\ParamConverterListener(${($ = isset($this->services['sensioframeworkextra.converter.manager']) ? $this->services['sensioframeworkextra.converter.manager'] : $this->get('sensioframeworkextra.converter.manager')) && false ?: ''}, true);
    }

    /**
     * Gets the public 'sensioframeworkextra.converter.manager' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager
     */
    protected function getSensioFrameworkExtraConverterManagerService()
    {
        $this->services['sensioframeworkextra.converter.manager'] = $instance = new \Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterManager();

        $instance->add(${($ = isset($this->services['sensioframeworkextra.converter.doctrine.orm']) ? $this->services['sensioframeworkextra.converter.doctrine.orm'] : $this->get('sensioframeworkextra.converter.doctrine.orm')) && false ?: ''}, 0, 'doctrine.orm');
        $instance->add(${($ = isset($this->services['sensioframeworkextra.converter.datetime']) ? $this->services['sensioframeworkextra.converter.datetime'] : $this->get('sensioframeworkextra.converter.datetime')) && false ?: ''}, 0, 'datetime');

        return $instance;
    }

    /**
     * Gets the public 'sensioframeworkextra.security.listener' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener
     */
    protected function getSensioFrameworkExtraSecurityListenerService()
    {
        return $this->services['sensioframeworkextra.security.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\SecurityListener(NULL, new \Sensio\Bundle\FrameworkExtraBundle\Security\ExpressionLanguage(), ${($ = isset($this->services['security.authentication.trustresolver']) ? $this->services['security.authentication.trustresolver'] : $this->getSecurityAuthenticationTrustResolverService()) && false ?: ''}, ${($ = isset($this->services['security.rolehierarchy']) ? $this->services['security.rolehierarchy'] : $this->getSecurityRoleHierarchyService()) && false ?: ''}, ${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['security.authorizationchecker']) ? $this->services['security.authorizationchecker'] : $this->get('security.authorizationchecker', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'sensioframeworkextra.view.guesser' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser
     */
    protected function getSensioFrameworkExtraViewGuesserService()
    {
        return $this->services['sensioframeworkextra.view.guesser'] = new \Sensio\Bundle\FrameworkExtraBundle\Templating\TemplateGuesser(${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''});
    }

    /**
     * Gets the public 'sensioframeworkextra.view.listener' shared service.
     *
     * @return \Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener
     */
    protected function getSensioFrameworkExtraViewListenerService()
    {
        return $this->services['sensioframeworkextra.view.listener'] = new \Sensio\Bundle\FrameworkExtraBundle\EventListener\TemplateListener($this);
    }

    /**
     * Gets the public 'session' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    protected function getSessionService()
    {
        return $this->services['session'] = new \Symfony\Component\HttpFoundation\Session\Session(${($ = isset($this->services['session.storage.native']) ? $this->services['session.storage.native'] : $this->get('session.storage.native')) && false ?: ''}, new \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag(), new \Symfony\Component\HttpFoundation\Session\Flash\FlashBag());
    }

    /**
     * Gets the public 'session.handler' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler
     */
    protected function getSessionHandlerService()
    {
        return $this->services['session.handler'] = new \Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler(($this->targetDirs[1].'/var/sessions/dev'));
    }

    /**
     * Gets the public 'session.savelistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\SaveSessionListener
     */
    protected function getSessionSaveListenerService()
    {
        return $this->services['session.savelistener'] = new \Symfony\Component\HttpKernel\EventListener\SaveSessionListener();
    }

    /**
     * Gets the public 'session.storage.filesystem' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage
     */
    protected function getSessionStorageFilesystemService()
    {
        return $this->services['session.storage.filesystem'] = new \Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage((DIR.'/sessions'), 'MOCKSESSID', ${($ = isset($this->services['session.storage.metadatabag']) ? $this->services['session.storage.metadatabag'] : $this->getSessionStorageMetadataBagService()) && false ?: ''});
    }

    /**
     * Gets the public 'session.storage.native' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage
     */
    protected function getSessionStorageNativeService()
    {
        return $this->services['session.storage.native'] = new \Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage(array('cookiehttponly' => true, 'gcprobability' => 1), ${($ = isset($this->services['session.handler']) ? $this->services['session.handler'] : $this->get('session.handler')) && false ?: ''}, ${($ = isset($this->services['session.storage.metadatabag']) ? $this->services['session.storage.metadatabag'] : $this->getSessionStorageMetadataBagService()) && false ?: ''});
    }

    /**
     * Gets the public 'session.storage.phpbridge' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage
     */
    protected function getSessionStoragePhpBridgeService()
    {
        return $this->services['session.storage.phpbridge'] = new \Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage(${($ = isset($this->services['session.handler']) ? $this->services['session.handler'] : $this->get('session.handler')) && false ?: ''}, ${($ = isset($this->services['session.storage.metadatabag']) ? $this->services['session.storage.metadatabag'] : $this->getSessionStorageMetadataBagService()) && false ?: ''});
    }

    /**
     * Gets the public 'sessionlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\SessionListener
     */
    protected function getSessionListenerService()
    {
        return $this->services['sessionlistener'] = new \Symfony\Component\HttpKernel\EventListener\SessionListener(new \Symfony\Component\DependencyInjection\ServiceLocator(array('session' => function () {
            return ${($ = isset($this->services['session']) ? $this->services['session'] : $this->get('session', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        })));
    }

    /**
     * Gets the public 'sonata.admin.audit.manager' shared service.
     *
     * @return \Sonata\AdminBundle\Model\AuditManager
     */
    protected function getSonataAdminAuditManagerService()
    {
        return $this->services['sonata.admin.audit.manager'] = new \Sonata\AdminBundle\Model\AuditManager($this);
    }

    /**
     * Gets the public 'sonata.admin.block.adminlist' shared service.
     *
     * @return \Sonata\AdminBundle\Block\AdminListBlockService
     */
    protected function getSonataAdminBlockAdminListService()
    {
        return $this->services['sonata.admin.block.adminlist'] = new \Sonata\AdminBundle\Block\AdminListBlockService('sonata.admin.block.adminlist', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.block.searchresult' shared service.
     *
     * @return \Sonata\AdminBundle\Block\AdminSearchBlockService
     */
    protected function getSonataAdminBlockSearchResultService()
    {
        return $this->services['sonata.admin.block.searchresult'] = new \Sonata\AdminBundle\Block\AdminSearchBlockService('sonata.admin.block.searchresult', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.search.handler']) ? $this->services['sonata.admin.search.handler'] : $this->get('sonata.admin.search.handler')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.block.stats' shared service.
     *
     * @return \Sonata\AdminBundle\Block\AdminStatsBlockService
     */
    protected function getSonataAdminBlockStatsService()
    {
        return $this->services['sonata.admin.block.stats'] = new \Sonata\AdminBundle\Block\AdminStatsBlockService('sonata.admin.block.stats', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.breadcrumbsbuilder' shared service.
     *
     * @return \Sonata\AdminBundle\Admin\BreadcrumbsBuilder
     */
    protected function getSonataAdminBreadcrumbsBuilderService()
    {
        return $this->services['sonata.admin.breadcrumbsbuilder'] = new \Sonata\AdminBundle\Admin\BreadcrumbsBuilder(array('childadminroute' => 'edit'));
    }

    /**
     * Gets the public 'sonata.admin.builder.filter.factory' shared service.
     *
     * @return \Sonata\AdminBundle\Filter\FilterFactory
     */
    protected function getSonataAdminBuilderFilterFactoryService()
    {
        return $this->services['sonata.admin.builder.filter.factory'] = new \Sonata\AdminBundle\Filter\FilterFactory($this, array('Sonata\\DoctrineORMAdminBundle\\Filter\\BooleanFilter' => 'sonata.admin.orm.filter.type.boolean', 'doctrineormboolean' => 'sonata.admin.orm.filter.type.boolean', 'Sonata\\DoctrineORMAdminBundle\\Filter\\CallbackFilter' => 'sonata.admin.orm.filter.type.callback', 'doctrineormcallback' => 'sonata.admin.orm.filter.type.callback', 'Sonata\\DoctrineORMAdminBundle\\Filter\\ChoiceFilter' => 'sonata.admin.orm.filter.type.choice', 'doctrineormchoice' => 'sonata.admin.orm.filter.type.choice', 'Sonata\\DoctrineORMAdminBundle\\Filter\\ModelFilter' => 'sonata.admin.orm.filter.type.model', 'doctrineormmodel' => 'sonata.admin.orm.filter.type.model', 'Sonata\\DoctrineORMAdminBundle\\Filter\\ModelAutocompleteFilter' => 'sonata.admin.orm.filter.type.modelautocomplete', 'doctrineormmodelautocomplete' => 'sonata.admin.orm.filter.type.modelautocomplete', 'Sonata\\DoctrineORMAdminBundle\\Filter\\StringFilter' => 'sonata.admin.orm.filter.type.string', 'doctrineormstring' => 'sonata.admin.orm.filter.type.string', 'Sonata\\DoctrineORMAdminBundle\\Filter\\NumberFilter' => 'sonata.admin.orm.filter.type.number', 'doctrineormnumber' => 'sonata.admin.orm.filter.type.number', 'Sonata\\DoctrineORMAdminBundle\\Filter\\DateFilter' => 'sonata.admin.orm.filter.type.date', 'doctrineormdate' => 'sonata.admin.orm.filter.type.date', 'Sonata\\DoctrineORMAdminBundle\\Filter\\DateRangeFilter' => 'sonata.admin.orm.filter.type.daterange', 'doctrineormdaterange' => 'sonata.admin.orm.filter.type.daterange', 'Sonata\\DoctrineORMAdminBundle\\Filter\\DateTimeFilter' => 'sonata.admin.orm.filter.type.datetime', 'doctrineormdatetime' => 'sonata.admin.orm.filter.type.datetime', 'Sonata\\DoctrineORMAdminBundle\\Filter\\TimeFilter' => 'sonata.admin.orm.filter.type.time', 'doctrineormtime' => 'sonata.admin.orm.filter.type.time', 'Sonata\\DoctrineORMAdminBundle\\Filter\\DateTimeRangeFilter' => 'sonata.admin.orm.filter.type.datetimerange', 'doctrineormdatetimerange' => 'sonata.admin.orm.filter.type.datetimerange', 'Sonata\\DoctrineORMAdminBundle\\Filter\\ClassFilter' => 'sonata.admin.orm.filter.type.class', 'doctrineormclass' => 'sonata.admin.orm.filter.type.class'));
    }

    /**
     * Gets the public 'sonata.admin.builder.ormdatagrid' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Builder\DatagridBuilder
     */
    protected function getSonataAdminBuilderOrmDatagridService()
    {
        return $this->services['sonata.admin.builder.ormdatagrid'] = new \Sonata\DoctrineORMAdminBundle\Builder\DatagridBuilder(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.builder.filter.factory']) ? $this->services['sonata.admin.builder.filter.factory'] : $this->get('sonata.admin.builder.filter.factory')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.guesser.ormdatagridchain']) ? $this->services['sonata.admin.guesser.ormdatagridchain'] : $this->get('sonata.admin.guesser.ormdatagridchain')) && false ?: ''}, true);
    }

    /**
     * Gets the public 'sonata.admin.builder.ormform' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Builder\FormContractor
     */
    protected function getSonataAdminBuilderOrmFormService()
    {
        return $this->services['sonata.admin.builder.ormform'] = new \Sonata\DoctrineORMAdminBundle\Builder\FormContractor(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.builder.ormlist' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Builder\ListBuilder
     */
    protected function getSonataAdminBuilderOrmListService()
    {
        return $this->services['sonata.admin.builder.ormlist'] = new \Sonata\DoctrineORMAdminBundle\Builder\ListBuilder(${($ = isset($this->services['sonata.admin.guesser.ormlistchain']) ? $this->services['sonata.admin.guesser.ormlistchain'] : $this->get('sonata.admin.guesser.ormlistchain')) && false ?: ''}, array('array' => 'SonataAdminBundle:CRUD:listarray.html.twig', 'boolean' => 'SonataAdminBundle:CRUD:listboolean.html.twig', 'date' => 'SonataAdminBundle:CRUD:listdate.html.twig', 'time' => 'SonataAdminBundle:CRUD:listtime.html.twig', 'datetime' => 'SonataAdminBundle:CRUD:listdatetime.html.twig', 'text' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'textarea' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'email' => 'SonataAdminBundle:CRUD:listemail.html.twig', 'trans' => 'SonataAdminBundle:CRUD:listtrans.html.twig', 'string' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'smallint' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'bigint' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'integer' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'decimal' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'identifier' => 'SonataAdminBundle:CRUD:liststring.html.twig', 'currency' => 'SonataAdminBundle:CRUD:listcurrency.html.twig', 'percent' => 'SonataAdminBundle:CRUD:listpercent.html.twig', 'choice' => 'SonataAdminBundle:CRUD:listchoice.html.twig', 'url' => 'SonataAdminBundle:CRUD:listurl.html.twig', 'html' => 'SonataAdminBundle:CRUD:listhtml.html.twig'));
    }

    /**
     * Gets the public 'sonata.admin.builder.ormshow' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Builder\ShowBuilder
     */
    protected function getSonataAdminBuilderOrmShowService()
    {
        return $this->services['sonata.admin.builder.ormshow'] = new \Sonata\DoctrineORMAdminBundle\Builder\ShowBuilder(${($ = isset($this->services['sonata.admin.guesser.ormshowchain']) ? $this->services['sonata.admin.guesser.ormshowchain'] : $this->get('sonata.admin.guesser.ormshowchain')) && false ?: ''}, array('array' => 'SonataAdminBundle:CRUD:showarray.html.twig', 'boolean' => 'SonataAdminBundle:CRUD:showboolean.html.twig', 'date' => 'SonataAdminBundle:CRUD:showdate.html.twig', 'time' => 'SonataAdminBundle:CRUD:showtime.html.twig', 'datetime' => 'SonataAdminBundle:CRUD:showdatetime.html.twig', 'text' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'email' => 'SonataAdminBundle:CRUD:showemail.html.twig', 'trans' => 'SonataAdminBundle:CRUD:showtrans.html.twig', 'string' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'smallint' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'bigint' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'integer' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'decimal' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig', 'currency' => 'SonataAdminBundle:CRUD:showcurrency.html.twig', 'percent' => 'SonataAdminBundle:CRUD:showpercent.html.twig', 'choice' => 'SonataAdminBundle:CRUD:showchoice.html.twig', 'url' => 'SonataAdminBundle:CRUD:showurl.html.twig', 'html' => 'SonataAdminBundle:CRUD:showhtml.html.twig'));
    }

    /**
     * Gets the public 'sonata.admin.controller.admin' shared service.
     *
     * @return \Sonata\AdminBundle\Controller\HelperController
     */
    protected function getSonataAdminControllerAdminService()
    {
        return $this->services['sonata.admin.controller.admin'] = new \Sonata\AdminBundle\Controller\HelperController(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.helper']) ? $this->services['sonata.admin.helper'] : $this->get('sonata.admin.helper')) && false ?: ''}, ${($ = isset($this->services['validator']) ? $this->services['validator'] : $this->get('validator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.doctrineorm.form.type.choicefieldmask' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType
     */
    protected function getSonataAdminDoctrineOrmFormTypeChoiceFieldMaskService()
    {
        return $this->services['sonata.admin.doctrineorm.form.type.choicefieldmask'] = new \Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType();
    }

    /**
     * Gets the public 'sonata.admin.event.extension' shared service.
     *
     * @return \Sonata\AdminBundle\Event\AdminEventExtension
     */
    protected function getSonataAdminEventExtensionService()
    {
        return $this->services['sonata.admin.event.extension'] = new \Sonata\AdminBundle\Event\AdminEventExtension(${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.exporter' shared service.
     *
     * @return \Sonata\AdminBundle\Export\Exporter
     *
     * @deprecated The "sonata.admin.exporter" service is deprecated. You should stop using it, as it will soon be removed.
     */
    protected function getSonataAdminExporterService()
    {
        @triggererror('The "sonata.admin.exporter" service is deprecated. You should stop using it, as it will soon be removed.', EUSERDEPRECATED);

        return $this->services['sonata.admin.exporter'] = new \Sonata\AdminBundle\Export\Exporter();
    }

    /**
     * Gets the public 'sonata.admin.form.extension.choice' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Extension\ChoiceTypeExtension
     */
    protected function getSonataAdminFormExtensionChoiceService()
    {
        return $this->services['sonata.admin.form.extension.choice'] = new \Sonata\AdminBundle\Form\Extension\ChoiceTypeExtension();
    }

    /**
     * Gets the public 'sonata.admin.form.extension.field' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Extension\Field\Type\FormTypeFieldExtension
     */
    protected function getSonataAdminFormExtensionFieldService()
    {
        return $this->services['sonata.admin.form.extension.field'] = new \Sonata\AdminBundle\Form\Extension\Field\Type\FormTypeFieldExtension(array('email' => '', 'textarea' => '', 'text' => '', 'choice' => '', 'integer' => '', 'datetime' => 'sonata-medium-date', 'date' => 'sonata-medium-date', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\ChoiceType' => '', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\DateType' => 'sonata-medium-date', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\DateTimeType' => 'sonata-medium-date', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\EmailType' => '', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\IntegerType' => '', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextareaType' => '', 'Symfony\\Component\\Form\\Extension\\Core\\Type\\TextType' => ''), array('html5validate' => true, 'sortadmins' => false, 'confirmexit' => true, 'useselect2' => true, 'useicheck' => true, 'usebootlint' => false, 'usestickyforms' => true, 'pagerlinks' => NULL, 'formtype' => 'standard', 'dropdownnumbergroupspercolums' => 2, 'titlemode' => 'both', 'lockprotection' => false, 'enablejmsdiextraautoregistration' => true, 'javascripts' => array(0 => 'bundles/sonatacore/vendor/jquery/dist/jquery.min.js', 1 => 'bundles/sonataadmin/vendor/jquery.scrollTo/jquery.scrollTo.min.js', 2 => 'bundles/sonatacore/vendor/moment/min/moment.min.js', 3 => 'bundles/sonataadmin/vendor/jqueryui/ui/minified/jquery-ui.min.js', 4 => 'bundles/sonataadmin/vendor/jqueryui/ui/minified/i18n/jquery-ui-i18n.min.js', 5 => 'bundles/sonatacore/vendor/bootstrap/dist/js/bootstrap.min.js', 6 => 'bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 7 => 'bundles/sonataadmin/vendor/jquery-form/jquery.form.js', 8 => 'bundles/sonataadmin/jquery/jquery.confirmExit.js', 9 => 'bundles/sonataadmin/vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js', 10 => 'bundles/sonatacore/vendor/select2/select2.min.js', 11 => 'bundles/sonataadmin/vendor/admin-lte/dist/js/app.min.js', 12 => 'bundles/sonataadmin/vendor/iCheck/icheck.min.js', 13 => 'bundles/sonataadmin/vendor/slimScroll/jquery.slimscroll.min.js', 14 => 'bundles/sonataadmin/vendor/waypoints/lib/jquery.waypoints.min.js', 15 => 'bundles/sonataadmin/vendor/waypoints/lib/shortcuts/sticky.min.js', 16 => 'bundles/sonataadmin/vendor/readmore-js/readmore.min.js', 17 => 'bundles/sonataadmin/vendor/masonry/dist/masonry.pkgd.min.js', 18 => 'bundles/sonataadmin/Admin.js', 19 => 'bundles/sonataadmin/treeview.js'), 'stylesheets' => array(0 => 'bundles/sonatacore/vendor/bootstrap/dist/css/bootstrap.min.css', 1 => 'bundles/sonatacore/vendor/components-font-awesome/css/font-awesome.min.css', 2 => 'bundles/sonatacore/vendor/ionicons/css/ionicons.min.css', 3 => 'bundles/sonataadmin/vendor/admin-lte/dist/css/AdminLTE.min.css', 4 => 'bundles/sonataadmin/vendor/admin-lte/dist/css/skins/skin-black.min.css', 5 => 'bundles/sonataadmin/vendor/iCheck/skins/square/blue.css', 6 => 'bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css', 7 => 'bundles/sonataadmin/vendor/jqueryui/themes/base/jquery-ui.css', 8 => 'bundles/sonatacore/vendor/select2/select2.css', 9 => 'bundles/sonatacore/vendor/select2-bootstrap-css/select2-bootstrap.min.css', 10 => 'bundles/sonataadmin/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css', 11 => 'bundles/sonataadmin/css/styles.css', 12 => 'bundles/sonataadmin/css/layout.css', 13 => 'bundles/sonataadmin/css/tree.css')));
    }

    /**
     * Gets the public 'sonata.admin.form.extension.field.mopa' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Extension\Field\Type\MopaCompatibilityTypeFieldExtension
     */
    protected function getSonataAdminFormExtensionFieldMopaService()
    {
        return $this->services['sonata.admin.form.extension.field.mopa'] = new \Sonata\AdminBundle\Form\Extension\Field\Type\MopaCompatibilityTypeFieldExtension();
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.choice' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\ChoiceType
     */
    protected function getSonataAdminFormFilterTypeChoiceService()
    {
        return $this->services['sonata.admin.form.filter.type.choice'] = new \Sonata\AdminBundle\Form\Type\Filter\ChoiceType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.date' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\DateType
     */
    protected function getSonataAdminFormFilterTypeDateService()
    {
        return $this->services['sonata.admin.form.filter.type.date'] = new \Sonata\AdminBundle\Form\Type\Filter\DateType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.daterange' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\DateRangeType
     */
    protected function getSonataAdminFormFilterTypeDaterangeService()
    {
        return $this->services['sonata.admin.form.filter.type.daterange'] = new \Sonata\AdminBundle\Form\Type\Filter\DateRangeType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.datetime' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\DateTimeType
     */
    protected function getSonataAdminFormFilterTypeDatetimeService()
    {
        return $this->services['sonata.admin.form.filter.type.datetime'] = new \Sonata\AdminBundle\Form\Type\Filter\DateTimeType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.datetimerange' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\DateTimeRangeType
     */
    protected function getSonataAdminFormFilterTypeDatetimeRangeService()
    {
        return $this->services['sonata.admin.form.filter.type.datetimerange'] = new \Sonata\AdminBundle\Form\Type\Filter\DateTimeRangeType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.default' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\DefaultType
     */
    protected function getSonataAdminFormFilterTypeDefaultService()
    {
        return $this->services['sonata.admin.form.filter.type.default'] = new \Sonata\AdminBundle\Form\Type\Filter\DefaultType();
    }

    /**
     * Gets the public 'sonata.admin.form.filter.type.number' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\Filter\NumberType
     */
    protected function getSonataAdminFormFilterTypeNumberService()
    {
        return $this->services['sonata.admin.form.filter.type.number'] = new \Sonata\AdminBundle\Form\Type\Filter\NumberType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.type.admin' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\AdminType
     */
    protected function getSonataAdminFormTypeAdminService()
    {
        return $this->services['sonata.admin.form.type.admin'] = new \Sonata\AdminBundle\Form\Type\AdminType();
    }

    /**
     * Gets the public 'sonata.admin.form.type.collection' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\CollectionType
     */
    protected function getSonataAdminFormTypeCollectionService()
    {
        return $this->services['sonata.admin.form.type.collection'] = new \Sonata\AdminBundle\Form\Type\CollectionType();
    }

    /**
     * Gets the public 'sonata.admin.form.type.modelautocomplete' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ModelAutocompleteType
     */
    protected function getSonataAdminFormTypeModelAutocompleteService()
    {
        return $this->services['sonata.admin.form.type.modelautocomplete'] = new \Sonata\AdminBundle\Form\Type\ModelAutocompleteType();
    }

    /**
     * Gets the public 'sonata.admin.form.type.modelchoice' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ModelType
     */
    protected function getSonataAdminFormTypeModelChoiceService()
    {
        return $this->services['sonata.admin.form.type.modelchoice'] = new \Sonata\AdminBundle\Form\Type\ModelType(${($ = isset($this->services['propertyaccessor']) ? $this->services['propertyaccessor'] : $this->get('propertyaccessor')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.form.type.modelhidden' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ModelHiddenType
     */
    protected function getSonataAdminFormTypeModelHiddenService()
    {
        return $this->services['sonata.admin.form.type.modelhidden'] = new \Sonata\AdminBundle\Form\Type\ModelHiddenType();
    }

    /**
     * Gets the public 'sonata.admin.form.type.modellist' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ModelListType
     */
    protected function getSonataAdminFormTypeModelListService()
    {
        return $this->services['sonata.admin.form.type.modellist'] = new \Sonata\AdminBundle\Form\Type\ModelListType();
    }

    /**
     * Gets the public 'sonata.admin.form.type.modelreference' shared service.
     *
     * @return \Sonata\AdminBundle\Form\Type\ModelReferenceType
     */
    protected function getSonataAdminFormTypeModelReferenceService()
    {
        return $this->services['sonata.admin.form.type.modelreference'] = new \Sonata\AdminBundle\Form\Type\ModelReferenceType();
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormdatagrid' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Guesser\FilterTypeGuesser
     */
    protected function getSonataAdminGuesserOrmDatagridService()
    {
        return $this->services['sonata.admin.guesser.ormdatagrid'] = new \Sonata\DoctrineORMAdminBundle\Guesser\FilterTypeGuesser();
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormdatagridchain' shared service.
     *
     * @return \Sonata\AdminBundle\Guesser\TypeGuesserChain
     */
    protected function getSonataAdminGuesserOrmDatagridChainService()
    {
        return $this->services['sonata.admin.guesser.ormdatagridchain'] = new \Sonata\AdminBundle\Guesser\TypeGuesserChain(array(0 => ${($ = isset($this->services['sonata.admin.guesser.ormdatagrid']) ? $this->services['sonata.admin.guesser.ormdatagrid'] : $this->get('sonata.admin.guesser.ormdatagrid')) && false ?: ''}));
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormlist' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Guesser\TypeGuesser
     */
    protected function getSonataAdminGuesserOrmListService()
    {
        return $this->services['sonata.admin.guesser.ormlist'] = new \Sonata\DoctrineORMAdminBundle\Guesser\TypeGuesser();
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormlistchain' shared service.
     *
     * @return \Sonata\AdminBundle\Guesser\TypeGuesserChain
     */
    protected function getSonataAdminGuesserOrmListChainService()
    {
        return $this->services['sonata.admin.guesser.ormlistchain'] = new \Sonata\AdminBundle\Guesser\TypeGuesserChain(array(0 => ${($ = isset($this->services['sonata.admin.guesser.ormlist']) ? $this->services['sonata.admin.guesser.ormlist'] : $this->get('sonata.admin.guesser.ormlist')) && false ?: ''}));
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormshow' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Guesser\TypeGuesser
     */
    protected function getSonataAdminGuesserOrmShowService()
    {
        return $this->services['sonata.admin.guesser.ormshow'] = new \Sonata\DoctrineORMAdminBundle\Guesser\TypeGuesser();
    }

    /**
     * Gets the public 'sonata.admin.guesser.ormshowchain' shared service.
     *
     * @return \Sonata\AdminBundle\Guesser\TypeGuesserChain
     */
    protected function getSonataAdminGuesserOrmShowChainService()
    {
        return $this->services['sonata.admin.guesser.ormshowchain'] = new \Sonata\AdminBundle\Guesser\TypeGuesserChain(array(0 => ${($ = isset($this->services['sonata.admin.guesser.ormshow']) ? $this->services['sonata.admin.guesser.ormshow'] : $this->get('sonata.admin.guesser.ormshow')) && false ?: ''}));
    }

    /**
     * Gets the public 'sonata.admin.helper' shared service.
     *
     * @return \Sonata\AdminBundle\Admin\AdminHelper
     */
    protected function getSonataAdminHelperService()
    {
        return $this->services['sonata.admin.helper'] = new \Sonata\AdminBundle\Admin\AdminHelper(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.label.strategy.bc' shared service.
     *
     * @return \Sonata\AdminBundle\Translator\BCLabelTranslatorStrategy
     */
    protected function getSonataAdminLabelStrategyBcService()
    {
        return $this->services['sonata.admin.label.strategy.bc'] = new \Sonata\AdminBundle\Translator\BCLabelTranslatorStrategy();
    }

    /**
     * Gets the public 'sonata.admin.label.strategy.formcomponent' shared service.
     *
     * @return \Sonata\AdminBundle\Translator\FormLabelTranslatorStrategy
     */
    protected function getSonataAdminLabelStrategyFormComponentService()
    {
        return $this->services['sonata.admin.label.strategy.formcomponent'] = new \Sonata\AdminBundle\Translator\FormLabelTranslatorStrategy();
    }

    /**
     * Gets the public 'sonata.admin.label.strategy.native' shared service.
     *
     * @return \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy
     */
    protected function getSonataAdminLabelStrategyNativeService()
    {
        return $this->services['sonata.admin.label.strategy.native'] = new \Sonata\AdminBundle\Translator\NativeLabelTranslatorStrategy();
    }

    /**
     * Gets the public 'sonata.admin.label.strategy.noop' shared service.
     *
     * @return \Sonata\AdminBundle\Translator\NoopLabelTranslatorStrategy
     */
    protected function getSonataAdminLabelStrategyNoopService()
    {
        return $this->services['sonata.admin.label.strategy.noop'] = new \Sonata\AdminBundle\Translator\NoopLabelTranslatorStrategy();
    }

    /**
     * Gets the public 'sonata.admin.label.strategy.underscore' shared service.
     *
     * @return \Sonata\AdminBundle\Translator\UnderscoreLabelTranslatorStrategy
     */
    protected function getSonataAdminLabelStrategyUnderscoreService()
    {
        return $this->services['sonata.admin.label.strategy.underscore'] = new \Sonata\AdminBundle\Translator\UnderscoreLabelTranslatorStrategy();
    }

    /**
     * Gets the public 'sonata.admin.manager.orm' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Model\ModelManager
     */
    protected function getSonataAdminManagerOrmService()
    {
        return $this->services['sonata.admin.manager.orm'] = new \Sonata\DoctrineORMAdminBundle\Model\ModelManager(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.manipulator.acl.admin' shared service.
     *
     * @return \Sonata\AdminBundle\Util\AdminAclManipulator
     */
    protected function getSonataAdminManipulatorAclAdminService()
    {
        return $this->services['sonata.admin.manipulator.acl.admin'] = new \Sonata\AdminBundle\Util\AdminAclManipulator('Sonata\\AdminBundle\\Security\\Acl\\Permission\\MaskBuilder');
    }

    /**
     * Gets the public 'sonata.admin.manipulator.acl.object.orm' shared service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Util\ObjectAclManipulator
     */
    protected function getSonataAdminManipulatorAclObjectOrmService()
    {
        return $this->services['sonata.admin.manipulator.acl.object.orm'] = new \Sonata\DoctrineORMAdminBundle\Util\ObjectAclManipulator();
    }

    /**
     * Gets the public 'sonata.admin.menu.matcher.voter.active' shared service.
     *
     * @return \Sonata\AdminBundle\Menu\Matcher\Voter\ActiveVoter
     */
    protected function getSonataAdminMenuMatcherVoterActiveService()
    {
        return $this->services['sonata.admin.menu.matcher.voter.active'] = new \Sonata\AdminBundle\Menu\Matcher\Voter\ActiveVoter();
    }

    /**
     * Gets the public 'sonata.admin.menu.matcher.voter.admin' shared service.
     *
     * @return \Sonata\AdminBundle\Menu\Matcher\Voter\AdminVoter
     */
    protected function getSonataAdminMenuMatcherVoterAdminService()
    {
        return $this->services['sonata.admin.menu.matcher.voter.admin'] = new \Sonata\AdminBundle\Menu\Matcher\Voter\AdminVoter();
    }

    /**
     * Gets the public 'sonata.admin.menu.matcher.voter.children' shared service.
     *
     * @return \Sonata\AdminBundle\Menu\Matcher\Voter\ChildrenVoter
     */
    protected function getSonataAdminMenuMatcherVoterChildrenService()
    {
        return $this->services['sonata.admin.menu.matcher.voter.children'] = new \Sonata\AdminBundle\Menu\Matcher\Voter\ChildrenVoter(${($ = isset($this->services['knpmenu.matcher']) ? $this->services['knpmenu.matcher'] : $this->get('knpmenu.matcher')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.menubuilder' shared service.
     *
     * @return \Sonata\AdminBundle\Menu\MenuBuilder
     */
    protected function getSonataAdminMenuBuilderService()
    {
        return $this->services['sonata.admin.menubuilder'] = new \Sonata\AdminBundle\Menu\MenuBuilder(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, ${($ = isset($this->services['knpmenu.factory']) ? $this->services['knpmenu.factory'] : $this->get('knpmenu.factory')) && false ?: ''}, ${($ = isset($this->services['knpmenu.menuprovider']) ? $this->services['knpmenu.menuprovider'] : $this->get('knpmenu.menuprovider')) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.object.manipulator.acl.admin' shared service.
     *
     * @return \Sonata\AdminBundle\Util\AdminObjectAclManipulator
     */
    protected function getSonataAdminObjectManipulatorAclAdminService()
    {
        return $this->services['sonata.admin.object.manipulator.acl.admin'] = new \Sonata\AdminBundle\Util\AdminObjectAclManipulator(${($ = isset($this->services['form.factory']) ? $this->services['form.factory'] : $this->get('form.factory')) && false ?: ''}, 'Sonata\\AdminBundle\\Security\\Acl\\Permission\\MaskBuilder');
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.boolean' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter
     */
    protected function getSonataAdminOrmFilterTypeBooleanService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\BooleanFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.callback' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter
     */
    protected function getSonataAdminOrmFilterTypeCallbackService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.choice' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter
     */
    protected function getSonataAdminOrmFilterTypeChoiceService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\ChoiceFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.class' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ClassFilter
     */
    protected function getSonataAdminOrmFilterTypeClassService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\ClassFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.date' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateFilter
     */
    protected function getSonataAdminOrmFilterTypeDateService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\DateFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.daterange' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter
     */
    protected function getSonataAdminOrmFilterTypeDateRangeService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.datetime' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter
     */
    protected function getSonataAdminOrmFilterTypeDatetimeService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\DateTimeFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.datetimerange' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter
     */
    protected function getSonataAdminOrmFilterTypeDatetimeRangeService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.model' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ModelFilter
     */
    protected function getSonataAdminOrmFilterTypeModelService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\ModelFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.modelautocomplete' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter
     */
    protected function getSonataAdminOrmFilterTypeModelAutocompleteService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.number' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\NumberFilter
     */
    protected function getSonataAdminOrmFilterTypeNumberService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\NumberFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.string' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\StringFilter
     */
    protected function getSonataAdminOrmFilterTypeStringService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\StringFilter();
    }

    /**
     * Gets the public 'sonata.admin.orm.filter.type.time' service.
     *
     * @return \Sonata\DoctrineORMAdminBundle\Filter\TimeFilter
     */
    protected function getSonataAdminOrmFilterTypeTimeService()
    {
        return new \Sonata\DoctrineORMAdminBundle\Filter\TimeFilter();
    }

    /**
     * Gets the public 'sonata.admin.pool' shared service.
     *
     * @return \Sonata\AdminBundle\Admin\Pool
     */
    protected function getSonataAdminPoolService()
    {
        $this->services['sonata.admin.pool'] = $instance = new \Sonata\AdminBundle\Admin\Pool($this, 'Sonata Admin', 'bundles/sonataadmin/logotitle.png', array('html5validate' => true, 'sortadmins' => false, 'confirmexit' => true, 'useselect2' => true, 'useicheck' => true, 'usebootlint' => false, 'usestickyforms' => true, 'pagerlinks' => NULL, 'formtype' => 'standard', 'dropdownnumbergroupspercolums' => 2, 'titlemode' => 'both', 'lockprotection' => false, 'enablejmsdiextraautoregistration' => true, 'javascripts' => array(0 => 'bundles/sonatacore/vendor/jquery/dist/jquery.min.js', 1 => 'bundles/sonataadmin/vendor/jquery.scrollTo/jquery.scrollTo.min.js', 2 => 'bundles/sonatacore/vendor/moment/min/moment.min.js', 3 => 'bundles/sonataadmin/vendor/jqueryui/ui/minified/jquery-ui.min.js', 4 => 'bundles/sonataadmin/vendor/jqueryui/ui/minified/i18n/jquery-ui-i18n.min.js', 5 => 'bundles/sonatacore/vendor/bootstrap/dist/js/bootstrap.min.js', 6 => 'bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 7 => 'bundles/sonataadmin/vendor/jquery-form/jquery.form.js', 8 => 'bundles/sonataadmin/jquery/jquery.confirmExit.js', 9 => 'bundles/sonataadmin/vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js', 10 => 'bundles/sonatacore/vendor/select2/select2.min.js', 11 => 'bundles/sonataadmin/vendor/admin-lte/dist/js/app.min.js', 12 => 'bundles/sonataadmin/vendor/iCheck/icheck.min.js', 13 => 'bundles/sonataadmin/vendor/slimScroll/jquery.slimscroll.min.js', 14 => 'bundles/sonataadmin/vendor/waypoints/lib/jquery.waypoints.min.js', 15 => 'bundles/sonataadmin/vendor/waypoints/lib/shortcuts/sticky.min.js', 16 => 'bundles/sonataadmin/vendor/readmore-js/readmore.min.js', 17 => 'bundles/sonataadmin/vendor/masonry/dist/masonry.pkgd.min.js', 18 => 'bundles/sonataadmin/Admin.js', 19 => 'bundles/sonataadmin/treeview.js'), 'stylesheets' => array(0 => 'bundles/sonatacore/vendor/bootstrap/dist/css/bootstrap.min.css', 1 => 'bundles/sonatacore/vendor/components-font-awesome/css/font-awesome.min.css', 2 => 'bundles/sonatacore/vendor/ionicons/css/ionicons.min.css', 3 => 'bundles/sonataadmin/vendor/admin-lte/dist/css/AdminLTE.min.css', 4 => 'bundles/sonataadmin/vendor/admin-lte/dist/css/skins/skin-black.min.css', 5 => 'bundles/sonataadmin/vendor/iCheck/skins/square/blue.css', 6 => 'bundles/sonatacore/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css', 7 => 'bundles/sonataadmin/vendor/jqueryui/themes/base/jquery-ui.css', 8 => 'bundles/sonatacore/vendor/select2/select2.css', 9 => 'bundles/sonatacore/vendor/select2-bootstrap-css/select2-bootstrap.min.css', 10 => 'bundles/sonataadmin/vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css', 11 => 'bundles/sonataadmin/css/styles.css', 12 => 'bundles/sonataadmin/css/layout.css', 13 => 'bundles/sonataadmin/css/tree.css')), ${($ = isset($this->services['propertyaccessor']) ? $this->services['propertyaccessor'] : $this->get('propertyaccessor')) && false ?: ''});

        $instance->setTemplates(array('userblock' => 'SonataAdminBundle:Core:userblock.html.twig', 'addblock' => 'SonataAdminBundle:Core:addblock.html.twig', 'layout' => 'SonataAdminBundle::standardlayout.html.twig', 'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig', 'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig', 'search' => 'SonataAdminBundle:Core:search.html.twig', 'list' => 'SonataAdminBundle:CRUD:list.html.twig', 'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig', 'show' => 'SonataAdminBundle:CRUD:show.html.twig', 'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig', 'edit' => 'SonataAdminBundle:CRUD:edit.html.twig', 'preview' => 'SonataAdminBundle:CRUD:preview.html.twig', 'history' => 'SonataAdminBundle:CRUD:history.html.twig', 'acl' => 'SonataAdminBundle:CRUD:acl.html.twig', 'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig', 'action' => 'SonataAdminBundle:CRUD:action.html.twig', 'select' => 'SonataAdminBundle:CRUD:listselect.html.twig', 'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig', 'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig', 'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig', 'delete' => 'SonataAdminBundle:CRUD:delete.html.twig', 'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig', 'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig', 'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig', 'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig', 'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig', 'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig', 'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig', 'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig', 'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig', 'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig', 'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig', 'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig', 'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig', 'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig', 'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig', 'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig', 'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig', 'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig'));
        $instance->setAdminServiceIds(array(0 => 'app.admin.user', 1 => 'app.admin.office', 2 => 'app.admin.partner', 3 => 'app.admin.region'));
        $instance->setAdminGroups(array('admin' => array('label' => 'admin', 'labelcatalogue' => 'SonataAdminBundle', 'icon' => '<i class="fa fa-folder"></i>', 'roles' => array(), 'ontop' => false, 'keepopen' => false, 'items' => array(0 => array('admin' => 'app.admin.user', 'label' => 'User', 'route' => '', 'routeparams' => array(), 'routeabsolute' => false), 1 => array('admin' => 'app.admin.office', 'label' => 'Office', 'route' => '', 'routeparams' => array(), 'routeabsolute' => false), 2 => array('admin' => 'app.admin.partner', 'label' => 'Partner', 'route' => '', 'routeparams' => array(), 'routeabsolute' => false), 3 => array('admin' => 'app.admin.region', 'label' => 'Region', 'route' => '', 'routeparams' => array(), 'routeabsolute' => false)))));
        $instance->setAdminClasses(array('AppBundle\\Entity\\User' => array(0 => 'app.admin.user'), 'AppBundle\\Entity\\Office' => array(0 => 'app.admin.office'), 'AppBundle\\Entity\\Partner' => array(0 => 'app.admin.partner'), 'AppBundle\\Entity\\Region' => array(0 => 'app.admin.region')));

        return $instance;
    }

    /**
     * Gets the public 'sonata.admin.route.cache' shared service.
     *
     * @return \Sonata\AdminBundle\Route\RoutesCache
     */
    protected function getSonataAdminRouteCacheService()
    {
        return $this->services['sonata.admin.route.cache'] = new \Sonata\AdminBundle\Route\RoutesCache((DIR.'/sonata/admin'), true);
    }

    /**
     * Gets the public 'sonata.admin.route.cachewarmup' shared service.
     *
     * @return \Sonata\AdminBundle\Route\RoutesCacheWarmUp
     */
    protected function getSonataAdminRouteCacheWarmupService()
    {
        return $this->services['sonata.admin.route.cachewarmup'] = new \Sonata\AdminBundle\Route\RoutesCacheWarmUp(${($ = isset($this->services['sonata.admin.route.cache']) ? $this->services['sonata.admin.route.cache'] : $this->get('sonata.admin.route.cache')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.route.defaultgenerator' shared service.
     *
     * @return \Sonata\AdminBundle\Route\DefaultRouteGenerator
     */
    protected function getSonataAdminRouteDefaultGeneratorService()
    {
        return $this->services['sonata.admin.route.defaultgenerator'] = new \Sonata\AdminBundle\Route\DefaultRouteGenerator(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}, ${($ = isset($this->services['sonata.admin.route.cache']) ? $this->services['sonata.admin.route.cache'] : $this->get('sonata.admin.route.cache')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.route.pathinfo' shared service.
     *
     * @return \Sonata\AdminBundle\Route\PathInfoBuilder
     */
    protected function getSonataAdminRoutePathInfoService()
    {
        return $this->services['sonata.admin.route.pathinfo'] = new \Sonata\AdminBundle\Route\PathInfoBuilder(${($ = isset($this->services['sonata.admin.audit.manager']) ? $this->services['sonata.admin.audit.manager'] : $this->get('sonata.admin.audit.manager')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.route.querystring' shared service.
     *
     * @return \Sonata\AdminBundle\Route\QueryStringBuilder
     */
    protected function getSonataAdminRouteQueryStringService()
    {
        return $this->services['sonata.admin.route.querystring'] = new \Sonata\AdminBundle\Route\QueryStringBuilder(${($ = isset($this->services['sonata.admin.audit.manager']) ? $this->services['sonata.admin.audit.manager'] : $this->get('sonata.admin.audit.manager')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.routeloader' shared service.
     *
     * @return \Sonata\AdminBundle\Route\AdminPoolLoader
     */
    protected function getSonataAdminRouteLoaderService()
    {
        return $this->services['sonata.admin.routeloader'] = new \Sonata\AdminBundle\Route\AdminPoolLoader(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, array(0 => 'app.admin.user', 1 => 'app.admin.office', 2 => 'app.admin.partner', 3 => 'app.admin.region'), $this);
    }

    /**
     * Gets the public 'sonata.admin.search.handler' shared service.
     *
     * @return \Sonata\AdminBundle\Search\SearchHandler
     */
    protected function getSonataAdminSearchHandlerService()
    {
        return $this->services['sonata.admin.search.handler'] = new \Sonata\AdminBundle\Search\SearchHandler(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.security.handler' shared service.
     *
     * @return \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler
     */
    protected function getSonataAdminSecurityHandlerService()
    {
        return $this->services['sonata.admin.security.handler'] = new \Sonata\AdminBundle\Security\Handler\NoopSecurityHandler();
    }

    /**
     * Gets the public 'sonata.admin.sidebarmenu' shared service.
     *
     * @return \Knp\Menu\MenuItem
     */
    protected function getSonataAdminSidebarMenuService()
    {
        return $this->services['sonata.admin.sidebarmenu'] = ${($ = isset($this->services['sonata.admin.menubuilder']) ? $this->services['sonata.admin.menubuilder'] : $this->get('sonata.admin.menubuilder')) && false ?: ''}->createSidebarMenu();
    }

    /**
     * Gets the public 'sonata.admin.twig.extension' shared service.
     *
     * @return \Sonata\AdminBundle\Twig\Extension\SonataAdminExtension
     */
    protected function getSonataAdminTwigExtensionService()
    {
        $this->services['sonata.admin.twig.extension'] = $instance = new \Sonata\AdminBundle\Twig\Extension\SonataAdminExtension(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''}, ${($ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});

        $instance->setXEditableTypeMapping(array('choice' => 'select', 'boolean' => 'select', 'text' => 'text', 'textarea' => 'textarea', 'html' => 'textarea', 'email' => 'email', 'string' => 'text', 'smallint' => 'text', 'bigint' => 'text', 'integer' => 'number', 'decimal' => 'number', 'currency' => 'number', 'percent' => 'number', 'url' => 'url', 'date' => 'date'));

        return $instance;
    }

    /**
     * Gets the public 'sonata.admin.twig.global' shared service.
     *
     * @return \Sonata\AdminBundle\Twig\GlobalVariables
     */
    protected function getSonataAdminTwigGlobalService()
    {
        return $this->services['sonata.admin.twig.global'] = new \Sonata\AdminBundle\Twig\GlobalVariables(${($ = isset($this->services['sonata.admin.pool']) ? $this->services['sonata.admin.pool'] : $this->get('sonata.admin.pool')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.admin.validator.inline' shared service.
     *
     * @return \Sonata\CoreBundle\Validator\InlineValidator
     */
    protected function getSonataAdminValidatorInlineService()
    {
        return $this->services['sonata.admin.validator.inline'] = new \Sonata\CoreBundle\Validator\InlineValidator($this, ${($ = isset($this->services['validator.validatorfactory']) ? $this->services['validator.validatorfactory'] : $this->getValidatorValidatorFactoryService()) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.cache.handler.default' shared service.
     *
     * @return \Sonata\BlockBundle\Cache\HttpCacheHandler
     */
    protected function getSonataBlockCacheHandlerDefaultService()
    {
        return $this->services['sonata.block.cache.handler.default'] = new \Sonata\BlockBundle\Cache\HttpCacheHandler();
    }

    /**
     * Gets the public 'sonata.block.cache.handler.noop' shared service.
     *
     * @return \Sonata\BlockBundle\Cache\NoopHttpCacheHandler
     */
    protected function getSonataBlockCacheHandlerNoopService()
    {
        return $this->services['sonata.block.cache.handler.noop'] = new \Sonata\BlockBundle\Cache\NoopHttpCacheHandler();
    }

    /**
     * Gets the public 'sonata.block.contextmanager.default' shared service.
     *
     * @return \Sonata\BlockBundle\Block\BlockContextManager
     */
    protected function getSonataBlockContextManagerDefaultService()
    {
        return $this->services['sonata.block.contextmanager.default'] = new \Sonata\BlockBundle\Block\BlockContextManager(${($ = isset($this->services['sonata.block.loader.chain']) ? $this->services['sonata.block.loader.chain'] : $this->get('sonata.block.loader.chain')) && false ?: ''}, ${($ = isset($this->services['sonata.block.manager']) ? $this->services['sonata.block.manager'] : $this->getSonataBlockManagerService()) && false ?: ''}, array('bytype' => array('sonata.admin.block.adminlist' => 'sonata.cache.noop')), ${($ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.exception.filter.debugonly' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Filter\DebugOnlyFilter
     */
    protected function getSonataBlockExceptionFilterDebugOnlyService()
    {
        return $this->services['sonata.block.exception.filter.debugonly'] = new \Sonata\BlockBundle\Exception\Filter\DebugOnlyFilter(true);
    }

    /**
     * Gets the public 'sonata.block.exception.filter.ignoreblockexception' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Filter\IgnoreClassFilter
     */
    protected function getSonataBlockExceptionFilterIgnoreBlockExceptionService()
    {
        return $this->services['sonata.block.exception.filter.ignoreblockexception'] = new \Sonata\BlockBundle\Exception\Filter\IgnoreClassFilter('Sonata\\BlockBundle\\Exception\\BlockExceptionInterface');
    }

    /**
     * Gets the public 'sonata.block.exception.filter.keepall' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Filter\KeepAllFilter
     */
    protected function getSonataBlockExceptionFilterKeepAllService()
    {
        return $this->services['sonata.block.exception.filter.keepall'] = new \Sonata\BlockBundle\Exception\Filter\KeepAllFilter();
    }

    /**
     * Gets the public 'sonata.block.exception.filter.keepnone' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Filter\KeepNoneFilter
     */
    protected function getSonataBlockExceptionFilterKeepNoneService()
    {
        return $this->services['sonata.block.exception.filter.keepnone'] = new \Sonata\BlockBundle\Exception\Filter\KeepNoneFilter();
    }

    /**
     * Gets the public 'sonata.block.exception.renderer.inline' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Renderer\InlineRenderer
     */
    protected function getSonataBlockExceptionRendererInlineService()
    {
        return $this->services['sonata.block.exception.renderer.inline'] = new \Sonata\BlockBundle\Exception\Renderer\InlineRenderer(${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, 'SonataBlockBundle:Block:blockexception.html.twig');
    }

    /**
     * Gets the public 'sonata.block.exception.renderer.inlinedebug' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Renderer\InlineDebugRenderer
     */
    protected function getSonataBlockExceptionRendererInlineDebugService()
    {
        return $this->services['sonata.block.exception.renderer.inlinedebug'] = new \Sonata\BlockBundle\Exception\Renderer\InlineDebugRenderer(${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, 'SonataBlockBundle:Block:blockexceptiondebug.html.twig', true, true);
    }

    /**
     * Gets the public 'sonata.block.exception.renderer.throw' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Renderer\MonkeyThrowRenderer
     */
    protected function getSonataBlockExceptionRendererThrowService()
    {
        return $this->services['sonata.block.exception.renderer.throw'] = new \Sonata\BlockBundle\Exception\Renderer\MonkeyThrowRenderer();
    }

    /**
     * Gets the public 'sonata.block.exception.strategy.manager' shared service.
     *
     * @return \Sonata\BlockBundle\Exception\Strategy\StrategyManager
     */
    protected function getSonataBlockExceptionStrategyManagerService()
    {
        $this->services['sonata.block.exception.strategy.manager'] = $instance = new \Sonata\BlockBundle\Exception\Strategy\StrategyManager($this, array('debugonly' => 'sonata.block.exception.filter.debugonly', 'ignoreblockexception' => 'sonata.block.exception.filter.ignoreblockexception', 'keepall' => 'sonata.block.exception.filter.keepall', 'keepnone' => 'sonata.block.exception.filter.keepnone'), array('inline' => 'sonata.block.exception.renderer.inline', 'inlinedebug' => 'sonata.block.exception.renderer.inlinedebug', 'throw' => 'sonata.block.exception.renderer.throw'), array(), array());

        $instance->setDefaultFilter('debugonly');
        $instance->setDefaultRenderer('throw');

        return $instance;
    }

    /**
     * Gets the public 'sonata.block.form.type.block' shared service.
     *
     * @return \Sonata\BlockBundle\Form\Type\ServiceListType
     */
    protected function getSonataBlockFormTypeBlockService()
    {
        return $this->services['sonata.block.form.type.block'] = new \Sonata\BlockBundle\Form\Type\ServiceListType(${($ = isset($this->services['sonata.block.manager']) ? $this->services['sonata.block.manager'] : $this->getSonataBlockManagerService()) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.form.type.containertemplate' shared service.
     *
     * @return \Sonata\BlockBundle\Form\Type\ContainerTemplateType
     */
    protected function getSonataBlockFormTypeContainerTemplateService()
    {
        return $this->services['sonata.block.form.type.containertemplate'] = new \Sonata\BlockBundle\Form\Type\ContainerTemplateType(array('SonataBlockBundle:Block:blockcontainer.html.twig' => 'SonataBlockBundle default template'));
    }

    /**
     * Gets the public 'sonata.block.loader.chain' shared service.
     *
     * @return \Sonata\BlockBundle\Block\BlockLoaderChain
     */
    protected function getSonataBlockLoaderChainService()
    {
        return $this->services['sonata.block.loader.chain'] = new \Sonata\BlockBundle\Block\BlockLoaderChain(array(0 => ${($ = isset($this->services['sonata.block.loader.service']) ? $this->services['sonata.block.loader.service'] : $this->get('sonata.block.loader.service')) && false ?: ''}));
    }

    /**
     * Gets the public 'sonata.block.loader.service' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Loader\ServiceLoader
     */
    protected function getSonataBlockLoaderServiceService()
    {
        return $this->services['sonata.block.loader.service'] = new \Sonata\BlockBundle\Block\Loader\ServiceLoader(array(0 => 'sonata.admin.block.adminlist'));
    }

    /**
     * Gets the public 'sonata.block.menu.registry' shared service.
     *
     * @return \Sonata\BlockBundle\Menu\MenuRegistry
     */
    protected function getSonataBlockMenuRegistryService()
    {
        return $this->services['sonata.block.menu.registry'] = new \Sonata\BlockBundle\Menu\MenuRegistry(array());
    }

    /**
     * Gets the public 'sonata.block.renderer.default' shared service.
     *
     * @return \Sonata\BlockBundle\Block\BlockRenderer
     */
    protected function getSonataBlockRendererDefaultService()
    {
        return $this->services['sonata.block.renderer.default'] = new \Sonata\BlockBundle\Block\BlockRenderer(${($ = isset($this->services['sonata.block.manager']) ? $this->services['sonata.block.manager'] : $this->getSonataBlockManagerService()) && false ?: ''}, ${($ = isset($this->services['sonata.block.exception.strategy.manager']) ? $this->services['sonata.block.exception.strategy.manager'] : $this->get('sonata.block.exception.strategy.manager')) && false ?: ''}, ${($ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, true);
    }

    /**
     * Gets the public 'sonata.block.service.container' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\ContainerBlockService
     */
    protected function getSonataBlockServiceContainerService()
    {
        return $this->services['sonata.block.service.container'] = new \Sonata\BlockBundle\Block\Service\ContainerBlockService('sonata.block.container', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.service.empty' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\EmptyBlockService
     */
    protected function getSonataBlockServiceEmptyService()
    {
        return $this->services['sonata.block.service.empty'] = new \Sonata\BlockBundle\Block\Service\EmptyBlockService('sonata.block.empty', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.service.menu' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\MenuBlockService
     */
    protected function getSonataBlockServiceMenuService()
    {
        return $this->services['sonata.block.service.menu'] = new \Sonata\BlockBundle\Block\Service\MenuBlockService('sonata.block.menu', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''}, ${($ = isset($this->services['knpmenu.menuprovider']) ? $this->services['knpmenu.menuprovider'] : $this->get('knpmenu.menuprovider')) && false ?: ''}, ${($ = isset($this->services['sonata.block.menu.registry']) ? $this->services['sonata.block.menu.registry'] : $this->get('sonata.block.menu.registry')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.service.rss' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\RssBlockService
     */
    protected function getSonataBlockServiceRssService()
    {
        return $this->services['sonata.block.service.rss'] = new \Sonata\BlockBundle\Block\Service\RssBlockService('sonata.block.rss', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.service.template' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\TemplateBlockService
     */
    protected function getSonataBlockServiceTemplateService()
    {
        return $this->services['sonata.block.service.template'] = new \Sonata\BlockBundle\Block\Service\TemplateBlockService('sonata.block.template', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.service.text' shared service.
     *
     * @return \Sonata\BlockBundle\Block\Service\TextBlockService
     */
    protected function getSonataBlockServiceTextService()
    {
        return $this->services['sonata.block.service.text'] = new \Sonata\BlockBundle\Block\Service\TextBlockService('sonata.block.text', ${($ = isset($this->services['templating']) ? $this->services['templating'] : $this->get('templating')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.templating.helper' shared service.
     *
     * @return \Sonata\BlockBundle\Templating\Helper\BlockHelper
     */
    protected function getSonataBlockTemplatingHelperService()
    {
        return $this->services['sonata.block.templating.helper'] = new \Sonata\BlockBundle\Templating\Helper\BlockHelper(${($ = isset($this->services['sonata.block.manager']) ? $this->services['sonata.block.manager'] : $this->getSonataBlockManagerService()) && false ?: ''}, array('bytype' => array('sonata.admin.block.adminlist' => 'sonata.cache.noop')), ${($ = isset($this->services['sonata.block.renderer.default']) ? $this->services['sonata.block.renderer.default'] : $this->get('sonata.block.renderer.default')) && false ?: ''}, ${($ = isset($this->services['sonata.block.contextmanager.default']) ? $this->services['sonata.block.contextmanager.default'] : $this->get('sonata.block.contextmanager.default')) && false ?: ''}, ${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''}, NULL, ${($ = isset($this->services['sonata.block.cache.handler.default']) ? $this->services['sonata.block.cache.handler.default'] : $this->get('sonata.block.cache.handler.default', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.block.twig.global' shared service.
     *
     * @return \Sonata\BlockBundle\Twig\GlobalVariables
     */
    protected function getSonataBlockTwigGlobalService()
    {
        return $this->services['sonata.block.twig.global'] = new \Sonata\BlockBundle\Twig\GlobalVariables(array('blockbase' => 'SonataBlockBundle:Block:blockbase.html.twig', 'blockcontainer' => 'SonataBlockBundle:Block:blockcontainer.html.twig'));
    }

    /**
     * Gets the public 'sonata.core.date.momentformatconverter' shared service.
     *
     * @return \Sonata\CoreBundle\Date\MomentFormatConverter
     */
    protected function getSonataCoreDateMomentFormatConverterService()
    {
        return $this->services['sonata.core.date.momentformatconverter'] = new \Sonata\CoreBundle\Date\MomentFormatConverter();
    }

    /**
     * Gets the public 'sonata.core.flashmessage.manager' shared service.
     *
     * @return \Sonata\CoreBundle\FlashMessage\FlashManager
     */
    protected function getSonataCoreFlashmessageManagerService()
    {
        return $this->services['sonata.core.flashmessage.manager'] = new \Sonata\CoreBundle\FlashMessage\FlashManager(${($ = isset($this->services['session']) ? $this->services['session'] : $this->get('session')) && false ?: ''}, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, array('success' => array('success' => array('domain' => 'SonataCoreBundle'), 'sonataflashsuccess' => array('domain' => 'SonataAdminBundle'), 'sonatausersuccess' => array('domain' => 'SonataUserBundle'), 'fosusersuccess' => array('domain' => 'FOSUserBundle')), 'warning' => array('warning' => array('domain' => 'SonataCoreBundle'), 'sonataflashinfo' => array('domain' => 'SonataAdminBundle')), 'danger' => array('error' => array('domain' => 'SonataCoreBundle'), 'sonataflasherror' => array('domain' => 'SonataAdminBundle'), 'sonatausererror' => array('domain' => 'SonataUserBundle'))), array('success' => 'success', 'warning' => 'warning', 'danger' => 'danger'));
    }

    /**
     * Gets the public 'sonata.core.flashmessage.twig.extension' shared service.
     *
     * @return \Sonata\CoreBundle\Twig\Extension\FlashMessageExtension
     */
    protected function getSonataCoreFlashmessageTwigExtensionService()
    {
        return $this->services['sonata.core.flashmessage.twig.extension'] = new \Sonata\CoreBundle\Twig\Extension\FlashMessageExtension(${($ = isset($this->services['sonata.core.flashmessage.manager']) ? $this->services['sonata.core.flashmessage.manager'] : $this->get('sonata.core.flashmessage.manager')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.array' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\ImmutableArrayType
     */
    protected function getSonataCoreFormTypeArrayService()
    {
        return $this->services['sonata.core.form.type.array'] = new \Sonata\CoreBundle\Form\Type\ImmutableArrayType();
    }

    /**
     * Gets the public 'sonata.core.form.type.boolean' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\BooleanType
     */
    protected function getSonataCoreFormTypeBooleanService()
    {
        return $this->services['sonata.core.form.type.boolean'] = new \Sonata\CoreBundle\Form\Type\BooleanType();
    }

    /**
     * Gets the public 'sonata.core.form.type.collection' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\CollectionType
     */
    protected function getSonataCoreFormTypeCollectionService()
    {
        return $this->services['sonata.core.form.type.collection'] = new \Sonata\CoreBundle\Form\Type\CollectionType();
    }

    /**
     * Gets the public 'sonata.core.form.type.colorselector' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\ColorSelectorType
     */
    protected function getSonataCoreFormTypeColorSelectorService()
    {
        return $this->services['sonata.core.form.type.colorselector'] = new \Sonata\CoreBundle\Form\Type\ColorSelectorType();
    }

    /**
     * Gets the public 'sonata.core.form.type.datepicker' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DatePickerType
     */
    protected function getSonataCoreFormTypeDatePickerService()
    {
        return $this->services['sonata.core.form.type.datepicker'] = new \Sonata\CoreBundle\Form\Type\DatePickerType(${($ = isset($this->services['sonata.core.date.momentformatconverter']) ? $this->services['sonata.core.date.momentformatconverter'] : $this->get('sonata.core.date.momentformatconverter')) && false ?: ''}, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.daterange' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DateRangeType
     */
    protected function getSonataCoreFormTypeDateRangeService()
    {
        return $this->services['sonata.core.form.type.daterange'] = new \Sonata\CoreBundle\Form\Type\DateRangeType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.daterangepicker' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DateRangePickerType
     */
    protected function getSonataCoreFormTypeDateRangePickerService()
    {
        return $this->services['sonata.core.form.type.daterangepicker'] = new \Sonata\CoreBundle\Form\Type\DateRangePickerType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.datetimepicker' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DateTimePickerType
     */
    protected function getSonataCoreFormTypeDatetimePickerService()
    {
        return $this->services['sonata.core.form.type.datetimepicker'] = new \Sonata\CoreBundle\Form\Type\DateTimePickerType(${($ = isset($this->services['sonata.core.date.momentformatconverter']) ? $this->services['sonata.core.date.momentformatconverter'] : $this->get('sonata.core.date.momentformatconverter')) && false ?: ''}, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.datetimerange' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DateTimeRangeType
     */
    protected function getSonataCoreFormTypeDatetimeRangeService()
    {
        return $this->services['sonata.core.form.type.datetimerange'] = new \Sonata\CoreBundle\Form\Type\DateTimeRangeType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.datetimerangepicker' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\DateTimeRangePickerType
     */
    protected function getSonataCoreFormTypeDatetimeRangePickerService()
    {
        return $this->services['sonata.core.form.type.datetimerangepicker'] = new \Sonata\CoreBundle\Form\Type\DateTimeRangePickerType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.equal' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\EqualType
     */
    protected function getSonataCoreFormTypeEqualService()
    {
        return $this->services['sonata.core.form.type.equal'] = new \Sonata\CoreBundle\Form\Type\EqualType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.form.type.translatablechoice' shared service.
     *
     * @return \Sonata\CoreBundle\Form\Type\TranslatableChoiceType
     */
    protected function getSonataCoreFormTypeTranslatableChoiceService()
    {
        return $this->services['sonata.core.form.type.translatablechoice'] = new \Sonata\CoreBundle\Form\Type\TranslatableChoiceType(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.model.adapter.chain' shared service.
     *
     * @return \Sonata\CoreBundle\Model\Adapter\AdapterChain
     */
    protected function getSonataCoreModelAdapterChainService()
    {
        $this->services['sonata.core.model.adapter.chain'] = $instance = new \Sonata\CoreBundle\Model\Adapter\AdapterChain();

        $instance->addAdapter(new \Sonata\CoreBundle\Model\Adapter\DoctrineORMAdapter(${($ = isset($this->services['doctrine']) ? $this->services['doctrine'] : $this->get('doctrine', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}));

        return $instance;
    }

    /**
     * Gets the public 'sonata.core.slugify.cocur' shared service.
     *
     * @return \Cocur\Slugify\Slugify
     *
     * @deprecated The "sonata.core.slugify.cocur" service is deprecated. You should stop using it, as it will soon be removed.
     */
    protected function getSonataCoreSlugifyCocurService()
    {
        @triggererror('The "sonata.core.slugify.cocur" service is deprecated. You should stop using it, as it will soon be removed.', EUSERDEPRECATED);

        return $this->services['sonata.core.slugify.cocur'] = new \Cocur\Slugify\Slugify();
    }

    /**
     * Gets the public 'sonata.core.slugify.native' shared service.
     *
     * @return \Sonata\CoreBundle\Component\NativeSlugify
     *
     * @deprecated The "sonata.core.slugify.native" service is deprecated. You should stop using it, as it will soon be removed.
     */
    protected function getSonataCoreSlugifyNativeService()
    {
        @triggererror('The "sonata.core.slugify.native" service is deprecated. You should stop using it, as it will soon be removed.', EUSERDEPRECATED);

        return $this->services['sonata.core.slugify.native'] = new \Sonata\CoreBundle\Component\NativeSlugify();
    }

    /**
     * Gets the public 'sonata.core.twig.extension.text' shared service.
     *
     * @return \Sonata\CoreBundle\Twig\Extension\DeprecatedTextExtension
     */
    protected function getSonataCoreTwigExtensionTextService()
    {
        return $this->services['sonata.core.twig.extension.text'] = new \Sonata\CoreBundle\Twig\Extension\DeprecatedTextExtension();
    }

    /**
     * Gets the public 'sonata.core.twig.extension.wrapping' shared service.
     *
     * @return \Sonata\CoreBundle\Twig\Extension\FormTypeExtension
     */
    protected function getSonataCoreTwigExtensionWrappingService()
    {
        return $this->services['sonata.core.twig.extension.wrapping'] = new \Sonata\CoreBundle\Twig\Extension\FormTypeExtension('standard');
    }

    /**
     * Gets the public 'sonata.core.twig.statusextension' shared service.
     *
     * @return \Sonata\CoreBundle\Twig\Extension\StatusExtension
     */
    protected function getSonataCoreTwigStatusExtensionService()
    {
        $this->services['sonata.core.twig.statusextension'] = $instance = new \Sonata\CoreBundle\Twig\Extension\StatusExtension();

        $instance->addStatusService(${($ = isset($this->services['sonata.core.flashmessage.manager']) ? $this->services['sonata.core.flashmessage.manager'] : $this->get('sonata.core.flashmessage.manager')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'sonata.core.twig.templateextension' shared service.
     *
     * @return \Sonata\CoreBundle\Twig\Extension\TemplateExtension
     */
    protected function getSonataCoreTwigTemplateExtensionService()
    {
        return $this->services['sonata.core.twig.templateextension'] = new \Sonata\CoreBundle\Twig\Extension\TemplateExtension(true, ${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, ${($ = isset($this->services['sonata.core.model.adapter.chain']) ? $this->services['sonata.core.model.adapter.chain'] : $this->get('sonata.core.model.adapter.chain')) && false ?: ''});
    }

    /**
     * Gets the public 'sonata.core.validator.inline' shared service.
     *
     * @return \Sonata\CoreBundle\Validator\InlineValidator
     */
    protected function getSonataCoreValidatorInlineService()
    {
        return $this->services['sonata.core.validator.inline'] = new \Sonata\CoreBundle\Validator\InlineValidator($this, ${($ = isset($this->services['validator.validatorfactory']) ? $this->services['validator.validatorfactory'] : $this->getValidatorValidatorFactoryService()) && false ?: ''});
    }

    /**
     * Gets the public 'streamedresponselistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener
     */
    protected function getStreamedResponseListenerService()
    {
        return $this->services['streamedresponselistener'] = new \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener();
    }

    /**
     * Gets the public 'swiftmailer.emailsender.listener' shared service.
     *
     * @return \Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener
     */
    protected function getSwiftmailerEmailSenderListenerService()
    {
        return $this->services['swiftmailer.emailsender.listener'] = new \Symfony\Bundle\SwiftmailerBundle\EventListener\EmailSenderListener($this, ${($ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'swiftmailer.mailer.default' shared service.
     *
     * @return \SwiftMailer
     */
    protected function getSwiftmailerMailerDefaultService()
    {
        return $this->services['swiftmailer.mailer.default'] = new \SwiftMailer(${($ = isset($this->services['swiftmailer.mailer.default.transport']) ? $this->services['swiftmailer.mailer.default.transport'] : $this->get('swiftmailer.mailer.default.transport')) && false ?: ''});
    }

    /**
     * Gets the public 'swiftmailer.mailer.default.plugin.messagelogger' shared service.
     *
     * @return \SwiftPluginsMessageLogger
     */
    protected function getSwiftmailerMailerDefaultPluginMessageloggerService()
    {
        return $this->services['swiftmailer.mailer.default.plugin.messagelogger'] = new \SwiftPluginsMessageLogger();
    }

    /**
     * Gets the public 'swiftmailer.mailer.default.spool' shared service.
     *
     * @return \SwiftMemorySpool
     */
    protected function getSwiftmailerMailerDefaultSpoolService()
    {
        return $this->services['swiftmailer.mailer.default.spool'] = new \SwiftMemorySpool();
    }

    /**
     * Gets the public 'swiftmailer.mailer.default.transport' shared service.
     *
     * @return \SwiftTransportSpoolTransport
     */
    protected function getSwiftmailerMailerDefaultTransportService()
    {
        $this->services['swiftmailer.mailer.default.transport'] = $instance = new \SwiftTransportSpoolTransport(${($ = isset($this->services['swiftmailer.mailer.default.transport.eventdispatcher']) ? $this->services['swiftmailer.mailer.default.transport.eventdispatcher'] : $this->getSwiftmailerMailerDefaultTransportEventdispatcherService()) && false ?: ''}, ${($ = isset($this->services['swiftmailer.mailer.default.spool']) ? $this->services['swiftmailer.mailer.default.spool'] : $this->get('swiftmailer.mailer.default.spool')) && false ?: ''});

        $instance->registerPlugin(${($ = isset($this->services['swiftmailer.mailer.default.plugin.messagelogger']) ? $this->services['swiftmailer.mailer.default.plugin.messagelogger'] : $this->get('swiftmailer.mailer.default.plugin.messagelogger')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'swiftmailer.mailer.default.transport.real' shared service.
     *
     * @return \SwiftTransportEsmtpTransport
     */
    protected function getSwiftmailerMailerDefaultTransportRealService()
    {
        $a = new \SwiftTransportEsmtpAuthHandler(array(0 => new \SwiftTransportEsmtpAuthCramMd5Authenticator(), 1 => new \SwiftTransportEsmtpAuthLoginAuthenticator(), 2 => new \SwiftTransportEsmtpAuthPlainAuthenticator()));
        $a->setUsername('itap.test.for.studens@gmail.com');
        $a->setPassword('1qaz@WSX29');
        $a->setAuthMode('login');

        $this->services['swiftmailer.mailer.default.transport.real'] = $instance = new \SwiftTransportEsmtpTransport(new \SwiftTransportStreamBuffer(new \SwiftStreamFiltersStringReplacementFilterFactory()), array(0 => $a), ${($ = isset($this->services['swiftmailer.mailer.default.transport.eventdispatcher']) ? $this->services['swiftmailer.mailer.default.transport.eventdispatcher'] : $this->getSwiftmailerMailerDefaultTransportEventdispatcherService()) && false ?: ''});

        $instance->setHost('smtp.gmail.com');
        $instance->setPort(465);
        $instance->setEncryption('ssl');
        $instance->setTimeout(30);
        $instance->setSourceIp(NULL);
        (new \Symfony\Bundle\SwiftmailerBundle\DependencyInjection\SmtpTransportConfigurator(NULL, ${($ = isset($this->services['router.requestcontext']) ? $this->services['router.requestcontext'] : $this->getRouterRequestContextService()) && false ?: ''}))->configure($instance);

        return $instance;
    }

    /**
     * Gets the public 'templating' shared service.
     *
     * @return \Symfony\Bundle\TwigBundle\TwigEngine
     */
    protected function getTemplatingService()
    {
        return $this->services['templating'] = new \Symfony\Bundle\TwigBundle\TwigEngine(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, ${($ = isset($this->services['templating.nameparser']) ? $this->services['templating.nameparser'] : $this->get('templating.nameparser')) && false ?: ''}, ${($ = isset($this->services['templating.locator']) ? $this->services['templating.locator'] : $this->getTemplatingLocatorService()) && false ?: ''});
    }

    /**
     * Gets the public 'templating.filenameparser' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Templating\TemplateFilenameParser
     */
    protected function getTemplatingFilenameParserService()
    {
        return $this->services['templating.filenameparser'] = new \Symfony\Bundle\FrameworkBundle\Templating\TemplateFilenameParser();
    }

    /**
     * Gets the public 'templating.helper.logouturl' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Templating\Helper\LogoutUrlHelper
     */
    protected function getTemplatingHelperLogoutUrlService()
    {
        return $this->services['templating.helper.logouturl'] = new \Symfony\Bundle\SecurityBundle\Templating\Helper\LogoutUrlHelper(${($ = isset($this->services['security.logouturlgenerator']) ? $this->services['security.logouturlgenerator'] : $this->getSecurityLogoutUrlGeneratorService()) && false ?: ''});
    }

    /**
     * Gets the public 'templating.helper.security' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Templating\Helper\SecurityHelper
     */
    protected function getTemplatingHelperSecurityService()
    {
        return $this->services['templating.helper.security'] = new \Symfony\Bundle\SecurityBundle\Templating\Helper\SecurityHelper(${($ = isset($this->services['security.authorizationchecker']) ? $this->services['security.authorizationchecker'] : $this->get('security.authorizationchecker', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'templating.loader' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Templating\Loader\FilesystemLoader
     */
    protected function getTemplatingLoaderService()
    {
        return $this->services['templating.loader'] = new \Symfony\Bundle\FrameworkBundle\Templating\Loader\FilesystemLoader(${($ = isset($this->services['templating.locator']) ? $this->services['templating.locator'] : $this->getTemplatingLocatorService()) && false ?: ''});
    }

    /**
     * Gets the public 'templating.nameparser' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Templating\TemplateNameParser
     */
    protected function getTemplatingNameParserService()
    {
        return $this->services['templating.nameparser'] = new \Symfony\Bundle\FrameworkBundle\Templating\TemplateNameParser(${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''});
    }

    /**
     * Gets the public 'translation.dumper.csv' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\CsvFileDumper
     */
    protected function getTranslationDumperCsvService()
    {
        return $this->services['translation.dumper.csv'] = new \Symfony\Component\Translation\Dumper\CsvFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.ini' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\IniFileDumper
     */
    protected function getTranslationDumperIniService()
    {
        return $this->services['translation.dumper.ini'] = new \Symfony\Component\Translation\Dumper\IniFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.json' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\JsonFileDumper
     */
    protected function getTranslationDumperJsonService()
    {
        return $this->services['translation.dumper.json'] = new \Symfony\Component\Translation\Dumper\JsonFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.mo' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\MoFileDumper
     */
    protected function getTranslationDumperMoService()
    {
        return $this->services['translation.dumper.mo'] = new \Symfony\Component\Translation\Dumper\MoFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.php' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\PhpFileDumper
     */
    protected function getTranslationDumperPhpService()
    {
        return $this->services['translation.dumper.php'] = new \Symfony\Component\Translation\Dumper\PhpFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.po' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\PoFileDumper
     */
    protected function getTranslationDumperPoService()
    {
        return $this->services['translation.dumper.po'] = new \Symfony\Component\Translation\Dumper\PoFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.qt' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\QtFileDumper
     */
    protected function getTranslationDumperQtService()
    {
        return $this->services['translation.dumper.qt'] = new \Symfony\Component\Translation\Dumper\QtFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.res' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\IcuResFileDumper
     */
    protected function getTranslationDumperResService()
    {
        return $this->services['translation.dumper.res'] = new \Symfony\Component\Translation\Dumper\IcuResFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.xliff' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\XliffFileDumper
     */
    protected function getTranslationDumperXliffService()
    {
        return $this->services['translation.dumper.xliff'] = new \Symfony\Component\Translation\Dumper\XliffFileDumper();
    }

    /**
     * Gets the public 'translation.dumper.yml' shared service.
     *
     * @return \Symfony\Component\Translation\Dumper\YamlFileDumper
     */
    protected function getTranslationDumperYmlService()
    {
        return $this->services['translation.dumper.yml'] = new \Symfony\Component\Translation\Dumper\YamlFileDumper();
    }

    /**
     * Gets the public 'translation.extractor' shared service.
     *
     * @return \Symfony\Component\Translation\Extractor\ChainExtractor
     */
    protected function getTranslationExtractorService()
    {
        $this->services['translation.extractor'] = $instance = new \Symfony\Component\Translation\Extractor\ChainExtractor();

        $instance->addExtractor('php', ${($ = isset($this->services['translation.extractor.php']) ? $this->services['translation.extractor.php'] : $this->get('translation.extractor.php')) && false ?: ''});
        $instance->addExtractor('twig', ${($ = isset($this->services['twig.translation.extractor']) ? $this->services['twig.translation.extractor'] : $this->get('twig.translation.extractor')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'translation.extractor.php' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\PhpExtractor
     */
    protected function getTranslationExtractorPhpService()
    {
        return $this->services['translation.extractor.php'] = new \Symfony\Bundle\FrameworkBundle\Translation\PhpExtractor();
    }

    /**
     * Gets the public 'translation.loader' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\TranslationLoader
     */
    protected function getTranslationLoaderService()
    {
        $a = ${($ = isset($this->services['translation.loader.xliff']) ? $this->services['translation.loader.xliff'] : $this->get('translation.loader.xliff')) && false ?: ''};

        $this->services['translation.loader'] = $instance = new \Symfony\Bundle\FrameworkBundle\Translation\TranslationLoader();

        $instance->addLoader('php', ${($ = isset($this->services['translation.loader.php']) ? $this->services['translation.loader.php'] : $this->get('translation.loader.php')) && false ?: ''});
        $instance->addLoader('yml', ${($ = isset($this->services['translation.loader.yml']) ? $this->services['translation.loader.yml'] : $this->get('translation.loader.yml')) && false ?: ''});
        $instance->addLoader('xlf', $a);
        $instance->addLoader('xliff', $a);
        $instance->addLoader('po', ${($ = isset($this->services['translation.loader.po']) ? $this->services['translation.loader.po'] : $this->get('translation.loader.po')) && false ?: ''});
        $instance->addLoader('mo', ${($ = isset($this->services['translation.loader.mo']) ? $this->services['translation.loader.mo'] : $this->get('translation.loader.mo')) && false ?: ''});
        $instance->addLoader('ts', ${($ = isset($this->services['translation.loader.qt']) ? $this->services['translation.loader.qt'] : $this->get('translation.loader.qt')) && false ?: ''});
        $instance->addLoader('csv', ${($ = isset($this->services['translation.loader.csv']) ? $this->services['translation.loader.csv'] : $this->get('translation.loader.csv')) && false ?: ''});
        $instance->addLoader('res', ${($ = isset($this->services['translation.loader.res']) ? $this->services['translation.loader.res'] : $this->get('translation.loader.res')) && false ?: ''});
        $instance->addLoader('dat', ${($ = isset($this->services['translation.loader.dat']) ? $this->services['translation.loader.dat'] : $this->get('translation.loader.dat')) && false ?: ''});
        $instance->addLoader('ini', ${($ = isset($this->services['translation.loader.ini']) ? $this->services['translation.loader.ini'] : $this->get('translation.loader.ini')) && false ?: ''});
        $instance->addLoader('json', ${($ = isset($this->services['translation.loader.json']) ? $this->services['translation.loader.json'] : $this->get('translation.loader.json')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'translation.loader.csv' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\CsvFileLoader
     */
    protected function getTranslationLoaderCsvService()
    {
        return $this->services['translation.loader.csv'] = new \Symfony\Component\Translation\Loader\CsvFileLoader();
    }

    /**
     * Gets the public 'translation.loader.dat' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\IcuDatFileLoader
     */
    protected function getTranslationLoaderDatService()
    {
        return $this->services['translation.loader.dat'] = new \Symfony\Component\Translation\Loader\IcuDatFileLoader();
    }

    /**
     * Gets the public 'translation.loader.ini' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\IniFileLoader
     */
    protected function getTranslationLoaderIniService()
    {
        return $this->services['translation.loader.ini'] = new \Symfony\Component\Translation\Loader\IniFileLoader();
    }

    /**
     * Gets the public 'translation.loader.json' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\JsonFileLoader
     */
    protected function getTranslationLoaderJsonService()
    {
        return $this->services['translation.loader.json'] = new \Symfony\Component\Translation\Loader\JsonFileLoader();
    }

    /**
     * Gets the public 'translation.loader.mo' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\MoFileLoader
     */
    protected function getTranslationLoaderMoService()
    {
        return $this->services['translation.loader.mo'] = new \Symfony\Component\Translation\Loader\MoFileLoader();
    }

    /**
     * Gets the public 'translation.loader.php' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\PhpFileLoader
     */
    protected function getTranslationLoaderPhpService()
    {
        return $this->services['translation.loader.php'] = new \Symfony\Component\Translation\Loader\PhpFileLoader();
    }

    /**
     * Gets the public 'translation.loader.po' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\PoFileLoader
     */
    protected function getTranslationLoaderPoService()
    {
        return $this->services['translation.loader.po'] = new \Symfony\Component\Translation\Loader\PoFileLoader();
    }

    /**
     * Gets the public 'translation.loader.qt' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\QtFileLoader
     */
    protected function getTranslationLoaderQtService()
    {
        return $this->services['translation.loader.qt'] = new \Symfony\Component\Translation\Loader\QtFileLoader();
    }

    /**
     * Gets the public 'translation.loader.res' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\IcuResFileLoader
     */
    protected function getTranslationLoaderResService()
    {
        return $this->services['translation.loader.res'] = new \Symfony\Component\Translation\Loader\IcuResFileLoader();
    }

    /**
     * Gets the public 'translation.loader.xliff' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\XliffFileLoader
     */
    protected function getTranslationLoaderXliffService()
    {
        return $this->services['translation.loader.xliff'] = new \Symfony\Component\Translation\Loader\XliffFileLoader();
    }

    /**
     * Gets the public 'translation.loader.yml' shared service.
     *
     * @return \Symfony\Component\Translation\Loader\YamlFileLoader
     */
    protected function getTranslationLoaderYmlService()
    {
        return $this->services['translation.loader.yml'] = new \Symfony\Component\Translation\Loader\YamlFileLoader();
    }

    /**
     * Gets the public 'translation.writer' shared service.
     *
     * @return \Symfony\Component\Translation\Writer\TranslationWriter
     */
    protected function getTranslationWriterService()
    {
        $this->services['translation.writer'] = $instance = new \Symfony\Component\Translation\Writer\TranslationWriter();

        $instance->addDumper('php', ${($ = isset($this->services['translation.dumper.php']) ? $this->services['translation.dumper.php'] : $this->get('translation.dumper.php')) && false ?: ''});
        $instance->addDumper('xlf', ${($ = isset($this->services['translation.dumper.xliff']) ? $this->services['translation.dumper.xliff'] : $this->get('translation.dumper.xliff')) && false ?: ''});
        $instance->addDumper('po', ${($ = isset($this->services['translation.dumper.po']) ? $this->services['translation.dumper.po'] : $this->get('translation.dumper.po')) && false ?: ''});
        $instance->addDumper('mo', ${($ = isset($this->services['translation.dumper.mo']) ? $this->services['translation.dumper.mo'] : $this->get('translation.dumper.mo')) && false ?: ''});
        $instance->addDumper('yml', ${($ = isset($this->services['translation.dumper.yml']) ? $this->services['translation.dumper.yml'] : $this->get('translation.dumper.yml')) && false ?: ''});
        $instance->addDumper('ts', ${($ = isset($this->services['translation.dumper.qt']) ? $this->services['translation.dumper.qt'] : $this->get('translation.dumper.qt')) && false ?: ''});
        $instance->addDumper('csv', ${($ = isset($this->services['translation.dumper.csv']) ? $this->services['translation.dumper.csv'] : $this->get('translation.dumper.csv')) && false ?: ''});
        $instance->addDumper('ini', ${($ = isset($this->services['translation.dumper.ini']) ? $this->services['translation.dumper.ini'] : $this->get('translation.dumper.ini')) && false ?: ''});
        $instance->addDumper('json', ${($ = isset($this->services['translation.dumper.json']) ? $this->services['translation.dumper.json'] : $this->get('translation.dumper.json')) && false ?: ''});
        $instance->addDumper('res', ${($ = isset($this->services['translation.dumper.res']) ? $this->services['translation.dumper.res'] : $this->get('translation.dumper.res')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the public 'translator' shared service.
     *
     * @return \Symfony\Component\Translation\DataCollectorTranslator
     */
    protected function getTranslatorService()
    {
        return $this->services['translator'] = new \Symfony\Component\Translation\DataCollectorTranslator(new \Symfony\Component\Translation\LoggingTranslator(${($ = isset($this->services['translator.default']) ? $this->services['translator.default'] : $this->get('translator.default')) && false ?: ''}, ${($ = isset($this->services['monolog.logger.translation']) ? $this->services['monolog.logger.translation'] : $this->get('monolog.logger.translation')) && false ?: ''}));
    }

    /**
     * Gets the public 'translator.default' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected function getTranslatorDefaultService()
    {
        $this->services['translator.default'] = $instance = new \Symfony\Bundle\FrameworkBundle\Translation\Translator(new \Symfony\Component\DependencyInjection\ServiceLocator(array('translation.loader.csv' => function () {
            return ${($ = isset($this->services['translation.loader.csv']) ? $this->services['translation.loader.csv'] : $this->get('translation.loader.csv')) && false ?: ''};
        }, 'translation.loader.dat' => function () {
            return ${($ = isset($this->services['translation.loader.dat']) ? $this->services['translation.loader.dat'] : $this->get('translation.loader.dat')) && false ?: ''};
        }, 'translation.loader.ini' => function () {
            return ${($ = isset($this->services['translation.loader.ini']) ? $this->services['translation.loader.ini'] : $this->get('translation.loader.ini')) && false ?: ''};
        }, 'translation.loader.json' => function () {
            return ${($ = isset($this->services['translation.loader.json']) ? $this->services['translation.loader.json'] : $this->get('translation.loader.json')) && false ?: ''};
        }, 'translation.loader.mo' => function () {
            return ${($ = isset($this->services['translation.loader.mo']) ? $this->services['translation.loader.mo'] : $this->get('translation.loader.mo')) && false ?: ''};
        }, 'translation.loader.php' => function () {
            return ${($ = isset($this->services['translation.loader.php']) ? $this->services['translation.loader.php'] : $this->get('translation.loader.php')) && false ?: ''};
        }, 'translation.loader.po' => function () {
            return ${($ = isset($this->services['translation.loader.po']) ? $this->services['translation.loader.po'] : $this->get('translation.loader.po')) && false ?: ''};
        }, 'translation.loader.qt' => function () {
            return ${($ = isset($this->services['translation.loader.qt']) ? $this->services['translation.loader.qt'] : $this->get('translation.loader.qt')) && false ?: ''};
        }, 'translation.loader.res' => function () {
            return ${($ = isset($this->services['translation.loader.res']) ? $this->services['translation.loader.res'] : $this->get('translation.loader.res')) && false ?: ''};
        }, 'translation.loader.xliff' => function () {
            return ${($ = isset($this->services['translation.loader.xliff']) ? $this->services['translation.loader.xliff'] : $this->get('translation.loader.xliff')) && false ?: ''};
        }, 'translation.loader.yml' => function () {
            return ${($ = isset($this->services['translation.loader.yml']) ? $this->services['translation.loader.yml'] : $this->get('translation.loader.yml')) && false ?: ''};
        })), new \Symfony\Component\Translation\MessageSelector(), 'en', array('translation.loader.php' => array(0 => 'php'), 'translation.loader.yml' => array(0 => 'yml'), 'translation.loader.xliff' => array(0 => 'xlf', 1 => 'xliff'), 'translation.loader.po' => array(0 => 'po'), 'translation.loader.mo' => array(0 => 'mo'), 'translation.loader.qt' => array(0 => 'ts'), 'translation.loader.csv' => array(0 => 'csv'), 'translation.loader.res' => array(0 => 'res'), 'translation.loader.dat' => array(0 => 'dat'), 'translation.loader.ini' => array(0 => 'ini'), 'translation.loader.json' => array(0 => 'json')), array('cachedir' => (DIR.'/translations'), 'debug' => true, 'resourcefiles' => array('sq' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.sq.xlf')), 'id' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.id.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.id.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.id.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.id.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.id.yml')), 'af' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.af.xlf'), 1 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.af.yml')), 'fr' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.fr.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.fr.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.fr.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.fr.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.fr.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.fr.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/translations/SonataBlockBundle.fr.xliff'), 7 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.fr.xliff')), 'eu' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.eu.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.eu.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.eu.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.eu.yml'), 4 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.eu.xliff'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.eu.xliff')), 'fa' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.fa.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.fa.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.fa.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.fa.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.fa.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.fa.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.fa.xliff')), 'el' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.el.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.el.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.el.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.el.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.el.yml')), 'bg' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.bg.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.bg.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.bg.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.bg.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.bg.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.bg.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.bg.xliff')), 'es' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.es.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.es.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.es.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.es.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.es.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.es.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.es.xliff')), 'de' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.de.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.de.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.de.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.de.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.de.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.de.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/translations/SonataBlockBundle.de.xliff'), 7 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.de.xliff')), 'hr' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.hr.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.hr.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.hr.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.hr.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.hr.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.hr.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.hr.xliff')), 'lt' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.lt.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.lt.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.lt.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.lt.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.lt.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.lt.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.lt.xliff')), 'lb' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.lb.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.lb.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.lb.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.lb.yml'), 4 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.lb.xliff'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.lb.xliff')), 'pt' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.pt.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.pt.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.pt.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.pt.yml'), 4 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.pt.xliff'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.pt.xliff')), 'ru' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ru.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ru.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ru.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ru.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ru.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ru.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ru.xliff')), 'he' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.he.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.he.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.he.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.he.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.he.yml')), 'en' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.en.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.en.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.en.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.en.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.en.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.en.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/translations/SonataBlockBundle.en.xliff'), 7 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.en.xliff')), 'nl' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.nl.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.nl.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.nl.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.nl.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.nl.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.nl.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.nl.xliff')), 'lv' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.lv.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.lv.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.lv.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.lv.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.lv.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.lv.xliff')), 'mn' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.mn.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.mn.xlf')), 'it' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.it.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.it.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.it.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.it.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.it.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.it.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/translations/SonataBlockBundle.it.xliff'), 7 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.it.xliff')), 'hu' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.hu.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.hu.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.hu.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.hu.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.hu.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.hu.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/translations/SonataBlockBundle.hu.xliff'), 7 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.hu.xliff')), 'ro' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ro.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ro.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ro.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ro.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ro.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ro.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ro.xliff')), 'nn' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.nn.xlf')), 'ptBR' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ptBR.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ptBR.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ptBR.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ptBR.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ptBR.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ptBR.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ptBR.xliff')), 'zhCN' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.zhCN.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.zhCN.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.zhCN.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.zhCN.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.zhCN.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.zhCN.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.zhCN.xliff')), 'tr' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.tr.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.tr.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.tr.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.tr.yml')), 'hy' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.hy.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.hy.xlf')), 'sl' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.sl.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.sl.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.sl.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.sl.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.sl.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.sl.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.sl.xliff')), 'srLatn' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.srLatn.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.srLatn.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.srLatn.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.srLatn.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.srLatn.yml')), 'sk' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.sk.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.sk.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.sk.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.sk.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.sk.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.sk.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.sk.xliff')), 'uk' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.uk.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.uk.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.uk.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.uk.yml'), 4 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.uk.xliff'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.uk.xliff')), 'th' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.th.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.th.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.th.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.th.yml')), 'ja' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ja.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ja.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ja.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ja.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ja.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ja.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ja.xliff')), 'ar' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ar.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ar.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ar.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ar.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ar.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ar.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ar.xliff')), 'ca' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.ca.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.ca.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ca.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ca.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ca.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.ca.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.ca.xliff')), 'vi' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.vi.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.vi.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.vi.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.vi.yml')), 'az' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.az.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.az.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.az.xlf')), 'cs' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.cs.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.cs.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.cs.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.cs.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.cs.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.cs.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.cs.xliff')), 'srCyrl' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.srCyrl.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.srCyrl.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.srCyrl.xlf')), 'da' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.da.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.da.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.da.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.da.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.da.yml')), 'zhTW' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.zhTW.xlf')), 'gl' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.gl.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.gl.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.gl.xlf')), 'sv' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.sv.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.sv.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.sv.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.sv.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.sv.yml')), 'pl' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.pl.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.pl.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.pl.xlf'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.pl.yml'), 4 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.pl.yml'), 5 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.pl.xliff'), 6 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.pl.xliff')), 'cy' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.cy.xlf')), 'et' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.et.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.et.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.et.yml')), 'no' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.no.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.no.xlf'), 2 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.no.xlf'), 3 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.no.xliff')), 'fi' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Validator/Resources/translations/validators.fi.xlf'), 1 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/translations/validators.fi.xlf'), 2 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.fi.yml'), 3 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.fi.yml'), 4 => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/translations/SonataCoreBundle.fi.xliff')), 'ua' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ua.xlf')), 'ptPT' => array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Security/Core/Resources/translations/security.ptPT.xlf')), 'eo' => array(0 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.eo.yml'), 1 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.eo.yml')), 'ky' => array(0 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.ky.yml'), 1 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.ky.yml')), 'nb' => array(0 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/FOSUserBundle.nb.yml'), 1 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/translations/validators.nb.yml')), 'svSE' => array(0 => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/translations/SonataAdminBundle.svSE.xliff')))));

        $instance->setConfigCacheFactory(${($ = isset($this->services['configcachefactory']) ? $this->services['configcachefactory'] : $this->get('configcachefactory')) && false ?: ''});
        $instance->setFallbackLocales(array(0 => 'en'));

        return $instance;
    }

    /**
     * Gets the public 'translatorlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\TranslatorListener
     */
    protected function getTranslatorListenerService()
    {
        return $this->services['translatorlistener'] = new \Symfony\Component\HttpKernel\EventListener\TranslatorListener(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''});
    }

    /**
     * Gets the public 'twig' shared service.
     *
     * @return \Twig\Environment
     */
    protected function getTwigService()
    {
        $a = ${($ = isset($this->services['debug.stopwatch']) ? $this->services['debug.stopwatch'] : $this->get('debug.stopwatch', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''};
        $b = ${($ = isset($this->services['debug.filelinkformatter']) ? $this->services['debug.filelinkformatter'] : $this->getDebugFileLinkFormatterService()) && false ?: ''};
        $c = ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''};
        $d = ${($ = isset($this->services['knpmenu.matcher']) ? $this->services['knpmenu.matcher'] : $this->get('knpmenu.matcher')) && false ?: ''};

        $e = new \Knp\Menu\Util\MenuManipulator();

        $f = new \Symfony\Component\VarDumper\Dumper\HtmlDumper(NULL, 'UTF-8', 0);
        $f->setDisplayOptions(array('fileLinkFormat' => $b));

        $g = new \Symfony\Component\VarDumper\Dumper\HtmlDumper(NULL, 'UTF-8', 1);
        $g->setDisplayOptions(array('maxStringLength' => 4096, 'fileLinkFormat' => $b));

        $h = new \Symfony\Bridge\Twig\AppVariable();
        $h->setEnvironment('dev');
        $h->setDebug(true);
        if ($this->has('security.tokenstorage')) {
            $h->setTokenStorage(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
        }
        if ($this->has('requeststack')) {
            $h->setRequestStack($c);
        }

        $this->services['twig'] = $instance = new \Twig\Environment(${($ = isset($this->services['twig.loader']) ? $this->services['twig.loader'] : $this->get('twig.loader')) && false ?: ''}, array('debug' => true, 'strictvariables' => true, 'formthemes' => array(0 => 'formdivlayout.html.twig', 1 => 'bootstrap3horizontallayout.html.twig'), 'exceptioncontroller' => 'twig.controller.exception:showAction', 'autoescape' => 'name', 'cache' => (DIR.'/twig'), 'charset' => 'UTF-8', 'paths' => array(), 'date' => array('format' => 'F j, Y H:i', 'intervalformat' => '%d days', 'timezone' => NULL), 'numberformat' => array('decimals' => 0, 'decimalpoint' => '.', 'thousandsseparator' => ',')));

        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\LogoutUrlExtension(${($ = isset($this->services['security.logouturlgenerator']) ? $this->services['security.logouturlgenerator'] : $this->getSecurityLogoutUrlGeneratorService()) && false ?: ''}));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\SecurityExtension(${($ = isset($this->services['security.authorizationchecker']) ? $this->services['security.authorizationchecker'] : $this->get('security.authorizationchecker', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\ProfilerExtension(${($ = isset($this->services['twig.profile']) ? $this->services['twig.profile'] : $this->get('twig.profile')) && false ?: ''}, $a));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\TranslationExtension(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''}));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\AssetExtension(${($ = isset($this->services['assets.packages']) ? $this->services['assets.packages'] : $this->get('assets.packages')) && false ?: ''}));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\CodeExtension($b, ($this->targetDirs[1].'/app'), 'UTF-8'));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\RoutingExtension(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router')) && false ?: ''}));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\YamlExtension());
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\StopwatchExtension($a, true));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\ExpressionExtension());
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\HttpKernelExtension());
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\HttpFoundationExtension($c, ${($ = isset($this->services['router.requestcontext']) ? $this->services['router.requestcontext'] : $this->getRouterRequestContextService()) && false ?: ''}));
        $instance->addExtension(new \Twig\Extension\DebugExtension());
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\FormExtension(array(0 => $this, 1 => 'twig.form.renderer')));
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\WebLinkExtension($c));
        $instance->addExtension(new \Doctrine\Bundle\DoctrineBundle\Twig\DoctrineExtension());
        $instance->addExtension(${($ = isset($this->services['sonata.core.flashmessage.twig.extension']) ? $this->services['sonata.core.flashmessage.twig.extension'] : $this->get('sonata.core.flashmessage.twig.extension')) && false ?: ''});
        $instance->addExtension(${($ = isset($this->services['sonata.core.twig.extension.wrapping']) ? $this->services['sonata.core.twig.extension.wrapping'] : $this->get('sonata.core.twig.extension.wrapping')) && false ?: ''});
        $instance->addExtension(${($ = isset($this->services['sonata.core.twig.extension.text']) ? $this->services['sonata.core.twig.extension.text'] : $this->get('sonata.core.twig.extension.text')) && false ?: ''});
        $instance->addExtension(${($ = isset($this->services['sonata.core.twig.statusextension']) ? $this->services['sonata.core.twig.statusextension'] : $this->get('sonata.core.twig.statusextension')) && false ?: ''});
        $instance->addExtension(${($ = isset($this->services['sonata.core.twig.templateextension']) ? $this->services['sonata.core.twig.templateextension'] : $this->get('sonata.core.twig.templateextension')) && false ?: ''});
        $instance->addExtension(new \Sonata\BlockBundle\Twig\Extension\BlockExtension(${($ = isset($this->services['sonata.block.templating.helper']) ? $this->services['sonata.block.templating.helper'] : $this->get('sonata.block.templating.helper')) && false ?: ''}));
        $instance->addExtension(new \Knp\Menu\Twig\MenuExtension(new \Knp\Menu\Twig\Helper(${($ = isset($this->services['knpmenu.rendererprovider']) ? $this->services['knpmenu.rendererprovider'] : $this->get('knpmenu.rendererprovider')) && false ?: ''}, ${($ = isset($this->services['knpmenu.menuprovider']) ? $this->services['knpmenu.menuprovider'] : $this->get('knpmenu.menuprovider')) && false ?: ''}, $e, $d), $d, $e));
        $instance->addExtension(${($ = isset($this->services['sonata.admin.twig.extension']) ? $this->services['sonata.admin.twig.extension'] : $this->get('sonata.admin.twig.extension')) && false ?: ''});
        $instance->addExtension(new \Symfony\Bridge\Twig\Extension\DumpExtension(${($ = isset($this->services['vardumper.cloner']) ? $this->services['vardumper.cloner'] : $this->get('vardumper.cloner')) && false ?: ''}, $f));
        $instance->addExtension(new \Symfony\Bundle\WebProfilerBundle\Twig\WebProfilerExtension($g));
        $instance->addGlobal('app', $h);
        $instance->addRuntimeLoader(new \Twig\RuntimeLoader\ContainerRuntimeLoader(new \Symfony\Component\DependencyInjection\ServiceLocator(array('Symfony\\Bridge\\Twig\\Extension\\HttpKernelRuntime' => function () {
            return ${($ = isset($this->services['twig.runtime.httpkernel']) ? $this->services['twig.runtime.httpkernel'] : $this->get('twig.runtime.httpkernel')) && false ?: ''};
        }, 'Symfony\\Bridge\\Twig\\Form\\TwigRenderer' => function () {
            return ${($ = isset($this->services['twig.form.renderer']) ? $this->services['twig.form.renderer'] : $this->get('twig.form.renderer')) && false ?: ''};
        }))));
        $instance->addGlobal('sonatablock', ${($ = isset($this->services['sonata.block.twig.global']) ? $this->services['sonata.block.twig.global'] : $this->get('sonata.block.twig.global')) && false ?: ''});
        $instance->addGlobal('sonataadmin', ${($ = isset($this->services['sonata.admin.twig.global']) ? $this->services['sonata.admin.twig.global'] : $this->get('sonata.admin.twig.global')) && false ?: ''});
        (new \Symfony\Bundle\TwigBundle\DependencyInjection\Configurator\EnvironmentConfigurator('F j, Y H:i', '%d days', NULL, 0, '.', ','))->configure($instance);

        return $instance;
    }

    /**
     * Gets the public 'twig.controller.exception' shared service.
     *
     * @return \Symfony\Bundle\TwigBundle\Controller\ExceptionController
     */
    protected function getTwigControllerExceptionService()
    {
        return $this->services['twig.controller.exception'] = new \Symfony\Bundle\TwigBundle\Controller\ExceptionController(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, true);
    }

    /**
     * Gets the public 'twig.controller.previewerror' shared service.
     *
     * @return \Symfony\Bundle\TwigBundle\Controller\PreviewErrorController
     */
    protected function getTwigControllerPreviewErrorService()
    {
        return $this->services['twig.controller.previewerror'] = new \Symfony\Bundle\TwigBundle\Controller\PreviewErrorController(${($ = isset($this->services['httpkernel']) ? $this->services['httpkernel'] : $this->get('httpkernel')) && false ?: ''}, 'twig.controller.exception:showAction');
    }

    /**
     * Gets the public 'twig.exceptionlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ExceptionListener
     */
    protected function getTwigExceptionListenerService()
    {
        return $this->services['twig.exceptionlistener'] = new \Symfony\Component\HttpKernel\EventListener\ExceptionListener('twig.controller.exception:showAction', ${($ = isset($this->services['monolog.logger.request']) ? $this->services['monolog.logger.request'] : $this->get('monolog.logger.request', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'twig.form.renderer' shared service.
     *
     * @return \Symfony\Bridge\Twig\Form\TwigRenderer
     */
    protected function getTwigFormRendererService()
    {
        return $this->services['twig.form.renderer'] = new \Symfony\Bridge\Twig\Form\TwigRenderer(new \Symfony\Bridge\Twig\Form\TwigRendererEngine(array(0 => 'formdivlayout.html.twig', 1 => 'bootstrap3horizontallayout.html.twig'), ${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}), ${($ = isset($this->services['security.csrf.tokenmanager']) ? $this->services['security.csrf.tokenmanager'] : $this->get('security.csrf.tokenmanager', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'twig.loader' shared service.
     *
     * @return \Symfony\Bundle\TwigBundle\Loader\FilesystemLoader
     */
    protected function getTwigLoaderService()
    {
        $this->services['twig.loader'] = $instance = new \Symfony\Bundle\TwigBundle\Loader\FilesystemLoader(${($ = isset($this->services['templating.locator']) ? $this->services['templating.locator'] : $this->getTemplatingLocatorService()) && false ?: ''}, ${($ = isset($this->services['templating.nameparser']) ? $this->services['templating.nameparser'] : $this->get('templating.nameparser')) && false ?: ''}, $this->targetDirs[1]);

        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views'), 'Framework');
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views'), 'Security');
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views'), 'Twig');
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/swiftmailer-bundle/Resources/views'), 'Swiftmailer');
        $instance->addPath(($this->targetDirs[1].'/vendor/doctrine/doctrine-bundle/Resources/views'), 'Doctrine');
        $instance->addPath(($this->targetDirs[1].'/app/Resources/FOSUserBundle/views'), 'FOSUser');
        $instance->addPath(($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/views'), 'FOSUser');
        $instance->addPath(($this->targetDirs[1].'/vendor/sonata-project/core-bundle/Resources/views'), 'SonataCore');
        $instance->addPath(($this->targetDirs[1].'/vendor/sonata-project/block-bundle/Resources/views'), 'SonataBlock');
        $instance->addPath(($this->targetDirs[1].'/vendor/knplabs/knp-menu-bundle/Resources/views'), 'KnpMenu');
        $instance->addPath(($this->targetDirs[1].'/vendor/sonata-project/doctrine-orm-admin-bundle/Resources/views'), 'SonataDoctrineORMAdmin');
        $instance->addPath(($this->targetDirs[1].'/vendor/sonata-project/admin-bundle/Resources/views'), 'SonataAdmin');
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/DebugBundle/Resources/views'), 'Debug');
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views'), 'WebProfiler');
        $instance->addPath(($this->targetDirs[1].'/app/Resources/views'));
        $instance->addPath(($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form'));
        $instance->addPath(($this->targetDirs[1].'/vendor/knplabs/knp-menu/src/Knp/Menu/Resources/views'));

        return $instance;
    }

    /**
     * Gets the public 'twig.profile' shared service.
     *
     * @return \Twig\Profiler\Profile
     */
    protected function getTwigProfileService()
    {
        return $this->services['twig.profile'] = new \Twig\Profiler\Profile();
    }

    /**
     * Gets the public 'twig.runtime.httpkernel' shared service.
     *
     * @return \Symfony\Bridge\Twig\Extension\HttpKernelRuntime
     */
    protected function getTwigRuntimeHttpkernelService()
    {
        return $this->services['twig.runtime.httpkernel'] = new \Symfony\Bridge\Twig\Extension\HttpKernelRuntime(${($ = isset($this->services['fragment.handler']) ? $this->services['fragment.handler'] : $this->get('fragment.handler')) && false ?: ''});
    }

    /**
     * Gets the public 'twig.translation.extractor' shared service.
     *
     * @return \Symfony\Bridge\Twig\Translation\TwigExtractor
     */
    protected function getTwigTranslationExtractorService()
    {
        return $this->services['twig.translation.extractor'] = new \Symfony\Bridge\Twig\Translation\TwigExtractor(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''});
    }

    /**
     * Gets the public 'urisigner' shared service.
     *
     * @return \Symfony\Component\HttpKernel\UriSigner
     */
    protected function getUriSignerService()
    {
        return $this->services['urisigner'] = new \Symfony\Component\HttpKernel\UriSigner('ThisTokenIsNotSoSecretChangeIt');
    }

    /**
     * Gets the public 'validaterequestlistener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener
     */
    protected function getValidateRequestListenerService()
    {
        return $this->services['validaterequestlistener'] = new \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener();
    }

    /**
     * Gets the public 'validator' shared service.
     *
     * @return \Symfony\Component\Validator\Validator\ValidatorInterface
     */
    protected function getValidatorService()
    {
        return $this->services['validator'] = ${($ = isset($this->services['validator.builder']) ? $this->services['validator.builder'] : $this->get('validator.builder')) && false ?: ''}->getValidator();
    }

    /**
     * Gets the public 'validator.builder' shared service.
     *
     * @return \Symfony\Component\Validator\ValidatorBuilderInterface
     */
    protected function getValidatorBuilderService()
    {
        $this->services['validator.builder'] = $instance = \Symfony\Component\Validator\Validation::createValidatorBuilder();

        $instance->setConstraintValidatorFactory(${($ = isset($this->services['validator.validatorfactory']) ? $this->services['validator.validatorfactory'] : $this->getValidatorValidatorFactoryService()) && false ?: ''});
        $instance->setTranslator(${($ = isset($this->services['translator']) ? $this->services['translator'] : $this->get('translator')) && false ?: ''});
        $instance->setTranslationDomain('validators');
        $instance->addXmlMappings(array(0 => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Component/Form/Resources/config/validation.xml'), 1 => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/Resources/config/validation.xml')));
        $instance->enableAnnotationMapping(${($ = isset($this->services['annotationreader']) ? $this->services['annotationreader'] : $this->get('annotationreader')) && false ?: ''});
        $instance->addMethodMapping('loadValidatorMetadata');
        $instance->addObjectInitializers(array(0 => ${($ = isset($this->services['doctrine.orm.validatorinitializer']) ? $this->services['doctrine.orm.validatorinitializer'] : $this->get('doctrine.orm.validatorinitializer')) && false ?: ''}, 1 => new \FOS\UserBundle\Validator\Initializer(${($ = isset($this->services['fosuser.util.canonicalfieldsupdater']) ? $this->services['fosuser.util.canonicalfieldsupdater'] : $this->getFosUserUtilCanonicalFieldsUpdaterService()) && false ?: ''})));
        $instance->addXmlMapping(($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle/DependencyInjection/Compiler/../../Resources/config/storage-validation/orm.xml'));

        return $instance;
    }

    /**
     * Gets the public 'validator.email' shared service.
     *
     * @return \Symfony\Component\Validator\Constraints\EmailValidator
     */
    protected function getValidatorEmailService()
    {
        return $this->services['validator.email'] = new \Symfony\Component\Validator\Constraints\EmailValidator(false);
    }

    /**
     * Gets the public 'validator.expression' shared service.
     *
     * @return \Symfony\Component\Validator\Constraints\ExpressionValidator
     */
    protected function getValidatorExpressionService()
    {
        return $this->services['validator.expression'] = new \Symfony\Component\Validator\Constraints\ExpressionValidator();
    }

    /**
     * Gets the public 'vardumper.clidumper' shared service.
     *
     * @return \Symfony\Component\VarDumper\Dumper\CliDumper
     */
    protected function getVarDumperCliDumperService()
    {
        return $this->services['vardumper.clidumper'] = new \Symfony\Component\VarDumper\Dumper\CliDumper(NULL, 'UTF-8', 0);
    }

    /**
     * Gets the public 'vardumper.cloner' shared service.
     *
     * @return \Symfony\Component\VarDumper\Cloner\VarCloner
     */
    protected function getVarDumperClonerService()
    {
        $this->services['vardumper.cloner'] = $instance = new \Symfony\Component\VarDumper\Cloner\VarCloner();

        $instance->setMaxItems(2500);
        $instance->setMaxString(-1);

        return $instance;
    }

    /**
     * Gets the public 'webprofiler.controller.exception' shared service.
     *
     * @return \Symfony\Bundle\WebProfilerBundle\Controller\ExceptionController
     */
    protected function getWebProfilerControllerExceptionService()
    {
        return $this->services['webprofiler.controller.exception'] = new \Symfony\Bundle\WebProfilerBundle\Controller\ExceptionController(${($ = isset($this->services['profiler']) ? $this->services['profiler'] : $this->get('profiler', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, true);
    }

    /**
     * Gets the public 'webprofiler.controller.profiler' shared service.
     *
     * @return \Symfony\Bundle\WebProfilerBundle\Controller\ProfilerController
     */
    protected function getWebProfilerControllerProfilerService()
    {
        return $this->services['webprofiler.controller.profiler'] = new \Symfony\Bundle\WebProfilerBundle\Controller\ProfilerController(${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['profiler']) ? $this->services['profiler'] : $this->get('profiler', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, array('datacollector.request' => array(0 => 'request', 1 => '@WebProfiler/Collector/request.html.twig'), 'datacollector.time' => array(0 => 'time', 1 => '@WebProfiler/Collector/time.html.twig'), 'datacollector.memory' => array(0 => 'memory', 1 => '@WebProfiler/Collector/memory.html.twig'), 'datacollector.ajax' => array(0 => 'ajax', 1 => '@WebProfiler/Collector/ajax.html.twig'), 'datacollector.form' => array(0 => 'form', 1 => '@WebProfiler/Collector/form.html.twig'), 'datacollector.exception' => array(0 => 'exception', 1 => '@WebProfiler/Collector/exception.html.twig'), 'datacollector.logger' => array(0 => 'logger', 1 => '@WebProfiler/Collector/logger.html.twig'), 'datacollector.events' => array(0 => 'events', 1 => '@WebProfiler/Collector/events.html.twig'), 'datacollector.router' => array(0 => 'router', 1 => '@WebProfiler/Collector/router.html.twig'), 'datacollector.cache' => array(0 => 'cache', 1 => '@WebProfiler/Collector/cache.html.twig'), 'datacollector.translation' => array(0 => 'translation', 1 => '@WebProfiler/Collector/translation.html.twig'), 'datacollector.security' => array(0 => 'security', 1 => '@Security/Collector/security.html.twig'), 'datacollector.twig' => array(0 => 'twig', 1 => '@WebProfiler/Collector/twig.html.twig'), 'datacollector.doctrine' => array(0 => 'db', 1 => '@Doctrine/Collector/db.html.twig'), 'swiftmailer.datacollector' => array(0 => 'swiftmailer', 1 => '@Swiftmailer/Collector/swiftmailer.html.twig'), 'datacollector.dump' => array(0 => 'dump', 1 => '@Debug/Profiler/dump.html.twig'), 'sonata.block.datacollector' => array(0 => 'block', 1 => 'SonataBlockBundle:Profiler:block.html.twig'), 'datacollector.config' => array(0 => 'config', 1 => '@WebProfiler/Collector/config.html.twig')), 'bottom', ${($ = isset($this->services['webprofiler.csp.handler']) ? $this->services['webprofiler.csp.handler'] : $this->getWebProfilerCspHandlerService()) && false ?: ''}, $this->targetDirs[1]);
    }

    /**
     * Gets the public 'webprofiler.controller.router' shared service.
     *
     * @return \Symfony\Bundle\WebProfilerBundle\Controller\RouterController
     */
    protected function getWebProfilerControllerRouterService()
    {
        return $this->services['webprofiler.controller.router'] = new \Symfony\Bundle\WebProfilerBundle\Controller\RouterController(${($ = isset($this->services['profiler']) ? $this->services['profiler'] : $this->get('profiler', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the public 'webprofiler.debugtoolbar' shared service.
     *
     * @return \Symfony\Bundle\WebProfilerBundle\EventListener\WebDebugToolbarListener
     */
    protected function getWebProfilerDebugToolbarService()
    {
        return $this->services['webprofiler.debugtoolbar'] = new \Symfony\Bundle\WebProfilerBundle\EventListener\WebDebugToolbarListener(${($ = isset($this->services['twig']) ? $this->services['twig'] : $this->get('twig')) && false ?: ''}, false, 2, 'bottom', ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, '^/(app([\\w]+)?\\.php/)?wdt', ${($ = isset($this->services['webprofiler.csp.handler']) ? $this->services['webprofiler.csp.handler'] : $this->getWebProfilerCspHandlerService()) && false ?: ''});
    }

    /**
     * Gets the private '10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker
     */
    protected function get10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service()
    {
        return $this->services['10d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7'] = new \Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker($this);
    }

    /**
     * Gets the private '20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7' shared service.
     *
     * @return \Symfony\Component\Config\Resource\SelfCheckingResourceChecker
     */
    protected function get20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7Service()
    {
        return $this->services['20d3e11a2c2a0d79f59fa6c801e286504de10c2a7a1188133cf147b91762c8de7'] = new \Symfony\Component\Config\Resource\SelfCheckingResourceChecker();
    }

    /**
     * Gets the private 'annotations.reader' shared service.
     *
     * @return \Doctrine\Common\Annotations\AnnotationReader
     */
    protected function getAnnotationsReaderService()
    {
        $a = new \Doctrine\Common\Annotations\AnnotationRegistry();
        $a->registerLoader('classexists');

        $this->services['annotations.reader'] = $instance = new \Doctrine\Common\Annotations\AnnotationReader();

        $instance->addGlobalIgnoredName('required', $a);

        return $instance;
    }

    /**
     * Gets the private 'argumentresolver.default' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver
     */
    protected function getArgumentResolverDefaultService()
    {
        return $this->services['argumentresolver.default'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\DefaultValueResolver();
    }

    /**
     * Gets the private 'argumentresolver.request' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver
     */
    protected function getArgumentResolverRequestService()
    {
        return $this->services['argumentresolver.request'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestValueResolver();
    }

    /**
     * Gets the private 'argumentresolver.requestattribute' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver
     */
    protected function getArgumentResolverRequestAttributeService()
    {
        return $this->services['argumentresolver.requestattribute'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\RequestAttributeValueResolver();
    }

    /**
     * Gets the private 'argumentresolver.service' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver
     */
    protected function getArgumentResolverServiceService()
    {
        return $this->services['argumentresolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array()));
    }

    /**
     * Gets the private 'argumentresolver.session' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver
     */
    protected function getArgumentResolverSessionService()
    {
        return $this->services['argumentresolver.session'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\SessionValueResolver();
    }

    /**
     * Gets the private 'argumentresolver.variadic' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver
     */
    protected function getArgumentResolverVariadicService()
    {
        return $this->services['argumentresolver.variadic'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\VariadicValueResolver();
    }

    /**
     * Gets the private 'cache.annotations' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\TraceableAdapter
     */
    protected function getCacheAnnotationsService()
    {
        return $this->services['cache.annotations'] = new \Symfony\Component\Cache\Adapter\TraceableAdapter(${($ = isset($this->services['cache.annotations.recorderinner']) ? $this->services['cache.annotations.recorderinner'] : $this->getCacheAnnotationsRecorderInnerService()) && false ?: ''});
    }

    /**
     * Gets the private 'cache.annotations.recorderinner' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getCacheAnnotationsRecorderInnerService($lazyLoad = true)
    {
        return $this->services['cache.annotations.recorderinner'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('1Wy+Zb++7K', 0, '6gFtqYeg776Jnq645sQEiN', (DIR.'/pools'), ${($ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the private 'cache.app.recorderinner' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\FilesystemAdapter
     */
    public function getCacheAppRecorderInnerService($lazyLoad = true)
    {
        if ($lazyLoad) {

            return $this->services['cache.app.recorderinner'] = SymfonyComponentCacheAdapterFilesystemAdapter000000005e7603da000000001c1e21934905e7acb9fc1a61530eec0a2e144cc0::staticProxyConstructor(
                function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getCacheAppRecorderInnerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                }
            );
        }

        $instance = new \Symfony\Component\Cache\Adapter\FilesystemAdapter('4ZmnyFhdSe', 0, (DIR.'/pools'));

        if ($this->has('monolog.logger.cache')) {
            $instance->setLogger(${($ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
        }

        return $instance;
    }

    /**
     * Gets the private 'cache.serializer.recorderinner' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getCacheSerializerRecorderInnerService($lazyLoad = true)
    {
        return $this->services['cache.serializer.recorderinner'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('DoETL+7oqs', 0, '6gFtqYeg776Jnq645sQEiN', (DIR.'/pools'), ${($ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the private 'cache.system.recorderinner' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getCacheSystemRecorderInnerService($lazyLoad = true)
    {
        return $this->services['cache.system.recorderinner'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('p50p98BR6n', 0, '6gFtqYeg776Jnq645sQEiN', (DIR.'/pools'), ${($ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the private 'cache.validator' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\TraceableAdapter
     */
    protected function getCacheValidatorService()
    {
        return $this->services['cache.validator'] = new \Symfony\Component\Cache\Adapter\TraceableAdapter(${($ = isset($this->services['cache.validator.recorderinner']) ? $this->services['cache.validator.recorderinner'] : $this->getCacheValidatorRecorderInnerService()) && false ?: ''});
    }

    /**
     * Gets the private 'cache.validator.recorderinner' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getCacheValidatorRecorderInnerService($lazyLoad = true)
    {
        return $this->services['cache.validator.recorderinner'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('c22v3Gti70', 0, '6gFtqYeg776Jnq645sQEiN', (DIR.'/pools'), ${($ = isset($this->services['monolog.logger.cache']) ? $this->services['monolog.logger.cache'] : $this->get('monolog.logger.cache', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the private 'console.errorlistener' shared service.
     *
     * @return \Symfony\Component\Console\EventListener\ErrorListener
     */
    protected function getConsoleErrorListenerService()
    {
        return $this->services['console.errorlistener'] = new \Symfony\Component\Console\EventListener\ErrorListener(${($ = isset($this->services['monolog.logger.console']) ? $this->services['monolog.logger.console'] : $this->get('monolog.logger.console', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});
    }

    /**
     * Gets the private 'controllernameconverter' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser
     */
    protected function getControllerNameConverterService()
    {
        return $this->services['controllernameconverter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(${($ = isset($this->services['kernel']) ? $this->services['kernel'] : $this->get('kernel')) && false ?: ''});
    }

    /**
     * Gets the private 'debug.filelinkformatter' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Debug\FileLinkFormatter
     */
    protected function getDebugFileLinkFormatterService()
    {
        return $this->services['debug.filelinkformatter'] = new \Symfony\Component\HttpKernel\Debug\FileLinkFormatter(NULL, ${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, $this->targetDirs[1], '/profiler/open?file=%f&line=%l#line%l');
    }

    /**
     * Gets the private 'debug.logprocessor' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Processor\DebugProcessor
     */
    protected function getDebugLogProcessorService()
    {
        return $this->services['debug.logprocessor'] = new \Symfony\Bridge\Monolog\Processor\DebugProcessor();
    }

    /**
     * Gets the private 'debug.security.access.decisionmanager' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager
     */
    protected function getDebugSecurityAccessDecisionManagerService()
    {
        return $this->services['debug.security.access.decisionmanager'] = new \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager(new \Symfony\Component\Security\Core\Authorization\AccessDecisionManager(new RewindableGenerator(function () {
            yield 0 => ${($ = isset($this->services['security.access.authenticatedvoter']) ? $this->services['security.access.authenticatedvoter'] : $this->getSecurityAccessAuthenticatedVoterService()) && false ?: ''};
            yield 1 => ${($ = isset($this->services['security.access.rolehierarchyvoter']) ? $this->services['security.access.rolehierarchyvoter'] : $this->getSecurityAccessRoleHierarchyVoterService()) && false ?: ''};
            yield 2 => ${($ = isset($this->services['security.access.expressionvoter']) ? $this->services['security.access.expressionvoter'] : $this->getSecurityAccessExpressionVoterService()) && false ?: ''};
        }, 3), 'affirmative', false, true));
    }

    /**
     * Gets the private 'doctrine.dbal.logger.profiling.default' shared service.
     *
     * @return \Doctrine\DBAL\Logging\DebugStack
     */
    protected function getDoctrineDbalLoggerProfilingDefaultService()
    {
        return $this->services['doctrine.dbal.logger.profiling.default'] = new \Doctrine\DBAL\Logging\DebugStack();
    }

    /**
     * Gets the private 'form.serverparams' shared service.
     *
     * @return \Symfony\Component\Form\Util\ServerParams
     */
    protected function getFormServerParamsService()
    {
        return $this->services['form.serverparams'] = new \Symfony\Component\Form\Util\ServerParams(${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack')) && false ?: ''});
    }

    /**
     * Gets the private 'fosuser.userprovider.usernameemail' shared service.
     *
     * @return \FOS\UserBundle\Security\EmailUserProvider
     */
    protected function getFosUserUserProviderUsernameEmailService()
    {
        return $this->services['fosuser.userprovider.usernameemail'] = new \FOS\UserBundle\Security\EmailUserProvider(${($ = isset($this->services['fosuser.usermanager']) ? $this->services['fosuser.usermanager'] : $this->get('fosuser.usermanager')) && false ?: ''});
    }

    /**
     * Gets the private 'fosuser.util.canonicalfieldsupdater' shared service.
     *
     * @return \FOS\UserBundle\Util\CanonicalFieldsUpdater
     */
    protected function getFosUserUtilCanonicalFieldsUpdaterService()
    {
        $a = ${($ = isset($this->services['fosuser.util.emailcanonicalizer']) ? $this->services['fosuser.util.emailcanonicalizer'] : $this->get('fosuser.util.emailcanonicalizer')) && false ?: ''};

        return $this->services['fosuser.util.canonicalfieldsupdater'] = new \FOS\UserBundle\Util\CanonicalFieldsUpdater($a, $a);
    }

    /**
     * Gets the private 'fosuser.util.passwordupdater' shared service.
     *
     * @return \FOS\UserBundle\Util\PasswordUpdater
     */
    protected function getFosUserUtilPasswordUpdaterService()
    {
        return $this->services['fosuser.util.passwordupdater'] = new \FOS\UserBundle\Util\PasswordUpdater(${($ = isset($this->services['security.encoderfactory']) ? $this->services['security.encoderfactory'] : $this->get('security.encoderfactory')) && false ?: ''});
    }

    /**
     * Gets the private 'resolvecontrollernamesubscriber' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber
     */
    protected function getResolveControllerNameSubscriberService()
    {
        return $this->services['resolvecontrollernamesubscriber'] = new \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber(${($ = isset($this->services['controllernameconverter']) ? $this->services['controllernameconverter'] : $this->getControllerNameConverterService()) && false ?: ''});
    }

    /**
     * Gets the private 'router.requestcontext' shared service.
     *
     * @return \Symfony\Component\Routing\RequestContext
     */
    protected function getRouterRequestContextService()
    {
        return $this->services['router.requestcontext'] = new \Symfony\Component\Routing\RequestContext('', 'GET', 'localhost', 'http', 80, 443);
    }

    /**
     * Gets the private 'security.access.authenticatedvoter' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter
     */
    protected function getSecurityAccessAuthenticatedVoterService()
    {
        return $this->services['security.access.authenticatedvoter'] = new \Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter(${($ = isset($this->services['security.authentication.trustresolver']) ? $this->services['security.authentication.trustresolver'] : $this->getSecurityAuthenticationTrustResolverService()) && false ?: ''});
    }

    /**
     * Gets the private 'security.access.expressionvoter' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter
     */
    protected function getSecurityAccessExpressionVoterService()
    {
        return $this->services['security.access.expressionvoter'] = new \Symfony\Component\Security\Core\Authorization\Voter\ExpressionVoter(new \Symfony\Component\Security\Core\Authorization\ExpressionLanguage(), ${($ = isset($this->services['security.authentication.trustresolver']) ? $this->services['security.authentication.trustresolver'] : $this->getSecurityAuthenticationTrustResolverService()) && false ?: ''}, ${($ = isset($this->services['security.rolehierarchy']) ? $this->services['security.rolehierarchy'] : $this->getSecurityRoleHierarchyService()) && false ?: ''});
    }

    /**
     * Gets the private 'security.access.rolehierarchyvoter' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter
     */
    protected function getSecurityAccessRoleHierarchyVoterService()
    {
        return $this->services['security.access.rolehierarchyvoter'] = new \Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter(${($ = isset($this->services['security.rolehierarchy']) ? $this->services['security.rolehierarchy'] : $this->getSecurityRoleHierarchyService()) && false ?: ''});
    }

    /**
     * Gets the private 'security.authentication.manager' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager
     */
    protected function getSecurityAuthenticationManagerService()
    {
        $this->services['security.authentication.manager'] = $instance = new \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager(new RewindableGenerator(function () {
            yield 0 => ${($ = isset($this->services['security.authentication.provider.dao.main']) ? $this->services['security.authentication.provider.dao.main'] : $this->getSecurityAuthenticationProviderDaoMainService()) && false ?: ''};
            yield 1 => ${($ = isset($this->services['security.authentication.provider.anonymous.main']) ? $this->services['security.authentication.provider.anonymous.main'] : $this->getSecurityAuthenticationProviderAnonymousMainService()) && false ?: ''};
        }, 2), true);

        $instance->setEventDispatcher(${($ = isset($this->services['debug.eventdispatcher']) ? $this->services['debug.eventdispatcher'] : $this->get('debug.eventdispatcher')) && false ?: ''});

        return $instance;
    }

    /**
     * Gets the private 'security.authentication.provider.anonymous.main' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider
     */
    protected function getSecurityAuthenticationProviderAnonymousMainService()
    {
        return $this->services['security.authentication.provider.anonymous.main'] = new \Symfony\Component\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider('599edd120b95d0.75981985');
    }

    /**
     * Gets the private 'security.authentication.provider.dao.main' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider
     */
    protected function getSecurityAuthenticationProviderDaoMainService()
    {
        return $this->services['security.authentication.provider.dao.main'] = new \Symfony\Component\Security\Core\Authentication\Provider\DaoAuthenticationProvider(${($ = isset($this->services['fosuser.userprovider.usernameemail']) ? $this->services['fosuser.userprovider.usernameemail'] : $this->getFosUserUserProviderUsernameEmailService()) && false ?: ''}, ${($ = isset($this->services['security.userchecker']) ? $this->services['security.userchecker'] : $this->getSecurityUserCheckerService()) && false ?: ''}, 'main', ${($ = isset($this->services['security.encoderfactory']) ? $this->services['security.encoderfactory'] : $this->get('security.encoderfactory')) && false ?: ''}, true);
    }

    /**
     * Gets the private 'security.authentication.sessionstrategy' shared service.
     *
     * @return \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategy
     */
    protected function getSecurityAuthenticationSessionStrategyService()
    {
        return $this->services['security.authentication.sessionstrategy'] = new \Symfony\Component\Security\Http\Session\SessionAuthenticationStrategy('migrate');
    }

    /**
     * Gets the private 'security.authentication.trustresolver' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolver
     */
    protected function getSecurityAuthenticationTrustResolverService()
    {
        return $this->services['security.authentication.trustresolver'] = new \Symfony\Component\Security\Core\Authentication\AuthenticationTrustResolver('Symfony\\Component\\Security\\Core\\Authentication\\Token\\AnonymousToken', 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\RememberMeToken');
    }

    /**
     * Gets the private 'security.firewall.map' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallMap
     */
    protected function getSecurityFirewallMapService()
    {
        return $this->services['security.firewall.map'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallMap(new \Symfony\Component\DependencyInjection\ServiceLocator(array('security.firewall.map.context.main' => function () {
            return ${($ = isset($this->services['security.firewall.map.context.main']) ? $this->services['security.firewall.map.context.main'] : $this->get('security.firewall.map.context.main')) && false ?: ''};
        })), new RewindableGenerator(function () {
            yield 'security.firewall.map.context.main' => ${($ = isset($this->services['security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db']) ? $this->services['security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db'] : $this->getSecurityRequestMatcherA64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60dbService()) && false ?: ''};
        }, 1));
    }

    /**
     * Gets the private 'security.logouturlgenerator' shared service.
     *
     * @return \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator
     */
    protected function getSecurityLogoutUrlGeneratorService()
    {
        $this->services['security.logouturlgenerator'] = $instance = new \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator(${($ = isset($this->services['requeststack']) ? $this->services['requeststack'] : $this->get('requeststack', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['router']) ? $this->services['router'] : $this->get('router', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''}, ${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});

        $instance->registerListener('main', '/logout', 'logout', 'csrftoken', NULL, NULL);

        return $instance;
    }

    /**
     * Gets the private 'security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\RequestMatcher
     */
    protected function getSecurityRequestMatcherA64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60dbService()
    {
        return $this->services['security.requestmatcher.a64d671f18e5575531d76c1d1154fdc4476cb8a79c02ed7a3469178c6d7b96b5ed4e60db'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/');
    }

    /**
     * Gets the private 'security.rolehierarchy' shared service.
     *
     * @return \Symfony\Component\Security\Core\Role\RoleHierarchy
     */
    protected function getSecurityRoleHierarchyService()
    {
        return $this->services['security.rolehierarchy'] = new \Symfony\Component\Security\Core\Role\RoleHierarchy(array('ROLEADMIN' => array(0 => 'ROLEUSER'), 'ROLESUPERADMIN' => array(0 => 'ROLEADMIN')));
    }

    /**
     * Gets the private 'security.userchecker' shared service.
     *
     * @return \Symfony\Component\Security\Core\User\UserChecker
     */
    protected function getSecurityUserCheckerService()
    {
        return $this->services['security.userchecker'] = new \Symfony\Component\Security\Core\User\UserChecker();
    }

    /**
     * Gets the private 'security.uservalueresolver' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\SecurityUserValueResolver
     */
    protected function getSecurityUserValueResolverService()
    {
        return $this->services['security.uservalueresolver'] = new \Symfony\Bundle\SecurityBundle\SecurityUserValueResolver(${($ = isset($this->services['security.tokenstorage']) ? $this->services['security.tokenstorage'] : $this->get('security.tokenstorage')) && false ?: ''});
    }

    /**
     * Gets the private 'servicelocator.e64d23c3bf770e2cf44b71643280668d' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    protected function getServiceLocatorE64d23c3bf770e2cf44b71643280668dService()
    {
        return $this->services['servicelocator.e64d23c3bf770e2cf44b71643280668d'] = new \Symfony\Component\DependencyInjection\ServiceLocator(array('esi' => function () {
            return ${($ = isset($this->services['fragment.renderer.esi']) ? $this->services['fragment.renderer.esi'] : $this->get('fragment.renderer.esi')) && false ?: ''};
        }, 'hinclude' => function () {
            return ${($ = isset($this->services['fragment.renderer.hinclude']) ? $this->services['fragment.renderer.hinclude'] : $this->get('fragment.renderer.hinclude')) && false ?: ''};
        }, 'inline' => function () {
            return ${($ = isset($this->services['fragment.renderer.inline']) ? $this->services['fragment.renderer.inline'] : $this->get('fragment.renderer.inline')) && false ?: ''};
        }, 'ssi' => function () {
            return ${($ = isset($this->services['fragment.renderer.ssi']) ? $this->services['fragment.renderer.ssi'] : $this->get('fragment.renderer.ssi')) && false ?: ''};
        }));
    }

    /**
     * Gets the private 'session.storage.metadatabag' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag
     */
    protected function getSessionStorageMetadataBagService()
    {
        return $this->services['session.storage.metadatabag'] = new \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag('sf2meta', '0');
    }

    /**
     * Gets the private 'sonata.block.manager' shared service.
     *
     * @return \Sonata\BlockBundle\Block\BlockServiceManager
     */
    protected function getSonataBlockManagerService()
    {
        $this->services['sonata.block.manager'] = $instance = new \Sonata\BlockBundle\Block\BlockServiceManager($this, true, ${($ = isset($this->services['logger']) ? $this->services['logger'] : $this->get('logger', ContainerInterface::NULLONINVALIDREFERENCE)) && false ?: ''});

        $instance->add('sonata.block.service.container', 'sonata.block.service.container', array());
        $instance->add('sonata.block.service.empty', 'sonata.block.service.empty', array());
        $instance->add('sonata.block.service.text', 'sonata.block.service.text', array());
        $instance->add('sonata.block.service.rss', 'sonata.block.service.rss', array());
        $instance->add('sonata.block.service.menu', 'sonata.block.service.menu', array());
        $instance->add('sonata.block.service.template', 'sonata.block.service.template', array());
        $instance->add('sonata.admin.block.adminlist', 'sonata.admin.block.adminlist', array(0 => 'admin'));
        $instance->add('sonata.admin.block.searchresult', 'sonata.admin.block.searchresult', array());
        $instance->add('sonata.admin.block.stats', 'sonata.admin.block.stats', array());

        return $instance;
    }

    /**
     * Gets the private 'swiftmailer.mailer.default.transport.eventdispatcher' shared service.
     *
     * @return \SwiftEventsSimpleEventDispatcher
     */
    protected function getSwiftmailerMailerDefaultTransportEventdispatcherService()
    {
        return $this->services['swiftmailer.mailer.default.transport.eventdispatcher'] = new \SwiftEventsSimpleEventDispatcher();
    }

    /**
     * Gets the private 'templating.locator' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Templating\Loader\TemplateLocator
     */
    protected function getTemplatingLocatorService()
    {
        return $this->services['templating.locator'] = new \Symfony\Bundle\FrameworkBundle\Templating\Loader\TemplateLocator(${($ = isset($this->services['filelocator']) ? $this->services['filelocator'] : $this->get('filelocator')) && false ?: ''}, DIR);
    }

    /**
     * Gets the private 'validator.validatorfactory' shared service.
     *
     * @return \Symfony\Component\Validator\ContainerConstraintValidatorFactory
     */
    protected function getValidatorValidatorFactoryService()
    {
        return $this->services['validator.validatorfactory'] = new \Symfony\Component\Validator\ContainerConstraintValidatorFactory(new \Symfony\Component\DependencyInjection\ServiceLocator(array('Sonata\\CoreBundle\\Validator\\InlineValidator' => function () {
            return ${($ = isset($this->services['sonata.admin.validator.inline']) ? $this->services['sonata.admin.validator.inline'] : $this->get('sonata.admin.validator.inline')) && false ?: ''};
        }, 'Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator' => function () {
            return ${($ = isset($this->services['doctrine.orm.validator.unique']) ? $this->services['doctrine.orm.validator.unique'] : $this->get('doctrine.orm.validator.unique')) && false ?: ''};
        }, 'Symfony\\Component\\Security\\Core\\Validator\\Constraints\\UserPasswordValidator' => function () {
            return ${($ = isset($this->services['security.validator.userpassword']) ? $this->services['security.validator.userpassword'] : $this->get('security.validator.userpassword')) && false ?: ''};
        }, 'Symfony\\Component\\Validator\\Constraints\\EmailValidator' => function () {
            return ${($ = isset($this->services['validator.email']) ? $this->services['validator.email'] : $this->get('validator.email')) && false ?: ''};
        }, 'Symfony\\Component\\Validator\\Constraints\\ExpressionValidator' => function () {
            return ${($ = isset($this->services['validator.expression']) ? $this->services['validator.expression'] : $this->get('validator.expression')) && false ?: ''};
        }, 'doctrine.orm.validator.unique' => function () {
            return ${($ = isset($this->services['doctrine.orm.validator.unique']) ? $this->services['doctrine.orm.validator.unique'] : $this->get('doctrine.orm.validator.unique')) && false ?: ''};
        }, 'security.validator.userpassword' => function () {
            return ${($ = isset($this->services['security.validator.userpassword']) ? $this->services['security.validator.userpassword'] : $this->get('security.validator.userpassword')) && false ?: ''};
        }, 'sonata.admin.validator.inline' => function () {
            return ${($ = isset($this->services['sonata.admin.validator.inline']) ? $this->services['sonata.admin.validator.inline'] : $this->get('sonata.admin.validator.inline')) && false ?: ''};
        }, 'sonata.core.validator.inline' => function () {
            return ${($ = isset($this->services['sonata.core.validator.inline']) ? $this->services['sonata.core.validator.inline'] : $this->get('sonata.core.validator.inline')) && false ?: ''};
        }, 'validator.expression' => function () {
            return ${($ = isset($this->services['validator.expression']) ? $this->services['validator.expression'] : $this->get('validator.expression')) && false ?: ''};
        })));
    }

    /**
     * Gets the private 'webprofiler.csp.handler' shared service.
     *
     * @return \Symfony\Bundle\WebProfilerBundle\Csp\ContentSecurityPolicyHandler
     */
    protected function getWebProfilerCspHandlerService()
    {
        return $this->services['webprofiler.csp.handler'] = new \Symfony\Bundle\WebProfilerBundle\Csp\ContentSecurityPolicyHandler(new \Symfony\Bundle\WebProfilerBundle\Csp\NonceGenerator());
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter($name)
    {
        $name = strtolower($name);

        if (!(isset($this->parameters[$name]) || arraykeyexists($name, $this->parameters) || isset($this->loadedDynamicParameters[$name]))) {
            throw new InvalidArgumentException(sprintf('The parameter "%s" must be defined.', $name));
        }
        if (isset($this->loadedDynamicParameters[$name])) {
            return $this->loadedDynamicParameters[$name] ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
        }

        return $this->parameters[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function hasParameter($name)
    {
        $name = strtolower($name);

        return isset($this->parameters[$name]) || arraykeyexists($name, $this->parameters) || isset($this->loadedDynamicParameters[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($name, $value)
    {
        throw new LogicException('Impossible to call set() on a frozen ParameterBag.');
    }

    /**
     * {@inheritdoc}
     */
    public function getParameterBag()
    {
        if (null === $this->parameterBag) {
            $parameters = $this->parameters;
            foreach ($this->loadedDynamicParameters as $name => $loaded) {
                $parameters[$name] = $loaded ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
            }
            $this->parameterBag = new FrozenParameterBag($parameters);
        }

        return $this->parameterBag;
    }

    private $loadedDynamicParameters = array(
        'kernel.rootdir' => false,
        'kernel.projectdir' => false,
        'kernel.logsdir' => false,
        'kernel.bundlesmetadata' => false,
        'session.savepath' => false,
        'router.resource' => false,
        'doctrinemigrations.dirname' => false,
    );
    private $dynamicParameters = array();

    /**
     * Computes a dynamic parameter.
     *
     * @param string The name of the dynamic parameter to load
     *
     * @return mixed The value of the dynamic parameter
     *
     * @throws InvalidArgumentException When the dynamic parameter does not exist
     */
    private function getDynamicParameter($name)
    {
        switch ($name) {
            case 'kernel.rootdir': $value = ($this->targetDirs[1].'/app'); break;
            case 'kernel.projectdir': $value = $this->targetDirs[1]; break;
            case 'kernel.logsdir': $value = ($this->targetDirs[1].'/var/logs'); break;
            case 'kernel.bundlesmetadata': $value = array(
                'FrameworkBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle'),
                    'namespace' => 'Symfony\\Bundle\\FrameworkBundle',
                ),
                'SecurityBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle'),
                    'namespace' => 'Symfony\\Bundle\\SecurityBundle',
                ),
                'TwigBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle'),
                    'namespace' => 'Symfony\\Bundle\\TwigBundle',
                ),
                'MonologBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/monolog-bundle'),
                    'namespace' => 'Symfony\\Bundle\\MonologBundle',
                ),
                'SwiftmailerBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/swiftmailer-bundle'),
                    'namespace' => 'Symfony\\Bundle\\SwiftmailerBundle',
                ),
                'DoctrineBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/doctrine/doctrine-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineBundle',
                ),
                'SensioFrameworkExtraBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sensio/framework-extra-bundle'),
                    'namespace' => 'Sensio\\Bundle\\FrameworkExtraBundle',
                ),
                'AppBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/src/AppBundle'),
                    'namespace' => 'AppBundle',
                ),
                'FOSUserBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/friendsofsymfony/user-bundle'),
                    'namespace' => 'FOS\\UserBundle',
                ),
                'SonataCoreBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sonata-project/core-bundle'),
                    'namespace' => 'Sonata\\CoreBundle',
                ),
                'SonataBlockBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sonata-project/block-bundle'),
                    'namespace' => 'Sonata\\BlockBundle',
                ),
                'KnpMenuBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/knplabs/knp-menu-bundle'),
                    'namespace' => 'Knp\\Bundle\\MenuBundle',
                ),
                'SonataDoctrineORMAdminBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sonata-project/doctrine-orm-admin-bundle'),
                    'namespace' => 'Sonata\\DoctrineORMAdminBundle',
                ),
                'SonataAdminBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sonata-project/admin-bundle'),
                    'namespace' => 'Sonata\\AdminBundle',
                ),
                'DoctrineMigrationsBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/doctrine/doctrine-migrations-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\MigrationsBundle',
                ),
                'DebugBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/DebugBundle'),
                    'namespace' => 'Symfony\\Bundle\\DebugBundle',
                ),
                'WebProfilerBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle'),
                    'namespace' => 'Symfony\\Bundle\\WebProfilerBundle',
                ),
                'SensioDistributionBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sensio/distribution-bundle'),
                    'namespace' => 'Sensio\\Bundle\\DistributionBundle',
                ),
                'DoctrineFixturesBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/doctrine/doctrine-fixtures-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\FixturesBundle',
                ),
                'SensioGeneratorBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/sensio/generator-bundle'),
                    'namespace' => 'Sensio\\Bundle\\GeneratorBundle',
                ),
                'WebServerBundle' => array(
                    'parent' => NULL,
                    'path' => ($this->targetDirs[1].'/vendor/symfony/symfony/src/Symfony/Bundle/WebServerBundle'),
                    'namespace' => 'Symfony\\Bundle\\WebServerBundle',
                ),
            ); break;
            case 'session.savepath': $value = ($this->targetDirs[1].'/var/sessions/dev'); break;
            case 'router.resource': $value = ($this->targetDirs[1].'/app/config/routingdev.yml'); break;
            case 'doctrinemigrations.dirname': $value = ($this->targetDirs[1].'/src/AppBundle/Migrations/'); break;
            default: throw new InvalidArgumentException(sprintf('The dynamic parameter "%s" must be defined.', $name));
        }
        $this->loadedDynamicParameters[$name] = true;

        return $this->dynamicParameters[$name] = $value;
    }

    /**
     * Gets the default parameters.
     *
     * @return array An array of the default parameters
     */
    protected function getDefaultParameters()
    {
        return array(
            'kernel.environment' => 'dev',
            'kernel.debug' => true,
            'kernel.name' => 'app',
            'kernel.cachedir' => DIR,
            'kernel.bundles' => array(
                'FrameworkBundle' => 'Symfony\\Bundle\\FrameworkBundle\\FrameworkBundle',
                'SecurityBundle' => 'Symfony\\Bundle\\SecurityBundle\\SecurityBundle',
                'TwigBundle' => 'Symfony\\Bundle\\TwigBundle\\TwigBundle',
                'MonologBundle' => 'Symfony\\Bundle\\MonologBundle\\MonologBundle',
                'SwiftmailerBundle' => 'Symfony\\Bundle\\SwiftmailerBundle\\SwiftmailerBundle',
                'DoctrineBundle' => 'Doctrine\\Bundle\\DoctrineBundle\\DoctrineBundle',
                'SensioFrameworkExtraBundle' => 'Sensio\\Bundle\\FrameworkExtraBundle\\SensioFrameworkExtraBundle',
                'AppBundle' => 'AppBundle\\AppBundle',
                'FOSUserBundle' => 'FOS\\UserBundle\\FOSUserBundle',
                'SonataCoreBundle' => 'Sonata\\CoreBundle\\SonataCoreBundle',
                'SonataBlockBundle' => 'Sonata\\BlockBundle\\SonataBlockBundle',
                'KnpMenuBundle' => 'Knp\\Bundle\\MenuBundle\\KnpMenuBundle',
                'SonataDoctrineORMAdminBundle' => 'Sonata\\DoctrineORMAdminBundle\\SonataDoctrineORMAdminBundle',
                'SonataAdminBundle' => 'Sonata\\AdminBundle\\SonataAdminBundle',
                'DoctrineMigrationsBundle' => 'Doctrine\\Bundle\\MigrationsBundle\\DoctrineMigrationsBundle',
                'DebugBundle' => 'Symfony\\Bundle\\DebugBundle\\DebugBundle',
                'WebProfilerBundle' => 'Symfony\\Bundle\\WebProfilerBundle\\WebProfilerBundle',
                'SensioDistributionBundle' => 'Sensio\\Bundle\\DistributionBundle\\SensioDistributionBundle',
                'DoctrineFixturesBundle' => 'Doctrine\\Bundle\\FixturesBundle\\DoctrineFixturesBundle',
                'SensioGeneratorBundle' => 'Sensio\\Bundle\\GeneratorBundle\\SensioGeneratorBundle',
                'WebServerBundle' => 'Symfony\\Bundle\\WebServerBundle\\WebServerBundle',
            ),
            'kernel.charset' => 'UTF-8',
            'kernel.containerclass' => 'appDevDebugProjectContaine',
            'databasehost' => '127.0.0.1',
            'databaseport' => NULL,
            'databasename' => 'gamification',
            'databaseuser' => 'root',
            'databasepassword' => 'root',
            'mailertransport' => 'gmail',
            'mailerhost' => 'smtp.google.com',
            'maileruser' => 'itap.test.for.studens@gmail.com',
            'mailerpassword' => '1qaz@WSX29',
            'secret' => 'ThisTokenIsNotSoSecretChangeIt',
            'locale' => 'en',
            'fragment.renderer.hinclude.globaltemplate' => NULL,
            'fragment.path' => '/fragment',
            'kernel.secret' => 'ThisTokenIsNotSoSecretChangeIt',
            'kernel.httpmethodoverride' => true,
            'kernel.trustedhosts' => array(

            ),
            'kernel.defaultlocale' => 'en',
            'templating.helper.code.filelinkformat' => NULL,
            'debug.filelinkformat' => NULL,
            'session.metadata.storagekey' => 'sf2meta',
            'session.storage.options' => array(
                'cookiehttponly' => true,
                'gcprobability' => 1,
            ),
            'session.metadata.updatethreshold' => '0',
            'form.typeextension.csrf.enabled' => true,
            'form.typeextension.csrf.fieldname' => 'token',
            'templating.loader.cache.path' => NULL,
            'templating.engines' => array(
                0 => 'twig',
            ),
            'validator.mapping.cache.prefix' => '',
            'validator.mapping.cache.file' => (DIR.'/validation.php'),
            'validator.translationdomain' => 'validators',
            'translator.logging' => true,
            'profilerlistener.onlyexceptions' => false,
            'profilerlistener.onlymasterrequests' => false,
            'profiler.storage.dsn' => ('file:'.DIR.'/profiler'),
            'debug.errorhandler.throwat' => -1,
            'debug.container.dump' => (DIR.'/appDevDebugProjectContaine.xml'),
            'router.options.generatorclass' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generatorbaseclass' => 'Symfony\\Component\\Routing\\Generator\\UrlGenerator',
            'router.options.generatordumperclass' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\PhpGeneratorDumper',
            'router.options.matcherclass' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcherbaseclass' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableUrlMatcher',
            'router.options.matcherdumperclass' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\PhpMatcherDumper',
            'router.options.matcher.cacheclass' => 'appDevDebugProjectContaineUrlMatcher',
            'router.options.generator.cacheclass' => 'appDevDebugProjectContaineUrlGenerator',
            'router.requestcontext.host' => 'localhost',
            'router.requestcontext.scheme' => 'http',
            'router.requestcontext.baseurl' => '',
            'router.cacheclassprefix' => 'appDevDebugProjectContaine',
            'requestlistener.httpport' => 80,
            'requestlistener.httpsport' => 443,
            'security.authentication.trustresolver.anonymousclass' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\AnonymousToken',
            'security.authentication.trustresolver.remembermeclass' => 'Symfony\\Component\\Security\\Core\\Authentication\\Token\\RememberMeToken',
            'security.rolehierarchy.roles' => array(
                'ROLEADMIN' => array(
                    0 => 'ROLEUSER',
                ),
                'ROLESUPERADMIN' => array(
                    0 => 'ROLEADMIN',
                ),
            ),
            'security.access.deniedurl' => NULL,
            'security.authentication.manager.erasecredentials' => true,
            'security.authentication.sessionstrategy.strategy' => 'migrate',
            'security.access.alwaysauthenticatebeforegranting' => false,
            'security.authentication.hideusernotfound' => true,
            'twig.exceptionlistener.controller' => 'twig.controller.exception:showAction',
            'twig.form.resources' => array(
                0 => 'formdivlayout.html.twig',
                1 => 'bootstrap3horizontallayout.html.twig',
            ),
            'monolog.usemicroseconds' => true,
            'monolog.swiftmailer.handlers' => array(

            ),
            'monolog.handlerstochannels' => array(
                'monolog.handler.serverlog' => NULL,
                'monolog.handler.console' => array(
                    'type' => 'exclusive',
                    'elements' => array(
                        0 => 'event',
                        1 => 'doctrine',
                        2 => 'console',
                    ),
                ),
                'monolog.handler.main' => array(
                    'type' => 'exclusive',
                    'elements' => array(
                        0 => 'event',
                    ),
                ),
            ),
            'swiftmailer.class' => 'SwiftMailer',
            'swiftmailer.transport.sendmail.class' => 'SwiftTransportSendmailTransport',
            'swiftmailer.transport.mail.class' => 'SwiftTransportMailTransport',
            'swiftmailer.transport.failover.class' => 'SwiftTransportFailoverTransport',
            'swiftmailer.plugin.redirecting.class' => 'SwiftPluginsRedirectingPlugin',
            'swiftmailer.plugin.impersonate.class' => 'SwiftPluginsImpersonatePlugin',
            'swiftmailer.plugin.messagelogger.class' => 'SwiftPluginsMessageLogger',
            'swiftmailer.plugin.antiflood.class' => 'SwiftPluginsAntiFloodPlugin',
            'swiftmailer.transport.smtp.class' => 'SwiftTransportEsmtpTransport',
            'swiftmailer.plugin.blackhole.class' => 'SwiftPluginsBlackholePlugin',
            'swiftmailer.spool.file.class' => 'SwiftFileSpool',
            'swiftmailer.spool.memory.class' => 'SwiftMemorySpool',
            'swiftmailer.emailsender.listener.class' => 'Symfony\\Bundle\\SwiftmailerBundle\\EventListener\\EmailSenderListener',
            'swiftmailer.datacollector.class' => 'Symfony\\Bundle\\SwiftmailerBundle\\DataCollector\\MessageDataCollector',
            'swiftmailer.mailer.default.transport.name' => 'smtp',
            'swiftmailer.mailer.default.transport.smtp.encryption' => 'ssl',
            'swiftmailer.mailer.default.transport.smtp.port' => 465,
            'swiftmailer.mailer.default.transport.smtp.host' => 'smtp.gmail.com',
            'swiftmailer.mailer.default.transport.smtp.username' => 'itap.test.for.studens@gmail.com',
            'swiftmailer.mailer.default.transport.smtp.password' => '1qaz@WSX29',
            'swiftmailer.mailer.default.transport.smtp.authmode' => 'login',
            'swiftmailer.mailer.default.transport.smtp.timeout' => 30,
            'swiftmailer.mailer.default.transport.smtp.sourceip' => NULL,
            'swiftmailer.mailer.default.transport.smtp.localdomain' => NULL,
            'swiftmailer.spool.default.memory.path' => (DIR.'/swiftmailer/spool/default'),
            'swiftmailer.mailer.default.spool.enabled' => true,
            'swiftmailer.mailer.default.plugin.impersonate' => NULL,
            'swiftmailer.mailer.default.singleaddress' => NULL,
            'swiftmailer.mailer.default.delivery.enabled' => true,
            'swiftmailer.spool.enabled' => true,
            'swiftmailer.delivery.enabled' => true,
            'swiftmailer.singleaddress' => NULL,
            'swiftmailer.mailers' => array(
                'default' => 'swiftmailer.mailer.default',
            ),
            'swiftmailer.defaultmailer' => 'default',
            'doctrinecache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrinecache.apcu.class' => 'Doctrine\\Common\\Cache\\ApcuCache',
            'doctrinecache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrinecache.chain.class' => 'Doctrine\\Common\\Cache\\ChainCache',
            'doctrinecache.couchbase.class' => 'Doctrine\\Common\\Cache\\CouchbaseCache',
            'doctrinecache.couchbase.connection.class' => 'Couchbase',
            'doctrinecache.couchbase.hostnames' => 'localhost:8091',
            'doctrinecache.filesystem.class' => 'Doctrine\\Common\\Cache\\FilesystemCache',
            'doctrinecache.phpfile.class' => 'Doctrine\\Common\\Cache\\PhpFileCache',
            'doctrinecache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrinecache.memcache.connection.class' => 'Memcache',
            'doctrinecache.memcache.host' => 'localhost',
            'doctrinecache.memcache.port' => 11211,
            'doctrinecache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrinecache.memcached.connection.class' => 'Memcached',
            'doctrinecache.memcached.host' => 'localhost',
            'doctrinecache.memcached.port' => 11211,
            'doctrinecache.mongodb.class' => 'Doctrine\\Common\\Cache\\MongoDBCache',
            'doctrinecache.mongodb.collection.class' => 'MongoCollection',
            'doctrinecache.mongodb.connection.class' => 'MongoClient',
            'doctrinecache.mongodb.server' => 'localhost:27017',
            'doctrinecache.predis.client.class' => 'Predis\\Client',
            'doctrinecache.predis.scheme' => 'tcp',
            'doctrinecache.predis.host' => 'localhost',
            'doctrinecache.predis.port' => 6379,
            'doctrinecache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrinecache.redis.connection.class' => 'Redis',
            'doctrinecache.redis.host' => 'localhost',
            'doctrinecache.redis.port' => 6379,
            'doctrinecache.riak.class' => 'Doctrine\\Common\\Cache\\RiakCache',
            'doctrinecache.riak.bucket.class' => 'Riak\\Bucket',
            'doctrinecache.riak.connection.class' => 'Riak\\Connection',
            'doctrinecache.riak.bucketpropertylist.class' => 'Riak\\BucketPropertyList',
            'doctrinecache.riak.host' => 'localhost',
            'doctrinecache.riak.port' => 8087,
            'doctrinecache.sqlite3.class' => 'Doctrine\\Common\\Cache\\SQLite3Cache',
            'doctrinecache.sqlite3.connection.class' => 'SQLite3',
            'doctrinecache.void.class' => 'Doctrine\\Common\\Cache\\VoidCache',
            'doctrinecache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrinecache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrinecache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrinecache.security.acl.cache.class' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\Acl\\Model\\AclCache',
            'doctrine.dbal.logger.chain.class' => 'Doctrine\\DBAL\\Logging\\LoggerChain',
            'doctrine.dbal.logger.profiling.class' => 'Doctrine\\DBAL\\Logging\\DebugStack',
            'doctrine.dbal.logger.class' => 'Symfony\\Bridge\\Doctrine\\Logger\\DbalLogger',
            'doctrine.dbal.configuration.class' => 'Doctrine\\DBAL\\Configuration',
            'doctrine.datacollector.class' => 'Doctrine\\Bundle\\DoctrineBundle\\DataCollector\\DoctrineDataCollector',
            'doctrine.dbal.connection.eventmanager.class' => 'Symfony\\Bridge\\Doctrine\\ContainerAwareEventManager',
            'doctrine.dbal.connectionfactory.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ConnectionFactory',
            'doctrine.dbal.events.mysqlsessioninit.class' => 'Doctrine\\DBAL\\Event\\Listeners\\MysqlSessionInit',
            'doctrine.dbal.events.oraclesessioninit.class' => 'Doctrine\\DBAL\\Event\\Listeners\\OracleSessionInit',
            'doctrine.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Registry',
            'doctrine.entitymanagers' => array(
                'default' => 'doctrine.orm.defaultentitymanager',
            ),
            'doctrine.defaultentitymanager' => 'default',
            'doctrine.dbal.connectionfactory.types' => array(

            ),
            'doctrine.connections' => array(
                'default' => 'doctrine.dbal.defaultconnection',
            ),
            'doctrine.defaultconnection' => 'default',
            'doctrine.orm.configuration.class' => 'Doctrine\\ORM\\Configuration',
            'doctrine.orm.entitymanager.class' => 'Doctrine\\ORM\\EntityManager',
            'doctrine.orm.managerconfigurator.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ManagerConfigurator',
            'doctrine.orm.cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine.orm.cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine.orm.cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine.orm.cache.memcachehost' => 'localhost',
            'doctrine.orm.cache.memcacheport' => 11211,
            'doctrine.orm.cache.memcacheinstance.class' => 'Memcache',
            'doctrine.orm.cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine.orm.cache.memcachedhost' => 'localhost',
            'doctrine.orm.cache.memcachedport' => 11211,
            'doctrine.orm.cache.memcachedinstance.class' => 'Memcached',
            'doctrine.orm.cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine.orm.cache.redishost' => 'localhost',
            'doctrine.orm.cache.redisport' => 6379,
            'doctrine.orm.cache.redisinstance.class' => 'Redis',
            'doctrine.orm.cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine.orm.cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine.orm.cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine.orm.metadata.driverchain.class' => 'Doctrine\\Common\\Persistence\\Mapping\\Driver\\MappingDriverChain',
            'doctrine.orm.metadata.annotation.class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
            'doctrine.orm.metadata.xml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedXmlDriver',
            'doctrine.orm.metadata.yml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedYamlDriver',
            'doctrine.orm.metadata.php.class' => 'Doctrine\\ORM\\Mapping\\Driver\\PHPDriver',
            'doctrine.orm.metadata.staticphp.class' => 'Doctrine\\ORM\\Mapping\\Driver\\StaticPHPDriver',
            'doctrine.orm.proxycachewarmer.class' => 'Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer',
            'form.typeguesser.doctrine.class' => 'Symfony\\Bridge\\Doctrine\\Form\\DoctrineOrmTypeGuesser',
            'doctrine.orm.validator.unique.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator',
            'doctrine.orm.validatorinitializer.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\DoctrineInitializer',
            'doctrine.orm.security.user.provider.class' => 'Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider',
            'doctrine.orm.listeners.resolvetargetentity.class' => 'Doctrine\\ORM\\Tools\\ResolveTargetEntityListener',
            'doctrine.orm.listeners.attachentitylisteners.class' => 'Doctrine\\ORM\\Tools\\AttachEntityListenersListener',
            'doctrine.orm.namingstrategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultNamingStrategy',
            'doctrine.orm.namingstrategy.underscore.class' => 'Doctrine\\ORM\\Mapping\\UnderscoreNamingStrategy',
            'doctrine.orm.quotestrategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultQuoteStrategy',
            'doctrine.orm.quotestrategy.ansi.class' => 'Doctrine\\ORM\\Mapping\\AnsiQuoteStrategy',
            'doctrine.orm.entitylistenerresolver.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Mapping\\ContainerAwareEntityListenerResolver',
            'doctrine.orm.secondlevelcache.defaultcachefactory.class' => 'Doctrine\\ORM\\Cache\\DefaultCacheFactory',
            'doctrine.orm.secondlevelcache.defaultregion.class' => 'Doctrine\\ORM\\Cache\\Region\\DefaultRegion',
            'doctrine.orm.secondlevelcache.filelockregion.class' => 'Doctrine\\ORM\\Cache\\Region\\FileLockRegion',
            'doctrine.orm.secondlevelcache.loggerchain.class' => 'Doctrine\\ORM\\Cache\\Logging\\CacheLoggerChain',
            'doctrine.orm.secondlevelcache.loggerstatistics.class' => 'Doctrine\\ORM\\Cache\\Logging\\StatisticsCacheLogger',
            'doctrine.orm.secondlevelcache.cacheconfiguration.class' => 'Doctrine\\ORM\\Cache\\CacheConfiguration',
            'doctrine.orm.secondlevelcache.regionsconfiguration.class' => 'Doctrine\\ORM\\Cache\\RegionsConfiguration',
            'doctrine.orm.autogenerateproxyclasses' => true,
            'doctrine.orm.proxydir' => (DIR.'/doctrine/orm/Proxies'),
            'doctrine.orm.proxynamespace' => 'Proxies',
            'sensioframeworkextra.view.guesser.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Templating\\TemplateGuesser',
            'sensioframeworkextra.controller.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\ControllerListener',
            'sensioframeworkextra.routing.loader.annotdir.class' => 'Symfony\\Component\\Routing\\Loader\\AnnotationDirectoryLoader',
            'sensioframeworkextra.routing.loader.annotfile.class' => 'Symfony\\Component\\Routing\\Loader\\AnnotationFileLoader',
            'sensioframeworkextra.routing.loader.annotclass.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Routing\\AnnotatedRouteControllerLoader',
            'sensioframeworkextra.converter.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\ParamConverterListener',
            'sensioframeworkextra.converter.manager.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\ParamConverterManager',
            'sensioframeworkextra.converter.doctrine.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\DoctrineParamConverter',
            'sensioframeworkextra.converter.datetime.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\Request\\ParamConverter\\DateTimeParamConverter',
            'sensioframeworkextra.view.listener.class' => 'Sensio\\Bundle\\FrameworkExtraBundle\\EventListener\\TemplateListener',
            'fosuser.backendtypeorm' => true,
            'fosuser.security.interactiveloginlistener.class' => 'FOS\\UserBundle\\EventListener\\LastLoginListener',
            'fosuser.security.loginmanager.class' => 'FOS\\UserBundle\\Security\\LoginManager',
            'fosuser.resetting.email.template' => 'email/passwordresetting.email.twig',
            'fosuser.registration.confirmation.template' => '@FOSUser/Registration/email.txt.twig',
            'fosuser.storage' => 'orm',
            'fosuser.firewallname' => 'main',
            'fosuser.modelmanagername' => NULL,
            'fosuser.model.user.class' => 'AppBundle\\Entity\\User',
            'fosuser.profile.form.type' => 'FOS\\UserBundle\\Form\\Type\\ProfileFormType',
            'fosuser.profile.form.name' => 'fosuserprofileform',
            'fosuser.profile.form.validationgroups' => array(
                0 => 'Profile',
                1 => 'Default',
            ),
            'fosuser.registration.confirmation.fromemail' => array(
                'itap.test.for.studens@gmail.com' => 'itap.test.for.studens@gmail.com',
            ),
            'fosuser.registration.confirmation.enabled' => true,
            'fosuser.registration.form.type' => 'AppBundle\\Form\\RegistrationType',
            'fosuser.registration.form.name' => 'fosuserregistrationform',
            'fosuser.registration.form.validationgroups' => array(
                0 => 'Registration',
                1 => 'Default',
            ),
            'fosuser.changepassword.form.type' => 'FOS\\UserBundle\\Form\\Type\\ChangePasswordFormType',
            'fosuser.changepassword.form.name' => 'fosuserchangepasswordform',
            'fosuser.changepassword.form.validationgroups' => array(
                0 => 'ChangePassword',
                1 => 'Default',
            ),
            'fosuser.resetting.email.fromemail' => array(
                'itap.test.for.studens@gmail.com' => 'itap.test.for.studens@gmail.com',
            ),
            'fosuser.resetting.retryttl' => 7200,
            'fosuser.resetting.tokenttl' => 86400,
            'fosuser.resetting.form.type' => 'FOS\\UserBundle\\Form\\Type\\ResettingFormType',
            'fosuser.resetting.form.name' => 'fosuserresettingform',
            'fosuser.resetting.form.validationgroups' => array(
                0 => 'ResetPassword',
                1 => 'Default',
            ),
            'sonata.core.flashmessage.manager.class' => 'Sonata\\CoreBundle\\FlashMessage\\FlashManager',
            'sonata.core.twig.extension.flashmessage.class' => 'Sonata\\CoreBundle\\Twig\\Extension\\FlashMessageExtension',
            'sonata.core.formtype' => 'standard',
            'sonata.core.form.mapping.type' => array(

            ),
            'sonata.core.form.mapping.extension' => array(

            ),
            'sonata.block.service.container.class' => 'Sonata\\BlockBundle\\Block\\Service\\ContainerBlockService',
            'sonata.block.service.empty.class' => 'Sonata\\BlockBundle\\Block\\Service\\EmptyBlockService',
            'sonata.block.service.text.class' => 'Sonata\\BlockBundle\\Block\\Service\\TextBlockService',
            'sonata.block.service.rss.class' => 'Sonata\\BlockBundle\\Block\\Service\\RssBlockService',
            'sonata.block.service.menu.class' => 'Sonata\\BlockBundle\\Block\\Service\\MenuBlockService',
            'sonata.block.service.template.class' => 'Sonata\\BlockBundle\\Block\\Service\\TemplateBlockService',
            'sonata.block.exception.strategy.manager.class' => 'Sonata\\BlockBundle\\Exception\\Strategy\\StrategyManager',
            'sonata.block.container.types' => array(
                0 => 'sonata.block.service.container',
                1 => 'sonata.page.block.container',
                2 => 'sonata.dashboard.block.container',
                3 => 'cmf.block.container',
                4 => 'cmf.block.slideshow',
            ),
            'sonatablock.blocks' => array(
                'sonata.admin.block.adminlist' => array(
                    'contexts' => array(
                        0 => 'admin',
                    ),
                    'templates' => array(

                    ),
                    'cache' => 'sonata.cache.noop',
                    'settings' => array(

                    ),
                ),
            ),
            'sonatablock.blocksbyclass' => array(

            ),
            'sonatablock.cacheblocks' => array(
                'bytype' => array(
                    'sonata.admin.block.adminlist' => 'sonata.cache.noop',
                ),
            ),
            'knpmenu.factory.class' => 'Knp\\Menu\\MenuFactory',
            'knpmenu.factoryextension.routing.class' => 'Knp\\Menu\\Integration\\Symfony\\RoutingExtension',
            'knpmenu.helper.class' => 'Knp\\Menu\\Twig\\Helper',
            'knpmenu.matcher.class' => 'Knp\\Menu\\Matcher\\Matcher',
            'knpmenu.menuprovider.chain.class' => 'Knp\\Menu\\Provider\\ChainProvider',
            'knpmenu.menuprovider.containeraware.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\ContainerAwareProvider',
            'knpmenu.menuprovider.builderalias.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\BuilderAliasProvider',
            'knpmenu.rendererprovider.class' => 'Knp\\Bundle\\MenuBundle\\Renderer\\ContainerAwareProvider',
            'knpmenu.renderer.list.class' => 'Knp\\Menu\\Renderer\\ListRenderer',
            'knpmenu.renderer.list.options' => array(

            ),
            'knpmenu.listener.voters.class' => 'Knp\\Bundle\\MenuBundle\\EventListener\\VoterInitializerListener',
            'knpmenu.voter.router.class' => 'Knp\\Menu\\Matcher\\Voter\\RouteVoter',
            'knpmenu.twig.extension.class' => 'Knp\\Menu\\Twig\\MenuExtension',
            'knpmenu.renderer.twig.class' => 'Knp\\Menu\\Renderer\\TwigRenderer',
            'knpmenu.renderer.twig.options' => array(

            ),
            'knpmenu.renderer.twig.template' => 'KnpMenuBundle::menu.html.twig',
            'knpmenu.defaultrenderer' => 'twig',
            'sonata.admin.manipulator.acl.object.orm.class' => 'Sonata\\DoctrineORMAdminBundle\\Util\\ObjectAclManipulator',
            'sonatadoctrineormadmin.entitymanager' => NULL,
            'sonatadoctrineormadmin.templates' => array(
                'types' => array(
                    'list' => array(
                        'array' => 'SonataAdminBundle:CRUD:listarray.html.twig',
                        'boolean' => 'SonataAdminBundle:CRUD:listboolean.html.twig',
                        'date' => 'SonataAdminBundle:CRUD:listdate.html.twig',
                        'time' => 'SonataAdminBundle:CRUD:listtime.html.twig',
                        'datetime' => 'SonataAdminBundle:CRUD:listdatetime.html.twig',
                        'text' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'textarea' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'email' => 'SonataAdminBundle:CRUD:listemail.html.twig',
                        'trans' => 'SonataAdminBundle:CRUD:listtrans.html.twig',
                        'string' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'smallint' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'bigint' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'integer' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'decimal' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'identifier' => 'SonataAdminBundle:CRUD:liststring.html.twig',
                        'currency' => 'SonataAdminBundle:CRUD:listcurrency.html.twig',
                        'percent' => 'SonataAdminBundle:CRUD:listpercent.html.twig',
                        'choice' => 'SonataAdminBundle:CRUD:listchoice.html.twig',
                        'url' => 'SonataAdminBundle:CRUD:listurl.html.twig',
                        'html' => 'SonataAdminBundle:CRUD:listhtml.html.twig',
                    ),
                    'show' => array(
                        'array' => 'SonataAdminBundle:CRUD:showarray.html.twig',
                        'boolean' => 'SonataAdminBundle:CRUD:showboolean.html.twig',
                        'date' => 'SonataAdminBundle:CRUD:showdate.html.twig',
                        'time' => 'SonataAdminBundle:CRUD:showtime.html.twig',
                        'datetime' => 'SonataAdminBundle:CRUD:showdatetime.html.twig',
                        'text' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'email' => 'SonataAdminBundle:CRUD:showemail.html.twig',
                        'trans' => 'SonataAdminBundle:CRUD:showtrans.html.twig',
                        'string' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'smallint' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'bigint' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'integer' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'decimal' => 'SonataAdminBundle:CRUD:baseshowfield.html.twig',
                        'currency' => 'SonataAdminBundle:CRUD:showcurrency.html.twig',
                        'percent' => 'SonataAdminBundle:CRUD:showpercent.html.twig',
                        'choice' => 'SonataAdminBundle:CRUD:showchoice.html.twig',
                        'url' => 'SonataAdminBundle:CRUD:showurl.html.twig',
                        'html' => 'SonataAdminBundle:CRUD:showhtml.html.twig',
                    ),
                ),
                'form' => array(
                    0 => 'SonataDoctrineORMAdminBundle:Form:formadminfields.html.twig',
                ),
                'filter' => array(
                    0 => 'SonataDoctrineORMAdminBundle:Form:filteradminfields.html.twig',
                ),
            ),
            'sonata.admin.twig.extension.xeditabletypemapping' => array(
                'choice' => 'select',
                'boolean' => 'select',
                'text' => 'text',
                'textarea' => 'textarea',
                'html' => 'textarea',
                'email' => 'email',
                'string' => 'text',
                'smallint' => 'text',
                'bigint' => 'text',
                'integer' => 'number',
                'decimal' => 'number',
                'currency' => 'number',
                'percent' => 'number',
                'url' => 'url',
                'date' => 'date',
            ),
            'sonata.admin.configuration.globalsearch.emptyboxes' => 'show',
            'sonata.admin.configuration.templates' => array(
                'userblock' => 'SonataAdminBundle:Core:userblock.html.twig',
                'addblock' => 'SonataAdminBundle:Core:addblock.html.twig',
                'layout' => 'SonataAdminBundle::standardlayout.html.twig',
                'ajax' => 'SonataAdminBundle::ajaxlayout.html.twig',
                'dashboard' => 'SonataAdminBundle:Core:dashboard.html.twig',
                'search' => 'SonataAdminBundle:Core:search.html.twig',
                'list' => 'SonataAdminBundle:CRUD:list.html.twig',
                'filter' => 'SonataAdminBundle:Form:filteradminfields.html.twig',
                'show' => 'SonataAdminBundle:CRUD:show.html.twig',
                'showcompare' => 'SonataAdminBundle:CRUD:showcompare.html.twig',
                'edit' => 'SonataAdminBundle:CRUD:edit.html.twig',
                'preview' => 'SonataAdminBundle:CRUD:preview.html.twig',
                'history' => 'SonataAdminBundle:CRUD:history.html.twig',
                'acl' => 'SonataAdminBundle:CRUD:acl.html.twig',
                'historyrevisiontimestamp' => 'SonataAdminBundle:CRUD:historyrevisiontimestamp.html.twig',
                'action' => 'SonataAdminBundle:CRUD:action.html.twig',
                'select' => 'SonataAdminBundle:CRUD:listselect.html.twig',
                'listblock' => 'SonataAdminBundle:Block:blockadminlist.html.twig',
                'searchresultblock' => 'SonataAdminBundle:Block:blocksearchresult.html.twig',
                'shortobjectdescription' => 'SonataAdminBundle:Helper:short-object-description.html.twig',
                'delete' => 'SonataAdminBundle:CRUD:delete.html.twig',
                'batch' => 'SonataAdminBundle:CRUD:listbatch.html.twig',
                'batchconfirmation' => 'SonataAdminBundle:CRUD:batchconfirmation.html.twig',
                'innerlistrow' => 'SonataAdminBundle:CRUD:listinnerrow.html.twig',
                'outerlistrowsmosaic' => 'SonataAdminBundle:CRUD:listouterrowsmosaic.html.twig',
                'outerlistrowslist' => 'SonataAdminBundle:CRUD:listouterrowslist.html.twig',
                'outerlistrowstree' => 'SonataAdminBundle:CRUD:listouterrowstree.html.twig',
                'baselistfield' => 'SonataAdminBundle:CRUD:baselistfield.html.twig',
                'pagerlinks' => 'SonataAdminBundle:Pager:links.html.twig',
                'pagerresults' => 'SonataAdminBundle:Pager:results.html.twig',
                'tabmenutemplate' => 'SonataAdminBundle:Core:tabmenutemplate.html.twig',
                'knpmenutemplate' => 'SonataAdminBundle:Menu:sonatamenu.html.twig',
                'actioncreate' => 'SonataAdminBundle:CRUD:dashboardactioncreate.html.twig',
                'buttonacl' => 'SonataAdminBundle:Button:aclbutton.html.twig',
                'buttoncreate' => 'SonataAdminBundle:Button:createbutton.html.twig',
                'buttonedit' => 'SonataAdminBundle:Button:editbutton.html.twig',
                'buttonhistory' => 'SonataAdminBundle:Button:historybutton.html.twig',
                'buttonlist' => 'SonataAdminBundle:Button:listbutton.html.twig',
                'buttonshow' => 'SonataAdminBundle:Button:showbutton.html.twig',
            ),
            'sonata.admin.configuration.adminservices' => array(

            ),
            'sonata.admin.configuration.dashboardgroups' => array(

            ),
            'sonata.admin.configuration.dashboardblocks' => array(
                0 => array(
                    'position' => 'left',
                    'settings' => array(

                    ),
                    'type' => 'sonata.admin.block.adminlist',
                    'roles' => array(

                    ),
                ),
            ),
            'sonata.admin.configuration.sortadmins' => false,
            'sonata.admin.configuration.breadcrumbs' => array(
                'childadminroute' => 'edit',
            ),
            'sonata.admin.security.aclusermanager' => 'fosuser.usermanager',
            'sonata.admin.configuration.security.information' => array(

            ),
            'sonata.admin.configuration.security.adminpermissions' => array(
                0 => 'CREATE',
                1 => 'LIST',
                2 => 'DELETE',
                3 => 'UNDELETE',
                4 => 'EXPORT',
                5 => 'OPERATOR',
                6 => 'MASTER',
            ),
            'sonata.admin.configuration.security.objectpermissions' => array(
                0 => 'VIEW',
                1 => 'EDIT',
                2 => 'DELETE',
                3 => 'UNDELETE',
                4 => 'OPERATOR',
                5 => 'MASTER',
                6 => 'OWNER',
            ),
            'sonata.admin.security.handler.noop.class' => 'Sonata\\AdminBundle\\Security\\Handler\\NoopSecurityHandler',
            'sonata.admin.security.handler.role.class' => 'Sonata\\AdminBundle\\Security\\Handler\\RoleSecurityHandler',
            'sonata.admin.security.handler.acl.class' => 'Sonata\\AdminBundle\\Security\\Handler\\AclSecurityHandler',
            'sonata.admin.security.mask.builder.class' => 'Sonata\\AdminBundle\\Security\\Acl\\Permission\\MaskBuilder',
            'sonata.admin.manipulator.acl.admin.class' => 'Sonata\\AdminBundle\\Util\\AdminAclManipulator',
            'sonata.admin.object.manipulator.acl.admin.class' => 'Sonata\\AdminBundle\\Util\\AdminObjectAclManipulator',
            'sonata.admin.extension.map' => array(
                'admins' => array(

                ),
                'excludes' => array(

                ),
                'implements' => array(

                ),
                'extends' => array(

                ),
                'instanceof' => array(

                ),
                'uses' => array(

                ),
            ),
            'sonata.admin.configuration.filters.persist' => false,
            'sonata.admin.configuration.show.mosaic.button' => true,
            'sonata.admin.configuration.translategrouplabel' => false,
            'doctrinemigrations.namespace' => 'AppBundle\\Migrations',
            'doctrinemigrations.tablename' => 'migrationversions',
            'doctrinemigrations.name' => 'App Migrations',
            'doctrinemigrations.organizemigrations' => false,
            'webprofiler.debugtoolbar.position' => 'bottom',
            'webprofiler.debugtoolbar.interceptredirects' => false,
            'webprofiler.debugtoolbar.mode' => 2,
            'datacollector.templates' => array(
                'datacollector.request' => array(
                    0 => 'request',
                    1 => '@WebProfiler/Collector/request.html.twig',
                ),
                'datacollector.time' => array(
                    0 => 'time',
                    1 => '@WebProfiler/Collector/time.html.twig',
                ),
                'datacollector.memory' => array(
                    0 => 'memory',
                    1 => '@WebProfiler/Collector/memory.html.twig',
                ),
                'datacollector.ajax' => array(
                    0 => 'ajax',
                    1 => '@WebProfiler/Collector/ajax.html.twig',
                ),
                'datacollector.form' => array(
                    0 => 'form',
                    1 => '@WebProfiler/Collector/form.html.twig',
                ),
                'datacollector.exception' => array(
                    0 => 'exception',
                    1 => '@WebProfiler/Collector/exception.html.twig',
                ),
                'datacollector.logger' => array(
                    0 => 'logger',
                    1 => '@WebProfiler/Collector/logger.html.twig',
                ),
                'datacollector.events' => array(
                    0 => 'events',
                    1 => '@WebProfiler/Collector/events.html.twig',
                ),
                'datacollector.router' => array(
                    0 => 'router',
                    1 => '@WebProfiler/Collector/router.html.twig',
                ),
                'datacollector.cache' => array(
                    0 => 'cache',
                    1 => '@WebProfiler/Collector/cache.html.twig',
                ),
                'datacollector.translation' => array(
                    0 => 'translation',
                    1 => '@WebProfiler/Collector/translation.html.twig',
                ),
                'datacollector.security' => array(
                    0 => 'security',
                    1 => '@Security/Collector/security.html.twig',
                ),
                'datacollector.twig' => array(
                    0 => 'twig',
                    1 => '@WebProfiler/Collector/twig.html.twig',
                ),
                'datacollector.doctrine' => array(
                    0 => 'db',
                    1 => '@Doctrine/Collector/db.html.twig',
                ),
                'swiftmailer.datacollector' => array(
                    0 => 'swiftmailer',
                    1 => '@Swiftmailer/Collector/swiftmailer.html.twig',
                ),
                'datacollector.dump' => array(
                    0 => 'dump',
                    1 => '@Debug/Profiler/dump.html.twig',
                ),
                'sonata.block.datacollector' => array(
                    0 => 'block',
                    1 => 'SonataBlockBundle:Profiler:block.html.twig',
                ),
                'datacollector.config' => array(
                    0 => 'config',
                    1 => '@WebProfiler/Collector/config.html.twig',
                ),
            ),
            'console.command.ids' => array(
                'console.command.symfonybundlesecuritybundlecommanduserpasswordencodercommand' => 'console.command.symfonybundlesecuritybundlecommanduserpasswordencodercommand',
                'console.command.sensiolabssecuritycommandsecuritycheckercommand' => 'sensiodistribution.securitychecker.command',
                'console.command.symfonybundlewebserverbundlecommandserverruncommand' => 'console.command.symfonybundlewebserverbundlecommandserverruncommand',
                'console.command.symfonybundlewebserverbundlecommandserverstartcommand' => 'console.command.symfonybundlewebserverbundlecommandserverstartcommand',
                'console.command.symfonybundlewebserverbundlecommandserverstopcommand' => 'console.command.symfonybundlewebserverbundlecommandserverstopcommand',
                'console.command.symfonybundlewebserverbundlecommandserverstatuscommand' => 'console.command.symfonybundlewebserverbundlecommandserverstatuscommand',
            ),
            'sonata.core.form.types' => array(
                0 => 'app.form.registration',
                1 => 'form.type.form',
                2 => 'form.type.choice',
                3 => 'form.type.entity',
                4 => 'fosuser.usernameformtype',
                5 => 'fosuser.profile.form.type',
                6 => 'fosuser.registration.form.type',
                7 => 'fosuser.changepassword.form.type',
                8 => 'fosuser.resetting.form.type',
                9 => 'sonata.core.form.type.array',
                10 => 'sonata.core.form.type.boolean',
                11 => 'sonata.core.form.type.collection',
                12 => 'sonata.core.form.type.translatablechoice',
                13 => 'sonata.core.form.type.daterange',
                14 => 'sonata.core.form.type.datetimerange',
                15 => 'sonata.core.form.type.datepicker',
                16 => 'sonata.core.form.type.datetimepicker',
                17 => 'sonata.core.form.type.daterangepicker',
                18 => 'sonata.core.form.type.datetimerangepicker',
                19 => 'sonata.core.form.type.equal',
                20 => 'sonata.core.form.type.colorselector',
                21 => 'sonata.block.form.type.block',
                22 => 'sonata.block.form.type.containertemplate',
                23 => 'sonata.admin.form.type.admin',
                24 => 'sonata.admin.form.type.modelchoice',
                25 => 'sonata.admin.form.type.modellist',
                26 => 'sonata.admin.form.type.modelreference',
                27 => 'sonata.admin.form.type.modelhidden',
                28 => 'sonata.admin.form.type.modelautocomplete',
                29 => 'sonata.admin.form.type.collection',
                30 => 'sonata.admin.doctrineorm.form.type.choicefieldmask',
                31 => 'sonata.admin.form.filter.type.number',
                32 => 'sonata.admin.form.filter.type.choice',
                33 => 'sonata.admin.form.filter.type.default',
                34 => 'sonata.admin.form.filter.type.date',
                35 => 'sonata.admin.form.filter.type.daterange',
                36 => 'sonata.admin.form.filter.type.datetime',
                37 => 'sonata.admin.form.filter.type.datetimerange',
            ),
            'sonata.core.form.typeextensions' => array(
                0 => 'form.typeextension.form.httpfoundation',
                1 => 'form.typeextension.form.validator',
                2 => 'form.typeextension.repeated.validator',
                3 => 'form.typeextension.submit.validator',
                4 => 'form.typeextension.upload.validator',
                5 => 'form.typeextension.csrf',
                6 => 'form.typeextension.form.datacollector',
                7 => 'sonata.admin.form.extension.field',
                8 => 'sonata.admin.form.extension.field.mopa',
                9 => 'sonata.admin.form.extension.choice',
            ),
        );
    }
}

class DoctrineORMEntityManager000000005e7601c1000000001c1e21934905e7acb9fc1a61530eec0a2e144cc0 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder599edd13886cc040005438 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer599edd13886e6931475329 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties599edd1388670398150991 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getConnection()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getConnection', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getConnection();
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadataFactory()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getMetadataFactory', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getMetadataFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function getExpressionBuilder()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getExpressionBuilder', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getExpressionBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function beginTransaction()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'beginTransaction', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->beginTransaction();
    }

    /**
     * {@inheritDoc}
     */
    public function getCache()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getCache', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getCache();
    }

    /**
     * {@inheritDoc}
     */
    public function transactional($func)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'transactional', array('func' => $func), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->transactional($func);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'commit', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function rollback()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'rollback', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->rollback();
    }

    /**
     * {@inheritDoc}
     */
    public function getClassMetadata($className)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getClassMetadata', array('className' => $className), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getClassMetadata($className);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuery($dql = '')
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'createQuery', array('dql' => $dql), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->createQuery($dql);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedQuery($name)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'createNamedQuery', array('name' => $name), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->createNamedQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->createNativeQuery($sql, $rsm);
    }

    /**
     * {@inheritDoc}
     */
    public function createNamedNativeQuery($name)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->createNamedNativeQuery($name);
    }

    /**
     * {@inheritDoc}
     */
    public function createQueryBuilder()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'createQueryBuilder', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->createQueryBuilder();
    }

    /**
     * {@inheritDoc}
     */
    public function flush($entity = null)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'flush', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->flush($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function find($entityName, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'find', array('entityName' => $entityName, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->find($entityName, $id, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getReference($entityName, $id)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getReference($entityName, $id);
    }

    /**
     * {@inheritDoc}
     */
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getPartialReference($entityName, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function clear($entityName = null)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'clear', array('entityName' => $entityName), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->clear($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'close', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->close();
    }

    /**
     * {@inheritDoc}
     */
    public function persist($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'persist', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->persist($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'remove', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->remove($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function refresh($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'refresh', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->refresh($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function detach($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'detach', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->detach($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function merge($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'merge', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->merge($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function copy($entity, $deep = false)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->copy($entity, $deep);
    }

    /**
     * {@inheritDoc}
     */
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->lock($entity, $lockMode, $lockVersion);
    }

    /**
     * {@inheritDoc}
     */
    public function getRepository($entityName)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getRepository', array('entityName' => $entityName), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getRepository($entityName);
    }

    /**
     * {@inheritDoc}
     */
    public function contains($entity)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'contains', array('entity' => $entity), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->contains($entity);
    }

    /**
     * {@inheritDoc}
     */
    public function getEventManager()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getEventManager', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getEventManager();
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguration()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getConfiguration', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getConfiguration();
    }

    /**
     * {@inheritDoc}
     */
    public function isOpen()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'isOpen', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->isOpen();
    }

    /**
     * {@inheritDoc}
     */
    public function getUnitOfWork()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getUnitOfWork', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getUnitOfWork();
    }

    /**
     * {@inheritDoc}
     */
    public function getHydrator($hydrationMode)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function newHydrator($hydrationMode)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->newHydrator($hydrationMode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyFactory()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getProxyFactory', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getProxyFactory();
    }

    /**
     * {@inheritDoc}
     */
    public function initializeObject($obj)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'initializeObject', array('obj' => $obj), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->initializeObject($obj);
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'getFilters', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->getFilters();
    }

    /**
     * {@inheritDoc}
     */
    public function isFiltersStateClean()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'isFiltersStateClean', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->isFiltersStateClean();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFilters()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'hasFilters', array(), $this->initializer599edd13886e6931475329);

        return $this->valueHolder599edd13886cc040005438->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?: $reflection = new \ReflectionClass(CLASS);
        $instance = (new \ReflectionClass(getclass()))->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->invoke($instance);

        $instance->initializer599edd13886e6931475329 = $initializer;

        return $instance;
    }

    /**
     * {@inheritDoc}
     */
    protected function construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder599edd13886cc040005438) {
            $reflection = $reflection ?: new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder599edd13886cc040005438 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->invoke($this);

        }

        $this->valueHolder599edd13886cc040005438->construct($conn, $config, $eventManager);
    }

    /**
     * @param string $name
     */
    public function & get($name)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'get', ['name' => $name], $this->initializer599edd13886e6931475329);

        if (isset(self::$publicProperties599edd1388670398150991[$name])) {
            return $this->valueHolder599edd13886cc040005438->$name;
        }

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd13886cc040005438;

            $backtrace = debugbacktrace(false);
            triggererror(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    getparentclass($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \EUSERNOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder599edd13886cc040005438;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'set', array('name' => $name, 'value' => $value), $this->initializer599edd13886e6931475329);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd13886cc040005438;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder599edd13886cc040005438;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function isset($name)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'isset', array('name' => $name), $this->initializer599edd13886e6931475329);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd13886cc040005438;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder599edd13886cc040005438;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function unset($name)
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'unset', array('name' => $name), $this->initializer599edd13886e6931475329);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd13886cc040005438;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder599edd13886cc040005438;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function clone()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'clone', array(), $this->initializer599edd13886e6931475329);

        $this->valueHolder599edd13886cc040005438 = clone $this->valueHolder599edd13886cc040005438;
    }

    public function sleep()
    {
        $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'sleep', array(), $this->initializer599edd13886e6931475329);

        return array('valueHolder599edd13886cc040005438');
    }

    public function wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->invoke($this);
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer599edd13886e6931475329 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer599edd13886e6931475329;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy() : bool
    {
        return $this->initializer599edd13886e6931475329 && $this->initializer599edd13886e6931475329->invoke($this->valueHolder599edd13886cc040005438, $this, 'initializeProxy', array(), $this->initializer599edd13886e6931475329);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder599edd13886cc040005438;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder599edd13886cc040005438;
    }


}

class SymfonyComponentCacheAdapterFilesystemAdapter000000005e7603da000000001c1e21934905e7acb9fc1a61530eec0a2e144cc0 extends \Symfony\Component\Cache\Adapter\FilesystemAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder599edd1395237407114908 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer599edd139524f551426359 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties599edd13951e8910582365 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getItem($key)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'getItem', array('key' => $key), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->getItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function getItems(array $keys = array())
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'getItems', array('keys' => $keys), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->getItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'save', array('item' => $item), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->save($item);
    }

    /**
     * {@inheritDoc}
     */
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'saveDeferred', array('item' => $item), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->saveDeferred($item);
    }

    /**
     * {@inheritDoc}
     */
    public function commit()
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'commit', array(), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->commit();
    }

    /**
     * {@inheritDoc}
     */
    public function destruct()
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'destruct', array(), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->destruct();
    }

    /**
     * {@inheritDoc}
     */
    public function hasItem($key)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'hasItem', array('key' => $key), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->hasItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function clear()
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'clear', array(), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->clear();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItem($key)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'deleteItem', array('key' => $key), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->deleteItem($key);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteItems(array $keys)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'deleteItems', array('keys' => $keys), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->deleteItems($keys);
    }

    /**
     * {@inheritDoc}
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'setLogger', array('logger' => $logger), $this->initializer599edd139524f551426359);

        return $this->valueHolder599edd1395237407114908->setLogger($logger);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?: $reflection = new \ReflectionClass(CLASS);
        $instance = (new \ReflectionClass(getclass()))->newInstanceWithoutConstructor();

        unset($instance->maxIdLength, $instance->logger);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\FilesystemAdapter $instance) {
            unset($instance->directory, $instance->tmp);
        }, $instance, 'Symfony\\Component\\Cache\\Adapter\\FilesystemAdapter')->invoke($instance);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\AbstractAdapter $instance) {
            unset($instance->createCacheItem, $instance->mergeByLifetime, $instance->namespace, $instance->deferred);
        }, $instance, 'Symfony\\Component\\Cache\\Adapter\\AbstractAdapter')->invoke($instance);

        $instance->initializer599edd139524f551426359 = $initializer;

        return $instance;
    }

    /**
     * {@inheritDoc}
     */
    public function construct($namespace = '', $defaultLifetime = 0, $directory = null)
    {
        static $reflection;

        if (! $this->valueHolder599edd1395237407114908) {
            $reflection = $reflection ?: new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\FilesystemAdapter');
            $this->valueHolder599edd1395237407114908 = $reflection->newInstanceWithoutConstructor();
        unset($this->maxIdLength, $this->logger);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\FilesystemAdapter $instance) {
            unset($instance->directory, $instance->tmp);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\FilesystemAdapter')->invoke($this);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\AbstractAdapter $instance) {
            unset($instance->createCacheItem, $instance->mergeByLifetime, $instance->namespace, $instance->deferred);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\AbstractAdapter')->invoke($this);

        }

        $this->valueHolder599edd1395237407114908->construct($namespace, $defaultLifetime, $directory);
    }

    /**
     * @param string $name
     */
    public function & get($name)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'get', ['name' => $name], $this->initializer599edd139524f551426359);

        if (isset(self::$publicProperties599edd13951e8910582365[$name])) {
            return $this->valueHolder599edd1395237407114908->$name;
        }

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd1395237407114908;

            $backtrace = debugbacktrace(false);
            triggererror(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    getparentclass($this),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \EUSERNOTICE
            );
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder599edd1395237407114908;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'set', array('name' => $name, 'value' => $value), $this->initializer599edd139524f551426359);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd1395237407114908;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder599edd1395237407114908;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function isset($name)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'isset', array('name' => $name), $this->initializer599edd139524f551426359);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd1395237407114908;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder599edd1395237407114908;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function unset($name)
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'unset', array('name' => $name), $this->initializer599edd139524f551426359);

        $realInstanceReflection = new \ReflectionClass(getparentclass($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder599edd1395237407114908;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder599edd1395237407114908;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
        $backtrace = debugbacktrace(true);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, getclass($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function clone()
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'clone', array(), $this->initializer599edd139524f551426359);

        $this->valueHolder599edd1395237407114908 = clone $this->valueHolder599edd1395237407114908;
    }

    public function sleep()
    {
        $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'sleep', array(), $this->initializer599edd139524f551426359);

        return array('valueHolder599edd1395237407114908');
    }

    public function wakeup()
    {
        unset($this->maxIdLength, $this->logger);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\FilesystemAdapter $instance) {
            unset($instance->directory, $instance->tmp);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\FilesystemAdapter')->invoke($this);

        \Closure::bind(function (\Symfony\Component\Cache\Adapter\AbstractAdapter $instance) {
            unset($instance->createCacheItem, $instance->mergeByLifetime, $instance->namespace, $instance->deferred);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\AbstractAdapter')->invoke($this);
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer599edd139524f551426359 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer599edd139524f551426359;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy() : bool
    {
        return $this->initializer599edd139524f551426359 && $this->initializer599edd139524f551426359->invoke($this->valueHolder599edd1395237407114908, $this, 'initializeProxy', array(), $this->initializer599edd139524f551426359);
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder599edd1395237407114908;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder599edd1395237407114908;
    }


}
