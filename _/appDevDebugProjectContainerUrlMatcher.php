<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContaineUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContaineUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/')) {
            // wdt
            if (0 === strpos($pathinfo, '/wdt') && pregmatch('#^/wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(arrayreplace($matches, array('route' => 'wdt')), array (  'controller' => 'webprofiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/profiler')) {
                // profilerhome
                if ('/profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'profilerhome');
                    }

                    return array (  'controller' => 'webprofiler.controller.profiler:homeAction',  'route' => 'profilerhome',);
                }

                if (0 === strpos($pathinfo, '/profiler/search')) {
                    // profilersearch
                    if ('/profiler/search' === $pathinfo) {
                        return array (  'controller' => 'webprofiler.controller.profiler:searchAction',  'route' => 'profilersearch',);
                    }

                    // profilersearchbar
                    if ('/profiler/searchbar' === $pathinfo) {
                        return array (  'controller' => 'webprofiler.controller.profiler:searchBarAction',  'route' => 'profilersearchbar',);
                    }

                }

                // profilerphpinfo
                if ('/profiler/phpinfo' === $pathinfo) {
                    return array (  'controller' => 'webprofiler.controller.profiler:phpinfoAction',  'route' => 'profilerphpinfo',);
                }

                // profilersearchresults
                if (pregmatch('#^/profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'profilersearchresults')), array (  'controller' => 'webprofiler.controller.profiler:searchResultsAction',));
                }

                // profileropenfile
                if ('/profiler/open' === $pathinfo) {
                    return array (  'controller' => 'webprofiler.controller.profiler:openAction',  'route' => 'profileropenfile',);
                }

                // profiler
                if (pregmatch('#^/profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'profiler')), array (  'controller' => 'webprofiler.controller.profiler:panelAction',));
                }

                // profilerrouter
                if (pregmatch('#^/profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'profilerrouter')), array (  'controller' => 'webprofiler.controller.router:panelAction',));
                }

                // profilerexception
                if (pregmatch('#^/profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'profilerexception')), array (  'controller' => 'webprofiler.controller.exception:showAction',));
                }

                // profilerexceptioncss
                if (pregmatch('#^/profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'profilerexceptioncss')), array (  'controller' => 'webprofiler.controller.exception:cssAction',));
                }

            }

            // twigerrortest
            if (0 === strpos($pathinfo, '/error') && pregmatch('#^/error/(?P<code>\\d+)(?:\\.(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(arrayreplace($matches, array('route' => 'twigerrortest')), array (  'controller' => 'twig.controller.previewerror:previewErrorPageAction',  'format' => 'html',));
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                $allow = arraymerge($allow, array('GET', 'POST'));
                goto nothomepage;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  'controller' => 'AppBundle\\Controller\\IndexController::indexAction',  'route' => 'homepage',);
        }
        nothomepage:

        if (0 === strpos($pathinfo, '/login')) {
            // fosusersecuritylogin
            if ('/login' === $pathinfo) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosusersecuritylogin;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  'route' => 'fosusersecuritylogin',);
            }
            notfosusersecuritylogin:

            // fosusersecuritycheck
            if ('/logincheck' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto notfosusersecuritycheck;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  'route' => 'fosusersecuritycheck',);
            }
            notfosusersecuritycheck:

        }

        elseif (0 === strpos($pathinfo, '/logout')) {
            // fosusersecuritylogout
            if ('/logout' === $pathinfo) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosusersecuritylogout;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  'route' => 'fosusersecuritylogout',);
            }
            notfosusersecuritylogout:

            // logout
            if ('/logout' === $pathinfo) {
                return array('route' => 'logout');
            }

        }

        elseif (0 === strpos($pathinfo, '/profile')) {
            // fosuserprofileshow
            if ('/profile' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto notfosuserprofileshow;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fosuserprofileshow');
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  'route' => 'fosuserprofileshow',);
            }
            notfosuserprofileshow:

            // fosuserprofileedit
            if ('/profile/edit' === $pathinfo) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosuserprofileedit;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  'route' => 'fosuserprofileedit',);
            }
            notfosuserprofileedit:

            // fosuserchangepassword
            if ('/profile/change-password' === $pathinfo) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosuserchangepassword;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  'route' => 'fosuserchangepassword',);
            }
            notfosuserchangepassword:

        }

        elseif (0 === strpos($pathinfo, '/register')) {
            // fosuserregistrationregister
            if ('/register' === $trimmedPathinfo) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosuserregistrationregister;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fosuserregistrationregister');
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  'route' => 'fosuserregistrationregister',);
            }
            notfosuserregistrationregister:

            // fosuserregistrationcheckemail
            if ('/register/check-email' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto notfosuserregistrationcheckemail;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  'route' => 'fosuserregistrationcheckemail',);
            }
            notfosuserregistrationcheckemail:

            if (0 === strpos($pathinfo, '/register/confirm')) {
                // fosuserregistrationconfirm
                if (pregmatch('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto notfosuserregistrationconfirm;
                    }

                    return $this->mergeDefaults(arrayreplace($matches, array('route' => 'fosuserregistrationconfirm')), array (  'controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                }
                notfosuserregistrationconfirm:

                // fosuserregistrationconfirmed
                if ('/register/confirmed' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto notfosuserregistrationconfirmed;
                    }

                    return array (  'controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  'route' => 'fosuserregistrationconfirmed',);
                }
                notfosuserregistrationconfirmed:

            }

        }

        elseif (0 === strpos($pathinfo, '/resetting')) {
            // fosuserresettingrequest
            if ('/resetting/request' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto notfosuserresettingrequest;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  'route' => 'fosuserresettingrequest',);
            }
            notfosuserresettingrequest:

            // fosuserresettingreset
            if (0 === strpos($pathinfo, '/resetting/reset') && pregmatch('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!inarray($canonicalMethod, array('GET', 'POST'))) {
                    $allow = arraymerge($allow, array('GET', 'POST'));
                    goto notfosuserresettingreset;
                }

                return $this->mergeDefaults(arrayreplace($matches, array('route' => 'fosuserresettingreset')), array (  'controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
            }
            notfosuserresettingreset:

            // fosuserresettingsendemail
            if ('/resetting/send-email' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto notfosuserresettingsendemail;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  'route' => 'fosuserresettingsendemail',);
            }
            notfosuserresettingsendemail:

            // fosuserresettingcheckemail
            if ('/resetting/check-email' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto notfosuserresettingcheckemail;
                }

                return array (  'controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  'route' => 'fosuserresettingcheckemail',);
            }
            notfosuserresettingcheckemail:

        }

        elseif (0 === strpos($pathinfo, '/admin')) {
            // sonataadminredirect
            if ('/admin' === $trimmedPathinfo) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonataadminredirect');
                }

                return array (  'controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonataadmindashboard',  'permanent' => 'true',  'route' => 'sonataadminredirect',);
            }

            // sonataadmindashboard
            if ('/admin/dashboard' === $pathinfo) {
                return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  'route' => 'sonataadmindashboard',);
            }

            if (0 === strpos($pathinfo, '/admin/core')) {
                if (0 === strpos($pathinfo, '/admin/core/get-')) {
                    // sonataadminretrieveformelement
                    if ('/admin/core/get-form-field-element' === $pathinfo) {
                        return array (  'controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  'route' => 'sonataadminretrieveformelement',);
                    }

                    // sonataadminshortobjectinformation
                    if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && pregmatch('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<format>html|json))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'sonataadminshortobjectinformation')), array (  'controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  'format' => 'html',));
                    }

                    // sonataadminretrieveautocompleteitems
                    if ('/admin/core/get-autocomplete-items' === $pathinfo) {
                        return array (  'controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  'route' => 'sonataadminretrieveautocompleteitems',);
                    }

                }

                // sonataadminappendformelement
                if ('/admin/core/append-form-field-element' === $pathinfo) {
                    return array (  'controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  'route' => 'sonataadminappendformelement',);
                }

                // sonataadminsetobjectfieldvalue
                if ('/admin/core/set-object-field-value' === $pathinfo) {
                    return array (  'controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  'route' => 'sonataadminsetobjectfieldvalue',);
                }

            }

            // sonataadminsearch
            if ('/admin/search' === $pathinfo) {
                return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  'route' => 'sonataadminsearch',);
            }

            if (0 === strpos($pathinfo, '/admin/app')) {
                if (0 === strpos($pathinfo, '/admin/app/user')) {
                    // adminappuserlist
                    if ('/admin/app/user/list' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappuserlist',  'route' => 'adminappuserlist',);
                    }

                    // adminappusercreate
                    if ('/admin/app/user/create' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappusercreate',  'route' => 'adminappusercreate',);
                    }

                    // adminappuserbatch
                    if ('/admin/app/user/batch' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappuserbatch',  'route' => 'adminappuserbatch',);
                    }

                    // adminappuseredit
                    if (pregmatch('#^/admin/app/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappuseredit')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappuseredit',));
                    }

                    // adminappuserdelete
                    if (pregmatch('#^/admin/app/user/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappuserdelete')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappuserdelete',));
                    }

                    // adminappusershow
                    if (pregmatch('#^/admin/app/user/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappusershow')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappusershow',));
                    }

                    // adminappuserexport
                    if ('/admin/app/user/export' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  'sonataadmin' => 'app.admin.user',  'sonataname' => 'adminappuserexport',  'route' => 'adminappuserexport',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/app/office')) {
                    // adminappofficelist
                    if ('/admin/app/office/list' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficelist',  'route' => 'adminappofficelist',);
                    }

                    // adminappofficecreate
                    if ('/admin/app/office/create' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficecreate',  'route' => 'adminappofficecreate',);
                    }

                    // adminappofficebatch
                    if ('/admin/app/office/batch' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficebatch',  'route' => 'adminappofficebatch',);
                    }

                    // adminappofficeedit
                    if (pregmatch('#^/admin/app/office/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappofficeedit')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficeedit',));
                    }

                    // adminappofficedelete
                    if (pregmatch('#^/admin/app/office/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappofficedelete')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficedelete',));
                    }

                    // adminappofficeshow
                    if (pregmatch('#^/admin/app/office/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappofficeshow')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficeshow',));
                    }

                    // adminappofficeexport
                    if ('/admin/app/office/export' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  'sonataadmin' => 'app.admin.office',  'sonataname' => 'adminappofficeexport',  'route' => 'adminappofficeexport',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/app/partner')) {
                    // adminapppartnerlist
                    if ('/admin/app/partner/list' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnerlist',  'route' => 'adminapppartnerlist',);
                    }

                    // adminapppartnercreate
                    if ('/admin/app/partner/create' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnercreate',  'route' => 'adminapppartnercreate',);
                    }

                    // adminapppartnerbatch
                    if ('/admin/app/partner/batch' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnerbatch',  'route' => 'adminapppartnerbatch',);
                    }

                    // adminapppartneredit
                    if (pregmatch('#^/admin/app/partner/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminapppartneredit')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartneredit',));
                    }

                    // adminapppartnerdelete
                    if (pregmatch('#^/admin/app/partner/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminapppartnerdelete')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnerdelete',));
                    }

                    // adminapppartnershow
                    if (pregmatch('#^/admin/app/partner/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminapppartnershow')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnershow',));
                    }

                    // adminapppartnerexport
                    if ('/admin/app/partner/export' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  'sonataadmin' => 'app.admin.partner',  'sonataname' => 'adminapppartnerexport',  'route' => 'adminapppartnerexport',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/app/region')) {
                    // adminappregionlist
                    if ('/admin/app/region/list' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregionlist',  'route' => 'adminappregionlist',);
                    }

                    // adminappregioncreate
                    if ('/admin/app/region/create' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregioncreate',  'route' => 'adminappregioncreate',);
                    }

                    // adminappregionbatch
                    if ('/admin/app/region/batch' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregionbatch',  'route' => 'adminappregionbatch',);
                    }

                    // adminappregionedit
                    if (pregmatch('#^/admin/app/region/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappregionedit')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregionedit',));
                    }

                    // adminappregiondelete
                    if (pregmatch('#^/admin/app/region/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappregiondelete')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregiondelete',));
                    }

                    // adminappregionshow
                    if (pregmatch('#^/admin/app/region/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(arrayreplace($matches, array('route' => 'adminappregionshow')), array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregionshow',));
                    }

                    // adminappregionexport
                    if ('/admin/app/region/export' === $pathinfo) {
                        return array (  'controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  'sonataadmin' => 'app.admin.region',  'sonataname' => 'adminappregionexport',  'route' => 'adminappregionexport',);
                    }

                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(arrayunique($allow)) : new ResourceNotFoundException();
    }
}
