Automatization Gamification for Beeline
=======================================
Добро Пожаловать Разработчик)

Это главная страница проекта под названием "AGB"                                                 
A = Automatization, G = Gamification, B = Beeline 

Протестить сайт можно на демо сервере, перейдя по ссылке
http://139.59.69.11
 

Задача ПО автоматизировать процессы внутренней геймификации отдела продаж 
Что внутри ?
--------------

В нашем проекте на Symfony настроены следующие значения:

  * AppBundle, который вы можете использовать для начала кодирования;

  * Twig как единственный сконфигурированный механизм шаблонов;

  * Доктрина ORM / DBAL;

  * Swiftmailer;

  * Аннотации включены для всего.

Он поставляется со следующими пакетами:

  * ** FrameworkBundle ** - Основной набор модулей Symfony

  * [** SensioFrameworkExtraBundle **] [6] - добавляет несколько улучшений, в том числе
    возможность создания шаблонов и маршрутизации

  * [** DoctrineBundle **] [7] - Добавляет поддержку Doctrine ORM

  * [** TwigBundle **] [8] - Добавляет поддержку движка Temig для Twig

  * [** FOSUserBundle **] [9] - основывается на том, чтобы быстро и легко хранить пользователей в базе данных, 
  а также есть функция регистрации, сброса пароля и страницы профиля.

  * [** SwiftmailerBundle **] [10] - добавляет поддержку Swiftmailer, библиотеки для
    отправка писем

  * [** SonataAdminBundle **] [11] - содержит основные библиотеки и службы

  * ** SonataDoctrineORMAdminBundle ** - объединяет проект Doctrine ORM с основным пакетом администрирования

  * ** LiuggioExcelBundle ** - добавляет возможность парсить excel файлы

  * [** VichUploaderBundle**] [13] - добавляет загрузку файлов в наши внутренние файлы,
  как изображения, так и обычные файлы, такие как файлы PDF.

  * ** Behat ** - самый популярный фреймфорк для тестов в PHP

Все библиотеки и пакеты, включенные в проект, являются
выпущенный под лицензией MIT или BSD.

Наслаждайтесь!

[1]:  https://symfony.com/doc/3.2/setup.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.2/doctrine.html
[8]:  https://symfony.com/doc/3.2/templating.html
[9]:  https://github.com/FriendsOfSymfony/FOSUserBundle
[10]: https://symfony.com/doc/3.2/email.html
[11]: https://symfony.com/doc/master/bundles/SonataAdminBundle/index.html
[12]: https://symfony.com/doc/3.2/assetic/asset_management.html
[13]: https://symfony.com/doc/master/bundles/EasyAdminBundle/integration/vichuploaderbundle.html
